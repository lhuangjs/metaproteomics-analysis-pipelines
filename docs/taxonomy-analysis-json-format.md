## peptide Quantitative information

```
{
    "sampleNameList": [
        "sample1",
        "sample2", ...
    ],
    "rank2TaxonQuant": {
        "superkingdom": [
            {
                "taxon": {
                    "id": 2,
                    "name": "Bacteria",
                    "parentId": 131567,
                    "rank": "superkingdom",
                    "lineageIdMap": {
                        "superkingdom": 2
                    },
                    "lineageNameMap": {
                        "superkingdom": "Bacteria"
                    }
                },
                "peptCountForTaxon": 7,
                "peptCountForSubtaxa": 32,
                "quantVals": [
                    824791.0190590002,
                    815245.636056,
                    813251.018767,
                    ...
                ]
            }
        ],
    ...
    }
}
```

## protein information related to peptide

```
{
"superkingdom": [
            {
                "totalPeptCountForTaxon": 39,
                "proteins": [
                  "prot1",
                  "prot2"
                ]
            }
            ...
        ],
    ...
}
```

## GO annotation
```
{
  "Molecular Function": [
    {"id":  "xxx", "protPercList":  [0.9, 0.4]},
    ...
  ]
}
```
