package cn.phoenixcenter.metaproteomics.oral;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;

public class HOMDProcessorTest {
    @Test
    public void parseFasta() throws IOException {
        Map<String, HOMDTaxonInfo> gid2Info = HOMDProcessor.extractTid2Gid(
                "/home/huangjs/archive/java/metaproteomics/raw_data/saliva#2/data/homd_taxonomy_table.txt",
                "/home/huangjs/archive/java/metaproteomics/raw_data/saliva#2/data/homd_taxonomy_level.txt"
                );
        HOMDProcessor.parseFasta(gid2Info,
                Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/saliva#2/data/HOMD_HUMAN.faa"),
                Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/saliva#2/data/formatted_HOMD_HUMAN.faa")
        );
    }
}