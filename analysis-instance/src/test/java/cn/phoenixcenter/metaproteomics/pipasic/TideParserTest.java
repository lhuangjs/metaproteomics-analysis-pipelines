package cn.phoenixcenter.metaproteomics.pipasic;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;

public class TideParserTest {

    @Test
    public void parsePsm() throws IOException {
        TideParser.parsePsm(
                Paths.get("/home/huangjs/archive/java/metaproteomics/practise/pipasic/example/percolator-species1/percolator.target.psms.txt"),
                Paths.get("/home/huangjs/archive/java/metaproteomics/practise/pipasic/example/percolator-species1/percolator.decoy.psms.txt"),
                Paths.get("/home/huangjs/archive/java/metaproteomics/practise/pipasic/example/percolator-species1/species1.txt"));

        TideParser.parsePsm(
                Paths.get("/home/huangjs/archive/java/metaproteomics/practise/pipasic/example/percolator-species2/percolator.target.psms.txt"),
                Paths.get("/home/huangjs/archive/java/metaproteomics/practise/pipasic/example/percolator-species2/percolator.decoy.psms.txt"),
                Paths.get("/home/huangjs/archive/java/metaproteomics/practise/pipasic/example/percolator-species2/species2.txt"));

    }
}