package cn.phoenixcenter.metaproteomics.preprocess;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class CaseComparatorTest {

    Map<String, Double> run1AbundanceMap = new HashMap<String, Double>() {
        {
            put("Agrobacterium tumefaciens", 6.45);
            put("Alteromonas macleodii", 0.46);
            put("Bacillus subtilis", 0.73);
            put("Chlamydomonas reinhardtii", 1.13);
            put("Chromobacterium violaceum", 1.13);
            put("Cupriavidus metallidurans", 21.17);
            put("Desulfovibrio vulgaris", 0.76);
            put("Enterobacteria phage ES18", 0.0);
            put("Escherichia coli", 10.62);
            put("Escherichia virus M13", 0.0);
            put("Escherichia virus MS2", 0.0);
            put("Nitrosomonas europaea", 0.01);
            put("Nitrosomonas ureae", 0.01);
            put("Nitrososphaera viennensis", 0.24);
            put("Nitrosospira multiformis", 0.18);
            put("Paraburkholderia xenovorans", 0.07);
            put("Paracoccus denitrificans", 0.49);
            put("Pseudomonas fluorescens", 9.59);
            put("Pseudomonas furukawaii", 1.9);
            put("Pseudomonas sp. ATCC 13867", 4.07);
            put("Rhizobium leguminosarum", 2.89);
            put("Roseobacter sp.", 1.08);
            put("Salmonella enterica", 23.09);
            put("Salmonella virus FelixO1", 0.0);
            put("Salmonella virus P22", 0.01);
            put("Staphylococcus aureus", 0.92);
            put("Stenotrophomonas maltophilia", 12.44);
            put("Thermus thermophilus", 0.56);
        }
    };

    /**
     * homo1
     *
     * @throws IOException
     */
    @Test
    public void calMSEIgnorePartTaxa1() throws IOException {
        Path[] replicaPaths = Stream.of("F002614", "F002615", "F002616", "F002617")
                .map(id -> Paths.get("/home/huangjs/Documents/metaprot/mc-pp-pipeline/P_Run4_5/homo1/distribution/", id))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.P);
        System.out.println(mse);

        // group
        replicaPaths = Stream.of("F002614", "F002615", "F002616", "F002617")
                .map(id -> Paths.get("/home/huangjs/Documents/metaprot/mc-pp-pipeline/P_Run4_5/homo1/distribution/", "pg_" + id))
                .toArray(Path[]::new);
        estimateAbundance = CaseComparator.calMean(replicaPaths);
        mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.P);
        System.out.println(mse);
    }

    @Test
    public void Run1U() {
        double runMse = CaseComparator.calMSEIgnorePartTaxa(run1AbundanceMap, CaseComparator.BiomassType.U);
        System.out.println("run1: " + runMse);
    }

    @Test
    public void calMSEIgnorePartTaxa2() throws IOException {
        Path[] replicaPaths = Stream.of("F002513", "F002514", "F002515", "F002516")
                .map(id -> Paths.get("/home/huangjs/Documents/metaprot/mc-mp-pipeline/U_Run1/original/distribution/", id))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println("mse: " + mse);
        System.out.println();
        Path[] replicaPgPaths = Stream.of("F002513", "F002514", "F002515", "F002516")
                .map(id -> Paths.get("/home/huangjs/Documents/metaprot/mc-mp-pipeline/U_Run1/original/distribution/", "pg_" + id))
                .toArray(Path[]::new);
        estimateAbundance = CaseComparator.calMean(replicaPgPaths);
        mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println("pg: " + mse);
    }

    @Test
    public void calMSEIgnorePartTaxa3() throws IOException {
        Path[] replicaPaths = Stream.of("F002651", "F002652", "F002653", "F002654")
                .map(id -> Paths.get("/home/huangjs/Documents/metaprot/mc-mp-pipeline/U_Run1/merge/distribution/", id))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println("mse: " + mse);
        System.out.println();
        Path[] replicaPgPaths = Stream.of("F002651", "F002652", "F002653", "F002654")
                .map(id -> Paths.get("/home/huangjs/Documents/metaprot/mc-mp-pipeline/U_Run1/merge/distribution/", "pg_" + id))
                .toArray(Path[]::new);
        estimateAbundance = CaseComparator.calMean(replicaPgPaths);
        mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println("pg: " + mse);
    }

    @Test
    public void calMSEIgnorePartTaxa4() throws IOException {
        Path[] replicaPaths = Stream.of("Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/biomass/fido/", id + "taxon.tsv"))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println("mse: " + mse);
        System.out.println();
//        Path[] replicaPgPaths = Stream.of("Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4")
//                .map(id -> Paths.get("/home/huangjs/Documents/metaprot/biomass/distribution", "pg_" + id))
//                .toArray(Path[]::new);
//        estimateAbundance = CaseComparator.calMean(replicaPgPaths);
//        mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
//        System.out.println("pg: " + mse);
    }

    @Test
    public void calMSEIgnorePartTaxa5() throws IOException {
        Path[] replicaPaths = Stream.of("F002651", "F002652", "F002653", "F002654")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/U_Run1/distribution", id))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println("mse: " + mse);
        System.out.println();
//        Path[] replicaPgPaths = Stream.of("Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4")
//                .map(id -> Paths.get("/home/huangjs/Documents/metaprot/biomass/distribution", "pg_" + id))
//                .toArray(Path[]::new);
//        estimateAbundance = CaseComparator.calMean(replicaPgPaths);
//        mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
//        System.out.println("pg: " + mse);
    }

    /**
     * @throws IOException
     */
    @Test
    public void calMSEIgnorePartTaxa6() throws IOException {
        Path[] replicaPaths = Stream.of("Run1_P1", "Run1_P2", "Run1_P3", "Run1_P4")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/biomass-P/fido", id + ".taxon.tsv"))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.P);
        System.out.println("mse: " + mse);
    }

    /**
     * Paper: P
     *
     * @throws IOException
     */
    @Test
    public void calMSEIgnorePartTaxa7() throws IOException {
        Path[] replicaPaths = Stream.of("Run1_P1", "Run1_P2", "Run1_P3", "Run1_P4")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/biomass-P/paper/", id + ".taxon.tsv"))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.P);
        System.out.println("mse: " + mse);
        System.out.println();
    }

    @Test
    public void calMSEIgnorePartTaxa8() throws IOException {
        Path[] replicaPaths = Stream.of("Run1_P1", "Run1_P2", "Run1_P3", "Run1_P4")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/biomass-P/fido_al", id + ".taxon.tsv"))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.P);
        System.out.println("mse: " + mse);
    }

    @Test
    public void calMSEIgnorePartTaxa9() throws IOException {
        Path[] replicaPaths = Stream.of("Run4_U1", "Run4_U2", "Run4_U3", "Run4_U4")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/biomass_Run4_U/fido_al", id + ".taxon.tsv"))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println("mse: " + mse);
    }

    @Test
    public void calMSEIgnorePartTaxa10() throws IOException {
        Path[] replicaPaths = Stream.of("Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/biomass/pg-psm", id + "_0.01_protein-group.taxon.tsv"))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println("mse: " + mse);
    }

    @Test
    public void calMSEIgnorePartTaxa11() throws IOException {
        Path[] replicaPaths = Stream.of("Run1_P1", "Run1_P2", "Run1_P3", "Run1_P4")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/biomass-P/pg-psm", id + "_0.01_protein-group.taxon.tsv"))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.P);
        System.out.println("mse: " + mse);
    }

    @Test
    public void calMSEIgnorePartTaxa12() throws IOException {
        Path[] replicaPaths = Stream.of("Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/biomass-homo/pg-psm", id + "_0.01_protein-group.taxon.tsv"))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        CaseComparator.printHomo(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println();
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println("mse: " + mse);
    }

    @Test
    public void calMSEIgnorePartTaxa13() throws IOException {
        Path[] replicaPaths = Stream.of("Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/biomass-homo/pg-psm-0.25", id + "_0.01_protein-group.taxon.tsv"))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        CaseComparator.printHomo(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println();
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println("mse: " + mse);
    }

    @Test
    public void calMSEIgnorePartTaxa14() throws IOException {
        Path[] replicaPaths = Stream.of("Run1_P1", "Run1_P2", "Run1_P3", "Run1_P4")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/biomass-P/pg-psm-0.25", id + "_0.01_protein-group.taxon.tsv"))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.P);
        System.out.println("mse: " + mse);
    }

    @Test
    public void calMSEIgnorePartTaxa15() throws IOException {
        Path[] replicaPaths = Stream.of("Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/biomass/pg-psm-0.25", id + "_0.01_protein-group.taxon.tsv"))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println("mse: " + mse);
    }

    @Test
    public void calMSEIgnorePartTaxa16() throws IOException {
        Path[] replicaPaths = Stream.of("Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/biomass-homo/pg-psm-uniq1-species", id + "_protein-group.taxon.tsv"))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        CaseComparator.printHomo(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println();
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println("mse: " + mse);
    }

    @Test
    public void calMSEIgnorePartTaxa16_1() throws IOException {
        Path[] replicaPaths = Stream.of("Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/biomass-homo/pg-psm-uniq1-species", id + "_protein-group.taxon.tsv"))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMeanBasedPerc(0.75, replicaPaths);
        CaseComparator.printHomo(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println();
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println("mse: " + mse);
    }

    @Test
    public void calMSEIgnorePartTaxa17() throws IOException {
        Path[] replicaPaths = Stream.of("Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/biomass/pg-psm-uniq2-species", id + "_protein-group.taxon.tsv"))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println("mse: " + mse);
    }

    @Test
    public void calMSEIgnorePartTaxa18() throws IOException {
        Path[] replicaPaths = Stream.of("Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4")
                .map(id -> Paths.get("/home/huangjs/Documents/map/test/biomass-homo/pg-psm-uniq2-species", id + "_protein-group.taxon.tsv"))
                .toArray(Path[]::new);
        Map<String, Double> estimateAbundance = CaseComparator.calMean(replicaPaths);
        CaseComparator.printHomo(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println();
        double mse = CaseComparator.calMSEIgnorePartTaxa(estimateAbundance, CaseComparator.BiomassType.U);
        System.out.println("mse: " + mse);
    }
}