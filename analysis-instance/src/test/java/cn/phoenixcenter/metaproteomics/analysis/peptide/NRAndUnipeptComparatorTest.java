package cn.phoenixcenter.metaproteomics.analysis.peptide;

import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import org.dom4j.DocumentException;
import org.junit.Test;

import java.io.IOException;

public class NRAndUnipeptComparatorTest {

    @Test
    public void combinePercolatorPeptide() throws IOException, DocumentException {
        NRAndUnipeptComparator.combinePercolatorPeptide(new String[]{
                        "/home/huangjs/Documents/map/test/biomass/percolator/Run1_U1.percolator.pout.xml",
                        "/home/huangjs/Documents/map/test/biomass/percolator/Run1_U2.percolator.pout.xml",
                        "/home/huangjs/Documents/map/test/biomass/percolator/Run1_U3.percolator.pout.xml",
                        "/home/huangjs/Documents/map/test/biomass/percolator/Run1_U4.percolator.pout.xml"
                },
                0.01,
                "/home/huangjs/Documents/map/result/peptide/biomass/peptide_Run1.tsv"
        );
    }

    @Test
    public void parseUnipeptResult() throws IOException {
        Taxon.Rank rank = Taxon.Rank.SPECIES;
        NRAndUnipeptComparator.parseUnipeptResult(
                true,
                4,
                0.75,
                "/home/huangjs/Documents/map/result/peptide/biomass/peptide_Run1.tsv",
                rank,
                2,
                "/home/huangjs/Documents/map/result/peptide/biomass/peptide_Run1_unipept.csv",
                "/home/huangjs/Documents/map/result/peptide/biomass/taxon_distribution_Unipept" + rank + ".tsv"
        );
    }

    @Test
    public void unipeptTaxonDistribution() throws IOException {
        int uniqPept = 3;
        int sampleThreshold = 2;
        NRAndUnipeptComparator.unipeptTaxonDistribution(
                true,
                sampleThreshold,
                "/home/huangjs/Documents/map/result/peptide/biomass/peptide_Run1.tsv",
                "/home/huangjs/Documents/map/result/peptide/biomass/peptide_Run1_unipept.csv",
                uniqPept,
                "/home/huangjs/Documents/map/result/peptide/biomass/taxon_distribution_Unipept_sample_threshold_" + sampleThreshold + "_uniq" + uniqPept + ".txt");
    }

    @Test
    public void nrTaxonDistribution() throws IOException {
        int uniqPept = 3;
        int sampleThreshold = 2;
        NRAndUnipeptComparator.nrTaxonDistribution(
                true,
                sampleThreshold,
                "/home/huangjs/Documents/map/result/peptide/biomass/peptide_Run1.tsv",
                "/home/huangjs/Documents/map/result/peptide/biomass/nr/lca.tsv",
                uniqPept,
                "/home/huangjs/Documents/map/result/peptide/biomass/taxon_distribution_NR_sample_threshold_" + sampleThreshold + "_uniq" + uniqPept + ".txt");
    }
}