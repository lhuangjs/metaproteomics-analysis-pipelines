package cn.phoenixcenter.metaproteomics.gut;

import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.StringJoiner;
import java.util.stream.IntStream;

@Log4j2
public class Preprocessing {
    @Test
    public void extractPept() throws IOException {
        BufferedReader br = Files.newBufferedReader(Paths.get(
                "/home/huangjs/archive/java/metaproteomics/raw_data/gut#3/txt/peptides.txt"
        ), StandardCharsets.UTF_8);
        BufferedWriter bw = Files.newBufferedWriter(Paths.get(
                "/home/huangjs/archive/java/metaproteomics/raw_data/gut#3/txt/experiment.tsv"
        ), StandardCharsets.UTF_8);
        // Sequence, Intensity HFD1_D0, ...
        int[] cols = IntStream.range(106, 139).toArray();
        cols[0] = 0;
        String line;
        // write content
        long count = 0;
        while ((line = br.readLine()) != null) {
            count++;
            StringJoiner joiner = new StringJoiner("\t");
            String[] tmp = line.split("\t");
            for (int i : cols) {
                joiner.add(tmp[i]);
            }
            bw.write(joiner.toString() + System.lineSeparator());
        }
        bw.close();
        br.close();
        log.info("total line count: {}", count);
    }
}
