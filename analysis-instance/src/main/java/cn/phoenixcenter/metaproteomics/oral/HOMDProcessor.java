package cn.phoenixcenter.metaproteomics.oral;

import cn.phoenixcenter.metaproteomics.utils.FileReaderUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;

@Log4j2
public class HOMDProcessor {

    /**
     * Link: http://www.homd.org/?name=Download&taxonly=1
     *
     * @param tableFile homd_taxonomy_table.txt(main)
     * @param levelFile homd_taxonomy_level.txt
     * @return
     * @throws IOException
     */
    public static Map<String, HOMDTaxonInfo> extractTid2Gid(String tableFile, String levelFile) throws IOException {
        FileReaderUtil reader = new FileReaderUtil(tableFile, "\t", true);
        final Map<String, HOMDTaxonInfo> gid2Info = new HashMap<>();

        /** parse homd_taxonomy_table.txt**/
        final int gidIdx1 = reader.getColNum("Genome_ID");
        final int tidIdx1 = reader.getColNum("NCBI_taxon_id");
        Consumer<String> tableConsumer = (String line) -> {
            String[] tmp = line.split("\t");
            if (tmp.length > gidIdx1) {
                String gids = tmp[gidIdx1];
                String tidStr = tmp[tidIdx1];
                if (gids.length() > 0 && tidStr.length() > 0) {
                    int tid = Integer.parseInt(tidStr);
                    Arrays.stream(gids.split("\\|"))
                            .forEach(gid -> {
                                if (gid2Info.containsKey(gid)) {
                                    throw new IllegalStateException("gid: " + gid + " match multiple taxa!");
                                }
                                gid2Info.put(gid, new HOMDTaxonInfo(tid, gid));
                            });

                }
            }
        };
        reader.read(tableConsumer);

        /** parse homd_taxonomy_level.txt **/
        reader = new FileReaderUtil(levelFile, "\t", true);
        final int gidIdx2 = reader.getColNum("HOMD Sequence ID");
        final int tidIdx2 = reader.getColNum("NCBI Taxonomy ID");
        Consumer<String> levelConsumer = (String line) -> {
            String[] tmp = line.split("\t");
            if (tmp.length > tidIdx2) {
                String gid = tmp[gidIdx2];
                String tidStr = tmp[tidIdx2];
                if (gid.startsWith("SEQ") && tidStr.length() > 0) {
                    int tid = Integer.parseInt(tidStr);
                    if (gid2Info.containsKey(gid) && gid2Info.get(gid).getTid() != tid) {
                        // some HOMD Sequence ID match low level in homd_taxonomy_level.txt file
                        log.info("HOMD Sequence Id {} match multiple taxa {} and {}", gid, gid2Info.get(gid).getTid(), tid);
                    } else {
                        gid2Info.put(gid, new HOMDTaxonInfo(tid, gid));
                    }
                }
            }
        };
        reader.read(levelConsumer);
        return gid2Info;
    }

    public static void parseFasta(Map<String, HOMDTaxonInfo> gid2Info, Path fastaPath, Path fFastaPath) throws IOException {
        BufferedReader br = Files.newBufferedReader(fastaPath, StandardCharsets.UTF_8);
        BufferedWriter bw = Files.newBufferedWriter(fFastaPath, StandardCharsets.UTF_8);
        ObjectMapper objectMapper = new ObjectMapper().setDefaultPropertyInclusion(JsonInclude.Include.NON_NULL);
        Set<String> missedGidSet = new HashSet<>();
        String line;
        HOMDTaxonInfo info = null;
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">")) {
                if (line.startsWith(">sp")) {
                    Map<String, Object> uniprotInfo = new HashMap<>();
                    int idx = line.indexOf(" ");
                    uniprotInfo.put("pid", line.substring(1, idx));
                    uniprotInfo.put("tid", 9606);
                    uniprotInfo.put("header", line.substring(idx + 1));
                    bw.write(line.substring(0, idx) + " " + objectMapper.writeValueAsString(uniprotInfo)
                            + System.lineSeparator());
                } else {
                    String gid = line.substring(1, line.indexOf("_"));
                    info = gid2Info.get(gid);
                    if (info == null) {
                        if (!missedGidSet.contains(gid)) {
                            missedGidSet.add(gid);
                        }
                        continue;
                    }
                    int idx = line.indexOf(" ");
                    info.pid = line.substring(1, idx);
                    info.header = line.substring(idx + 1);
                    bw.write(line.substring(0, idx) + " " + objectMapper.writeValueAsString(info)
                            + System.lineSeparator());
                }
            } else {
                if (info != null) {
                    bw.write(line + System.lineSeparator());
                }
            }
        }
        br.close();
        bw.close();
        if (missedGidSet.size() > 0) {
            System.err.println("there is no taxon id for " + missedGidSet.size() + " entries: ");
            for (String gid : missedGidSet) {
                System.err.println(gid);
            }
        }
    }
}

@Getter
class HOMDTaxonInfo {
    String pid;
    int tid; // taxon id
    String gid; // Genome ID
    String header;

    public HOMDTaxonInfo(int tid, String gid) {
        this.tid = tid;
        this.gid = gid;
    }
}