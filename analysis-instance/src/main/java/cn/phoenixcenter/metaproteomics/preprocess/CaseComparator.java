package cn.phoenixcenter.metaproteomics.preprocess;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The package is used to compare protein inference result
 */
public class CaseComparator {
    private static Map<String, Double> actualPAbundance = new HashMap<String, Double>() {
        {
            put("Agrobacterium tumefaciens", 4.259);
            put("Alteromonas macleodii", 4.259);
            put("Bacillus subtilis", 4.259);
            put("Chlamydomonas reinhardtii", 4.259);
            put("Chromobacterium violaceum", 4.259);
            put("Cupriavidus metallidurans", 4.259);
            put("Enterobacteria phage ES18", 0.411);
            put("Escherichia coli", 4.259);
            put("Escherichia virus M13", 0.411);
            put("Escherichia virus MS2", 0.411);
            put("Nitrososphaera viennensis", 4.259);
            put("Paracoccus denitrificans", 4.259);
            put("Pseudomonas fluorescens", 4.259);
            put("Pseudomonas furukawaii", 4.259);
            put("Pseudomonas sp. ATCC 13867", 4.259);
            put("Rhizobium leguminosarum", 8.518);
            put("Roseobacter sp.", 4.259);
            put("Salmonella enterica", 12.777);
            put("Salmonella virus FelixO1", 0.411);
            put("Salmonella virus P22", 0.411);
            put("Staphylococcus aureus", 8.518);
            put("Stenotrophomonas maltophilia", 4.259);
            put("Thermus thermophilus", 4.259);
        }
    };

    private static Map<String, Double> actualUAbundance = new HashMap<String, Double>() {
        {
            put("Agrobacterium tumefaciens", 5.647);
            put("Alteromonas macleodii", 0.954);
            put("Bacillus subtilis", 0.788);
            put("Chlamydomonas reinhardtii", 3.996);
            put("Chromobacterium violaceum", 1.259);
            put("Cupriavidus metallidurans", 15.519);
            put("Desulfovibrio vulgaris", 0.946);
            put("Enterobacteria phage ES18", 0.088);
            put("Escherichia coli", 5.788);
            put("Escherichia virus M13", 0.147);
            put("Escherichia virus MS2", 0.084);
            put("Nitrosomonas europaea", 0.082);
            put("Nitrosomonas ureae", 0.543);
            put("Nitrososphaera viennensis", 0.819);
            put("Nitrosospira multiformis", 0.209);
            put("Paraburkholderia xenovorans", 0.433);
            put("Paracoccus denitrificans", 0.922);
            put("Pseudomonas fluorescens", 6.696);
            put("Pseudomonas furukawaii", 1.165);
            put("Pseudomonas sp. ATCC 13867", 2.871);
            put("Rhizobium leguminosarum", 3.172);
            put("Roseobacter sp.", 1.596);
            put("Salmonella enterica", 33.773);
            put("Salmonella virus FelixO1", 0.088);
            put("Salmonella virus P22", 0.106);
            put("Staphylococcus aureus", 2.606);
            put("Stenotrophomonas maltophilia", 8.021);
            put("Thermus thermophilus", 1.68);
        }
    };

    public enum BiomassType {
        U,
        P;
    }

    public static Map<String, Double> calMean(Path... replicaPaths) throws IOException {
        final Map<String, List<Double>> tname2RepAbundance = new HashMap<>();
        for (Path path : replicaPaths) {
            Files.lines(path)
                    .skip(1)
                    .forEach(line -> {
                        String[] tmp = line.split("\t");
                        if (!tname2RepAbundance.containsKey(tmp[0])) {
                            tname2RepAbundance.put(tmp[0], new ArrayList<>(replicaPaths.length));
                        }
                        tname2RepAbundance.get(tmp[0]).add(Double.parseDouble(tmp[1]));
                    });
        }
        // cal mean
        Map<String, Double> tname2Abundance = tname2RepAbundance.entrySet().stream()
                .collect(Collectors.toMap(
                        e -> e.getKey(),
                        e -> e.getValue().stream().mapToDouble(d -> d.doubleValue()).sum() / e.getValue().size()
                ));
        return tname2Abundance;
    }

    public static Map<String, Double> calMeanBasedPerc(double percentage, Path... replicaPaths) throws IOException {
        final Map<String, double[]> tname2RepAbundance = new HashMap<>();
        for (int i = 0; i < replicaPaths.length; i++) {
            int k = i;
            Files.lines(replicaPaths[k])
                    .skip(1)
                    .forEach(line -> {
                        String[] tmp = line.split("\t");
                        if (!tname2RepAbundance.containsKey(tmp[0])) {
                            tname2RepAbundance.put(tmp[0], new double[replicaPaths.length]);
                        }
                        tname2RepAbundance.get(tmp[0])[k] = Double.parseDouble(tmp[1]);
                    });
        }
        Map<String, Double> tname2Abundance = tname2RepAbundance.entrySet().stream()
                .filter(e -> Arrays.stream(e.getValue()).filter(quant -> quant > 0.0).count()
                        / (double) replicaPaths.length
                        >= percentage
                )
                // cal mean
                .collect(Collectors.toMap(
                        e -> e.getKey(),
                        e -> Arrays.stream(e.getValue()).sum() / (double) replicaPaths.length
                ));
        return tname2Abundance;
    }

    /**
     * Calculate MSE value ignoring taxa that does not belong to actual taxa set
     *
     * @param estimateAbundance
     * @return
     */
    public static double calMSEIgnorePartTaxa(Map<String, Double> estimateAbundance, BiomassType type) {
        double mse = 0.0;
        Map<String, Double> actualAbundance = type == BiomassType.U ? actualUAbundance : actualPAbundance;
        System.out.println(String.join("\t",
                "Taxon name",
                "Actual value",
                "Estimate value",
                "Diff"
        ));
        for (Map.Entry<String, Double> e : actualAbundance.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))
                .collect(Collectors.toList())
        ) {
            double estVal = 0.0;
            if (estimateAbundance.containsKey(e.getKey())) {
                estVal = estimateAbundance.get(e.getKey());
                System.out.println(e.getKey()
                        + "\t" + e.getValue()
                        + "\t" + estVal
                        + "\t" + (e.getValue() - estVal));
            } else {
                System.err.println(e.getKey()
                        + "\t" + e.getValue()
                        + "\t" + estVal
                        + "\t" + (e.getValue() - estVal));
            }
            mse += Math.pow(e.getValue() - estVal, 2.0);
        }
        mse /= actualAbundance.size();
        return mse;
    }

    public static void calDiff(Map<String, Double> estimateAbundance, BiomassType type) {
        Map<String, Double> actualAbundance = type == BiomassType.U ? actualUAbundance : actualPAbundance;
        Map<String, Double> diff = new HashMap<>();
        for (Map.Entry<String, Double> e : actualAbundance.entrySet()) {
            diff.put(e.getKey(), e.getValue() - estimateAbundance.get(e.getKey()));
            System.out.println(e.getKey() + "\t" + estimateAbundance.get(e.getKey()) + "\t" + (e.getValue() - estimateAbundance.get(e.getKey())));
        }
    }

    public static void printHomo(Map<String, Double> estimateAbundance, BiomassType type) {
        double mse = 0.0;
        Map<String, Double> actualAbundance = type == BiomassType.U ? actualUAbundance : actualPAbundance;
        System.out.println(String.join("\t",
                "Taxon name",
                "Actual value",
                "Estimate value",
                "Diff(actual - estimate)"
        ));
        Set<String> tnameSet = new HashSet<>(actualAbundance.keySet());
        tnameSet.addAll(estimateAbundance.keySet());
        for (String tname : tnameSet.stream().sorted().collect(Collectors.toList())) {
            double actual = 0.0;
            double est = 0.0;
            if (estimateAbundance.containsKey(tname) && actualAbundance.containsKey(tname)) {
                actual = actualAbundance.get(tname);
                est = estimateAbundance.get(tname);
                System.out.println(tname
                        + "\t" + actual
                        + "\t" + est
                        + "\t" + (actual - est));
            } else if (actualAbundance.containsKey(tname)) {
                actual = actualAbundance.get(tname);
                System.out.println(tname
                        + "\t" + actual
                        + "\t" + est
                        + "\t" + (actual - est));
            } else {
                est = estimateAbundance.get(tname);
                System.out.println(tname
                        + "\t" + actual
                        + "\t" + est
                        + "\t" + (actual - est));
            }
            mse += Math.pow(actual - est, 2.0);
        }
        mse /= actualAbundance.size();
        System.out.println("mse = " + mse);
    }
}
