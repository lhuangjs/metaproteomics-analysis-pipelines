package cn.phoenixcenter.metaproteomics.preprocess;

import umich.ms.fileio.exceptions.FileParsingException;
import umich.ms.fileio.filetypes.pepxml.PepXmlParser;
import umich.ms.fileio.filetypes.pepxml.jaxb.standard.AltProteinDataType;
import umich.ms.fileio.filetypes.pepxml.jaxb.standard.SearchHit;
import umich.ms.fileio.filetypes.pepxml.jaxb.standard.SpectrumQuery;

import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PSMCounter {

    private final static Map<String, String> lable2OS = new HashMap<>();

    static {
        lable2OS.put("ATN", "Agrobacterium tumefaciens");
        lable2OS.put("Am2", "Alteromonas macleodii");
        lable2OS.put("BS", "Bacillus subtilis");
        lable2OS.put("CRH", "Chlamydomonas reinhardtii");
        lable2OS.put("CV", "Chromobacterium violaceum");
        lable2OS.put("Cup", "Cupriavidus metallidurans");
        lable2OS.put("DVH", "Desulfovibrio vulgaris");
        lable2OS.put("ES18", "Enterobacteria phage ES18");
        lable2OS.put("K12", "Escherichia coli");
        lable2OS.put("M13", "Escherichia virus M13");
        lable2OS.put("F2", "Escherichia virus MS2");
        lable2OS.put("Ne1", "Nitrosomonas europaea");
        lable2OS.put("Nu1", "Nitrosomonas ureae");
        lable2OS.put("NV", "Nitrososphaera viennensis");
        lable2OS.put("Nm1", "Nitrosospira multiformis");
        lable2OS.put("BXL", "Paraburkholderia xenovorans");
        lable2OS.put("PaD", "Paracoccus denitrificans");
        lable2OS.put("Pfl", "Pseudomonas fluorescens");
        lable2OS.put("KF7", "Pseudomonas furukawaii");
        lable2OS.put("PD", "Pseudomonas sp. ATCC 13867");
        lable2OS.put("841", "Rhizobium leguminosarum");
        lable2OS.put("VF", "Rhizobium leguminosarum");
        lable2OS.put("AK199", "Roseobacter sp.");
        lable2OS.put("LT2", "Salmonella enterica");
        lable2OS.put("F0", "Salmonella virus FelixO1");
        lable2OS.put("P22", "Salmonella virus P22");
        lable2OS.put("137", "Staphylococcus aureus");
        lable2OS.put("259", "Staphylococcus aureus");
        lable2OS.put("SMS", "Stenotrophomonas maltophilia");
        lable2OS.put("HB2", "Thermus Thermophilus");
    }

    public static void count(String... pepxmlFiles) throws FileParsingException {
        /** parse pepxml file to get MPPeptide set **/
        Map<String, Integer> label2Count = new HashMap<>();
        for (String pepxmlFile : pepxmlFiles) {
            List<SpectrumQuery> specQueryList = PepXmlParser.parse(Paths.get(pepxmlFile))
                    .getMsmsRunSummary()
                    .get(0)
                    .getSpectrumQuery();
            for (SpectrumQuery specQuery : specQueryList) {
                SearchHit searchHit = specQuery.getSearchResult().get(0).getSearchHit().get(0);
                // peptide
                String sequence = searchHit.getPeptide();
//            // probability
//            double prob = ((PeptideprophetResult) (searchHit.getAnalysisResult().get(0).getAny().get(0)))
//                    .getProbability();
                // proteins
                List<String> pidList = searchHit.getAlternativeProtein().stream()
                        .map(AltProteinDataType::getProtein)
                        .collect(Collectors.toList());
                pidList.add(searchHit.getProtein());
                for (String pid : pidList) {
                    String label = pid.substring(0, pid.indexOf("_"));
                    if (label2Count.containsKey(label)) {
                        label2Count.put(label, label2Count.get(label) + 1);
                    } else {
                        label2Count.put(label, 1);
                    }
                }
            }
        }

        label2Count.put("137;259", label2Count.get("137") + label2Count.get("259"));
        label2Count.put("841;VF", label2Count.get("841") + label2Count.get("VF"));
        label2Count.remove("137");
        label2Count.remove("259");
        label2Count.remove("841");
        label2Count.remove("VF");
        label2Count.remove("CRAP");

        List<String> labelList = Stream.of(
                "137;259", "841;VF", "AK199", "Am2", "ATN", "BS", "BXL", "CRH", "Cup", "CV", "DVH", "ES18", "F0", "F2", "HB2", "K12", "KF7", "LT2", "M13", "Ne1", "Nm1", "Nu1", "NV", "P22", "PaD", "PD", "Pfl", "SMS"
        ).collect(Collectors.toList());
        for (String label : labelList) {
            if (label2Count.containsKey(label)) {
                double avg = (double) (label2Count.get(label)) / pepxmlFiles.length;
                System.out.println(label + "\t" + avg);
            } else {
                System.out.println(label + "\t0");
            }
        }
    }

    public static void main(String[] args) {
        try {
//            count(
////                    "/home/huangjs/Documents/Run1_U1_2000ng.pep.xml"
////                    "/home/huangjs/Documents/Run1_U2_2000ng.pep.xml"
////                    "/home/huangjs/Documents/Run1_U3_2000ng.pep.xml"
////                    "/home/huangjs/Documents/Run1_U4_2000ng.pep.xml"
//            );
            count("/home/huangjs/Documents/metaprot/mc-pp-pipeline/0.01-F002499.xinteract.pep.xml");
        } catch (FileParsingException e) {
            e.printStackTrace();
        }
    }
}
