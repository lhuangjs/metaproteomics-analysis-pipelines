package cn.phoenixcenter.metaproteomics.preprocess;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.nr.NRSearcher;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import cn.phoenixcenter.metaproteomics.taxonomy.TaxonSearcher;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The package is used to extract homologous sequence
 */
public class HomologousSeqGenerator {

    TaxonSearcher taxonSearcher = GlobalConfig.getObj(TaxonSearcher.class);
    NRSearcher nrSearcher = GlobalConfig.getObj(NRSearcher.class);

    @Test
    public void generate() throws IOException {
        int tid = 136843;
        Path fastaPath = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/fasta/", "biomass_with_" + tid + ".fasta");
        Set<Taxon> relatedTaxonSet = taxonSearcher.getRelatedTaxa(taxonSearcher.getTaxonById(tid));
        // remove original tid
        Set<Integer> tidSet = Stream.of(294, 1149133, 1294143).collect(Collectors.toSet());
        relatedTaxonSet = relatedTaxonSet.stream()
                .filter(t -> !tidSet.contains(t.getId()))
                .collect(Collectors.toSet());
        System.out.println(relatedTaxonSet.stream().collect(Collectors.groupingBy(Taxon::getRank, Collectors.counting())));
        // download sequence
//        relatedTaxonSet = Stream.of(1232139)
//                .map(id -> taxonSearcher.getTaxonById(id))
//                .collect(Collectors.toSet());
        Map<Taxon, Long> taxon2SeqCount = new HashMap<>(relatedTaxonSet.size());
        BiConsumer<Taxon, Long> countConsumer = (Taxon taxon, Long count) -> {
            if (count > 0L) {
                taxon2SeqCount.put(taxon, count);
            }
        };
        nrSearcher.getProteinsByTaxa(relatedTaxonSet, fastaPath, countConsumer);
        for (Map.Entry<Taxon, Long> e : taxon2SeqCount.entrySet()) {
            Taxon taxon = e.getKey();
            long count = e.getValue();
            System.out.println(taxon.getName() + "[" + taxon.getId() + ", " + taxon.getRank() + "]: " + count);
        }
    }

    @Test
    public void generate2() throws IOException {
        List<String> tnameList = Files.readAllLines(
                Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/fasta/Pseudomonas.txt"))
                .parallelStream()
                .map(line -> line.trim())
                .filter(line -> !line.startsWith("#"))
                .collect(Collectors.toList());
        Path fastaPath = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/fasta/", "biomass_with_Pseudomonas.fasta");
        Set<Taxon> relatedTaxonSet = tnameList.stream()
                .map(tname -> taxonSearcher.getTaxonByName(tname))
                .collect(Collectors.toSet());
        // remove original tid
        Set<Integer> tidSet = Stream.of(294, 1149133, 1294143).collect(Collectors.toSet());
        relatedTaxonSet = relatedTaxonSet.stream()
                .filter(t -> !tidSet.contains(t.getId()))
                .collect(Collectors.toSet());
        // download sequence
        Map<Taxon, Long> taxon2SeqCount = new HashMap<>(relatedTaxonSet.size());
        BiConsumer<Taxon, Long> countConsumer = (Taxon taxon, Long count) -> {
            if (count > 0L) {
                taxon2SeqCount.put(taxon, count);
            }
        };
        nrSearcher.getProteinsByTaxa(relatedTaxonSet, fastaPath, countConsumer);
        for (Map.Entry<Taxon, Long> e : taxon2SeqCount.entrySet()) {
            Taxon taxon = e.getKey();
            long count = e.getValue();
            System.out.println(taxon.getName() + "[" + taxon.getId() + ", " + taxon.getRank() + "]: " + count);
        }
    }
}
