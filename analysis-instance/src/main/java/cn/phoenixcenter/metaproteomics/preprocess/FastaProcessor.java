package cn.phoenixcenter.metaproteomics.preprocess;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.nr.NRSearcher;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import cn.phoenixcenter.metaproteomics.taxonomy.TaxonSearcher;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class FastaProcessor {

    @Test
    public void formatFasta() {
        String fastaFile = "/home/huangjs/Documents/metaprot/biomass/Mock_Comm_RefDB_V3.fasta";
        String label2TidFile = "/home/huangjs/Documents/metaprot/biomass/label2tid.tsv";
        String formattedFastaFile = "/home/huangjs/Documents/metaprot/biomass/biomass.fasta";
        TaxonSearcher taxonSearcher = GlobalConfig.getObj(TaxonSearcher.class);
        Pattern pidPattern = Pattern.compile(">(\\S+)");

        try (BufferedReader br = Files.newBufferedReader(Paths.get(fastaFile), StandardCharsets.UTF_8);
             BufferedWriter bw = Files.newBufferedWriter(Paths.get(formattedFastaFile), StandardCharsets.UTF_8)) {
            // label => taxon id
            Map<String, Taxon> label2Tid = Files.readAllLines(Paths.get(label2TidFile))
                    .stream()
                    .map(line -> line.split("\t"))
                    .collect(Collectors.toMap(
                            tmp -> tmp[0],
                            tmp -> taxonSearcher.getTaxonById(Integer.parseInt(tmp[1]))
                    ));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.length() == 0) {
                    continue;
                } else if (line.startsWith(">")) {
                    if (line.startsWith(">CRAP")) {
                        bw.write(line + System.lineSeparator());
                    } else {
                        // label
                        String label = line.substring(1, line.indexOf("_"));
                        if (!label2Tid.containsKey(label)) {
                            System.err.println(line);
                            System.exit(255);
                        }
                        // protein id
                        Matcher m = pidPattern.matcher(line);
                        if (label.equals("Pfl")) {
                            System.out.println();
                        }
                        if (m.find()) {
                            //>AGL83738.1 {"id":"AGL83738.1","gi":500241580,"taxon":{"id":1124983,"name":"Pseudomonas protegens CHA0","rank":"no rank"}}
                            String pid = m.group(1);
                            XContentBuilder builder = XContentFactory.jsonBuilder();
                            builder.startObject();
                            builder.field("id", pid);
                            Taxon taxon = label2Tid.get(label);
                            builder.startObject("taxon");
                            builder.field("id", taxon.getId());
                            builder.field("name", taxon.getName());
                            builder.field("rank", taxon.getRank());
                            builder.endObject();
                            builder.endObject();
                            bw.write(">" + pid
                                    + " "
                                    + Strings.toString(builder)
                                    + System.lineSeparator());
                        } else {
                            System.err.println(line);
                            System.exit(255);
                        }
                    }
                } else {
                    bw.write(line + System.lineSeparator());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(255);
        }
    }

    /**
     * Generate new library with homologous sequences.
     *
     * @throws IOException
     */
    @Test
    public void genNewLib() throws IOException {
        Path originalLibPath = Paths.get("/home/huangjs/Documents/metaprot/biomass/biomass.fasta");
        TaxonSearcher taxonSearcher = GlobalConfig.getObj(TaxonSearcher.class);
        NRSearcher nrSearcher = GlobalConfig.getObj(NRSearcher.class);
        // write homologous sequences and original sequences together
//        int[] homologousTids = new int[]{358, 119219, 294, 40324};
        int[] homologousTids = new int[]{358, 119219, 562, 294, 28901, 40324};
        Path libWithHomologousSeqPath = Paths.get("/home/huangjs/Documents/metaprot/biomass/biomass-with-homologous_tid="
                + Arrays.stream(homologousTids).mapToObj(String::valueOf).collect(Collectors.joining("+"))
                + ".fasta");
        // copy original sequences
        Files.copy(originalLibPath, libWithHomologousSeqPath, StandardCopyOption.REPLACE_EXISTING);
        // get related taxa and remove redundancy
        Set<Taxon> homologousTaxonSet = Arrays.stream(homologousTids)
                .mapToObj(tid -> taxonSearcher.getTaxonById(tid))
                .map(taxon -> new AbstractMap.SimpleEntry<>(taxon, taxonSearcher.getRelatedTaxa(taxon)))
                .filter(e -> e.getValue() != null)
                .peek(e -> System.out.println(e.getKey().getId() + "=>" + e.getValue().stream()
                        .map(t -> String.valueOf(t.getId()))
                        .collect(Collectors.joining(";")))
                )
                .flatMap(e -> e.getValue().stream())
                .collect(Collectors.toSet());
        for (Taxon taxon : homologousTaxonSet) {
            Path tmpPath = Files.createTempFile(taxon.getId() + "", ".fasta");
            nrSearcher.getProteinsByTaxon(taxon, tmpPath);
            OutputStream out = Files.newOutputStream(libWithHomologousSeqPath, StandardOpenOption.APPEND);
            byte[] buf = new byte[1024];
            InputStream in = Files.newInputStream(tmpPath);
            int b = 0;
            while ((b = in.read(buf)) != -1) {
                out.write(buf, 0, b);
            }
            in.close();
            Files.delete(tmpPath);
            out.close();
        }
    }
}
