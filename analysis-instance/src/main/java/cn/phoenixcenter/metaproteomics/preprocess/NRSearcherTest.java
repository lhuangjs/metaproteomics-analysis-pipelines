package cn.phoenixcenter.metaproteomics.preprocess;

import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NRSearcherTest {

    List<String> rankList = Stream.of(Taxon.Rank.values())
            .map(Taxon.Rank::rankToString)
            .collect(Collectors.toList());
    @Test
    public void findDiff() throws IOException {
        Map<String, Taxon.Rank> unipept = Files.readAllLines(Paths.get("/home/huangjs/Documents/metaprot/nr-searcher/unipept_result.txt"))
                .stream()
                .skip(1)
                .parallel()
                .map(line -> {
                    String[]  tmp = line.split("\t");
                    String rank = tmp[2].toLowerCase();
                    if(rankList.contains(rank)){
                        return new AbstractMap.SimpleEntry<>(tmp[0], Taxon.Rank.stringToRank(rank));
                    }else{
                        return null;
                    }
                })
                .filter(e -> e != null)
                .collect(Collectors.toMap(
                        e -> e.getKey(),
                        e -> e.getValue()
                ));

        Map<String, Taxon.Rank> myres = Files.readAllLines(Paths.get("/home/huangjs/Documents/metaprot/nr-searcher/myres.txt"))
                .stream()
                .skip(2)
                .parallel()
                .map(line -> {
                    String[]  tmp = line.split("\t");
                    String rank = tmp[3].toLowerCase();
                    if(rankList.contains(rank)){
                        return new AbstractMap.SimpleEntry<>(tmp[0], Taxon.Rank.stringToRank(rank));
                    }else{
                        return null;
                    }
                })
                .filter(e -> e != null)
                .collect(Collectors.toMap(
                        e -> e.getKey(),
                        e -> e.getValue()
                ));

        unipept.entrySet().stream()
                .filter(e -> {
                    if(myres.containsKey(e.getKey())){
                        return myres.get(e.getKey()).index() - e.getValue().index() > 0;
                    }else{
                        return false;
                    }
                })
                .forEach(e -> System.out.println(e.getKey() + "\t" + e.getValue() + "\t" + myres.get(e.getKey())));

    }
}
