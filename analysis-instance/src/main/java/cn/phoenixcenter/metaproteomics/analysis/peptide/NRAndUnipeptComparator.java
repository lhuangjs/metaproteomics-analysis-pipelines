package cn.phoenixcenter.metaproteomics.analysis.peptide;

import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import lombok.extern.log4j.Log4j2;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
public class NRAndUnipeptComparator {

    private static final SAXReader reader = new SAXReader();

    public static void combinePercolatorPeptide(String[] percolatorXMLFiles,
                                                double peptFDRThreshold,
                                                String outputTSVFile) throws DocumentException, IOException {
        // key: peptide; value: PSM count array in copies
        Map<String, int[]> seq2Line = new HashMap<>(50_000);
        for (int i = 0; i < percolatorXMLFiles.length; i++) {
            String percolatorXMLFile = percolatorXMLFiles[i];
            Document document = reader.read(percolatorXMLFile);
            List<Element> peptElemList = document.getRootElement().element("peptides").elements();
            for (Element peptElem : peptElemList) {
                double qval = Double.parseDouble(peptElem.element("q_value").getStringValue());
                if (qval < peptFDRThreshold) {
                    boolean isTarget = !Boolean.valueOf(peptElem.attributeValue("decoy"));
                    if (isTarget) {
                        String peptSeq = peptElem.attributeValue("peptide_id").replaceAll("\\[.*?\\]", "");
                        int psmCount = peptElem.element("psm_ids").elements("psm_id").size();
                        seq2Line.putIfAbsent(peptSeq, new int[percolatorXMLFiles.length]);
                        seq2Line.get(peptSeq)[i] = psmCount;
                    }
                }
            }
        }
        List<String> lines = seq2Line.entrySet().parallelStream()
                .map(e -> e.getKey() + "\t" + Arrays.stream(e.getValue())
                        .mapToObj(String::valueOf).collect(Collectors.joining("\t")))
                .collect(Collectors.toList());
        lines.add(0, String.join("\t", "Peptide", "U1", "U2", "U3", "U4"));
        Files.write(Paths.get(outputTSVFile), lines, StandardCharsets.UTF_8);
    }

    public static void parseUnipeptResult(boolean equateIL,
                                          int sampleSize,
                                          double threshold,
                                          String rawPeptideFile,
                                          Taxon.Rank rank,
                                          int uniqPeptThreshold,
                                          String unipeptFile,
                                          String resultFile) throws IOException {
        // keu: peptide, value: PSM count
        Map<String, int[]> pept2SampleQuant = new HashMap<>(30_000);
        Files.lines(Paths.get(rawPeptideFile))
                .skip(1)
                .forEach(line -> {
                    String[] tmp = line.split("\t");
                    if (equateIL) tmp[0] = tmp[0].replace("I", "L");
                    if (pept2SampleQuant.containsKey(tmp[0])) {
                        int[] quantVals = pept2SampleQuant.get(tmp[0]);
                        for (int i = 0; i < quantVals.length; i++) {
                            quantVals[i] += Integer.parseInt(tmp[i + 1]);
                        }
                        pept2SampleQuant.put(tmp[0], quantVals);
                    } else {
                        pept2SampleQuant.put(tmp[0], Arrays.stream(tmp).skip(1).mapToInt(Integer::parseInt).toArray());
                    }
                });
        log.info("There are {} peptides", pept2SampleQuant.size());
        // filter
        Map<String, Double> pept2Quant = pept2SampleQuant.entrySet().parallelStream()
                .filter(e -> Arrays.stream(e.getValue()).filter(quant -> quant > 0).count() / (double) sampleSize >= threshold)
                .collect(Collectors.toMap(
                        e -> e.getKey(),
                        e -> Arrays.stream(e.getValue()).sum() / (double) sampleSize
                ));
        log.info("There are {} peptides after filtering by threshold = {}", pept2Quant.size(), threshold);

        // read Unipept LCA result
        int[] idx = new int[]{2, 6, 9, 13, 18, 22, 26};
        Map<String, Double> taxon2PeptQuants = Files.lines(Paths.get(unipeptFile))
                .skip(1)
                .map(line -> {
                    // [peptide, taxon on specific rank]
                    String[] tmp = line.split(",", 28);
                    if (equateIL) tmp[0] = tmp[0].replace("I", "L");
                    if (!pept2Quant.containsKey(tmp[0])) {
                        // some peptides are removed because they only are in few samples
                        return null;
                    }
                    return new String[]{tmp[0], tmp[idx[rank.index()]]};
                })
                .filter(arr -> arr != null && arr[1].length() > 0)
                // taxon => [pept1 quant val, ....]
                .collect(Collectors.groupingBy((String[] arr) -> arr[1], Collectors.mapping(
                        (String[] arr) -> pept2Quant.get(arr[0]), // taxon => []
                        Collectors.toList()
                )))
                .entrySet()
                .stream()
                .filter(e -> e.getValue().size() >= uniqPeptThreshold)
                .collect(Collectors.toMap(
                        e -> e.getKey(),
                        e -> e.getValue().stream().mapToDouble(Double::doubleValue).sum()
                ));
        // write
        double total = taxon2PeptQuants.values().stream().mapToDouble(Double::doubleValue).sum();
        List<String> lines = taxon2PeptQuants.entrySet().stream()
                .sorted(Comparator.comparing(e -> e.getKey()))
                .map(e -> e.getKey() + "\t" + e.getValue() / total * 100)
                .collect(Collectors.toList());
        lines.add(0, "taxon\tquantitative values(%)");
        Files.write(Paths.get(resultFile), lines);
    }

    public static void unipeptTaxonDistribution(boolean equateIL,
                                                int sampleThreshold,
                                                String rawPeptideFile,
                                                String unipeptFile,
                                                int uniqPeptThreshold,
                                                String resultFile) throws IOException {
        // keu: peptide, value: PSM count
        Map<String, int[]> pept2SampleQuant = new HashMap<>(30_000);
        Files.lines(Paths.get(rawPeptideFile))
                .skip(1)
                .forEach(line -> {
                    String[] tmp = line.split("\t");
                    if (equateIL) tmp[0] = tmp[0].replace("I", "L");
                    if (pept2SampleQuant.containsKey(tmp[0])) {
                        int[] quantVals = pept2SampleQuant.get(tmp[0]);
                        for (int i = 0; i < quantVals.length; i++) {
                            quantVals[i] += Integer.parseInt(tmp[i + 1]);
                        }
                        pept2SampleQuant.put(tmp[0], quantVals);
                    } else {
                        pept2SampleQuant.put(tmp[0], Arrays.stream(tmp).skip(1).mapToInt(Integer::parseInt).toArray());
                    }
                });
        log.info("There are {} peptides", pept2SampleQuant.size());
        // filter
        Set<String> filteredPeptideSet = pept2SampleQuant.entrySet().parallelStream()
                .filter(e -> Arrays.stream(e.getValue()).filter(quant -> quant > 0).count() >= sampleThreshold)
                .map(e -> e.getKey())
                .collect(Collectors.toSet());
        log.info("There are {} peptides after filtering by threshold = {}", filteredPeptideSet.size(), sampleThreshold);

        BufferedWriter bw = Files.newBufferedWriter(Paths.get(resultFile));
        // read Unipept LCA result
        int[] idx = new int[]{2, 6, 9, 13, 18, 22, 26};
        Taxon.Rank ranks[] = Taxon.Rank.values();
        List<String[]> taxonList = Files.lines(Paths.get(unipeptFile))
                .skip(1)
                .map(line -> {
                    // [peptide, taxon on specific rank]
                    String[] tmp = line.split(",", 28);
                    if (equateIL) tmp[0] = tmp[0].replace("I", "L");
                    if (!filteredPeptideSet.contains(tmp[0])) {
                        return null;
                    }
                    String[] lineage = new String[idx.length];
                    for (int i = 0; i < idx.length; i++) {
                        lineage[i] = tmp[idx[i]];
                    }
                    return lineage;
                })
                .filter(lineage -> lineage != null)
                .collect(Collectors.toList());
        for (int i = 0; i < ranks.length; i++) {
            bw.write("Taxon on " + ranks[i].rankToString() + System.lineSeparator());
            int j = i;
            Set<String> taxonSet = taxonList.parallelStream()
                    .map(lineage -> lineage[j])
                    .filter(taxon -> taxon.length() > 0)
                    // taxon => unique peptide count
                    .collect(Collectors.groupingBy(taxon -> taxon, Collectors.counting()))
                    .entrySet()
                    .stream()
                    // filter by unique peptide count
                    .filter(e -> e.getValue() >= uniqPeptThreshold)
                    .map(e -> e.getKey())
                    .collect(Collectors.toSet());
            for (String t : taxonSet.stream().sorted().collect(Collectors.toList())) {
                bw.write(t + System.lineSeparator());
            }
            bw.write(System.lineSeparator());
        }
        bw.close();
    }

    /**
     * 计算物种在各个rank上的分布。原始肽段需要过滤
     *
     * @param equateIL
     * @param sampleSize
     * @param sampleThreshold
     * @param rawPeptideFile
     * @param nrLCAFile
     * @param uniqPeptThreshold
     * @param resultFile
     * @throws IOException
     */
    public static void nrTaxonDistribution(boolean equateIL,
                                           int sampleThreshold,
                                           String rawPeptideFile,
                                           String nrLCAFile,
                                           int uniqPeptThreshold,
                                           String resultFile) throws IOException {
        /**读取肽段和定量数据，合并相同肽段，仅仅保留样本中存在定量值多余threshold的肽段**/
        // keu: peptide, value: PSM count
        Map<String, int[]> pept2SampleQuant = new HashMap<>(30_000);
        Files.lines(Paths.get(rawPeptideFile))
                .skip(1)
                .forEach(line -> {
                    String[] tmp = line.split("\t");
                    if (equateIL) tmp[0] = tmp[0].replace("I", "L");
                    if (pept2SampleQuant.containsKey(tmp[0])) {
                        int[] quantVals = pept2SampleQuant.get(tmp[0]);
                        for (int i = 0; i < quantVals.length; i++) {
                            quantVals[i] += Integer.parseInt(tmp[i + 1]);
                        }
                        pept2SampleQuant.put(tmp[0], quantVals);
                    } else {
                        pept2SampleQuant.put(tmp[0], Arrays.stream(tmp).skip(1).mapToInt(Integer::parseInt).toArray());
                    }
                });
        log.info("There are {} peptides", pept2SampleQuant.size());
        // filter
        Set<String> filteredPeptideSet = pept2SampleQuant.entrySet().parallelStream()
                .filter(e -> Arrays.stream(e.getValue()).filter(quant -> quant > 0).count() >= sampleThreshold)
                .map(e -> e.getKey())
                .collect(Collectors.toSet());
        log.info("There are {} peptides after filtering by threshold = {}", filteredPeptideSet.size(), sampleThreshold);

        /****/
        BufferedWriter bw = Files.newBufferedWriter(Paths.get(resultFile));
        // read Unipept LCA result
        int[] idx = new int[]{4, 5, 6, 7, 8, 9, 10};
        Taxon.Rank ranks[] = Taxon.Rank.values();
        List<String[]> taxonList = Files.lines(Paths.get(nrLCAFile))
                .filter(line -> !(line.startsWith("#") || line.startsWith("Peptide")))
                .map(line -> {
                    // [peptide, taxon on specific rank]
                    String[] tmp = line.split("\t", 12);
                    if (equateIL) tmp[0] = tmp[0].replace("I", "L");
                    if (!filteredPeptideSet.contains(tmp[0])) {
                        return null;
                    }
                    String[] lineage = new String[idx.length];
                    for (int i = 0; i < idx.length; i++) {
                        lineage[i] = tmp[idx[i]];
                    }
                    return lineage;
                })
                .filter(lineage -> lineage != null)
                .collect(Collectors.toList());
        for (int i = 0; i < ranks.length; i++) {
            bw.write("Taxon on " + ranks[i].rankToString() + System.lineSeparator());
            int j = i;
            Set<String> taxonSet = taxonList.parallelStream()
                    .map(lineage -> lineage[j])
                    .filter(taxon -> taxon.length() > 0)
                    // taxon => unique peptide count
                    .collect(Collectors.groupingBy(taxon -> taxon, Collectors.counting()))
                    .entrySet()
                    .stream()
                    // filter by unique peptide count
                    .filter(e -> e.getValue() >= uniqPeptThreshold)
                    .map(e -> e.getKey())
                    .collect(Collectors.toSet());
            for (String t : taxonSet.stream().sorted().collect(Collectors.toList())) {
                bw.write(t + System.lineSeparator());
            }
            bw.write(System.lineSeparator());
        }
        bw.close();
    }
}
