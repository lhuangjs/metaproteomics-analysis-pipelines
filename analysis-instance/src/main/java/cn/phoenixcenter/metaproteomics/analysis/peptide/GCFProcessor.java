package cn.phoenixcenter.metaproteomics.analysis.peptide;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.StringJoiner;

public class GCFProcessor {
    @Test
    public void extractPeptide() throws IOException {
        Path peptPath = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/nr/sample_data/peptide.tsv");
        Path inputPath = Paths.get("/home/huangjs/metadata/taxonomy/user2/experiment.tsv");
        BufferedReader br = Files.newBufferedReader(peptPath);
        BufferedWriter bw = Files.newBufferedWriter(inputPath, StandardCharsets.UTF_8);
        String line = br.readLine();
        String[] tmp = line.split("\t");
        if (tmp.length != 109) {
            System.out.println(line);
            throw new IllegalArgumentException();
        }
        StringJoiner joiner = new StringJoiner("\t");
        joiner.add("peptide");
        for (int i = 5; i < tmp.length; i++) {
            joiner.add(tmp[i]);
        }
        bw.write(joiner.toString() + System.lineSeparator());

        while ((line = br.readLine()) != null) {
            if (line.length() > 0) {
                tmp = line.split("\t");
                if (tmp[3].indexOf("HUMAN") == -1) {
                    String[] data = new String[105];
                    // _NWSQC[Carbamidomethyl]VELAR_.2
                    data[0] = tmp[4].replaceAll("(_)|(\\[\\w+\\])|(\\.\\d+)", "");
                    for (int i = 5; i < tmp.length; i++) {
                        switch (tmp[i]) {
                            case "Filtered":
                            case "NaN":
                                data[i - 4] = "0";
                                break;
                            default:
                                data[i - 4] = tmp[i];
                        }
                    }
                    bw.write(String.join("\t", data) + System.lineSeparator());
                }
            }
        }
        bw.close();
        br.close();
    }
}
