package cn.phoenixcenter.metaproteomics.pipasic;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class TideParser {
    static double fdr = 0.001;
    static double score = 1.0;

    public static void parsePsm(Path targetPath, Path decoyPath, Path rsPath) throws IOException {
        BufferedWriter bw = Files.newBufferedWriter(rsPath);
        bw.write("\t Spectrum\t Peptid\t Score\t isDecoy\t FDR\t matchedProtein \n");
        bw.write("\t --------\t ------\t -----\t -------\t ---\t -------------- \n");
        parsePsm(targetPath, true, bw);
        fdr = 0.004;
        score = 0.8;
        parsePsm(decoyPath, false, bw);
        bw.close();
    }

    private static void parsePsm(Path psmPath, boolean isTarget, BufferedWriter bw) throws IOException {
        BufferedReader br = Files.newBufferedReader(psmPath);
        String line;
        br.readLine();
        while ((line = br.readLine()) != null) {
            String[] tmp = line.split("\t");
            if (fdr < 0.01) {
                bw.write("\t" + String.join("\t", tmp[1], tmp[10], String.valueOf(score),
                        String.valueOf(isTarget), tmp[7], tmp[11]) + System.lineSeparator());
                fdr += 0.001;
                score += 0.1;
            } else {
                break;
            }
        }
        br.close();
    }
}
