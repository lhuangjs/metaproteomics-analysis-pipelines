package cn.phoenixcenter.metaproteomics.pipeline.base;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TreeNode<T> {

    private T data;

    private List<TreeNode<T>> children;

    private boolean placeholder = false;

    private boolean nullNode = false;
}
