package cn.phoenixcenter.metaproteomics.pipeline.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 *
 */
@NoArgsConstructor
@Log4j2
public class PeptideMapper {

    private static final String delimiter = ";";

    // encoded char for delimiter
    private static final String encodedChar = "%";

    // TODO use a public thread pool?
    private static ExecutorService executorService = Executors.newFixedThreadPool(5);

    // TODO make it as global configuration
    private static int maxQueueLen = 10;

    private static List<Future> futureList = new ArrayList<>();


    /**
     * Read peptides to proteins from json file
     *
     * @param pept2ProtsJson
     * @return
     * @throws IOException
     */
    public Map<String, Set<String>> mapPept2Prots(String pept2ProtsJson)
            throws IOException {
        return new ObjectMapper().readValue(new File(pept2ProtsJson), new TypeReference<Map<String, Set<String>>>() {
        });
    }

    /**
     * Search for proteins containing specified peptides and write into json file
     *
     * @param fastaFile
     * @param pidPattern
     * @param jsonOutput
     * @throws IOException
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static void mapPept2Prots(Set<String> peptideSet,
                                     String fastaFile,
                                     Pattern pidPattern,
                                     DecoySeqFactory.Type decoyType,
                                     String decoyPrefix,
                                     String jsonOutput) throws InterruptedException, ExecutionException, IOException {
        new ObjectMapper().writeValue(new File(jsonOutput),
                mapPept2Prots(peptideSet, fastaFile, pidPattern, decoyType, decoyPrefix));
    }

    /**
     * Search for proteins containing peptides
     *
     * @param peptideSet
     * @param fastaFile
     * @param pidPattern
     * @param decoyType
     * @param decoyPrefix
     * @return
     * @throws IOException
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static Map<String, Set<String>> mapPept2Prots(Set<String> peptideSet,
                                                         String fastaFile,
                                                         Pattern pidPattern,
                                                         DecoySeqFactory.Type decoyType,
                                                         String decoyPrefix)
            throws IOException, ExecutionException, InterruptedException {

        ConcurrentHashMap<String, StringBuffer> pept2Pids = new ConcurrentHashMap<>(peptideSet.size());

        /** search for peptides in single sequence **/
        @AllArgsConstructor
        class Pept2ProtTask implements Runnable {

            String pid;

            StringBuilder sequence;

            @Override
            public void run() {
                mapPept2Prot(pid, sequence.toString());
                if (decoyType != null) {
                    // only one thread act on sequence(so StringBuilder not StringBuffer)
                    String decoySeq = DecoySeqFactory.generate(decoyType, sequence);
                    mapPept2Prot(decoyPrefix + pid, decoySeq);
                }
            }

            private void mapPept2Prot(String pid, String sequence) {
                for (String pept : peptideSet) {
                    if (sequence.contains(pept)) {
                        pid = pid.replace(delimiter, encodedChar);
                        pept2Pids.get(pept).append(pid + delimiter);
                    }
                }
            }
        }

        /** parse fasta and add task **/
        peptideSet.parallelStream().forEach(pept -> pept2Pids.put(pept, new StringBuffer()));
        String pid = null;
        StringBuilder sequence = new StringBuilder();
        String line;
        Matcher m;
        long counter = 0;
        BufferedReader br = new BufferedReader(new FileReader(fastaFile));
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">")) {
                if (pid != null) {
                    ++counter;
                    Pept2ProtTask task = new Pept2ProtTask(pid, sequence);
                    futureList.add(executorService.submit(task));
                    if (futureList.size() == maxQueueLen) {
                        for (Future future : futureList) {
                            future.get();
                        }
                        futureList.clear();
                    }
                    if (counter % 10000 == 0) {
                        log.debug("start to process NO.{}", counter);
                    }
                }
                // reset
                sequence = new StringBuilder();
                // extract protein id
                m = pidPattern.matcher(line);
                if (m.find()) {
                    pid = m.group(1);
                } else {
                    pid = null;
                    log.warn("line <{}> did not capture any protein id");
                }
            } else {
                if (pid != null) {
                    sequence.append(line);
                }
            }
        }
        br.close();
        // deal with the last entry
        if (pid != null) {
            Pept2ProtTask task = new Pept2ProtTask(pid, sequence);
            futureList.add(executorService.submit(task));
        }
        for (Future future : futureList) {
            future.get();
        }
        futureList.clear();
        log.info("{} protein sequences have been processed", ++counter);
        return pept2Pids.entrySet().parallelStream()
                .collect(Collectors.toMap(
                        e -> e.getKey(),
                        e -> Arrays.stream(e.getValue().toString().split(delimiter))
                                .map(protId -> protId.replace(encodedChar, delimiter))
                                .collect(Collectors.toSet())
                ));
    }


}
