package cn.phoenixcenter.metaproteomics.pipeline.base;

public enum ParsimonyType {

    // peptide
//    INDETEMINATE("INDETEMINATE"),
    DISTINCT("DISTINCT"),
    SHARED("SHARED"),

    // protein
    DISCRETE("DISCRETE"),
    DIFFERENTIABLE("DIFFERENTIABLE"),
    SUPERSET("SUPERSET"),
    SUBSET("SUBSET"),
    SUBSUMABLE("SUBSUMABLE");
//    EQUIVALENT("EQUIVALENT");

    private String parsimony;

    ParsimonyType(String parsimony) {
        this.parsimony = parsimony;
    }

    @Override
    public String toString() {
        return this.parsimony;
    }
}
