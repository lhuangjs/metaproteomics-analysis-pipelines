package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido;

import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model.*;
import cn.phoenixcenter.metaproteomics.utils.MathUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * My Fido implementation
 */
@Getter
@Setter
@NoArgsConstructor
@Log4j2
public class Fido {

    private double psmThreshold;

    private double pruningThreshold;

    private String pgDelimiter;

    private String decoyPrefix;

    private double peptidePrior = 0.1;

    private final int numLambda = 100;

    private final double maxLambda = 0.5;

    private final int numBoot = 100;

    // FIXME resset
    private long seed = 3458738122L;

    private final long kRandMax = 4294967291L;

    // threshold for MSE estimation
    private double mseThreshold = 0.1;

    // threshold for ROC AUC estimation
    private int rocN = 50;

    //  value that balances the objective function equation
    //  (kObjectiveLambda * rocR) - (1-kObjectiveLambda) * (fdr_mse)
    private final double kObjectiveLambda = 0.15;


    public Fido(String pgDelimiter, String decoyPrefix, double peptidePrior) {
        this.pgDelimiter = pgDelimiter;
        this.decoyPrefix = decoyPrefix;
        this.peptidePrior = peptidePrior;
    }

    public List<BipartiteGraphWithWeight<ProteinNode, PSMNode, Double>>
    initialize(BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> graph) {
        List<BipartiteGraphWithWeight<ProteinNode, PSMNode, Double>> subGraphList = partitioning(graph);
        clustering(subGraphList);
        return subGraphList;
    }


    /**
     * Partition bipartite graph into small subgraphs
     *
     * @param graph
     * @return
     */
    public List<BipartiteGraphWithWeight<ProteinNode, PSMNode, Double>>
    partitioning(BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> graph) {
        List<BipartiteGraphWithWeight<ProteinNode, PSMNode, Double>> subgraphList = new ArrayList<>();
        Map<ProteinNode, Boolean> visitedProtNodeMap = graph.getVertexCollectionA().keySet()
                .parallelStream()
                .collect(Collectors.toMap(
                        node -> node,
                        node -> false
                ));
        // generate subgraphs by recursive
        for (Map.Entry<ProteinNode, Boolean> e : visitedProtNodeMap.entrySet()) {
            if (!e.getValue()) {
                BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> subGraph = new BipartiteGraphWithWeight<>();
                generateSubgraph(e.getKey(), subGraph, graph, visitedProtNodeMap);
                subgraphList.add(subGraph);
            }
        }
        return subgraphList;
    }

    /**
     * Recursive to generate subgraph.
     *
     * @param node
     * @param subgraph
     * @param graph
     * @param visitedProtNodeMap
     */
    private void generateSubgraph(Node node,
                                  BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> subgraph,
                                  BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> graph,
                                  Map<ProteinNode, Boolean> visitedProtNodeMap) {
        if (node instanceof ProteinNode) {
            visitedProtNodeMap.replace((ProteinNode) node, true);
            // get associated psmCount nodes
            for (Map.Entry<PSMNode, Double> entry : graph.getVertexCollectionA().get(node).entrySet()) {
                PSMNode psmNode = entry.getKey();
                subgraph.addEdge((ProteinNode) node, psmNode, entry.getValue());
                generateSubgraph(psmNode, subgraph, graph, visitedProtNodeMap);
            }
        } else {
            // get associated peptide nodes
            for (Map.Entry<ProteinNode, Double> entry : graph.getVertexCollectionB().get(node).entrySet()) {
                ProteinNode protNode = entry.getKey();
                if (!visitedProtNodeMap.get(protNode)) {
                    generateSubgraph(protNode, subgraph, graph, visitedProtNodeMap);
                }
            }
        }
    }


    /**
     * Collapse peptide nodes which links to a set of identical psmCount nodes.
     * The identical peptide node ids are concatenated by specific delimiter as new peptide node id
     *
     * @param subgraphList
     */
    public void clustering(List<BipartiteGraphWithWeight<ProteinNode, PSMNode, Double>> subgraphList) {
        subgraphList.parallelStream()
                .forEach(subgraph -> {
                    // psmCount nodes hash value => peptide nodes
                    Map<Integer, List<ProteinNode>> groupedProtNodes = subgraph.getVertexCollectionA().keySet().stream()
                            .collect(Collectors.groupingBy(protNode ->
                                    Objects.hash(subgraph.getVertexCollectionA().get(protNode).keySet().toArray())
                            ));
                    for (Map.Entry<Integer, List<ProteinNode>> e : groupedProtNodes.entrySet()) {
                        if (e.getValue().size() > 1) {
                            // collapse identical peptide nodes
                            ProteinNode newProtNode = new ProteinNode(
                                    e.getValue().stream()
                                            .map(Node::getId)
                                            .collect(Collectors.joining(pgDelimiter))
                            );
                            subgraph.updateVertexInA(e.getValue().get(0), newProtNode);
                            // remove all peptide nodes except first peptide node
                            subgraph.removeVertexFromA(
                                    IntStream.range(1, e.getValue().size())
                                            .mapToObj(i -> e.getValue().get(i)).toArray(ProteinNode[]::new)
                            );
                        }
                    }

                });
    }

    // TODO design prune method
    public void pruningByThreshold(List<BipartiteGraphWithWeight<ProteinNode, PSMNode, Double>> subgraphList) {
        System.out.println(subgraphList.size());
        int i = 0;
        for (BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> graph : subgraphList) {
            int protNodeSize = graph.getVertexCollectionA().size();
            double totalState = Math.pow(2, protNodeSize);
            if (totalState >= Integer.MAX_VALUE - 10) {
                System.out.println("=============================");
                System.out.println(i++);

                System.out.println("graph peptide vertex count: " + protNodeSize);
                System.out.println("graph PSM vertex count: " + graph.getVertexCollectionB().size());
                System.out.println("total state: " + totalState);
                Map<PSMNode, ProteinNode> cutEdgeMap = findCuttingEdges(graph);
                System.out.println("graph cut point count: " + cutEdgeMap.size());
                System.out.println();
            }
        }
    }

    public void gridSearch(List<BipartiteGraphWithWeight<ProteinNode, PSMNode, Double>> subgraphList,
                           int gridSearchDepth) {

        /** grid search parameter **/
        List<Double> alphaList = new ArrayList<>();
        List<Double> betaList = new ArrayList<>();
        List<Double> gammaList = new ArrayList<>();
        switch (gridSearchDepth) {
            case 1:
                for (double i = 0.002; i <= 0.4; i *= 4) {
                    alphaList.add(i);
                }
                betaList.add(0.001);
                gammaList.add(0.1);
                gammaList.add(0.5);
                gammaList.add(0.9);
                break;
            case 2:
                for (double i = 0.001; i <= 0.4; i *= 2) {
                    alphaList.add(i);
                }
                betaList.add(0.001);
                gammaList.add(0.1);
                gammaList.add(0.3);
                gammaList.add(0.5);
                gammaList.add(0.7);
                gammaList.add(0.9);
                break;
            case 3:
                for (double i = 0.01; i <= 0.76; i += 0.05) {
                    alphaList.add(i);
                }
                betaList.add(0.001);
                for (double i = 0.05; i <= 0.95; i += 0.05) {
                    gammaList.add(i);
                }
                break;
            case 4: // grid search from FIDO paper
                for (double i = 0.01; i <= 0.76; i += 0.05) {
                    alphaList.add(i);
                }
                for (double i = 0.0; i <= 0.80; i += 0.05) {
                    betaList.add(i);
                }
                for (double i = 0.1; i <= 0.9; i += 0.1) {
                    gammaList.add(i);
                }
                break;
            default:
                alphaList.add(0.008);
                alphaList.add(0.032);
                alphaList.add(0.128);
                betaList.add(0.001);
                gammaList.add(0.5);
        }

        /** do grid search **/
        double bestObjective = Double.NEGATIVE_INFINITY;
        FidoModel bestFidoModel = null;
        // peptide id => peptide PEP, to store peptide probabilities for the best objective
        Map<String, Double> pid2PEPMap = null;
        // set peptide type
        setProteinType(subgraphList);
        // grid search
        for (double alpha : alphaList) {
            FidoModel fidoModel = new FidoModel();
            fidoModel.setPeptidePrior(peptidePrior);
            fidoModel.setAlpha(alpha);
            for (double beta : betaList) {
                fidoModel.setBeta(beta);
                for (double gamma : gammaList) {
                    fidoModel.setGamma(gamma);
                    double objective = calcObjective(subgraphList, fidoModel);
                    if (objective > bestObjective) {
                        bestFidoModel = fidoModel;
                        bestObjective = objective;
                        // backup peptide nodes
                        pid2PEPMap = subgraphList.parallelStream()
                                .flatMap(subgraph -> subgraph.getVertexCollectionA().keySet().stream())
                                .collect(Collectors.toMap(
                                        protNode -> protNode.getId(),
                                        protNode -> protNode.getPosteriorErrorProb()
                                ));
                    }
                }
            }
        }
        // reset peptide node PEP
        for (BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> subgraph : subgraphList) {
            for (ProteinNode protNode : subgraph.getVertexCollectionA().keySet()) {
                protNode.setPosteriorErrorProb(pid2PEPMap.get(protNode.getId()));
            }
        }
        System.err.println("== Fido model best parameters ==");
        System.err.println(bestFidoModel);
    }

    /**
     * Calculate Pr(R | D) of subgraph
     *
     * @param subgraph
     * @param fidoModel
     */
    public void probRGivenD(BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> subgraph,
                            FidoModel fidoModel) {
        // initialize counter of peptide node
        ProteinNode[] proteinNodes = subgraph.getVertexCollectionA().keySet().stream().toArray(ProteinNode[]::new);
        int protNodeSize = proteinNodes.length;
        int j = 0;
        Counter[] counters = new Counter[protNodeSize];
        for (ProteinNode protNode : subgraph.getVertexCollectionA().keySet()) {
            counters[j] = new Counter(1);
            protNode.setCounter(counters[j]);
            j++;
        }
        int totalState = (int) Math.pow(2, protNodeSize);

        /** traverse all state **/
        //  likelihoodNnGivenD array to record all probabilities of L(N = n | D) when n is taken different values
        double[] likelihoodNGivenD = new double[totalState];
        //  probNn array to record all probabilities of Pr(N = n) when n is taken different values
        double[] probN = new double[totalState];
        // probRGivenN array to record all probabilities of Pr(R | N = n) when n is taken different values
        double[][] probRGivenN = new double[totalState][];
        for (int i = 0; Counter.inRange(counters); Counter.advance(counters), i++) {
            likelihoodNGivenD[i] = likelihoodNnGivenD(subgraph, fidoModel);
            probN[i] = probNn(counters, fidoModel);
            probRGivenN[i] = probRGivenNn(counters);
        }

        /** calculate Pr(R | D ) **/
        // sum all L(N = n | D) * Pr(N = n)
        double constant = 0.0;
        for (int i = 0; i < totalState; i++) {
            constant += likelihoodNGivenD[i] * probN[i];
        }
        // Pr(N = n | D)
        double[] probNGivenD = new double[totalState];
        for (int i = 0; i < totalState; i++) {
            probNGivenD[i] = likelihoodNGivenD[i] * probN[i] / constant;
        }
        // Pr(R | D)
        double[] probRGivenD = new double[protNodeSize];
        for (int i = 0; i < totalState; i++) {
            probRGivenD = MathUtil.vectorPlus(probRGivenD, MathUtil.numberMultiply(probNGivenD[i], probRGivenN[i]));
        }
        // set posterior error for peptide nodes
        for (int i = 0; i < probRGivenD.length; i++) {
            proteinNodes[i].setPosteriorErrorProb(1 - probRGivenD[i]);
        }
    }


    /**
     * L(N = n | D)
     *
     * @param subgraph
     * @param fidoModel
     * @return
     */
    private double likelihoodNnGivenD(BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> subgraph,
                                      FidoModel fidoModel) {
        double result = 1.0;
        for (PSMNode psmNode : subgraph.getAllVerticesInB()) {
            Map<ProteinNode, Double> assocProtNodes = subgraph.getVertexCollectionB().get(psmNode);
            // Pr(E_epsilon = e_epsilon | D)
            double probEEpsilonGivenD = assocProtNodes.entrySet()
                    .stream().findFirst().get().getValue(); // the weight is same linked to the same psmCount node
            // Pr(E_epsilon = e_epsilon | Q)
            double probEEpsilon = fidoModel.getPeptidePrior();
            // Pr(E_epsilon = e_epsilon | N = n)
            int activeProtCount = assocProtNodes.keySet()
                    .stream()
                    .mapToInt(protNode -> protNode.getCounter().getState())
                    .sum();
            double probEEpsilonGivenNn = 1 - (1 - fidoModel.getBeta())
                    * Math.pow(1 - fidoModel.getAlpha(), activeProtCount);
            double termE = probEEpsilonGivenD / probEEpsilon * probEEpsilonGivenNn;
            double termNotE = (1 - probEEpsilonGivenD) / (1 - probEEpsilon) * (1 - probEEpsilonGivenNn);
            double term = termE + termNotE;
            result *= term;
        }
        return result;
    }

    /**
     * Pr(N = n)
     *
     * @param counters
     * @param fidoModel
     * @return
     */
    private double probNn(Counter[] counters, FidoModel fidoModel) {
//        double result = 0.0;
//        for (Counter counter: counters) {
//            result += Math.log10(MathUtil.combination(counter.getSize(),counter.getSize()))
//                    + counter.getState() * Math.log10(fidoModel.getGamma())
//                    + (counter.getSize() - counter.getState()) * Math.log10(fidoModel.getGamma());
//
//        }
//        return Math.pow(10, result);
        double result = 1.0;
        for (Counter counter : counters) {
            result *= MathUtil.combination(counter.getSize(), counter.getSize())
                    * Math.pow(fidoModel.getGamma(), counter.getState())
                    * Math.pow(fidoModel.getGamma(), counter.getSize() - counter.getState());

        }
        return Math.pow(10, result);
    }

    /**
     * Pr(R | N = n)
     *
     * @param counters
     * @return
     */
    private double[] probRGivenNn(Counter[] counters) {
        double[] results = new double[counters.length];
        for (int i = 0; i < counters.length; i++) {
            results[i] = (double) counters[i].getState() / counters[i].getSize();
        }
        return results;
    }

    private void setProteinType(List<BipartiteGraphWithWeight<ProteinNode, PSMNode, Double>> subgraphList) {
        subgraphList.parallelStream()
                .forEach(subgraph -> subgraph.getVertexCollectionA().keySet().stream()
                        .forEach(protNode -> {
                            String[] proteinGroup = protNode.getId().split(pgDelimiter);
                            long decoyCount = Arrays.stream(proteinGroup)
                                    .filter(protId -> protId.startsWith(decoyPrefix))
                                    .count();
                            // FIXME Is is right way to judge whether a peptide group is decoy or target peptide?
                            if (decoyCount == 0) {
                                protNode.setProteinType(ProteinNode.ProteinType.TARGET);
                            } else if (decoyCount == proteinGroup.length) {
                                protNode.setProteinType(ProteinNode.ProteinType.DECOY);
                            } else {
                                protNode.setProteinType(ProteinNode.ProteinType.UNCERTAINTY);
                            }
                        }));
    }

    private double calcObjective(List<BipartiteGraphWithWeight<ProteinNode, PSMNode, Double>> subgraphList,
                                 FidoModel fidoModel) {
        // calculate the PEP of peptide nodes
        for (BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> subgraph : subgraphList) {
            probRGivenD(subgraph, fidoModel);
        }
        List<ProteinNode> proteinNodeList = subgraphList.parallelStream()
                .flatMap(subgraph -> subgraph.getVertexCollectionA().keySet().stream())
                .sorted(Comparator.comparingDouble(ProteinNode::getPosteriorErrorProb))
                .collect(Collectors.toList());
        double[][] tmp = getEstimatedFDRAndEmpiricalFDR(proteinNodeList);
        double mse = getFDR_MSE(tmp[0], tmp[1]);
        double roc = getROC_AUC(proteinNodeList);
        return (kObjectiveLambda * roc) - Math.abs((1 - kObjectiveLambda) * mse);
    }

    private double[][] getEstimatedFDRAndEmpiricalFDR(List<ProteinNode> proteinNodeList) {
        // get p-value array
        double[] pvalues = getPValues(proteinNodeList);
        // get estimate pi0
        double pi0 = estimatePi0(pvalues);
        double[] estq = getQValuesFromPEP(proteinNodeList);
        double[] empq = getQValues(pi0, proteinNodeList);
        double[][] result = new double[2][];
        result[0] = estq;
        result[1] = empq;
        return result;
    }

    /**
     * Check: https://github.com/percolator/percolator/issues/237
     *
     * @param proteinNodeList
     * @return
     */
    private double[] getPValues(List<ProteinNode> proteinNodeList) {
        List<Double> pvalueList = new ArrayList<>();
        int nDecoys = 1, posSame = 0, negSame = 0;
        for (int i = 0; i < proteinNodeList.size(); i++) {
            if (proteinNodeList.get(i).getProteinType() == ProteinNode.ProteinType.DECOY) {
                negSame++;
            } else {
                posSame++;
            }
            if (i + 1 == proteinNodeList.size()
                    || proteinNodeList.get(i).getPosteriorErrorProb() != proteinNodeList.get(i + 1).getPosteriorErrorProb()) {
                for (int j = 0; j < posSame; j++) {
                    pvalueList.add(nDecoys + negSame * (j + 1) / (double) (posSame + 1));
                }
                nDecoys += negSame;
                posSame = 0;
                negSame = 0;
            }
        }
        double[] pvalues = new double[pvalueList.size()];
        for (int i = 0; i < pvalueList.size(); i++) {
            pvalues[i] = pvalueList.get(i) / nDecoys;
        }
        return pvalues;
    }

    private double estimatePi0(double[] pvalues) {
        List<Double> lambdas = new ArrayList<>();
        List<Double> pi0s = new ArrayList<>();
        for (int i = 0; i < numLambda; i++) {
            double lambda = ((i + 1) / (double) numLambda) * maxLambda;
            // find the first element in p-value list that doesn't match < lambda
            double wl = pvalues.length - lowerBound(pvalues, lambda);
            double pi0 = wl / pvalues.length / (1 - lambda);
            if (pi0 > 0.0) {
                lambdas.add(lambda);
                pi0s.add(pi0);
            }
        }
        if (pi0s.size() == 0) { // FIXME delete?
            throw new RuntimeException("Error in the input nodeData: too good separation between target "
                    + "and decoy PSMs.\nImpossible to estimate pi0. Terminating.\n");
        }
        double minPi0 = pi0s.stream()
                .min(Comparator.comparingDouble(pi0 -> pi0.doubleValue()))
                .get();
        // initialize the mse array with zero
        double[] mse = new double[pi0s.size()];
        for (int boot = 0; boot < numBoot; boot++) {
            // create an array of bootstrapped p-values, and sort in ascending order
            double[] pBoot = bootstrap(pvalues);
            for (int i = 0; i < lambdas.size(); i++) {
                double wl = pBoot.length - lowerBound(pBoot, lambdas.get(i));
                double pi0Boot = wl / pBoot.length / (1 - lambdas.get(i));
                // estimated mean-squared error
                mse[i] += (pi0Boot - minPi0) * (pi0Boot - minPi0);
            }
        }
        int minIndex = 0;
        double minMSE = mse[0];
        for (int i = 1; i < mse.length; i++) {
            if (mse[i] < minMSE) {
                minIndex = i;
            }
        }
        return Double.max(Double.min(pi0s.get(minIndex), 1.0), 0.0);
    }

    private int lowerBound(double[] arr, double val) {
        int i = 0;
        for (; i < arr.length; i++) {
            if (arr[i] >= val) {
                return i;
            }
        }
        return i;
    }

    private double[] bootstrap(double[] in) {
        // FIXME set it as field?
        int maxSize = 1000;
        int size = Integer.min(in.length, maxSize);
        double[] result = new double[size];
        for (int i = 0; i < size; i++) {
            int draw = (int) ((double) lcgRand() / (kRandMax + 1) * in.length);
            result[i] = in[draw];
        }
        return Arrays.stream(result).sorted().toArray();
    }

    private long lcgRand() {
        seed = (seed * 279470273L) % kRandMax;
        return seed;
    }

    private double[] getQValuesFromPEP(List<ProteinNode> proteinNodeList) {
        double[] qvalues = new double[proteinNodeList.size()];
        int numProt = 1;
        double sum = 0.0;
        for (int i = 0; i < proteinNodeList.size(); i++) {
            sum += proteinNodeList.get(i).getPosteriorErrorProb();
            qvalues[i] = sum / numProt++;
        }
        for (int i = 0; i < qvalues.length; i++) {
            qvalues[i] = Double.min(qvalues[i], qvalues[qvalues.length - 1]);
        }
        return qvalues;
    }

    // FIXME reference : Uri Keich's code written in R;这一步计算时省略了部分计算（见IGNORE）

    /**
     * The FDR-derived q-value estimation. we multiply the percentage of incorrect target PSMs (π0, pi0) by
     * the number of decoy PSMs, and then divide by the number of target PSMs above a score threshold
     *
     * @param pi0
     * @param proteinNodeList
     * @return
     */
    private double[] getQValues(double pi0, List<ProteinNode> proteinNodeList) {
        List<Double> qvalueList = new ArrayList<>();
        List<Integer> h_w_le_z = new ArrayList<>(); // N_{ w <= z }
        List<Integer> h_z_le_z = new ArrayList<>(); // N_{ z <= z }
        if (pi0 < 1.0) { // 没有测试到
            getMixMaxCounts(proteinNodeList, h_w_le_z, h_z_le_z);
        }
        double estPx_lt_zj = 0.0;
        double E_f1_mod_run_tot = 0.0;
        double fdr = 0.0;
        int n_z_ge_w = 1, n_w_ge_w = 0; // N_{z>=w} and N_{w>=w}
        int decoyQueue = 0, targetQueue = 0; // handles ties
        for (int i = 0; i < proteinNodeList.size(); i++) {
            if (proteinNodeList.get(i).getProteinType() == ProteinNode.ProteinType.TARGET) {
                n_w_ge_w++;
                targetQueue++;
            } else {
                n_z_ge_w++;
                decoyQueue++;
            }
            if (i + 1 == proteinNodeList.size() ||
                    proteinNodeList.get(i).getPosteriorErrorProb() != proteinNodeList.get(i + 1).getPosteriorErrorProb()) {
                if (pi0 < 1.0 && decoyQueue > 0) { // 没有测试到
                    int j = h_w_le_z.size() - (n_z_ge_w - 1);
                    int cnt_w = h_w_le_z.get(j);
                    int cnt_z = h_z_le_z.get(j);
                    estPx_lt_zj = (cnt_w - pi0 * cnt_z) / ((1.0 - pi0) * cnt_z);
                    estPx_lt_zj = estPx_lt_zj > 1 ? 1 : estPx_lt_zj;
                    estPx_lt_zj = estPx_lt_zj < 0 ? 0 : estPx_lt_zj;
                    E_f1_mod_run_tot += decoyQueue * estPx_lt_zj * (1.0 - pi0);
                }
                targetQueue += decoyQueue;
                fdr = (n_z_ge_w * pi0 + E_f1_mod_run_tot) / Integer.max(1, n_w_ge_w);
                for (int j = 0; j < targetQueue; j++) {
                    qvalueList.add(Double.min(fdr, 1.0));
                }
                decoyQueue = 0;
                targetQueue = 0;
            }
        }
        double[] qvalues = new double[qvalueList.size()];
        for (int i = 0; i < qvalueList.size(); i++) {
            qvalues[i] = Double.min(qvalueList.get(i), qvalueList.get(qvalueList.size() - 1));
        }
        return qvalues;
    }

    /**
     * The posterior error probability need to sorted in descending order
     *
     * @param proteinNodeList
     */
    private void getMixMaxCounts(List<ProteinNode> proteinNodeList,
                                 List<Integer> h_w_le_z,
                                 List<Integer> h_z_le_z) {
        int cntZ = 0, cntW = 0, queue = 0;
        for (int i = proteinNodeList.size() - 1; i >= 0; i--) {
            if (proteinNodeList.get(i).getProteinType() == ProteinNode.ProteinType.TARGET) {
                cntW++;
            } else {
                cntZ++;
                queue++;
            }
            if (i == 0 || proteinNodeList.get(i).getPosteriorErrorProb()
                    != proteinNodeList.get(i - 1).getPosteriorErrorProb()) {
                for (int j = 0; j < queue; j++) {
                    h_w_le_z.add(cntW);
                    h_z_le_z.add(cntZ);
                }
                queue = 0;
            }
        }
    }

    private double getFDR_MSE(double[] estFDR, double[] empFDR) {
        double mse = 0.0;
        if (Double.min(estFDR[0], estFDR[estFDR.length - 1]) >= mseThreshold
                || estFDR.length != empFDR.length
                || estFDR.length == 0
                || empFDR.length == 0
                || (Double.max(estFDR[0], estFDR[estFDR.length - 1]) <= 0.0
                && Double.max(empFDR[0], empFDR[empFDR.length - 1]) <= 0.0)
        ) {
            return 1.0;
        }
        double x1 = 0.0, x2 = 0.0, y1 = 0.0, y2 = 0.0; // 没有测试到
        for (int i = 0; i < estFDR.length - 1; i++) {
            if (estFDR[i] <= mseThreshold && empFDR[i] <= mseThreshold) {
                x1 = estFDR[i];
                x2 = estFDR[i + 1];
                y1 = x1 - empFDR[i];
                y2 = x2 - empFDR[i + 1];
            } else if (estFDR[i] <= mseThreshold) {
                x1 = estFDR[i];
                x2 = estFDR[i + 1];
                y1 = x1;
                y2 = x2;
            } else {
                rocN = Integer.max(rocN, Integer.max(50, Integer.min(i, 500)));
            }
            if (x1 != x2 && x2 != 0 && y2 != 0) {
                x2 = Double.min(x2, mseThreshold); // x2 is above mseThreshold
                mse += areaSq(x1, y2, x2, y2);

            }
        }
        double normalizer = Math.abs(Double.min(estFDR[estFDR.length - 1], mseThreshold) - estFDR[0]);
        mse /= Math.pow(normalizer, 3) / 3;
        return mse;
    }

    private double areaSq(double x1, double y1, double x2, double y2) {
        double m = (y2 - y1) / (x2 - x1);
        double b = y1 - m * x1;
        return squareAntiderivativeAt(m, b, x2) - squareAntiderivativeAt(m, b, x1);
    }

    private double squareAntiderivativeAt(double m, double b, double xVal) {
        double u = m * m;
        double v = 2 * m * b;
        double t = b * b;
        return (u * xVal * xVal * xVal / 3.0 + v * xVal * xVal / 2.0 + t * xVal);
    }

    private double getROC_AUC(List<ProteinNode> proteinNodeList) {
        int prevTp = 0, prevFp = 0, tp = 0, fp = 0;
        double prevProb = -1;
        double auc = 0.0;
        for (int i = 0; i < proteinNodeList.size(); i++) {
            double prob = proteinNodeList.get(i).getPosteriorErrorProb();
            String[] pgNames = proteinNodeList.get(i).getId().split(pgDelimiter);
            int tpChange = (int) Arrays.stream(pgNames)
                    .filter(pid -> !pid.startsWith(decoyPrefix))
                    .count();
            int fpChange = pgNames.length - tpChange;
            if (tpChange > 0) {
                tpChange = 1;
                fpChange = 0;
            } else if (fpChange > 0) {
                fpChange = 1;
            }

            tp += tpChange;
            fp += fpChange;

            // only do it when fp changes and either of them is != 0
            if (prevProb != -1 && fp != 0 && tp != 0 && fp != prevFp) {
                double trapezoid = trapezoidArea(fp, prevFp, tp, prevTp);
                prevFp = fp;
                prevTp = tp;
                auc += trapezoid;
            }
            prevProb = prob;
        }
        int normalizer = tp * fp;
        if (normalizer > 0) {
            auc /= normalizer;
        } else {
            auc = 0.0;
        }
        return auc;
    }

    private double trapezoidArea(double x1, double x2, double y1, double y2) {
        double base = Math.abs(x1 - x2);
        double height_avg = Math.abs((y1 + y2) / 2);
        return base * height_avg;
    }

    /**
     * Find cutting edges to split big bipartite graph.
     * The cutting edge where we find has some difference with traditional cutting edge:
     * <p>
     * 1. only remain cutting edges
     * 2. only remain the cutting edge linked to the peptide node with
     * bigger degree when multiple cutting edges are linked to the same psmCount node
     *
     * @param graph
     * @return
     */
    public Map<PSMNode, ProteinNode> findCuttingEdges(BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> graph) {
        Map<PSMNode, ProteinNode> cuttingEdgeMap = new HashMap<>();
        Map<Node, CutPointHelper> node2CutPointHelper = Stream.of(graph.getVertexCollectionA(), graph.getVertexCollectionB())
                .flatMap(collection -> collection.keySet().stream())
                .collect(Collectors.toMap(
                        node -> node,
                        node -> new CutPointHelper()
                ));
        int time = 0;
        for (Node node : node2CutPointHelper.keySet()) {
            if (!node2CutPointHelper.get(node).isVisited()) {
                findCuttingEdges(node, time, null,
                        graph, node2CutPointHelper, cuttingEdgeMap);
            }
        }
        return cuttingEdgeMap;
    }

    private void findCuttingEdges(Node node, int time, Node parentNode,
                                  BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> graph,
                                  Map<Node, CutPointHelper> node2CutPointHelper,
                                  Map<PSMNode, ProteinNode> cuttingEdgeMap) {
        CutPointHelper nodeCPH = node2CutPointHelper.get(node);
        nodeCPH.setDiscoveryTime(time);
        nodeCPH.setMinDiscoveryTime(time);
        nodeCPH.setParentNode(parentNode);
        nodeCPH.setVisited(true);
        time++;
        Set<Node> nodeSet = node instanceof ProteinNode
                ? new HashSet<>(graph.getVertexCollectionA().get(node).keySet())
                : new HashSet<>(graph.getVertexCollectionB().get(node).keySet());
        for (Node associatedNode : nodeSet) {
            CutPointHelper assoCPH = node2CutPointHelper.get(associatedNode);
            if (!assoCPH.isVisited()) {
                findCuttingEdges(associatedNode, time, node,
                        graph, node2CutPointHelper, cuttingEdgeMap);
                nodeCPH.setMinDiscoveryTime(Integer.min(nodeCPH.getMinDiscoveryTime(), assoCPH.getMinDiscoveryTime()));
                if (assoCPH.getMinDiscoveryTime() > nodeCPH.getDiscoveryTime()) {
                    final ProteinNode protNode;
                    final PSMNode psmNode;
                    if (node instanceof ProteinNode) {
                        protNode = (ProteinNode) node;
                        psmNode = (PSMNode) associatedNode;
                    } else {
                        protNode = (ProteinNode) associatedNode;
                        psmNode = (PSMNode) node;
                    }
                    if (graph.getDegreeInA(protNode) > 1 && graph.getDegreeInB(psmNode) > 1) {
                        // only remain the cutting edge linked to the peptide node with
                        // bigger degree when multiple cutting edges are linked to the same psmCount node
                        cuttingEdgeMap.merge(psmNode, protNode, (protNode1, protNode2) -> {
                                    log.debug("merge: key - {}, value - {}:{} vs {}:{}",
                                            psmNode.getId(),
                                            protNode1.getId(), graph.getDegreeInA(protNode1),
                                            protNode2.getId(), graph.getDegreeInA(protNode2));
                                    return graph.getDegreeInA(protNode1) >= graph.getDegreeInA(protNode2)
                                            ? protNode1
                                            : protNode2;
                                }
                        );
                    }
                }
            } else if (!associatedNode.equals(nodeCPH.getParentNode())) { // v is not parent node of u
                nodeCPH.setMinDiscoveryTime(Integer.min(nodeCPH.getMinDiscoveryTime(), assoCPH.getDiscoveryTime()));
            }
        }
    }

    /**
     * for finding articulation points(cut vertices)
     */
    @Getter
    @Setter
    @NoArgsConstructor
    @ToString
    private class CutPointHelper {

        private boolean visited = false;

        // discovery time of the vertex in DFS tree
        private int discoveryTime;

        // the earliest discovery time of the vertex in DFS tree
        private int minDiscoveryTime;

        // the parent node of the vertex in DFS tree
        private Node parentNode;
    }
}
