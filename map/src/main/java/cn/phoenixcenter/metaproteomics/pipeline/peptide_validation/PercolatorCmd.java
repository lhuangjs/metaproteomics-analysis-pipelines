package cn.phoenixcenter.metaproteomics.pipeline.peptide_validation;

import cn.phoenixcenter.metaproteomics.base.ICommand;
import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.pipeline.peptide_validation.model.PercolatorParam;
import cn.phoenixcenter.metaproteomics.utils.CommandExecutor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.bind.annotation.GetMapping;

@Data
@NoArgsConstructor
public class PercolatorCmd implements ICommand {

    private static final long serialVersionUID = 1L;

    private final String cruxLocation = GlobalConfig.getValue("crux");

    private String percolatorOptions;

    public PercolatorCmd(PercolatorParam percolatorParam) {
        try {
            this.percolatorOptions = PercolatorParamParser.toOptions(percolatorParam);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getDesc() {
        return "percolator";
    }

    @Override
    public void execute() throws Exception {
        String command = String.join(" ",
                cruxLocation, "percolator",
                percolatorOptions
        );
        CommandExecutor.exec(command);
    }
}
