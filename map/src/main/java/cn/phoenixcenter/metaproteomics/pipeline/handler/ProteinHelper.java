package cn.phoenixcenter.metaproteomics.pipeline.handler;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import cn.phoenixcenter.metaproteomics.taxonomy.TaxonSearcher;
import cn.phoenixcenter.metaproteomics.utils.CommandExecutor;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rits.cloning.Cloner;
import lombok.extern.log4j.Log4j2;
import org.junit.Assert;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Log4j2
public class ProteinHelper {

    private static final JsonFactory jsonFactory = new ObjectMapper().getFactory();

    private static final TaxonSearcher taxonSearch = GlobalConfig.getObj(TaxonSearcher.class);

    private static final String qvalityLocation = GlobalConfig.getValue("qvality");

    /**
     * Set alternative info for MPProtein
     *
     * @param mpProteinSet output
     */
    public static void setAltProteinSet(Set<MPProtein> mpProteinSet) {
        // extract equivalent proteins containing same peptides
        List<MPProtein> mpProteinList = new ArrayList<>(mpProteinSet);
        log.debug("protein set size before collapsing equivalent proteins: {}", mpProteinList.size());
        // 2th, 3th ... equivalent proteins => 1th equivalent
        Map<MPProtein, MPProtein> prot2AltProt = new HashMap<>(mpProteinList.size());
        int altProtCount = 0;
        for (int i = 0; i < mpProteinList.size() - 1; i++) {
            MPProtein prot1 = mpProteinList.get(i);
            if (!prot2AltProt.containsKey(prot1)) {
                for (int j = i + 1; j < mpProteinList.size(); j++) {
                    MPProtein prot2 = mpProteinList.get(j);
                    if (prot1.getPeptideCount() == prot2.getPeptideCount()
                            && prot1.getContainedPeptideSet().containsAll(prot2.getContainedPeptideSet())) {
                        prot2AltProt.put(prot2, prot1);
                        altProtCount++;
                    }
                }
            }
        }
        log.debug("alternative protein pair count: {}", altProtCount);
        // set altProteinSet for MPProtein
        for (Map.Entry<MPProtein, MPProtein> entry : prot2AltProt.entrySet()) {
            MPProtein altMPProt = entry.getKey();
            // remove alternative protein from associated peptide
            for (MPPeptide mpPept : altMPProt.getContainedPeptideSet()) {
                mpPept.getAssociatedProteinSet().remove(altMPProt);
            }
            // add alternative protein for the protein
            MPProtein mainMPProt = entry.getValue();
            if (mainMPProt.getAltProteinSet() == null) {
                mainMPProt.setAltProteinSet(new HashSet<>());
            }
            mainMPProt.getAltProteinSet().add(altMPProt.getId());
            mpProteinSet.remove(altMPProt);
        }
        // only for check
        Assert.assertEquals(mpProteinList.size() - altProtCount, mpProteinSet.size());
        log.debug("protein set size after collapsing equivalent proteins: {}", mpProteinSet.size());
    }

    public static void writeJsonLibrary(Path fastaPath, String pidRegexp, String tidRegexp, Path libraryJsonPath) throws IOException {
        JsonGenerator generator = jsonFactory.createGenerator(libraryJsonPath.toFile(), JsonEncoding.UTF8);
        BufferedReader br = Files.newBufferedReader(fastaPath, StandardCharsets.UTF_8);
        long totalCount = 0;
        long illegalPidCount = 0;
        long illegalTidCount = 0;
        generator.writeStartArray();
        String line;
        String pid = null;
        Integer tid = null;
        StringBuilder sequence = new StringBuilder();
        Pattern pidPattern = Pattern.compile(pidRegexp);
        Pattern tidPattern = Pattern.compile(tidRegexp);
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">")) {
                totalCount++;
                if (pid != null) {
                    generator.writeStartObject();
                    generator.writeStringField("id", pid);
                    generator.writeStringField("sequence", sequence.toString());
                    generator.writeNumberField("length", sequence.length());
                    generator.writeObjectField("taxon", tid == null ? null : taxonSearch.getTaxonById(tid));
                    generator.writeEndObject();
                    generator.writeRaw(System.lineSeparator());
                    // reset
                    sequence = new StringBuilder(sequence.length());
                }
                Matcher pidMatcher = pidPattern.matcher(line);
                if (pidMatcher.find()) {
                    pid = pidMatcher.group(1);
                    Matcher tidMatcher = tidPattern.matcher(line);
                    if (tidMatcher.find()) {
                        tid = new Integer(tidMatcher.group(1));
                    } else {
                        illegalTidCount++;
                        log.warn("Cannot capture taxon id info in line: {}", line);
                    }
                } else {
                    pid = null;
                    illegalPidCount++;
                    log.warn("Cannot capture protein id info in line: {}", line);
                }
            } else {
                if (pid != null) {
                    sequence.append(line);
                }
            }
        }
        // deal with the last entry
        if (pid != null) {
            generator.writeStartObject();
            generator.writeStringField("id", pid);
            generator.writeStringField("sequence", sequence.toString());
            generator.writeNumberField("length", sequence.length());
            generator.writeObjectField("taxon", tid == null ? null : taxonSearch.getTaxonById(tid));
            generator.writeEndObject();
            generator.writeRaw(System.lineSeparator());
        }
        generator.writeEndArray();
        generator.close();
        log.info("Total protein entry count: {}. In those entries, " +
                        "{} entries have been ignored because protein id cannot be captured " +
                        "and {} entries' taxon id cannot be captured.",
                totalCount, illegalPidCount, illegalTidCount);

    }

    public static void parseJsonLibrary(String libraryJsonFile, Consumer<MPProtein> consumer) throws IOException {
        JsonParser parser = jsonFactory.createParser(new File(libraryJsonFile));
        parser.nextToken(); // [
        while (parser.nextToken() != JsonToken.END_ARRAY) { // {
            MPProtein mpProt = new MPProtein();
            // pid
            parser.nextToken();
            parser.nextToken();
            mpProt.setId(parser.getValueAsString());
            // sequence
            parser.nextToken();
            parser.nextToken();
            mpProt.setSequence(parser.getValueAsString());
            // length
            parser.nextToken();
            parser.nextToken();
            mpProt.setLength(parser.getValueAsInt());
            //  taxon
            parser.nextToken();
            parser.nextToken();
            Taxon taxon = parser.readValueAs(Taxon.class);
            mpProt.setTaxon(taxon);
            consumer.accept(mpProt);
            parser.nextToken(); // }
        }
        parser.close();
    }

    public static void writeMPProteins(String protJsonFile,
                                       String libraryJsonFile,
                                       Path mpProteinTSVPath) throws IOException {
        Map<String, MPProtein> pid2Prot = new HashMap<>(5_000);
        Cloner cloner = new Cloner();
        JsonParser jsonParser = jsonFactory.createParser(new File(protJsonFile));
        jsonParser.nextToken(); // [
        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
            MPProtein mpProt = jsonParser.readValueAs(MPProtein.class);
            if (mpProt.getAltProteinSet() != null) {
                mpProt.getAltProteinSet().stream().forEach(id -> {
                    MPProtein altProt = cloner.deepClone(mpProt);
                    altProt.setId(id);
                    altProt.setAltProteinSet(null);
                    pid2Prot.put(id, altProt);
                });
                mpProt.setAltProteinSet(null);
            }
            pid2Prot.put(mpProt.getId(), mpProt);
        }
        jsonParser.close();
        log.debug("Total protein count: {}", pid2Prot.size());
        // write
        Taxon.Rank[] ranks = Taxon.Rank.values();
        String ranksStr = Arrays.stream(ranks)
                .map(Taxon.Rank::rankToString)
                .collect(Collectors.joining("\t"));
        final String nullTaxonStr = IntStream.range(0, ranks.length)
                .mapToObj(i -> "")
                .collect(Collectors.joining("\t"));

        BufferedWriter bw = Files.newBufferedWriter(mpProteinTSVPath, StandardCharsets.UTF_8);
        bw.write(String.join("\t",
                "Protein",
                "Probability",
                "Q-value",
                "PEP",
                "Peptide",
                "PSM count",
                "Peptide count",
                "Taxon id",
                "Taxon name",
                ranksStr,
                ranksStr,
                "Length",
                "Sequence"
        ) + System.lineSeparator());
        Consumer<MPProtein> consumer = (MPProtein mpProtein) -> {
            try {
                MPProtein existProt;
                if ((existProt = pid2Prot.get(mpProtein.getId())) != null) {
                    List<MPPeptide> mpPeptideList = new ArrayList<>(existProt.getContainedPeptideSet());
                    String peptSeq = mpPeptideList.get(0).getSequence();
                    String peptPSM = String.valueOf(mpPeptideList.get(0).getSpectrumCount());
                    for (int i = 1; i < mpPeptideList.size(); i++) {
                        peptSeq += "," + mpPeptideList.get(i).getSequence();
                        peptPSM += "," + mpPeptideList.get(i).getSpectrumCount();
                    }
                    Taxon taxon = mpProtein.getTaxon();

                    bw.write(String.join("\t",
                            // id
                            existProt.getId(),
                            // Probability
                           String.valueOf( existProt.getProb()),
                           // Q-value
                            String.valueOf( existProt.getQvalue()),
                            // PEP
                            String.valueOf( existProt.getPep()),
                            // peptide sequences
                            peptSeq,
                            // peptide PSMs
                            peptPSM,
                            // peptide count
                            String.valueOf(existProt.getContainedPeptideSet().size()),
                            // taxon
                            taxon != null ? String.valueOf(taxon.getId()) : "",
                            taxon != null ? taxon.getName() : "",
                            taxon != null ? taxon.tid2String("\t") : nullTaxonStr,
                            taxon != null ? taxon.tname2String("\t"): nullTaxonStr,
                            String.valueOf(mpProtein.getLength()),
                            mpProtein.getSequence()
                    ) + System.lineSeparator());
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        };
        parseJsonLibrary(libraryJsonFile, consumer);
    }


    public static Set<MPProtein> parseJsonMPProteins(String protJsonFile, double protFDRThreshold) throws IOException {
        Set<MPProtein> mpProteinSet = new HashSet<>(5_000);
        JsonParser jsonParser = jsonFactory.createParser(new File(protJsonFile));
        jsonParser.nextToken(); // [
        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
            MPProtein mpProt = jsonParser.readValueAs(MPProtein.class);
            if (mpProt.getQvalue() < protFDRThreshold) {
                mpProteinSet.add(mpProt);
            }
        }
        jsonParser.close();
        log.info("There are {} protein whose Q-value < {}", mpProteinSet.size(), protFDRThreshold);
        Map<String, MPPeptide> seq2Pept = new HashMap<>(mpProteinSet.size());
        for (MPProtein mpProt : mpProteinSet) {
            Set<MPPeptide> mpPeptSet = mpProt.getContainedPeptideSet();
            Set<MPPeptide> newMPPeptideSet = new HashSet<>(mpPeptSet.size());
            for (MPPeptide mpPept : mpPeptSet) {
                MPPeptide newMPPept = null;
                if (seq2Pept.containsKey(mpPept.getSequence())) {
                    newMPPept = seq2Pept.get(mpPept.getSequence());
                } else {
                    newMPPept = mpPept;
                    newMPPept.setAssociatedProteinSet(new HashSet<>());
                    seq2Pept.put(mpPept.getSequence(), mpPept);
                }
                newMPPept.getAssociatedProteinSet().add(mpProt);
                newMPPeptideSet.add(newMPPept);
            }
            // update peptides for the protein
            mpProt.setContainedPeptideSet(newMPPeptideSet);
        }
        Set<MPPeptide> mpPeptSet = new HashSet<>(seq2Pept.values());
        ProteinGroupHandler.addParsimonyTypeInfo(mpPeptSet, mpProteinSet);
        return mpProteinSet;
    }

    /**
     * Calculate q-value(high score is better).
     *
     * @param pid2Prob
     * @param decoyPrefix
     * @return protein id => prob, pep, q-value
     * @throws IOException
     */
    public static Map<String, double[]> calQvalue(Map<String, Double> pid2Prob, String decoyPrefix) throws IOException {
        // sequence, prob, pep, q-value
        String[][] tmpResults = pid2Prob.entrySet().stream()
                .sorted(Comparator.comparingDouble((Map.Entry<String, Double> e) -> e.getValue()).reversed())
                .map(e -> new String[]{e.getKey(), e.getValue().toString(), null, null})
                .toArray(String[][]::new);
        // organize qvality input
        Map<Boolean, String> targetDecoyPart = Stream.of(tmpResults)
                // true - target; false - decoy
                .collect(Collectors.groupingBy((String[] e) -> !e[0].startsWith(decoyPrefix),
                        Collectors.mapping(e -> e[1], Collectors.joining(System.lineSeparator()))
                ));
        Path targetPath = Files.createTempFile("target", ".txt");
        Path decoyPath = Files.createTempFile("decoy", ".txt");
        log.debug("create temporary files <{}> and <{}> to save target and decoy scores respectively", targetPath, decoyPath);
        Files.write(targetPath, targetDecoyPart.get(true).getBytes(StandardCharsets.UTF_8));
        Files.write(decoyPath, targetDecoyPart.get(false).getBytes(StandardCharsets.UTF_8));
        // run qvality
        String command = String.join(" ",
                qvalityLocation,
                targetPath.toString(),
                decoyPath.toString(),
                "-Y -d");
        AtomicInteger index = new AtomicInteger(0);
        Consumer<InputStream> outConsumer = (InputStream in) -> {
            try (
                    BufferedReader br = new BufferedReader(new InputStreamReader(in))
            ) {
                String line;
                while ((line = br.readLine()) != null) {
                    // score, pep, q-value
                    String[] tmp = line.split("\t");
                    if (!tmp[0].equals("Score")) {
                        tmpResults[index.get()][2] = tmp[1];
                        tmpResults[index.get()][3] = tmp[2];
                        index.getAndIncrement();
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        };
        CommandExecutor.exec(command, outConsumer);
        // delete temporary files
        Files.delete(targetPath);
        Files.delete(decoyPath);
        log.debug("delete temporary files <{}> and <{}>", targetPath, decoyPath);
        Map<String, double[]> result = new LinkedHashMap<>(tmpResults.length);
        for (String[] tmp : tmpResults) {
            result.put(tmp[0], Stream.of(tmp).skip(1).mapToDouble(str -> Double.parseDouble(str)).toArray());
        }
        return result;
    }

    /**
     * Reference: https://pubs.acs.org/doi/abs/10.1021/ac9023999
     *
     * @param mpProteinSet
     */
    public static void dNSAFPSMCount(Set<MPProtein> mpProteinSet) {
        Map<MPProtein, Integer> prot2UniqPeptPSMCount = new ConcurrentHashMap<>(mpProteinSet.size());
        Map<MPProtein, Integer> prot2SharedPeptPSMCount = new ConcurrentHashMap<>(mpProteinSet.size());
        mpProteinSet.parallelStream()
                .forEach(mpProt -> {
                    // true - unique peptide, false - shared peptide
                    Map<Boolean, Integer> uniqueSharedPeptPSMCount = mpProt.getContainedPeptideSet()
                            .stream()
                            .collect(Collectors.groupingBy(mpPept -> mpPept.isUniq(),
                                    Collectors.summingInt(p -> p.getSpectrumCount().intValue())
                            ));
                    Integer uniquePeptPSMCount = uniqueSharedPeptPSMCount.get(true);
                    Integer sharedPeptPSMCount = uniqueSharedPeptPSMCount.get(false);
                    prot2UniqPeptPSMCount.put(mpProt, uniquePeptPSMCount != null ? uniquePeptPSMCount : 0);
                    prot2SharedPeptPSMCount.put(mpProt, sharedPeptPSMCount != null ? sharedPeptPSMCount : 0);
                });
        double totalUniqPeptPSMCount = prot2UniqPeptPSMCount.values().parallelStream()
                .mapToInt(Integer::intValue)
                .sum();
        mpProteinSet.parallelStream()
                .forEach(mpProt -> {
                    int uniqPeptPSMCount = prot2UniqPeptPSMCount.get(mpProt);
                    int altProtSize = mpProt.getAltProteinSet() != null ?
                            mpProt.getAltProteinSet().size()
                            : 1;
//                    if (uniqPeptPSMCount != 0) {
////                        mpProt.setPsmCount((uniqPeptPSMCount +
////                                prot2SharedPeptPSMCount.get(mpProt) * uniqPeptPSMCount / totalUniqPeptPSMCount)
////                                / mpProt.getContainedPeptideSet().stream().mapToInt(mpPept -> mpPept.getSequence().length()).sum()
////                        );
//                        mpProt.setPsmCount((uniqPeptPSMCount +
//                                prot2SharedPeptPSMCount.get(mpProt) * uniqPeptPSMCount / totalUniqPeptPSMCount)
//                                / mpProt.getLength() * altProtSize
//                        );
//                    } else {
//                        mpProt.setPsmCount(0.0);
//                    }
                    mpProt.setPsmCount(mpProt.getContainedPeptideSet().stream()
                            .mapToDouble(MPPeptide::getSpectrumCount).sum()
                    );
                });
    }

    public static void writeTSVProtein(String protJsonFile, double protFDRThreshold, Path protTSVPath) throws IOException {
        Set<MPProtein> mpProteinSet = parseJsonMPProteins(protJsonFile, protFDRThreshold);
        writeTSVProtein(mpProteinSet, protTSVPath);
    }

    public static void writeTSVProtein(Collection<MPProtein> mpProteins, Path protTSVPath) throws IOException {
        // sort by probability
        mpProteins = mpProteins.parallelStream()
                .sorted(Comparator.comparingDouble(MPProtein::getProb).reversed().thenComparing(MPProtein::getId))
                .collect(Collectors.toList());
        BufferedWriter writer = Files.newBufferedWriter(protTSVPath, StandardCharsets.UTF_8);
        StringJoiner ranksJoiner = new StringJoiner("\t");
        StringJoiner nullRanksJoiner = new StringJoiner("\t");
        for (Taxon.Rank rank : Taxon.Rank.values()) {
            ranksJoiner.add(rank.rankToString());
            nullRanksJoiner.add("");
        }
        String ranks = ranksJoiner.toString();
        String nullRanks = nullRanksJoiner.toString();
        writer.write(String.join("\t",
                "Protein", "Alternative proteins", "Probability", "Q-value",
                "PEP", "Parsimony type", "PSM count", "Peptides", "Length",
                "Taxon id", "Taxon name", "Rank", ranks, ranks, "Sequence"
        ) + System.lineSeparator());
        for (MPProtein mpProtein : mpProteins) {
            Taxon taxon = mpProtein.getTaxon();
            writer.write(String.join("\t",
                    mpProtein.getId(),
                    mpProtein.getAltProteinSet() != null
                            ? mpProtein.getAltProteinSet().stream()
                            .collect(Collectors.joining(","))
                            : "",
                    String.valueOf(mpProtein.getProb()),
                    String.valueOf(mpProtein.getQvalue()),
                    String.valueOf(mpProtein.getPep()),
                    mpProtein.getParsimonyType().toString(),
                    String.valueOf(mpProtein.getPsmCount()),
                    mpProtein.getContainedPeptideSet().stream()
                            .map(MPPeptide::getSequence)
                            .collect(Collectors.joining(",")),
                    String.valueOf(mpProtein.getLength()),
                    taxon != null ? String.valueOf(taxon.getId()) : "",
                    taxon != null ? taxon.getName() : "",
                    taxon != null ? taxon.getRank() : "",
                    taxon != null ? taxon.tid2String("\t") : nullRanks,
                    taxon != null ? taxon.tname2String("\t") : nullRanks,
                    mpProtein.getSequence()
            ) + System.lineSeparator());
        }
        writer.close();
    }

    /**
     * @param mpProteins
     * @param rankThreshold
     * @return
     */
    public static Map<Taxon, Double> calTaxonDistribution(Collection<MPProtein> mpProteins,
                                                          Taxon.Rank rankThreshold) {
        Map<Integer, List<MPProtein>> tid2Proteins = mpProteins.stream()
                .filter(prot -> prot.getTaxon() != null && prot.getTaxon().existsTaxonOnRank(rankThreshold))
                .collect(Collectors.groupingBy(prot ->
                        prot.getTaxon().getLineageIdMap().get(rankThreshold.rankToString()))
                );
        Map<Taxon, Double> taxon2Dist = tid2Proteins.entrySet().stream()
                .collect(Collectors.toMap(
                        e -> taxonSearch.getTaxonById(e.getKey()),
                        e -> e.getValue().stream().mapToDouble(MPProtein::getPsmCount).sum()
                ));
        double total = taxon2Dist.values().stream().mapToDouble(Double::doubleValue).sum();
        taxon2Dist.keySet().forEach(taxon ->
                taxon2Dist.put(taxon, 100 * taxon2Dist.get(taxon) / total));
        return taxon2Dist;
    }
}
