package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.utils.CommandExecutor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created by huangjs
 * <p>
 * Perform fido algorithm by software supported by https://noble.gs.washington.edu/proj/fido/
 */

@Setter
@Getter
@NoArgsConstructor
@Log4j2
public class FidoAlgorithm {

    private String fidoLocation = GlobalConfig.getValue("fido");

    // TODO consider other params
    public void run(int accuraryLevel, Path graphPath, Path targetDecoyPath, Path fidoOutputPath) {
        String command = fidoLocation + " -c " + accuraryLevel
                + " " + graphPath.toString()
                + " " + targetDecoyPath.toString();

        Consumer<InputStream> outConsumer = (InputStream in) -> {
            try (
                    BufferedWriter bw = Files.newBufferedWriter(fidoOutputPath, StandardCharsets.UTF_8,
                            StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                    BufferedReader br = new BufferedReader(new InputStreamReader(in))
            ) {
                String line;
                while ((line = br.readLine()) != null) {
                    bw.write(line + System.lineSeparator());
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        };
        CommandExecutor.exec(command, outConsumer);

    }

    /**
     * 1. Merge the equivalent proteins: record the equivalent proteins into field `altProteinSet`
     * and delete those protein form mpProteinSet.
     * <p>
     * 2. Extract probability
     *
     * @param fidoOutputPath
     * @param mpProteinSet
     * @throws IOException
     */
    public static void parseFidoResult(Path fidoOutputPath,
                                       Set<MPProtein> mpProteinSet) throws IOException {
        // index
        Map<String, MPProtein> pid2Prot = mpProteinSet.parallelStream()
                .collect(Collectors.toMap(
                        mpProt -> mpProt.getId(),
                        mpProt -> mpProt
                ));
        // extract equivalent proteins containing same peptides
        List<MPProtein> mpProteinList = new ArrayList<>(mpProteinSet);
        Map<MPProtein, MPProtein> prot2AltProt = new HashMap<>(mpProteinSet.size());
        for (int i = 0; i < mpProteinList.size() - 1; i++) {
            MPProtein prot1 = mpProteinList.get(i);
            if (!prot2AltProt.containsKey(prot1)) {
                for (int j = i + 1; j < mpProteinList.size(); j++) {
                    MPProtein prot2 = mpProteinList.get(j);
                    if (prot1.getPeptideCount() == prot2.getPeptideCount()
                            && prot1.getContainedPeptideSet().containsAll(prot2.getContainedPeptideSet())) {
                        prot2AltProt.put(prot2, prot1);
                    }
                }
            }
        }
        // obtain probability
        BufferedReader br = Files.newBufferedReader(fidoOutputPath);
        String line;
        while ((line = br.readLine()) != null) {
            int startIdx = line.indexOf("{");
            int endIdx = line.lastIndexOf("}");
            if (startIdx != -1 && endIdx != -1) {
                // probability
                double prob = Double.parseDouble(line.substring(0, startIdx).trim());
                // protein
                String[] pidArr = line.substring(startIdx + 1, endIdx).split(","); // trim is must
                MPProtein mpProtein = pid2Prot.get(pidArr[0].trim());
                mpProtein.setProb(prob);
                for (String pid : pidArr) {
                    pid = pid.trim();
                    if (prot2AltProt.containsKey(pid2Prot.get(pid))) {
                        MPProtein altMPProt = pid2Prot.get(pid);
                        MPProtein mainMPProt = prot2AltProt.get(altMPProt);
                        // set alternative proteins for main protein
                        if (mainMPProt.getAltProteinSet() == null) {
                            mainMPProt.setAltProteinSet(new HashSet<>());
                        }
                        mainMPProt.getAltProteinSet().add(pid);
                        // delete alternative proteins from mpProteinSet and mpPeptideSet
                        for (MPPeptide mpPept : altMPProt.getContainedPeptideSet()) {
                            mpPept.getAssociatedProteinSet().remove(altMPProt);
                        }
                        mpProteinSet.remove(altMPProt);
                    } else {
                        pid2Prot.get(pid).setProb(prob);
                    }
                }
            } else {
                log.warn("abnormal fido result line: {}", line);
            }
        }
        br.close();
    }
}
