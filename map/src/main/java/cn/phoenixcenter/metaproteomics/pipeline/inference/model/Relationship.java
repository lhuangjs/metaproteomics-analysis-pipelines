package cn.phoenixcenter.metaproteomics.pipeline.inference.model;

public enum Relationship {
    SUPER_SUBSET,
    SUBSET_SUPER,
    // differentiable suggest that the two protein is differentiable, so the two protein
    // may or may not have the shared peptide.
    DIFFERENTIABLE,
    EQUIVALENT
}
