package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by huangjs
 */
@Getter
@Setter
@NoArgsConstructor
public class ProteinNode extends Node {

    private Counter counter;

    // posterior error probability
    private double posteriorErrorProb;

    private ProteinType proteinType;

    public enum ProteinType{
        TARGET,
        DECOY,
        UNCERTAINTY
    }

    public ProteinNode(String id) {
        super(id);
    }

    @Override
    public String toString() {
        return "ProteinNode{" +
                "id='" + id + '\'' +
                ", counter=" + counter +
                ", posteriorErrorProb=" + posteriorErrorProb +
                ", proteinType=" + proteinType +
                '}';
    }
}
