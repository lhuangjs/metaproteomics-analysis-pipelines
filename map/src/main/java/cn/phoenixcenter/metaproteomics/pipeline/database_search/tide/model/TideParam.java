package cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.model;


import cn.phoenixcenter.metaproteomics.base.Param;
import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
@Setter
@NoArgsConstructor
public class TideParam {
    /*************************************************************/
    /*************************  Global  **************************/
    /*************************************************************/
    // The name of the directory where output files will be created.
    private String output_dir;

    // Set the precision for masses and m/z written to sqt and text files.
    private int mass_precision = 4;

    // The fileroot string will be added as a prefix to all output file names.
    @Param(paramName = "fileroot",
            paramDesc = "The fileroot string will be added as a prefix to all output file names.")
    private String fileroot;

    /*************************************************************/
    /*************************  Custom  **************************/
    /*************************************************************/
//    // The name of the database used in database search
//    private String databaseName;
//
//    // The fasta file location
//    private Path fastaPath;

    // The param is different with max-mods.
    @Param(paramName = "max variable modifications of each type per peptide",
            paramAlias = "Max. variable modifications of each type per peptide",
            paramDesc = "The maximum number of variable modifications of each type allowed on a single peptide.",
            nativeParam = false)
    private int maxVariablePtmsPerTypePerPeptide = 2;

    private final String mods = GlobalConfig.getValue("modifications");

    @Param(paramName = "fixed modification",
            paramAlias = "Fixed modification",
            paramDesc = "fixed modification",
            nativeParam = false,
            candidates = {
                    "Acetylation of K",
                    "Acetylation of protein N-term",
                    "Carbamidomethylation of C",
                    "Deamidation of N",
                    "Deamidation of Q",
                    "Oxidation of M",
                    "Phosphorylation of S",
                    "Phosphorylation of T",
                    "Phosphorylation of Y",
                    "Pyrolidone from E",
                    "TMT 10-plex of K",
                    "TMT 10-plex of peptide N-term",
                    "TMT 6-plex of K",
                    "TMT 6-plex of peptide N-term",
                    "iTRAQ 4-plex of K",
                    "iTRAQ 4-plex of Y",
                    "iTRAQ 4-plex of peptide N-term",
                    "iTRAQ 8-plex of K",
                    "iTRAQ 8-plex of Y",
                    "iTRAQ 8-plex of peptide N-term"
            })
    private List<String> fixedModList = Stream.of("Carbamidomethylation of C")
            .collect(Collectors.toList());

    @Param(paramName = "variable modification",
            paramAlias = "variable modification",
            paramDesc = "variable modification",
            nativeParam = false,
            candidates = {
                    "Acetylation of K",
                    "Acetylation of protein N-term",
                    "Carbamidomethylation of C",
                    "Deamidation of N",
                    "Deamidation of Q",
                    "Oxidation of M",
                    "Phosphorylation of S",
                    "Phosphorylation of T",
                    "Phosphorylation of Y",
                    "Pyrolidone from E",
                    "TMT 10-plex of K",
                    "TMT 10-plex of peptide N-term",
                    "TMT 6-plex of K",
                    "TMT 6-plex of peptide N-term",
                    "iTRAQ 4-plex of K",
                    "iTRAQ 4-plex of Y",
                    "iTRAQ 4-plex of peptide N-term",
                    "iTRAQ 8-plex of K",
                    "iTRAQ 8-plex of Y",
                    "iTRAQ 8-plex of peptide N-term"
            })
    private List<String> variableModList = Stream.of("Acetylation of protein N-term",
            "Oxidation of M")
            .collect(Collectors.toList());

    /*************************************************************/
    /*************************  Tide-Index  **********************/
    /*************************************************************/

    /**************************  modification  *******************/
    @Param(paramName = "clip-nterm-methionine",
            paramAlias = "Clip N-terminal Met",
            paramDesc = "When set to true, for each protein that begins with methionine, tide-index " +
                    "will put two copies of the leading peptide into the index, with and without " +
                    "the N-terminal methionine. Default = false.",
            booleanType = true)
    private boolean clipNtermMethionine = false;

    @Param(paramName = "nterm-peptide-mods-spec",
            paramDesc = "Specify a comma-separated list of N-terminal static and variable " +
                    "mass modifications on peptides")
    private String ntermPeptideModsSpec;

    @Param(paramName = "cterm-peptide-mods-spec",
            paramDesc = "Specify a comma-separated list of C-terminal static and variable " +
                    "mass modifications on peptides")
    private String ctermPeptideModsSpec;

    @Param(paramName = "mods-spec",
            paramDesc = "Expression for static and variable mass modifications to include. Specify a " +
                    "comma-separated list of modification sequences of the form: " +
                    "C+57.02146,2M+15.9949,1STY+79.966331,...")
    private String modsSpec;

    @Param(paramName = "min-mods",
            paramDesc = "The minimum number of modifications that can be applied to a single peptide.")
    private int midMods = 0;

    @Param(paramName = "max-mods",
            paramDesc = "The maximum number of modifications that can be applied to a single peptide.")
    private int maxMods = 255;

    @Param(paramName = "isotopic-mass",
            paramAlias = "Isotopic mass",
            paramDesc = "Specify the type of isotopic masses to use when calculating the peptide " +
                    "mass.",
            candidates = {"average", "mono"})
    private String isotopicMass = "mono";


    /**************************  peptide  ************************/
    @Param(paramName = "decoy-prefix",
            paramDesc = "Specifies the prefix of the protein names that indicate a decoy.")
    private String decoyPrefix = "decoy_";

    @Param(paramName = "decoy-format",
            paramAlias = "Decoy format",
            paramDesc = "Include a decoy version of every peptide by shuffling or reversing the " +
                    "target sequence or protein. In shuffle or peptide-reverse mode, each " +
                    "peptide is either reversed or shuffled, leaving the N-terminal and " +
                    "C-terminal amino acids in place. Note that peptides appear multiple times " +
                    "in the target database are only shuffled once. In peptide-reverse mode, " +
                    "palindromic peptides are shuffled. Also, if a shuffled peptide produces an " +
                    "overlap with the target or decoy database, then the peptide is re-shuffled " +
                    "up to 5 times. Note that, despite this repeated shuffling, homopolymers " +
                    "will appear in both the target and decoy database. The protein-reverse mode " +
                    "reverses the entire protein sequence, irrespective of the composite " +
                    "peptides.",
            candidates = {"shuffle", "peptide-reverse", "protein-reverse"})
    private String decoyFormat = "protein-reverse";

    @Param(paramName = "keep-terminal-aminos",
            paramDesc = "When creating decoy peptides using decoy-format=shuffle or " +
                    "decoy-format=peptide-reverse, this option specifies whether the N-terminal " +
                    "and C-terminal amino acids are kept in place or allowed to be shuffled or " +
                    "reversed. For a target peptide 'EAMPK' with decoy-format=peptide-reverse, " +
                    "setting keep-terminal-aminos to 'NC' will yield 'EPMAK'; setting it to 'C' " +
                    "will yield 'PMAEK'; setting it to 'N' will yield 'EKPMA'; and setting it to " +
                    "'none' will yield 'KPMAE'",
            candidates = {"N", "C", "NC", "none"})
    private String keepTerminalAminos = "NC";

    @Param(paramName = "enzyme",
            paramAlias = "Enzyme",
            paramDesc = "Specify the enzyme used to digest the proteins in silica.",
            candidates = {"trypsin", "trypsin/p", "chymotrypsin", "elastase", "clostripain", "cyanogen-bromide",
                    "iodosobenzoate", "proline-endopeptidase", "staph-protease", "asp-n", "lys-c", "lys-n",
                    "arg-c", "glu-c", "pepsin-a", "elastase-trypsin-chymotrypsin"})
    private String enzyme = "trypsin";

    @Param(paramName = "digestion",
            paramAlias = "Digestion mode",
            paramDesc = "Specify whether every peptide in the database must have two enzymatic " +
                    "termini (full-digest) or if peptides with only one enzymatic terminus are " +
                    "also included (partial-digest).",
            candidates = {"full-digest", "partial-digest", "non-specific-digest"})
    private String digestion = "full-digest";

    @Param(paramName = "min-length",
            paramAlias = "Min. peptide length",
            paramDesc = "The minimum length of peptides to consider.")
    private int minLength = 6;

    @Param(paramName = "max-length",
            paramAlias = "Max. peptide length",
            paramDesc = "The maximum length of peptides to consider.")
    private int maxLength = 50;

    @Param(paramName = "missed-cleavages",
            paramAlias = "Max. missed cleavages",
            paramDesc = "Maximum number of missed cleavages per peptide to allow in enzymatic " +
                    "digestion.")
    private int missedCleavages = 2;

    /*************************************************************/
    /************************  Tide-Search  **********************/
    /*************************************************************/
    @Param(paramName = "precursor-window",
            paramAlias = "Precursor m/z tolerance",
            paramDesc = "Tolerance used for matching peptides to spectra. Peptides must be within " +
                    "+/- 'precursor-window' of the spectrum value. The precursor window units " +
                    "depend upon precursor-window-type.")
    private double precursorWindow = 10.0;

    @Param(paramName = "precursor-window-type",
            paramAlias = "Precursor m/z tolerance unit",
            paramDesc = "Specify the units for the window that is used to select peptides around the " +
                    "precursor mass location (mass, mz, ppm). The magnitude of the window is " +
                    "defined by the precursor-window option, and candidate peptides must fall " +
                    "within this window. For the mass window-type, the spectrum precursor m+h " +
                    "value is converted to mass, and the window is defined as that mass +/- " +
                    "precursor-window. If the m+h value is not available, then the mass is " +
                    "calculated from the precursor m/z and provided charge. The peptide mass is " +
                    "computed as the sum of the average amino acid masses plus 18 Da for the " +
                    "terminal OH group. The mz window-type calculates the window as spectrum " +
                    "precursor m/z +/- precursor-window and then converts the resulting m/z " +
                    "range to the peptide mass range using the precursor charge. For the " +
                    "parts-per-million (ppm) window-type, the spectrum mass is calculated as in " +
                    "the mass type. The lower bound of the mass window is then defined as the " +
                    "spectrum mass / (1.0 + (precursor-window / 1000000)) and the upper bound is " +
                    "defined as spectrum mass / (1.0 - (precursor-window / 1000000)).",
            candidates = {"ppm", "mass", "mz"})
    private String precursorWindowType = "ppm";

    @Param(paramName = "fragment-tolerance",
            paramAlias = "Fragment m/z tolerance",
            paramDesc = "Mass tolerance (in Da) for scoring pairs of peaks when creating the residue " +
                    "evidence matrix. This parameter only makes sense when score-function is " +
                    "'residue-evidence' or 'both'.")
    private double fragmentTolerance = 0.5;

    @Param(paramName = "compute-sp",
            paramDesc = "Compute the preliminary score Sp for all candidate peptides. Report this " +
                    "score in the output, along with the corresponding rank, the number of " +
                    "matched ions and the total number of ions. This option is recommended if " +
                    "results are to be analyzed by Percolator or Barista. If sqt-output is " +
                    "enabled, then compute-sp is automatically enabled and cannot be overridden. " +
                    "Note that the Sp computation requires re-processing each observed spectrum, " +
                    "so turning on this switch involves significant computational overhead. " +
                    "Default = true.",
            booleanType = true)
    private boolean computeSp = true;

    @Param(paramName = "spectrum-charge",
            paramAlias = "Precursor charge",
            paramDesc = "The spectrum charges to search. With 'all' every spectrum will be searched " +
                    "and spectra with multiple charge states will be searched once at each " +
                    "charge state. With 1, 2, or 3 only spectra with that charge state will be " +
                    "searched.",
            candidates = {"1", "2", "3", "all"})
    private String spectrumCharge = "all";

    /**************************  output  *************************/
    @Param(paramName = "pepxml-output",
            paramDesc = "Output a pepXML results file to the output directory.",
            booleanType = true)
    private boolean pepxmlOutput = true;

    @Param(paramName = "pin-output",
            paramDesc = "Output a Percolator input (PIN) file to the output directory.",
            booleanType = true)
    private boolean pinOutput = true;

    @Param(paramName = "sqt-output",
            paramDesc = "Outputs an SQT results file to the output directory. " +
                    "Note that if sqt-output is enabled, then compute-sp is automatically " +
                    "enabled and cannot be overridden.",
            booleanType = true)
    private boolean sqtOutput = true;

    @Param(paramName = "concat",
            paramDesc = "When set to T, target and decoy search results are reported in a single " +
                    "file, and only the top-scoring N matches (as specified via --top-match) are " +
                    "reported for each spectrum, irrespective of whether the matches involve " +
                    "target or decoy peptides.Note that when used with search-for-xlinks, this " +
                    "parameter only has an effect if use-old-xlink=F.",
            booleanType = true)
    private boolean concat = true;

    @Param(paramName = "mod-precision",
            paramDesc = "Set the precision for modifications as written to .txt files.")
    private int modPrecision = 5;

    @Param(paramName = "num-threads",
            paramDesc = "0=poll CPU to set num threads; else specify num threads directly.")
    private int numThreads = 0;
}
