package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.bipartite_graph;

import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
public class BipartiteGraph<S, R> {
    private Map<S, List<R>> vertexCollectionA;
    private Map<R, List<S>> vertexCollectionB;

    public BipartiteGraph(Map<S, List<R>> vertexCollectionA, Map<R, List<S>> vertexCollectionB) {
        this.vertexCollectionA = vertexCollectionA;
        this.vertexCollectionB = vertexCollectionB;
    }

    /**
     * Add edge between vertex1 and vertex2.
     *
     * @param vertex1 the vertex1 derive from vertex collection A
     * @param vertex2 the vertex2 derive from vertex collection B
     */
    public void addEdge(S vertex1, R vertex2) {
        if (!vertexCollectionA.containsKey(vertex1)) {
            vertexCollectionA.put(vertex1, new ArrayList<>());
        }
        vertexCollectionA.get(vertex1).add(vertex2);
        if (!vertexCollectionB.containsKey(vertex2)) {
            vertexCollectionB.put(vertex2, new ArrayList<>());
        }
        vertexCollectionB.get(vertex2).add(vertex1);
    }

    public int getDegreeInA(S vertex1) {
        return this.vertexCollectionA.get(vertex1).size();
    }

    public int getDegreeInB(R vertex2) {
        return this.vertexCollectionB.get(vertex2).size();
    }

    /**
     * Remove vertex from A
     *
     * @param vertex1
     * @return
     */
    public boolean removeVertexFromA(S vertex1) {
        boolean result = true;
        // remove the vertex1 in collection B
        for (R vertex2 : vertexCollectionA.get(vertex1)) {
            result = result && (vertexCollectionB.get(vertex2).remove(vertex1));
            // the vertex2 will be deleted from B if the node does not link other nodes(HashMap.values size == 0)
            // after remove vertex1
            if (vertexCollectionB.get(vertex2).size() == 0) {
                vertexCollectionB.remove(vertex2);
            }
        }
        result = result && (vertexCollectionA.remove(vertex1) != null);
        return result;
    }

    public boolean removeVertexFromB(R vertex2) {
        boolean result = true;
        for (S vertex1 : vertexCollectionB.get(vertex2)) {
            result = result && (vertexCollectionA.get(vertex1).remove(vertex2));
            if (vertexCollectionA.get(vertex1).size() == 0) {
                vertexCollectionA.remove(vertex1);
            }
        }
        result = result && (vertexCollectionB.remove(vertex2) != null);
        return result;
    }

//    /**
//     * Remove edge between vertex1 and vertex2 from A and B.
//     *
//     * @param vertex1 the vertex1 derive from vertex collection A
//     * @param vertex2 the vertex2 derive from vertex collection B
//     */
//    public void removeEdge(S vertex1, R vertex2) {
//        removeEdgeFromA(vertex1, vertex2);
//        removeEdgeFromB(vertex1, vertex2);
//    }
//
//    /**
//     * Remove edge between vertex1 and vertex2 from vertex collection A.
//     *
//     * @param vertex1 the vertex1 derive from vertex collection A
//     * @param vertex2 the vertex2 derive from vertex collection B
//     */
//    private void removeEdgeFromA(S vertex1, R vertex2) {
//        if (vertexCollectionA.containsKey(vertex1)) {
//            vertexCollectionA.get(vertex1).remove(vertex2);
//        }
//    }
//
//    /**
//     * Remove edge between vertex1 and vertex2 from vertex collection B.
//     *
//     * @param vertex1 the vertex1 derive from vertex collection A
//     * @param vertex2 the vertex2 derive from vertex collection B
//     */
//    private void removeEdgeFromB(S vertex1, R vertex2) {
//        if (vertexCollectionB.containsKey(vertex2)) {
//            vertexCollectionB.get(vertex2).remove(vertex1);
//        }
//    }
//
//    /**
//     * Remove vertex from vertex collection B.
//     *
//     * @param vertex2
//     */
//    public void removeVertexFromB(R vertex2) {
//        if (vertexCollectionB.containsKey(vertex2)) {
//            // remove vertex2 from vertex collection A
//            vertexCollectionB.get(vertex2).stream()
//                    .forEach((S vertex1) -> removeEdgeFromA(vertex1, vertex2));
//            // remove vertex2 from vertex collection B
//            vertexCollectionB.remove(vertex2);
//        }
//    }
//
//    /**
//     * Collapse the two vertices derive from vertex collection A.
//     * <p>
//     * Note: the two vertices collapsed are linked to the same vertices in vertex collection B
//     *
//     * @param vertex1 the vertex1 derive from vertices collection A
//     * @param vertex2 the vertex2 derive from vertices collection A
//     * @return true if the operation process successful, otherwise false
//     */
//    public boolean collapseVerticesInA(S vertex1, S vertex2, S newVertex) {
//        if (vertexCollectionA.containsKey(vertex1)
//                && vertexCollectionA.containsKey(vertex2)) {
//            //
//            List<R> value = vertexCollectionA.get(vertex1);
//            vertexCollectionA.put(newVertex, value);
//            vertexCollectionA.remove(vertex1);
//            vertexCollectionA.remove(vertex2);
//            value.stream().forEach((R r) -> vertexCollectionB.get(r)
//                    .set(vertexCollectionB.get(r).indexOf(vertex1), newVertex));
//            return true;
//        }
//        return false;
//    }

    @Override
    public String toString() {
        String collectionA = "## Collection A ##" + System.lineSeparator() +
                vertexCollectionA.keySet().stream()
                        .map((S s) -> s + "=>" +
                                vertexCollectionA.get(s).stream()
                                        .map((R r) -> r.toString())
                                        .collect(Collectors.joining(","))
                        ).collect(Collectors.joining(System.lineSeparator()));

        String collectionB = "## Collection B ##" + System.lineSeparator() +
                vertexCollectionB.keySet().stream()
                        .map((R r) -> r + "=>" +
                                vertexCollectionB.get(r).stream()
                                        .map((S s) -> s.toString())
                                        .collect(Collectors.joining(","))
                        ).collect(Collectors.joining(System.lineSeparator()));
        return String.join(System.lineSeparator(), collectionA, collectionB);
    }
}
