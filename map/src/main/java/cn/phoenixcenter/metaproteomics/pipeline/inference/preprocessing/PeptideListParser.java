package cn.phoenixcenter.metaproteomics.pipeline.inference.preprocessing;

import cn.phoenixcenter.metaproteomics.pipeline.inference.model.Peptide;
import cn.phoenixcenter.metaproteomics.pipeline.inference.model.Protein;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * The class is used to read multiple kinds peptide list file and
 * transform text into Object Protein and Peptide.
 * These attributes need be added:
 * <PRE>
 * 1. Peptide.sequence, Peptide.associatedProteinSet
 * 2. Protein.id, Protein.containedPeptideSet
 * </PRE>
 */
@Getter
@NoArgsConstructor
public class PeptideListParser {

    // non-redundant peptide set
    private Set<Peptide> peptideSet;

    // non-redundant protein set
    private Set<Protein> proteinSet;

    /**
     * Read peptides list file and transform text into Object Protein and Peptide.
     * The file has two columns: peptide sequence and proteins list that contained the peptide
     * and they are separated tab, looks like
     * <PRE>
     * ATSCAAA	protein1,protein2
     * ATSRTAA	protein1,protein3
     * </PRE>
     *
     * @param inputFile input file path
     * @return
     * @throws IOException
     */
    public void parsePep2Pros(String inputFile) throws IOException {
        this.peptideSet = new HashSet<>();
        Map<String, Protein> id2ProteinMap = new HashMap<>();

        BufferedReader br = new BufferedReader(new FileReader(inputFile));
        String line = null;
        while ((line = br.readLine()) != null) {
            String[] cols = Arrays.stream(line.split("\t"))
                    .map(String::trim)
                    .toArray(String[]::new);
            String[] proIds = Arrays.stream(cols[1].split(",")).map(String::trim).toArray(String[]::new);
            /** organize Peptide and Protein object **/
            Peptide peptide = new Peptide();
            peptide.setSequence(cols[0]);
            // the protein set associated with the peptide
            Set<Protein> associatedProtein = new HashSet<>();
            for (String p : proIds) {
                Protein protein;
                if (id2ProteinMap.containsKey(p)) {
                    protein = id2ProteinMap.get(p);
                } else {
                    protein = new Protein();
                    protein.setId(p);
                    protein.setContainedPeptideSet(new HashSet<>());
                    id2ProteinMap.put(p, protein);
                }
                protein.getContainedPeptideSet().add(peptide);
                associatedProtein.add(protein);
            }
            // add associated proteins for the peptide: those proteins contained the peptide
            peptide.setAssociatedProteinSet(associatedProtein);
            peptideSet.add(peptide);
        }
        br.close();
        this.proteinSet = new HashSet<>(id2ProteinMap.values());
    }

    /**
     * The class is used to read multiple kinds peptide list file and
     * transform text into Object Protein and Peptide.
     * <p/>
     * The peptide list file looks like:
     * <PRE>
     * peptide1	protein1
     * peptide1	protein2
     * peptide2	protein2
     * </PRE>
     * Each peptide and one of protein containing this peptide are recorded one line.
     *
     * @param inputFile input file path
     * @throws IOException
     */
    public void parsePep2Pro(String inputFile) throws IOException {
        this.peptideSet = new HashSet<>();
        Map<String, Peptide> seq2PepMap = new HashMap<>();
        Map<String, Protein> id2ProteinMap = new HashMap<>();

        BufferedReader br = new BufferedReader(new FileReader(inputFile));
        String line = null;
        while ((line = br.readLine()) != null) {
            String[] cols = Arrays.stream(line.split("\t"))
                    .map(String::trim)
                    .toArray(String[]::new);

            // organize Peptide and Protein object.
            Peptide peptide;
            Protein protein;
            if (seq2PepMap.containsKey(cols[0])) {
                peptide = seq2PepMap.get(cols[0]);
            } else {
                peptide = new Peptide();
                peptide.setSequence(cols[0]);
                peptide.setAssociatedProteinSet(new HashSet<>());
                seq2PepMap.put(cols[0], peptide);
            }
            if (id2ProteinMap.containsKey(cols[1])) {
                protein = id2ProteinMap.get(cols[1]);
            } else {
                protein = new Protein(cols[1]);
                protein.setContainedPeptideSet(new HashSet<>());
                id2ProteinMap.put(cols[1], protein);
            }
            // add the associated protein for the peptide: the protein contained the peptide.
            peptide.getAssociatedProteinSet().add(protein);
            // add peptide for the protein.
            protein.getContainedPeptideSet().add(peptide);
        }
        br.close();
        this.peptideSet = new HashSet<>(seq2PepMap.values());
        this.proteinSet = new HashSet<>(id2ProteinMap.values());
    }

    /**
     * Obtain proteins matching the input peptides from fasta library.
     *
     * @param equateIL
     * @param pepSet
     * @param libraryFile
     * @throws IOException
     */
    public void parsePep2Library(boolean equateIL, Set<String> pepSet, String libraryFile) throws IOException {
        Pattern trypsinPat = Pattern.compile("(?<=[KR])(?!P)");
        Pattern proIdPat = Pattern.compile(">(.*?)\\s");
        Set<String> noMissedCleavagePepSet = Collections.synchronizedSet(new HashSet<>());
        Set<String> existMissedCleavagePepSet = Collections.synchronizedSet(new HashSet<>());
        this.proteinSet = new HashSet<>();
        if (equateIL) {
            pepSet = pepSet.parallelStream()
                    .map(seq -> seq.replace("I", "L"))
                    .collect(Collectors.toSet());
        }
        Map<String, Peptide> seq2PepMap = pepSet.parallelStream()
                // classify peptides as two types: one exists missed cleavage and other dose not exist
                .peek(seq -> {
                    if (trypsinPat.split(seq).length > 1) {
                        existMissedCleavagePepSet.add(seq);
                    } else {
                        noMissedCleavagePepSet.add(seq);
                    }
                })
                .map(seq -> {
                    Peptide peptide = new Peptide(seq);
                    peptide.setAssociatedProteinSet(new HashSet<>());
                    return peptide;
                })
                .collect(Collectors.toMap(
                        peptide -> peptide.getSequence(),
                        peptide -> peptide
                ));
        this.peptideSet = new HashSet<>(seq2PepMap.values());

        /** read library: the string between ">" and the first space is set protein id **/
        BufferedReader br = new BufferedReader(new FileReader(libraryFile));
        String line;
        Protein protein = null;
        String proSeq = "";
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">")) {
                if (protein != null) {
                    // deal with peptides without missed cleavage: split protein sequence (trypsin)
                    for (String pepSeq : trypsinPat.split(proSeq)) {
                        if (noMissedCleavagePepSet.contains(pepSeq)) {
                            // add protein for the peptide
                            seq2PepMap.get(pepSeq).getAssociatedProteinSet().add(protein);
                            // add peptide for the protein
                            protein.getContainedPeptideSet().add(seq2PepMap.get(pepSeq));
                        }
                    }
                    // deal with peptides with missed cleavage
                    for (String pepSeq : existMissedCleavagePepSet) {
                        if (proSeq.indexOf(pepSeq) != -1) {
                            seq2PepMap.get(pepSeq).getAssociatedProteinSet().add(protein);
                            protein.getContainedPeptideSet().add(seq2PepMap.get(pepSeq));
                        }
                    }
                    // initialize
                    proSeq = "";
                }
                // create Protein object
                protein = new Protein();
                Matcher m = proIdPat.matcher(line);
                if(m.find()){
                    protein.setId(m.group(1));
                }else{
                    protein.setId(line.substring(1));
                }
                protein.setContainedPeptideSet(new HashSet<>());
                this.proteinSet.add(protein);
            } else {
                if (equateIL) {
                    proSeq += line.replace("I", "L");
                } else {
                    proSeq += line;
                }
            }
        }
        br.close();
    }
}
