package cn.phoenixcenter.metaproteomics.pipeline.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Map;

public class CloneUtil {

    public static <S> S deepCopy(S source) {
        IdentityHashMap<Object, Object> source2Clone = new IdentityHashMap<>();
        return (S) deepCopy(source, source2Clone);
    }

    private static Object deepCopy(Object source, IdentityHashMap<Object, Object> source2Clone) {
        if (source2Clone.containsKey(source)) {
            return source2Clone.get(source);
        } else if (source == null) {
            return null;
        } else if (source instanceof Number) {
            Number num = (Number) source;
            if (source instanceof Double) {
                return new Double(num.doubleValue());
            } else if (source instanceof Integer) {
                return new Integer(num.intValue());
            } else {
                throw new UnsupportedOperationException(source.getClass() + ": need to be added");
            }
        } else if (source instanceof Boolean) {
            return new Boolean((Boolean) source).booleanValue();
        } else if (source instanceof String) {
            return new String((String) source);
        }else if(source instanceof Enum){
            return source;
        }
        Object clonedObj = null;
        try {
            Class clazz = source.getClass();
            clonedObj = clazz.getDeclaredConstructor().newInstance();
            source2Clone.put(source, clonedObj);
            Field[] fields = source.getClass().getDeclaredFields();
            for (Field f : fields) {
                f.setAccessible(true);
                Object fieldVal = f.get(source);
                if (source2Clone.containsKey(fieldVal)) {
                    f.set(clonedObj, source2Clone.get(fieldVal));
                } else if (f.getType().isPrimitive()) {
                    f.set(clonedObj, f.get(source));
                } else if (fieldVal instanceof String) {
                    f.set(clonedObj, new String((String) fieldVal));
                } else if (fieldVal instanceof Collection) {
                    Collection fieldValCopy = (Collection) (fieldVal.getClass().getDeclaredConstructor().newInstance());
                    source2Clone.put(fieldVal, fieldValCopy);
                    Iterator itr = ((Collection) fieldVal).iterator();
                    while (itr.hasNext()) {
                        fieldValCopy.add(deepCopy(itr.next(), source2Clone));
                    }
                    f.set(clonedObj, fieldValCopy);
                } else if (fieldVal instanceof Map) {
                    Map fieldValCopy = (Map) (fieldVal.getClass().getDeclaredConstructor().newInstance());
                    source2Clone.put(fieldVal, fieldValCopy);
                    Iterator<Map.Entry> itr = ((Map) fieldVal).entrySet().iterator();
                    while (itr.hasNext()) {
                        Map.Entry entry = itr.next();
                        Object key = deepCopy(entry.getKey(), source2Clone);
                        Object value = deepCopy(entry.getValue(), source2Clone);
                        fieldValCopy.put(key, value);
                    }
                    f.set(clonedObj, fieldValCopy);
                } else {
                    f.set(clonedObj, deepCopy(f.get(source), source2Clone));
                }
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            System.exit(255);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            System.exit(255);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            System.exit(255);
        } catch (InstantiationException e) {
            e.printStackTrace();
            System.exit(255);
        }
        return clonedObj;
    }
}

