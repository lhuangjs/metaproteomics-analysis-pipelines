package cn.phoenixcenter.metaproteomics.pipeline.inference.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ProteinWithRelationship extends Protein {

    // protein set contained at least one shared peptides with the protein
    private Set<ProteinWithRelationship> associatedProteinSet;

    // differentiable proteins set of the protein
    private Set<ProteinWithRelationship> differentiable;

    // superset of the protein
    private Set<ProteinWithRelationship> superset;

    // subset of the protein
    private Set<ProteinWithRelationship> subset;

    // equivalent proteins set of the protein
    private Set<ProteinWithRelationship> equivalent;

    public ProteinWithRelationship(String id) {
        super(id);
    }

    /**
     * Generate a ProteinWithRelationship instance from Protein instance.
     *
     * @param protein
     */
    public ProteinWithRelationship(Protein protein) {
        this.id = protein.getId();
        this.parsimonyType = protein.getParsimonyType();
        this.containedPeptideSet = protein.getContainedPeptideSet().stream().collect(Collectors.toSet());
    }

    /**
     * Clone a protein instance.
     *
     * @param protein
     */
    public ProteinWithRelationship(ProteinWithRelationship protein) {
        this.id = protein.getId();
        this.parsimonyType = protein.getParsimonyType();
        this.containedPeptideSet = protein.getContainedPeptideSet().stream().collect(Collectors.toSet());
        this.associatedProteinSet = protein.getAssociatedProteinSet().stream().collect(Collectors.toSet());
        this.differentiable = protein.getDifferentiable().stream().collect(Collectors.toSet());
        this.superset = protein.getSuperset().stream().collect(Collectors.toSet());
        this.subset = protein.getSubset().stream().collect(Collectors.toSet());
        this.equivalent = protein.getEquivalent().stream().collect(Collectors.toSet());
    }

    public void init() {
        this.associatedProteinSet = new HashSet<>();
        this.differentiable = new HashSet<>();
        this.superset = new HashSet<>();
        this.subset = new HashSet<>();
        this.equivalent = new HashSet<>();
    }

    /**
     * Add associated protein. However, the protein self need to be excluded before added.
     *
     * @param proteinSet
     */
    public void addAssociatedProteinSet(Set<ProteinWithRelationship> proteinSet) {
        this.associatedProteinSet.addAll(proteinSet);
        this.getAssociatedProteinSet().remove(this);
    }

    /**
     * obtain the number of the protein's peptides.
     *
     * @return
     */
    public int getPeptideCount() {
        return this.containedPeptideSet.size();
    }

    /**
     * judge whether the protein's peptides all belong to distinct peptide.
     *
     * @return
     */
    public boolean isAllDistinctPeptides() {
        return this.containedPeptideSet.stream()
                .allMatch((Peptide p) -> p.getParsimonyType() == ParsimonyType.DISTINCT);
    }

    /**
     * judge whether the protein's peptides all belong to shared peptide.
     *
     * @return
     */
    public boolean isAllSharedPeptides() {
        return this.containedPeptideSet.stream()
                .allMatch((Peptide p) -> p.getParsimonyType() == ParsimonyType.SHARED);
    }

    /**
     * judge whether the protein's peptides contain at least one distinct peptide.
     *
     * @return
     */
    public boolean isContainDistinctPeptide() {
        return this.containedPeptideSet.stream()
                .anyMatch((Peptide p) -> p.getParsimonyType() == ParsimonyType.DISTINCT);
    }

    public String fPrint() {
        String parsimonyStr = parsimonyType == null ? "null" : parsimonyType.toString();
        String peptidesStr = containedPeptideSet.stream()
                .map(Peptide::getSequence)
                .sorted((String s1, String s2) -> s1.compareToIgnoreCase(s2))
                .collect(Collectors.joining(","));
        String equivalentStr = combineStr(equivalent);
        String differentiableStr = combineStr(differentiable);
        String supersetStr = combineStr(superset);
        String subsetStr = combineStr(subset);
//        String associatedProteinSetStr = combineStr(associatedProteinSet);
//        return String.join("\t", this.id, parsimonyStr, peptidesStr, equivalentStr,
//                differentiableStr, supersetStr, subsetStr, associatedProteinSetStr);
        return String.join("\t", this.id, parsimonyStr, peptidesStr, equivalentStr,
                differentiableStr, supersetStr, subsetStr);

    }

    private String combineStr(Set<ProteinWithRelationship> proteinSet) {
        if (proteinSet.size() == 0) {
            return "null";
        } else {
            return proteinSet.stream()
                    .map(ProteinWithRelationship::getId)
                    .sorted((String s1, String s2) -> s1.compareToIgnoreCase(s2))
                    .collect(Collectors.joining(","));
        }
    }
}
