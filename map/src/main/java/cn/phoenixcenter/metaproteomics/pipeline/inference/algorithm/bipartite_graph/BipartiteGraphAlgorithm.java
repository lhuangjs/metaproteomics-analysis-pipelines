package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.bipartite_graph;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Perform the bipartite graph algorithm to inference proteins.
 */
@NoArgsConstructor
@Log4j2
public class BipartiteGraphAlgorithm {

    private Set<MPProtein> visitedSet;

    public Set<MPProtein> run(Set<MPProtein> mpProteins) {
        /** build multiple independence sub bipartite graphs **/
        visitedSet = new HashSet<>(mpProteins.size());
        List<BipartiteGraph<MPProtein, MPPeptide>> subgraphList = new ArrayList<>();
        for (MPProtein mpProtein : mpProteins) {
            if (!visitedSet.contains(mpProtein)) {
                // the size is estimated value not the number of MPProtein node in bipartite graph
                int size = mpProtein.getAssociatedProteinSet().size();
                BipartiteGraph<MPProtein, MPPeptide> subgraph = new BipartiteGraph<>(
                        new HashMap<>(size),
                        new HashMap<>()
                );
                buildBipartiteGraph(mpProtein, subgraph);
                subgraphList.add(subgraph);
            }
        }
        visitedSet = null;
        log.debug("bipartite graph count: {}", subgraphList.size());

        /** infer proteins by greedy set cover algorithm **/
        return subgraphList.parallelStream()
                .flatMap(subgraph -> {
                    Set<MPProtein> candidateProteinSet = new HashSet<>();
                    doGreedySetCoverAlgorithm(subgraph, candidateProteinSet);
                    return candidateProteinSet.stream();
                })
                .collect(Collectors.toSet());
    }

    private void buildBipartiteGraph(MPProtein mpProtein,
                                     BipartiteGraph<MPProtein, MPPeptide> subgraph) {
        if (!visitedSet.contains(mpProtein)) {
            // set the node visited
            visitedSet.add(mpProtein);
            // add edge
            for (MPPeptide mpPepide : mpProtein.getContainedPeptideSet()) {
                subgraph.addEdge(mpProtein, mpPepide);
            }
            for (MPProtein assoProt : mpProtein.getAssociatedProteinSet()) {
                buildBipartiteGraph(assoProt, subgraph);
            }
        }
    }

    /**
     * Obtain candidate proteins using greedy cover algorithm.
     *
     * @param subgraph
     * @param candidateProteinSet
     */
    private void doGreedySetCoverAlgorithm(BipartiteGraph<MPProtein, MPPeptide> subgraph,
                                           Set<MPProtein> candidateProteinSet) {
        Optional<MPProtein> largestCoverNode = subgraph.getVertexCollectionA().keySet().parallelStream()
                .max(Comparator.comparingInt(prot -> subgraph.getDegreeInA(prot)));
        if (largestCoverNode.isPresent()) {
            // add candidate protein
            MPProtein candidate = largestCoverNode.get();
            candidateProteinSet.add(candidate);
            // remove the protein vertex and associated peptide vertices
            for (MPPeptide pept : subgraph.getVertexCollectionA().get(candidate).stream().collect(Collectors.toSet())) {
                subgraph.removeVertexFromB(pept);
            }
            doGreedySetCoverAlgorithm(subgraph, candidateProteinSet);
        }
    }
}
