package cn.phoenixcenter.metaproteomics.pipeline.function;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.utils.CommandExecutor;
import cn.phoenixcenter.metaproteomics.utils.FileReaderUtil;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Log4j2
public class COGAnnotation {

    private static final String diamondLocation = GlobalConfig.getValue("diamond");
    private static final String cogDBLocation = GlobalConfig.getValue("cogDB");

    private static Map<String, String> pid2COGId;
    private static Map<String, COG> cogId2COG;

    static {
        try {
            pid2COGId = extractProCOGIdMap(COGAnnotation.class
                    .getResource("/COG/cog2003-2014.csv")
                    .getPath());
            cogId2COG = parseCOGNode(COGAnnotation.class
                    .getResource("/COG/cognames2003-2014.tab")
                    .getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Read cog2003-2014.csv and extract mapping from peptide id to COG-id
     * <PRE>
     * comma-delimited
     * [domain-id],[genome-name],[peptide-id],[peptide-length],
     * [domain-start],[domain-end],[COG-id],[membership-class]
     * </PRE>
     *
     * @param cogFile
     * @return
     * @throws IOException
     */
    public static Map<String, String> extractProCOGIdMap(String cogFile) throws IOException {
        // peptide id in COG db=> COG id
        Map<String, String> pid2COGId = new HashMap<>();
        // read
        FileReaderUtil fr = new FileReaderUtil(cogFile, ",", false);
        Consumer<String> rowConsumer = (String line) -> {
            String[] tmp = line.split(",");
            pid2COGId.put(tmp[2].trim(), tmp[6].trim());
        };
        fr.read(rowConsumer);
        return pid2COGId;
    }

    /**
     * Read cogname2003-2014.tab file and parse as COG object
     * <PRE>
     * tab-delimited
     * [COG-id], [functional-class], [COG-annotation]
     * </PRE>
     *
     * @param cogFile
     * @return
     * @throws IOException
     */
    public static Map<String, COG> parseCOGNode(String cogFile) throws IOException {
        // COG id => COG object
        Map<String, COG> cogId2COG = new HashMap<>();
        FileReaderUtil fr = new FileReaderUtil(cogFile, "\t", true);
        Consumer<String> rowConsumer = (String line) -> {
            String[] tmp = line.split("\t");
            COG cog = new COG();
            cog.setId(tmp[0].trim());
            cog.setFunctionClass(tmp[1].trim());
            cog.setAnnotation(tmp[2].trim());
            cogId2COG.put(cog.getId(), cog);
        };
        fr.read(rowConsumer);
        return cogId2COG;
    }


    /**
     *
     * @param queryPidSet
     * @param protSeqFile
     * @param pidPattern
     * @return query protein id => COG
     * @throws IOException
     * @throws InterruptedException
     */
    public static Map<String, COG> annotate(Set<String> queryPidSet,
                                            String protSeqFile,
                                            Pattern pidPattern) throws IOException, InterruptedException {
        Path queryProtSeqPath = Files.createTempFile("query-peptide", ".fasta");
        Path alignmentPath = Files.createTempFile("alignment", ".tsv");
        log.debug("create temporary files: {}(query peptide sequences), {}(diamond alignment result file)",
                queryProtSeqPath, alignmentPath);
        extractSequence(queryPidSet, protSeqFile, pidPattern, queryProtSeqPath.toString());
        align(queryProtSeqPath.toString(), alignmentPath.toString());
        Map<String, COG> queryPid2COG = parseAlignmentResult(alignmentPath.toString());
        Files.delete(queryProtSeqPath);
        Files.delete(alignmentPath);
        log.debug("delete temporary files: {}(query peptide sequences), {}(diamond alignment result file)",
                queryProtSeqPath, alignmentPath);
        return queryPid2COG;
    }

    /**
     * Extract sequence by peptide id
     *
     * @param queryPidSet peptide id set need to be extracted
     * @param protSeqFile the location of fasta format sequence library file
     * @param pidPattern
     * @param outputFile
     * @throws IOException
     */
    public static void extractSequence(Set<String> queryPidSet,
                                       String protSeqFile,
                                       Pattern pidPattern,
                                       String outputFile) throws IOException {
        BufferedReader br = Files.newBufferedReader(Paths.get(protSeqFile), StandardCharsets.UTF_8);
        BufferedWriter bw = Files.newBufferedWriter(Paths.get(outputFile), StandardCharsets.UTF_8);
        String pid = null;
        String sequence = "";
        Matcher matcher = null;
        Set<String> pidSetBackup = queryPidSet.stream().collect(Collectors.toSet());
        String line;
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">")) {
                if (pid != null) {
                    pidSetBackup.remove(pid);
                    bw.write(">" + pid + System.lineSeparator() + sequence + System.lineSeparator());
                    sequence = "";
                }
                matcher = pidPattern.matcher(line);
                if (matcher.find()) {
                    pid = matcher.group(1);
                    if (!queryPidSet.contains(pid)) {
                        pid = null;
                    }
                } else {
                    pid = null;
                    log.warn("the line {} capture nothing");
                }
            } else {
                if (pid != null) {
                    sequence += line;
                }
            }
        }
        // process the last entry
        if (pid != null) {
            bw.write(">" + pid + System.lineSeparator() + sequence + System.lineSeparator());
        }
        br.close();
        bw.close();
        if (pidSetBackup.size() != 0) {
            // TODO exit?
            log.error("Cannot find sequence for those proteins: {}", pidSetBackup.stream()
                    .collect(Collectors.joining(", ")));
        }
    }

    /**
     * Align peptide sequences with COG db using diamond
     *
     * @param querySeqFile
     * @param outputFile
     * @throws IOException
     * @throws InterruptedException
     */
    public static void align(String querySeqFile, String outputFile) throws IOException, InterruptedException {
        // TODO make diamond params as input
        String command = String.join(" ", diamondLocation, "blastp -d",
                cogDBLocation, "-q", querySeqFile,
                "-b 0.2 -k 1 -o", outputFile);
        CommandExecutor.exec(command);
    }


    /**
     * <PRE>
     * tab-delimited
     * [qseqid], [sseqid], [pident], [length], [mismatch],
     * [gapopen], [qstart], [qend], [sstart], [send], [evalue], [bitscore]
     * </PRE>
     *
     * @param alignmentFile
     */
    public static Map<String, COG> parseAlignmentResult(String alignmentFile) throws IOException {
        // extract mapping from query sequence ids to subject sequence ids
        Map<String, String> queryPid2SubjectPid = new HashMap<>();
        FileReaderUtil fr = new FileReaderUtil(alignmentFile, "\t", false);
        //gi|29349220|ref|NP_812723.1|
        final Pattern pidPat = Pattern.compile("gi\\|(\\d+)\\|");
        Consumer<String> rowConsumer = (String line) -> {
            String[] tmp = line.split("\t");
            Matcher m = pidPat.matcher(tmp[1].trim());
            m.find();
            queryPid2SubjectPid.put(tmp[0].trim(), m.group(1));
        };
        fr.read(rowConsumer);
        // annotate query sequences by subject sequences id
        return queryPid2SubjectPid.entrySet().stream()
//                .peek(e -> System.out.println(e))
                .collect(HashMap::new,
                        (map, entry) -> map.put(entry.getKey(), convertPid2COG(entry.getValue())),
                        HashMap::putAll);
    }


    /**
     * peptide id in COG db => COG id => COG
     *
     * @param protId
     * @return
     */
    public static COG convertPid2COG(String protId) {
        if (pid2COGId.containsKey(protId)) {
            if (cogId2COG.containsKey(pid2COGId.get(protId))) {
                return cogId2COG.get(pid2COGId.get(protId));
            } else {
                log.warn("COG id {} does not match any COG entry", pid2COGId.get(protId));
            }
        } else {
            log.warn("peptide id {} in COG db does not match any COG entry", protId);
        }
        return null;
    }
}