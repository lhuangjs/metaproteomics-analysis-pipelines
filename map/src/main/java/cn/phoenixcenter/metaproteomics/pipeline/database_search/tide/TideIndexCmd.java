package cn.phoenixcenter.metaproteomics.pipeline.database_search.tide;


import cn.phoenixcenter.metaproteomics.base.ICommand;
import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.utils.CommandExecutor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.nio.file.Paths;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TideIndexCmd implements ICommand {

    private static final long serialVersionUID = 1L;

    private final String cruxLocation = Paths.get(GlobalConfig.getValue("crux")).toAbsolutePath().toString();

    private String paramFile;

    private String fastaFile;

    private String libraryDir;

    private String outputDir;

    @Override
    public String getDesc() {
        return "tide-index";
    }

    @Override
    public void execute() {
        String command = String.join(" ", cruxLocation,
                "tide-index",
                "--output-dir", outputDir,
                "--overwrite true",
                "--parameter-file", paramFile,
                fastaFile,
                libraryDir);
        CommandExecutor.exec(command);
    }
}
