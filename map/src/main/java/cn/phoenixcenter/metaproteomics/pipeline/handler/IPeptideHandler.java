package cn.phoenixcenter.metaproteomics.pipeline.handler;

import cn.phoenixcenter.metaproteomics.pipeline.base.ParsimonyType;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public interface IPeptideHandler {

     void writePeptide(Path outputPath) throws IOException;

    /**
     * Obtain peptide prob distribution.
     */
    void getPeptideProbDistribution();

    /**
     * Obtain unique-shared peptide percentage.
     *
     * @return
     */
    Map<ParsimonyType, Integer> getPeptideTypeDistribution(double qvalue);

    /**
     * Obtain peptide charge state distribution.
     *
     * @return
     */
    List<Map.Entry<String, Long>> getPeptideChargeStateDistribution(double qvalue);


    /**
     * Obtain peptide length distribution.
     *
     * @return
     */
    Map<Integer, Long> getPeptideLengthDistribution(double qvalue);
}
