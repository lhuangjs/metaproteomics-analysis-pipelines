package cn.phoenixcenter.metaproteomics.pipeline.inference.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class ProteinGroup {

    protected Set<Protein> members;

    public String fPrint(){
        String pids = members.stream()
                .map(p -> p.getId())
                .collect(Collectors.joining(","));
        String parsimonyTypes = members.stream()
                .filter(p -> p.getParsimonyType() != null)
                .map(p -> p.getParsimonyType().toString())
                .collect(Collectors.joining(","));
        return String.join("\t", pids, parsimonyTypes);
    }
}
