package cn.phoenixcenter.metaproteomics.pipeline.inference;

import cn.phoenixcenter.metaproteomics.base.ICommand;
import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.pipeline.handler.ProteinHelper;
import cn.phoenixcenter.metaproteomics.utils.CommandExecutor;
import cn.phoenixcenter.metaproteomics.utils.FileReaderUtil;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@Log4j2
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class FidoCmd implements ICommand {

    private static final long serialVersionUID = 1L;

    private transient final SAXReader reader = new SAXReader();

    private final JsonFactory jsonFactory = new ObjectMapper().getFactory();

    private final String fidoLocation = GlobalConfig.getValue("fido");

    private final String tempDir = GlobalConfig.getValue("temporary.dir");

    private String decoyPrefix = "decoy_";

    private String percolatorXml;

    private double peptFDRThreshold = 0.01;

    private int accuracyLevel = 2;

    private String mpProteinJsonFile;

    @Override
    public String getDesc() {
        return "fido";
    }

    @Override
    public void execute() throws Exception {
        Path tmpDirPath = Paths.get(tempDir);
        Path graphPath = Files.createTempFile(tmpDirPath, "graph", ".txt");
        Path targetDecoyPath = Files.createTempFile(tmpDirPath, "target-decoy", ".txt");
        Path proteinMappingPath = Files.createTempFile(tmpDirPath, "protein-mapping", ".txt");
        Path fidoOutputPath = Files.createTempFile(tmpDirPath, "fido", ".txt");
        log.debug("Create temporary files: {}, {} and {}", graphPath, targetDecoyPath, proteinMappingPath);
        parsePercolatorXML(percolatorXml, graphPath, targetDecoyPath, proteinMappingPath);
        // execute command
        String command = String.join(" ", fidoLocation,
                "-c" + accuracyLevel,
                graphPath.toString(),
                targetDecoyPath.toString());
        // redirect fido output to file
        Consumer<InputStream> outConsumer = (InputStream in) -> {
            try (
                    BufferedWriter bw = Files.newBufferedWriter(fidoOutputPath, StandardCharsets.UTF_8,
                            StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                    BufferedReader br = new BufferedReader(new InputStreamReader(in))
            ) {
                String line;
                while ((line = br.readLine()) != null) {
                    bw.write(line + System.lineSeparator());
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        };
        CommandExecutor.exec(command, outConsumer);
        writeFidoResult(fidoOutputPath, proteinMappingPath);
//        groupProteins(fidoOutputPath, proteinMappingPath);
        Files.delete(graphPath);
        Files.delete(targetDecoyPath);
        Files.delete(proteinMappingPath);
        log.debug("Delete temporary files: {}, {} and {}", graphPath, targetDecoyPath, proteinMappingPath);
    }

    /**
     * Parse percolator xml file(contains target and decoy info in one file)
     * to get fido input file: graph file, target-decoy protein ids file.
     * Meanwhile, we will get a mapping file: protein id - peptides - peptides psm count
     *
     * @param percolatorXml
     * @param graphPath
     * @param targetDecoyPath
     * @param proteinMappingPath
     * @throws DocumentException
     * @throws IOException
     */
    private void parsePercolatorXML(String percolatorXml,
                                    Path graphPath,
                                    Path targetDecoyPath,
                                    Path proteinMappingPath) throws DocumentException, IOException {
        /** read file **/
        Map<String, MPPeptide> seq2Pept = new HashMap<>();
        Map<String, MPProtein> pid2Prot = new HashMap<>();
        Document document = reader.read(percolatorXml);
        List<Element> peptElemList = document.getRootElement().element("peptides").elements();
        for (Element peptElem : peptElemList) {
            double qval = Double.parseDouble(peptElem.element("q_value").getStringValue());
            if (qval < peptFDRThreshold) {
                String peptSeq = peptElem.attributeValue("peptide_id").replaceAll("\\[.*?\\]", "");
                boolean isTarget = !Boolean.valueOf(peptElem.attributeValue("decoy"));
                // some like:
                // <peptide p:peptide_id="ATGFPIAK" p:decoy="true"> and
                // <peptide p:peptide_id="ATGFPIAK" p:decoy="false">
                if (seq2Pept.containsKey(peptSeq)) {
                    if (isTarget) {
                        MPPeptide mpPeptide = seq2Pept.get(peptSeq);
                        for (MPProtein prot : mpPeptide.getAssociatedProteinSet()) {
                            pid2Prot.get(prot.getId()).getContainedPeptideSet().remove(mpPeptide);
                            if (pid2Prot.get(prot.getId()).getContainedPeptideSet().size() == 0) {
                                pid2Prot.remove(prot.getId());
                            }
                        }
                    } else {
                        continue;
                    }
                }
                MPPeptide mpPeptide = new MPPeptide(peptSeq);
                mpPeptide.setQvalue(qval);
                mpPeptide.setProb(1 - Double.parseDouble(peptElem.element("pep").getStringValue()));
                mpPeptide.setSpectrumCount(peptElem.element("psm_ids").elements("psm_id").size());
                mpPeptide.setAssociatedProteinSet(new HashSet<>());
                seq2Pept.put(peptSeq, mpPeptide);
                // set associated proteins
                Set<String> pidSet = peptElem.elements("protein_id").stream()
                        .map(Element::getStringValue)
                        .collect(Collectors.toSet());
                Set<MPProtein> associatedProtSet = new HashSet<>();
                for (String pid : pidSet) {
                    MPProtein mpProt = null;
                    if (pid2Prot.containsKey(pid)) {
                        mpProt = pid2Prot.get((pid));
                    } else {
                        mpProt = new MPProtein(pid);
                        mpProt.setTarget(isTarget);
                        mpProt.setContainedPeptideSet(new HashSet<>());
                        pid2Prot.put(pid, mpProt);
                    }
                    mpProt.getContainedPeptideSet().add(mpPeptide);
                    associatedProtSet.add(mpProt);
                }
                mpPeptide.getAssociatedProteinSet().addAll(associatedProtSet);
            }
        }
        Set<MPProtein> mpProteinSet = new HashSet<>(pid2Prot.values());
        // protein and peptide will be updated after calling the method
        // protein number may reduce(proteins contain same peptides will be collapsed)
        ProteinHelper.setAltProteinSet(mpProteinSet);
        pid2Prot = null;

        /** write files **/
        // write fido input files
        writeFidoInputFiles(seq2Pept.values(), mpProteinSet, graphPath, targetDecoyPath);
        // write protein info
        writeProteinMappingFile(mpProteinSet, proteinMappingPath);
    }

    private void writeFidoInputFiles(Collection<MPPeptide> mpPeptides,
                                     Set<MPProtein> mpProteinSet,
                                     Path graphPath,
                                     Path targetDecoyPath) throws IOException {
        // graph file
        BufferedWriter writer = Files.newBufferedWriter(graphPath, StandardCharsets.UTF_8);
        for (MPPeptide pept : mpPeptides) {
            writer.write(String.join(System.lineSeparator(),
                    "e " + pept.getSequence(),
                    pept.getAssociatedProteinSet().stream()
                            .map(prot -> "r " + prot.getId())
                            .collect(Collectors.joining(System.lineSeparator())),
                    "p " + pept.getProb()
            ) + System.lineSeparator());
        }
        writer.close();
        // target-decoy file
        Map<Boolean, List<String>> tdProteinMap = mpProteinSet.stream()
                .collect(Collectors.groupingBy(MPProtein::getTarget,
                        Collectors.mapping(MPProtein::getId, Collectors.toList()))
                );
        writer = Files.newBufferedWriter(targetDecoyPath);
        List<String> targetPidList = null;
        List<String> decoyPidList = null;
        try {
            targetPidList = tdProteinMap.get(true);
            decoyPidList = tdProteinMap.get(false);
        } catch (NullPointerException e) {
            if (targetPidList == null) {
                throw new RuntimeException("There is target proteins");
            } else {
                throw new RuntimeException("There is decoy proteins");
            }
        }
        writer.write("{ ");
        int i = 0;
        for (; i < targetPidList.size() - 1; i++) {
            writer.write(targetPidList.get(i) + " , ");
        }
        writer.write(targetPidList.get(i) + " }" + System.lineSeparator());
        writer.write("{ ");
        for (i = 0; i < decoyPidList.size() - 1; i++) {
            writer.write(decoyPidList.get(i) + " , ");
        }
        writer.write(decoyPidList.get(i) + " }" + System.lineSeparator());
        writer.close();
    }

    private void writeProteinMappingFile(Set<MPProtein> mpProteinSet, Path proteinMappingPath) throws IOException {
        BufferedWriter writer = Files.newBufferedWriter(proteinMappingPath, StandardCharsets.UTF_8);
        // if change header: the method "writeFidoResult" also need change
        writer.write(String.join("\t",
                "Protein", "Alternative proteins", "Peptide&PSM"
        ) + System.lineSeparator());
        for (MPProtein mpProt : mpProteinSet) {
            // remove decoy proteins
            if (mpProt.getTarget()) {
                writer.write(String.join("\t",
                        mpProt.getId(),
                        mpProt.getAltProteinSet() != null
                                ? mpProt.getAltProteinSet().stream()
                                .collect(Collectors.joining(","))
                                : "",
                        // peptide info
                        mpProt.getContainedPeptideSet().stream()
                                .map(mpPept -> mpPept.getSequence() + "$" + mpPept.getSpectrumCount())
                                .collect(Collectors.joining(","))
                ) + System.lineSeparator());
            }
        }
        writer.close();
    }

    /**
     * Parse fido result and calculate Q-value
     *
     * @return
     * @throws IOException
     */
    private Map<String, double[]> parseFidoResult(Path fidoOutputPath) throws IOException {
        Map<String, Double> pid2Prob = Files.lines(fidoOutputPath, StandardCharsets.UTF_8)
                .flatMap(line -> {
                    String[] tmp = line.split(" ", 2);
                    if (tmp[1].startsWith("{")) {
                        tmp[1] = tmp[1].substring(2, tmp[1].length() - 2);
                        String[] pids = tmp[1].split(" , ");
                        return Stream.of(pids).map(pid -> new String[]{pid, tmp[0]});
                    } else {
                        return null;
                    }
                })
                .filter(arr -> arr != null)
                .collect(Collectors.toMap(
                        arr -> arr[0],
                        arr -> new Double(arr[1])
                ));
        // cal q-value
        return ProteinHelper.calQvalue(pid2Prob, decoyPrefix);
    }

    /**
     * Write Fido result: protein json file - only contains target proteins.
     */
    private void writeFidoResult(Path fidoOutputPath, Path proteinMappingPath) throws IOException {
        Map<String, double[]> pid2Stat = parseFidoResult(fidoOutputPath);
        // write
        JsonGenerator jsonGen = jsonFactory.createGenerator(new File(mpProteinJsonFile), JsonEncoding.UTF8);
        FileReaderUtil reader = new FileReaderUtil(proteinMappingPath.toString(), "\t", true);
        Consumer<String> consumer = (String line) -> {
            try {
                String[] tmp = line.split("\t");
                String pid = tmp[reader.getColNum("Protein")];
                double[] stat = pid2Stat.get(pid);
                jsonGen.writeStartObject();
                // protein id
                jsonGen.writeStringField("id", pid);
                // alternative protein set
                String altProteins = tmp[reader.getColNum("Alternative proteins")];
                if (altProteins.length() == 0) {
                    jsonGen.writeObjectField("altProteinSet", null);
                } else {
                    jsonGen.writeArrayFieldStart("altProteinSet");
                    for (String id : tmp[reader.getColNum("Alternative proteins")].split(",")) {
                        jsonGen.writeString(id);
                    }
                    jsonGen.writeEndArray();
                }
                // prob - pep - qvalue
                jsonGen.writeNumberField("prob", stat[0]);
                jsonGen.writeNumberField("pep", stat[1]);
                jsonGen.writeNumberField("qvalue", stat[2]);
                // peptide
                jsonGen.writeArrayFieldStart("containedPeptideSet");
                for (String pept : tmp[reader.getColNum("Peptide&PSM")].split(",")) {
                    String[] peptTmp = pept.split("\\$");
                    jsonGen.writeStartObject();
                    jsonGen.writeStringField("sequence", peptTmp[0]);
                    jsonGen.writeNumberField("spectrumCount", Integer.parseInt(peptTmp[1]));
                    jsonGen.writeEndObject();
                }
                jsonGen.writeEndArray();
                jsonGen.writeEndObject();
                jsonGen.writeRaw(System.lineSeparator());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        };
        jsonGen.writeStartArray();
        reader.read(consumer);
        jsonGen.writeEndArray();
        jsonGen.close();
    }

//    private void groupProteins(Path fidoResultPath, Path proteinMappingPath) throws IOException {
//        // parse fido result and calculate q-value
//        Map<String, double[]> pid2Stat = parseFidoResult(fidoResultPath).entrySet()
//                .stream()
//                .filter(e -> !e.getKey().startsWith(decoyPrefix) && e.getValue()[2] < protFDRThreshold)
//                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
//        // obtain protein info from mapping file
//        FileReaderUtil reader = new FileReaderUtil(proteinMappingPath.toString(), "\t", true);
//        Set<MPProtein> mpProteinSet = new HashSet<>(pid2Stat.size());
//        Map<String, MPPeptide> seq2Pept = new HashMap<>(pid2Stat.size());
//        Consumer<String> consumer = (String line) -> {
//            String[] tmp = line.split("\t");
//            String pid = tmp[reader.getColNum("Protein")];
//            if (pid2Stat.containsKey(pid)) {
//                MPProtein mpProtein = new MPProtein();
//                double[] stat = pid2Stat.get(pid);
//                // protein id
//                mpProtein.setId(pid);
//                // alternative protein set
//                String altProteins = tmp[reader.getColNum("Alternative proteins")];
//                if (altProteins.length() != 0) {
//                    mpProtein.setAltProteinSet(Arrays.stream(tmp[reader.getColNum("Alternative proteins")].split(","))
//                            .collect(Collectors.toSet()));
//                }
//                // prob - pep - qvalue
//                mpProtein.setProb(stat[0]);
//                mpProtein.setPep(stat[1]);
//                mpProtein.setQvalue(stat[2]);
//                // peptide
//                Set<MPPeptide> mpPeptideSet = new HashSet();
//                for (String pept : tmp[reader.getColNum("Peptide&PSM")].split(",")) {
//                    String[] peptTmp = pept.split("\\$");
//                    MPPeptide mpPept = null;
//                    if (seq2Pept.containsKey(peptTmp[0])) {
//                        mpPept = seq2Pept.get(peptTmp[0]);
//                    } else {
//                        mpPept = new MPPeptide();
//                        mpPept.setSequence(peptTmp[0]);
//                        mpPept.setSpectrumCount(Integer.parseInt(peptTmp[1]));
//                        mpPept.setAssociatedProteinSet(new HashSet<>());
//                        seq2Pept.put(mpPept.getSequence(), mpPept);
//                    }
//                    // add protein
//                    mpPept.getAssociatedProteinSet().add(mpProtein);
//                    mpPeptideSet.add(mpPept);
//                }
//                // add peptides
//                mpProtein.setContainedPeptideSet(mpPeptideSet);
//                mpProteinSet.add(mpProtein);
//            }
//        };
//        reader.read(consumer);
//        // group proteins
//        Set<MPPeptide> mpPeptideSet = new HashSet<>(seq2Pept.values());
//        ProteinGroupHandler.addParsimonyTypeInfo(mpPeptideSet, mpProteinSet);
//        mpProteinGroupList = ProteinGroupHandler.groupProteins(
//                mpProteinSet,
//                libraryJsonFile,
//                uniqPeptThreshold,
//                rank,
//                minTaxonConsistency
//        );
//    }
}
