package cn.phoenixcenter.metaproteomics.pipeline.processor;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.pipeline.base.ParsimonyType;
import cn.phoenixcenter.metaproteomics.pipeline.utils.SequenceHandler;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import cn.phoenixcenter.metaproteomics.taxonomy.TaxonSearcher;
import cn.phoenixcenter.metaproteomics.utils.CommandExecutor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Log4j2
public class ProteinPostprocess {

    @Setter
    private static String qvalityLocation = GlobalConfig.getValue("qvality");

    private static TaxonSearcher taxonSearcher = GlobalConfig.getObj(TaxonSearcher.class);

    /**
     * Extract taxon id from protein json file -> set taxon id -> set taxon info using taxon-searcher
     *
     * @param mpProteinSet
     * @param protJsonPath
     */
    public static void addTaxonInfo(Set<MPProtein> mpProteinSet, Path protJsonPath) {
        // add taxon id info(taxon id from json file)
        Map<String, MPProtein> id2MPProt = mpProteinSet.parallelStream()
                .collect(Collectors.toMap(
                        mpProt -> mpProt.getId(),
                        mpProt -> mpProt
                ));
        SequenceHandler.json2MPProteins(protJsonPath, true, true, false,
                (MPProtein mpProt) -> {
                    if (id2MPProt.containsKey(mpProt.getId())) {
                        id2MPProt.get(mpProt.getId()).setTaxon(mpProt.getTaxon());
                    }
                }
        );
        Set<Integer> tidSet = mpProteinSet.parallelStream()
                .filter(mpProt -> mpProt.getTaxon() != null)
                .map(mpProt -> mpProt.getTaxon().getId())
                .collect(Collectors.toSet());
        // add taxon info(name, rank, ...)
        Map<Integer, Taxon> id2Taxon = taxonSearcher.getTaxaByIds(tidSet);
        mpProteinSet.parallelStream()
                .filter(mpProt -> mpProt.getTaxon() != null)
                .forEach(mpProt -> mpProt.setTaxon(id2Taxon.get(mpProt.getTaxon().getId())));
    }

    /**
     * Calculate q-value(high score is better).
     *
     * @param mpProtList
     * @throws IOException
     * @throws InterruptedException
     */
    public static void calQvalue(List<MPProtein> mpProtList) throws IOException {
        //sort
        Collections.sort(mpProtList, Comparator.comparingDouble(MPProtein::getProb).reversed());
        // organize qvality input
        Map<Boolean, List<String>> targetDecoyPart = mpProtList.parallelStream()
                .collect(Collectors.groupingBy(MPProtein::getTarget,
                        Collectors.collectingAndThen(Collectors.toList(),
                                (List<MPProtein> list) -> list.stream()
                                        // the bigger scores are better
                                        .map((MPProtein mpProt) -> String.valueOf(mpProt.getProb()))
                                        .collect(Collectors.toList()))
                ));
        Path targetPath = Files.createTempFile("target", ".txt");
        Path decoyPath = Files.createTempFile("decoy", ".txt");
        log.debug("create temporary files <{}> and <{}> to save target and decoy scores respectively", targetPath, decoyPath);
        Files.write(targetPath, targetDecoyPart.get(true), StandardCharsets.UTF_8,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        Files.write(decoyPath, targetDecoyPart.get(false), StandardCharsets.UTF_8,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        // run qvality
        String command = String.join(" ",
                qvalityLocation,
                targetPath.toString(),
                decoyPath.toString(),
                "-Y -d");
        AtomicInteger index = new AtomicInteger(0);
        Consumer<InputStream> outConsumer = (InputStream in) -> {
            try (
                    BufferedReader br = new BufferedReader(new InputStreamReader(in))
            ) {
                String line;
                while ((line = br.readLine()) != null) {
                    String[] tmp = line.split("\t");
                    if (!tmp[0].equals("Score")) {
                        mpProtList.get(index.getAndIncrement()).setQvalue(Double.parseDouble(tmp[2]));
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        };
        CommandExecutor.exec(command, outConsumer);
        // delete temporary files
        Files.delete(targetPath);
        Files.delete(decoyPath);
        log.debug("delete temporary files <{}> and <{}>", targetPath, decoyPath);
    }

    /**
     * Only remain MPProtein whose q-value are less than threshold
     *
     * @param mpProtList
     * @param threshold
     * @return
     */
    public static List<MPProtein> filterByQvalue(List<MPProtein> mpProtList, double threshold) {
        log.debug("protein list size is {} before filtering by q-value[{}]", mpProtList.size(), threshold);
        List<MPProtein> filteredMPProtList = mpProtList.stream()
                .filter(mpProt -> mpProt.getQvalue() < threshold)
                .collect(Collectors.toList());
        log.debug("protein list size is {} before filtering by q-value[{}]", filteredMPProtList.size(), threshold);
        return filteredMPProtList;
    }

    /**
     * Only remain MPProtein whose q-value are less than threshold or unique peptide count is
     * more than or equal with minUniqPeptCount
     *
     * @param mpProtList
     * @param threshold
     * @return
     */
    public static List<MPProtein> filterByQvalueAndUniqPept(List<MPProtein> mpProtList, double threshold, int minUniqPeptCount) {
        log.debug("protein list size is {} before filtering by q-value[{}]", mpProtList.size(), threshold);
        List<MPProtein> filteredMPProtList = mpProtList.parallelStream()
                .filter(mpProt -> {
                    boolean qvalFlag = mpProt.getQvalue() < threshold;
                    boolean uniqFlag = mpProt.getUniqPeptCount() >= minUniqPeptCount;
                    if (uniqFlag) {
                        log.debug("protein <{}> has {} unique peptides", mpProt.getId(), mpProt.getUniqPeptCount());
                    }
                    return qvalFlag || uniqFlag;
                })
                .collect(Collectors.toList());
        log.debug("protein list size is {} before filtering by q-value[{}]", filteredMPProtList.size(), threshold);
        return filteredMPProtList;
    }

    public static Map<ParsimonyType, List<MPProtein>> groupByParsimony(Collection<MPProtein> mpProteins) {
        return mpProteins.parallelStream()
                .collect(Collectors.groupingBy(MPProtein::getParsimonyType));
    }

    /**
     * Delete proteins belong to decoy protein
     *
     * @param mpProteins
     * @return
     */
    public static Collection<MPProtein> delDecoyProts(Collection<MPProtein> mpProteins) {
        log.info("protein count = {} before removing decoy proteins", mpProteins.size());
        Collection<MPProtein> result = mpProteins.parallelStream()
                .filter(MPProtein::getTarget)
                .collect(Collectors.toList());
        log.info("protein count = {} after removing decoy proteins", result.size());
        return result;
    }


    /**
     * Delete proteins whose unique peptide count less than threshold
     *
     * @param mpProteins
     * @param minUniquePeptCount
     * @return
     */
    public static Collection<MPProtein> filterByUniqPeptide(Collection<MPProtein> mpProteins,
                                                            int minUniquePeptCount) {
        return mpProteins.parallelStream()
                .filter(prot -> prot.getContainedPeptideSet().stream()
                        .filter((MPPeptide pept) -> pept.isUniq())
                        .count() >= minUniquePeptCount
                )
                .collect(Collectors.toList());
    }

    /**
     * Calculate PSM count based on protein by summing up PSM count of all associated peptides.
     * Meanwhile sort the list by PSM count on descending order.
     * <p>
     * If a peptide is shared by multiple protein and ignoreSharedPeptide is fasle,
     * the related PSM count is 1/n, where n is the protein count contained the peptide.
     *
     * @param mpProteins
     * @param ignoreSharedPeptide
     * @return
     */
    public static Collection<MPProtein> calPSMCount(Collection<MPProtein> mpProteins, boolean ignoreSharedPeptide) {
        for (MPProtein prot : mpProteins) {
            double psmCount = 0.0;
            for (MPPeptide pept : prot.getContainedPeptideSet()) {
                if (pept.getAssociatedProteinSet().size() == 1) {
                    // unique peptide
                    psmCount += pept.getSpectrumCount();
                } else if (!ignoreSharedPeptide) {
//                    psmCount += 1.0 / pept.getSpectrumCount();
//                    psmCount += pept.getSpectrumCount();
                    psmCount += (double)pept.getSpectrumCount() / pept.getAssociatedProteinSet().size();
                }
            }
            prot.setPsmCount(psmCount);
        }
        return mpProteins;
    }

    /**
     * Retain only proteins whose peptide count is greater than or equal to minPeptCount
     *
     * @return
     */
    public static Collection<MPProtein> filterByPeptideCount(Collection<MPProtein> mpProteins, int minPeptCount) {
        return mpProteins.parallelStream()
                .filter(mpProt -> mpProt.getPeptideCount() >= minPeptCount)
                .collect(Collectors.toList());
    }

    /**
     * Note: it will be ignored if taxon is null
     *
     * @param mpProteins
     * @return
     */
    public static Map<Taxon, List<MPProtein>> groupByTaxon(Collection<MPProtein> mpProteins) {
        return mpProteins.stream()
                .filter(mpProt -> mpProt.getTaxon() != null)
                .collect(Collectors.groupingBy(MPProtein::getTaxon));
    }

    public static Map<Taxon, Double> calTaxonDistribution(Map<Taxon, List<MPProtein>> taxon2Prots) {
        Map<Taxon, Double> taxon2TotalPSM = taxon2Prots.entrySet().stream()
                .collect(Collectors.toMap(
                        e -> e.getKey(),
                        e -> e.getValue().stream().mapToDouble(MPProtein::getPsmCount).sum()
                ));
        double totalPSMCount = taxon2TotalPSM.values().stream().mapToDouble(Double::doubleValue).sum();
        return taxon2TotalPSM.entrySet().stream()
                .collect(Collectors.toMap(
                        e -> e.getKey(),
                        e -> 100.0 * e.getValue() / totalPSMCount
                ));
    }

    public static void writeTaxonDistribution(Map<Taxon, List<MPProtein>> taxon2Prots, Path outputPath) throws IOException {
        Map<Taxon, Double> taxon2Percentage = calTaxonDistribution(taxon2Prots);
        List<String> lines = new ArrayList<>(taxon2Percentage.size() + 1);
        lines.add(String.join("\t", "Taxon name", "Percentage(%)"));
        lines.addAll(taxon2Percentage.entrySet().stream()
                .sorted(Comparator.comparing(e -> e.getKey().getName()))
                .map(e -> String.join("\t", e.getKey().getName(), String.valueOf(e.getValue())))
                .collect(Collectors.toList())
        );
        Files.write(outputPath, lines);
    }

    /**
     * Some proteins were removed after doing protein inference. But the field containedPeptideSet
     * is constant for existed proteins. So we need update those fields:
     * <p>
     * MPPeptide: associatedProteinSet, parsimonyType
     * <p>
     * MPProtein: psmCount, parsimonyType, associatedProteinSet, differentiable, superset, subset
     * <p>
     * NOTE: the process will modify nodeData of mpPeptides and mpPeptides
     *
     * @param mpPeptSet
     * @param mpProtSet
     * @param ignoreSharedPeptide
     */
    public static void updatePeptProt(Set<MPPeptide> mpPeptSet,
                                      Set<MPProtein> mpProtSet,
                                      boolean ignoreSharedPeptide) {
        // update associatedProteinSet
        Map<MPPeptide, Set<MPProtein>> pept2ProtSet = new HashMap<>(mpPeptSet.size());
        // record all peptides(existed peptide => proteins)
        for (MPProtein mpProt : mpProtSet) {
            for (MPPeptide mpPept : mpProt.getContainedPeptideSet()) {
                if (!pept2ProtSet.containsKey(mpPept)) {
                    pept2ProtSet.put(mpPept, new HashSet<>());
                }
                pept2ProtSet.get(mpPept).add(mpProt);
            }
        }
        for (Map.Entry<MPPeptide, Set<MPProtein>> e : pept2ProtSet.entrySet()) {
            // update field associatedProteinSet
            e.getKey().setAssociatedProteinSet(e.getValue());
        }
        mpPeptSet.clear();
        mpPeptSet.addAll(pept2ProtSet.keySet());
        // set alternative protein set
        SequenceHandler.setAltProteinSet(mpProtSet);
        // update parsimony type
        ProteinGroupProcess.addParsimonyTypeInfo(mpPeptSet, mpProtSet);
        // update PSM count
        calPSMCount(mpProtSet, ignoreSharedPeptide);
    }

    /**
     * Some peptides were removed after filtering. But the field associatedProteinSet
     * is constant for existed peptides. So we need update those fields:
     * <p>
     * MPProtein: containedPeptideSet, psmCount, parsimonyType, altProteinSet, associatedProteinSet, differentiable, superset, subset
     * <p>
     * NOTE: the process will modify nodeData of mpPeptides and mpPeptides
     *
     * @param mpPeptSet
     * @param mpProtSet
     * @param ignoreSharedPeptide
     */
    public static void updatePeptProtByPept(Set<MPPeptide> mpPeptSet,
                                            Set<MPProtein> mpProtSet,
                                            boolean ignoreSharedPeptide) {
        mpProtSet.clear();
        Map<String, MPProtein> pid2Prot = new HashMap<>(mpProtSet.size() / 3 * 2);
        for (MPPeptide pept : mpPeptSet) {
            for (MPProtein prot : pept.getAssociatedProteinSet()) {
                if (!pid2Prot.containsKey(prot.getId())) {
                    pid2Prot.put(prot.getId(), prot);
                    prot.getContainedPeptideSet().clear();
                }
                prot.getContainedPeptideSet().add(pept);
            }
        }
        mpProtSet.addAll(pid2Prot.values());
        // set alternative protein set
        SequenceHandler.setAltProteinSet(mpProtSet);
        // update parsimony type
        ProteinGroupProcess.addParsimonyTypeInfo(mpPeptSet, mpProtSet);
        // update PSM count
        calPSMCount(mpProtSet, ignoreSharedPeptide);
    }


    public static void writeProtein(Collection<MPProtein> mpProteins, Path outputPath) throws IOException {
        String delimiter = "\t";
        BufferedWriter bw = Files.newBufferedWriter(outputPath);
        bw.write(String.join(delimiter, "Protein", "Type", "Probability",
                "Taxon id", "Taxon name", "Taxon rank", "Peptide count", "PSM count") + System.lineSeparator()
        );
        for (MPProtein mpProt : mpProteins) {
            Taxon taxon = mpProt.getTaxon();
            StringJoiner joiner = new StringJoiner(delimiter);
            joiner.add(mpProt.getId());
            joiner.add(mpProt.getParsimonyType().toString());
            joiner.add(String.valueOf(mpProt.getProb()));
            joiner.add(taxon != null
                    ? String.join(delimiter, String.valueOf(taxon.getId()), taxon.getName(), taxon.getRank())
                    : "\t\t"
            );
            joiner.add(String.valueOf(mpProt.getPeptideCount()));
            joiner.add(String.valueOf(mpProt.getPsmCount()));
            bw.write(joiner.toString() + System.lineSeparator());
        }
        bw.close();
    }
}
