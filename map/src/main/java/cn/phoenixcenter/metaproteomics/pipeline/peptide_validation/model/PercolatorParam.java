package cn.phoenixcenter.metaproteomics.pipeline.peptide_validation.model;

import cn.phoenixcenter.metaproteomics.base.Param;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class PercolatorParam {

    @Param(paramName = "decoy-prefix",
            paramAlias = "Decoy prefix",
            paramDesc = "Specifies the prefix of the protein names that indicate a decoy.")
    private String decoyPrefix = "decoy_";

    @Param(paramName = "decoy-xml-output",
            booleanType = true,
            paramDesc = "Include decoys (PSMs, peptides, and/or proteins) in the XML output. Default = false.")
    private boolean decoyXmlOutput = true;

    @Param(paramName = "pepxml-output",
            booleanType = true,
            paramDesc = "Output a pepXML results file to the output directory.")
    private boolean pepxmlOutput = true;

    @Param(paramName = "pout-output",
            booleanType = true,
            paramDesc = "Output a Percolator pout.xml format results file to the output directory.")
    private boolean poutOutput = true;

    @Param(paramName = "output-dir",
            paramDesc = "The name of the directory where output files will be created.")
    @NonNull
    private String outputDir;

    @Param(paramName = "overwrite",
            booleanType = true,
            paramDesc = "Replace existing files if true or fail when trying to overwrite a file if false.")
    private boolean overwrite = true;

    @Param(paramName = "",
            paramDesc = "Feature file")
    @NonNull
    private String featureFile;

    public PercolatorParam(String decoyPrefix, @NonNull String outputDir, @NonNull String featureFile) {
        this.decoyPrefix = decoyPrefix;
        this.outputDir = outputDir;
        this.featureFile = featureFile;
    }
}
