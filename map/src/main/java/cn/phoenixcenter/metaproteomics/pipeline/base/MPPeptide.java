package cn.phoenixcenter.metaproteomics.pipeline.base;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class MPPeptide {

    private String sequence;

    // probability
    private Double prob;

    private Double qvalue;

    private Integer spectrumCount = 0;

    private String charge;

    @ToString.Exclude
    private Set<MPProtein> associatedProteinSet;

    private ParsimonyType parsimonyType;

    public MPPeptide(String sequence) {
        this.sequence = sequence;
    }

    public void increaseSpectrumCount() {
        this.spectrumCount += 1;
    }

    public boolean isUniq() {
        return this.associatedProteinSet.size() == 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MPPeptide mpPeptide = (MPPeptide) o;
        return Objects.equals(sequence, mpPeptide.sequence);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sequence);
    }
}
