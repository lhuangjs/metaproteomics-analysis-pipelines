package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm;


import cn.phoenixcenter.metaproteomics.pipeline.inference.model.*;
import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;

/**
 * The class is used to calculate peptides and protein parsimony type
 * These attributes need be added before doing parsimony algorithm:
 * <PRE>
 * 1. Peptide.sequence, Peptide.associatedProteinSet
 * 2. Protein.id, Protein.containedPeptideSet
 * </PRE>
 */
@Getter
public class ParsimonyAlgorithm {

    private Set<ProteinWithRelationship> proteinWithRelationshipSet;

    /**
     * Protein classification needs two steps:
     * 1. set peptide parsimony type;
     * 2. set protein parsimony type.
     * The method combine the two steps.
     *
     * @param peptideSet
     * @param proteinSet
     */
    public void addParsimonyTypeInfo(Set<Peptide> peptideSet, Set<Protein> proteinSet) {
        setPepParsimonyType(peptideSet);
        setProParsimonyType(peptideSet, proteinSet);
    }

    /**
     * Set the parsimony type of peptide.
     *
     * @param peptideSet
     */
    public void setPepParsimonyType(Set<Peptide> peptideSet) {
        for (Peptide peptide : peptideSet) {
            // peptide parsimony type
            if (peptide.getAssociatedProteinSet().size() == 1) {
                peptide.setParsimonyType(ParsimonyType.DISTINCT);
            } else if (peptide.getAssociatedProteinSet().size() > 1) {
                peptide.setParsimonyType(ParsimonyType.SHARED);
            } else {
                System.err.println(peptide.getSequence() + " did not map any protein");
                System.exit(255);
            }
        }
    }

    /**
     * Protein parsimonyType: the peptide parsimonyType must be process before protein parsimonyType.
     * <PRE>
     * NOTE:
     * 1. ParsimonyType analysis is applied in ascending hierarchy:
     * equivalent, subset, superset, subsumable, differential, and
     * distinct proteins.
     * 2. Each protein is counted in exactly one
     * category. It is possible for a protein to be listed in more than
     * one category, in which case it is counted in the highest category
     * in which it occurs where distinct is the highest category and
     * equivalent the lowest.
     * </PRE>
     *
     * @param peptideSet
     * @param proteinSet
     */
    public void setProParsimonyType(Set<Peptide> peptideSet, Set<Protein> proteinSet) {
        Map<String, ProteinWithRelationship> id2ProMap = proteinSet.parallelStream()
                .map(protein -> new ProteinWithRelationship(protein))
                .peek(ProteinWithRelationship::init)
                .collect(Collectors.toMap(
                        pro -> pro.getId(),
                        pro -> pro
                ));
        // add associated proteins for each protein: these proteins have at least one shared peptide.
        peptideSet.parallelStream()
                .filter(pep -> pep.getAssociatedProteinSet().size() > 1)
                .forEach(pep -> {
                    Set<ProteinWithRelationship> proSet = pep.getAssociatedProteinSet().stream()
                            .map((Protein protein) -> id2ProMap.get(protein.getId()))
                            .collect(Collectors.toSet());
                    // those proteins have at least shared peptide
                    proSet.stream().forEach(pro -> pro.addAssociatedProteinSet(proSet));

                });
        this.proteinWithRelationshipSet = new HashSet<>(id2ProMap.values());

        /** set parsimony type for ProteinWithRelationship **/
        for (ProteinWithRelationship pro : proteinWithRelationshipSet) {
            if (pro.isContainDistinctPeptide()) {
                if (pro.isAllDistinctPeptides()) {
                    pro.setParsimonyType(ParsimonyType.DISCRETE);
                } else {
                    pro.setParsimonyType(ParsimonyType.DIFFERENTIABLE);
                    classifyAssociatedProSet(pro);
                }
            } else {
                classifyAssociatedProSet(pro);
                if (pro.getSuperset().size() > 0) {
                    pro.setParsimonyType(ParsimonyType.SUBSET);
                } else if (isSubsumable(pro)) {
                    pro.setParsimonyType(ParsimonyType.SUBSUMABLE);
                } else if (pro.getSubset().size() > 0) {
                    pro.setParsimonyType(ParsimonyType.SUPERSET);
                } else {
                    pro.setParsimonyType(ParsimonyType.EQUIVALENT);
                }
            }
        }

        /** set parsimony type for Protein **/
        proteinSet.parallelStream()
                .forEach(protein -> protein.setParsimonyType(id2ProMap.get(protein.getId()).getParsimonyType()));
    }

    /**
     * classify the specified protein and its associated proteins
     *
     * @param pro
     */
    private void classifyAssociatedProSet(ProteinWithRelationship pro) {
        for (ProteinWithRelationship pro2 : pro.getAssociatedProteinSet()) {
            switch (determineRelationship(pro, pro2)) {
                case SUBSET_SUPER:
                    pro.getSuperset().add(pro2);
                    pro2.getSubset().add(pro);
                    break;
                case SUPER_SUBSET:
                    pro.getSubset().add(pro2);
                    pro2.getSuperset().add(pro);
                    break;
                case DIFFERENTIABLE:
                    pro.getDifferentiable().add(pro2);
                    pro2.getDifferentiable().add(pro);
                    break;
                case EQUIVALENT:
                    pro.getEquivalent().add(pro2);
                    pro2.getEquivalent().add(pro);
                    break;
                default:
                    System.err.println("Exist unknown relationship between the two protein("
                            + pro + ", " + pro2 + ")");
            }
        }
    }


    /**
     * Determine the relationship between two proteins
     *
     * @param pro1
     * @param pro2
     * @return
     */
    private Relationship determineRelationship(ProteinWithRelationship pro1, ProteinWithRelationship pro2) {
        if (pro1.getPeptideCount() > pro2.getPeptideCount()) {
            if (pro1.getContainedPeptideSet().containsAll(pro2.getContainedPeptideSet())) {
                return Relationship.SUPER_SUBSET;
            } else {
                return Relationship.DIFFERENTIABLE;
            }
        } else {
            if (pro2.getContainedPeptideSet().containsAll(pro1.getContainedPeptideSet())) {
                if (pro2.getPeptideCount() == pro1.getPeptideCount()) {
                    return Relationship.EQUIVALENT;
                } else {
                    return Relationship.SUBSET_SUPER;
                }
            } else {
                return Relationship.DIFFERENTIABLE;
            }
        }
    }

    private boolean isSubsumable(ProteinWithRelationship pro) {
        if (pro.getDifferentiable().size() < 2) {
            return false;
        }

        List<ProteinWithRelationship> differentiableList = new ArrayList<>(pro.getDifferentiable());
        Set<Peptide> peptideSet = new HashSet<>();
        for (int i = 0; i < differentiableList.size() - 1; i++) {
            ProteinWithRelationship pro1 = differentiableList.get(i);
            for (int j = i + 1; j < differentiableList.size(); j++) {
                ProteinWithRelationship pro2 = differentiableList.get(j);
                if (determineRelationship(pro1, pro2) == Relationship.DIFFERENTIABLE) {
                    peptideSet.addAll(pro1.getContainedPeptideSet());
                    peptideSet.addAll(pro2.getContainedPeptideSet());
                }
            }
        }
        return peptideSet.containsAll(pro.getContainedPeptideSet());
    }
}
