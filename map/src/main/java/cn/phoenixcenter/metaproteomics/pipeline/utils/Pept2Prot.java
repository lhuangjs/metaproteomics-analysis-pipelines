package cn.phoenixcenter.metaproteomics.pipeline.utils;

import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Log4j2
public class Pept2Prot {
    private static final Pattern trypsinPat = Pattern.compile("(?<=[KR])(?!P)");

    public static Map<String, Set<String>> map(boolean equateIL,
                                               Pattern pidPat,
                                               Path libraryPath,
                                               Set<String> peptideSet) throws IOException {
        Set<String> eqILPeptSet = equateIL ? peptideSet.parallelStream()
                .map(p -> p.replace("I", "L"))
                .collect(Collectors.toSet())
                : peptideSet;

        Map<String, Set<String>> pept2ProtSet = new HashMap<>();
        Map<String, List<String[]>> pept2CleavagePepts = new HashMap<>();
        for (String p : eqILPeptSet) {
            pept2ProtSet.put(p, new HashSet<>());
            String[] arr = trypsinPat.split(p);
            if (pept2CleavagePepts.containsKey(p)) {
                pept2CleavagePepts.get(p).add(arr);
            } else {
                List<String[]> cleavagePepts = new ArrayList<>();
                cleavagePepts.add(arr);
                pept2CleavagePepts.put(p, cleavagePepts);
            }
        }


        BufferedReader br = Files.newBufferedReader(libraryPath, StandardCharsets.UTF_8);
        String pid = null;
        StringBuilder sequence = null;
        String line;
        Matcher matcher = null;
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">")) {
                if (pid != null) {
                    map(pid, trypsinPat.split(
                            equateIL ? sequence.toString().replace("I", "L")
                                    : sequence.toString()
                            ),
                            pept2CleavagePepts,
                            pept2ProtSet
                    );
                }
                matcher = pidPat.matcher(line);
                if (matcher.find()) {
                    pid = matcher.group(1);
                    sequence = new StringBuilder();
                } else {
                    pid = null;
                    log.warn("line {} cannot capture protein id", line);
                }
            } else {
                if (pid != null) {
                    sequence.append(line);
                }
            }
        }
        br.close();
        if (equateIL) {
            Map<String, Set<String>> newPept2ProtSet = new HashMap<>();
            for (String p : peptideSet) {
                newPept2ProtSet.put(p, pept2ProtSet.get(p.replace("I", "L")));
            }
            pept2ProtSet = newPept2ProtSet;
        }
        return pept2ProtSet;
    }

    private static void map(String pid, String[] peptides, Map<String, List<String[]>> pept2CleavagePepts,
                            Map<String, Set<String>> pept2ProtSet) {
        for (int i = 0; i < peptides.length; i++) {
            String p = peptides[i];
            if (pept2CleavagePepts.containsKey(p)) {
                List<String[]> cleavagePepts = pept2CleavagePepts.get(p);
                for (String[] pepts : cleavagePepts) {
                    if (pepts.length == 1) {
                        // don't exist missed cleavage
                        pept2ProtSet.get(p).add(pid);
                    } else {
                        // exists missed cleavage
                        int j = 1;
                        for (; j < pepts.length; j++) {
                            if (i + j < peptides.length && !peptides[i + j].equals(peptides[j])) {
                                break;
                            }
                        }
                        if (j == pepts.length) {
                            pept2ProtSet.get(p).add(pid);
                        }
                    }
                }
            }
        }
    }
}
