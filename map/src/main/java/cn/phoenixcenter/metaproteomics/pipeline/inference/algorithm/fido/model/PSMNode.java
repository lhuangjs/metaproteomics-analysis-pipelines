package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model;

import lombok.NoArgsConstructor;

/**
 * Created by huangjs
 */
@NoArgsConstructor
public class PSMNode extends Node {
    public PSMNode(String id) {
        super(id);
    }

    @Override
    public String toString() {
        return "PSMNode{" +
                "id='" + id + '\'' +
                '}';
    }
}
