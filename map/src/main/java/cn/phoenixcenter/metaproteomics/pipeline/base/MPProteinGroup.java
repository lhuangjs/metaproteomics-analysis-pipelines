package cn.phoenixcenter.metaproteomics.pipeline.base;

import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Created by huangjs
 * <p>
 * Protein group
 */

@Getter
@Setter
@NoArgsConstructor
@ToString
public class MPProteinGroup extends MPProtein{

    private List<MPProtein> members;

//    private Taxon lca;
}
