package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model;

import lombok.*;

import java.util.Objects;

/**
 * Created by huangjs
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Node {

    protected String id;

//    /** for finding articulation points(cut vertices) **/
//    // discovery time of the vertex in DFS tree
//    private int discoveryTime;
//
//    // the earliest discovery time of the vertex in DFS tree
//    private int minDiscoveryTime;
//
//    // the parent node of the vertex in DFS tree
//    private Node parentNode;

    public Node(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return id.equals(node.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
