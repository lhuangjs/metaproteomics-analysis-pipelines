package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.model;

import lombok.*;

import java.util.List;

/**
 * Created by huangjs
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProteinGroup {

    private Protein representativeProt;

    private List<Protein> memberList;
}
