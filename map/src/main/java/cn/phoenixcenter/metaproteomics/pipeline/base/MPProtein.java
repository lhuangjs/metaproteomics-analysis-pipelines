package cn.phoenixcenter.metaproteomics.pipeline.base;

import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by huangjs
 */

@Getter
@Setter
@NoArgsConstructor
@ToString
public class MPProtein {

    private String id;

    // probability
    private Double prob;

    private Double pep;

    private Double qvalue;

    private Boolean target;

    private Taxon taxon;

    private Double psmCount;

    // alternative peptide set
    private Set<String> altProteinSet;

    private String sequence;

    private Integer length;

    @ToString.Exclude
    private Set<MPPeptide> containedPeptideSet;

    // protein parsimony type
    private ParsimonyType parsimonyType;

    // protein set contained at least one shared peptides with the protein
    @ToString.Exclude
    private Set<MPProtein> associatedProteinSet;

    // differentiable proteins set of the protein
    @ToString.Exclude
    private Set<MPProtein> differentiable;

    // superset of the protein
    @ToString.Exclude
    private Set<MPProtein> superset;

    // subset of the protein
    @ToString.Exclude
    private Set<MPProtein> subset;

    public MPProtein(String id) {
        this.id = id;
    }

//    public MPProtein(MPProtein mpProtein) {
//        try {
//            Field[] fields = MPProtein.class.getDeclaredFields();
//            for (Field f : fields) {
//                f.set(this, f.get(mpProtein));
//            }
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//            System.exit(255);
//        }
//    }

    /**
     * Obtain the number of the protein's peptides.
     *
     * @return
     */
    public int getPeptideCount() {
        return containedPeptideSet.size();
    }

    public int getUniqPeptCount() {
        return (int) (this.getContainedPeptideSet().stream()
                .filter(mpPept -> mpPept.getAssociatedProteinSet().size() == 1)
                .count());
    }

    public void init() {
        this.associatedProteinSet = new HashSet<>();
        this.differentiable = new HashSet<>();
        this.superset = new HashSet<>();
        this.subset = new HashSet<>();
    }

    /**
     * Add associated protein. However, the protein self need to be excluded.
     *
     * @param proteinSet
     */
    public void addAssociatedProteinSet(Set<MPProtein> proteinSet) {
        this.associatedProteinSet.addAll(proteinSet);
        this.getAssociatedProteinSet().remove(this);
    }

    /**
     * Judge whether the proteins's peptides all belong to distinct peptide.
     *
     * @return
     */
    public boolean isAllDistinctPeptides() {
        return this.containedPeptideSet.stream()
                .allMatch((MPPeptide p) -> p.getParsimonyType() == ParsimonyType.DISTINCT);
    }

    /**
     * Judge whether the protein's peptides all belong to shared peptide.
     *
     * @return
     */
    public boolean isAllSharedPeptides() {
        return this.containedPeptideSet.stream()
                .allMatch((MPPeptide p) -> p.getParsimonyType() == ParsimonyType.SHARED);
    }

    /**
     * Judge whether the protein contains at least one distinct peptide.
     *
     * @return
     */
    public boolean containsDistinctPeptide() {
        return this.containedPeptideSet.stream()
                .anyMatch((MPPeptide p) -> p.getParsimonyType() == ParsimonyType.DISTINCT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MPProtein mpProtein = (MPProtein) o;
        return Objects.equals(id, mpProtein.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
