package cn.phoenixcenter.metaproteomics.pipeline.database_search.tide;

import cn.phoenixcenter.metaproteomics.base.Param;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.model.TideParam;
import com.compomics.util.experiment.biology.AminoAcidPattern;
import com.compomics.util.experiment.biology.PTM;
import com.compomics.util.experiment.biology.PTMFactory;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Reference: SearcgGUI(https://github.com/compomics/searchgui)
 * class: eu.isas.searchgui.processbuilders.TideIndexProcessBuilder
 */
public class TideParamParser {

    // The compomics PTM factory.
    private final static PTMFactory ptmFactory = PTMFactory.getInstance();

    private final static JsonFactory jsonFactory = new JsonFactory();

    private enum ModType {
        NON_TERM_MOD,
        C_TERM_MOD,
        N_TERM_MOD
    }

    /**
     * @param tideParam
     * @param paramFilePath
     * @throws IOException
     * @throws IllegalAccessException
     */
    public static void toParamFile(TideParam tideParam, Path paramFilePath) throws IOException, IllegalAccessException {
        Map<ModType, Map<String, String>> modType2Mod = parseMods(tideParam);
        BufferedWriter bw = Files.newBufferedWriter(paramFilePath);
        Field[] fields = TideParam.class.getDeclaredFields();
        for (Field field : fields) {
            Param paramAnno = field.getAnnotation(Param.class);
            if (paramAnno != null && paramAnno.nativeParam()) {
                field.setAccessible(true);
                Object value = field.get(tideParam);
                bw.write("# " + paramAnno.paramDesc() + System.lineSeparator());
                switch (paramAnno.paramName()) {
                    case "nterm-peptide-mods-spec":
                        writeModSettings(ModType.N_TERM_MOD, modType2Mod, paramAnno, bw);
                        break;
                    case "cterm-peptide-mods-spec":
                        writeModSettings(ModType.C_TERM_MOD, modType2Mod, paramAnno, bw);
                        break;
                    case "mods-spec":
                        writeModSettings(ModType.NON_TERM_MOD, modType2Mod, paramAnno, bw);
                        break;
                    default:
                        if (value != null) {
                            bw.write(paramAnno.paramName() + "=" + value.toString() + System.lineSeparator());
                        } else {
                            bw.write(paramAnno.paramName() + "=" + System.lineSeparator());
                        }
                }
                bw.write(System.lineSeparator());
            }
        }
        bw.close();
    }

    /**
     * Write TideParam to json file for front-end display.
     *
     * @param jsonFilePath
     * @throws IOException
     * @throws IllegalAccessException
     */
    public static void toJsonFile(TideParam tideParam, Path jsonFilePath) throws IOException, IllegalAccessException {
        JsonGenerator gen = jsonFactory.createGenerator(jsonFilePath.toFile(), JsonEncoding.UTF8);
        gen.writeStartObject();
        Field[] fields = TideParam.class.getDeclaredFields();
        for (Field field : fields) {
            // obtain TideParam field annotation
            Param paramAnno = field.getAnnotation(Param.class);
            if (paramAnno != null && !paramAnno.paramAlias().isEmpty()) {
                field.setAccessible(true);
                // obtain param value
                Object value = field.get(tideParam);
                gen.writeObjectFieldStart(paramAnno.paramName());
                gen.writeStringField("fieldName", field.getName());
                gen.writeStringField("alias", paramAnno.paramAlias());
                gen.writeStringField("desc", paramAnno.paramDesc());
                gen.writeBooleanField("booleanType", paramAnno.booleanType());
                switch (paramAnno.paramName()) {
                    case "fixed modification":
                    case "variable modification": {
                        // write value
                        List<String> mods = (List<String>) value;
                        if (mods != null && mods.size() > 0) {
                            gen.writeArrayFieldStart("value");
                            for (String mod : mods) {
                                gen.writeString(mod);
                            }
                            gen.writeEndArray();
                        }
                        // write candidates
                        String[] candidates = paramAnno.candidates();
                        gen.writeArrayFieldStart("candidates");
                        for (String cand : candidates) {
                            gen.writeString(cand);
                        }
                        gen.writeEndArray();
                        break;
                    }
                    default: {
                        // write value
                        if (field.getType().isAssignableFrom(int.class)) {
                            gen.writeNumberField("value", (int) value);
                        } else if (field.getType().isAssignableFrom(double.class)) {
                            gen.writeNumberField("value", (double) value);
                        } else if (field.getType().isAssignableFrom(boolean.class)) {
                            gen.writeBooleanField("value", (boolean) value);
                        } else {
                            gen.writeStringField("value", (String) value);
                        }
                        // write candidate
                        String[] candidates = paramAnno.candidates();
                        if (candidates.length != 0) {
                            gen.writeArrayFieldStart("candidates");
                            for (String cand : candidates) {
                                gen.writeString(cand);
                            }
                            gen.writeEndArray();
                        }
                    }
                }
                gen.writeEndObject();
            }
        }
        gen.writeEndObject();
        gen.close();
    }

    private static void writeModSettings(ModType modType,
                                         Map<ModType, Map<String, String>> modType2Mod,
                                         Param paramAnno,
                                         BufferedWriter bw) throws IOException {
        Map<String, String> name2Mod = modType2Mod.get(modType);
        if (name2Mod != null) {
            // write comment
            bw.write(name2Mod.entrySet().stream()
                    .map(e -> "# " + e.getValue() + " : " + e.getKey())
                    .collect(Collectors.joining(System.lineSeparator()))
                    + System.lineSeparator());
            // write value
            bw.write(paramAnno.paramName() + "=" + name2Mod.values().stream()
                    .collect(Collectors.joining(","))
                    + System.lineSeparator());
        } else {
            bw.write(paramAnno.paramName() + "=" + System.lineSeparator());
        }
    }

    private static Map<ModType, Map<String, String>> parseMods(TideParam tideParam) {
        Map<ModType, Map<String, String>> modType2Mod = new HashMap<>(3);
        // Get the non-terminal modifications
        Map<String, String> name2TideMod = getNonTermMods(tideParam);
        if (name2TideMod != null) {
            modType2Mod.put(ModType.NON_TERM_MOD, name2TideMod);
        }
        // Get the N-terminal modifications
        name2TideMod = getTermMods(tideParam, true);
        if (name2TideMod != null) {
            modType2Mod.put(ModType.N_TERM_MOD, name2TideMod);
        }
        // Get the C-terminal modifications
        name2TideMod = getTermMods(tideParam, false);
        if (name2TideMod != null) {
            modType2Mod.put(ModType.C_TERM_MOD, name2TideMod);
        }
        return modType2Mod.size() != 0 ? modType2Mod : null;
    }

    private static Map<String, String> getNonTermMods(TideParam tideParam) {
        // mod name => [max_per_peptide]residues[+/-]mass_change
        Map<String, String> name2TideMod = new HashMap<>();
        Map<String, String> tmpName2TideMod = getNonTermMods(tideParam, true);
        if (tmpName2TideMod != null) {
            name2TideMod.putAll(tmpName2TideMod);
        }
        tmpName2TideMod = getNonTermMods(tideParam, false);
        if (tmpName2TideMod != null) {
            name2TideMod.putAll(tmpName2TideMod);
        }
        return name2TideMod;
    }

    /**
     * Get the non-terminal modifications.
     * tide ptm pattern: [max_per_peptide]residues[+/-]mass_change
     *
     * @param fixed if the modifications are to to be added as fixed or variable
     * @return if there is no modification, null will be returned. otherwise,
     * return modification name =>
     * the non-terminal modifications as a string in the Tide format
     */
    private static Map<String, String> getNonTermMods(TideParam tideParam, boolean fixed) {
        // the modifications to check
        List<String> modifications = fixed ? tideParam.getFixedModList() : tideParam.getVariableModList();
        if (modifications != null) {
            // mod name => [max_per_peptide]residues[+/-]mass_change
            Map<String, String> name2TideMod = new HashMap<>();
            for (String ptmName : modifications) {
                PTM ptm = ptmFactory.getPTM(ptmName);
                if (!ptm.isNTerm() && !ptm.isCTerm()) {
                    StringBuilder nonTermMod = new StringBuilder();
                    // add the number of allowed ptms per peptide
                    if (!fixed) {
                        nonTermMod.append(tideParam.getMaxVariablePtmsPerTypePerPeptide());
                    }
                    // add the residues affected
                    AminoAcidPattern ptmPattern = ptm.getPattern();
                    if (ptmPattern != null && ptmPattern.length() > 0) {
                        for (Character aminoAcid : ptmPattern.getAminoAcidsAtTarget()) {
                            nonTermMod.append(aminoAcid);
                        }
                    }
                    // add the ptm mass
                    if (ptm.getRoundedMass() > 0) {
                        nonTermMod.append("+");
                    }
                    nonTermMod.append(ptm.getRoundedMass());
                    name2TideMod.put(ptmName, nonTermMod.toString());
                }
            }
            return name2TideMod;
        } else {
            return null;
        }
    }

    private static Map<String, String> getTermMods(TideParam tideParam, boolean nTerm) {
        Map<String, String> name2TideMod = new HashMap<>();
        Map<String, String> tmpName2TideMod = getTermMods(tideParam, true, nTerm);
        if (tmpName2TideMod != null) {
            name2TideMod.putAll(tmpName2TideMod);
        }
        tmpName2TideMod = getTermMods(tideParam, false, nTerm);
        if (tmpName2TideMod != null) {
            name2TideMod.putAll(tmpName2TideMod);
        }
        return name2TideMod.size() != 0 ? name2TideMod : null;
    }

    /**
     * Get the terminal modifications.
     *
     * @param fixed if the modifications are to to be added as fixed or variable
     * @param nTerm true if the modifications are n-terminal, false if c-terminal
     * @return if there is no modification, null will be returned. otherwise,
     * return modification name =>
     * the terminal modifications as a string in the Tide format
     */
    private static Map<String, String> getTermMods(TideParam tideParam, boolean fixed, boolean nTerm) {
        // the modifications to check
        List<String> modifications = fixed ? tideParam.getFixedModList() : tideParam.getVariableModList();
        if (modifications != null) {
            Map<String, String> name2TideMod = new HashMap<>();
            for (String ptmName : modifications) {
                PTM ptm = ptmFactory.getPTM(ptmName);
                if ((ptm.isNTerm() && nTerm) || (ptm.isCTerm() && !nTerm)) {
                    StringBuilder termMod = new StringBuilder();
                    // add the number of allowed ptms per peptide
                    if (!fixed) {
                        termMod.append(1);
                    }
                    // add the residues affected
                    AminoAcidPattern ptmPattern = ptm.getPattern();
                    String tempPtmPattern = "";
                    if (ptmPattern != null && ptmPattern.length() > 0) {
                        for (Character aminoAcid : ptmPattern.getAminoAcidsAtTarget()) {
                            tempPtmPattern += aminoAcid;
                        }
                    }
                    if (tempPtmPattern.length() == 0) {
                        tempPtmPattern = "X";
                    }
                    termMod.append(tempPtmPattern);
                    // add the ptm mass
                    if (ptm.getRoundedMass() > 0) {
                        termMod.append("+");
                    }
                    termMod.append(ptm.getRoundedMass());
                    name2TideMod.put(ptmName, termMod.toString());
                }
            }
            return name2TideMod;
        } else {
            return null;
        }
    }

    public static void printMods() {
        List<String> modList = ptmFactory.getDefaultModificationsOrdered();
        for (int i = 0; i < modList.size(); i++) {
            if (i != modList.size() - 1) {
                System.out.println("\"" + modList.get(i) + ",\" + ");
            } else {
                System.out.println("\"" + modList.get(i) + "\"");
            }
        }
    }
}
