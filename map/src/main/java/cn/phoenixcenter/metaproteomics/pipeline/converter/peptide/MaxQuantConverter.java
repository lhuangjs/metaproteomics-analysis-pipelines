package cn.phoenixcenter.metaproteomics.pipeline.converter.peptide;

import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.EnzymeDigester;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.utils.FileReaderUtil;
import lombok.Getter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 *
 */
public class MaxQuantConverter {

    @Getter
    private Set<MPPeptide> mpPeptideSet;

    @Getter
    private Set<MPProtein> mpProteinSet;

    public Set<MPPeptide> convert2UniquePeptide(String peptidesFile,
                                                boolean delContaminant) throws IOException {
        /** read peptides.txt **/
        Set<MPPeptide> mpPeptideSet = new HashSet<>();
        final FileReaderUtil reader = new FileReaderUtil(peptidesFile, "\t", true);
        Consumer<String> rowConsumer = (String line) -> {
            String[] tmp = line.split("\t");
            if (!tmp[reader.getColNum("Potential contaminant")].equals("+") || !delContaminant) {
                String peptSeq = tmp[reader.getColNum("Sequence")];
                double prob = 1 - Double.parseDouble(tmp[reader.getColNum("PEP")]);
                // MPPeptide
                MPPeptide mpPept = new MPPeptide();
                mpPept.setSequence(peptSeq);
                mpPept.setProb(prob);
                mpPeptideSet.add(mpPept);
            }
        };
        reader.read(rowConsumer);
        return mpPeptideSet;
    }

    /**
     * Convert MaxQuant peptides.txt file as fido input. Meanwhile, build peptide and protein set.
     * Fido needs two input files:
     * <p>
     * 1. graph file, like:
     * <PRE>
     * e peptide_string
     * r protein that would create this peptide using theoretical digest
     * r second protein
     * ...
     * r final protein
     * p probability of the peptide match to the spectrum (given by PeptideProphetEnhancer or comparable).
     * </PRE>
     * <p>
     * 2. target-decoy file
     * <PRE>
     * { target_prot1 , target_prot2 , ... target_protn }
     * { decoy_prot1 , decoy_prot2 , ... decoy_protn }
     * </PRE>
     *
     * @param peptidesFile
     * @param delContaminant
     * @param decoyPrefix
     * @param graphFile
     * @param targetDecoyFile
     * @throws IOException
     */
    public void convertToFido(String peptidesFile,
                              boolean delContaminant,
                              String decoyPrefix,
                              String graphFile,
                              String targetDecoyFile) throws IOException {
        final Set<String> pidSet = new HashSet<>();
        mpPeptideSet = new HashSet<>();
        final Map<String, MPProtein> pid2Prot = new HashMap<>();

        /** write graph file **/
        final BufferedWriter graphWriter = Files.newBufferedWriter(Paths.get(graphFile), StandardCharsets.UTF_8);
        final FileReaderUtil reader = new FileReaderUtil(peptidesFile, "\t", true);
        Consumer<String> rowConsumer = (String line) -> {
            try {
                String[] tmp = line.split("\t");
                if (!tmp[reader.getColNum("Potential contaminant")].equals("+") || !delContaminant) {
                    String peptSeq = tmp[reader.getColNum("Sequence")];
                    double prob = 1 - Double.parseDouble(tmp[reader.getColNum("PEP")]);
                    String[] pidArr = tmp[reader.getColNum("Reverse")].equals("+")
                            ? tmp[reader.getColNum("Leading razor protein")].split(";")
                            : tmp[reader.getColNum("Proteins")].split(";");
                    // MPPeptide
                    MPPeptide mpPept = new MPPeptide();
                    mpPept.setSequence(peptSeq);
                    mpPept.setProb(prob);
                    if (mpPept.getAssociatedProteinSet() == null) {
                        mpPept.setAssociatedProteinSet(new HashSet<>());
                    }
                    MPProtein mpProt;
                    for (String pid : pidArr) {
                        if (pid2Prot.containsKey(pid)) {
                            mpProt = pid2Prot.get(pid);
                        } else {
                            mpProt = new MPProtein();
                            mpProt.setId(pid);
                            mpProt.setTarget(!pid.startsWith(decoyPrefix));
                            pid2Prot.put(pid, mpProt);
                        }
                        if (mpProt.getContainedPeptideSet() == null) {
                            mpProt.setContainedPeptideSet(new HashSet<>());
                        }
                        mpPept.getAssociatedProteinSet().add(mpProt);
                        mpProt.getContainedPeptideSet().add(mpPept);
                    }
                    mpPeptideSet.add(mpPept);
                    // write
                    graphWriter.write(String.join(System.lineSeparator(),
                            "e " + peptSeq,
                            Arrays.stream(pidArr)
                                    .peek(pid -> pidSet.add(pid))
                                    .map(pid -> "r " + pid)
                                    .collect(Collectors.joining(System.lineSeparator())),
                            "p " + prob + System.lineSeparator()

                    ));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
        reader.read(rowConsumer);
        graphWriter.close();
        mpProteinSet = new HashSet<>(pid2Prot.values());

        /** write target-decoy file **/
        // false => target, true => decoy
        Map<Boolean, String> targetDecoy = pidSet.parallelStream()
                .collect(Collectors.groupingBy((String pid) -> pid.startsWith(decoyPrefix),
                        Collectors.joining(" , "))
                );
        BufferedWriter tdWriter = Files.newBufferedWriter(Paths.get(targetDecoyFile), StandardCharsets.UTF_8);
        tdWriter.write("{ " + targetDecoy.get(false) + " }" + System.lineSeparator());
        tdWriter.write("{ " + targetDecoy.get(true) + " }" + System.lineSeparator());
        tdWriter.close();
    }

    /**
     * Convert MaxQuant peptides.txt file as fido input. Meanwhile, build peptide and protein set.
     * Fido needs two input files:
     * <p>
     * 1. graph file, like:
     * <PRE>
     * e peptide_string
     * r protein that would create this peptide using theoretical digest
     * r second protein
     * ...
     * r final protein
     * p probability of the peptide match to the spectrum (given by PeptideProphetEnhancer or comparable).
     * </PRE>
     * <p>
     * 2. target-decoy file
     * <PRE>
     * { target_prot1 , target_prot2 , ... target_protn }
     * { decoy_prot1 , decoy_prot2 , ... decoy_protn }
     * </PRE>
     *
     * @param peptidesFile
     * @param delContaminant
     * @param decoyPrefix
     * @param graphFile
     * @param targetDecoyFile
     * @throws IOException
     */
    public void convertToFido2(String peptidesFile,
                               boolean delContaminant,
                               String decoyPrefix,
                               String graphFile,
                               String targetDecoyFile) throws IOException {
        final Set<String> pidSet = new HashSet<>();
        mpPeptideSet = new HashSet<>();
        final Map<String, MPProtein> pid2Prot = new HashMap<>();

        /** write graph file **/
        final BufferedWriter graphWriter = Files.newBufferedWriter(Paths.get(graphFile), StandardCharsets.UTF_8);
        final FileReaderUtil reader = new FileReaderUtil(peptidesFile, "\t", true);
        Consumer<String> rowConsumer = (String line) -> {
            try {
                String[] tmp = line.split("\t");
                if (!tmp[reader.getColNum("Potential contaminant")].equals("+") || !delContaminant) {
                    String peptSeq = tmp[reader.getColNum("Sequence")];
                    double prob = Double.parseDouble(tmp[reader.getColNum("PEP")]);
                    String[] pidArr = tmp[reader.getColNum("Reverse")].equals("+")
                            ? tmp[reader.getColNum("Leading razor protein")].split(";")
                            : tmp[reader.getColNum("Proteins")].split(";");
                    // MPPeptide
                    MPPeptide mpPept = new MPPeptide();
                    mpPept.setSequence(peptSeq);
                    mpPept.setProb(prob);
                    if (mpPept.getAssociatedProteinSet() == null) {
                        mpPept.setAssociatedProteinSet(new HashSet<>());
                    }
                    MPProtein mpProt;
                    for (String pid : pidArr) {
                        if (pid2Prot.containsKey(pid)) {
                            mpProt = pid2Prot.get(pid);
                        } else {
                            mpProt = new MPProtein();
                            mpProt.setId(pid);
                            mpProt.setTarget(!pid.startsWith(decoyPrefix));
                            pid2Prot.put(pid, mpProt);
                        }
                        if (mpProt.getContainedPeptideSet() == null) {
                            mpProt.setContainedPeptideSet(new HashSet<>());
                        }
                        mpProt.getContainedPeptideSet().add(mpPept);
                    }
                    mpPeptideSet.add(mpPept);
                    // write
                    graphWriter.write(String.join(System.lineSeparator(),
                            "e " + peptSeq,
                            Arrays.stream(pidArr)
                                    .peek(pid -> pidSet.add(pid))
                                    .map(pid -> "r " + pid)
                                    .collect(Collectors.joining(System.lineSeparator())),
                            "p " + prob + System.lineSeparator()

                    ));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
        reader.read(rowConsumer);
        graphWriter.close();
        mpProteinSet = new HashSet<>(pid2Prot.values());

        /** write target-decoy file **/
        // false => target, true => decoy
        Map<Boolean, String> targetDecoy = pidSet.parallelStream()
                .collect(Collectors.groupingBy((String pid) -> pid.startsWith(decoyPrefix),
                        Collectors.joining(" , "))
                );
        BufferedWriter tdWriter = Files.newBufferedWriter(Paths.get(targetDecoyFile), StandardCharsets.UTF_8);
        tdWriter.write("{ " + targetDecoy.get(false) + " }" + System.lineSeparator());
        tdWriter.write("{ " + targetDecoy.get(true) + " }" + System.lineSeparator());
        tdWriter.close();
    }

    /**
     * @param peptidesFile
     * @param delContaminant
     * @param decoyPrefix
     * @param digester
     * @throws IOException
     */
    public void convertToPickedProtein(String peptidesFile,
                                       boolean delContaminant,
                                       String decoyPrefix,
                                       EnzymeDigester digester) throws IOException {
        mpPeptideSet = new HashSet<>();
        final Map<String, MPProtein> pid2Prot = new HashMap<>();
        digester.setMinPeptideLen(Integer.MAX_VALUE);
        digester.setMaxPeptideLen(-1);
        digester.setMaxMissedCleavageNum(-1);

        /** assemble MPPeptide and MPProtein **/
        final FileReaderUtil reader = new FileReaderUtil(peptidesFile, "\t", true);
        Consumer<String> rowConsumer = (String line) -> {
            String[] tmp = line.split("\t");
            if (!tmp[reader.getColNum("Potential contaminant")].equals("+") || !delContaminant) {
                String peptSeq = tmp[reader.getColNum("Sequence")];
                // set digester
                int peptLen = peptSeq.length();
                if (peptLen > digester.getMaxPeptideLen()) {
                    digester.setMaxPeptideLen(peptLen);
                }
                if (peptLen < digester.getMinPeptideLen()) {
                    digester.setMinPeptideLen(peptLen);
                }
                int mcNum = digester.obtainMissedCleavageNum(peptSeq);
                if (mcNum > digester.getMaxMissedCleavageNum()) {
                    digester.setMaxMissedCleavageNum(mcNum);
                }
                // probability
                double prob = 1 - Double.parseDouble(tmp[reader.getColNum("PEP")]);
                String[] pidArr = tmp[reader.getColNum("Reverse")].equals("+")
                        ? tmp[reader.getColNum("Leading razor protein")].split(";")
                        : tmp[reader.getColNum("Proteins")].split(";");
                // MPPeptide
                MPPeptide mpPept = new MPPeptide();
                mpPept.setSequence(peptSeq);
                mpPept.setProb(prob);
                if (mpPept.getAssociatedProteinSet() == null) {
                    mpPept.setAssociatedProteinSet(new HashSet<>());
                }
                MPProtein mpProt;
                for (String pid : pidArr) {
                    if (pid2Prot.containsKey(pid)) {
                        mpProt = pid2Prot.get(pid);
                    } else {
                        mpProt = new MPProtein();
                        mpProt.setId(pid);
                        mpProt.setTarget(!pid.startsWith(decoyPrefix));
                        pid2Prot.put(pid, mpProt);
                    }
                    if (mpProt.getContainedPeptideSet() == null) {
                        mpProt.setContainedPeptideSet(new HashSet<>());
                    }
                    mpPept.getAssociatedProteinSet().add(mpProt); // add proteins for the peptide
                    mpProt.getContainedPeptideSet().add(mpPept); // add peptide for the protein
                }
                mpPeptideSet.add(mpPept);
            }
        };
        reader.read(rowConsumer);
        mpProteinSet = new HashSet<>(pid2Prot.values());
    }
}
