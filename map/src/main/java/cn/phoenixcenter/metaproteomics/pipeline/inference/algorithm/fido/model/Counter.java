package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * To record whether a peptide group is present or not.
 * <PRE>
 * size: the size of a peptide group.
 * state: 0 is absent; 1 is present.
 * </PRE>
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Counter {

    private int size;

    private int state;

    public Counter(int size) {
        this.size = size;
        start();
    }

    public void start() {
        this.state = 0;
    }

    public boolean inRange() {
        return state <= size;
    }

    public void advance() {
        this.state++;
    }

    public static boolean inRange(Counter[] counters) {
        return counters[counters.length - 1].inRange();
    }

    public static void advance(Counter[] counters) {
        for (int i = 0; i < counters.length; i++) {
            counters[i].advance();
            if (!counters[i].inRange() && i != counters.length - 1) {
                counters[i].start();
            } else {
                break;
            }
        }
    }
}
