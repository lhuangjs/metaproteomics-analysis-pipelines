package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein;

import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.model.Peptide;
import cn.phoenixcenter.metaproteomics.utils.FileReaderUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by huangjs
 */
@Log4j2
@Getter
public class Preprocessing {

    private EnzymeDigester enzymeDigester;

    /**
     * The input file contains those field:
     * PSMId, score, q-value, posterior_error_prob, peptide, proteinIds(tab-delimited).
     * Those fields are separated by tab.
     *
     * @param targetPeptideFile
     * @param decoyPeptideFile
     * @return
     * @throws IOException
     */
    // TODO delete the method
    public List<Peptide> parsePercolatorTabFile(String targetPeptideFile, String decoyPeptideFile) throws IOException {
        Function<String[], Peptide> rowProcessor = (String[] fields) -> {
            Peptide peptide = new Peptide();
            peptide.setScore(Double.parseDouble(fields[1]));
            peptide.setPosteriorErrorProb(Double.parseDouble(fields[3]));
            // extract peptide sequence
            String peptSeq = fields[4];
            if (peptSeq.charAt(1) == '.') {
                peptSeq = peptSeq.substring(2);
            }
            if (peptSeq.charAt(peptSeq.length() - 2) == '.') {
                peptSeq = peptSeq.substring(0, peptSeq.length() - 2);
            }
            if (peptSeq.indexOf("[") != -1) {
                peptSeq = peptSeq.replaceAll("\\[.*\\]", "");
            }
            peptide.setSequence(peptSeq);
            // extract protein id
            peptide.setAsscociatedProtIdSet(
                    IntStream.range(5, fields.length)
                            .mapToObj(i -> fields[i])
                            .collect(Collectors.toSet())
            );
            return peptide;
        };
        // read target peptides
        FileReaderUtil fr = new FileReaderUtil(targetPeptideFile, "\t", true);
        List<Peptide> peptideList = fr.read(rowProcessor);
        int targetPeptCount = peptideList.size();
        // read decoy peptides
        fr = new FileReaderUtil(decoyPeptideFile, "\t", true);
        peptideList.addAll(fr.read(rowProcessor));
        log.info("target peptide count: {}; decoy peptide count: {}",
                targetPeptCount, peptideList.size() - targetPeptCount);
        return peptideList;
    }

    /**
     * Obtain peptide list from MqxQuant peptides.txt file and write result into json file.
     *
     * @param peptidesFile
     * @param fastaLibraryFile
     * @param delContaminant
     * @param reversePrefix
     * @param enzyme
     * @param outputFile
     * @throws IOException
     */
    public void parseMaxQuantPeptidesFile(String peptidesFile,
                                          String fastaLibraryFile,
                                          boolean delContaminant,
                                          String reversePrefix,
                                          EnzymeDigester.Enzyme enzyme,
                                          String outputFile) throws IOException {
        List<Peptide> peptideList = parseMaxQuantPeptidesFile(peptidesFile,
                fastaLibraryFile,
                delContaminant,
                reversePrefix,
                enzyme);
        Map<String, Object> result = new HashMap<>(2);
        result.put("enzymeDigester", enzymeDigester);
        result.put("peptideList", peptideList);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(new File(outputFile), result);
    }

    /**
     * Obtain peptide list from MqxQuant peptides.txt file
     *
     * @param peptidesFile
     * @param fastaLibraryFile
     * @param delContaminant
     * @param reversePrefix
     * @param enzyme
     * @return
     * @throws IOException
     */
    public List<Peptide> parseMaxQuantPeptidesFile(String peptidesFile,
                                                   String fastaLibraryFile,
                                                   boolean delContaminant,
                                                   String reversePrefix,
                                                   EnzymeDigester.Enzyme enzyme) throws IOException {
        /** read nodeData from maxquant **/
        log.debug("start to parse MaxQuant peptides.txt");
        enzymeDigester = new EnzymeDigester(enzyme);
        final FileReaderUtil fr = new FileReaderUtil(peptidesFile, "\t", true);
        final AtomicInteger usefulPeptCount = new AtomicInteger(0); // filtered peptide count
        final AtomicInteger contPeptCount = new AtomicInteger(0); // contaminant peptide count

        Predicate<String[]> rowDeleter = (String[] fields) -> {
            // remove peptide that "Potential contaminant" is "+"
            if (delContaminant && fields[fr.getColNum("Potential contaminant")].equals("+")) {
                contPeptCount.incrementAndGet();
                return true;
            }
            // remove peptide whose score is NaN
            if(fields[fr.getColNum("Score")].equals("NaN")){
                return true;
            }
            return false;
        };
        // obtain peptide info
        Function<String[], Peptide> rowProcessor = (String[] fields) -> {
            usefulPeptCount.incrementAndGet();
            Peptide peptide = new Peptide();
            String peptSeq = fields[fr.getColNum("Sequence")];
            int len = peptSeq.length();
            if (len > enzymeDigester.getMaxPeptideLen()) {
                enzymeDigester.setMaxPeptideLen(len);
            }
            if (len < enzymeDigester.getMinPeptideLen()) {
                enzymeDigester.setMinPeptideLen(len);
            }
            int missedCleavageNum = enzymeDigester.obtainMissedCleavageNum(peptSeq);
            if (missedCleavageNum > enzymeDigester.getMaxMissedCleavageNum()) {
                enzymeDigester.setMaxMissedCleavageNum(missedCleavageNum);
            }
            // peptide sequence
            peptide.setSequence(peptSeq);
            peptide.setScore(Double.parseDouble(fields[fr.getColNum("Score")]));
            peptide.setPosteriorErrorProb(Double.parseDouble(fields[fr.getColNum("PEP")]));
            return peptide;
        };
        // read peptides
        List<Peptide> peptideList = fr.read(rowDeleter, rowProcessor);

        /** map peptides to proteins: in maxquant, only the leading protein is saved
         * when a peptide match a decoy protein. So we need to remap peptides to proteins.
         */
        log.debug("remap peptides to proteins");
        Map<String, Peptide> seq2Peptide = peptideList.parallelStream()
                .collect(Collectors.toMap(
                        pept -> pept.getSequence(),
                        pept -> pept
                ));
        Map<String, Set<String>> pept2ProtSet = mapPept2Prot(seq2Peptide.keySet(), fastaLibraryFile,
                true, reversePrefix, false);
        seq2Peptide.entrySet().parallelStream()
                .forEach(e -> e.getValue().setAsscociatedProtIdSet(pept2ProtSet.get(e.getKey())));
        log.info("total peptide count: {}; potential contaminant peptide count: {}; useful peptide count: {}",
                usefulPeptCount.get() + contPeptCount.get(), contPeptCount.get(), usefulPeptCount.get());
        log.info("enzyme: {}; minimum peptide length: {}; maximum peptide length: {}; maximum missed cleavage: {}",
                enzymeDigester.getEnzyme(), enzymeDigester.getMinPeptideLen(),
                enzymeDigester.getMaxPeptideLen(), enzymeDigester.getMaxMissedCleavageNum());
        return peptideList;
    }

    /**
     * Map peptide sequence to its protein
     *
     * @param peptSet
     * @param fastaLibrary
     * @param genDecoyLibrary
     * @param reversePrefix
     * @param equateIL
     * @return peptide sequence => protein id set contains the peptide
     * @throws IOException
     */
    public Map<String, Set<String>> mapPept2Prot(Set<String> peptSet,
                                                 String fastaLibrary,
                                                 boolean genDecoyLibrary,
                                                 String reversePrefix,
                                                 boolean equateIL) throws IOException {
        Map<String, Set<String>> pept2ProtSet = peptSet.parallelStream()
                .collect(Collectors.toMap(
                        pept -> pept,
                        pept -> new HashSet<>()
                ));
        Map<String, String> pept2EqILPept = null;
        if (equateIL) {
            pept2EqILPept = peptSet.parallelStream()
                    .collect(Collectors.toMap(
                            pept -> pept,
                            pept -> pept.replace("I", "L")
                            // TODO add (k,v) -> v
                    ));
        }
        BufferedReader br = new BufferedReader(new FileReader(fastaLibrary));
        String line;
        long count = 0;
        String pid = null;
        StringBuilder sequence = new StringBuilder();
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">")) {
                if (pid != null) {
                    if (++count % 10000 == 0) {
                        log.debug("{} protein sequences have been processed", count);
                    }
                    // map peptide to protein
                    Set<AbstractMap.SimpleEntry<String, String>> containedPeptSet = getContainedPeptSet(pid, sequence,
                            peptSet, pept2EqILPept, genDecoyLibrary, reversePrefix, equateIL);
                    for (AbstractMap.SimpleEntry<String, String> pept : containedPeptSet) {
                        pept2ProtSet.get(pept.getKey()).add(pept.getValue());
                    }
                    // reset
                    sequence = sequence.delete(0, sequence.length());
                }
                pid = line.split("\\s", 2)[0].substring(1);
            } else {
                sequence.append(line);
            }
        }
        br.close();
        // map peptide to protein
        Set<AbstractMap.SimpleEntry<String, String>> containedPeptSet = getContainedPeptSet(pid, sequence,
                peptSet, pept2EqILPept, genDecoyLibrary, reversePrefix, equateIL);
        for (AbstractMap.SimpleEntry<String, String> pept : containedPeptSet) {
            pept2ProtSet.get(pept.getKey()).add(pept.getValue());
        }
        log.info("{} protein sequences have been processed", ++count);
        return pept2ProtSet;
    }

    /**
     * Obtain peptides matched with sequence in peptSet or eqILPeptSet.
     *
     * @param pid
     * @param sequence
     * @param peptSet
     * @param pept2EqILPept
     * @param genDecoyLibrary
     * @param reversePrefix
     * @param equateIL
     * @return original peptide sequence => protein id or "reversePrefix + protein id"
     */
    private Set<AbstractMap.SimpleEntry<String, String>> getContainedPeptSet(String pid,
                                                                             StringBuilder sequence,
                                                                             Set<String> peptSet,
                                                                             Map<String, String> pept2EqILPept,
                                                                             boolean genDecoyLibrary,
                                                                             String reversePrefix,
                                                                             boolean equateIL) {
        final Set<AbstractMap.SimpleEntry<String, String>> containedPeptSet;
        final String protSeq = sequence.toString();
        final String reverseProtSeq = genDecoyLibrary ? sequence.reverse().toString() : null;
        containedPeptSet = peptSet.stream()
                .map(pept -> {
                    if (protSeq.indexOf(pept) != -1) {
                        return new AbstractMap.SimpleEntry<String, String>(pept, pid);
                    }
                    if (genDecoyLibrary && reverseProtSeq.indexOf(pept) != -1) {
                        return new AbstractMap.SimpleEntry<String, String>(pept, reversePrefix + pid);
                    }
                    return null;
                })
                .filter(e -> e != null)
                .collect(Collectors.toSet());
        if (equateIL) {
            final String eqILProtSeq = sequence.toString();
            final String eqILReverseProtSeq = genDecoyLibrary ? sequence.reverse().toString() : null;
            containedPeptSet.addAll(
                    pept2EqILPept.entrySet().stream()
                            .map(e -> {
                                if (eqILProtSeq.indexOf(e.getValue()) != -1) {
                                    return new AbstractMap.SimpleEntry<String, String>(e.getKey(), pid);
                                }
                                if (genDecoyLibrary && eqILReverseProtSeq.indexOf(e.getValue()) != -1) {
                                    return new AbstractMap.SimpleEntry<String, String>(e.getKey(), reversePrefix + pid);
                                }
                                return null;
                            })
                            .filter(e -> e != null)
                            .collect(Collectors.toSet())
            );
        }
        return containedPeptSet;
    }

    public List<Peptide> parseGenericJsonFile(String jsonFile) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(new File(jsonFile));
        List<Peptide> peptideList = objectMapper.readValue(jsonNode.get("peptideList").toString(),
                new TypeReference<List<Peptide>>() {
                });
        enzymeDigester = objectMapper.readValue(jsonNode.get("enzymeDigester").toString(), EnzymeDigester.class);
        return peptideList;
    }
}