package cn.phoenixcenter.metaproteomics.pipeline.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by huangjs
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EnzymeDigester {

    private Enzyme enzyme = Enzyme.TRYPSIN;

    private int minPeptideLen = Integer.MAX_VALUE;

    private int maxPeptideLen = 0;

    private int maxMissedCleavageNum = 0;

    public enum Enzyme {
        TRYPSIN;

        public Pattern getDigestionPattern() {
            switch (this) {
                case TRYPSIN:
                    return Pattern.compile("(?<=[KR])(?!P)");
            }
            return null;
        }
    }

    public EnzymeDigester(Enzyme enzyme) {
        this.enzyme = enzyme;
    }

    public EnzymeDigester(int maxMissedCleavageNum, int minPeptideLen, int maxPeptideLen) {
        this.enzyme = Enzyme.TRYPSIN;
        this.maxMissedCleavageNum = maxMissedCleavageNum;
        this.minPeptideLen = minPeptideLen;
        this.maxPeptideLen = maxPeptideLen;
    }

    /**
     * Do theoretical digestion
     *
     * @param protSequence
     * @return peptide sequence => whether the peptide exists the N-terminal Met
     */
    public List<AbstractMap.SimpleEntry<String, Boolean>> digest(String protSequence) {
        // entry: peptide sequence => whether the N-terminal Met is digested for the peptide
        List<AbstractMap.SimpleEntry<String, Boolean>> peptideList = new ArrayList<>();
        // do theoretical full digestion
        String[] peptides = enzyme.getDigestionPattern().split(protSequence);
        for (int i = 0; i < peptides.length; i++) {
            generatePeptide(peptides[i], i, peptides, 0, peptideList);
        }
        return peptideList;
    }

    private void generatePeptide(String peptide, int currIndex, String[] peptides, int missedCleavageNum,
                                 List<AbstractMap.SimpleEntry<String, Boolean>> peptideList) {
        int len = peptide.length();
        if (missedCleavageNum <= maxMissedCleavageNum && len <= maxPeptideLen) {
            if (len >= minPeptideLen) {
                boolean existNTermMet = false;
                if (currIndex == 0 && peptide.startsWith("M") && len - 1 >= minPeptideLen) {
                    existNTermMet = true;
                }
                peptideList.add(new AbstractMap.SimpleEntry(peptide, existNTermMet));
            }

            if (currIndex > 0) {
                generatePeptide(peptides[currIndex - 1] + peptide,
                        currIndex - 1,
                        peptides,
                        ++missedCleavageNum,
                        peptideList);
            }
        }
    }

    /**
     * Obtain the number of missed cleavage sites for input peptide.
     * Note: the enzyme must be set before using.
     *
     * @param peptSeq
     * @return
     */
    public int obtainMissedCleavageNum(String peptSeq) {
        return enzyme.getDigestionPattern().split(peptSeq).length - 1;
    }
}
