package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.*;

/**
 * Created by huangjs
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Protein {

    private String id;

    private boolean isTarget;

    private double score;

    private double posteriorErrorProb;

    private double qValue;

    // TODO confine
    private double minFDR;

    private Set<Peptide> containedPeptSet;

    public Protein(String id) {
        this.id = id;
    }

    public void addPeptide(Peptide pept) {
        if (containedPeptSet == null) {
            containedPeptSet = new HashSet<>();
        }
        containedPeptSet.add(pept);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Protein protein = (Protein) o;
        return id.equals(protein.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
