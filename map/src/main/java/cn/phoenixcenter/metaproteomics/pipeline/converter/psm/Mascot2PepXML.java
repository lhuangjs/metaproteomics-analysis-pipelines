/*
 * TODO description
 */

/*

Program       : Mascot2XML converter to pepXML
Author        : Andrew Keller <akeller@systemsbiology.org>
Date          : 11.27.02
SVN Version   : $Id: MascotConverter.cpp 7829 2018-10-12 22:14:25Z dshteyn $

Copyright (C) 2003 Andrew Keller

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Andrew Keller
Institute for Systems Biology
401 Terry Avenue North
Seattle, WA  98109  USA
akeller@systemsbiology.org

*/
package cn.phoenixcenter.metaproteomics.pipeline.converter.psm;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.regex.Pattern;


/**
 * Created by huangjs
 * <p>
 * Convert mascot dat file to pepXML format
 */

@Log4j2
public class Mascot2PepXML {

    private final String SECTION_START = "Content-Type: application/x-Mascot; name=\"";

    private final String Section_END = "--gc0p4Jq0M2Yt08jU534c0p";

    private final double MIN_PERCENTAGE = 0.75;

    private String decoyPrefix;

    private String datFileName;

    // amino acid => fixed masses, which may or may not be modified
    private Map<Character, Double> workingAAMasses;

    // amino acid => corresponding workingAAMasses entry modified
    private Map<Character, Boolean> aa2Mod;

    private List<MascotModification> staticModList;

    private List<MascotModification> variableModList;

    private EnzymeInfo enzymeInfo;

    // average unless proven otherwise
    private int massType;

    private enum Section {
        PARAMETERS("parameters"),
        MASSES("masses"),
        UNIMOD("unimod"),
        ENZYME("enzyme"),
        HEADER("header"),
        SUMMARY("summary"),
        DECOY_SUMMARY("decoy_summary"),
        PEPTIDES("peptides"),
        DECOY_PEPTIDES("decoy_peptides"),
        PROTEINS("proteins"),
        QUERY("query");

        private String section;

        Section(String section) {
            this.section = section;
        }

        @Override
        public String toString() {
            return this.section;
        }

        public static Section str2Section(String sectionStr) {
            if (sectionStr.startsWith("query")) {
                return Section.QUERY;
            }
            for (Section s : Section.values()) {
                if (sectionStr.equals(s.toString())) {
                    return s;
                }
            }
            return null;
        }
    }

    public Mascot2PepXML() {
        workingAAMasses = new HashMap<>();
        aa2Mod = new HashMap<>();
        staticModList = new ArrayList<>();
        variableModList = new ArrayList<>();
        massType = 0;
        for (char i = 'A'; i <= 'Z'; i++) {
            workingAAMasses.put(i, -1.0);
            aa2Mod.put(i, false);
        }
        workingAAMasses.put('n', -1.0);
        aa2Mod.put('n', false);
        workingAAMasses.put('c', -1.0);
        aa2Mod.put('c', false);
    }

    /**
     * parameters, masses, unimod, enzyme, header, summary, decoy_summary, peptides, decoy_peptides, proteins,
     * queryN, index
     *
     * @param decoyPrefix
     * @param libraryFile
     * @param datFile
     * @param completeLibOutput
     * @param pepXMLOutput
     * @throws XMLStreamException
     * @throws IOException
     */
    public void convert(String decoyPrefix, String libraryFile, String datFile,
                        String completeLibOutput, String pepXMLOutput) throws IOException, XMLStreamException {
        this.decoyPrefix = decoyPrefix;
        /** check parameters **/
        if (!datFile.endsWith(".dat")) {
            log.error("Please name input mascot file as *.dat", datFile);
            System.exit(1);
        }
        Path datFilePath = Paths.get(datFile).toAbsolutePath();
        if (Files.notExists(datFilePath)) {
            log.error("Mascot dat file{} did not exist", datFile);
            System.exit(1);
        }
        Path libraryFilePath = Paths.get(libraryFile).toAbsolutePath();
        if (Files.notExists(libraryFilePath)) {
            log.error("Library file{} did not exist", libraryFile);
            System.exit(1);
        }

        /** generate library with target and decoy sequences **/
        genCompleteLibrary(libraryFile, completeLibOutput);

        /** init params **/
        String datFilePathStr = datFilePath.toString();
        String baseName = datFilePathStr.substring(0, datFilePathStr.length() - 4); // .dat = 4
        datFileName = datFilePath.getFileName().toString();
        datFileName = datFileName.substring(0, datFileName.length() - 4);
        List<Map.Entry<String, String>> paramList = null;
        SpectrumQuery[] targetQueries = null;
        SpectrumQuery[] decoyQueries = null;
        int index = 1;

        /** read **/
        Path pepXMLOutputPath = Paths.get(pepXMLOutput);
        OutputStream os = Files.newOutputStream(pepXMLOutputPath,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        XMLOutputFactory factory = XMLOutputFactory.newFactory();
        XMLStreamWriter writer = factory.createXMLStreamWriter(os);
        writer.writeStartDocument("UTF-8", "1.0");
        writer.writeCharacters(System.lineSeparator());
        // msms_pipeline_analysis
        writer.writeStartElement("msms_pipeline_analysis");
        writer.writeAttribute("xmlns", "http://regis-web.systemsbiology.net/pepXML");
        writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        writer.writeAttribute("xsi:schemaLocation", "http://regis-web.systemsbiology.net/pepXML " +
                "http://sashimi.sourceforge.net/schema_revision/pepXML/pepXML_v122.xsd");
        writer.writeAttribute("summary_xml", pepXMLOutputPath.toAbsolutePath().toString());
        writer.writeCharacters(System.lineSeparator());
        // msms_run_summary
        writer.writeStartElement("msms_run_summary");
        writer.writeAttribute("base_name", baseName);
        writer.writeAttribute("raw_data_type", "raw");
        writer.writeAttribute("raw_data", "");
        writer.writeCharacters(System.lineSeparator());

        /** read dat file **/
        BufferedReader br = Files.newBufferedReader(Paths.get(datFile), StandardCharsets.UTF_8);
        String line;
        while ((line = br.readLine()) != null) {
            if (line.startsWith(SECTION_START)) {
                String sectionStr = line.substring(SECTION_START.length(), line.lastIndexOf("\""));
                Section section = Section.str2Section(sectionStr);
                if (section == Section.QUERY) {
                    int queryIndex = Integer.parseInt(sectionStr.substring(5));
                    SpectrumQuery query = parseQuery(br, queryIndex, targetQueries, decoyQueries);
                    if (query != null) {
                        writeSpectrumQuery(index, query, writer);
                        index++;
                    }
                } else if (section == Section.SUMMARY) {
                    parseSummary(br, targetQueries);
                } else if (section == Section.DECOY_SUMMARY) {
                    parseSummary(br, decoyQueries);
                } else if (section == Section.PEPTIDES) {
                    parsePeptides(br, targetQueries, true);
                } else if (section == Section.DECOY_PEPTIDES) {
                    parsePeptides(br, decoyQueries, false);
                } else if (section == Section.PARAMETERS) {
                    paramList = parseParameters(br);
                } else if (section == section.MASSES) {
                    parseMass(br);
                } else if (section == section.ENZYME) {
                    enzymeInfo = parseEnzyme(br);
                    writeSampleEnzyme(enzymeInfo, writer);
                    writeSearchSummary(baseName, completeLibOutput, enzymeInfo, paramList, writer);
                    paramList = null;
                } else if (section == Section.HEADER) {
                    int queryCount = parseHeader(br);
                    targetQueries = new SpectrumQuery[queryCount];
                    decoyQueries = new SpectrumQuery[queryCount];
                }
            }
        }
        writer.writeEndElement(); // msms_run_summary
        writer.writeCharacters(System.lineSeparator());
        writer.writeEndElement(); // msms_pipeline_analysis
        writer.writeCharacters(System.lineSeparator());
        writer.writeEndDocument();
        br.close();
        writer.close();
    }

    private void genCompleteLibrary(String libraryFile, String completeLibOutput) throws IOException {
        log.info("Start to build library with target and decoy sequences");
        BufferedReader br = Files.newBufferedReader(Paths.get(libraryFile), StandardCharsets.UTF_8);
        BufferedWriter bw = Files.newBufferedWriter(Paths.get(completeLibOutput), StandardCharsets.UTF_8,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        String line;
        String header = null;
        StringBuilder sequence = new StringBuilder();
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">")) {
                if (header != null) {
                    // target
                    bw.write(header + System.lineSeparator());
                    bw.write(sequence.toString() + System.lineSeparator());
                    // decoy
                    bw.write(">" + decoyPrefix + header.substring(1) + System.lineSeparator());
                    bw.write(sequence.reverse().toString() + System.lineSeparator());
                    // reset
                    sequence.delete(0, sequence.length());
                }
                header = line;
            } else {
                sequence.append(line);
            }
        }
        // the last
        // target
        bw.write(header + System.lineSeparator());
        bw.write(sequence.toString() + System.lineSeparator());
        // decoy
        bw.write(">" + decoyPrefix + header.substring(1));
        bw.write(sequence.reverse().toString() + System.lineSeparator());
        br.close();
        bw.close();
        log.info("Finish building library with target and decoy sequences");
    }

    /**
     * @param br
     * @throws IOException
     * @throws XMLStreamException
     */
    private List<Map.Entry<String, String>> parseParameters(BufferedReader br) throws IOException {
        List<Map.Entry<String, String>> params = new ArrayList<>();
        String line;
        while (!(line = br.readLine()).equals(Section_END)) {
            String[] tmp = line.split("=");
            String name = tmp[0].trim();
            if (name.length() > 0) {
                String value = tmp.length == 2 ? tmp[1].trim() : "";
                if (name.equals("MASS") && value.equals("Monoisotopic")) {
                    massType = 1;
                }
                params.add(new AbstractMap.SimpleEntry<>(name, value));
            }
        }
        return params;
    }

    /**
     * <sample_enzyme name = "trypsin" >
     * <specificity cut = "KR" no_cut = "P" sense = "C" / >
     * </sample_enzyme >
     * Title:Trypsin
     * Cleavage:KR
     * Restrict:P
     * Cterm
     *
     * @param br
     * @return
     * @throws IOException
     */
    private EnzymeInfo parseEnzyme(BufferedReader br) throws IOException {
        String line;
        EnzymeInfo enzymeInfo = new EnzymeInfo();
        while (!(line = br.readLine()).equals(Section_END)) {
            if (line.startsWith("Title:")) {
                String name = line.substring(6).toLowerCase();
                if (name.startsWith("semi")) {
                    name = name.substring(4);
                    enzymeInfo.setMinTerminalNum(1);
                } else {
                    enzymeInfo.setMinTerminalNum(2);
                }
                enzymeInfo.setName(name);
            } else if (line.startsWith("Cleavage:")) {
                enzymeInfo.setCut(line.substring(9));
                enzymeInfo.setMaxInternalCleavages(enzymeInfo.getCut().length());
            } else if (line.startsWith("Restrict:")) {
                enzymeInfo.setNoCut(line.substring(9));
            } else if (line.endsWith("term")) {
                enzymeInfo.setSense(line.substring(0, 1));
                return enzymeInfo;
            }
        }
        return null;
    }

    private void parseMass(BufferedReader br) throws IOException {
        int fixedModNumber = -1;
        double fixedModMass = -1.0;
        String fixedModDesc = null;
        String line;
        while (!(line = br.readLine()).equals(Section_END)) {
            char[] chars = line.toCharArray();
            if (chars.length > 2 && chars[1] == '=' && chars[0] >= 'A' && chars[0] <= 'Z') {
                /** single amino acid entry **/
                // jmt: use these single AA masses instead of ResidueMass.cxx
                // entries although they may be modified values, know that the
                // modifications are listed in the "FixedMod" entries that
                // will follow.  Bookkeeping for fixed mods is dealt with
                // there.
                double mass = Double.parseDouble(line.substring(2));
                recordStaticAAMass(chars[0], mass);
                log.debug("{} is stored as {}", chars[0], mass);
            } else if (line.startsWith("C_term=") || line.startsWith("N_term=")) {
                /** N-terminal or C-terminal BASE (unmodded) mass **/
                char term = chars[0] == 'C' ? 'c' : 'n';
                double mass = Double.parseDouble(line.substring(7));
                recordStaticAAMass(term, mass);
                log.debug("{} is stored as {}", chars[0], mass);
            } else if (line.startsWith("delta")) {
                /** variable modificiations, link [delta1=42.010565,Acetyl (Protein N-term)] **/
                int commaIndex = line.indexOf(",");
                int equalSignIndex = line.indexOf("=");
                double variableModMass = Double.parseDouble(line.substring(equalSignIndex + 1, commaIndex));
                String modName = line.substring(commaIndex + 1);
                int modNumber = Integer.parseInt(line.substring(5, equalSignIndex));
                log.debug("variable modification {}: delta mass {} for {}",
                        modNumber, variableModMass, modName);
                // extract modification info
                boolean[] terminals = new boolean[3];
                String residues = extractModificationInfo(modName, terminals);
                boolean isNTerminal = terminals[0];
                boolean isCTerminal = terminals[1];
                boolean isProtTerminal = terminals[2];
                log.debug("residues: {}: N-terminal mod: {}, C-terminal mod: {}, peptide-terminal mod: {}",
                        residues, isNTerminal, isCTerminal, isProtTerminal);
                // go through all the affected residues
                char[] residuesArr = residues.toCharArray();
                for (int i = 0; i < residuesArr.length; i++) {
                    // TODO 应该删除么？
                    if (residuesArr[i] == 'n' && isProtTerminal) {
                        residuesArr[i] = '1';
                    } else if (residuesArr[i] == 'c' && isProtTerminal) {
                        residuesArr[i] = '2';
                    }
                    recordVariableAAMod(residuesArr[i], variableModMass, isNTerminal, isCTerminal, modNumber);
                    log.debug("added variable modification for {}", residuesArr[i]);
                }
            } else if (line.startsWith("FixedMod") && chars[8] != 'R') {
                /** fixed modifications, like[FixedMod1=57.021464,Carbamidomethyl (C)].
                 *  Note: do not pick up FixedModResidues here!
                 */
                 /*This is only here for completeness.  Fixed (static)
                 modifications should have already been directly entered in
                 the single AA list above.  The value for the mod in this
                 location should add to the ResidueMass base AA mass to equal
                 the (hopefully already recorded) static AA mod above.*/
                // find the equals sign, and grab the number of the fixed mod
                fixedModNumber = Integer.parseInt(line.substring(8, line.indexOf("=")));
                fixedModMass = Double.parseDouble(line.substring(line.indexOf("=") + 1, line.indexOf(",")));
                fixedModDesc = line.substring(line.indexOf(","));
                log.debug("Fixed modification: {}th: mass = {}, desc = {}",
                        fixedModNumber, fixedModMass, fixedModDesc);
            } else if (line.startsWith("FixedModResidues")) {
                /** fixed modification residues **/
                /*The value for the modification in this location should add to the
                ResidueMass base AA mass to equal the (hopefully already
                recorded) static AA modification above.*/
                int fmrNumber = Integer.parseInt(line.substring(16, line.indexOf("=")));
                if (fmrNumber != fixedModNumber) {
                    log.error("error parsing FixedModResidues: {}", line);
                    log.error("Expected fixed mod number = {}; Actual fixed mod number = {}",
                            fixedModNumber, fmrNumber);
                    System.exit(1);
                }
                // extractModificationInfo
                boolean[] terminals = new boolean[3];
                String residues = extractModificationInfo(fixedModDesc, terminals);
                boolean isNTerminal = terminals[0];
                boolean isCTerminal = terminals[1];
                boolean isProtTerminal = terminals[2];
                log.debug("residues: {}, N-terminal: {}, C-terminal: {}, peptide-terminal: {}",
                        isNTerminal, isCTerminal, isProtTerminal);
                // go through all the affected residues
                char[] residuesArr = residues.toCharArray();
                for (int i = 0; i < residuesArr.length; i++) {
                    recordStaticAAMod(residuesArr[i], fixedModMass, isNTerminal, isCTerminal);
                    log.debug("added fixed modification for {}", residuesArr[i]);
                }

            }
        }
    }

    /**
     * The main aim: extract query count
     *
     * @param br
     * @return
     * @throws IOException
     */
    private int parseHeader(BufferedReader br) throws IOException {
        String line;
        while (!(line = br.readLine()).equals(Section_END)) {
            // queries=164413
            if (line.startsWith("queries")) {
                return Integer.parseInt(line.substring(line.indexOf("=") + 1));
            }
        }
        return -1;
    }

    private void parseSummary(BufferedReader br, SpectrumQuery[] queries) throws IOException {
        /*
        qmass1=758.137974
        qexp1=380.076263,2+
        qintensity1=74180.2031
        qmatch1=0
        qplughole1=0.000000
        */
        String line;
        int index = 0;
        while (!(line = br.readLine()).equals(Section_END)) {
            if (line.startsWith("qexp")) {
                queries[index] = new SpectrumQuery();
                queries[index].setAssumedCharge(Integer.parseInt(line.substring(line.lastIndexOf(",") + 1,
                        line.lastIndexOf("+"))));
            } else if (line.startsWith("qmatch")) {
                int matchedPeptMass = Integer.parseInt(line.substring(line.indexOf("=") + 1));
                queries[index].setIdentityScore(10 * Math.log10(matchedPeptMass));
            } else if (line.startsWith("qplughole")) {
                queries[index].setHomologyScore(Double.parseDouble(line.substring(line.indexOf("=") + 1)));
                index++;
            }
        }
    }

    private void parsePeptides(BufferedReader br, SpectrumQuery[] queries, boolean isTarget) throws IOException {
        /*
        q1_p1=-1
        q2_p1_db=01
        q2_p1=1,1137.548813,-0.002921,5,AYPEVMKER,14,00000020000,8.63,0002002000000000000,0,0;"Am2_A0A0E0YIF9":0:210:218:1
        q2_p1_terms=-,Y
        q2_p1_primary_nl=00000010000
        q2_p2_db=01
        q2_p2=1,1137.544815,0.001077,6,MTTLEDRTR,33,02000000000,7.50,0002002000000000000,0,0;"DVH_Q729Z3":0:18:26:1
        q2_p2_terms=R,-
        q2_p2_primary_nl=02000000000
        q5_p2_db=010101
        q5_p2=0,758.367096,0.004582,3,AVDANNR,16,000000000,7.07,0000012000000000000,0,0;"K12_P0CF89":0:106:112:1,"K12_P0CF88":0:106:112:1,"K12_P0CF90":0:106:112:1
        q5_p2_terms=K,A:K,A:K,A
        */
        String line;
        int qIdx = 0;
        int numIonSeries = 2; // b and y
        Pattern altPat = Pattern.compile("^q\\d+_p[2-9][0-9]*=");
        // TODO delete ?
        double proton = 1.007826035; // add proton!
        double minIonsScorePct = 0.0;
        SearchHit searchHit = null;
        Protein[] proteins = null;
        boolean processed = false;
        String prefix = "q" + (qIdx + 1) + "_" + "p1"; // qn_p1
        while (!(line = br.readLine()).equals(Section_END)) {
            if (line.startsWith(prefix + "=")) {
                if (line.equals(prefix + "=-1")) { // -1 indicates there is no match
                    // q1_p1=-1
                    queries[qIdx] = null;
                    // update prefix
                    qIdx++;
                    prefix = "q" + (qIdx + 1) + "_" + "p1";
                } else {
                    // q2_p1=1,1137.548813...
                    SpectrumQuery currQuery = queries[qIdx];
                    int idx = line.indexOf(";");
                    String[] tmp = line.substring(prefix.length() + 1, idx).split(",");
                    searchHit = new SearchHit();
                    currQuery.setSearchHit(searchHit);
                    searchHit.setRank(1);
                    searchHit.setMissedCleavageNum(tmp[0]);
                    searchHit.setMatchedIonsNum(Integer.parseInt(tmp[3]));
                    searchHit.setPeptide(tmp[4]);
                    searchHit.setIonsScore(Double.parseDouble(tmp[7]));
                    searchHit.setStar(0); // Maybe it will be updated later
                    // tot_num_ions
                    int totNumIons = (searchHit.getPeptide().length() - 1) * numIonSeries;
                    if (currQuery.getAssumedCharge() > 2) {
                        totNumIons *= 2;
                    }
                    searchHit.setTotIonsNum(totNumIons);
                    // precursor_neutral_mass(spectrum_query)
                    double peptideMr = Double.parseDouble(tmp[1]);
                    double delta = Double.parseDouble(tmp[2]);
                    currQuery.setPrecursorNeutralMass(peptideMr + delta + proton - 1);
                    searchHit.setNeutralPepMass(peptideMr + proton - 1); // calc_neutral_pep_mass
                    searchHit.setMassDiff(delta > 0.0 ? "+" + delta : String.valueOf(delta));  // massdiff
                    // search_score: expect
                    // ASMS Workshop and User Meeting 2005
                    // as defined in http://www.matrixscience.com/pdf/2005WKSHP4.pdf
                    // E = P{threshold} * (10 ** ((S{threshold}-score)/ 10))
                    searchHit.setExpect(0.05 * Math.pow(10.0,
                            (currQuery.getIdentityScore() - searchHit.getIonsScore()) / 10.0)
                    );
                    // modification
                    ModificationInfo modInfo = parseModificationInfo(tmp[4], tmp[6]);
                    currQuery.setModificationInfo(modInfo);
                    // proteins
                    proteins = Arrays.stream(line.substring(idx + 1).split(","))
                            .map(str -> str.substring(str.indexOf("\"") + 1, str.lastIndexOf("\"")))
                            .map(pid -> {
                                Protein prot = new Protein();
                                prot.setId(isTarget ? pid : decoyPrefix + pid);
                                return prot;
                            })
                            .toArray(Protein[]::new);
                    searchHit.setProteins(proteins);
                    // reset
                    processed = false;
                }
            } else if (line.startsWith(prefix + "_terms=")) {
                String peptSeq = searchHit.getPeptide();
                // q5_p2_terms=K,A:K,A:K,A
                char[] residues = line.substring(prefix.length() + 7).toCharArray();
                for (int i = 0; i < proteins.length; i++) {
                    char prevAA = residues[i * 4];
                    char nextAA = residues[i * 4 + 2];
                    proteins[i].setPrevAA(prevAA);
                    proteins[i].setNextAA(nextAA);
                    proteins[i].setTolTermNum(getTolTermNum(prevAA, peptSeq, nextAA));
                }
                // update prefix
                qIdx++;
                prefix = "q" + (qIdx + 1) + "_" + "p1";
            } else if (!processed && altPat.matcher(line).find()) {
                // alternative: q2_p2=1,1137.544815,0.001077,
                String[] tmp = line.split(",", 9);
                String altPept = tmp[4];
                double altIonsScore = Double.parseDouble(tmp[7]);
                // I dont't know why it doesn't loop if
                // "altIonsScore < searchHit.getIonsScore() * minIonsScorePct".
                // I only code this according to the source code logic
                if (altIonsScore >= searchHit.getIonsScore() * minIonsScorePct) {
                    if (homologous(searchHit.getPeptide(), altPept)) {
                        searchHit.setStar(1);
                        processed = true;
                    }
                } else {
                    processed = true;
                }
            }
        }
    }

    private SpectrumQuery parseQuery(BufferedReader br, int queryIndex,
                                     SpectrumQuery[] targetQueries, SpectrumQuery[] decoyQueries) throws IOException {
        /*
        title=Run1_U1_2000ng%2e10375%2e10375%2e%20File%3a%22Run1_U1_2000ng%2eraw%22%2c%20NativeID%3a%22controllerType%3d0%20controllerNumber%3d1%20scan%3d10375%22
        rtinseconds=2585.4192
        index=8180
        charge=2+,3+,4+
        mass_min=53.351551
        mass_max=716.688782
        int_min=4469
        int_max=6.314e+05
        num_vals=111
        num_used1=-1
        Ions1=86.096977:6.314e+05,185.164780:2.639e+05,304.161255:8.662e+04,407.228546:2.612e+04,546.300232:1.43e+05,560.322082:8744,659.383240:1.171e+04,72.081474:5.151e+05,175.118881:1.615e+05,333.186737:5.364e+04,432.257111:2.137e+04,488.223785:1.189e+05,645.346435:5522,716.688782:5837,70.065857:5.146e+05,233.127914:4.453e+04,310.175568:2.844e+04,431.204041:2.039e+04,460.251709:7.281e+04,684.064453:5089,84.081360:3.306e+05,218.150360:4.227e+04,316.161438:2.454e+04,403.230835:1.694e+04,547.303223:3.766e+04,129.102402:2.705e+05,213.159607:3.657e+04,286.150787:2.074e+04,529.273498:2.945e+04,84.044952:1.54e+05,223.107651:3.448e+04,331.233643:1.151e+04,489.224945:1.807e+04,136.075806:1.447e+05,187.071182:3.13e+04,327.201111:1.123e+04,530.299866:9064,102.055382:1.446e+05,186.123642:2.439e+04,281.078095:1.11e+04,515.349853:8099,147.112823:1.029e+05,216.097855:2.257e+04,269.124268:1.091e+04,518.261231:7100,130.086456:6.102e+04,186.168213:2.054e+04,287.133911:7212,521.311462:6907,53.351551:4671,55.055107:3.661e+04,56.050365:2.601e+04,56.437325:4570,60.056435:1.211e+04,63.927166:4.968e+04,69.070496:2.046e+04,71.069191:7708,73.084816:9959,74.060753:2.992e+04,80.691574:5232,85.028969:5481,87.055916:4.412e+04,87.100227:1.287e+04,88.039803:1.255e+04,101.071365:9472,101.107636:1.804e+04,104.580803:4469,106.194122:4623,108.994827:4652,110.071549:6190,112.087143:1.216e+04,113.070717:7848,115.086662:2.377e+04,116.070450:1.152e+04,117.048042:4867,120.080864:1.158e+04,130.050186:2.346e+04,130.106033:1.044e+04,137.078903:5931,141.065353:8020,141.102066:1.142e+04,145.097473:4.081e+04,157.097153:1.172e+04,157.133453:7406,158.092667:9217,159.076324:8379,170.044128:1.083e+04,171.113068:1.972e+04,171.149033:8356,171.356323:5015,173.092148:8421,176.122162:5505,181.096405:7089,183.112442:7853,185.128998:8630,197.165054:8136,200.137955:6585,201.124451:1.954e+04,204.117096:5454,214.117874:7639,214.190643:9223,229.116867:9235,242.186493:8905,244.091507:8294,252.097015:5936,265.484497:6534,271.213501:6770,298.150421:6417,305.165008:6448,464.851410:5286,549.305420:5863
         */
        // query index is 1-based system
        SpectrumQuery targetQuery = targetQueries[queryIndex - 1];
        SpectrumQuery decoyQuery = decoyQueries[queryIndex - 1];
        SpectrumQuery bestQuery = null;
        if (targetQuery == null && decoyQuery == null) {
            log.debug("query{} does not match any psmCount", queryIndex);
            return null;
        } else if (targetQuery != null && decoyQuery != null) {
            if (targetQuery.getSearchHit().getIonsScore() > decoyQuery.getSearchHit().getIonsScore()) {
                bestQuery = targetQuery;
            } else {
                log.debug("decoy query{} was selected", queryIndex);
                bestQuery = decoyQuery;
            }
        } else if (targetQuery != null) {
            bestQuery = targetQuery;
        } else {
            bestQuery = decoyQuery;
        }
        return parseBestQuery(br, bestQuery);
    }

    /**
     * @param br
     * @param bestQuery
     * @return
     * @throws IOException
     */
    private SpectrumQuery parseBestQuery(BufferedReader br, SpectrumQuery bestQuery) throws IOException {
        String line;
        while (!(line = br.readLine()).equals(Section_END)) {
            if (line.startsWith("title=")) {
                String url = URLDecoder.decode(line.substring(6), "UTF-8");
                int scanIndex = url.indexOf("scan=");
                if (scanIndex != -1) {
                    String scan = url.substring(scanIndex + 5, url.lastIndexOf("\""));
                    bestQuery.setStartScan(scan);
                    bestQuery.setEndScan(scan);
                    bestQuery.setSpectrumId(String.join(".", datFileName, scan, scan,
                            String.valueOf(bestQuery.assumedCharge)));
                } else {
                    log.error("Unrecognized format: {}", line);
                }

            }
        }
        return bestQuery;
    }

    /**
     * Stores the working fixed amino acid mass. this may be modified or not (we
     * don't know at this point). Exits if AA already exists.
     *
     * @param aminoAcid
     * @param mass
     */
    private void recordStaticAAMass(char aminoAcid, double mass) {
        if (workingAAMasses.get(aminoAcid) >= 0.0) {
            log.error("Trying to store static amino acid mass for {} when value already stored.", aminoAcid);
            System.exit(1);
        }
        workingAAMasses.put(aminoAcid, mass);
        switch (aminoAcid) {
            case 'B':
            case 'J':
            case 'O':
            case 'U':
            case 'X':
            case 'Z':
                // a value has been recorded for a non-standard AA;
                // record this as a static mod
                // !!!
                // assume it's not nterm or cterm!
                recordStaticAAMod(aminoAcid, mass, false, false);
                break;
            default:
                // just return normally
                break;
        }
    }

    /**
     * Records a static modification for a specific AA(amino acid); give the delta from
     * the unmodified mass; we will now assume that the working AA mass has
     * been modified by this delta, so the base (unmodded) AA mass is
     * (working - delta). Failure if AA exists.
     * <p>
     * example: Carbamidomethyl (C) will have delta of 57.021469, which
     * implies that the working AA for C, 160.030649, is modified, and that
     * the base mass for C is (160.030649-57.021469)=103.009180, as we'd
     * expect.
     * <p>
     */
    // TODO make sure parameters isNTerminal and isCTerminal is necessary(源代码设为false)
    void recordStaticAAMod(char aminoAcid, double fixedDelta, boolean isNTerminal, boolean isCTerminal) {
        // first, make sure we've already recorded *some* mass for the AA
        if (workingAAMasses.get(aminoAcid) < 0.0) {
            log.error("Trying to store static amino acid modification for {} without any prior info for this amino acid.",
                    aminoAcid);
            System.exit(1);
        }
        // have we tried to modify this AA already?
        if (aa2Mod.get(aminoAcid) == true) {
            log.error("amino acid {} has been modified!", aminoAcid);
            System.exit(1);
        }
        aa2Mod.put(aminoAcid, true);
        double modifiedMass = workingAAMasses.get(aminoAcid);
        // double baseMass = modifiedMass - fixedDelta;
        double delta = fixedDelta; // TODO 上面注释掉的是应该的？fixedDelta如何得到

        // store with index 0, as the index is used to correlate variable mods
        // with the variable mod strings in the psmCount ID section and 0 is unused.
        // TODO source code always set isNTerminal and isCTerminal false ?这里的index参数作用
        MascotModification mod = new MascotModification(aminoAcid, modifiedMass, delta, false,
                0, isNTerminal, isCTerminal);
        if (isNTerminal || isCTerminal) {
            log.error("recordStaticAAMod exists N-term or C-term");
        }
        staticModList.add(mod);
    }

    /**
     * Records a static modification for a specific AA; give the delta from
     * the unmodified mass; we will now assume that the working AA mass *can be*
     * modified by this delta, so the (variable/optional) modified AA mass would be
     * (working + delta). Exits with error on failure.
     * <p>
     * Note: working could be mod or unmod.
     * <p>
     * example: Oxidation (M) has a delta of 15.994919, so the variable
     * modded mass for M would be base mass 131.040480 + 15.994919 = modded
     * mass of 147.035399.
     *
     * @param aminoAcid
     * @param variableDelta
     * @param isNTerminal
     * @param isCTerminal
     * @param variableIndex
     */
    private void recordVariableAAMod(char aminoAcid, double variableDelta, boolean isNTerminal,
                                     boolean isCTerminal, int variableIndex) {
        if (!workingAAMasses.containsKey(aminoAcid)) {
            workingAAMasses.put(aminoAcid, 0.0);
        }
        // make sure we've already recorded mass info for the amino acid
        if (workingAAMasses.get(aminoAcid) < 0.0) {
            log.error("Trying to store variable amino acid modification for {} " +
                    "without any prior info for this amino acid.", aminoAcid);
            System.exit(1);
        }
        // note, we record the modified mass as delta + working,
        // where working may be modified or not, we don't care.
        double baseMass = workingAAMasses.get(aminoAcid);
        double modifiedMass = baseMass + variableDelta;
        // TODO source code
        // jmt: should the delta be based on the working mass if it's a static mod, or the base mass?
        double delta = variableDelta;
        // the index is used to correlate variable mods with the variable
        // mod strings in the psmCount ID section
        MascotModification mod = new MascotModification(aminoAcid, modifiedMass, delta, true,
                variableIndex, isNTerminal, isCTerminal);
        variableModList.add(mod);
    }


    /**
     * Extract modification info and returns string of residues
     *
     * @param line     like Acetyl (Protein N-term)
     * @param terminal isNTerminal, isCTerminal, isProtTerminal
     * @return residues
     */
    private String extractModificationInfo(String line, boolean[] terminal) {
        String residues = null;
        boolean isNTerminal = line.contains("N-term");
        boolean isCTerminal = line.contains("C-term");
        boolean isProtTerminal = line.contains("Protein");
        terminal[0] = isNTerminal;
        terminal[1] = isCTerminal;
        terminal[2] = isProtTerminal;
        int residuesStartPos = line.lastIndexOf("(") + 1;
        String affectedResidues = line.substring(residuesStartPos, line.indexOf(")", residuesStartPos));
        // if we have exactly "N-term" or "C-term": these translate to AAs(amino acid) "n" or "c"
        if ((affectedResidues.length() == 7 || affectedResidues.length() == 14) && isProtTerminal) {
            // following ResidueMass in assuming "Protein" is N terminal, "Protein N-term"
            residues = "n";
        } else if (affectedResidues.length() == 6 && (isNTerminal || isCTerminal)) {
            // C-term
            if (isNTerminal && isCTerminal) {  // N-term and C-term
                log.error("Both N-terminal and N-terminal modification were detected");
                System.exit(1);
            } else if (isNTerminal) {
                residues = "n";
            } else if (isCTerminal) {
                residues = "c";
            }
        } else if ((isNTerminal || isCTerminal) && affectedResidues.length() > 6) {
            // only extract Q for "N-term Q"
            residues = affectedResidues.substring(7);
        } else {
            residues = affectedResidues;
        }
        if (affectedResidues.length() == 0) {
            log.error("Modification {} was not recognized", line);
            System.exit(1);
        }
        return residues;
    }

    private ModificationInfo parseModificationInfo(String peptSeq, String modSeq) {
        /** check params **/
        if (peptSeq.length() + 2 != modSeq.length()) {
            log.error("error with modification: psmCount<{}> - modification<{}>", peptSeq, modSeq);
            System.exit(1);
        }
        if (staticModList.size() == 0 && variableModList.size() == 0) {
            return null;
        }

        /** parse **/
        boolean modified = getModification('n', true, -1) != null
                || getModification('c', true, -1) != null;
        for (MascotModification staticMod : staticModList) {
            if (staticMod.getAminoAcid() == 'n'
                    || staticMod.getAminoAcid() == 'c'
                    || peptSeq.contains(String.valueOf(staticMod.getAminoAcid()))
            ) {
                modified = true;
                break;
            }
        }
        if (!modified) {
            for (int i = 0; i < modSeq.length(); i++) {
                if (modSeq.charAt(i) != '0') {
                    modified = true;
                    break;
                }
            }
        }
        if (!modified) {
            return null;
        }
        ModificationInfo modInfo = new ModificationInfo();
        if (modSeq.charAt(0) != '0') {
            MascotModification mod = getModification('n', false, modSeq.charAt(0) - '0');
            if (mod != null) {
                // variable modification
                if (mod.isNTerminal() && mod.getAminoAcid() != 'n') {
                    // TODO test 这是什么情况
                    AminoAcidMod aaMod = new AminoAcidMod();
                    modInfo.addAminoAcidMod(aaMod);
                    aaMod.setPosition(1);
                    aaMod.setMass(mod.getMass());
                } else {
                    modInfo.setNTermMod(true);
                    modInfo.setModNTermMass(mod.getMass());
                }
            }
        } else {
            MascotModification mod = getModification('n', true, 0);
            if (mod != null) {
                // static modification
                if (mod.isNTerminal() && mod.getAminoAcid() != 'n') {
                    AminoAcidMod aaMod = new AminoAcidMod();
                    modInfo.addAminoAcidMod(aaMod);
                    aaMod.setPosition(1);
                    aaMod.setMass(mod.getMass());
                } else {
                    modInfo.setNTermMod(true);
                    modInfo.setModNTermMass(mod.getMass());
                }
            }
        }
        if (modSeq.charAt(modSeq.length() - 1) != '0') {
            MascotModification mod = getModification('c', false,
                    modSeq.charAt(modSeq.length() - 1) - '0');
            if (mod != null) {
                // variable modification
                if (mod.isCTerminal() && mod.getAminoAcid() != 'c') {
                    // TODO test 这是什么情况
                    AminoAcidMod aaMod = new AminoAcidMod();
                    modInfo.addAminoAcidMod(aaMod);
                    aaMod.setPosition(1);
                    aaMod.setMass(mod.getMass());
                } else {
                    modInfo.setCTermMod(true);
                    modInfo.setModCTermMass(mod.getMass());
                }
            }
        } else {
            // TODO 确定这里是0而非倒数第一个字符？
            MascotModification mod = getModification('c', true, 0);
            if (mod != null) {
                // static modification
                if (mod.isCTerminal() && mod.getAminoAcid() != 'c') {
                    AminoAcidMod aaMod = new AminoAcidMod();
                    modInfo.addAminoAcidMod(aaMod);
                    aaMod.setPosition(modSeq.length() - 2);
                    aaMod.setMass(mod.getMass());
                } else {
                    modInfo.setCTermMod(true);
                    modInfo.setModCTermMass(mod.getMass());
                }
            }
        }

        /** look for amino acid modification **/
        for (int i = 1; i < modSeq.length() - 1; i++) {
            if (modSeq.charAt(i) != '0') {
                MascotModification mod = getModification(peptSeq.charAt(i - 1), false,
                        modSeq.charAt(i) - '0');
                if (mod != null) {
                    AminoAcidMod aaMod = new AminoAcidMod();
                    modInfo.addAminoAcidMod(aaMod);
                    aaMod.setPosition(i);
                    aaMod.setMass(mod.getMass());
                }
            } else {
                MascotModification mod = getModification(peptSeq.charAt(i - 1), true,
                        0);
                if (mod != null) {
                    AminoAcidMod aaMod = new AminoAcidMod();
                    modInfo.addAminoAcidMod(aaMod);
                    aaMod.setPosition(i);
                    aaMod.setMass(mod.getMass());
                }
            }
        }
        return modInfo;
    }

    private MascotModification getModification(char residues, boolean isStaticMod, int index) {
        if (isStaticMod) {
            for (MascotModification mod : staticModList) {
                if (mod.getAminoAcid() == residues) {
                    return mod;
                }
            }
        } else {
            for (MascotModification mod : variableModList) {
                if (mod.getAminoAcid() == residues
                        || (mod.isNTerminal() && residues == 'n')
                        || (mod.isCTerminal() && residues == 'c')
                ) {
                    if (index < 0 || mod.getIndex() == index) {
                        return mod;
                    }
                }
            }
        }
        return null;
    }

    private boolean homologous(String peptSeq1, String peptSeq2) {
        int peptLen1 = peptSeq1.length();
        int peptLen2 = peptSeq2.length();
        int diffInSequence = Math.abs(peptLen1 - peptLen2);
        // align: K/Q and I/L don't count as differences
        for (int i = 0; i < Math.min(peptLen1, peptLen2); i++) {
            char aa1 = peptSeq1.charAt(i);
            char aa2 = peptSeq2.charAt(i);
            if (aa1 != aa2) {
                if (!((aa1 == 'I' || aa1 == 'L') && (aa2 == 'I' || aa2 == 'L'))
                        && !((aa1 == 'K' || aa1 == 'Q') && (aa2 == 'K' || aa2 == 'Q'))
                ) {
                    diffInSequence++;
                }
            }
        }
        return peptLen1 > diffInSequence && (double) (peptLen1 - diffInSequence) / peptLen1 >= MIN_PERCENTAGE;
    }

    private int getTolTermNum(char prevAA, String peptSeq, char nextAA) {
        int tolTerm = 0;
        if (prevAA == '-') {
            tolTerm++;
        } else if (enzymeInfo.getCut().contains(String.valueOf(prevAA))) {
            if (!enzymeInfo.getNoCut().contains(peptSeq.substring(0, 1))) {
                tolTerm++;
            }
        }
        int pepLen = peptSeq.length();
        if (nextAA == '-') {
        } else if (enzymeInfo.getCut().contains(peptSeq.substring(pepLen - 1, pepLen))) {
            if (!enzymeInfo.getNoCut().contains(String.valueOf(nextAA))) {
                tolTerm++;
            }
        }
        return tolTerm;
    }

    public void writeSampleEnzyme(EnzymeInfo enzymeInfo, XMLStreamWriter writer) throws XMLStreamException {
        writer.writeStartElement("sample_enzyme");
        writer.writeAttribute("name", enzymeInfo.getName());
        writer.writeCharacters(System.lineSeparator());
        writer.writeStartElement("specificity");
        writer.writeAttribute("cut", enzymeInfo.getCut());
        if (enzymeInfo.getNoCut() != null) {
            writer.writeAttribute("no_cut", enzymeInfo.getNoCut());
        }
        writer.writeAttribute("sense", enzymeInfo.getSense());
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());
    }

    public void writeSearchSummary(String baseName,
                                   String libraryPath,
                                   EnzymeInfo enzymeInfo,
                                   List<Map.Entry<String, String>> paramList,
                                   XMLStreamWriter writer) throws XMLStreamException {
        if (massType == 0) {
            writer.writeStartElement("search_summary");
            writer.writeAttribute("base_name", baseName);
            writer.writeAttribute("search_engine", "MASCOT");
            writer.writeAttribute("precursor_mass_type", "average");
            writer.writeAttribute("fragment_mass_type", "average");
            writer.writeAttribute("out_data_type", "out");
            writer.writeAttribute("out_data", ".tgz");
            writer.writeAttribute("search_id", "1");
            writer.writeCharacters(System.lineSeparator());
        } else {
            writer.writeStartElement("search_summary");
            writer.writeAttribute("base_name", baseName);
            writer.writeAttribute("search_engine", "MASCOT");
            writer.writeAttribute("precursor_mass_type", "monoisotopic");
            writer.writeAttribute("fragment_mass_type", "monoisotopic");
            writer.writeAttribute("out_data_type", "out");
            writer.writeAttribute("out_data", ".tgz");
            writer.writeAttribute("search_id", "1");
            writer.writeCharacters(System.lineSeparator());
        }

        /** library **/
        writer.writeStartElement("search_database");
        writer.writeAttribute("local_path", libraryPath);
        writer.writeAttribute("type", "AA");
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());
        writer.writeStartElement("enzymatic_search_constraint");
        writer.writeAttribute("enzyme", enzymeInfo.getName());
        writer.writeAttribute("max_num_internal_cleavages", String.valueOf(enzymeInfo.getMaxInternalCleavages()));
        writer.writeAttribute("min_number_termini", String.valueOf(enzymeInfo.getMinTerminalNum()));
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());

        /** modification **/
        for (int i = 0; i < staticModList.size(); i++) {
            writeModification(false, staticModList.get(i), writer);
        }
        for (int i = 0; i < variableModList.size(); i++) {
            writeModification(true, variableModList.get(i), writer);
        }

        /** parameters **/
        for (Map.Entry<String, String> param : paramList) {
            writer.writeStartElement("parameter");
            writer.writeAttribute("name", param.getKey());
            writer.writeAttribute("value", param.getValue());
            writer.writeEndElement();
            writer.writeCharacters(System.lineSeparator());
        }
        writer.writeEndElement(); // search_summary
        writer.writeCharacters(System.lineSeparator());
    }

    private void writeModification(boolean isVariableMod, MascotModification mod, XMLStreamWriter writer) throws XMLStreamException {
        if (mod.getAminoAcid() == 'n' || mod.getAminoAcid() == 'c') { // TODO 寻找测试数据
            writer.writeStartElement("terminal_modification");
            writer.writeAttribute("terminus", String.valueOf(mod.getAminoAcid()));
            writer.writeAttribute("mass", String.valueOf(mod.getMass()));
            writer.writeAttribute("massdiff", String.valueOf(mod.getMassDiff()));
            writer.writeAttribute("variable", isVariableMod ? "Y" : "N");
            writer.writeAttribute("protein_terminus", mod.isProtTerminal() ? "Y" : "N");
            writer.writeEndElement();
            writer.writeCharacters(System.lineSeparator());
        } else {
            if (mod.isCTerminal() || mod.isCTerminal()) {
                if (mod.isCTerminal() && mod.isCTerminal()) { // TODO 似乎不会存在
                    writer.writeStartElement("aminoacid_modification");
                    writer.writeAttribute("aminoacid", String.valueOf(mod.getAminoAcid()));
                    writer.writeAttribute("mass", String.valueOf(mod.getMass()));
                    writer.writeAttribute("massdiff", String.valueOf(mod.getMassDiff()));
                    writer.writeAttribute("peptide_terminus", "nc");
                    writer.writeAttribute("variable", isVariableMod ? "Y" : "N");
                    writer.writeEndElement();
                    writer.writeCharacters(System.lineSeparator());
                } else if (mod.isNTerminal()) {
                    writer.writeStartElement("aminoacid_modification");
                    writer.writeAttribute("aminoacid", String.valueOf(mod.getAminoAcid()));
                    writer.writeAttribute("mass", String.valueOf(mod.getMass()));
                    writer.writeAttribute("massdiff", String.valueOf(mod.getMassDiff()));
                    writer.writeAttribute("peptide_terminus", "n");
                    writer.writeAttribute("variable", isVariableMod ? "Y" : "N");
                    writer.writeEndElement();
                    writer.writeCharacters(System.lineSeparator());
                } else {
                    writer.writeStartElement("aminoacid_modification");
                    writer.writeAttribute("aminoacid", String.valueOf(mod.getAminoAcid()));
                    writer.writeAttribute("mass", String.valueOf(mod.getMass()));
                    writer.writeAttribute("massdiff", String.valueOf(mod.getMassDiff()));
                    writer.writeAttribute("peptide_terminus", "c");
                    writer.writeAttribute("variable", isVariableMod ? "Y" : "N");
                    writer.writeEndElement();
                    writer.writeCharacters(System.lineSeparator());
                }
            } else {
                writer.writeStartElement("aminoacid_modification");
                writer.writeAttribute("aminoacid", String.valueOf(mod.getAminoAcid()));
                writer.writeAttribute("mass", String.valueOf(mod.getMass()));
                writer.writeAttribute("massdiff", String.valueOf(mod.getMassDiff()));
                writer.writeAttribute("variable", isVariableMod ? "Y" : "N");
                writer.writeEndElement();
                writer.writeCharacters(System.lineSeparator());
            }
        }
    }

    private void writeSpectrumQuery(int index, SpectrumQuery query, XMLStreamWriter writer) throws XMLStreamException {
        // spectrum_query
        writer.writeStartElement("spectrum_query");
        writer.writeAttribute("spectrum", query.getSpectrumId());
        writer.writeAttribute("start_scan", query.getStartScan());
        writer.writeAttribute("end_scan", query.getEndScan());
        writer.writeAttribute("precursor_neutral_mass", String.valueOf(query.getPrecursorNeutralMass()));
        writer.writeAttribute("assumed_charge", String.valueOf(query.getAssumedCharge()));
        writer.writeAttribute("index", String.valueOf(index));
        writer.writeCharacters(System.lineSeparator());
        // search_hit
        SearchHit hit = query.getSearchHit();
        Protein[] proteins = hit.getProteins();
        writer.writeStartElement("search_result");
        writer.writeCharacters(System.lineSeparator());
        writer.writeStartElement("search_hit");
        writer.writeAttribute("hit_rank", "1");
        writer.writeAttribute("psmCount", hit.getPeptide());
        writer.writeAttribute("num_missed_cleavages", hit.getMissedCleavageNum());
        writer.writeAttribute("num_tot_proteins", String.valueOf(proteins.length));
        writer.writeAttribute("peptide", proteins[0].getId());
        writer.writeAttribute("peptide_prev_aa", String.valueOf(proteins[0].getPrevAA()));
        writer.writeAttribute("peptide_next_aa", String.valueOf(proteins[0].getNextAA()));
        writer.writeAttribute("num_tol_term", String.valueOf(proteins[0].getTolTermNum()));
        writer.writeAttribute("num_matched_ions", String.valueOf(hit.getMatchedIonsNum()));
        writer.writeAttribute("tot_num_ions", String.valueOf(hit.getTotIonsNum()));
        writer.writeAttribute("calc_neutral_pep_mass", String.valueOf(hit.getNeutralPepMass()));
        writer.writeAttribute("massdiff", hit.getMassDiff());
        writer.writeAttribute("is_rejected", "0");
        writer.writeCharacters(System.lineSeparator());
        // alternative_protein
        for (int i = 1; i < proteins.length; i++) {
            writer.writeStartElement("alternative_protein");
            writer.writeAttribute("peptide", proteins[i].getId());
            writer.writeAttribute("peptide_prev_aa", String.valueOf(proteins[i].getPrevAA()));
            writer.writeAttribute("peptide_next_aa", String.valueOf(proteins[i].getNextAA()));
            writer.writeAttribute("num_tol_term", String.valueOf(proteins[i].getTolTermNum()));
            writer.writeEndElement();
            writer.writeCharacters(System.lineSeparator());
        }
        // modification_info
        ModificationInfo modInfo = query.getModificationInfo();
        if (modInfo != null) {
            writer.writeStartElement("modification_info");
            if (modInfo.isNTermMod()) {
                writer.writeAttribute("mod_nterm_mass", String.valueOf(modInfo.getModNTermMass()));
            }
            if (modInfo.isCTermMod()) {
                writer.writeAttribute("mod_cterm_mass", String.valueOf(modInfo.getModCTermMass()));
            }
            writer.writeCharacters(System.lineSeparator());
            // mod_aminoacid_mass
            if (modInfo.getAminoAcidModList() != null) {
                for (AminoAcidMod aaMod : modInfo.getAminoAcidModList()) {
                    writer.writeStartElement("mod_aminoacid_mass");
                    writer.writeAttribute("position", String.valueOf(aaMod.getPosition()));
                    writer.writeAttribute("mass", String.valueOf(aaMod.getMass()));
                    writer.writeEndElement();
                    writer.writeCharacters(System.lineSeparator());
                }
            }
            writer.writeEndElement();
            writer.writeCharacters(System.lineSeparator());
        }
        // search_score
        writer.writeStartElement("search_score");
        writer.writeAttribute("name", "ionscore");
        writer.writeAttribute("value", String.valueOf(hit.getIonsScore()));
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());
        writer.writeStartElement("search_score");
        writer.writeAttribute("name", "identityscore");
        writer.writeAttribute("value", String.valueOf(query.getIdentityScore()));
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());
        writer.writeStartElement("search_score");
        writer.writeAttribute("name", "star");
        writer.writeAttribute("value", String.valueOf(hit.getStar())); // TODO hit query attribute?
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());
        writer.writeStartElement("search_score");
        writer.writeAttribute("name", "homologyscore");
        writer.writeAttribute("value", String.valueOf(query.getHomologyScore()));
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());
        writer.writeStartElement("search_score");
        writer.writeAttribute("name", "expect");
        writer.writeAttribute("value", String.valueOf(hit.getExpect()));
        writer.writeEndElement();
        writer.writeCharacters(System.lineSeparator());
        writer.writeEndElement(); // </search_hit>
        writer.writeCharacters(System.lineSeparator());
        writer.writeEndElement(); // </search_result>
        writer.writeCharacters(System.lineSeparator());
        writer.writeEndElement(); // </spectrum_query>
        writer.writeCharacters(System.lineSeparator());
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @ToString
    private class EnzymeInfo {

        String name;

        // One or more 1-letter residue codes. Enzyme
        // cleaves on the sense side of the residue(s) listed in cut
        // unless one of the residues listed in no_cut is adjacent to
        // the potential cleavage site.
        String cut;

        // no_cut
        String noCut;

        // Defines whether cleavage occurs on the
        // C-terminal or N-terminal side of the residue(s) listed in cut
        String sense;

        // Maximum number of enzyme cleavage sites allowable within psmCount: cut.length()
        int maxInternalCleavages;

        // Minimum number of termini compatible with enzymatic cleavage
        int minTerminalNum;
    }

    @Getter
    @Setter
    @ToString
    private class MascotModification {
        char aminoAcid;
        double mass;
        double massDiff;
        boolean isVariable;
        int index;
        boolean isNTerminal;
        boolean isCTerminal;
        boolean isProtTerminal;

        public MascotModification(char aminoAcid, double mass, double massDiff, boolean isVariable,
                                  int index, boolean isNTerminal, boolean isCTerminal) {
            if (aminoAcid == '1') {
                // peptide n term
                this.aminoAcid = 'n';
                this.isProtTerminal = true;
            } else if (aminoAcid == '2') {
                // peptide n term
                this.aminoAcid = 'c';
                this.isProtTerminal = true;
            } else {
                this.aminoAcid = aminoAcid;
                this.isProtTerminal = false;
            }
            this.mass = mass;
            this.massDiff = massDiff;
            this.isVariable = isVariable;
            this.index = index;
            this.isNTerminal = isNTerminal;
            this.isCTerminal = isCTerminal;
        }
    }

    @Getter
    @Setter
    @ToString
    @NoArgsConstructor
    private class ModificationInfo {
        boolean isNTermMod = false;
        boolean isCTermMod = false;
        double modNTermMass;
        double modCTermMass;
        List<AminoAcidMod> aminoAcidModList;

        void addAminoAcidMod(AminoAcidMod aaMod) {
            if (aminoAcidModList == null) {
                aminoAcidModList = new ArrayList<>();
            }
            aminoAcidModList.add(aaMod);
        }
    }

    @Getter
    @Setter
    @ToString
    @NoArgsConstructor
    private class AminoAcidMod {
        int position;
        double mass;
    }

    @Getter
    @Setter
    @ToString
    @NoArgsConstructor
    private class SpectrumQuery {
        String spectrumId;
        String startScan;
        String endScan;
        double precursorNeutralMass;
        int assumedCharge;
        double identityScore;
        double homologyScore;
        SearchHit searchHit; // only record the best PSM
        ModificationInfo modificationInfo;
    }

    @Getter
    @Setter
    @ToString
    @NoArgsConstructor
    private class SearchHit {
        int rank;
        String peptide;
        Protein[] proteins;
        int matchedIonsNum;
        int totIonsNum;
        double neutralPepMass;
        String massDiff;
        String missedCleavageNum;
        double ionsScore;
        int star;
        double homologyScore;
        double expect;
    }

    @Getter
    @Setter
    @ToString
    @NoArgsConstructor
    private class Protein {
        String id;
        char prevAA;
        char nextAA;
        int tolTermNum;
    }
}
