package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.model;

import lombok.*;

import java.util.Set;

/**
 * Created by huangjs
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Peptide {

    private String sequence;

    private double score;

    private double qValue;

    private double posteriorErrorProb;

    // store protein id list containing the peptide
    private Set<String> asscociatedProtIdSet;
}
