package cn.phoenixcenter.metaproteomics.pipeline.inference.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PPProteinGroup extends ProteinGroup {

    private double probability;
}
