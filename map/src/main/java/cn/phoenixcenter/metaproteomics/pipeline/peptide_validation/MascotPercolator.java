package cn.phoenixcenter.metaproteomics.pipeline.peptide_validation;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.pipeline.processor.ProteinPostprocess;
import cn.phoenixcenter.metaproteomics.pipeline.utils.Pept2Prot;
import cn.phoenixcenter.metaproteomics.pipeline.utils.SequenceHandler;
import cn.phoenixcenter.metaproteomics.utils.CommandExecutor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by huangjs
 * <p>
 * Call MascotPercolator to process dat file.
 * MascotPercolator: https://www.sanger.ac.uk/science/tools/mascotpercolator
 */

@Getter
@Setter
@ToString
@NoArgsConstructor
@Log4j2
public class MascotPercolator {

    private final static String javaHome = GlobalConfig.getValue("java_home");

    private final static String mascotPercolatorLocation = GlobalConfig.getValue("MascotPercolator");

    private final static String percolatorLocation = GlobalConfig.getValue("percolator");

    private final static XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

    private final static SAXReader reader = new SAXReader();

    /**
     * Run MascotPercolator
     *
     * @param targetDatPath
     * @param decoyDatPath
     * @param featuresPath
     * @param xmlResultPath
     * @throws IOException
     */
    public static void run(Path targetDatPath, Path decoyDatPath,
                           Path featuresPath, Path xmlResultPath) throws IOException {
        generateFeaturesFile(targetDatPath, decoyDatPath, featuresPath);
        runPercolator(featuresPath, xmlResultPath);
    }

    public static void generateFeaturesFile(Path autoModelDatPath,
                                            Path featuresPath) throws IOException {
        generateFeaturesFile(autoModelDatPath, autoModelDatPath, featuresPath);
    }

    // TODO add complete input params for MascotPercolator

    /**
     * @param targetDatPath
     * @param decoyDatPath
     * @param featuresPath
     * @throws IOException
     */
    public static void generateFeaturesFile(Path targetDatPath, Path decoyDatPath,
                                            Path featuresPath) throws IOException {
        Path parentPath = featuresPath.getParent();
        String tempFileName = UUID.randomUUID().toString();
        Path tempPath = parentPath.resolve(tempFileName);
        String command = String.join(" ",
                Paths.get(javaHome, "bin", "java").toString(), "-cp",
                Paths.get(mascotPercolatorLocation, "MascotPercolator.jar").toString(),
                "cli.MascotPercolator",
                "-target", targetDatPath.toString(),
                "-decoy", decoyDatPath.toString(),
                "-out", tempPath.toString(),
                "-features",
                "-overwrite");
        CommandExecutor.exec(command);
        Files.move(parentPath.resolve(tempFileName + ".features.txt"), featuresPath, StandardCopyOption.REPLACE_EXISTING);
        Files.deleteIfExists(parentPath.resolve(tempFileName + ".peptides"));
        Files.deleteIfExists(parentPath.resolve(tempFileName + "_decoy.peptides"));
        Files.deleteIfExists(parentPath.resolve(tempFileName + ".log.txt"));
    }

    /**
     * Some peptide may match other protein after equate I and L. However,
     * there is no info for those peptides, so we need update matching proteins info in feature file
     *
     * @param equateIL
     * @param pidPat
     * @param libraryPath
     * @param decoyPrefix
     * @param oldFeaturesPath
     * @param newFeaturesPath
     * @throws IOException
     */
    public static void updateFeaturesFile(boolean equateIL, Pattern pidPat, Path libraryPath, String decoyPrefix,
                                          Path oldFeaturesPath, Path newFeaturesPath) throws IOException {
        /** map peptide to protein **/
        BufferedReader br = Files.newBufferedReader(oldFeaturesPath, StandardCharsets.UTF_8);
        String line;
        String[] headers = br.readLine().split("\t");
        br.readLine(); // DefaultDirection
        int peptIdx = headers.length - 2;
        Set<String> peptSet = new HashSet<>();
        while ((line = br.readLine()) != null) {
            String pept = line.split("\t")[peptIdx];
            peptSet.add(pept.substring(2, pept.length() - 2));
        }
        br.close();
        Map<String, Set<String>> pept2ProtSet = Pept2Prot.map(equateIL, pidPat, libraryPath, peptSet);

        /** write new feature files **/
        br = Files.newBufferedReader(oldFeaturesPath, StandardCharsets.UTF_8);
        BufferedWriter bw = Files.newBufferedWriter(newFeaturesPath, StandardCharsets.UTF_8);
        bw.write(br.readLine() + System.lineSeparator()); // headers
        bw.write(br.readLine() + System.lineSeparator()); // DefaultDirection
        while ((line = br.readLine()) != null) {
            String[] tmp = line.split("\t");
            String pept = line.split("\t")[peptIdx];
            pept = pept.substring(2, pept.length() - 2);
            bw.write(String.join("\t",
                    String.join("\t", Arrays.copyOfRange(tmp, 0, peptIdx + 1)),
                    pept2ProtSet.get(pept).stream()
                            .filter(pid -> {
                                if (tmp[1].equals("1")) {
                                    return !pid.startsWith(decoyPrefix);
                                } else {
                                    return pid.startsWith(decoyPrefix);
                                }
                            })
                            .collect(Collectors.joining("\t"))
            ) + System.lineSeparator());
        }
        br.close();
        bw.close();
    }

    public static void runPercolator(Path featuresPath, Path xmlResultPath) {
        String command = String.join(" ", percolatorLocation,
                featuresPath.toString(),
                "--xmloutput", xmlResultPath.toString(),
                "--decoy-xml-output");
        CommandExecutor.exec(command);
    }

    /**
     * Read and parse percolator xml file. In process,
     * the peptides whose q-value are less than fdrThreshold will be delete.
     *
     * @param xmlPath
     * @param fdrThreshold
     * @param mpPeptideSet
     * @param mpProteinSet
     * @throws FileNotFoundException
     * @throws XMLStreamException
     */
    public static void convert2PeptProt(Path xmlPath,
                                        double fdrThreshold,
                                        boolean ignoreSharedPept,
                                        Set<MPPeptide> mpPeptideSet,
                                        Set<MPProtein> mpProteinSet)
            throws DocumentException {
        if (mpPeptideSet.size() != 0 || mpProteinSet.size() != 0) {
            throw new IllegalArgumentException("mpPeptideSet or mpProteinSet size must be empty when input");
        }
        Map<String, MPPeptide> seq2Pept = new HashMap<>();
        Map<String, MPProtein> pid2Prot = new HashMap<>();
        Document document = reader.read(xmlPath.toFile());
        List<Element> peptElemList = document.getRootElement().element("peptides").elements();
        for (Element peptElem : peptElemList) {
            double qval = Double.parseDouble(peptElem.element("q_value").getStringValue());
            if (qval < fdrThreshold) {
                String peptSeq = peptElem.attributeValue("peptide_id");
                boolean isTarget = !Boolean.valueOf(peptElem.attributeValue("decoy"));
                // some like:
                // <peptide p:peptide_id="ATGFPIAK" p:decoy="true"> and
                // <peptide p:peptide_id="ATGFPIAK" p:decoy="false">
                MPPeptide mpPeptide = null;
                if (seq2Pept.containsKey(peptSeq)) {
                    mpPeptide = seq2Pept.get(peptSeq);
                    double prob = 1 - Double.parseDouble(peptElem.element("pep").getStringValue());
                    if (prob > mpPeptide.getProb()) {
                        mpPeptide.setProb(prob);
                    }
                } else {
                    mpPeptide = new MPPeptide(peptSeq);
                    mpPeptide.setQvalue(qval);
                    mpPeptide.setProb(1 - Double.parseDouble(peptElem.element("pep").getStringValue()));
                    mpPeptide.setSpectrumCount(peptElem.element("psm_ids").elements("psm_id").size());
                    mpPeptide.setAssociatedProteinSet(new HashSet<>());
                    seq2Pept.put(peptSeq, mpPeptide);
                }
                // set associated proteins
                Set<String> pidSet = peptElem.elements("protein_id").stream()
                        .map(Element::getStringValue)
                        .collect(Collectors.toSet());
                Set<MPProtein> associatedProtSet = new HashSet<>();
                for (String pid : pidSet) {
                    MPProtein mpProt = null;
                    if (pid2Prot.containsKey(pid)) {
                        mpProt = pid2Prot.get((pid));
                    } else {
                        mpProt = new MPProtein(pid);
                        mpProt.setTarget(isTarget);
                        mpProt.setContainedPeptideSet(new HashSet<>());
                        pid2Prot.put(pid, mpProt);
                    }
                    mpProt.getContainedPeptideSet().add(mpPeptide);
                    associatedProtSet.add(mpProt);
                }
                mpPeptide.getAssociatedProteinSet().addAll(associatedProtSet);
            }
        }
        mpPeptideSet.addAll(seq2Pept.values());
        mpProteinSet.addAll(pid2Prot.values());
        log.debug("peptide count: {}", mpPeptideSet.size());
        // set alternative protein set
        SequenceHandler.setAltProteinSet(mpProteinSet);
        // add PSM info
        ProteinPostprocess.calPSMCount(mpProteinSet, ignoreSharedPept);
    }
}
