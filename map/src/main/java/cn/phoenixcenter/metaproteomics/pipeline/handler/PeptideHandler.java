package cn.phoenixcenter.metaproteomics.pipeline.handler;

import lombok.extern.log4j.Log4j2;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
public class PeptideHandler {
    private static final SAXReader reader = new SAXReader();

    public static void extractPeptide(double peptFDRThreshold,
                                      String[] percolatorXMLFiles,
                                      String outputTSVFile) throws DocumentException, IOException {
        Map<String, String> seq2Line = new HashMap<>(50_000);
        for (String percolatorXMLFile : percolatorXMLFiles) {
            Document document = reader.read(percolatorXMLFile);
            List<Element> peptElemList = document.getRootElement().element("peptides").elements();
            for (Element peptElem : peptElemList) {
                double qval = Double.parseDouble(peptElem.element("q_value").getStringValue());
                if (qval < peptFDRThreshold) {
                    boolean isTarget = !Boolean.valueOf(peptElem.attributeValue("decoy"));
                    if (isTarget) {
                        String peptSeq = peptElem.attributeValue("peptide_id").replaceAll("\\[.*?\\]", "");
                        double prob = 1 - Double.parseDouble(peptElem.element("pep").getStringValue());
                        int psmCount = peptElem.element("psm_ids").elements("psm_id").size();
                        seq2Line.put(peptSeq, String.join("\t", peptSeq,
                                String.valueOf(prob),
                                String.valueOf(psmCount)
                        ));
                    }
                }
            }
            log.debug("Finish extracting file {}", percolatorXMLFile);
        }
        Files.write(Paths.get(outputTSVFile), seq2Line.values(), StandardCharsets.UTF_8);
    }
}
