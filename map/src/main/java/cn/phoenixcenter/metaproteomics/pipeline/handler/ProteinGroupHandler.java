package cn.phoenixcenter.metaproteomics.pipeline.handler;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProteinGroup;
import cn.phoenixcenter.metaproteomics.pipeline.base.ParsimonyType;
import cn.phoenixcenter.metaproteomics.taxonomy.LCAAnalysis;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import cn.phoenixcenter.metaproteomics.taxonomy.TaxonSearcher;
import com.rits.cloning.Cloner;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The class is used to calculate peptides and protein parsimony type
 * These attributes need be added before doing protein group:
 * <PRE>
 * 1. MPPeptide: sequence, associatedProteinSet
 * 2. MPProtein: id, containedPeptideSet
 * </PRE>
 */
@Log4j2
public class ProteinGroupHandler {

    private static final TaxonSearcher taxonSearch = GlobalConfig.getObj(TaxonSearcher.class);

    public enum Relationship {
        SUPER_SUBSET,
        SUBSET_SUPER,
        // differentiable suggest that the two protein is differentiable, so the two protein
        // may or may not have the shared peptide.
        DIFFERENTIABLE
    }

    /**
     * Protein classification needs two steps:
     * 1. set peptide parsimony type;
     * 2. set protein parsimony type.
     * The method combine the two steps.
     * <p>
     * Before doing parsimony algorithm these attributes need be added:
     * <PRE>
     * 1. Peptide.sequence, Peptide.associatedProteinSet
     * 2. Protein.id, Protein.containedPeptideSet
     * </PRE>
     * <p>
     * NOTE: If you do some operation for MPPeptide or MPProtein,
     * those operation may affect mpPeptide or mpProtein.
     * So please backup source mpPeptide or mpProtein set when you need to use
     * source mpPeptide or mpProtein set in following analysis.
     *
     * @param mpPeptides
     * @param mpProteins
     */
    public static void addParsimonyTypeInfo(Collection<MPPeptide> mpPeptides,
                                            Collection<MPProtein> mpProteins) {
        setPeptParsimonyType(mpPeptides);
        setProtParsimonyType(mpPeptides, mpProteins);
    }

    /**
     * Set the parsimony type of peptide.
     *
     * @param mpPeptides
     */
    private static void setPeptParsimonyType(Collection<MPPeptide> mpPeptides) {
        mpPeptides.parallelStream()
                .forEach(mpPept -> {
                    if (mpPept.getAssociatedProteinSet().size() == 1) {
                        mpPept.setParsimonyType(ParsimonyType.DISTINCT);
                    } else if (mpPept.getAssociatedProteinSet().size() > 1) {
                        mpPept.setParsimonyType(ParsimonyType.SHARED);
                    } else {
                        throw new IllegalArgumentException("peptide <" + mpPept.getSequence()
                                + "> did not match any protein");
                    }
                });
    }

    /**
     * Protein parsimonyType: the peptide parsimony type must be process before setting protein parsimony type.
     * <PRE>
     * NOTE:
     * 1. Parsimony type analysis is applied in ascending hierarchy:
     * equivalent, subset, superset, subsumable, differential, and
     * distinct proteins.
     * 2. Each protein is counted in exactly one
     * category. It is possible for a protein to be listed in more than
     * one category, in which case it is counted in the highest category
     * in which it occurs where distinct is the highest category and
     * equivalent the lowest.
     * </PRE>
     *
     * @param mpPeptides
     * @param mpProteins
     */
    private static void setProtParsimonyType(Collection<MPPeptide> mpPeptides,
                                             Collection<MPProtein> mpProteins) {
        // set associated proteins for each protein: these proteins have at least one shared peptide.
        mpProteins.parallelStream().forEach(MPProtein::init);
        mpPeptides.stream() // not parallel stream because of thread unsafe
                // obtain shared peptide
                .filter(mpPeptType -> mpPeptType.getAssociatedProteinSet().size() > 1)
                .forEach(mpPeptType -> {
                    Set<MPProtein> protSetWithSharedPept = mpPeptType.getAssociatedProteinSet();
                    // those proteins have at least shared peptide
                    protSetWithSharedPept.stream()
                            .forEach(pro -> pro.addAssociatedProteinSet(protSetWithSharedPept));
                });

        /** set parsimony type for MPProtein **/
        for (MPProtein mpProt : mpProteins) {
            if (mpProt.containsDistinctPeptide()) {
                // exists distinct peptides
                if (mpProt.isAllDistinctPeptides()) {
                    mpProt.setParsimonyType(ParsimonyType.DISCRETE);
                } else {
                    mpProt.setParsimonyType(ParsimonyType.DIFFERENTIABLE);
                    classifyAssociatedProtSet(mpProt);
                }
            } else {
                // only contains shared peptides
                classifyAssociatedProtSet(mpProt);
                if (mpProt.getSuperset().size() > 0) {
                    // exists superset
                    mpProt.setParsimonyType(ParsimonyType.SUBSET);
                } else if (isSubsumable(mpProt)) {
                    // exists superset(combined superset)
                    mpProt.setParsimonyType(ParsimonyType.SUBSUMABLE);
                } else if (mpProt.getSubset().size() > 0) {
                    // exists subset
                    mpProt.setParsimonyType(ParsimonyType.SUPERSET);
                } else {
                    log.error("protein {} cannot be classified to any type", mpProt.getId());
                    System.exit(255);
                }
            }
        }
    }

    /**
     * Classify the specified protein and its associated proteins
     *
     * @param prot
     */
    private static void classifyAssociatedProtSet(MPProtein prot) {
        for (MPProtein prot2 : prot.getAssociatedProteinSet()) {
            switch (determineRelationship(prot, prot2)) {
                case SUBSET_SUPER:
                    prot.getSuperset().add(prot2);
                    prot2.getSubset().add(prot);
                    break;
                case SUPER_SUBSET:
                    prot.getSubset().add(prot2);
                    prot2.getSuperset().add(prot);
                    break;
                case DIFFERENTIABLE:
                    prot.getDifferentiable().add(prot2);
                    prot2.getDifferentiable().add(prot);
                    break;
                default:
                    log.error("Exist unknown relationship between the two protein <{}, {}>", prot, prot2);
                    System.exit(255);
            }
        }
    }


    /**
     * Determine the relationship between two proteins. The two proteins have at least one shared peptide.
     *
     * @param prot1
     * @param prot2
     * @return
     */
    private static Relationship determineRelationship(MPProtein prot1, MPProtein prot2) {
        if (prot1.getPeptideCount() > prot2.getPeptideCount()) {
            if (prot1.getContainedPeptideSet().containsAll(prot2.getContainedPeptideSet())) {
                return Relationship.SUPER_SUBSET;
            } else {
                return Relationship.DIFFERENTIABLE;
            }
        } else {
            if (prot2.getContainedPeptideSet().containsAll(prot1.getContainedPeptideSet())) {
                if (prot2.getPeptideCount() == prot1.getPeptideCount()) {
                    log.error("{}<{}> and {}<{}> have same peptide, " +
                                    "please confirm the field \"altProteinSet\" has been set before ProteinGroupProcess",
                            prot1.getId(), prot1.getContainedPeptideSet()
                                    .stream().map(MPPeptide::getSequence)
                                    .collect(Collectors.joining(",")),
                            prot2.getId(),
                            prot1.getContainedPeptideSet()
                                    .stream().map(MPPeptide::getSequence)
                                    .collect(Collectors.joining(",")));
                    System.exit(255);
                    return null; //  only for compile
                } else {
                    return Relationship.SUBSET_SUPER;
                }
            } else {
                return Relationship.DIFFERENTIABLE;
            }
        }
    }

    /**
     * @param prot
     * @return
     */
    private static boolean isSubsumable(MPProtein prot) {
        if (prot.getDifferentiable().size() < 2) {
            return false;
        }
        List<MPProtein> differentiableList = new ArrayList<>(prot.getDifferentiable());
        Set<MPPeptide> peptideSet = new HashSet<>();
        // obtain superset protein of the protein
        for (int i = 0; i < differentiableList.size() - 1; i++) {
            MPProtein prot1 = differentiableList.get(i);
            for (int j = i + 1; j < differentiableList.size(); j++) {
                MPProtein prot2 = differentiableList.get(j);
                if (determineRelationship(prot1, prot2) == Relationship.DIFFERENTIABLE) {
                    peptideSet.addAll(prot1.getContainedPeptideSet());
                    peptideSet.addAll(prot2.getContainedPeptideSet());
                }
            }
        }
        return peptideSet.containsAll(prot.getContainedPeptideSet());
    }


    public static List<MPProteinGroup> groupProteins(String mpProteinJsonFile,
                                                     double protFDRThreshold,
                                                     String libraryJsonFile,
                                                     int uniqPeptThreshold) throws IOException {
        Set<MPProtein> mpProteinSet = ProteinHelper.parseJsonMPProteins(mpProteinJsonFile, protFDRThreshold);
        return groupProteins(mpProteinSet, libraryJsonFile, uniqPeptThreshold);
    }

    /**
     * Group proteins
     *
     * @param mpProteins
     * @return
     */
    public static List<MPProteinGroup> groupProteins(Collection<MPProtein> mpProteins,
                                                     String libraryJsonFile,
                                                     int uniqPeptThreshold) throws IOException {
        log.debug("Input protein size: {}", mpProteins.size());
        List<MPProteinGroup> mpProtGroupList = new ArrayList<>();
        Set<MPProtein> visited = new HashSet<>(mpProteins.size());
        for (MPProtein mpProt : mpProteins) {
            if (!visited.contains(mpProt)) {
                List<MPProtein> members = null;
                switch (mpProt.getParsimonyType()) {
                    case DISCRETE:
                        members = Stream.of(mpProt).collect(Collectors.toList());
                        break;
                    case DIFFERENTIABLE:
                    case SUPERSET:
                        members = Stream.of(mpProt).collect(Collectors.toList());
                        members.addAll(mpProt.getSubset().stream().collect(Collectors.toList()));
                        break;
                    case SUBSUMABLE:
                        Set<MPProtein> mpProtSet = new HashSet<>();
                        collectSubsumablePG(mpProt, mpProtSet);
                        members = new ArrayList<>(mpProtSet);
                        break;
                }
                // null if protein is SUBSET
                if (members != null) {
                    // add visited members
                    visited.addAll(members);
                    MPProteinGroup mpPG = new MPProteinGroup();
                    mpPG.setMembers(members);
                    mpProtGroupList.add(mpPG);
                }
            }
        }
        if (visited.size() != mpProteins.size()) {
            log.error("Those {} proteins are not in any group: ", (mpProteins.size() - visited.size()));
            List<MPProtein> unvisited = mpProteins.stream()
                    .filter(prot -> !visited.contains(prot))
                    .collect(Collectors.toList());
            for (MPProtein prot : unvisited) {
                log.error("{} - {}", prot.getId(), prot.getParsimonyType().toString());
            }
            throw new RuntimeException();
        }

        /** add taxon info **/
        // read taxon info
        Map<String, Taxon> pid2Taxon = new HashMap<>(mpProtGroupList.size());
        Consumer<MPProtein> consumer = (MPProtein mpProtein) -> {
            pid2Taxon.put(mpProtein.getId(), mpProtein.getTaxon());
        };
        ProteinHelper.parseJsonLibrary(libraryJsonFile, consumer);
        // add taxon info for protein group members
        final Cloner cloner = new Cloner();
        mpProtGroupList = mpProtGroupList.parallelStream()
                .flatMap(pg -> {
                    List<MPProtein> members = pg.getMembers().stream()
                            .flatMap(prot -> {
                                prot.setTaxon(pid2Taxon.get(prot.getId()));
                                if (prot.getAltProteinSet() != null) {
                                    List<MPProtein> altProtList = prot.getAltProteinSet()
                                            .stream()
                                            .map(pid -> {
                                                MPProtein altProt = cloner.deepClone(prot);
                                                altProt.setId(pid);
                                                altProt.setTaxon(pid2Taxon.get(pid));
                                                return altProt;
                                            })
                                            .collect(Collectors.toList());
                                    altProtList.add(prot);
                                    return altProtList.stream();
                                } else {
                                    return Stream.of(prot);
                                }
                            })
                            .collect(Collectors.toList());
                    members = members.stream().filter(prot -> prot.getTaxon() != null)
                            .collect(Collectors.toList());
                    if (members.size() == 0) {
                        return null;
                    } else {
                        MPProteinGroup mpProteinGroup = new MPProteinGroup();
                        mpProteinGroup.setMembers(members);
                        return Stream.of(mpProteinGroup);
                    }
//                    Map<Integer, List<MPProtein>> taxon2Prot = members.stream()
//                            .filter(prot -> {
//                                if (prot.getTaxon() != null && prot.getTaxon().existsTaxonOnRank(rank)) {
//                                    return true;
//                                } else {
//                                    log.warn("Protein {} don't have taxon info on {}",
//                                            prot.getId(), rank.rankToString());
//                                    return false;
//                                }
//                            })
//                            .collect(Collectors.groupingBy(prot ->
//                                    prot.getTaxon().getLineageIdMap().get(rank.rankToString())));
//                    if (taxon2Prot.size() == 1) {
//                        return Stream.of(pg);
//                    } else {
//                        double total = taxon2Prot.values().stream()
//                                .flatMap(mem -> mem.stream())
//                                .count();
//                        // TODO check 0 element stream
//                        List<MPProteinGroup> pgs = taxon2Prot.values().stream()
//                                .filter(prots -> prots.size() / total >= minTaxonConsistency)
//                                .map(prots -> {
//                                    MPProteinGroup mpProteinGroup = new MPProteinGroup();
//                                    mpProteinGroup.setMembers(prots);
//                                    return mpProteinGroup;
//                                })
//                                .collect(Collectors.toList());
//                        if (pgs.size() == 0) {
//                            return null;
//                        } else {
//                            return pgs.stream();
//                        }
//                    }
                })
                .filter(pg -> pg != null)
                .collect(Collectors.toList());

        /** filter by unique peptide count **/
        log.info("Protein group count before filtering by unique peptide count >= {}: {}",
                uniqPeptThreshold, mpProtGroupList.size());
        Map<String, Integer> pept2PG = new HashMap<>(mpProtGroupList.size() * 2);
        Map<MPProteinGroup, List<String>> pg2pept = new HashMap<>(mpProtGroupList.size());
        for (int i = 0; i < mpProtGroupList.size(); i++) {
            MPProteinGroup pg = mpProtGroupList.get(i);
            List<String> peptList = new ArrayList<>();
            pg.getMembers().stream()
                    .flatMap(prot -> prot.getContainedPeptideSet().stream().map(MPPeptide::getSequence))
                    .forEach(seq -> {
                        if (pept2PG.containsKey(seq)) {
                            pept2PG.put(seq, pept2PG.get(seq) + 1);
                        } else {
                            pept2PG.put(seq, 1);
                        }
                        peptList.add(seq);
                    });
            // a temporary id for equals method
            pg.setId(i + "");
            pg2pept.put(pg, peptList);
        }
        mpProtGroupList = mpProtGroupList.parallelStream()
                .filter(pg -> {
                    int uniq = 0;
                    for (String seq : pg2pept.get(pg)) {
                        if (pept2PG.get(seq) == 1) {
                            if (++uniq > uniqPeptThreshold) {
                                return true;
                            }
                        }
                    }
                    return false;
                })
                .collect(Collectors.toList());
        log.info("Protein group count after filtering by unique peptide count >= {}: {}",
                uniqPeptThreshold, mpProtGroupList.size());

        // set id, peptide, taxon
        Map<String, MPPeptide> seq2Pept = new HashMap<>(mpProtGroupList.size());
        for (MPProteinGroup pg : mpProtGroupList) {
            StringJoiner id = new StringJoiner(",");
            Set<MPPeptide> peptideSet = new HashSet<>();
            Map<Integer, Taxon> tid2Taxon = new HashMap<>();
            double prob = 0.0;
            for (MPProtein prot : pg.getMembers()) {
                id.add(prot.getId());
                peptideSet.addAll(prot.getContainedPeptideSet());
                tid2Taxon.put(prot.getTaxon().getId(), prot.getTaxon());
                if (prot.getProb() > prob) {
                    prob = prot.getProb();
                }
            }
            pg.setId(id.toString());
            pg.setProb(prob);
            // taxon
            pg.setTaxon(taxonSearch.getTaxonById(LCAAnalysis.calculateUnipeptLCA(tid2Taxon, tid2Taxon.keySet())));
            // peptide
            Set<MPPeptide> containedPeptSet = new HashSet<>();
            for (MPPeptide pept : peptideSet) {
                MPPeptide containedPept = null;
                if (seq2Pept.containsKey(pept.getSequence())) {
                    containedPept = seq2Pept.get(pept.getSequence());
                } else {
                    containedPept = pept;
                    pept.setAssociatedProteinSet(new HashSet<>());
                }
                pept.getAssociatedProteinSet().add(pg);
                containedPeptSet.add(containedPept);
            }
            pg.setContainedPeptideSet(containedPeptSet);
        }
        // calculate PSM
        mpProtGroupList.parallelStream()
                .forEach(pg -> {
                    double psmCount = pg.getContainedPeptideSet().stream()
                            .mapToDouble(pept -> {
                                if (pept.isUniq()) {
                                    return pept.getSpectrumCount();
                                } else {
                                    return (double) pept.getSpectrumCount() / pept.getAssociatedProteinSet().size();
                                }
                            })
                            .sum();
                    pg.setPsmCount(psmCount);
                });
        return mpProtGroupList;
    }

    /**
     * A subsumable protein group contains:
     * 1. the subset proteins of the protein
     * 2. the differentiable proteins of the protein except proteins with "differentiable type"
     * 3. the subset and differentiable proteins(except proteins with "differentiable type") of above protein
     *
     * @param mpProtein
     */
    private static void collectSubsumablePG(MPProtein mpProtein, Set<MPProtein> members) {
        members.add(mpProtein);
        Set<MPProtein> potentialSet = new HashSet<>(mpProtein.getSubset());
        potentialSet.addAll(mpProtein.getDifferentiable().stream()
                .filter(mpProt -> mpProt.getParsimonyType() != ParsimonyType.DIFFERENTIABLE)
                .collect(Collectors.toSet())
        );
        potentialSet = potentialSet.stream()
                .filter(mpProt -> !members.contains(mpProt))
                .collect(Collectors.toSet());
        for (MPProtein mpProt : potentialSet) {
            collectSubsumablePG(mpProt, members);
        }
    }

    /**
     * @param mpProteinGroupList
     * @param rankThreshold
     * @return
     */
    public static Map<Taxon, Double> calTaxonDistribution(List<MPProteinGroup> mpProteinGroupList,
                                                          Taxon.Rank rankThreshold) {
        Map<Integer, List<MPProteinGroup>> tid2PG = mpProteinGroupList.stream()
                .filter(pg -> pg.getTaxon() != null && pg.getTaxon().existsTaxonOnRank(rankThreshold))
                .collect(Collectors.groupingBy(pg ->
                        pg.getTaxon().getLineageIdMap().get(rankThreshold.rankToString()))
                );
        Map<Taxon, Double> taxon2Dist = tid2PG.entrySet().stream()
                .collect(Collectors.toMap(
                        e -> taxonSearch.getTaxonById(e.getKey()),
                        e -> e.getValue().stream().mapToDouble(MPProteinGroup::getPsmCount).sum()
                ));
        double total = taxon2Dist.values().stream().mapToDouble(Double::doubleValue).sum();
        taxon2Dist.keySet().forEach(taxon ->
                taxon2Dist.put(taxon, 100 * taxon2Dist.get(taxon) / total));
        return taxon2Dist;
    }

//    public static void writeTSVProteinGroup(Collection<MPProteinGroup> mpProteinGroups,
//                                            Path tsvProteinGroupPath) throws IOException {
//        StringJoiner ranksJoiner = new StringJoiner("\t");
//        StringJoiner nullRanksJoiner = new StringJoiner("\t");
//        for (Taxon.Rank rank : Taxon.Rank.values()) {
//            ranksJoiner.add(rank.rankToString());
//            nullRanksJoiner.add("");
//        }
//        String ranks = ranksJoiner.toString();
//        String nullRanks = nullRanksJoiner.toString();
//        BufferedWriter writer = Files.newBufferedWriter(tsvProteinGroupPath, StandardCharsets.UTF_8);
//        writer.write(String.join("\t",
//                "Protein group",
//                "PSM count",
//                "LCA id",
//                "LCA name",
//                "LCA rank",
//                ranks,
//                ranks
//        ) + System.lineSeparator());
//
//        for (MPProteinGroup pg : mpProteinGroups) {
//            Taxon lca = pg.getLca();
//            writer.write(String.join("\t",
//                    pg.getMembers().stream().map(MPProtein::getId).collect(Collectors.joining(",")),
//                    String.valueOf(pg.getPsmCount()),
//                    lca != null ? String.valueOf(lca.getId()) : "",
//                    lca != null ? lca.getName() : "",
//                    lca != null ? lca.getRank() : "",
//                    lca != null ? lca.tid2String("\t") : nullRanks,
//                    lca != null ? lca.tname2String("\t") : nullRanks
//            ) + System.lineSeparator());
//        }
//        writer.close();
//    }

    public static void writeTSVProteinGroup(Collection<MPProteinGroup> mpProteinGroups, Path pgTSVPath) throws IOException {
        // sort by probability
        mpProteinGroups = mpProteinGroups.parallelStream()
                .sorted(Comparator.comparingDouble(MPProtein::getProb).reversed().thenComparing(MPProtein::getId))
                .collect(Collectors.toList());
        BufferedWriter writer = Files.newBufferedWriter(pgTSVPath, StandardCharsets.UTF_8);
        StringJoiner ranksJoiner = new StringJoiner("\t");
        StringJoiner nullRanksJoiner = new StringJoiner("\t");
        for (Taxon.Rank rank : Taxon.Rank.values()) {
            ranksJoiner.add(rank.rankToString());
            nullRanksJoiner.add("");
        }
        String ranks = ranksJoiner.toString();
        String nullRanks = nullRanksJoiner.toString();
        writer.write(String.join("\t",
                "Protein group", "Member count", "Probability",
                "PSM count", "Peptides",
                "LCA id", "LCA name", "LCA rank", ranks, ranks
        ) + System.lineSeparator());
        for (MPProteinGroup pg : mpProteinGroups) {
            Taxon taxon = pg.getTaxon();
            writer.write(String.join("\t",
                    pg.getId(),
                    String.valueOf(pg.getMembers().size()),
                    String.valueOf(pg.getProb()),
                    String.valueOf(pg.getPsmCount()),
                    pg.getContainedPeptideSet().stream()
                            .map(MPPeptide::getSequence)
                            .collect(Collectors.joining(",")),
                    taxon != null ? String.valueOf(taxon.getId()) : "",
                    taxon != null ? taxon.getName() : "",
                    taxon != null ? taxon.getRank() : "",
                    taxon != null ? taxon.tid2String("\t") : nullRanks,
                    taxon != null ? taxon.tname2String("\t") : nullRanks
            ) + System.lineSeparator());
        }
        writer.close();
    }
}
