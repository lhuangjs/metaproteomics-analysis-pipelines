package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
public class BipartiteGraphWithWeight<A, B, W> {
    private Map<A, Map<B, W>> vertexCollectionA;
    private Map<B, Map<A, W>> vertexCollectionB;

    public BipartiteGraphWithWeight() {
        this.vertexCollectionA = new HashMap<>();
        this.vertexCollectionB = new HashMap<>();
    }

    public W getWeight(A vertex1, B vertex2) {
        return vertexCollectionA.get(vertex1).get(vertex2);
    }

    /**
     * Add edge between vertex1 and vertex2.
     *
     * @param vertex1 the vertex1 derive from vertex collection A
     * @param vertex2 the vertex2 derive from vertex collection B
     */
    public void addEdge(A vertex1, B vertex2, W weight) {
        if (!vertexCollectionA.containsKey(vertex1)) {
            vertexCollectionA.put(vertex1, new HashMap<>());
        }
        vertexCollectionA.get(vertex1).put(vertex2, weight);
        if (!vertexCollectionB.containsKey(vertex2)) {
            vertexCollectionB.put(vertex2, new HashMap<>());
        }
        vertexCollectionB.get(vertex2).put(vertex1, weight);
    }

    /**
     * Remove vertex from vertex collection A.
     *
     * @param vertices
     * @return
     */
    public boolean removeVertexFromA(A... vertices) {
        boolean result = true;
        for (A vertex1 : vertices) {
            // remove vertices linked to the vertex in collection B
            for (Map.Entry<B, W> entry : vertexCollectionA.get(vertex1).entrySet()) { // throw NullPointerException if the vertex doesn't exist
                result = result
                        && vertexCollectionB.get(entry.getKey()).remove(vertex1) != null;
                // vertex2 will be removed if vertices linked to vertex2 are empty
                if (vertexCollectionB.get(entry.getKey()).size() == 0) {
                    result = result && vertexCollectionB.remove(entry.getKey()) != null;
                }
            }
            // remove vertex1 from vertex collection A
            result = result && vertexCollectionA.remove(vertex1) != null;
        }
        return result;
    }

    /**
     * Update vertex in vertex collection A
     *
     * @param oldVer
     * @param newVer
     */
    public void updateVertexInA(A oldVer, A newVer) {
        // add new edges
        for (Map.Entry<B, W> entry : vertexCollectionA.get(oldVer).entrySet()) {
            addEdge(newVer, entry.getKey(), entry.getValue());
        }
        // remove oldVer in collection A
        removeVertexFromA(oldVer);
    }

    /**
     * Obtain the number of vertices in graph
     *
     * @return
     */
    public int getVertexCount() {
        return vertexCollectionA.size() + vertexCollectionB.size();
    }

    /**
     * Obtain the degree of vertex
     *
     * @param vertex
     * @return
     */
    public int getDegreeInA(A vertex) {
        return vertexCollectionA.get(vertex).size();
    }

    /**
     * Obtain the degree of vertex
     *
     * @param vertex
     * @return
     */
    public int getDegreeInB(B vertex) {
        return vertexCollectionB.get(vertex).size();
    }

    /**
     * Obtain all vertices in vertex collectionA
     *
     * @return
     */
    public Set<A> getAllVerticesInA() {
        return vertexCollectionA.keySet();
    }

    /**
     * Obtain all vertices in vertex collectionB
     *
     * @return
     */
    public Set<B> getAllVerticesInB() {
        return vertexCollectionB.keySet();
    }

    @Override
    public String toString() {
        String collectionA = "## Collection A ##" + System.lineSeparator() +
                vertexCollectionA.keySet().stream()
                        .map((A a) -> a + "=>" +
                                vertexCollectionA.get(a).entrySet().stream()
                                        .map((Map.Entry<B, W> e) -> e.getKey() + ":[" + e.getValue() + "]")
                                        .collect(Collectors.joining(","))
                        ).collect(Collectors.joining(System.lineSeparator()));

        String collectionB = "## Collection B ##" + System.lineSeparator() +
                vertexCollectionB.keySet().stream()
                        .map((B b) -> b + "=>" +
                                vertexCollectionB.get(b).entrySet().stream()
                                        .map((Map.Entry<A, W> e) -> e.getKey() + ":[" + e.getValue() + "]")
                                        .collect(Collectors.joining(","))
                        ).collect(Collectors.joining(System.lineSeparator()));
        return String.join(System.lineSeparator(), collectionA, collectionB);
    }
}
