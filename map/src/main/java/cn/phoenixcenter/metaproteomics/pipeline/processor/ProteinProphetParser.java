package cn.phoenixcenter.metaproteomics.pipeline.processor;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProteinGroup;
import cn.phoenixcenter.metaproteomics.utils.CommandExecutor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import umich.ms.fileio.exceptions.FileParsingException;
import umich.ms.fileio.filetypes.protxml.ProtXmlParser;
import umich.ms.fileio.filetypes.protxml.jaxb.standard.IndistinguishableProtein;
import umich.ms.fileio.filetypes.protxml.jaxb.standard.Protein;
import umich.ms.fileio.filetypes.protxml.jaxb.standard.ProteinGroup;
import umich.ms.fileio.filetypes.protxml.jaxb.standard.ProteinSummary;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;

// TODO delete?
/**
 * Created by huangjs
 * <p>
 * Parser ProteinProphet algorithm inference result(prot.xml).
 */
@Getter
@Setter
@Log4j2
public class ProteinProphetParser {

    private String qvalityLocation;

    public List<MPProteinGroup> parse(String protXMLFile, String decoyPrefix) throws FileParsingException {
        ProteinSummary proteinSummary = ProtXmlParser.parse(Paths.get(protXMLFile));
        List<ProteinGroup> pgList = proteinSummary.getProteinGroup();
        List<MPProteinGroup> mpPGList = pgList.parallelStream()
                .map(pg -> {
                    MPProteinGroup mpPG = new MPProteinGroup();
                    List<MPProtein> mpProtList = pg.getProtein().stream()
                            .map(prot -> {
                                MPProtein mpProt = new MPProtein();
                                mpProt.setId(prot.getProteinName());
                                mpProt.setProb(prot.getProbability());
                                mpProt.setTarget(!mpProt.getId().startsWith(decoyPrefix));
                                mpProt.setAltProteinSet(
                                        prot.getIndistinguishableProtein().stream()
                                        .map(IndistinguishableProtein::getProteinName)
                                        .collect(Collectors.toSet())
                                );
                                // TODO add Subsuming ?
                                System.out.println(">>>");
                                System.out.println(prot.getProteinName());
                                System.out.println(prot.getSubsumingProteinEntry());
                                System.out.println("<<<");
                                return mpProt;
                            })
                            .collect(Collectors.toList());
                    mpPG.setMembers(mpProtList);
                    mpPG.setProb(pg.getProbability());
                    // all members are decoy, the group is decoy
                    mpPG.setTarget(mpPG.getMembers().stream().anyMatch(mpProt -> mpProt.getTarget()));
                    return mpPG;
                })
                .sorted(Comparator.comparingDouble(MPProteinGroup::getProb).reversed())
                .collect(Collectors.toList());
        return mpPGList;
    }

    /**
     * Calculate q-value.
     *
     * @param mpPGList the input must have been sorted on descending order
     */
    public void calQvalue(List<MPProteinGroup> mpPGList) throws IOException {
        // organize qvality input
        Map<Boolean, List<String>> targetDecoyPart = mpPGList.parallelStream()
                .collect(Collectors.groupingBy(MPProteinGroup::getTarget,
                        Collectors.collectingAndThen(Collectors.toList(),
                                (List<MPProteinGroup> list) -> list.stream()
                                        // the bigger scores are better
                                        .map((MPProteinGroup mpPG) -> String.valueOf(mpPG.getProb()))
                                        .collect(Collectors.toList()))
                ));
        Path targetPath = Files.createTempFile("target", ".txt");
        Path decoyPath = Files.createTempFile("decoy", ".txt");
        log.debug("create temporary files <{}> and <{}> to save target and decoy scores respectively", targetPath, decoyPath);
        Files.write(targetPath, targetDecoyPart.get(true), StandardCharsets.UTF_8,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        Files.write(decoyPath, targetDecoyPart.get(false), StandardCharsets.UTF_8,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        // run qvality
        String command = String.join(" ",
                qvalityLocation,
                targetPath.toString(),
                decoyPath.toString(),
                "-Y -d");
        AtomicInteger index = new AtomicInteger(0);
        Consumer<InputStream> outConsumer  = (InputStream in) -> {
            try (
                    BufferedReader br = new BufferedReader(new InputStreamReader(in))
            ) {
                String line;
                while ((line = br.readLine()) != null) {
                    String[] tmp = line.split("\t");
                    if (!tmp[0].equals("Score")) {
                        MPProteinGroup mpPG = mpPGList.get(index.get());
                        if(!mpPG.getTarget()){
                            System.err.println(mpPG);
                        }
                        mpPGList.get(index.getAndIncrement()).setQvalue(Double.parseDouble(tmp[2]));
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        };
        CommandExecutor.exec(command, outConsumer);
        // TODO delete annotation
//        // delete temporary files
//        Files.delete(targetPath);
//        Files.delete(decoyPath);
//        log.debug("delete temporary files <{}> and <{}>", targetPath, decoyPath);
    }

//    /**
//     * Calculate q-value.
//     *
//     * @param mpPGList the input must have been sorted
//     */
//    public void calQvalue2(List<MPProteinGroup> mpPGList) {
//        int decoy = 0;
//        int len = mpPGList.size();
//        for (int i = 0; i < len; i++) {
//            MPProteinGroup mpPG = mpPGList.get(i);
//            // the peptide group is decoy when all proteins are decoy
//            boolean isDecoy = mpPG.getMembers().stream()
//                    .allMatch(mpProt -> mpProt.getId().startsWith(decoyPrefix));
//            if (isDecoy) {
//                decoy++;
//            }
//            if (i == 0 || mpPG.getProb() != mpPGList.get(i - 1).getProb()) {
//                mpPG.setQvalue(decoy / (i + 1.0));
//            } else {
//                mpPG.setQvalue(mpPGList.get(i - 1).getQvalue());
//            }
//        }
//    }

    /**
     * Calculate q-value.
     *
     * @param mpPGList the input must have been sorted
     */
    public void calQvalue2(List<MPProteinGroup> mpPGList) {
        int decoy = 0;
        int sameProb = 0;
        int len = mpPGList.size();
        for (int i = 0; i < len; i++) {
            MPProteinGroup mpPG = mpPGList.get(i);
            if (!mpPG.getTarget()) {
                if(mpPG.getProb() == 1.0) {
                    System.err.println(mpPG);
                }
                decoy++;
            }
            if (i == len - 1 || mpPG.getProb() != mpPGList.get(i + 1).getProb()) {
                double qvalue = decoy / (i + 1.0);
                mpPG.setQvalue(qvalue);
                for (int j = 0; j < sameProb; j++) {
                    mpPGList.get(i - j - 1).setQvalue(qvalue);
                }
                if(mpPG.getProb() == 1.0){
                    System.out.println("decoy num = " + decoy);
                    System.out.println("total = " + ( i + 1));
                }
                sameProb = 0;
            } else {
                sameProb++;
            }
        }
    }

    public void writeTSV(List<MPProteinGroup> mpPGList, String outputFile) throws IOException {
        BufferedWriter bw = Files.newBufferedWriter(Paths.get(outputFile), StandardCharsets.UTF_8,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        bw.write(String.join("\t", "Protein Group", "probability", "Q-value") + System.lineSeparator());
        for (MPProteinGroup mpPG : mpPGList) {
            bw.write(String.join("\t",
                    mpPG.getMembers().stream()
                            .map(MPProtein::getId)
                            .collect(Collectors.joining(";")),
                    String.valueOf(mpPG.getProb()),
                    String.valueOf(mpPG.getQvalue())
                    ) + System.lineSeparator()
            );
        }
        bw.close();
    }

    public void statistic(String protXMLFile) throws FileParsingException {
        ProteinSummary proteinSummary = ProtXmlParser.parse(Paths.get(protXMLFile));
        List<ProteinGroup> pgList = proteinSummary.getProteinGroup();
        String decoyPrefix = "REV__";
        int targetGroup = 0;
        int decoyGroup = 0;
        int simiGroup = 0;
        int totalTarget = 0;
        int totalDecoy = 0;
        for (ProteinGroup pg : pgList) {
            if (pg.getProbability() >= 0.0) {
                int partDecoy = 0;
                List<Protein> protList = pg.getProtein();
                for (int i = 0; i < protList.size(); i++) {
                    if (protList.get(i).getProteinName().startsWith(decoyPrefix)) {
                        partDecoy++;
                        totalDecoy++;
                    } else {
                        totalTarget++;
                    }
                }
                if (partDecoy == protList.size()) {
                    decoyGroup++;
                } else if (partDecoy > 0) {
                    simiGroup++;
                } else {
                    targetGroup++;
                }
                // reset
                partDecoy = 0;
            }
        }
        log.info("totalTarget = {}", totalTarget);
        log.info("totalDecoy = {}", totalDecoy);
        log.info("targetGroup = {}", targetGroup);
        log.info("decoyGroup = {}", decoyGroup);
        log.info("simiGroup = {}", simiGroup);
        log.info("totalGroup = {}", (targetGroup + decoyGroup + simiGroup));
    }
}
