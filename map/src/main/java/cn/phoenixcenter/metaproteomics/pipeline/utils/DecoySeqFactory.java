package cn.phoenixcenter.metaproteomics.pipeline.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class DecoySeqFactory {

    public enum Type {
        MAXQUANT,
        REVERSE
    }

    public static String generate(Type type, StringBuilder sequence) {
        switch (type) {
            case MAXQUANT:
                return genMaxQuantDecoySeq(sequence);
            case REVERSE:
                return sequence.reverse().toString();
        }
        return null;
    }

    /**
     * Generate decoy sequence using MaxQuant method.
     * <p>
     * AAAAAR BBBBBR CC -> CCR BBBBBR AAAAA -> CR CBBBBR BAAAAA
     *
     * @param sequence
     */
    private static String genMaxQuantDecoySeq(StringBuilder sequence) {
        String reverse = sequence.reverse().toString();
        char[] seqArr = reverse.toCharArray();
        for (int i = 1; i < seqArr.length; i++) {
            if (seqArr[i] == 'R' || seqArr[i] == 'K') {
                char tmp = seqArr[i];
                seqArr[i] = seqArr[i - 1];
                seqArr[i - 1] = tmp;
            }
        }
        return String.valueOf(seqArr);
    }

    public static void genLibraryWithDecoy(Path originalLibPath, String decoyPrefix,
                                           DecoySeqFactory.Type decoyType, Path libFileWithDecoyPath
    ) throws IOException {
        BufferedReader br = Files.newBufferedReader(originalLibPath, StandardCharsets.UTF_8);
        BufferedWriter bw = Files.newBufferedWriter(libFileWithDecoyPath, StandardCharsets.UTF_8,
                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
        String line;
        String anno = null;
        StringBuilder sequence = null;
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">")) {
                if (sequence != null) {
                    // write target
                    bw.write(">" + anno + System.lineSeparator());
                    bw.write(sequence.toString() + System.lineSeparator());
                    // write decoy
                    bw.write(">" + decoyPrefix + anno + System.lineSeparator());
                    bw.write(generate(decoyType, sequence) + System.lineSeparator());
                }
                anno = line.substring(1);
                sequence = new StringBuilder();
            } else {
                sequence.append(line);
            }
        }
        // the last
        if (sequence != null) {
            // write target
            bw.write(">" + anno + System.lineSeparator());
            bw.write(sequence.toString() + System.lineSeparator());
            // write decoy
            bw.write(">" + decoyPrefix + anno + System.lineSeparator());
            bw.write(generate(decoyType, sequence) + System.lineSeparator());
        }
        br.close();
        bw.close();
    }
}
