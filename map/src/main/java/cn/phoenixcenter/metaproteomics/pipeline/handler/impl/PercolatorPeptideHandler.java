package cn.phoenixcenter.metaproteomics.pipeline.handler.impl;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.pipeline.base.ParsimonyType;
import cn.phoenixcenter.metaproteomics.pipeline.handler.IPeptideHandler;
import cn.phoenixcenter.metaproteomics.pipeline.handler.ProteinHelper;
import cn.phoenixcenter.metaproteomics.utils.FileReaderUtil;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PercolatorPeptideHandler implements IPeptideHandler {

    private Set<MPPeptide> mpPeptideSet;

    private Set<MPProtein> mpProteinSet;

    private final String delimiter = "\t";

    public PercolatorPeptideHandler(String peptideTSVFile) {
        try {
            mpPeptideSet = new HashSet<>();
            Map<String, MPPeptide> seq2Pept = new HashMap<>(50000);
            Map<String, MPProtein> id2Prot = new HashMap<>(50000);
            FileReaderUtil reader = new FileReaderUtil(peptideTSVFile, "\t", true);
            Consumer<String> rowConsumer = (String line) -> {
                String[] tmp = line.split("\t");
                String seq = tmp[reader.getColNum("sequence")].replaceAll("\\[.*\\]", "");
                double prob = 1 - Double.parseDouble(tmp[reader.getColNum("percolator PEP")]);
                if (seq2Pept.containsKey(seq2Pept)) {
                    MPPeptide mpPeptide = seq2Pept.get(seq);
                    // only remain peptide with larger prob
                    if (mpPeptide.getProb() < prob) {
                        mpPeptide.setProb(prob);
                        mpPeptide.setQvalue(Double.parseDouble(tmp[reader.getColNum("percolator q-value")]));
                        mpPeptide.setCharge(tmp[reader.getColNum("charge")]);
                        mpPeptide.setSpectrumCount(Integer.parseInt(tmp[reader.getColNum("total matches/spectrum")]));
                    }
                } else {
                    MPPeptide mpPeptide = new MPPeptide();
                    seq2Pept.put(seq, mpPeptide);
                    mpPeptide.setSequence(seq);
                    mpPeptide.setProb(prob);
                    mpPeptide.setQvalue(Double.parseDouble(tmp[reader.getColNum("percolator q-value")]));
                    mpPeptide.setCharge(tmp[reader.getColNum("charge")]);
                    mpPeptide.setSpectrumCount(Integer.parseInt(tmp[reader.getColNum("total matches/spectrum")]));
                    mpPeptide.setAssociatedProteinSet(
                            Stream.of(tmp[reader.getColNum("protein id")].split(","))
                                    .map(id -> {
                                        MPProtein mpProtein;
                                        if (!id2Prot.containsKey(id)) {
                                            mpProtein = new MPProtein(id);
                                            mpProtein.setContainedPeptideSet(new HashSet<>());
                                            id2Prot.put(id, mpProtein);
                                        }
                                        mpProtein = id2Prot.get(id);
                                        // add peptide for the protein
                                        mpProtein.getContainedPeptideSet().add(mpPeptide);
                                        return mpProtein;
                                    })
                                    .collect(Collectors.toSet())
                    );
                }
            };
            reader.read(rowConsumer);
            mpPeptideSet = new HashSet<>(seq2Pept.values());
            mpProteinSet = new HashSet<>(id2Prot.values());
            ProteinHelper.setAltProteinSet(mpProteinSet);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void writePeptide(Path outputPath) throws IOException {
        BufferedWriter writer = Files.newBufferedWriter(outputPath);
        writer.write(String.join(delimiter, "Sequence", "Q-value", "Probability", "PSM count", "Charge")
                + System.lineSeparator());
        for (MPPeptide pept : mpPeptideSet) {
            writer.write(String.join(delimiter,
                    pept.getSequence(),
                    String.valueOf(pept.getQvalue()),
                    String.valueOf(pept.getProb()),
                    String.valueOf(pept.getSpectrumCount()),
                    pept.getCharge()
            ) + System.lineSeparator());
        }
        writer.close();
    }

    @Override
    public void getPeptideProbDistribution() {

    }

    @Override
    public Map<ParsimonyType, Integer> getPeptideTypeDistribution(double qvalue) {
        Map<Boolean, Long> distribution = mpPeptideSet.stream()
                .filter(pept -> pept.getQvalue() < qvalue)
                .collect(Collectors.groupingBy(pept -> pept.isUniq(), Collectors.counting()));
        Map<ParsimonyType, Integer> peptideTypeDistribution = new HashMap<>(2);
        peptideTypeDistribution.put(ParsimonyType.DISTINCT,
                distribution.get(true) != null ? distribution.get(true).intValue() : 0);
        peptideTypeDistribution.put(ParsimonyType.SHARED,
                distribution.get(false) != null ? distribution.get(false).intValue() : 0);
        return peptideTypeDistribution;
    }

    @Override
    public List<Map.Entry<String, Long>> getPeptideChargeStateDistribution(double qvalue) {
        return mpPeptideSet.stream()
                .filter(pept -> pept.getQvalue() < qvalue)
                .collect(Collectors.groupingBy(pept -> pept.getCharge(), Collectors.counting()))
                .entrySet().stream()
                .sorted(Comparator.comparing(e -> e.getKey()))
                .collect(Collectors.toList());
    }

    @Override
    public Map<Integer, Long> getPeptideLengthDistribution(double qvalue) {
        return mpPeptideSet.stream()
                .filter(pept -> pept.getQvalue() < qvalue)
                .collect(Collectors.groupingBy(pept -> pept.getSequence().length(), Collectors.counting()));
    }


}