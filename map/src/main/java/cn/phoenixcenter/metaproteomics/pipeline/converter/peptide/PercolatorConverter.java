package cn.phoenixcenter.metaproteomics.pipeline.converter.peptide;

import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model.BipartiteGraphWithWeight;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model.PSMNode;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model.ProteinNode;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.EnzymeDigester;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.model.Peptide;
import cn.phoenixcenter.metaproteomics.utils.FileReaderUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by huangjs
 * <p>
 * Convert percolator result as peptide inference input.
 */

@Log4j2
public class PercolatorConverter {

    private final ObjectMapper objectMapper = new ObjectMapper();

    // picked peptide enzyme digester key (do not want to create too many class ^-^)
    private final String ppEnzymeDigesterKey = "enzymeDigester";

    // picked peptide psmCount list key
    private final String ppPeptideListKey = "peptideList";

    /**
     * Parse MascotPercolator result file as input of picked peptide algorithm.
     * <p>
     * 1. parse records as Peptide object
     * <p>
     * 2. add decoy prefix for proteins matched by decoy peptides.
     * <p>
     * The input file contains those field:
     * PSMId, score, q-value, posterior_error_prob, psmCount, proteinIds(tab-delimited).
     * Those fields are separated by tab.
     *
     * @param targetPeptTsvFile
     * @param decoyPeptTsvFile
     */
    public void convertMPToPickedProtein(String targetPeptTsvFile,
                                         String decoyPeptTsvFile,
                                         String decoyProtPrefix,
                                         EnzymeDigester.Enzyme enzyme,
                                         String outputFile) throws IOException {
        final EnzymeDigester enzymeDigester = new EnzymeDigester(enzyme);
        // min psmCount len; max psmCount len; max missed cleavage
        final int[] mark = new int[]{Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE};
        final Function<String[], Peptide> targetProcessor = (String[] fields) -> {
            Peptide peptide = new Peptide();
            peptide.setScore(Double.parseDouble(fields[1]));
            peptide.setQValue(Double.parseDouble(fields[2]));
            peptide.setPosteriorErrorProb(Double.parseDouble(fields[3]));
            // extract psmCount sequence
            String peptSeq = fields[4];
            if (peptSeq.charAt(1) == '.') {
                peptSeq = peptSeq.substring(2);
            }
            if (peptSeq.charAt(peptSeq.length() - 2) == '.') {
                peptSeq = peptSeq.substring(0, peptSeq.length() - 2);
            }
            if (peptSeq.indexOf("[") != -1) {
                peptSeq = peptSeq.replaceAll("\\[.*\\]", "");
            }
            peptide.setSequence(peptSeq);
            int len = peptSeq.length();
            if (len < mark[0]) {
                mark[0] = len;
            }
            if (len > mark[1]) {
                mark[1] = len;
            }
            int missedCleavageCount = enzymeDigester.obtainMissedCleavageNum(peptSeq);
            if (missedCleavageCount > mark[2]) {
                mark[2] = missedCleavageCount;
            }
            // extract peptide id
            peptide.setAsscociatedProtIdSet(
                    IntStream.range(5, fields.length)
                            .mapToObj(i -> fields[i])
                            .collect(Collectors.toSet())
            );
            return peptide;
        };
        // read target peptides
        FileReaderUtil fr = new FileReaderUtil(targetPeptTsvFile, "\t", true);
        List<Peptide> peptideList = fr.read(targetProcessor);
        int targetPeptCount = peptideList.size();
        // read decoy peptides and add prefix
        fr = new FileReaderUtil(decoyPeptTsvFile, "\t", true);
        Function<String[], Peptide> decoyProcessor = (String[] fields) -> {
            Peptide peptide = targetProcessor.apply(fields);
            peptide.setAsscociatedProtIdSet(
                    peptide.getAsscociatedProtIdSet().stream()
                            .map(prot -> decoyProtPrefix + prot)
                            .collect(Collectors.toSet())
            );
            return peptide;
        };
        peptideList.addAll(fr.read(decoyProcessor));
        // record enzyme info
        enzymeDigester.setMinPeptideLen(mark[0]);
        enzymeDigester.setMaxPeptideLen(mark[1]);
        enzymeDigester.setMaxMissedCleavageNum(mark[2]);
        log.info("target psmCount count: {}; decoy psmCount count: {}",
                targetPeptCount, peptideList.size() - targetPeptCount);
        log.info("enzyme info enzyme: {}, min psmCount length: {}, max psmCount length: {}, max missed cleavage: {}",
                enzyme.toString(),
                enzymeDigester.getMinPeptideLen(),
                enzymeDigester.getMaxPeptideLen(),
                enzymeDigester.getMaxMissedCleavageNum());
        Map<String, Object> result = new HashMap<>(2);
        result.put(ppEnzymeDigesterKey, enzymeDigester);
        result.put(ppPeptideListKey, peptideList);
        objectMapper.writeValue(new File(outputFile), result);
    }

    /**
     * Parse MascotPercolator percolator result file as input of fido algorithm.
     * <p>
     * 1. Build bipartite graph from percolator tsv result file.
     * <p>
     * 2. add decoy prefix for proteins matched by decoy peptides.
     * <p>
     * The input file contains those field:
     * PSMId, score, q-value, posterior_error_prob, psmCount, proteinIds(tab-delimited).
     * Those fields are separated by tab.
     */
    public BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> convertMPToFido(String targetPeptTsvFile,
                                                                                  String decoyPeptTsvFile,
                                                                                  String decoyProtPrefix) throws IOException {
        BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> graph = new BipartiteGraphWithWeight<>();
        Map<String, ProteinNode> id2ProtNodeMap = new HashMap<>();
        Map<String, PSMNode> id2PSMNodeMap = new HashMap<>();

        /** read target psmCount **/
        BufferedReader br = new BufferedReader(new FileReader(targetPeptTsvFile));
        String line;
        br.readLine(); // resume header line
        while ((line = br.readLine()) != null) {
            String[] tmp = line.split("\t");
            // weight
            Double weight = 1 - Double.parseDouble(tmp[3]);
            // psmCount node
            if (!id2PSMNodeMap.containsKey(tmp[4])) {
                id2PSMNodeMap.put(tmp[4], new PSMNode(tmp[4]));
            }
            PSMNode psmNode = id2PSMNodeMap.get(tmp[4]);
            // peptide nodes
            for (int i = 5; i < tmp.length; i++) {
                if (!id2ProtNodeMap.containsKey(tmp[i])) {
                    id2ProtNodeMap.put(tmp[i], new ProteinNode(tmp[i]));
                }
                ProteinNode protNode = id2ProtNodeMap.get(tmp[i]);
                graph.addEdge(protNode, psmNode, weight);
            }
        }
        br.close();

        /** read decoy psmCount **/
        br = new BufferedReader(new FileReader(decoyPeptTsvFile));
        br.readLine();
        while ((line = br.readLine()) != null) {
            String[] tmp = line.split("\t");
            // weight
            Double weight = 1 - Double.parseDouble(tmp[3]);
            // psmCount node
            if (!id2PSMNodeMap.containsKey(tmp[4])) {
                id2PSMNodeMap.put(tmp[4], new PSMNode(tmp[4]));
            }
            PSMNode psmNode = id2PSMNodeMap.get(tmp[4]);
            // peptide nodes
            for (int i = 5; i < tmp.length; i++) {
                String decoyPid = decoyProtPrefix + tmp[i];
                if (!id2ProtNodeMap.containsKey(decoyPid)) {
                    id2ProtNodeMap.put(decoyPid, new ProteinNode(decoyPid));
                }
                ProteinNode protNode = id2ProtNodeMap.get(decoyPid);
                graph.addEdge(protNode, psmNode, weight);
            }
        }
        br.close();
        return graph;
    }

    /**
     * Parse MascotPercolator percolator result file as input of fido algorithm(from ).
     */
    public void convertMP2Fido(){

    }


}
