package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.unique_peptide;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.util.Set;
import java.util.stream.Collectors;


@Log4j2
@NoArgsConstructor
public class UniquePeptideAlgorithm {

    /**
     * Obtain MPProteins which contain at least minUniquePeptCount(>=) unique peptides
     *
     * @param mpPeptideSet
     * @param mpProteinSet
     * @param minUniquePeptCount
     * @return
     */
    public Set<MPProtein> run(Set<MPPeptide> mpPeptideSet, Set<MPProtein> mpProteinSet, int minUniquePeptCount) {
        // obtain unique peptides
        Set<MPPeptide> uniquePeptideSet = mpPeptideSet.parallelStream()
//                .peek(mpPept -> System.out.println(mpPept.getAssociatedProteinSet().stream().map(MPProtein::getId).collect(Collectors.joining(";"))))
                .filter(mpPept -> mpPept.getAssociatedProteinSet().size() == 1)
                .collect(Collectors.toSet());
        log.debug("unique peptide count: {}", uniquePeptideSet.size());
        // obtain proteins whose unique peptide count is equal with or greater than threshold
        return mpProteinSet.parallelStream()
                .filter(mpProt -> mpProt.getContainedPeptideSet().stream()
                        .filter(mpPept -> uniquePeptideSet.contains(mpPept))
                        .count() >= minUniquePeptCount
                )
                .collect(Collectors.toSet());
    }
}
