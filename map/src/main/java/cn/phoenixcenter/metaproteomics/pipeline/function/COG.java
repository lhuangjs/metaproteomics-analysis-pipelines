package cn.phoenixcenter.metaproteomics.pipeline.function;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class COG {
    private String id;

    private String functionClass;

    private String annotation;

    public String fPrint() {
        return String.join("\t", id, functionClass, annotation);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        COG cog = (COG) o;
        return Objects.equals(id, cog.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
