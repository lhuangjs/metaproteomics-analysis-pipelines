//package cn.phoenixcenter.metaproteomics.pipeline.processor;
//
//import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
//import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
//import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
//import cn.phoenixcenter.metaproteomics.pipeline.base.MPProteinGroup;
//import cn.phoenixcenter.metaproteomics.pipeline.base.TreeNode;
//import cn.phoenixcenter.metaproteomics.taxonomy.LCAAnalysis;
//import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
//import cn.phoenixcenter.metaproteomics.taxonomy.TaxonSearcher;
//import cn.phoenixcenter.metaproteomics.utils.CommandExecutor;
//import com.fasterxml.jackson.core.JsonEncoding;
//import com.fasterxml.jackson.core.JsonFactory;
//import com.fasterxml.jackson.core.JsonGenerator;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;
//import lombok.extern.log4j.Log4j2;
//
//import java.io.*;
//import java.nio.charset.StandardCharsets;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.StandardOpenOption;
//import java.util.*;
//import java.util.concurrent.atomic.AtomicInteger;
//import java.util.function.Consumer;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//@Log4j2
//public class ProteinGroupPostprocess {
//
//    private static String qvalityLocation = GlobalConfig.getValue("qvality");
//
//    private static final Taxon.Rank[] ranks = Taxon.Rank.values();
//
//    private static final TaxonSearcher taxonSearcher = GlobalConfig.getObj(TaxonSearcher.class);
//
//    private static final ObjectMapper objectMapper = new ObjectMapper();
//
//
//    /**
//     * Delete decoy members from protein groups
//     *
//     * @param mpPGList
//     * @return
//     */
//    public static void delDecoyMemFromPG(List<MPProteinGroup> mpPGList) {
//        Iterator<MPProteinGroup> itr = mpPGList.iterator();
//        while (itr.hasNext()) {
//            MPProteinGroup mpPG = itr.next();
//            // remove decoy protein in group
//            List<MPProtein> members = mpPG.getMembers().stream()
//                    .filter(MPProtein::getTarget)
//                    .sorted(Comparator.comparingDouble(MPProtein::getProb).reversed())
//                    .collect(Collectors.toList());
//            if (members.size() > 0) {
//                mpPG.setMembers(members);
//            } else {
//                itr.remove();
//            }
//        }
//    }
//
//    /**
//     * Calculate LCA.
//     * <p>
//     * Note: we will ignore members whose taxon is null
//     *
//     * @param mpPGList
//     * @param tid2Taxon
//     */
//    public static void calLCA(List<MPProteinGroup> mpPGList,
//                              Map<Integer, Taxon> tid2Taxon) {
//        mpPGList.parallelStream()
//                .forEach(mpPG -> {
//                    Set<Integer> tidSet = mpPG.getMembers().stream()
//                            // ignore protein entries that taxon are null
//                            .filter(prot -> prot.getTaxon() != null)
//                            .map(prot -> prot.getTaxon().getId())
//                            .collect(Collectors.toSet());
//                    if (tidSet.size() > 0) {
//                        int lca = LCAAnalysis.calculateUnipeptLCA(tid2Taxon, tidSet);
//                        mpPG.setLca(taxonSearcher.getTaxonById(lca));
//                    }
//                });
//    }
//
//    /**
//     * @param mpProteinGroups
//     * @param mpPeptides
//     * @param ignoreSharedPeptide
//     * @return
//     */
//    public static List<MPProteinGroup> filterByLCA(Taxon.Rank rankThreshold,
//                                                   Collection<MPProteinGroup> mpProteinGroups,
//                                                   Collection<MPPeptide> mpPeptides,
//                                                   boolean ignoreSharedPeptide) {
//        List<MPProteinGroup> filteredPGList = mpProteinGroups.parallelStream()
//                .filter(mpPG -> {
//                    if (mpPG.getLca() == null || mpPG.getLca().getId() == 1) {
//                        // root
//                        return false;
//                    } else if (Taxon.Rank.stringToRank(mpPG.getLca().getRank()).index() >= rankThreshold.index()) {
//                        return true;
//                    } else {
//                        return false;
//                    }
//                })
//                .collect(Collectors.toList());
//        // update peptides because some proteins have been removed
//        List<MPProtein> filteredProtList = filteredPGList.parallelStream()
//                .flatMap(pg -> pg.getMembers().stream())
//                .collect(Collectors.toList());
//        ProteinPostprocess.updatePeptProt(new HashSet<>(mpPeptides), new HashSet<>(filteredProtList), ignoreSharedPeptide);
//        return filteredPGList;
//    }
//
//    /**
//     * @param mpProteinGroupList
//     */
//    public static void calPSM(List<MPProteinGroup> mpProteinGroupList) {
//        mpProteinGroupList.parallelStream()
//                .forEach(pg ->
//                        pg.setPsmCount(pg.getMembers().stream().mapToDouble(MPProtein::getPsmCount).sum())
//                );
//    }
//
//    public static Map<Taxon, List<MPProteinGroup>> groupByTaxon(List<MPProteinGroup> mpProteinGroupList) {
//        return mpProteinGroupList.stream()
//                .filter(prot -> prot.getLca() != null)
//                .collect(Collectors.groupingBy(MPProteinGroup::getLca));
//    }
//
//    public static Map<Taxon, Double> calTaxonDistribution(Map<Taxon, List<MPProteinGroup>> taxon2PGs) {
//        Map<Taxon, Double> taxon2TotalPSM = taxon2PGs.entrySet().stream()
//                .collect(Collectors.toMap(
//                        e -> e.getKey(),
//                        e -> e.getValue().stream().mapToDouble(MPProteinGroup::getPsmCount).sum()
//                ));
//        double totalPSMCount = taxon2TotalPSM.values().stream().mapToDouble(Double::doubleValue).sum();
//        return taxon2TotalPSM.entrySet().stream()
//                .collect(Collectors.toMap(
//                        e -> e.getKey(),
//                        e -> 100.0 * e.getValue() / totalPSMCount
//                ));
//    }
//
//    public static void writeTaxonDistribution(Map<Taxon, List<MPProteinGroup>> taxon2PGs, Path outputPath) throws IOException {
//        Map<Taxon, Double> taxon2Percentage = calTaxonDistribution(taxon2PGs);
//        List<String> lines = new ArrayList<>(taxon2Percentage.size() + 1);
//        lines.add(String.join("\t", "Taxon name", "Percentage(%)"));
//        lines.addAll(taxon2Percentage.entrySet().stream()
//                .sorted(Comparator.comparing(e -> e.getKey().getName()))
//                .map(e -> String.join("\t", e.getKey().getName(), String.valueOf(e.getValue())))
//                .collect(Collectors.toList())
//        );
//        Files.write(outputPath, lines);
//    }
//
//    public static void writePGResult(List<MPProteinGroup> mpPGList, Path resultPath) throws IOException {
//        final String delimiter = "\t";
//        BufferedWriter bw = Files.newBufferedWriter(resultPath);
//        String headers = String.join(delimiter, "Protein Group", "LCA id", "LCA name", "LCA rank",
//                "Total PSM Count", "Parsimony Type of Members", "PSM Count of Members", "Prob of Members",
//                "Peptide Count of Members");
//        bw.write(headers + System.lineSeparator());
//        for (MPProteinGroup pg : mpPGList) {
//            StringJoiner members = new StringJoiner(";");
//            StringJoiner types = new StringJoiner(";");
//            StringJoiner psmCounts = new StringJoiner(";");
//            StringJoiner probs = new StringJoiner(";");
//            StringJoiner peptCounts = new StringJoiner(";");
//            for (MPProtein mpProt : pg.getMembers()) {
//                members.add(mpProt.getId());
//                types.add(mpProt.getParsimonyType().toString());
//                psmCounts.add(String.valueOf(mpProt.getPsmCount()));
//                probs.add(String.valueOf(mpProt.getProb()));
//                peptCounts.add(String.valueOf(mpProt.getPeptideCount()));
//            }
//            Taxon lca = pg.getLca();
//            String lcaInfo = lca != null
//                    ? String.join(delimiter, String.valueOf(lca.getId()), lca.getName(), lca.getRank())
//                    : "\t\t";
//            bw.write(String.join(delimiter, members.toString(),
//                    lcaInfo,
//                    String.valueOf(pg.getPsmCount()),
//                    types.toString(),
//                    psmCounts.toString(),
//                    probs.toString(),
//                    peptCounts.toString()
//            ) + System.lineSeparator());
//        }
//        bw.close();
//    }
//
//    /**
//     * Calculate q-value.
//     *
//     * @param mpPGList the input must have been sorted on descending order
//     */
//    public static void calQvalue(List<MPProteinGroup> mpPGList) throws IOException {
//        // organize qvality input
//        Map<Boolean, List<String>> targetDecoyPart = mpPGList.parallelStream()
//                .collect(Collectors.groupingBy(MPProteinGroup::getTarget,
//                        Collectors.collectingAndThen(Collectors.toList(),
//                                (List<MPProteinGroup> list) -> list.stream()
//                                        // the bigger scores are better
//                                        .map((MPProteinGroup mpPG) -> String.valueOf(mpPG.getProb()))
//                                        .collect(Collectors.toList()))
//                ));
//        Path targetPath = Files.createTempFile("target", ".txt");
//        Path decoyPath = Files.createTempFile("decoy", ".txt");
//        log.debug("create temporary files <{}> and <{}> to save target and decoy scores respectively", targetPath, decoyPath);
//        Files.write(targetPath, targetDecoyPart.get(true), StandardCharsets.UTF_8,
//                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
//        Files.write(decoyPath, targetDecoyPart.get(false), StandardCharsets.UTF_8,
//                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
//        // run qvality
//        String command = String.join(" ",
//                qvalityLocation,
//                targetPath.toString(),
//                decoyPath.toString(),
//                "-Y -d");
//        AtomicInteger index = new AtomicInteger(0);
//        Consumer<InputStream> outConsumer = (InputStream in) -> {
//            try (
//                    BufferedReader br = new BufferedReader(new InputStreamReader(in))
//            ) {
//                String line;
//                while ((line = br.readLine()) != null) {
//                    String[] tmp = line.split("\t");
//                    if (!tmp[0].equals("Score")) {
//                        mpPGList.get(index.getAndIncrement()).setQvalue(Double.parseDouble(tmp[2]));
//                    }
//                }
//            } catch (IOException e) {
//                throw new RuntimeException(e);
//            }
//        };
//        CommandExecutor.exec(command, outConsumer);
//        // delete temporary files
//        Files.delete(targetPath);
//        Files.delete(decoyPath);
//        log.debug("delete temporary files <{}> and <{}>", targetPath, decoyPath);
//    }
//
//    /**
//     * Remove protein groups whose q-value are greater than threshold.
//     *
//     * @param mpPGList
//     * @param qvalueThreshold
//     * @return
//     */
//    public static List<MPProteinGroup> filterByQValue(List<MPProteinGroup> mpPGList, double qvalueThreshold) {
//        return mpPGList.stream()
//                .filter(pg -> pg.getQvalue() <= qvalueThreshold)
//                .collect(Collectors.toList());
//    }
//
//    public static void toTree(List<MPProteinGroup> mpPGList, String treeJsonFile) throws IOException {
//        /** group protein groups by LCA **/
//        Map<Taxon, List<MPProteinGroup>> taxon2PGs = mpPGList.stream()
//                .collect(Collectors.groupingBy(pg -> pg.getLca()));
//
//        /** build tree **/
//        Map<Integer, TreeNode<TreeNodeData>> visitedTid2Node = new HashMap<>();
//        // root node
//        Taxon rootTaxon = new Taxon(1);
//        rootTaxon.setRank("no rank");
//        rootTaxon.setName("root");
//        TreeNodeData rootData = new TreeNodeData();
//        rootData.setTaxon(rootTaxon);
//        rootData.setPgListSpecificToTaxon(taxon2PGs.get(rootTaxon));
//        TreeNode<TreeNodeData> root = new TreeNode<>();
//        root.setData(rootData);
//        for (Taxon taxon : taxon2PGs.keySet()) {
//            Map<Integer, Taxon> tid2Taxon = taxonSearcher.getTaxaByIds(new HashSet<>(taxon.getLineageIdMap().values()));
//            for (Taxon.Rank rank : ranks) {
//                if (taxon.existsTaxonOnRank(rank)) {
//                    Integer currTid = taxon.getLineageIdMap().get(rank.rankToString());
//                    if (!visitedTid2Node.containsKey(currTid)) {
//                        Taxon currTaxon = tid2Taxon.get(taxon.getLineageIdMap().get(rank.rankToString()));
//                        TreeNodeData data = new TreeNodeData();
//                        data.setTaxon(currTaxon);
//                        data.setPgListSpecificToTaxon(taxon2PGs.get(currTaxon));
//                        TreeNode<TreeNodeData> node = new TreeNode<>();
//                        node.setData(data);
//                        String parentRank = getParentRank(currTaxon);
//                        if (parentRank == null) {
//                            if (root.getChildren() == null) {
//                                root.setChildren(new ArrayList<>());
//                            }
//                            root.getChildren().add(node);
//                        } else {
//                            TreeNode<TreeNodeData> parentNode = visitedTid2Node
//                                    .get(currTaxon.getLineageIdMap().get(parentRank));
//                            if (parentNode.getChildren() == null) {
//                                parentNode.setChildren(new ArrayList<>());
//                            }
//                            parentNode.getChildren().add(node);
//                        }
//                        visitedTid2Node.put(currTaxon.getId(), node);
//                    }
//                }
//            }
//        }
//
//        /** add null node and placeholder node for visualization **/
//        for (TreeNode<TreeNodeData> node : root.getChildren()) {
//            addNode(root, node, Taxon.Rank.SUPERKINGDOM);
//        }
//        /** write json **/
//        JsonGenerator jsonGen = new JsonFactory().createGenerator(new File(treeJsonFile), JsonEncoding.UTF8);
//        writeTree(jsonGen, root);
//        jsonGen.close();
//    }
//
//    private static String getParentRank(Taxon taxon) {
//        String parentRank = null;
//        for (int i = Taxon.Rank.stringToRank(taxon.getRank()).index() - 1; i >= 0; i--) {
//            if (taxon.getLineageIdMap().containsKey(ranks[i].rankToString())) {
//                parentRank = ranks[i].rankToString();
//                break;
//            }
//        }
//        return parentRank;
//    }
//
//
//    private static void addNode(TreeNode<TreeNodeData> parentNode,
//                                TreeNode<TreeNodeData> node, Taxon.Rank rank) {
//        if (rank == Taxon.Rank.SPECIES) {
//            return;
//        } else if (node.getData().getTaxon().getRank().equals(rank.rankToString())) {
//            if (node.getChildren() != null) {
//                for (TreeNode<TreeNodeData> childNode : node.getChildren()) {
//                    addNode(node, childNode, ranks[rank.index() + 1]);
//                }
//            } else {
//                // there is no children for a node: add null node
//                for (int i = rank.index() + 1; i < ranks.length; i++) {
//                    if (i == ranks.length - 1) {
//                        // species
//                        TreeNode<TreeNodeData> treeNode = new TreeNode<>();
//                        treeNode.setNullNode(true);
//                    } else {
//                        TreeNode<TreeNodeData> treeNode = new TreeNode<>();
//                        treeNode.setNullNode(true);
//                        node.setChildren(Stream.of(treeNode).collect(Collectors.toList()));
//                    }
//                }
//            }
//        } else {
//            // current rank is null, so need to add a placeholder node
//            TreeNode<TreeNodeData> treeNode = new TreeNode<>();
//            treeNode.setPlaceholder(true);
//            treeNode.setChildren(parentNode.getChildren());
//            parentNode.setChildren(Stream.of(treeNode).collect(Collectors.toList()));
//            for (TreeNode<TreeNodeData> childNode : treeNode.getChildren()) {
//                addNode(treeNode, childNode, ranks[rank.index() + 1]);
//            }
//        }
//    }
//
//    private static void writeTree(JsonGenerator jsonGen, TreeNode<TreeNodeData> treeNode) throws IOException {
//        if (treeNode.isNullNode() || treeNode.isPlaceholder()) {
//            jsonGen.writeStartObject();
//            if (treeNode.isNullNode()) {
//                jsonGen.writeBooleanField("nullNode", true);
//            } else {
//                jsonGen.writeBooleanField("placeholder", true);
//            }
//            if (treeNode.getChildren() != null) {
//                jsonGen.writeArrayFieldStart("children");
//                for (TreeNode<TreeNodeData> childNode : treeNode.getChildren()) {
//                    writeTree(jsonGen, childNode);
//                }
//                jsonGen.writeEndObject();
//            }
//            jsonGen.writeEndObject();
//        } else {
//            jsonGen.writeStartObject();
//            jsonGen.writeStringField("nodeData", treeNode.getData().getTaxon().getName());
//            if (treeNode.getChildren() != null) {
//                jsonGen.writeArrayFieldStart("children");
//                for (TreeNode<TreeNodeData> childNode : treeNode.getChildren()) {
//                    writeTree(jsonGen, childNode);
//                }
//                jsonGen.writeEndArray();
//            }
//            jsonGen.writeEndObject();
//        }
//
//    }
//
//
//    @Getter
//    @Setter
//    @NoArgsConstructor
//    private static class TreeNodeData {
//        Taxon taxon;
//        List<MPProteinGroup> pgListSpecificToTaxon;
//    }
//}
