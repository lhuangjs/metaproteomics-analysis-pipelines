package cn.phoenixcenter.metaproteomics.pipeline.inference.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Store protein info from ProteinProphet
 */
@Getter
@Setter
@NoArgsConstructor
public class PPProtein extends Protein{

    // the probability of protein calculated by ProteinProphet
    private double probability;

}
