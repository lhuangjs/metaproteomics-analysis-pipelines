package cn.phoenixcenter.metaproteomics.pipeline;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.pipeline.converter.peptide.PeptideProphetConverter;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.FidoAlgorithm;
import cn.phoenixcenter.metaproteomics.pipeline.processor.ProteinPostprocess;
import cn.phoenixcenter.metaproteomics.pipeline.utils.CloneUtil;
import cn.phoenixcenter.metaproteomics.taxonomy.TaxonSearcher;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
public class MascotPipeline {

    private static final TaxonSearcher taxonSearcher = GlobalConfig.getObj(TaxonSearcher.class);


//    public static void runBGModel(Set<MPPeptide> mpPeptideSet,
//                                  Set<MPProteinWithType> mpProteinWithTypeSet,
//                                  Set<MPPeptideWithType> candidatePeptideWithTypeSet,
//                                  Set<MPProteinWithType> candidateProteinWithTypeSet) {
//        Set<MPPeptide> mpPeptideSetCopy = mpPeptideSet.parallelStream()
//                .map(mpPept -> CloneUtil.deepCopy(mpPept))
//                .collect(Collectors.toSet());
//        // run bipartite graph algorithm
//        BipartiteGraphAlgorithm bg = new BipartiteGraphAlgorithm();
//        Set<MPProteinWithType> candidateMPProtSet = bg.run(mpProteinWithTypeSet);
//        // update MPPeptide
//        Set<MPProtein> mpProteinSet = candidateMPProtSet.parallelStream()
//                .map(mpProtWithType -> new MPProtein(mpProtWithType))
//                .collect(Collectors.toSet());
//        SequenceHandler.updateMPPeptide(mpPeptideSetCopy, mpProteinSet);
//        // update parsimony type
//        ProteinGroupProcess.addParsimonyTypeInfo(mpPeptideSetCopy, mpProteinSet,
//                candidatePeptideWithTypeSet, candidateProteinWithTypeSet);
//    }

    // split the method to 2 method for performance
//    public static void runFidoModel(Collection<MPPeptide> mpPeptides,
//                                    Collection<MPProtein> mpProteins,
//                                    int accuraryLevel,
//                                    double fdrThreshold,
//                                    boolean ignoreSharedPeptide,
//                                    Collection<MPPeptide> candidatePeptides,
//                                    Collection<MPProtein> candidateProteins) {
//        if (candidatePeptides.size() != 0 || candidateProteins.size() != 0) {
//            throw new IllegalArgumentException("the size of candidatePeptides and candidateProteins must be empty when input");
//        }
//        // backup
//        Set<MPPeptide> mpPeptSetCopy = mpPeptides.parallelStream()
//                .map(mpPept -> CloneUtil.deepCopy(mpPept))
//                .collect(Collectors.toSet());
//        Set<MPProtein> mpProtSetCopy = mpProteins.parallelStream()
//                .map(mpProt -> CloneUtil.deepCopy(mpProt))
//                .collect(Collectors.toSet());
//        try {
//            // generate fido input file
//            Path graphPath = Files.createTempFile("graph", ".txt");
//            Path tdPath = Files.createTempFile("target-decoy", ".txt");
//            Path fidoRsPath = Files.createTempFile("fido", "txt");
//            log.debug("create temp files used in Fido: fido-graph[{}], fido-target-decoy[{}] and fido-result[{}]",
//                    graphPath, tdPath, fidoRsPath);
//            PeptideProphetConverter.convert2Fido(mpPeptSetCopy, mpProtSetCopy, graphPath.toString(), tdPath.toString());
//            // run fido
//            FidoAlgorithm fido = new FidoAlgorithm();
//            fido.run(accuraryLevel, graphPath.toString(), tdPath.toString(), fidoRsPath.toString());
//            fido.parseFidoResult(fidoRsPath.toString(), mpProtSetCopy);
//            // delete temp files
//            Files.delete(graphPath);
//            Files.delete(tdPath);
//            Files.delete(fidoRsPath);
//            log.debug("delete temp files: fido-graph[{}], fido-target-decoy[{}] and fido-result[{}]",
//                    graphPath, tdPath, fidoRsPath);
//            // filter by FDR
//            List<MPProtein> mpProteinList = new ArrayList<>(mpProtSetCopy);
//            ProteinPostprocess.calQvalue(mpProteinList);
//            mpProteinList = ProteinPostprocess.filterByQvalue(mpProteinList, fdrThreshold);
//            // update peptides and proteins
//            ProteinPostprocess.updatePeptProt(mpPeptSetCopy, mpProteinList, ignoreSharedPeptide);
//            candidatePeptides.addAll(mpPeptSetCopy);
//            candidateProteins.addAll(mpProteinList);
//        } catch (IOException e) {
//            e.printStackTrace();
//            System.exit(255);
//        }
//    }

    public static void runFidoModel(Collection<MPPeptide> mpPeptides,
                                    Collection<MPProtein> mpProteins,
                                    int accuraryLevel,
                                    Path fidoOutputPath) throws IOException {
        // generate fido input file
        Path graphPath = Files.createTempFile("graph", ".txt");
        Path tdPath = Files.createTempFile("target-decoy", ".txt");
        log.debug("create temp files used in Fido: fido-graph[{}] andfido-target-decoy[{}]",
                graphPath, tdPath);
        PeptideProphetConverter.convert2Fido(new HashSet<>(mpPeptides), new HashSet<>(mpProteins),
                graphPath, tdPath);
        // run fido
        FidoAlgorithm fido = new FidoAlgorithm();
        fido.run(accuraryLevel, graphPath, tdPath, fidoOutputPath);
        // delete temp files
        Files.delete(graphPath);
        Files.delete(tdPath);
        log.debug("delete temp files: fido-graph[{}] and fido-target-decoy[{}]",
                graphPath, tdPath);
    }

    /**
     * @param mpPeptideSet
     * @param mpProteinSet
     * @param fdrThreshold
     * @param ignoreSharedPept  ignore shared peptide when do PSM counting
     * @param fidoOutputPath
     * @param candidatePeptideSet
     * @param candidateProteinSet
     * @throws IOException
     */
    public static void parseFidoResult(Set<MPPeptide> mpPeptideSet,
                                       Set<MPProtein> mpProteinSet,
                                       double fdrThreshold,
                                       boolean ignoreSharedPept,
                                       Path fidoOutputPath,
                                       Set<MPPeptide> candidatePeptideSet,
                                       Set<MPProtein> candidateProteinSet) throws IOException {

        if (candidatePeptideSet.size() != 0 || candidateProteinSet.size() != 0) {
            throw new IllegalArgumentException("the size of candidatePeptides and candidateProteins must be empty when input");
        }
        // backup
        Set<MPPeptide> mpPeptSetCopy = mpPeptideSet.parallelStream()
                .map(mpPept -> CloneUtil.deepCopy(mpPept))
                .collect(Collectors.toSet());
        Set<MPProtein> mpProtSetCopy = mpProteinSet.parallelStream()
                .map(mpProt -> CloneUtil.deepCopy(mpProt))
                .collect(Collectors.toSet());
        // parse fido result
        FidoAlgorithm.parseFidoResult(fidoOutputPath, mpProtSetCopy);
        // filter by FDR
        List<MPProtein> mpProteinList = new ArrayList<>(mpProtSetCopy);
        ProteinPostprocess.calQvalue(mpProteinList);
        log.debug("protein count = {} before filtering by FDR = {}", mpProteinList.size(), fdrThreshold);
        mpProteinList = ProteinPostprocess.filterByQvalue(mpProteinList, fdrThreshold);
        log.debug("protein count = {} after filtering by FDR = {}", mpProteinList.size(), fdrThreshold);
        // update peptides and proteins
        candidatePeptideSet.addAll(mpPeptSetCopy);
        candidateProteinSet.addAll(mpProteinList);
        ProteinPostprocess.updatePeptProt(candidatePeptideSet, candidateProteinSet, ignoreSharedPept);
    }

    /**
     * @param mpPeptideSet
     * @param mpProteinSet
     * @param fdrThreshold
     * @param minUniqPept
     * @param ignoreSharedPept    ignore shared peptide when do PSM counting
     * @param fidoOutputPath
     * @param candidatePeptideSet
     * @param candidateProteinSet
     * @throws IOException
     */
    public static void parseFidoResult(Set<MPPeptide> mpPeptideSet,
                                       Set<MPProtein> mpProteinSet,
                                       double fdrThreshold,
                                       int minUniqPept,
                                       boolean ignoreSharedPept,
                                       Path fidoOutputPath,
                                       Set<MPPeptide> candidatePeptideSet,
                                       Set<MPProtein> candidateProteinSet) throws IOException {

        if (candidatePeptideSet.size() != 0 || candidateProteinSet.size() != 0) {
            throw new IllegalArgumentException("the size of candidatePeptides and candidateProteins must be empty when input");
        }
        // backup
        Set<MPPeptide> mpPeptSetCopy = mpPeptideSet.parallelStream()
                .map(mpPept -> CloneUtil.deepCopy(mpPept))
                .collect(Collectors.toSet());
        Set<MPProtein> mpProtSetCopy = mpProteinSet.parallelStream()
                .map(mpProt -> CloneUtil.deepCopy(mpProt))
                .collect(Collectors.toSet());
        // parse fido result
        FidoAlgorithm.parseFidoResult(fidoOutputPath, mpProtSetCopy);
        // filter by FDR
        List<MPProtein> mpProteinList = new ArrayList<>(mpProtSetCopy);
        ProteinPostprocess.calQvalue(mpProteinList);
        log.debug("protein count = {} before filtering by FDR = {}", mpProteinList.size(), fdrThreshold);
        mpProteinList = ProteinPostprocess.filterByQvalueAndUniqPept(mpProteinList, fdrThreshold, minUniqPept);
        log.debug("protein count = {} after filtering by FDR = {}", mpProteinList.size(), fdrThreshold);
        // update peptides and proteins
        candidatePeptideSet.addAll(mpPeptSetCopy);
        candidateProteinSet.addAll(mpProteinList);
        ProteinPostprocess.updatePeptProt(candidatePeptideSet, candidateProteinSet, ignoreSharedPept);
    }
}
