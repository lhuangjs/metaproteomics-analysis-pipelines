package cn.phoenixcenter.metaproteomics.pipeline.processor;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProteinGroup;
import cn.phoenixcenter.metaproteomics.pipeline.base.ParsimonyType;
import lombok.extern.log4j.Log4j2;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The class is used to calculate peptides and protein parsimony type
 * These attributes need be added before doing protein group:
 * <PRE>
 * 1. MPPeptide: sequence, associatedProteinSet
 * 2. MPProtein: id, containedPeptideSet
 * </PRE>
 */
@Log4j2
public class ProteinGroupProcess {

    public enum Relationship {
        SUPER_SUBSET,
        SUBSET_SUPER,
        // differentiable suggest that the two protein is differentiable, so the two protein
        // may or may not have the shared peptide.
        DIFFERENTIABLE
    }

    /**
     * Group proteins
     *
     * @param mpProteins
     * @return
     */
    public static List<MPProteinGroup> groupProteins(Collection<MPProtein> mpProteins) {
        log.debug("input protein size: {}", mpProteins.size());
        List<MPProteinGroup> mpProtGroupList = new ArrayList<>();
        List<MPProtein> members = null;
        Set<MPProtein> visited = new HashSet<>(mpProteins.size());
        for (MPProtein mpProt : mpProteins) {
            if (!visited.contains(mpProt)) {
                switch (mpProt.getParsimonyType()) {
                    case DISCRETE:
                        members = Stream.of(mpProt).collect(Collectors.toList());
                        break;
                    case DIFFERENTIABLE:
                    case SUPERSET:
                        members = Stream.of(mpProt).collect(Collectors.toList());
                        members.addAll(mpProt.getSubset().stream().collect(Collectors.toList()));
                        break;
                    case SUBSUMABLE:
                        Set<MPProtein> mpProtSet = new HashSet<>();
                        collectSubsumablePG(mpProt, mpProtSet);
                        members = new ArrayList<>(mpProtSet);
                        break;
                    default:
                        members = null;
                }
                if (members != null) {
                    // add visited members
                    visited.addAll(members);
                    MPProteinGroup mpPG = new MPProteinGroup();
                    mpPG.setMembers(members);
                    mpProtGroupList.add(mpPG);
                }
            }
        }
        if (visited.size() != mpProteins.size()) {
            log.error("Those {} proteins are not in any group: ", (mpProteins.size() - visited.size()));
            List<MPProtein> unvisited = mpProteins.stream()
                    .filter(prot -> !visited.contains(prot))
                    .collect(Collectors.toList());
            for (MPProtein prot : unvisited) {
                log.error("{} - {}", prot.getId(), prot.getParsimonyType().toString());
            }
            throw new RuntimeException();
        }
        return mpProtGroupList;
    }

    /**
     * A subsumable protein group contains:
     * 1. the subset proteins of the protein
     * 2. the differentiable proteins of the protein except proteins with "differentiable type"
     * 3. the subset and differentiable proteins(except proteins with "differentiable type") of above protein
     *
     * @param mpProtein
     */
    private static void collectSubsumablePG(MPProtein mpProtein, Set<MPProtein> members) {
        members.add(mpProtein);
        Set<MPProtein> potentialSet = new HashSet<>(mpProtein.getSubset());
        potentialSet.addAll(mpProtein.getDifferentiable().stream()
        .filter(mpProt -> mpProt.getParsimonyType() != ParsimonyType.DIFFERENTIABLE)
                .collect(Collectors.toSet())
        );
        potentialSet = potentialSet.stream()
                .filter(mpProt -> !members.contains(mpProt))
                .collect(Collectors.toSet());
        for(MPProtein mpProt : potentialSet){
            collectSubsumablePG(mpProt, members);
        }
    }

    // TODO delete?

    /**
     * Group protein groups and sort by probability in descending order
     */
    public List<MPProteinGroup> groupProteinsAndCalProb(Set<MPProtein> mpProtTypeSet) {
        List<MPProteinGroup> mpProtGroupList = new ArrayList<>();
        MPProteinGroup mpPG = null;
        // TODO only for check, delete the set
        Set<MPProtein> backup = new HashSet<>(mpProtTypeSet);
        for (MPProtein mpProtWithRelationship : mpProtTypeSet) {
            switch (mpProtWithRelationship.getParsimonyType()) {
                case DISCRETE:
                    mpPG = new MPProteinGroup();
                    mpPG.setProb(mpProtWithRelationship.getProb());
                    mpPG.setMembers(Stream.of(mpProtWithRelationship).collect(Collectors.toList()));
                    mpProtGroupList.add(mpPG);
                    // all members are decoy, the group is decoy
                    mpPG.setTarget(mpPG.getMembers().stream().anyMatch(mpProt -> mpProt.getTarget()));

                    backup.removeAll(mpPG.getMembers());
                    break;
                case DIFFERENTIABLE:
                case SUPERSET:
                    mpPG = new MPProteinGroup();
                    mpPG.setProb(mpProtWithRelationship.getProb());
                    mpPG.setMembers(Stream.of(mpProtWithRelationship).collect(Collectors.toList()));
                    mpPG.getMembers().addAll(mpProtWithRelationship.getSubset().stream()
                            .sorted(Comparator.comparingDouble(MPProtein::getProb).reversed())
                            .collect(Collectors.toList())
                    );
                    mpProtGroupList.add(mpPG);
                    // all members are decoy, the group is decoy
                    mpPG.setTarget(mpPG.getMembers().stream().anyMatch(MPProtein::getTarget));

                    backup.removeAll(mpPG.getMembers());
                    break;
                case SUBSUMABLE:
                    mpPG = new MPProteinGroup();
                    double p = mpProtWithRelationship.getDifferentiable().stream()
                            .map(MPProtein::getProb)
                            .reduce(mpProtWithRelationship.getProb(), (p1, p2) -> (1 - p1) * (1 - p2))
                            .doubleValue();
                    mpPG.setProb(1 - p * (1 - mpProtWithRelationship.getProb()));
                    mpPG.setMembers(Stream.of(mpProtWithRelationship).collect(Collectors.toList()));
                    mpPG.getMembers().addAll(mpProtWithRelationship.getDifferentiable());
                    Collections.sort(mpPG.getMembers(), Comparator.comparingDouble(MPProtein::getProb).reversed());
                    mpPG.getMembers().addAll(mpProtWithRelationship.getSubset().stream()
                            .sorted(Comparator.comparingDouble(MPProtein::getProb).reversed())
                            .collect(Collectors.toList())
                    );
                    mpProtGroupList.add(mpPG);
                    // all members are decoy, the group is decoy
                    mpPG.setTarget(mpPG.getMembers().stream().anyMatch(MPProtein::getTarget));

                    backup.removeAll(mpPG.getMembers());
                    break;
            }
        }
        if (backup.size() > 0) {
            System.err.println("Those proteins are not in any group");
            for (MPProtein prot : backup) {
                System.err.println(prot.getId());
            }
        }
        Collections.sort(mpProtGroupList, Comparator.comparingDouble(MPProteinGroup::getProb).reversed());
        return mpProtGroupList;
    }

    /**
     * Count the distribution of protein by protein type.
     *
     * @param MPProteins
     * @return
     */
    public static Map<ParsimonyType, Long> countProtDistribution(Collection<MPProtein> MPProteins) {
        return MPProteins.stream()
                .collect(Collectors.groupingBy(
                        MPProtein::getParsimonyType, Collectors.counting()
                ));
    }

    /**
     * Count the distribution of peptide by peptide type.
     *
     * @param MPPeptides
     * @return
     */
    public static Map<ParsimonyType, Long> countPeptDistribution(Collection<MPPeptide> MPPeptides) {
        return MPPeptides.stream()
                .collect(Collectors.groupingBy(
                        MPPeptide::getParsimonyType, Collectors.counting()
                ));
    }

    /**
     * Protein classification needs two steps:
     * 1. set peptide parsimony type;
     * 2. set protein parsimony type.
     * The method combine the two steps.
     * <p>
     * Before doing parsimony algorithm these attributes need be added:
     * <PRE>
     * 1. Peptide.sequence, Peptide.associatedProteinSet
     * 2. Protein.id, Protein.containedPeptideSet
     * </PRE>
     * <p>
     * NOTE: If you do some operation for MPPeptide or MPProtein,
     * those operation may affect mpPeptide or mpProtein.
     * So please backup source mpPeptide or mpProtein set when you need to use
     * source mpPeptide or mpProtein set in following analysis.
     *
     * @param mpPeptides
     * @param mpProteins
     */
    public static void addParsimonyTypeInfo(Collection<MPPeptide> mpPeptides,
                                            Collection<MPProtein> mpProteins) {
        setPeptParsimonyType(mpPeptides);
        setProtParsimonyType(mpPeptides, mpProteins);
    }

    /**
     * Set the parsimony type of peptide.
     *
     * @param mpPeptides
     */
    private static void setPeptParsimonyType(Collection<MPPeptide> mpPeptides) {
        mpPeptides.parallelStream()
                .forEach(mpPept -> {
                    if (mpPept.getAssociatedProteinSet().size() == 1) {
                        mpPept.setParsimonyType(ParsimonyType.DISTINCT);
                    } else if (mpPept.getAssociatedProteinSet().size() > 1) {
                        mpPept.setParsimonyType(ParsimonyType.SHARED);
                    } else {
                        throw new IllegalArgumentException("peptide <" + mpPept.getSequence()
                                + "> did not match any protein");
                    }
                });
    }

    /**
     * Protein parsimonyType: the peptide parsimony type must be process before setting protein parsimony type.
     * <PRE>
     * NOTE:
     * 1. Parsimony type analysis is applied in ascending hierarchy:
     * equivalent, subset, superset, subsumable, differential, and
     * distinct proteins.
     * 2. Each protein is counted in exactly one
     * category. It is possible for a protein to be listed in more than
     * one category, in which case it is counted in the highest category
     * in which it occurs where distinct is the highest category and
     * equivalent the lowest.
     * </PRE>
     *
     * @param mpPeptides
     * @param mpProteins
     */
    private static void setProtParsimonyType(Collection<MPPeptide> mpPeptides,
                                             Collection<MPProtein> mpProteins) {
        // set associated proteins for each protein: these proteins have at least one shared peptide.
        mpProteins.parallelStream().forEach(MPProtein::init);
        mpPeptides.stream() // not parallel stream because of thread unsafe
                // obtain shared peptide
                .filter(mpPeptType -> mpPeptType.getAssociatedProteinSet().size() > 1)
                .forEach(mpPeptType -> {
                    Set<MPProtein> protSetWithSharedPept = mpPeptType.getAssociatedProteinSet();
                    // those proteins have at least shared peptide
                    protSetWithSharedPept.stream()
                            .forEach(pro -> pro.addAssociatedProteinSet(protSetWithSharedPept));
                });

        /** set parsimony type for MPProtein **/
        for (MPProtein mpProt : mpProteins) {
            if (mpProt.containsDistinctPeptide()) {
                // exists distinct peptides
                if (mpProt.isAllDistinctPeptides()) {
                    mpProt.setParsimonyType(ParsimonyType.DISCRETE);
                } else {
                    mpProt.setParsimonyType(ParsimonyType.DIFFERENTIABLE);
                    classifyAssociatedProtSet(mpProt);
                }
            } else {
                // only contains shared peptides
                classifyAssociatedProtSet(mpProt);
                if (mpProt.getSuperset().size() > 0) {
                    // exists superset
                    mpProt.setParsimonyType(ParsimonyType.SUBSET);
                } else if (isSubsumable(mpProt)) {
                    // exists superset(combined superset)
                    mpProt.setParsimonyType(ParsimonyType.SUBSUMABLE);
                } else if (mpProt.getSubset().size() > 0) {
                    // exists subset
                    mpProt.setParsimonyType(ParsimonyType.SUPERSET);
                } else {
                    log.error("protein {} cannot be classified to any type", mpProt.getId());
                    System.exit(255);
                }
            }
        }
    }

    /**
     * Classify the specified protein and its associated proteins
     *
     * @param prot
     */
    private static void classifyAssociatedProtSet(MPProtein prot) {
        for (MPProtein prot2 : prot.getAssociatedProteinSet()) {
            switch (determineRelationship(prot, prot2)) {
                case SUBSET_SUPER:
                    prot.getSuperset().add(prot2);
                    prot2.getSubset().add(prot);
                    break;
                case SUPER_SUBSET:
                    prot.getSubset().add(prot2);
                    prot2.getSuperset().add(prot);
                    break;
                case DIFFERENTIABLE:
                    prot.getDifferentiable().add(prot2);
                    prot2.getDifferentiable().add(prot);
                    break;
                default:
                    log.error("Exist unknown relationship between the two protein <{}, {}>", prot, prot2);
                    System.exit(255);
            }
        }
    }


    /**
     * Determine the relationship between two proteins. The two proteins have at least one shared peptide.
     *
     * @param prot1
     * @param prot2
     * @return
     */
    private static Relationship determineRelationship(MPProtein prot1, MPProtein prot2) {
        if (prot1.getPeptideCount() > prot2.getPeptideCount()) {
            if (prot1.getContainedPeptideSet().containsAll(prot2.getContainedPeptideSet())) {
                return Relationship.SUPER_SUBSET;
            } else {
                return Relationship.DIFFERENTIABLE;
            }
        } else {
            if (prot2.getContainedPeptideSet().containsAll(prot1.getContainedPeptideSet())) {
                if (prot2.getPeptideCount() == prot1.getPeptideCount()) {
                    log.error("{}<{}> and {}<{}> have same peptide, " +
                                    "please confirm the field \"altProteinSet\" has been set before ProteinGroupProcess",
                            prot1.getId(), prot1.getContainedPeptideSet()
                                    .stream().map(MPPeptide::getSequence)
                                    .collect(Collectors.joining(",")),
                            prot2.getId(),
                            prot1.getContainedPeptideSet()
                                    .stream().map(MPPeptide::getSequence)
                                    .collect(Collectors.joining(",")));
                    System.exit(255);
                    return null; //  only for compile
                } else {
                    return Relationship.SUBSET_SUPER;
                }
            } else {
                return Relationship.DIFFERENTIABLE;
            }
        }
    }

    /**
     * @param prot
     * @return
     */
    private static boolean isSubsumable(MPProtein prot) {
        if (prot.getDifferentiable().size() < 2) {
            return false;
        }
        List<MPProtein> differentiableList = new ArrayList<>(prot.getDifferentiable());
        Set<MPPeptide> peptideSet = new HashSet<>();
        // obtain superset protein of the protein
        for (int i = 0; i < differentiableList.size() - 1; i++) {
            MPProtein prot1 = differentiableList.get(i);
            for (int j = i + 1; j < differentiableList.size(); j++) {
                MPProtein prot2 = differentiableList.get(j);
                if (determineRelationship(prot1, prot2) == Relationship.DIFFERENTIABLE) {
                    peptideSet.addAll(prot1.getContainedPeptideSet());
                    peptideSet.addAll(prot2.getContainedPeptideSet());
                }
            }
        }
        return peptideSet.containsAll(prot.getContainedPeptideSet());
    }
}
