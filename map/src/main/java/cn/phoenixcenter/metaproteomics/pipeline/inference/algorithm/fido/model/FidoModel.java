package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model;

/**
 * Created by huangjs
 */

import lombok.*;

/**
 * The parameters of Fido model.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FidoModel {

    private double alpha;

    private double beta;

    private double gamma;

    private double peptidePrior;
}