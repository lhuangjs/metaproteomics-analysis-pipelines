package cn.phoenixcenter.metaproteomics.pipeline.database_search.tide;

import cn.phoenixcenter.metaproteomics.base.ICommand;
import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.utils.CommandExecutor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TideSearchCmd implements ICommand {

    private static final long serialVersionUID = 1L;

    private final String cruxLocation = GlobalConfig.getValue("crux");

    private String paramFile;

    private String[] mgfFiles;

    private String libraryDir;

    private String outputDir;

    public TideSearchCmd(String paramFile, String[] mgfFiles, String libraryDir, String outputDir) {
        this.paramFile = paramFile;
        this.mgfFiles = mgfFiles;
        this.libraryDir = libraryDir;
        this.outputDir = outputDir;
    }

    @Override
    public String getDesc() {
        return "tide-search";
    }

    @Override
    public void execute() {
        String command = String.join(" ", cruxLocation,
                "tide-search",
                "--output-dir", outputDir,
                "--overwrite true",
                "--parameter-file", paramFile,
                String.join(" ", mgfFiles),
                libraryDir
        );
        CommandExecutor.exec(command);
    }
}
