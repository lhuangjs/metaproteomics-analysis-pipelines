package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein;

import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.model.Peptide;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.model.Protein;
import cn.phoenixcenter.metaproteomics.utils.CommandExecutor;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by huangjs
 */
@NoArgsConstructor
@Log4j2
public class PickedProteinAlgorithm {

    @Getter
    @Setter
    private String decoyProtPrefix;

    @Getter
    @Setter
    private EnzymeDigester enzymeDigester;

    private final DB db = DBMaker.memoryDB().make();

    // peptide => the id of proteins containing the peptide
    private HTreeMap<String, Set<String>> pept2Pids;

    @Getter
    // protein id => protein group members.
    private Map<String, List<String>> pid2ProteinGroup;

    private final JsonFactory jsonFactory = new JsonFactory();

    // TODO remove @Setter
    @Getter
    @Setter
    private List<Protein> repProteinList;

    // TODO only consider json file input
    public PickedProteinAlgorithm(String decoyProtPrefix, EnzymeDigester enzymeDigester) {
        this.decoyProtPrefix = decoyProtPrefix;
        this.enzymeDigester = enzymeDigester;
    }

    public void run(String inputJsonFile, String fastaFile, boolean genDecoyLibrary) throws IOException, InterruptedException {
        // parse input
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(new File(inputJsonFile));
        // TODO organize inputJsonFile with better method: set peptideList and enzymeDigester as constant?
        List<Peptide> peptideList = objectMapper.readValue(jsonNode.get("peptideList").toString(),
                new TypeReference<List<Peptide>>() {
                });
        enzymeDigester = objectMapper.readValue(jsonNode.get("enzymeDigester").toString(),
                EnzymeDigester.class);
        // perform algorithm
        Path pid2PepsTempJsonFile = mapPept2Prot(fastaFile, genDecoyLibrary);
        organizeProteinGroup(pid2PepsTempJsonFile);
        filterProtein(peptideList);
        pickedProteinStrategy();
        calProbabilities();
    }

    /**
     * Parse fasta library to obtain:
     * <p>
     * peptide => the id of proteins containing the peptide
     * <p>
     * json file to store protein id => theoretical enzyme digestion result
     *
     * @param fastaFile
     * @param genDecoyLibrary
     * @throws IOException
     */
    public Path mapPept2Prot(String fastaFile, boolean genDecoyLibrary) throws IOException {
        Path pid2PepsTempJsonFile = Files.createTempFile("pid2pepts", ".json");
        log.debug("create temporary file {} to store protein theoretical enzyme digestion result", pid2PepsTempJsonFile);
        JsonGenerator jsonGenerator = jsonFactory.createGenerator(pid2PepsTempJsonFile.toFile(), JsonEncoding.UTF8);
        jsonGenerator.writeStartArray();

        /** digest library sequences **/
        // peptide => the id of proteins containing the peptide
        pept2Pids = db.hashMap("pept2Pids", Serializer.STRING, Serializer.JAVA).create();

        // read fasta file
        BufferedReader br = new BufferedReader(new FileReader(fastaFile));
        int counter = 0;
        String line;
        StringBuilder sequence = new StringBuilder();
        String pid = null;
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">")) {
                if (pid != null) {
                    mapPept2Prot(pid, sequence, genDecoyLibrary, jsonGenerator);
                    ++counter;
                    // reset
                    sequence = sequence.delete(0, sequence.length());
                }
                pid = line.split("\\s", 2)[0].substring(1);
            } else {
                sequence.append(line);
            }
        }
        br.close();
        // deal with the last protein sequence
        mapPept2Prot(pid, sequence, jsonGenerator);
        ++counter;
        jsonGenerator.writeEndArray();
        jsonGenerator.close();
        log.info("total {} proteins in library", counter);
        return pid2PepsTempJsonFile;
    }

    /**
     * Do theoretical enzyme digestion and then add mappings: peptide sequence => protein id.
     * Meanwhile, the protein and its theoretical enzyme digestion result will be wrote into file.
     * One mapping stores a line using json string.
     *
     * @param pid
     * @param sequence
     * @param genDecoyLibrary
     * @param jsonGenerator
     * @throws IOException
     */
    private void mapPept2Prot(String pid, StringBuilder sequence, boolean genDecoyLibrary,
                              JsonGenerator jsonGenerator) throws IOException {
        mapPept2Prot(pid, sequence, jsonGenerator);
        if (genDecoyLibrary) {
            mapPept2Prot(decoyProtPrefix + pid, sequence.reverse(), jsonGenerator);
        }
    }

    private void mapPept2Prot(String pid, StringBuilder sequence,
                              JsonGenerator jsonGenerator) throws IOException {
        // digest
        List<AbstractMap.SimpleEntry<String, Boolean>> peptList = enzymeDigester.digest(sequence.toString());
        if (peptList.size() > 0) {
            for (AbstractMap.SimpleEntry<String, Boolean> entry : peptList) {
                String peptSeq = entry.getKey();
                if (pept2Pids.containsKey(peptSeq)) {
                    // cannot use `pept2Pids.get(peptSeq).add(pid)`
                    Set<String> pidSet = pept2Pids.get(peptSeq);
                    pidSet.add(pid);
                    pept2Pids.replace(peptSeq, pidSet);
                } else {
                    Set<String> pidSet = new HashSet<>();
                    pidSet.add(pid);
                    pept2Pids.put(peptSeq, pidSet);
                }
                if (entry.getValue()) {
                    // exists N-terminal Met
                    peptSeq = entry.getKey().substring(1);
                    if (pept2Pids.containsKey(peptSeq)) {
                        // cannot use `pept2Pids.get(peptSeq).add(pid)`
                        Set<String> pidSet = pept2Pids.get(peptSeq);
                        pidSet.add(pid);
                        pept2Pids.replace(peptSeq, pidSet);
                    } else {
                        Set<String> pidSet = new HashSet<>();
                        pidSet.add(pid);
                        pept2Pids.put(peptSeq, pidSet);
                    }
                }
            }
            // write into json file: {"CRAP_ALBU_BOVIN":[{"WVTFISLLLLFSSAYSR":false},{"MKWVTFISLLLLFSSAYSR":true}]}
            jsonGenerator.writeStartObject();
            jsonGenerator.writeFieldName(pid);
            jsonGenerator.writeStartArray();
            for (AbstractMap.SimpleEntry<String, Boolean> entry : peptList) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeBooleanField(entry.getKey(), entry.getValue());
                jsonGenerator.writeEndObject();
            }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
            jsonGenerator.writeRaw(System.lineSeparator());
        }
    }

    /**
     * @param pid2PepsTempJsonFile
     * @throws IOException
     */
    public void organizeProteinGroup(Path pid2PepsTempJsonFile) throws IOException {
        pid2ProteinGroup = db.hashMap("pid2ProteinGroup", Serializer.STRING, Serializer.JAVA).create();
        JsonParser jsonParser = jsonFactory.createParser(pid2PepsTempJsonFile.toFile());
        jsonParser.nextToken(); // [
        while (jsonParser.nextToken() != JsonToken.END_ARRAY) { // {

            /** read protein id => peptide : exists N-terminal Met **/
            String pid = jsonParser.nextFieldName();
            List<AbstractMap.SimpleEntry<String, Boolean>> peptList = new ArrayList<>();
            jsonParser.nextToken(); // [
            while (jsonParser.nextToken() != JsonToken.END_ARRAY) { // {
                peptList.add(new AbstractMap.SimpleEntry<>(jsonParser.nextFieldName(), jsonParser.nextBooleanValue()));
                jsonParser.nextToken(); // }
            }
            jsonParser.nextToken(); // }

            /** group proteins **/
            // get intersection of proteins containing peptides derived from the current protein
            AbstractMap.SimpleEntry<String, Boolean> firstPept = peptList.get(0);
            Set<String> intersection = new HashSet<>(pept2Pids.get(firstPept.getKey()));
            if (firstPept.getValue()) {
                // exists N-terminal Met
                String peptSeq = firstPept.getKey().substring(1);
                if (pept2Pids.containsKey(peptSeq)) {
                    intersection.retainAll(pept2Pids.get(peptSeq));
                }
            }
            if (intersection.size() > 1) {
                for (int i = 1; i < peptList.size(); i++) {
                    AbstractMap.SimpleEntry<String, Boolean> pept = peptList.get(i);
                    intersection.retainAll(pept2Pids.get(pept.getKey()));
                    if (pept.getValue()) {
                        // exists N-terminal Met
                        String peptSeq = pept.getKey().substring(1);
                        if (pept2Pids.containsKey(peptSeq)) {
                            intersection.retainAll(pept2Pids.get(peptSeq));
                        }
                    }
                    if (intersection.size() == 1) {
                        // the peptide is unique
                        break;
                    }
                }
            }
            if (intersection.size() > 1) {
                // there is at least one protein is superset protein for the current protein in intersection
                List<String> sortedIntersection = new ArrayList<>(intersection);
                Collections.sort(sortedIntersection, Comparator
                        // sorted by protein id in ascending order
                        .comparing((String member) -> member)
                        // sorted by peptide count for the protein in descending order
                        .thenComparing(Comparator.comparingInt((String member) -> peptList.size()))
                );
                String supersetPid = sortedIntersection.get(0);
                if (pid2ProteinGroup.containsKey(supersetPid)) {
                    // there may be some subset protein not added
                    if (!pid2ProteinGroup.get(supersetPid).contains(pid)) {
                        List<String> membList = pid2ProteinGroup.get(supersetPid);
                        membList.add(pid);
                        pid2ProteinGroup.put(supersetPid, membList);
                        pid2ProteinGroup.put(pid, membList);
                    }
                } else {
                    for (String protId : sortedIntersection) {
                        pid2ProteinGroup.put(protId, sortedIntersection);
                    }
                }
            } else {
                pid2ProteinGroup.put(pid, new ArrayList<>(intersection));
            }
        }
        jsonParser.close();
        pept2Pids = null; // destroy
        Files.delete(pid2PepsTempJsonFile);
        log.debug("create temporary file {} that store protein theoretical enzyme digestion result", pid2PepsTempJsonFile);
    }

    public void filterProtein(List<Peptide> peptideList) {

        /** only remain proteins that contain unique peptides **/
        log.debug("filter protein groups by unique peptides");
        // the representative protein id =>  the representative protein
        Map<String, Protein> repPid2Protein = new HashMap<>();
        int sharedPeptCount = 0;
        for (Peptide pept : peptideList) {
            /**
             * judge whether the peptide is unique:
             * the peptide is unique if it only belongs to one protein group
             */
            // obtain proteins containing the peptide sequence
            Set<String> pidSet = pept.getAsscociatedProtIdSet();
            if (pidSet.size() > 0) {
                Iterator<String> itr = pidSet.iterator();
                System.out.println(pept);
                System.out.println(pidSet);
                String pid = itr.next(); // first protein id
                List<String> proteinGroup = pid2ProteinGroup.get(pid); // corresponding protein group
                String repProtId = proteinGroup.get(0); // representative protein id
                boolean isUniquePept = true;
                while (itr.hasNext()) {
                    pid = itr.next();
                    if (!pid2ProteinGroup.get(pid).equals(proteinGroup)) {
                        // the peptide belongs to two or more protein groups
                        isUniquePept = false;
                        break;
                    }
                }

                /**
                 * if the peptide is unique, the representative protein of the protein group
                 * containing the peptide will be added into protein list
                 */
                if (isUniquePept) {
                    if (repPid2Protein.containsKey(repProtId)) {
                        repPid2Protein.get(repProtId).addPeptide(pept);
                    } else {
                        Protein protein = new Protein(repProtId);
                        protein.addPeptide(pept);
                        repPid2Protein.put(repProtId, protein);
                        if (repProtId.startsWith(decoyProtPrefix)) {
                            protein.setTarget(false);
                        } else {
                            protein.setTarget(true);
                        }
                    }
                } else {
                    sharedPeptCount++;
                }
            } else {
                log.warn("peptide <{}> does not match any proteins", pept.getSequence());
            }
        }
        repProteinList = new ArrayList<>(repPid2Protein.values());
        log.info("total input peptide count: {}; " +
                        "unique peptide count: {}; " +
                        "shared peptide count: {}",
                peptideList.size(),
                peptideList.size() - sharedPeptCount,
                sharedPeptCount);
    }

    /**
     * Executes the picked protein-FDR strategy from Savitski et al. 2015.
     * For protein groups, if one of the corresponding proteins has been observed
     * the whole group is eliminated.
     */
    public void pickedProteinStrategy() {
        log.debug("perform picked protein strategy");

        /** set scores for each protein in protein list and sorted by score in descending order **/
        // make maximum peptide score belonging a protein as protein group score
        repProteinList.parallelStream()
                .forEach(prot -> {
                    prot.setTarget(!prot.getId().startsWith(decoyProtPrefix));
                    prot.setScore(
                            prot.getContainedPeptSet().stream()
                                    .mapToDouble(Peptide::getScore)
                                    .max()
                                    .getAsDouble()
                    );
                });
        Collections.sort(repProteinList, Comparator.comparingDouble((Protein prot) -> prot.getScore()).reversed());

        /** perform picked protein strategy **/
        Set<String> targetProtIdSet = new HashSet<>();
        Set<String> decoyProtIdSet = new HashSet<>();
        int erasedProtCount = 0;
        Iterator<Protein> itr = repProteinList.iterator();
        while (itr.hasNext()) {
            String repPid = itr.next().getId();
            boolean isFound = false;
            for (String pid : pid2ProteinGroup.get(repPid)) {
                isFound = isFound || checkId(pid, targetProtIdSet, decoyProtIdSet);
            }
            if (isFound) {
                itr.remove();
                erasedProtCount++;
            }
        }
        if (erasedProtCount > 0) {
            log.info("total {} proteins have been erased and {} proteins were remained",
                    erasedProtCount, repProteinList.size());
        } else {
            // TODO quit?
            log.error("Warning: No target-decoy protein pairs were found for the picked protein strategy.");
        }
    }

    /**
     * @param pid
     * @param targetProtIdSet
     * @param decoyProtIdSet
     * @return
     */
    private boolean checkId(String pid, Set<String> targetProtIdSet, Set<String> decoyProtIdSet) {
        boolean isFound = false;
        boolean isTarget = !pid.startsWith(decoyProtPrefix);
        if (isTarget) {
            if (decoyProtIdSet.contains(decoyProtPrefix + pid)) {
                isFound = true;
            } else {
                targetProtIdSet.add(pid);
            }
        } else {
            if (targetProtIdSet.contains(pid.substring(decoyProtPrefix.length()))) {
                isFound = true;
            } else {
                decoyProtIdSet.add(pid);
            }
        }
        return isFound;
    }

    /**
     * Calculate protein groups <b>posterior error probability</b> using qvality
     * <p>
     * -r Indicating that the scoring mechanism is
     * reversed, i.e., that low scores are better
     * than higher scores
     * <p>
     * -Y Turns off the pi0 correction for search
     * results from a concatenated database.
     * <p>
     * -d Include negative hits (decoy) probabilities
     * in the results
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void calProbabilities() throws IOException, InterruptedException {
        log.info("calculate probability by call qvality");
        // group: true - target score; false - decoy score
        Map<Boolean, List<String>> targetDecoyPart = repProteinList.parallelStream()
                .collect(Collectors.groupingBy(Protein::isTarget,
                        Collectors.collectingAndThen(Collectors.toList(),
                                list -> list.stream()
                                        // the smaller scores are better
                                        .map((Protein prot) -> String.valueOf(-prot.getScore()))
                                        .collect(Collectors.toList()))
                ));
        // create temporary file to save target and decoy scores
        Path targetPath = Files.createTempFile("target", ".txt");
        Path decoyPath = Files.createTempFile("decoy", ".txt");
        log.debug("create temporary files <{}> and <{}> to save target and decoy scores respectively", targetPath, decoyPath);
        Files.write(targetPath, targetDecoyPart.get(true), StandardCharsets.UTF_8);
        Files.write(decoyPath, targetDecoyPart.get(false), StandardCharsets.UTF_8);
        // execute qvality command to calculate PEP scores
        String qvality = this.getClass().getResource("/software/qvality").getPath();
        // -r: Indicating that the scoring mechanism is reversed, i.e., that low scores are better
        // than higher scores
        // -Y: Turns off the pi0 correction for search results from a concatenated database.
        // -d: Include negative hits (decoy) probabilities in the results
        String command = String.join(" ",
                qvality,
                targetPath.toString(),
                decoyPath.toString(),
                "-r -Y -d");
        AtomicInteger index = new AtomicInteger(0);
        CommandExecutor.exec(command, (InputStream in) -> {
            try (
                    BufferedReader br = new BufferedReader(new InputStreamReader(in))
            ) {
                String line;
                while ((line = br.readLine()) != null) {
                    String[] tmp = line.split("\t");
                    if (!tmp[0].equals("Score")) {
                        Protein prot = repProteinList.get(index.getAndIncrement());
                        prot.setPosteriorErrorProb(Double.parseDouble(tmp[1]));
                        prot.setQValue(Double.parseDouble(tmp[2]));
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        });
        // TODO delete annotation
//        // delete temporary files
//        Files.delete(targetPath);
//        Files.delete(decoyPath);
//        log.debug("delete temporary files <{}> and <{}>", targetPath, decoyPath);
    }

    // TODO choose a suitable FDR
    public void calFDR() throws IOException {
        Collections.sort(repProteinList, Comparator.comparingDouble((Protein repProt) -> repProt.getScore()).reversed());
        int targetCount = 0;
        int decoyCount = 0;
        double pepSum = 0.0;
        int maxIndex = repProteinList.size() - 1;
        List<String> lines = new ArrayList<>(maxIndex + 2);
        lines.add(String.join("\t", "protein", "q-value based FDR",
                "PEP based FDR", "traditional FDR"));
        for (int i = 0; i <= maxIndex; i++) {
            Protein prot = repProteinList.get(i);
            if (prot.isTarget()) {
                targetCount++;
            } else {
                decoyCount++;
            }
            pepSum += prot.getPosteriorErrorProb();
            // test
            if (i != maxIndex && prot.getScore() < repProteinList.get(i + 1).getScore()) {
                log.error("score {}: {} < {}: {}", prot.getId(), repProteinList.get(i + 1).getId());
            }
            if (i != maxIndex && prot.getQValue() > repProteinList.get(i + 1).getQValue()) {
                log.error("q-value {}: {} < {}: {}", prot.getId(), repProteinList.get(i + 1).getId());
            }
            // calculate FDR
            if (i == maxIndex || prot.getPosteriorErrorProb() != repProteinList.get(i + 1).getPosteriorErrorProb()) {
                double pepFDR = pepSum / (i + 1);
                double tradFDR = (double) decoyCount / (i + 1);
                lines.add(String.join("\t", prot.getId(), String.valueOf(prot.getQValue()),
                        String.valueOf(pepFDR), String.valueOf(tradFDR))
                );
            }
        }
        Files.write(Paths.get("./results/inference/multi-FDR.tsv"), lines,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    /**
     * @param outputTsvFile
     * @throws IOException
     */
    public void exportReport(String outputTsvFile) throws IOException {
        BufferedWriter bw = Files.newBufferedWriter(Paths.get(outputTsvFile),
                StandardCharsets.UTF_8,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        bw.write(String.join("\t", "Protein group", "q-value",
                "Posterior error probability", "peptides") + System.lineSeparator());
        for (Protein prot : repProteinList) {
            bw.write(String.join("\t",
                    pid2ProteinGroup.get(prot.getId()).stream().collect(Collectors.joining(",")),
                    String.valueOf(prot.getQValue()),
                    String.valueOf(prot.getPosteriorErrorProb()),
                    prot.getContainedPeptSet().stream()
                            .map(Peptide::getSequence)
                            .collect(Collectors.joining(","))
            ) + System.lineSeparator());
        }
        bw.close();
    }
}
