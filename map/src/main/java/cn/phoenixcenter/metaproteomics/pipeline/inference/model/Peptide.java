package cn.phoenixcenter.metaproteomics.pipeline.inference.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class Peptide {

    // peptide sequence
    private String sequence;

    private ParsimonyType parsimonyType;

    // peptide-associated protein set: the protein set contained the peptide
    private Set<Protein> associatedProteinSet;

    public Peptide(String sequence) {
        this.sequence = sequence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Peptide)) return false;
        Peptide peptide = (Peptide) o;
        return this.sequence.equals(peptide.getSequence());
    }

    @Override
    public int hashCode() {
        return this.sequence.hashCode();
    }

    /**
     * @return peptide sequence, parsimonyType, protein name linked by "\t"
     */
    public String fPrint() {
        String protrinsStr = associatedProteinSet.size() == 0
                ? "null"
                : associatedProteinSet.stream()
                .map(Protein::getId)
                .sorted((String s1, String s2) -> s1.compareToIgnoreCase(s2))
                .collect(Collectors.joining(","));

        String parsimonyStr = parsimonyType == null ? "null" : parsimonyType.toString();
        return String.join("\t", sequence, parsimonyStr, protrinsStr);
    }
}
