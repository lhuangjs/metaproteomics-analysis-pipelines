package cn.phoenixcenter.metaproteomics.pipeline.inference.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class Protein {

    // the protein id should be unique in a input file
    protected String id;

    // the parsimony type of input nodeData
    protected ParsimonyType parsimonyType;

    // peptide set contained in the protein
    protected Set<Peptide> containedPeptideSet;

    public Protein(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Protein)) return false;
        Protein protein = (Protein) o;
        return this.id.equals(protein.getId());
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public String toString() {
        String containedPeptideSetStr = containedPeptideSet.parallelStream()
                .map(pep -> "{" + pep.getSequence() + pep.getParsimonyType() + "}")
                .collect(Collectors.joining(","));
        return "Protein{" +
                "id='" + id + '\'' +
                ", parsimonyType=" + parsimonyType +
                ", containedPeptideSet=[" + containedPeptideSetStr + "]" +
                '}';
    }
}
