package cn.phoenixcenter.metaproteomics.pipeline.utils;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import com.fasterxml.jackson.core.*;
import lombok.extern.log4j.Log4j2;
import org.junit.Assert;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Log4j2
public class SequenceHandler {

    private static final int defaultNullTid = GlobalConfig.getValueAsInt("defaultNullTid");

    /**
     * Read and parse fasta file and write result into json file.
     * The json file will be used for following analysis.
     * The json file looks like:
     * <PRE>
     * [{
     * "pid": "protein id", "tid": 2, "sequence": "AAAAAA..."
     * },
     * ...]
     * </PRE>
     * <p>
     * NOTE:
     * 1. the protein entry will be ignore if  a protein' id info was not captured
     * 2. the taxon id will be set "defaultNullTid" if a protein's taxon info was not captured;
     *
     * @param fastaPath
     * @param jsonPath
     */
    public static void fasta2Json(Path fastaPath, Pattern pidPattern, Pattern tidPattern, Path jsonPath) {
        try (JsonGenerator generator = new JsonFactory().createGenerator(jsonPath.toFile(), JsonEncoding.UTF8);
             BufferedReader br = Files.newBufferedReader(fastaPath, StandardCharsets.UTF_8)) {
            long totalCount = 0;
            long illegalPidCount = 0;
            long illegalTidCount = 0;
            generator.writeStartArray();
            String line;
            String pid = null;
            int tid = defaultNullTid;
            StringBuilder sequence = new StringBuilder();
            Matcher matcher = null;
            while ((line = br.readLine()) != null) {
                if (line.startsWith(">")) {
                    totalCount++;
                    if (pid != null) {
                        generator.writeStartObject();
                        generator.writeStringField("pid", pid);
                        generator.writeNumberField("tid", tid);
                        generator.writeStringField("seq", sequence.toString());
                        generator.writeEndObject();
                        generator.writeRaw(System.lineSeparator());
                        // reset
                        sequence = new StringBuilder(sequence.length());
                    }
                    matcher = pidPattern.matcher(line);
                    if (matcher.find()) {
                        pid = matcher.group(1);
                        matcher = tidPattern.matcher(line);
                        if (matcher.find()) {
                            tid = Integer.parseInt(matcher.group(1));
                        } else {
                            tid = defaultNullTid;
                            illegalTidCount++;
                            log.warn("cannot capture taxon id info in line: {}", line);
                        }
                    } else {
                        pid = null;
                        illegalPidCount++;
                        log.warn("cannot capture protein id info in line: {}", line);
                    }
                } else {
                    if (pid != null) {
                        sequence.append(line);
                    }
                }
            }
            // deal with the last entry
            if (pid != null) {
                generator.writeStartObject();
                generator.writeStringField("pid", pid);
                generator.writeNumberField("tid", tid);
                generator.writeStringField("seq", sequence.toString());
                generator.writeEndObject();
            }
            generator.writeEndArray();
            log.info("Total protein entry count: {}. In those entries, " +
                            "{} entries have been ignored because protein id cannot be captured " +
                            "and {} entries' taxon id were set {} because taxon id cannot be captured.",
                    totalCount, illegalPidCount, illegalTidCount, defaultNullTid);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void json2MPProteins(Path jsonPath,
                                       Consumer<MPProtein> consumer) {
        json2MPProteins(jsonPath, true, true, true, consumer);
    }

    /**
     * Parse the json file from method "fasta2Json"
     *
     * @param protJsonPath
     * @param setPid
     * @param setTid
     * @param setSequence
     * @param consumer
     */
    public static void json2MPProteins(Path protJsonPath,
                                       boolean setPid,
                                       boolean setTid,
                                       boolean setSequence,
                                       Consumer<MPProtein> consumer) {
        try (JsonParser parser = new JsonFactory().createParser(protJsonPath.toFile())) {
            parser.nextToken(); // [
            while (parser.nextToken() != JsonToken.END_ARRAY) { // {
                MPProtein mpProt = new MPProtein();
                // pid
                parser.nextToken();
                parser.nextToken();
                if (setPid) {
                    mpProt.setId(parser.getValueAsString());
                }
                //  tid
                parser.nextToken();
                parser.nextToken();
                if (setTid && parser.getValueAsInt() != defaultNullTid) {
                    Taxon taxon = new Taxon();
                    taxon.setId(parser.getValueAsInt());
                    mpProt.setTaxon(taxon);
                }
                // sequence
                parser.nextToken();
                parser.nextToken();
                if (setSequence) {
                    mpProt.setSequence(parser.getValueAsString());
                }
                if (consumer != null) {
                    consumer.accept(mpProt);
                }
                parser.nextToken(); // }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(255);
        }
    }

    /**
     * Set alternative info for MPProtein
     *
     * @param mpProteinSet output
     */
    public static void setAltProteinSet(Set<MPProtein> mpProteinSet) {
        // extract equivalent proteins containing same peptides
        List<MPProtein> mpProteinList = new ArrayList<>(mpProteinSet);
        log.debug("protein set size before collapsing equivalent proteins: {}", mpProteinList.size());
        // 2th, 3th ... equivalent proteins => 1th equivalent
        Map<MPProtein, MPProtein> prot2AltProt = new HashMap<>(mpProteinList.size());
        int altProtCount = 0;
        for (int i = 0; i < mpProteinList.size() - 1; i++) {
            MPProtein prot1 = mpProteinList.get(i);
            if (!prot2AltProt.containsKey(prot1)) {
                for (int j = i + 1; j < mpProteinList.size(); j++) {
                    MPProtein prot2 = mpProteinList.get(j);
                    if (prot1.getPeptideCount() == prot2.getPeptideCount()
                            && prot1.getContainedPeptideSet().containsAll(prot2.getContainedPeptideSet())) {
                        prot2AltProt.put(prot2, prot1);
                        altProtCount++;
                    }
                }
            }
        }
        log.debug("alternative protein pair count: {}", altProtCount);
        // set altProteinSet for MPProtein
        for (Map.Entry<MPProtein, MPProtein> entry : prot2AltProt.entrySet()) {
            MPProtein altMPProt = entry.getKey();
            // remove alternative protein from associated peptide
            for (MPPeptide mpPept : altMPProt.getContainedPeptideSet()) {
                mpPept.getAssociatedProteinSet().remove(altMPProt);
            }
            // add alternative protein for the protein
            MPProtein mainMPProt = entry.getValue();
            if (mainMPProt.getAltProteinSet() == null) {
                mainMPProt.setAltProteinSet(new HashSet<>());
            }
            mainMPProt.getAltProteinSet().add(altMPProt.getId());
            mpProteinSet.remove(altMPProt);
        }
        // only for check
        Assert.assertEquals(mpProteinList.size() - altProtCount, mpProteinSet.size());
        log.debug("protein set size after collapsing equivalent proteins: {}", mpProteinSet.size());
    }
}
