package cn.phoenixcenter.metaproteomics.pipeline.peptide_validation;

import cn.phoenixcenter.metaproteomics.base.Param;
import cn.phoenixcenter.metaproteomics.pipeline.peptide_validation.model.PercolatorParam;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.StringJoiner;

public class PercolatorParamParser {

    private static final JsonFactory jsonFactory = new JsonFactory();

    public static String toOptions(PercolatorParam percolatorParam) throws IllegalAccessException {
        Field[] fields = PercolatorParam.class.getDeclaredFields();
        Field featureFileField = null;
        StringJoiner options = new StringJoiner(" ");
        for (Field field : fields) {
            field.setAccessible(true);
            Param paramAnno = field.getAnnotation(Param.class);
            if (field.getName().equals("featureFile")) {
                featureFileField = field;
            } else {
                options.add("--" + paramAnno.paramName() + " " + field.get(percolatorParam));
            }
        }
        options.add(featureFileField.get(percolatorParam).toString());
        return options.toString();
    }

    /**
     * Write crux percolator to file.
     *
     * @param paramFilePath
     * @throws IOException
     * @throws IllegalAccessException
     */
    public static void toParamFile(PercolatorParam percolatorParam, Path paramFilePath) throws IOException, IllegalAccessException {
        BufferedWriter bw = Files.newBufferedWriter(paramFilePath);
        Field[] fields = PercolatorParam.class.getDeclaredFields();
        for (Field field : fields) {
            Param paramAnno = field.getAnnotation(Param.class);
            if (paramAnno != null && paramAnno.nativeParam()) {
                field.setAccessible(true);
                Object value = field.get(percolatorParam);
                bw.write("# " + paramAnno.paramDesc() + System.lineSeparator());
                if (value != null) {
                    bw.write(paramAnno.paramName() + "=" + value.toString() + System.lineSeparator());
                } else {
                    bw.write(paramAnno.paramName() + "=" + System.lineSeparator());
                }
            }
            bw.write(System.lineSeparator());
        }
        bw.close();
    }

    /**
     * Write TideParam to json file for front-end display.
     *
     * @param jsonFilePath
     * @throws IOException
     * @throws IllegalAccessException
     */
    public static void toJsonFile(PercolatorParam percolatorParam, Path jsonFilePath) throws IOException, IllegalAccessException {
        JsonGenerator gen = jsonFactory.createGenerator(jsonFilePath.toFile(), JsonEncoding.UTF8);
        gen.writeStartObject();
        Field[] fields = PercolatorParam.class.getDeclaredFields();
        for (Field field : fields) {
            // obtain PercolatorParam field annotation
            Param paramAnno = field.getAnnotation(Param.class);
            if (paramAnno != null && !paramAnno.paramAlias().isEmpty()) {
                field.setAccessible(true);
                // obtain param value
                Object value = field.get(percolatorParam);
                gen.writeObjectFieldStart(paramAnno.paramName());
                gen.writeStringField("fieldName", field.getName());
                gen.writeStringField("alias", paramAnno.paramAlias());
                gen.writeStringField("desc", paramAnno.paramDesc());
                gen.writeBooleanField("booleanType", paramAnno.booleanType());
                // write value
                if (field.getType().isAssignableFrom(int.class)) {
                    gen.writeNumberField("value", (int) value);
                } else if (field.getType().isAssignableFrom(double.class)) {
                    gen.writeNumberField("value", (double) value);
                } else if (field.getType().isAssignableFrom(boolean.class)) {
                    gen.writeBooleanField("value", (boolean) value);
                } else {
                    gen.writeStringField("value", (String) value);
                }
                // write candidate
                String[] candidates = paramAnno.candidates();
                if (candidates.length != 0) {
                    gen.writeArrayFieldStart("candidates");
                    for (String cand : candidates) {
                        gen.writeString(cand);
                    }
                    gen.writeEndArray();
                }
                gen.writeEndObject();
            }
        }
        gen.writeEndObject();
        gen.close();
    }
}
