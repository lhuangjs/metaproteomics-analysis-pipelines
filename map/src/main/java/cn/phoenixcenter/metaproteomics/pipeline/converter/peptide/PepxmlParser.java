package cn.phoenixcenter.metaproteomics.pipeline.converter.peptide;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import lombok.extern.log4j.Log4j2;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Use XMLStreamReader read pepxml file
 */
@Log4j2
public class PepxmlParser {

    private final static XMLInputFactory inputFactory = XMLInputFactory.newInstance();

    public static void parse(String pepxmlFile, BiConsumer<Integer, MPPeptide> consumer) throws FileNotFoundException, XMLStreamException {

        XMLStreamReader reader = inputFactory.createXMLStreamReader(new FileInputStream(pepxmlFile));
        while (reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case XMLStreamReader.START_ELEMENT:
                    String elementName = reader.getLocalName();
                    if (elementName.equals("spectrum_query")) {
                        parseQuery(reader, consumer);
                    }
                    break;
            }
        }
    }

    private static MPPeptide parseQuery(XMLStreamReader reader, BiConsumer<Integer, MPPeptide> consumer) throws XMLStreamException {
        String queryId = null;
        for (int i = 0; i < reader.getAttributeCount(); i++) {
            if (reader.getAttributeLocalName(i).equals("spectrum")) {
                queryId = reader.getAttributeValue(i);
                break;
            }
        }
        MPPeptide mpPeptide = null;
        Set<MPProtein> assosiatedProteinSet = null;
        Integer rank = null;
        while (reader.hasNext()) {
            int eventType = reader.next();
            if (eventType == XMLStreamReader.START_ELEMENT) {
                String elementName = reader.getLocalName();
                if (elementName.equals("search_hit")) { // <search_hit>
                    mpPeptide = new MPPeptide();
                    assosiatedProteinSet = new HashSet<>();
                    mpPeptide.setAssociatedProteinSet(assosiatedProteinSet);
                    for (int i = 0; i < reader.getAttributeCount(); i++) {
                        String attr = reader.getAttributeLocalName(i);
                        if (attr.equals("hit_rank")) {
                            rank = Integer.valueOf(reader.getAttributeValue(i));
                            if (rank.intValue() > 1) {
                                log.warn("query <{}> has multiple hit", queryId);
                            }
                        } else if (attr.equals("peptide")) {
                            mpPeptide.setSequence(reader.getAttributeValue(i));
                        } else if (attr.equals("protein")) {
                            assosiatedProteinSet.add(new MPProtein(reader.getAttributeValue(i)));
                        }
                    }
                } else if (elementName.equals("alternative_protein")) { // <alternative_protein/>
                    for (int i = 0; i < reader.getAttributeCount(); i++) {
                        if (reader.getAttributeLocalName(i).equals("protein")) {
                            assosiatedProteinSet.add(new MPProtein(reader.getAttributeValue(i)));
                            break;
                        }
                    }
                } else if (elementName.equals("analysis_result")) {
                    parseAnalysisResult(reader, mpPeptide); // <analysis_result>
                }
            } else if (eventType == XMLStreamReader.END_ELEMENT && reader.getLocalName().equals("search_hit")) {
                consumer.accept(rank, mpPeptide);
            } else if (eventType == XMLStreamReader.END_ELEMENT && reader.getLocalName().equals("spectrum_query")) { // </spectrum_query>
                break;
            }
        }
        return mpPeptide;
    }

    private static void parseAnalysisResult(XMLStreamReader reader, MPPeptide mpPeptide) throws XMLStreamException {
        while (reader.hasNext()) {
            int eventType = reader.next();
            if (eventType == XMLStreamReader.START_ELEMENT) {
                for (int i = 0; i < reader.getAttributeCount(); i++) {
                    String attr = reader.getAttributeLocalName(i);
                    if (attr.equals("probability")) {
                        mpPeptide.setProb(Double.parseDouble(reader.getAttributeValue(i)));
                    } else if (attr.equals("q-Value")) {
                        mpPeptide.setQvalue(Double.parseDouble(reader.getAttributeValue(i)));
                    }
                }
            } else if (eventType == XMLStreamReader.END_ELEMENT) {
                break;
            }
        }
    }

//    public static void convert2PeptProt(String pepxml, Set<MPPeptide> mpPeptideSet, Set<MPProtein> mpProteinSet) throws FileNotFoundException, XMLStreamException {
//        if (mpPeptideSet.size() != 0 || mpProteinSet.size() != 0) {
//            throw new IllegalArgumentException("the size must be 0 for input arguments mpPeptideSet and mpProteinSet");
//        }
//        /** parse pepxml file to get MPPeptide set **/
//        Map<String, MPPeptide> seq2MPPept = new HashMap<>();
//        Map<String, MPProtein> pid2MPProt = new HashMap<>();
//        BiConsumer<Integer, MPPeptide> consumer = (Integer rank, MPPeptide mpPeptide) -> {
//            if (rank == 1) {
//                if (seq2MPPept.containsKey(mpPeptide.getSequence())) {
//                    if (seq2MPPept.get(mpPeptide.getSequence()).getProb() < mpPeptide.getProb()) {
//                        // current PSM is a better PSM
//                        seq2MPPept.get(mpPeptide.getSequence()).setProb(mpPeptide.getProb());
//                    }
//                } else {
//                    seq2MPPept.put(mpPeptide.getSequence(), mpPeptide);
//                    mpPeptide.getAssociatedProteinSet().stream()
//                            .filter(mpProt -> pid2MPProt.containsKey(mpProt.getId()))
//                }
//            }
//            seq2MPPept.get(mpPeptide.getSequence()).increaseSpectrumCount();
//
//        };
//        parse(pepxml, consumer);
//        mpPeptideSet.addAll(seq2MPPept.values());
//        mpProteinSet.addAll()
//    }

    @Deprecated
    public static void search(String pepxmlFile, String label) throws IOException {
        long lineNum = 0;
        Pattern queryPat = Pattern.compile("spectrum_query spectrum=\"(\\S+)\"");
        Pattern pidPat = Pattern.compile("protein=\"(" + label + "\\S+)\"");
        BufferedReader br = Files.newBufferedReader(Paths.get(pepxmlFile));
        String qid = null;
        String line;
        while ((line = br.readLine()) != null) {
            lineNum++;
            Matcher m = queryPat.matcher(line);
            if (m.find()) {
                qid = m.group(1);
                continue;
            }
            m = pidPat.matcher(line);
            if (m.find()) {
                System.out.println(lineNum + " " + qid + ": " + m.group(1));
            }
        }
        br.close();


    }
}
