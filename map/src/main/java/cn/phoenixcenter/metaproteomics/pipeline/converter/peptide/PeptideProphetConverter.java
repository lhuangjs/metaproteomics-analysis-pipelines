package cn.phoenixcenter.metaproteomics.pipeline.converter.peptide;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model.BipartiteGraphWithWeight;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model.PSMNode;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model.ProteinNode;
import cn.phoenixcenter.metaproteomics.pipeline.processor.ProteinPostprocess;
import cn.phoenixcenter.metaproteomics.pipeline.utils.SequenceHandler;
import lombok.extern.log4j.Log4j2;
import umich.ms.fileio.exceptions.FileParsingException;
import umich.ms.fileio.filetypes.pepxml.PepXmlParser;
import umich.ms.fileio.filetypes.pepxml.jaxb.standard.AltProteinDataType;
import umich.ms.fileio.filetypes.pepxml.jaxb.standard.PeptideprophetResult;
import umich.ms.fileio.filetypes.pepxml.jaxb.standard.SearchHit;
import umich.ms.fileio.filetypes.pepxml.jaxb.standard.SpectrumQuery;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by huangjs
 * <p>
 * Convert PeptideProphetEnhancer result as peptide inference input.
 */
@Log4j2
public class PeptideProphetConverter {

    public static BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> convert2Fido(String pepXML) throws FileParsingException {
        BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> graph = new BipartiteGraphWithWeight<>();
        List<SpectrumQuery> specQueryList = PepXmlParser.parse(Paths.get(pepXML))
                .getMsmsRunSummary()
                .get(0)
                .getSpectrumQuery();
        for (SpectrumQuery specQuery : specQueryList) {
            SearchHit searchHit = specQuery.getSearchResult().get(0).getSearchHit().get(0);
            String peptide = searchHit.getPeptide();
            List<String> pidList = searchHit.getAlternativeProtein().stream()
                    .map(altProt -> altProt.getProtein())
                    .collect(Collectors.toList());
            pidList.add(searchHit.getProtein());
            double prob = ((PeptideprophetResult) (searchHit.getAnalysisResult().get(0).getAny().get(0))).getProbability();
            for (String pid : pidList) {
                ProteinNode protNode = new ProteinNode(pid);
                PSMNode psmNode = new PSMNode(peptide);
                graph.addEdge(protNode, psmNode, prob);
            }
        }
        return graph;
    }

    /**
     * Convert pepxml file as fido input.
     * Fido needs two input files:
     * <p>
     * 1. graph file, like:
     * <PRE>
     * e peptide_string
     * r peptide that would create this psmCount using theoretical digest
     * r second peptide
     * ...
     * r final peptide
     * p probability of the psmCount match to the spectrum (given by PeptideProphetEnhancer or comparable).
     * </PRE>
     * <p>
     * 2. target-decoy file
     * <PRE>
     * { target_prot1 , target_prot2 , ... target_protn }
     * { decoy_prot1 , decoy_prot2 , ... decoy_protn }
     * </PRE>
     *
     * @param mpPeptideSet
     * @param mpProteinSet
     * @param graphPath
     * @param targetDecoyPath
     * @throws IOException
     */
    public static void convert2Fido(Set<MPPeptide> mpPeptideSet, Set<MPProtein> mpProteinSet,
                                    Path graphPath, Path targetDecoyPath) throws IOException {
        /** write graph and target-decoy files **/
        // graph file
        BufferedWriter graphWriter = Files.newBufferedWriter(graphPath, StandardCharsets.UTF_8,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        for (MPPeptide mpPept : mpPeptideSet) {
            graphWriter.write(String.join(System.lineSeparator(),
                    "e " + mpPept.getSequence(),
                    mpPept.getAssociatedProteinSet().stream()
                            .map(prot -> "r " + prot.getId())
                            .collect(Collectors.joining(System.lineSeparator())),
                    "p " + mpPept.getProb()
                    ) + System.lineSeparator()
            );
        }
        graphWriter.close();
        // target-decoy file
        Map<Boolean, String> targetDecoy = mpProteinSet.stream()
                .collect(Collectors.groupingBy(
                        MPProtein::getTarget,
                        Collectors.mapping(MPProtein::getId, Collectors.joining(" , "))
                ));
        BufferedWriter tdWriter = Files.newBufferedWriter(targetDecoyPath, StandardCharsets.UTF_8,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        tdWriter.write("{ " + targetDecoy.get(true) + " }" + System.lineSeparator());
        tdWriter.write("{ " + targetDecoy.get(false) + " }" + System.lineSeparator());
        tdWriter.close();
    }

    /**
     * Parse pepxml file to add new MPPeptide(sequence, associatedProteinSet, psmCount)
     * and MPProtein(id, isTarget, containedPeptideSet, altProteinSet) in to set.
     * Only remain the best PSM when multiple spectra match one peptide
     *
     * @param pepxmlPath
     * @param decoyPrefix
     * @param ignoreSharedPept
     * @param mpPeptideSet     output, it contains nothing when it is come
     * @param mpProteinSet     output, it contains nothing when it is come
     * @throws FileParsingException
     */
    public static void convert2PeptProt(Path pepxmlPath, String decoyPrefix,
                                        boolean ignoreSharedPept,
                                        Set<MPPeptide> mpPeptideSet,
                                        Set<MPProtein> mpProteinSet) throws FileParsingException {
        if (mpPeptideSet.size() != 0 || mpProteinSet.size() != 0) {
            throw new IllegalArgumentException("the size must be 0 for input arguments mpPeptideSet and mpProteinSet");
        }
        /** parse pepxml file to get MPPeptide set **/
        Map<String, MPPeptide> seq2MPPept = new HashMap<>();
        Map<String, MPProtein> pid2MPProt = new HashMap<>();
        List<SpectrumQuery> specQueryList = PepXmlParser.parse(pepxmlPath)
                .getMsmsRunSummary()
                .get(0)
                .getSpectrumQuery();
        for (SpectrumQuery specQuery : specQueryList) {
            SearchHit searchHit = specQuery.getSearchResult().get(0).getSearchHit().get(0);
            // peptide
            String sequence = searchHit.getPeptide();
            // probability
            double prob = ((PeptideprophetResult) (searchHit.getAnalysisResult().get(0).getAny().get(0)))
                    .getProbability();
            if (seq2MPPept.containsKey(sequence)) {
                if (seq2MPPept.get(sequence).getProb() > prob) {
                    // current psmCount is a better psmCount
                    seq2MPPept.get(sequence).setProb(prob);
                }
                // spectrum count +1
                seq2MPPept.get(sequence).increaseSpectrumCount();
            } else {
                // assemble MPPeptide
                MPPeptide mpPept = new MPPeptide(sequence);
                mpPept.setProb(prob);
                mpPept.setSpectrumCount(1);
                // proteins
                Set<String> pidSet = searchHit.getAlternativeProtein().stream()
                        .map(AltProteinDataType::getProtein)
                        .collect(Collectors.toSet());
                pidSet.add(searchHit.getProtein());
                Set<MPProtein> mpProtSet = new HashSet<>(pidSet.size());
                for (String pid : pidSet) {
                    if (!pid2MPProt.containsKey(pid)) {
                        // add new MPProtein
                        MPProtein mpProt = new MPProtein(pid);
                        if (pid.startsWith(decoyPrefix)) {
                            mpProt.setTarget(false);
                        } else {
                            mpProt.setTarget(true);
                        }
                        pid2MPProt.put(pid, mpProt);
                    }
                    MPProtein mpProt = pid2MPProt.get(pid);
                    if (mpProt.getContainedPeptideSet() == null) {
                        pid2MPProt.get(pid).setContainedPeptideSet(new HashSet<>());
                    }
                    mpProt.getContainedPeptideSet().add(mpPept);
                    mpProtSet.add(mpProt);
                }
                mpPept.setAssociatedProteinSet(mpProtSet);
                seq2MPPept.put(sequence, mpPept);
            }
        }
        mpPeptideSet.addAll(seq2MPPept.values());
        log.debug("peptide count: {}", mpPeptideSet.size());
        mpProteinSet.addAll(pid2MPProt.values());
        // set alternative protein set
        SequenceHandler.setAltProteinSet(mpProteinSet);
        // add PSM info
        ProteinPostprocess.calPSMCount(mpProteinSet, ignoreSharedPept);
    }
}