package cn.phoenixcenter.metaproteomics.pipeline.processor;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PeptidePostprocess {
    public static void writePeptide(Collection<MPPeptide> mpPeptides, Path outputPath) throws IOException {
        final String delimiter = "\t";
        BufferedWriter bw = Files.newBufferedWriter(outputPath);
        bw.write(String.join(delimiter, "sequence", "probability", "q-value",
                "spectrum count", "proteins")
                + System.lineSeparator());
        for (MPPeptide mpPept : mpPeptides) {
            bw.write(String.join(delimiter, mpPept.getSequence(),
                    String.valueOf(mpPept.getProb()), String.valueOf(mpPept.getQvalue()),
                    String.valueOf(mpPept.getSpectrumCount()),
                    mpPept.getAssociatedProteinSet().stream()
                            .flatMap(mpProt -> {
                                Stream.Builder<String> builder = Stream.builder();
                                if(mpProt.getAltProteinSet() != null && mpProt.getAltProteinSet().size() > 0){
                                    mpProt.getAltProteinSet().forEach(pid -> builder.add(pid));
                                }
                                return builder.add(mpProt.getId()).build();
                            })
                            .collect(Collectors.joining(";"))
                    ) + System.lineSeparator()
            );
        }
        bw.close();
    }
}
