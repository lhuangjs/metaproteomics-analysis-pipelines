package cn.phoenixcenter.metaproteomics.pipeline.inference;

import cn.phoenixcenter.metaproteomics.base.ICommand;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProteinGroup;
import cn.phoenixcenter.metaproteomics.pipeline.handler.ProteinGroupHandler;
import cn.phoenixcenter.metaproteomics.pipeline.handler.ProteinHelper;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;

/**
 *
 */
@Log4j2
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class ProteinGroupCmd implements ICommand {

    private final JsonFactory jsonFactory = new ObjectMapper().getFactory();

    private String proteinJsonFile;

    private double protFDRThreshold = 0.01;

    private List<MPProteinGroup> mpProteinGroupList;

    private String proteinGroupTSVFile;

    @Override
    public String getDesc() {
        return "protein group";
    }

    @Override
    public void execute() throws Exception {
        parseProteinJsonFile();
    }

    private void parseProteinJsonFile() throws IOException {
        Set<MPProtein> mpProteinSet = ProteinHelper.parseJsonMPProteins(proteinJsonFile, protFDRThreshold);
//        mpProteinGroupList = ProteinGroupHandler.groupProteins(mpProteinSet);
        ProteinGroupHandler.writeTSVProteinGroup(mpProteinGroupList, Paths.get(proteinGroupTSVFile));
    }
}
