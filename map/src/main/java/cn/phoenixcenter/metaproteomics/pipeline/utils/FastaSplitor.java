package cn.phoenixcenter.metaproteomics.pipeline.utils;

import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Log4j2
public class FastaSplitor {

    /**
     * @param fastaFile
     * @param sizePerFile unit is MB
     */
    public static void split(String fastaFile, int sizePerFile, String outDir) throws IOException {
        // obtain total shard count
        Path fastaPath = Paths.get(fastaFile);
        int shardCount = (int) Math.ceil(Files.size(fastaPath) / 1024.0 / 1024.0 / sizePerFile);
        log.debug("the number of shards: {}", shardCount);
        BufferedReader br = Files.newBufferedReader(Paths.get(fastaFile), StandardCharsets.UTF_8);
        long seqCount = 0;
        // obtain sequence count
        String line;
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">")) {
                seqCount++;
            }
        }
        log.debug("the number of sequence: {}", seqCount);
        long seqCountPerFile = seqCount % shardCount == 0 ? seqCount / shardCount : seqCount / shardCount + 1;
        log.debug("the number of sequence for a shard: {}", seqCountPerFile);

        int shardNum = 0;
        long seqNum = 0;
        br = Files.newBufferedReader(Paths.get(fastaFile), StandardCharsets.UTF_8);
        BufferedWriter bw = null;
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">")) {
                if (seqNum != 0) {
                    bw.write(System.lineSeparator());
                }
                if (seqNum % seqCountPerFile == 0) {
                    // write a new shard file
                    if (bw != null) bw.close();
                    log.debug("start to write shard NO.{}", shardNum);
                    bw = Files.newBufferedWriter(Paths.get(outDir,
                            shardNum + "_" + fastaPath.getFileName().toString()));
                    shardNum++;
                }
                bw.write(line + System.lineSeparator());
                seqNum++;
            } else {
                bw.write(line);
            }
        }
        br.close();
        bw.close();
    }
}
