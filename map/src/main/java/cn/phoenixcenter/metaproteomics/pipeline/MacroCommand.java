package cn.phoenixcenter.metaproteomics.pipeline;

import cn.phoenixcenter.metaproteomics.base.ICommand;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class MacroCommand implements ICommand {

    private static final long serialVersionUID = 1L;

    @NonNull
    private List<ICommand> commandList;

    public void addCommand(ICommand ... commands) {
        for(ICommand command : commands){
            addCommand(command);
        }
    }

    public void addCommand(ICommand command) {
        if (commandList == null) {
            commandList = new ArrayList<>();
        }
        commandList.add(command);
    }

    @Override
    public String getDesc() {
        StringJoiner joiner = new StringJoiner(System.lineSeparator());
        for (ICommand cmd : commandList) {
            joiner.add(cmd.getDesc());
        }
        return joiner.toString();
    }

    @Override
    public void execute() throws Exception {
        for (ICommand cmd : commandList) {
            cmd.execute();
        }
    }
}
