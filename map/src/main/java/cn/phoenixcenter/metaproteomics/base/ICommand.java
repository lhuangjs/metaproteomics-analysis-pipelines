package cn.phoenixcenter.metaproteomics.base;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.Serializable;

public interface ICommand extends Serializable {

    static ICommand toObj(ObjectInput in) throws IOException, ClassNotFoundException {
        return (ICommand) in.readObject();
    }

    /**
     * Return the description of the command
     *
     * @return
     */
    String getDesc();

    void execute() throws Exception;
}
