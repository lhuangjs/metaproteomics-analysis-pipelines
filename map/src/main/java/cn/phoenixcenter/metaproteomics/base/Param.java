package cn.phoenixcenter.metaproteomics.base;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Param {

    // true: the param belongs to tide-index or tide-search param
    boolean nativeParam() default true;

    String paramName();

    String paramAlias() default "";

    String paramDesc();

//    // candidate values with String type
//    String[] stringCand() default {};
//
//    // candidate values with boolean type
//    boolean[] booleanCand() default {};

    String[] candidates() default {};

    boolean booleanType() default false;
}
