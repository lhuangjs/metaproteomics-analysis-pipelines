package cn.phoenixcenter.metaproteomics.dental.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

public class TSVToXLSX {

    private static Logger logger = LogManager.getLogger(TSVToXLSX.class);

    public static void parse(String inputFile, String outputFile) throws IOException {
        // create workbook
        SXSSFWorkbook workbook = new SXSSFWorkbook(100);
        // create sheet
        SXSSFSheet sheet = workbook.createSheet();
        // read and write
        BufferedReader br = new BufferedReader(new FileReader(inputFile));
        String line;
        int i = 0;
        for (; (line = br.readLine()) != null; i++) {
            String[] tmp = line.split("\t");
            Row row = sheet.createRow(i);
            for (int j = 0; j < tmp.length; j++) {
                Cell cell = row.createCell(j);
                cell.setCellValue(tmp[j]);
            }
            if (i % 1000 == 0) {
                logger.info("{} lines have been processed ... ", (i + 1));
            }
        }
        br.close();
        logger.info("all lines({}) have been processed ", (i + 1));
        FileOutputStream fos = new FileOutputStream(outputFile);
        workbook.write(fos);
        fos.close();
        workbook.close();
    }
}
