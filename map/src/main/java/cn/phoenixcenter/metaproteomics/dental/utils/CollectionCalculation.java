package cn.phoenixcenter.metaproteomics.dental.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class CollectionCalculation {
    /**
     * set 1 - set 2
     * @param inputFile1
     * @param inputFile2
     * @param ignoreEmptyLine
     * @return
     * @throws IOException
     */
    public static List<String> calDifferenceSet(String inputFile1, String inputFile2, boolean ignoreEmptyLine) throws IOException {
        List<String> list1 = loadData(inputFile1, ignoreEmptyLine);
        List<String> list2 = loadData(inputFile2, ignoreEmptyLine);

        return list1.parallelStream()
                .filter(str -> list2.indexOf(str) == -1)
                .collect(Collectors.toList());
    }

    /**
     * remove the redundant line
     * @param inputFile
     * @param ignoreEmptyLine
     * @return
     * @throws IOException
     */
    public static List<String> removeRedundancy(String inputFile, boolean ignoreEmptyLine) throws IOException {
        return loadData(inputFile, ignoreEmptyLine)
                .parallelStream()
                .distinct()
                .collect(Collectors.toList());
    }

    public static List<String> loadData(String inputFile, boolean ignoreEmptyLine) throws IOException {
        List<String> rs = Files.readAllLines(Paths.get(inputFile));
        if (ignoreEmptyLine) {
            rs = rs.parallelStream()
                    .filter(line -> !line.equals(""))
                    .collect(Collectors.toList());
        }
        return rs;
    }
}
