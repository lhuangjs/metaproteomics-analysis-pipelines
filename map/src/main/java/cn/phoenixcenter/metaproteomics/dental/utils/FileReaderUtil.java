package cn.phoenixcenter.metaproteomics.dental.utils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;


@NoArgsConstructor
public class FileReaderUtil<S> {

    private Map<String, Integer> headerMap;

    @Getter
    @Setter
    private String inputFile;

    @Getter
    @Setter
    private String separator;

    private Logger logger = LogManager.getLogger(FileReaderUtil.class);

    public FileReaderUtil(String inputFile, String separator) {
        this.inputFile = inputFile;
        this.separator = separator;
    }

    public Map<String, Integer> getHeaderMap() {
        return headerMap == null ? extractHeaderMap() : headerMap;
    }

    public Map<String, Integer> extractHeaderMap() {
        headerMap = new HashMap<>();
        try {
            String[] headers = Files.lines(Paths.get(inputFile))
                    .limit(1)
                    .map(header -> header.split(separator))
                    .flatMap((String[] arr) -> Arrays.stream(arr).map(header -> header.trim()))
                    .toArray(String[]::new);
            for (int i = 0; i < headers.length; i++) {
                headerMap.put(headers[i], i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return headerMap;
    }

    public List<S> read(Predicate<String[]> rowFilter,
                        Function<String[], S> rowProcessor) throws IOException {
        List<S> result = new ArrayList<>();

        BufferedReader br = new BufferedReader(new FileReader(inputFile));
        String line;
        int lineNum = 1;
        br.readLine();

        while ((line = br.readLine()) != null) {
            lineNum++;
            String[] tmp = line.split(separator);
            if (tmp.length == 0) {
                logger.warn("line {} is empty!", lineNum);
            }
            if (rowFilter.test(tmp)) {
                S s = rowProcessor.apply(tmp);
                if (s != null) {
                    result.add(s);
                } else {
                    logger.warn("line {} did not capture anything!", lineNum);
                }
            }
        }
        br.close();
        return result;
    }

    public List<S> read(Function<String[], S> rowProcessor) throws IOException {
        List<S> result = new ArrayList<>();

        BufferedReader br = new BufferedReader(new FileReader(inputFile));
        String line;
        int lineNum = 1;
        br.readLine();

        while ((line = br.readLine()) != null) {
            lineNum++;
            String[] tmp = line.split(separator);
            if (tmp.length == 0) {
                logger.warn("line {} is empty!", lineNum);
            }

            S s = rowProcessor.apply(tmp);
            if (s != null) {
                result.add(s);
            } else {
                logger.warn("line {} did not capture anything!", lineNum);
            }
        }
        br.close();
        return result;
    }
}
