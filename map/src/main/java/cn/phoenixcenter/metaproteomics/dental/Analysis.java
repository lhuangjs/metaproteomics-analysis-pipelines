package cn.phoenixcenter.metaproteomics.dental;

import cn.phoenixcenter.metaproteomics.dental.utils.FileReaderUtil;
import com.google.common.math.DoubleMath;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.inference.TTest;
import org.apache.commons.math3.stat.inference.TestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Analysis {
    private FileReaderUtil fileReaderUtil = new FileReaderUtil();

    private Logger logger = LogManager.getLogger(Analysis.class);

    public Map<String, double[]> bacterialDistribution(String proteinGroupsFile) throws IOException {

        /** read nodeData **/
        List<String[]> entryList = loadProteinGroupData(proteinGroupsFile);

        /** taxon => samples LFQ intensity arr **/
        Map<String, double[]> taxonIntensityMap = new HashMap();
        for (String[] entry : entryList) {
            double[] intensityArr = new double[entry.length - 1];
            for (int i = 1; i < entry.length; i++) {
                if (entry[i].length() == 0) {
                    intensityArr[i - 1] = 0;
                } else {
                    intensityArr[i - 1] = Double.parseDouble(entry[i]);
                }
            }
            // sum up the LFQ intensity values belong to the same taxon
            if (taxonIntensityMap.containsKey(entry[0])) {
                taxonIntensityMap.replace(entry[0], matrixPlus(taxonIntensityMap.get(entry[0]), intensityArr));
            } else {
                taxonIntensityMap.put(entry[0], intensityArr);
            }
        }

        /** calculate taxon distribution **/
        double[] sum = taxonIntensityMap.values().stream()
                .reduce((double[] matrix1, double[] matrix2) -> matrixPlus(matrix1, matrix2))
                .get();
        for (String taxon : taxonIntensityMap.keySet()) {
            // taxon intensity for each sample ./ total intensity for each sample * 100
            taxonIntensityMap.replace(taxon, numberMultiply(100.0,
                    dotDivision(taxonIntensityMap.get(taxon), sum)));
        }
        return taxonIntensityMap;
    }

    public List<String[]> loadProteinGroupData(String proteinGroupsFile) throws IOException {
        /** read nodeData **/
        FileReaderUtil<String[]> fileReaderUtil = new FileReaderUtil<>(proteinGroupsFile, "\t");
        Map<String, Integer> headerMap = fileReaderUtil.getHeaderMap();
//        String[] samples = new String[]{
//                "LFQ intensity Tjaerby9",
//                "LFQ intensity Tjaerby15",
//                "LFQ intensity Tjaerby13",
//                "LFQ intensity Tjaerby2",
//                "LFQ intensity Tjaerby7",
//                "LFQ intensity Tjaerby4",
//                "LFQ intensity Tjaerby1",
//                "LFQ intensity Tjaerby24",
//                "LFQ intensity Tjaerby19",
//                "LFQ intensity Tjaerby20",
//                "LFQ intensity Tjaerby10",
//                "LFQ intensity Tjaerby2",
//                "LFQ intensity Tjaerby14",
//                "LFQ intensity Tjaerby16",
//                "LFQ intensity Tjaerby12",
//                "LFQ intensity Tjaerby11",
//                "LFQ intensity Tjaerby22",
//                "LFQ intensity Tjaerby21",
//                "LFQ intensity Tjaerby23",
//                "LFQ intensity Tjaerby6",
//                "LFQ intensity Tjaerby5",
//                "LFQ intensity Tjaerby18",
//
//                "LFQ intensity Plaque5",
//                "LFQ intensity Plaque6",
//                "LFQ intensity Plaque3",
//                "LFQ intensity Plaque7",
//                "LFQ intensity Plaque2",
//                "LFQ intensity Plaque4",
//                "LFQ intensity Plaque1",
//
//                "LFQ intensity Calc5",
//                "LFQ intensity Calc2",
//                "LFQ intensity Calc7",
//                "LFQ intensity Calc4",
//                "LFQ intensity Calc6",
//                "LFQ intensity Calc1"
//        };

        String[] samples = new String[]{
                "LFQ intensity Calc1",
                "LFQ intensity Calc2",
                "LFQ intensity Calc3",
                "LFQ intensity Calc4",
                "LFQ intensity Calc5",
                "LFQ intensity Calc6",
                "LFQ intensity Calc7",
                "LFQ intensity Plaque1",
                "LFQ intensity Plaque2",
                "LFQ intensity Plaque3",
                "LFQ intensity Plaque4",
                "LFQ intensity Plaque5",
                "LFQ intensity Plaque6",
                "LFQ intensity Plaque7",
                "LFQ intensity Tjaerby1",
                "LFQ intensity Tjaerby10",
                "LFQ intensity Tjaerby11",
                "LFQ intensity Tjaerby12",
                "LFQ intensity Tjaerby13",
                "LFQ intensity Tjaerby14",
                "LFQ intensity Tjaerby15",
                "LFQ intensity Tjaerby16",
                "LFQ intensity Tjaerby17",
                "LFQ intensity Tjaerby18",
                "LFQ intensity Tjaerby19",
                "LFQ intensity Tjaerby2",
                "LFQ intensity Tjaerby20",
                "LFQ intensity Tjaerby21",
                "LFQ intensity Tjaerby22",
                "LFQ intensity Tjaerby23",
                "LFQ intensity Tjaerby24",
                "LFQ intensity Tjaerby25",
                "LFQ intensity Tjaerby3",
                "LFQ intensity Tjaerby4",
                "LFQ intensity Tjaerby5",
                "LFQ intensity Tjaerby6",
                "LFQ intensity Tjaerby7",
                "LFQ intensity Tjaerby8",
                "LFQ intensity Tjaerby9"
        };
        // the genera ignored
        List<String> ignoredGenusList = Arrays.asList(
                "Anaerolinea",
                "Atopobium",
                "Brevibacillus",
                "Cupriavidus",
                "Dichelobacter",
                "Flexistipes",
                "Janthinobacterium",
                "Micrococcus",
                "Mogibacterium",
                "Moraxella",
                "Parabacteroides",
                "Rhodoferax",
                "Shuttleworthia",
                "Xanthomonas"
        ).stream().collect(Collectors.toList());
        Predicate<String[]> predicate = (String[] tmp) -> tmp.length != 0
                && !tmp[headerMap.get("Reverse")].trim().equals("+") // remove reverse hit(line 340)
                && !tmp[headerMap.get("Potential contaminant")].trim().equals("+") // remove contaminant(line 341)
                && tmp[headerMap.get("superkingdom")].trim().equals("2") // only remain bacteria
                && !tmp[headerMap.get("TaxName_genus")].trim().equals("root")
                && ignoredGenusList.indexOf(tmp[headerMap.get("TaxName_genus")].trim()) == -1
                && tmp[headerMap.get("Rank_LCA")].trim().matches("genus|no rank|species|subspecies"); // remain entry on genus(line 359)
        Function<String[], String[]> rowProcessor = (String[] tmp) -> {
            String[] lineRs = new String[samples.length + 1];
            lineRs[0] = tmp[headerMap.get("TaxName_genus")];
            IntStream.range(0, samples.length)
                    .forEach((int index) -> lineRs[index + 1] = tmp[headerMap.get(samples[index])]);
            return lineRs;
        };
        List<String[]> entryList = fileReaderUtil.read(predicate, rowProcessor);
        return entryList;
    }

    /**
     * matrix1 + matrix2
     *
     * @param matrix1
     * @param matrix2
     * @return
     */
    public static double[] matrixPlus(double[] matrix1, double[] matrix2) {
        if (matrix1.length != matrix2.length) {
            throw new RuntimeException("matrix1's length must equate matrix2's, but "
                    + matrix1.length + " != " + matrix2.length);
        }
        double[] result = new double[matrix1.length];
        for (int i = 0; i < matrix1.length; i++) {
            result[i] = matrix1[i] + matrix2[i];
        }
        return result;
    }

    /**
     * matrix1 - matrix2
     *
     * @param matrix1
     * @param matrix2
     * @return
     */
    public static double[] matrixSubtraction(double[] matrix1, double[] matrix2) {
        if (matrix1.length != matrix2.length) {
            throw new RuntimeException("matrix1's length must equate matrix2's, but "
                    + matrix1.length + " != " + matrix2.length);
        }
        double[] result = new double[matrix1.length];
        for (int i = 0; i < matrix1.length; i++) {
            result[i] = matrix1[i] - matrix2[i];
        }
        return result;
    }

    /**
     * matrix1 ./ matrix2
     *
     * @param matrix1
     * @param matrix2
     * @return
     */
    public static double[] dotDivision(double[] matrix1, double[] matrix2) {
        if (matrix1.length != matrix2.length) {
            throw new RuntimeException("matrix1's length must equate matrix2's, but "
                    + matrix1.length + " != " + matrix2.length);
        }
        double[] result = new double[matrix1.length];
        for (int i = 0; i < matrix1.length; i++) {
            result[i] = matrix1[i] / matrix2[i];
        }
        return result;
    }

    /**
     * num * matrix
     *
     * @param num
     * @param matrix
     * @return
     */
    public static double[] numberMultiply(double num, double[] matrix) {
        double[] result = new double[matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            result[i] = num * matrix[i];
        }
        return result;
    }

    public List<AbundanceData> calAbundanceDifference(String abundanceFile) throws IOException {
        String[] g1 = new String[]{"Tjaerby9", "Tjaerby15", "Tjaerby13", "Tjaerby2", "Tjaerby7", "Tjaerby4",
                "Tjaerby1", "Tjaerby24", "Tjaerby19", "Tjaerby20", "Tjaerby10", "Tjaerby3", "Tjaerby14",
                "Tjaerby16", "Tjaerby12", "Tjaerby11"};
//        String[] g1 = new String[]{"Tjaerby9", "Tjaerby15", "Tjaerby13", "Tjaerby2", "Tjaerby7", "Tjaerby4",
//                "Tjaerby1", "Tjaerby24", "Tjaerby19", "Tjaerby20", "Tjaerby10", "Tjaerby3", "Tjaerby14",
//                "Tjaerby16", "Tjaerby12", "Tjaerby11"
//                , "Tjaerby25" };
        String[] g2 = new String[]{"Tjaerby22", "Tjaerby21", "Tjaerby23", "Tjaerby6", "Tjaerby5", "Tjaerby18"};
//        String[] g2 = new String[]{"Tjaerby22", "Tjaerby21", "Tjaerby23", "Tjaerby6", "Tjaerby5", "Tjaerby18"
//                , "Tjaerby17" , "Tjaerby8" };

        /** read taxon abundance file **/
        fileReaderUtil.setSeparator("\t");
        fileReaderUtil.setInputFile(abundanceFile);
        Map<String, Integer> headerMap = fileReaderUtil.getHeaderMap();
        Function<String[], AbundanceData> rowProcessor = (String[] tmp) -> {
            AbundanceData data = new AbundanceData();
            data.setTaxonName(tmp[0]);
            double[] abundanceArr1 = Arrays.stream(g1).map((String element) -> tmp[headerMap.get("Abundance " + element)])
                    .mapToDouble(Double::parseDouble)
                    .toArray();
            double[] abundanceArr2 = Arrays.stream(g2).map((String element) -> tmp[headerMap.get("Abundance " + element)])
                    .mapToDouble(Double::parseDouble)
                    .toArray();
            data.setAbundanceArr1(abundanceArr1);
            data.setAbundanceArr2(abundanceArr2);
            return data;
        };
        List<AbundanceData> abundanceDataList = fileReaderUtil.read(rowProcessor);

        /** remove abundance nodeData that all abundance values are zero **/
        abundanceDataList = abundanceDataList.parallelStream()
                .filter((AbundanceData data) -> {
                    boolean shouldRemoved = isAllZero(data.getAbundanceArr1()) && isAllZero(data.getAbundanceArr2());
                    if (shouldRemoved) {
                        logger.info("All abundance values are zero in taxon **{}**, so it has been removed", data.getTaxonName());
                    }
                    return !shouldRemoved;
                })
                .collect(Collectors.toList());

        /** difference analysis **/
        for (AbundanceData data : abundanceDataList) {
            double[] arr1 = Arrays.stream(data.getAbundanceArr1())
                    .map(d -> d != 0.0 ? d : new DescriptiveStatistics(data.getAbundanceArr1()).getMean())
                    .toArray();
            double[] arr2 = Arrays.stream(data.getAbundanceArr2())
                    .map(d -> d != 0.0 ? d : new DescriptiveStatistics(data.getAbundanceArr2()).getMean())
                    .toArray();
            System.out.println("arr1=>" + Arrays.stream(arr1).mapToObj(String::valueOf).collect(Collectors.joining(", ")));
            System.out.println("arr1=>" + Arrays.stream(arr2).mapToObj(String::valueOf).collect(Collectors.joining(", ")) + "\n");


            // calculate p-value
            data.setPValue(TestUtils.homoscedasticTTest(log2(arr1),
                    log2(arr2)));
//            System.out.println(Arrays.stream(nodeData.abundanceArr1).mapToObj(String::valueOf).collect(Collectors.joining(", ")));
//            System.out.println(Arrays.stream(nodeData.abundanceArr2).mapToObj(String::valueOf).collect(Collectors.joining(", ")));
            // calculate -log(p-value)
            data.setNegLog10PValue(-Math.log10(data.getPValue()));
            // calculate fold change
            data.setFc(calFoldChange(log2(arr1),
                    log2(arr2)));
            // calculate log2(fold change)
            data.setLog2FC(DoubleMath.log2(data.getFc()));
        }
        return abundanceDataList;
    }

    public static double calFoldChange(double[] group1, double[] group2) {
        double mean1 = new DescriptiveStatistics(group1).getMean();
        double mean2 = new DescriptiveStatistics(group2).getMean();
//        return (mean1 - mean2) / Double.min(mean1, mean2);
        return mean1 - mean2;
//        return Math.abs(mean1 - mean2) / mean2;
//        group1 = Arrays.stream(group1)
//                .sorted()
//                .toArray();
//        group2 = Arrays.stream(group2)
//                .sorted()
//                .toArray();
//        double median1 = group1.length % 2 == 0
//                ? (group1[group1.length / 2 - 1] + group1[group1.length / 2]) / 2
//                : group1[group1.length / 2];
//        double median2 = group2.length % 2 == 0
//                ? (group2[group2.length / 2 - 1] + group2[group2.length / 2]) / 2
//                : group2[group2.length / 2];
//        return median1 - median2;
    }

    /**
     * Bray–Curtis dissimilarity
     */
    public static double calBrayCurtisDissimilarity(double[] group1, double[] group2) {
        double d1 = Arrays.stream(matrixSubtraction(group1, group2))
                .map(Math::abs)
                .sum();
        double d2 = Arrays.stream(matrixPlus(group1, group2))
                .sum();
        return d1 / d2;
    }

    private boolean isAllZero(double[] array) {
        for (double d : array) {
            if (d != 0.0) {
                return false;
            }
        }
        return true;
    }

    private static double[] log2(double[] data) {
        return Arrays.stream(data)
                .map(DoubleMath::log2)
                .toArray();
    }

    /**
     * TODO 后期可以删除
     * 读取all species file中的species，检索其对应的genus
     *
     * @param speciesFile
     * @throws IOException
     */
    public void valid(String speciesFile) throws IOException {
        /** read file **/
        final String[] extractedCol = new String[]{
                "TaxName",
                "Abundance Calc1",
                "Abundance Calc2",
                "Abundance Calc3",
                "Abundance Calc4",
                "Abundance Calc5",
                "Abundance Calc6",
                "Abundance Calc7",
                "Abundance Plaque1",
                "Abundance Plaque2",
                "Abundance Plaque3",
                "Abundance Plaque4",
                "Abundance Plaque5",
                "Abundance Plaque6",
                "Abundance Plaque7",
                "Abundance Tjaerby1",
                "Abundance Tjaerby10",
                "Abundance Tjaerby11",
                "Abundance Tjaerby12",
                "Abundance Tjaerby13",
                "Abundance Tjaerby14",
                "Abundance Tjaerby15",
                "Abundance Tjaerby16",
                "Abundance Tjaerby17",
                "Abundance Tjaerby18",
                "Abundance Tjaerby19",
                "Abundance Tjaerby2",
                "Abundance Tjaerby20",
                "Abundance Tjaerby21",
                "Abundance Tjaerby22",
                "Abundance Tjaerby23",
                "Abundance Tjaerby24",
                "Abundance Tjaerby25",
                "Abundance Tjaerby3",
                "Abundance Tjaerby4",
                "Abundance Tjaerby5",
                "Abundance Tjaerby6",
                "Abundance Tjaerby7",
                "Abundance Tjaerby8",
                "Abundance Tjaerby9"
        };

        fileReaderUtil.setInputFile(speciesFile);
        fileReaderUtil.setSeparator("\t");
        Map<String, Integer> headerMap = fileReaderUtil.getHeaderMap();
        Function<String[], List<String>> rowProcessor = (String[] tmp) ->
                Arrays.stream(extractedCol)
                        .map((String col) -> tmp[headerMap.get(col)])
                        .collect(Collectors.toList());
        List<List<String>> entryList = fileReaderUtil.read(rowProcessor);
        List<String> speciesList = entryList.stream()
                .map(entry -> entry.get(0))
                .collect(Collectors.toList());

        /** obtain genus **/
        fileReaderUtil.setInputFile(Analysis.class.getResource("/taxonomy/lineages.csv").getPath());
        fileReaderUtil.setSeparator(",");
        Map<String, Integer> lineagesHeaderMap = fileReaderUtil.getHeaderMap();
        Predicate<String[]> lineageRowFilter = (String[] tmp) -> speciesList.contains(tmp[1]);
        Function<String[], String[]> lineageRowProcessor = (String[] tmp) ->
                new String[]{tmp[lineagesHeaderMap.get("genus_name")], tmp[lineagesHeaderMap.get("species_name")]};
        List<String[]> lineageList = fileReaderUtil.read(lineageRowFilter, lineageRowProcessor);


    }

}

@Setter
@Getter
@NoArgsConstructor
@ToString
class AbundanceData {
    String taxonName;
    double[] abundanceArr1;
    double[] abundanceArr2;
    double pValue;
    double negLog10PValue;
    double fc;
    double log2FC;
}
