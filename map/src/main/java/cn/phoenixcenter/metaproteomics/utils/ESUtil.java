package cn.phoenixcenter.metaproteomics.utils;

import cn.phoenixcenter.metaproteomics.utils.model.ESIndex;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.action.ActionResponse;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

@Log4j2
public class ESUtil {

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static TransportClient createESClient(String esName,
                                                 String esHosts,
                                                 String esPorts) throws IOException {
        // parse params
        String[] esHostArr = Arrays.stream(esHosts.split(",")).toArray(String[]::new);
        int[] esPortArr = Arrays.stream(esPorts.split(",")).mapToInt(Integer::parseInt).toArray();
        // create es client
        Settings settings = Settings.builder()
                .put("cluster.name", esName)
                .build();
        TransportClient esClient = new PreBuiltTransportClient(settings);
        for (int i = 0; i < esHostArr.length; i++) {
            esClient.addTransportAddress(
                    new TransportAddress(InetAddress.getByName(esHostArr[i]), esPortArr[i])
            );
        }
        return esClient;
    }

    /**
     * obtain a default BulkRequest Listener
     *
     * @return
     */
    public static BulkProcessor.Listener getDefaultBulkProcessorListener() {
        return new BulkProcessor.Listener() {
            @Override
            public void beforeBulk(long l, BulkRequest bulkRequest) {
            }

            @Override
            public void afterBulk(long l, BulkRequest bulkRequest, BulkResponse bulkResponse) {
                if (bulkResponse.hasFailures()) {
                    for (BulkItemResponse bulkItemResponse : bulkResponse) {
                        BulkItemResponse.Failure failure = bulkItemResponse.getFailure();
                        log.error(failure.toString());
                    }
                } else {
                    log.debug("after bulk: the {} time" +
                                    " - total actions[{}]" +
                                    " - success actions[{}]",
                            l, bulkRequest.numberOfActions(), bulkResponse.getItems().length);

                }
            }

            @Override
            public void afterBulk(long l, BulkRequest bulkRequest, Throwable throwable) {
                log.error(throwable.toString());
            }
        };
    }

    /**
     * Parse ES configuration from json file
     *
     * @param indexName
     * @param indexConfigStream
     * @return
     * @throws IOException
     */
    public static ESIndex parseJsonIndex(String indexName, InputStream indexConfigStream) throws IOException {
        JsonNode jsonNode = objectMapper.readTree(indexConfigStream);
        ESIndex esIndex = new ESIndex();
        esIndex.setIndexName(indexName);
        esIndex.setSettings(jsonNode.get("settings").toString());
        esIndex.setPrimaryShardsNum(jsonNode.get("settings").get("number_of_shards").intValue());
        esIndex.setMappings(jsonNode.get("mappings").toString());
        esIndex.setTypeName(jsonNode.get("mappings").fieldNames().next());
        return esIndex;
    }

    /**
     * Create an index from json file
     *
     * @param esClient
     * @param indexName
     * @param indexConfigStream
     * @param deleteIfExist
     * @return
     * @throws IOException
     */
    public static ESIndex createIndex(TransportClient esClient, String indexName, InputStream indexConfigStream, boolean deleteIfExist) throws IOException {
        // parse json file
        ESIndex esIndex = ESUtil.parseJsonIndex(indexName, indexConfigStream);
        // create
        return createIndex(esClient, esIndex, deleteIfExist);
    }

    public static ESIndex createIndex(TransportClient esClient, ESIndex esIndex, boolean deleteIfExist) throws IOException {
        // create
        IndicesAdminClient indicesAdminClient = esClient.admin().indices();
        if (indicesAdminClient.prepareExists(esIndex.getIndexName()).get().isExists()) {
            if (deleteIfExist) {
                indicesAdminClient.prepareDelete(esIndex.getIndexName()).get();
                log.debug("delete existing ES index <{}>", esIndex.getIndexName());
            } else {
                throw new RuntimeException("Index <" + esIndex.getIndexName() + "> already exists");
            }
        }
        indicesAdminClient.prepareCreate(esIndex.getIndexName())
                .setSettings(esIndex.getSettings(), XContentType.JSON)
                .addMapping(esIndex.getTypeName(), esIndex.getMappings(), XContentType.JSON)
                .get();
        log.debug("create new ES index <{}>", esIndex.getIndexName());
        return esIndex;
    }

    public static ActionResponse deleteIndex(TransportClient esClient, String... indexNames) {
        return esClient.admin().indices().prepareDelete(indexNames).get();
    }

    public static void esBulkProcessorTest(TransportClient esClient,
                                           InputStream indexConfigStream,
                                           String jsonObject,
                                           int[] bulkActionsArr,
                                           ByteSizeValue bulkSize,
                                           int counts,
                                           int warmupCounts,
                                           int baseline) throws IOException, InterruptedException {
        warmupCounts = 0 - warmupCounts;
        long startTime, endTime;
        double timeCost[] = new double[bulkActionsArr.length];
        for (int j = 0; j < bulkActionsArr.length; j++) {
            int bulkActions = bulkActionsArr[j];
            // create temporary index
            ESIndex index = createIndex(esClient, String.valueOf(j), indexConfigStream, false);
            BulkProcessor bulkProcessor = BulkProcessor.builder(esClient, getDefaultBulkProcessorListener())
                    .setBulkActions(bulkActions)
                    .setBulkSize(bulkSize)
                    .build();
            int total = bulkActions * counts;
            // process
            startTime = System.currentTimeMillis();
            for (int i = 0; i < total; i++) {
                bulkProcessor.add(
                        new IndexRequest(index.getIndexName(), index.getTypeName())
                                .source(jsonObject, XContentType.JSON)
                );
            }
            bulkProcessor.flush();
            bulkProcessor.awaitClose(10, TimeUnit.MINUTES);
            esClient.admin().indices().prepareRefresh(index.getIndexName()).get();
            endTime = System.currentTimeMillis();
            timeCost[j] = (double) (endTime - startTime) / total / 1000.0 * baseline;
            System.err.println(timeCost[j]);
            // delete temporary index
            deleteIndex(esClient, index.getIndexName());
            if (warmupCounts++ < 0) {
                j = -1;
            }
        }

        System.out.println();
        for (int j = 0; j < bulkActionsArr.length; j++) {
            System.out.println(bulkActionsArr[j] + ": " + timeCost[j] + " s / " + baseline);
        }
    }
}
