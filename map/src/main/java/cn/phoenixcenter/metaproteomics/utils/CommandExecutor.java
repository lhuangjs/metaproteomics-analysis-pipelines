package cn.phoenixcenter.metaproteomics.utils;

import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.function.Consumer;

/**
 * Created by huangjs
 */
@Log4j2
public class CommandExecutor {

    /**
     * Execute system commands.
     * <p>
     * Note: do not merge error and output, we need obtain output not error to do some process sometimes.
     *
     * @param command
     * @param outConsumer the output of command
     * @param errConsumer the error output of command
     */
    public static void exec(String command,
                            Consumer<InputStream> outConsumer,
                            Consumer<InputStream> errConsumer) {
        log.info("command>>{}", command);
        try {
            final Process process = Runtime.getRuntime().exec(command);
            Runnable stdOut = () -> {
                if (outConsumer != null) {
                    outConsumer.accept(process.getInputStream());
                } else {
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                        String line;
                        while ((line = br.readLine()) != null) {
                            log.debug(line);
                        }
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            };

            Runnable stdErr = () -> {
                if (errConsumer != null) {
                    errConsumer.accept(process.getErrorStream());
                } else {
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(process.getErrorStream()))) {
                        String line;
                        while ((line = br.readLine()) != null) {
                            log.debug(line);
                        }
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            };
            new Thread(stdOut).start();
            new Thread(stdErr).start();
            if(process.waitFor() != 0){
                throw new RuntimeException("command <" + command+ "> cannot work");
            }
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @param command
     * @param outConsumer
     * @see #exec(String, Consumer, Consumer)
     */
    public static void exec(String command, Consumer<InputStream> outConsumer) {
        exec(command, outConsumer, null);
    }

    /**
     * @param command
     * @see #exec(String, Consumer, Consumer)
     */
    public static void exec(String command) {
        exec(command, null, null);
    }
}
