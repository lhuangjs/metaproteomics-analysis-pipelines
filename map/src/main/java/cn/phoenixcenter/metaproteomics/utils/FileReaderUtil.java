package cn.phoenixcenter.metaproteomics.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipInputStream;

public class FileReaderUtil {

    private String inputFile;

    private String separator;

    private boolean hasHeader;

    private List<String> headers;

    private Format format;

    public enum Format {
        GENERAL,
        ZIP,
        GZIP
    }

    private Logger logger = LogManager.getLogger(FileReaderUtil.class);

    public FileReaderUtil(String inputFile, String separator, boolean hasHeader, Format format) {
        this.inputFile = inputFile;
        this.separator = separator;
        this.hasHeader = hasHeader;
        this.format = format;
    }

    public FileReaderUtil(String inputFile, String separator, boolean hasHeader) {
        this.inputFile = inputFile;
        this.separator = separator;
        this.hasHeader = hasHeader;
        this.format = Format.GENERAL;
    }

    public FileReaderUtil(String inputFile, String separator) {
        this(inputFile, separator, false);
    }

    private BufferedReader getReader() throws IOException {
        BufferedReader br = null;
        switch (format) {
            case GENERAL:
                br = new BufferedReader(new FileReader(inputFile));
                break;
            case ZIP:
                br = new BufferedReader(new InputStreamReader(new ZipInputStream(new FileInputStream(inputFile))));
                break;
            case GZIP:
                br = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(inputFile))));
                break;
        }
        return br;
    }

    /**
     * Extract headers by the specific separator
     *
     * @return
     */
    public List<String> extractHeaders() {
        if (!hasHeader) {
            throw new UnsupportedOperationException("The file don't have header! Attribute[hasHeader=false]");
        }
        List<String> headers = null;

        try (BufferedReader br = getReader()) { // try-with-resources
            headers = Arrays.stream(br.readLine().split(separator)).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return headers;
    }

    /**
     * Obtain column index(0-based system) by column name.
     *
     * @param colName
     * @return
     */
    public int getColNum(String colName) {
        if (headers == null) {
            headers = extractHeaders();
        }
        return headers.indexOf(colName);
    }

    /**
     * It can be used if the header line need special process.
     *
     * @param headerProcessor
     * @param <S>
     * @return
     */
    public <S> S extractHeaders(Function<String, S> headerProcessor) throws IOException {
        if (!hasHeader) {
            throw new UnsupportedOperationException("The file don't have header! Attribute[hasHeader=false]");
        }
        BufferedReader br = getReader();
        S headers = headerProcessor.apply(br.readLine());
        br.close();
        return headers;
    }

    /**
     * @param rowDeleter
     * @param rowProcessor
     * @param <S>
     * @return
     * @throws IOException
     */
    public <S> List<S> read(Predicate<String[]> rowDeleter,
                            Function<String[], S> rowProcessor) throws IOException {
        List<S> result = new ArrayList<>();

        BufferedReader br = getReader();
        String line;
        int lineNum = 1;
        if (hasHeader) {
            br.readLine();
            lineNum = 2;
        }

        while ((line = br.readLine()) != null) {
            lineNum++;
            String[] tmp = line.split(separator);
            if (tmp.length == 0) {
                logger.warn("line {} is empty!", lineNum);
            }
            if (rowDeleter.test(tmp)) {
                logger.debug("line {} has been removed!", lineNum);
            } else {
                S s = rowProcessor.apply(tmp);
                if (s != null) {
                    result.add(s);
                } else {
                    logger.warn("line {} did not capture anything!", lineNum);
                }
            }
        }
        br.close();
        return result;
    }

    public <S> List<S> read(Function<String[], S> rowProcessor) throws IOException {
        List<S> result = new ArrayList<>();
        BufferedReader br = getReader();
        String line;
        int lineNum = 1;
        if (hasHeader) {
            br.readLine();
            lineNum = 2;
        }

        while ((line = br.readLine()) != null) {
            lineNum++;
            String[] tmp = line.split(separator);
            if (tmp.length == 0) {
                logger.warn("line {} is empty!", lineNum);
            }

            S s = rowProcessor.apply(tmp);
            if (s != null) {
                result.add(s);
            } else {
                logger.warn("line {} did not capture anything!", lineNum);
            }
        }
        br.close();
        return result;
    }

    /**
     * It can be used when you want to process and immediately use each line.
     *
     * @param rowConsumer
     * @throws IOException
     */
    public void read(Consumer<String> rowConsumer) throws IOException {
        BufferedReader br = getReader();
        String line;
        if (hasHeader) {
            br.readLine();
        }
        while ((line = br.readLine()) != null) {
            rowConsumer.accept(line);
        }
        br.close();
    }

    public void readByColName(Consumer<Map<String, String>> rowConsumer) throws IOException {
        BufferedReader br = getReader();
        String line;
        if (hasHeader) {
            br.readLine();
        }
        while ((line = br.readLine()) != null) {
            String[] tmp = line.split(separator);
            Map<String, String> colName2Val = new HashMap<>(tmp.length);
            for (int i = 0; i < tmp.length; i++) {
                colName2Val.put(headers.get(i), tmp[i]);
            }
            rowConsumer.accept(colName2Val);
        }
        br.close();
    }
}
