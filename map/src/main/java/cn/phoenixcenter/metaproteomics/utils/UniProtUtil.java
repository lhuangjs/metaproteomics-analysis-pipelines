package cn.phoenixcenter.metaproteomics.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import uk.ac.ebi.kraken.interfaces.uniprot.UniProtEntry;
import uk.ac.ebi.uniprot.dataservice.client.Client;
import uk.ac.ebi.uniprot.dataservice.client.QueryResult;
import uk.ac.ebi.uniprot.dataservice.client.exception.ServiceException;
import uk.ac.ebi.uniprot.dataservice.client.uniprot.QuerySpec;
import uk.ac.ebi.uniprot.dataservice.client.uniprot.UniProtQueryBuilder;
import uk.ac.ebi.uniprot.dataservice.client.uniprot.UniProtService;
import uk.ac.ebi.uniprot.dataservice.query.Query;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class UniProtUtil {

    private static final String uniprotUrl = "https://www.uniprot.org/uniprot/";

    private static final Pattern taxonIdPattern = Pattern.compile("OX\\s+NCBI_TaxID=(\\d+)");

    private static final Pattern restTaxonIdPattern = Pattern.compile("\"taxonomy\":(\\d+)");

    private static final UniProtService uniProtService = Client.getServiceFactoryInstance().getUniProtQueryService();

    private static final RestTemplate restTemplate = new RestTemplate();

    private static final HttpHeaders httpHeaders = new HttpHeaders();

    private static Logger logger = LogManager.getLogger(UniProtUtil.class);

    static {
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));
    }

    /**
     * Retrieve taxon id by accession number using UniProt history.
     * If no taxon was found, null will be returned.
     *
     * @param accessionNum
     * @return
     * @throws IOException
     */
    public static Map.Entry<String, Integer> accToTaxonByHistory(String accessionNum) {

        Map.Entry<String, Integer> accTaxonIdEntry = null;

        try {
            // get the newest version number
            String versionUrl = uniprotUrl + accessionNum + "?version=*";
            Document versionDoc = Jsoup.connect(versionUrl).get();
            Element tableBody = versionDoc.getElementById("results").getElementsByTag("tbody").first();
            String version = tableBody.child(1).child(1).child(0).text();

            // get content of the newest version
            String proInfoUrl = uniprotUrl + accessionNum + ".txt" + "?version=" + version;
            Jsoup.connect(proInfoUrl);
            Document proInfoDoc = Jsoup.connect(proInfoUrl).get();
            Element proInfoBody = proInfoDoc.body();

            // parse taxon id
            Matcher m = taxonIdPattern.matcher(proInfoBody.text());
            if (m.find()) {
                accTaxonIdEntry = new AbstractMap.SimpleEntry<>(accessionNum, Integer.parseInt(m.group(1)));
            } else {
                throw new RuntimeException("UniProt History: nothing be captured when accession number is <"
                        + accessionNum + ">");
            }
            // FIXME 断网了会抛出这个异常么？
        } catch (IOException e) {
            logger.info("UniProt History: nothing when accession number is <" + accessionNum + "> ");
        }
        return accTaxonIdEntry;
    }

    public static Map<String, Integer> accToTaxonByHistory(Set<String> accessionSet) {
//        final AtomicInteger count = new AtomicInteger(0);
//        final int partCount = 10; // the number of total parts need to be separated
//        final int perPartSize = accessionSet.size() % partCount == 0
//                ? accessionSet.size() / partCount
//                : accessionSet.size() / partCount + 1; // the number of each part
//
//                    // display progress: not exactly accurate process display
//                    if (count.incrementAndGet() % perPartSize == 0) {
//                        logger.info("{}% ......",
//                                String.valueOf(count.get() / perPartSize / (double) partCount * 100)
//                                        .split("\\.")[0]);
//                    }
//                    if (count.get() == accessionSet.size()) {
//                        logger.info("100% ......");
//                    }

        return accessionSet.parallelStream()
                .map(acc -> accToTaxonByHistory(acc))
                .filter(acc -> acc != null)
                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()));

    }

    public static Map<String, Integer> accToTaxonIdByJAPI(Set<String> accessions)
            throws ServiceException {

        Map<String, Integer> accTaxonMap = new HashMap<>(accessions.size());

        if (!uniProtService.isStarted()) {
            uniProtService.start();
        }

        // retrieve
        Query query = UniProtQueryBuilder.accessions(accessions);
        QueryResult<UniProtEntry> queryResult = uniProtService.getEntries(query,
                EnumSet.of(QuerySpec.WithIsoform));
        while (queryResult.hasNext()) {
            UniProtEntry entry = queryResult.next();
            accTaxonMap.put(entry.getPrimaryUniProtAccession().getValue(),
                    Integer.parseInt(entry.getNcbiTaxonomyIds().get(0).getValue()));
        }
        return accTaxonMap;
    }

    /**
     * Retrieve taxon info by UniProt Rest API.
     * Accession numbers will be ignored if a accession numbers did not match taxon info
     *
     * @param accessions
     * @return
     */
    public static Map<String, Integer> accToTaxonIdByRest(List<String> accessions) {
        return accessions.parallelStream()
                .map(acc -> accToTaxonIdByRest(acc))
                .filter(acc -> acc != null)
                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()));
    }

    /**
     * Retrieve taxon info by UniProt Rest API.
     * null will be returned if a accession numbers did not match taxon info
     *
     * @param acc
     * @return
     */
    public static Map.Entry<String, Integer> accToTaxonIdByRest(String acc) {

        Map.Entry<String, Integer> accTaxonIdEntry = null;

        String url = "https://www.ebi.ac.uk/proteins/api/proteins/" + acc;
        RequestEntity requestEntity = new RequestEntity(httpHeaders, HttpMethod.GET, URI.create(url));
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);

            Matcher matcher = restTaxonIdPattern.matcher(responseEntity.getBody());
            if (matcher.find()) {
                accTaxonIdEntry = new AbstractMap.SimpleEntry(acc, Integer.parseInt(matcher.group(1)));
            } else {
                logger.info("UniProt Rest API: nothing be captured when accession number is <" + acc + ">");
                return null;
            }
        } catch (HttpClientErrorException e) {
            logger.info("UniProt Rest API: nothing when accession number is <" + acc + "> ");
        }
        return accTaxonIdEntry;
    }

    public static Map<String, Integer> batchAccessionToTaxonId(Set<String> accessionSet) throws ServiceException, IOException {
        return batchAccessionToTaxonId(accessionSet, null, "\t");
    }

    /**
     * Map accession number to taxon id by UniPro JAPI, Rest API and history.
     * If there is no taxon id mapping, nothing will be returned.
     * For example, {acc1 => taxon1, acc2 => null}, return value is {acc1 => taxon1}
     *
     * @param accessionSet
     * @param processedAccFile
     * @param separator
     * @return
     * @throws ServiceException
     * @throws IOException
     */
    public static Map<String, Integer> batchAccessionToTaxonId(Set<String> accessionSet,
                                                               String processedAccFile,
                                                               String separator) throws ServiceException, IOException {
        Map<String, Integer> accessionTaxonIdMap = null;

        // read processed accession and save into return vale
        // FIXME consider have header or not
        if (processedAccFile != null) {
            accessionTaxonIdMap = Files.readAllLines(Paths.get(processedAccFile), Charset.forName("UTF-8"))
                    .parallelStream()
                    .map(line -> line.split(separator))
                    .filter(tmp -> tmp.length == 2)
                    .collect(Collectors.toMap(tmp -> tmp[0], tmp -> Integer.parseInt(tmp[1])));
            logger.info("{} accession numbers have been processed ...", accessionTaxonIdMap.size());
            accessionSet.removeAll(accessionTaxonIdMap.keySet());
        } else {
            accessionTaxonIdMap = new HashMap<>(accessionSet.size());
        }

        // create temporary file to save accession-taxon id mapping
        File tmpFile = File.createTempFile("acc-taxon-mapping", ".tsv");
        BufferedWriter bw = new BufferedWriter(new FileWriter(tmpFile));
        logger.info("create temporary file <{}> to save accession-taxon id mapping", tmpFile.getAbsolutePath());

        logger.info("{} accession numbers need to be processed ...", accessionSet.size());
        List<String> accessionList = new ArrayList<>(accessionSet);
        List<String> unprocessedAccessionList = new ArrayList<>(accessionSet);

        // retrieve in uniprot
        logger.info("====retrieve by UniProt JAPI<{}>====", unprocessedAccessionList.size());
        uniProtService.start();
        int batchSize = 100, start = 0, end = 0;
        for (int i = 0; i < accessionList.size() / batchSize; i++) {
            start = batchSize * i;
            end = start + batchSize;
            List<String> batchData = accessionList.subList(start, end).stream()
                    .collect(Collectors.toList());
            Map<String, Integer> result = accToTaxonIdByJAPI(new HashSet<>(batchData)); // UniProt JAPI

            // write result
            for (String key : result.keySet()) {
                bw.write(key + separator + String.valueOf(result.get(key)) + System.lineSeparator());
            }
            bw.flush();

            // remove processed accession
            unprocessedAccessionList = unprocessedAccessionList.parallelStream()
                    .filter(acc -> !result.keySet().contains(acc))
                    .collect(Collectors.toList());
        }
        if (end < accessionList.size()) {
            start = end;
            end = accessionList.size();
            List<String> batchData = accessionList.subList(start, end).stream()
                    .collect(Collectors.toList());
            Map<String, Integer> result = accToTaxonIdByJAPI(new HashSet<>(batchData)); // UniProt JAPI

            // write result
            for (String key : result.keySet()) {
                bw.write(key + separator + String.valueOf(result.get(key)) + System.lineSeparator());
            }
            bw.flush();

            // remove processed accession
            unprocessedAccessionList = unprocessedAccessionList.parallelStream()
                    .filter(acc -> !result.keySet().contains(acc))
                    .collect(Collectors.toList());
        }
        uniProtService.stop();

        logger.info("====retrieve by UniProt Rest API and history<{}>====", unprocessedAccessionList.size());
        final AtomicInteger count = new AtomicInteger(0);
        final int partCount = 10; // the number of total parts need to be separated
        final int perPartSize = unprocessedAccessionList.size() % partCount == 0
                ? accessionSet.size() / partCount
                : accessionSet.size() / partCount + 1; // the number of each part

        // retrieve by Rest API and History
        for (String acc : unprocessedAccessionList) {
            Map.Entry<String, Integer> accTaxonIdEntry = null;
            if ((accTaxonIdEntry = accToTaxonIdByRest(acc)) != null
                    || (accTaxonIdEntry = accToTaxonByHistory(acc)) != null) {

                // write result
                bw.write(accTaxonIdEntry.getKey() + separator
                        + String.valueOf(accTaxonIdEntry.getValue()) + System.lineSeparator());
                bw.flush();
            }
            // display progress: not exactly accurate process display
            if (count.incrementAndGet() % perPartSize == 0) {
                logger.info("{}% ......",
                        String.valueOf(count.get() / perPartSize / (double) partCount * 100)
                                .split("\\.")[0]);
            }
            if (count.get() == accessionSet.size()) {
                logger.info("100% ......");
            }
        }
        bw.close();

        // export result
        List<String> lines = Files.readAllLines(tmpFile.toPath(), Charset.forName("UTF-8"));
        for (String line : lines) {
            String[] tmp = line.split(separator);
            accessionTaxonIdMap.put(tmp[0], Integer.parseInt(tmp[1]));
        }
        tmpFile.deleteOnExit();
        return accessionTaxonIdMap;
    }
}
