package cn.phoenixcenter.metaproteomics.utils.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class ESIndex {

    private String indexName;

    private String typeName;

    private String settings;

    private String mappings;

    private int primaryShardsNum;
}
