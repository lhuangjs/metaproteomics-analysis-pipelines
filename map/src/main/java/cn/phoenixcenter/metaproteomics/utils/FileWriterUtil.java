package cn.phoenixcenter.metaproteomics.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;

public class FileWriterUtil {
    public <T> void write(String outputFile,
                          Function<T, String> rowParser,
                          String headers,
                          List<T> data) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
        if (headers != null) {
            bw.write(headers + System.lineSeparator());
        }
        for (T d : data) {
            bw.write(rowParser.apply(d) + System.lineSeparator());
        }
        bw.close();
    }
}
