package cn.phoenixcenter.metaproteomics.utils;

import com.google.common.math.DoubleMath;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MathUtil {

    /**
     * vector1 + vector2
     *
     * @param vector1
     * @param vector2
     * @return
     */
    public static double[] vectorPlus(double[] vector1, double[] vector2) {
        if (vector1.length != vector2.length) {
            throw new RuntimeException("vector1's length must equate vector2's, but "
                    + vector1.length + " != " + vector2.length);
        }
        double[] result = new double[vector1.length];
        for (int i = 0; i < vector1.length; i++) {
            result[i] = vector1[i] + vector2[i];
        }
        return result;
    }

    /**
     * vector1 - vector2
     *
     * @param vector1
     * @param vector2
     * @return
     */
    public static double[] vectorSubtraction(double[] vector1, double[] vector2) {
        if (vector1.length != vector2.length) {
            throw new RuntimeException("vector1's length must equate vector2's, but "
                    + vector1.length + " != " + vector2.length);
        }
        double[] result = new double[vector1.length];
        for (int i = 0; i < vector1.length; i++) {
            result[i] = vector1[i] - vector2[i];
        }
        return result;
    }

    /**
     * vector1 ./ vector2
     *
     * @param vector1
     * @param vector2
     * @return
     */
    public static double[] dotDivision(double[] vector1, double[] vector2) {
        if (vector1.length != vector2.length) {
            throw new RuntimeException("vector1's length must equate vector2's, but "
                    + vector1.length + " != " + vector2.length);
        }
        double[] result = new double[vector1.length];
        for (int i = 0; i < vector1.length; i++) {
            result[i] = vector1[i] / vector2[i];
        }
        return result;
    }

    /**
     * num * vector
     *
     * @param num
     * @param vector
     * @return
     */
    public static double[] numberMultiply(double num, double[] vector) {
        double[] result = new double[vector.length];
        for (int i = 0; i < vector.length; i++) {
            result[i] = num * vector[i];
        }
        return result;
    }

    public static double[] log2(double[] arr) {
        return Arrays.stream(arr)
                .map(DoubleMath::log2)
                .toArray();
    }


    /**
     * C<sub>n</sub><sup>m</sup>
     *
     * @param n
     * @param m
     * @return
     */
    // FIXME when the result is to big, it may overflow
    public static long combination(int n, int m) {
        Map<String, Double> combinationMap = new HashMap<>(n * m);
        if (n < m) {
            throw new IllegalArgumentException("n must be greater than or equal to m");
        } else {
            if (m == 0) return 1;
            if (m == 1) return n;
            if (m > n / 2) return combination(n, n - m);
            String key = n + "," + m;
            if (!combinationMap.containsKey(key)) {
                double result = (double) combination(n - 1, m - 1) + (combination(n - 1, m));
                if (result > Long.MAX_VALUE) {
                    throw new RuntimeException("The result is to big");
                }
                combinationMap.put(key, result);
            }
            return combinationMap.get(key).longValue();
        }
    }
}
