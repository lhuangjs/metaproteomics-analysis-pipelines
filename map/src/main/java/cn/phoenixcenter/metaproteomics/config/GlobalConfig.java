package cn.phoenixcenter.metaproteomics.config;

import cn.phoenixcenter.metaproteomics.nr.NRSearcher;
import cn.phoenixcenter.metaproteomics.taxonomy.TaxonSearcher;
import cn.phoenixcenter.metaproteomics.utils.ESUtil;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.unit.TimeValue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

@Log4j2
public class GlobalConfig {

    private static Properties probs;

    private static Map<Class, Object> clazz2Bean;

    static {
        try {
            Path configFilePath = Paths.get(System.getProperty("user.dir"), "map.properties")
                    .toAbsolutePath();
            if(Files.notExists(configFilePath)){
                System.err.println("The configuration file is not in <" + configFilePath + ">");
                System.exit(255);
            }
            log.info("we will obtain configuration file from <{}>", configFilePath.toString());
            init(configFilePath);
            initBean();
        } catch (IOException e) {
            throw new IllegalStateException("Global setting initialization error!!");
        }
    }

    /**
     * Initialize Properties
     *
     * @param configFilePath
     * @throws IOException
     */
    public static void init(Path configFilePath) throws IOException {
        probs = new Properties();
        probs.load(Files.newInputStream(configFilePath));
    }

    private static void initBean() throws IOException {
        clazz2Bean = new HashMap<>();
        // elasticsearch client
        TransportClient esClient = ESUtil.createESClient(
                getValue("es.cluster.name"),
                getValue("es.hosts"),
                getValue("es.ports"));
        clazz2Bean.put(TransportClient.class, esClient);
        // taxon searcher
        TaxonSearcher taxonSearcher = new TaxonSearcher(
                esClient,
                getValue("taxon-searcher.index.name"),
                getValueAsInt("taxon-searcher.scroll.size"),
                new TimeValue(getValueAsInt("taxon-searcher.scroll.keep.alive.minute"), TimeUnit.MINUTES)
        );
        clazz2Bean.put(TaxonSearcher.class, taxonSearcher);
        // nr searcher
        NRSearcher nrSearcher = new NRSearcher(
                esClient,
                getValue("nr-searcher.index.name"),
                getValueAsInt("nr-searcher.scroll.size"),
                new TimeValue(getValueAsInt("nr-searcher.keep.alive.minute"), TimeUnit.MINUTES),
                getValueAsInt("nr-searcher.multi.phrase.query.size"),
                getValueAsInt("nr-searcher.multi.term.query.size"),
                getValueAsInt("nr-searcher.terms.query.size"),
                taxonSearcher
        );
        clazz2Bean.put(NRSearcher.class, nrSearcher);
    }

    public static String getValue(String key) {
        return probs.getProperty(key);
    }

    public static int getValueAsInt(String key) {
        return Integer.parseInt(probs.getProperty(key));
    }

    public static <T> T getObj(Class<T> clazz) {
        return (T) (clazz2Bean.get(clazz));
    }
}
