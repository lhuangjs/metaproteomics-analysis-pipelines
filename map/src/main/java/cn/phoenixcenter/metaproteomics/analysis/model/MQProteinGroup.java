package cn.phoenixcenter.metaproteomics.analysis.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * MaxQuant protein group object.
 */

@Getter
@Setter
@NoArgsConstructor
@ToString
public class MQProteinGroup {

    private String[] majorityProteinIds;

    private int razorPlusUniquePeptides;

    private boolean onlyIdentifiedBySite;

    private boolean reverse;

    private boolean potentialContaminant;

    private double[] lfqIntensityArr;

    // FIXME delete
    public String fPrint() {
        return String.join("\t",
                Arrays.stream(majorityProteinIds).collect(Collectors.joining(";")),
                String.valueOf(razorPlusUniquePeptides),
                onlyIdentifiedBySite ? "+" : "",
                reverse ? "+" : "",
                potentialContaminant ? "+" : "",
                Arrays.stream(lfqIntensityArr).mapToObj(String::valueOf).collect(Collectors.joining("\t"))
        );
    }
}
