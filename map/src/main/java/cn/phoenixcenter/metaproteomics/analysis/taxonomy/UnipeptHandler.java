package cn.phoenixcenter.metaproteomics.analysis.taxonomy;

import cn.phoenixcenter.metaproteomics.analysis.model.MQPeptide;
import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import cn.phoenixcenter.metaproteomics.utils.FileReaderUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class UnipeptHandler {

    private static RestTemplate restTemplate = new RestTemplate();

    private static Logger logger = LogManager.getLogger(UnipeptPeptInfo.class);


    static {
        System.setProperty("webdriver.chrome.driver",
                GlobalConfig.getValue("webdriver.chrome.driver"));
    }

//    /**
//     * Get peptide information by Unipept pepinfo API in bulk
//     *
//     * @param peptideSet
//     * @param equateIL
//     * @param extra
//     * @param domains
//     * @return
//     */
//    public static List<UnipeptPeptInfo> pepInfo(Set<String> peptideSet,
//                                                boolean equateIL,
//                                                boolean extra,
//                                                boolean domains) {
//        List<UnipeptPeptInfo> unipeptPeptInfoList = new ArrayList<>(peptideSet.size());
//
//        // batch fetch
//        List<String> peptideList = new ArrayList<>(peptideSet);
//        int batchSize = 10000;
//        int times = peptideList.size() % batchSize == 0
//                ? peptideList.size() / batchSize
//                : peptideList.size() / batchSize + 1;
//        int start, end;
//        for (int i = 0; i < times; i++) {
//            start = batchSize * i;
//            end = start + batchSize <= peptideList.size() ? start + batchSize : peptideList.size();
//            logger.debug("{} - {} is being processed...", start, (end - 1));
//            unipeptPeptInfoList.addAll(peptinfo(peptideList.subList(start, end), equateIL, extra, domains));
//        }
//        return unipeptPeptInfoList;
//    }
//
//    /**
//     * Get peptide information by Unipept pepinfo API
//     *
//     * @param peptideList
//     * @param equateIL
//     * @param extra
//     * @param domains
//     * @return
//     */
//    private static List<UnipeptPeptInfo> peptinfo(List<String> peptideList,
//                                                  boolean equateIL,
//                                                  boolean extra,
//                                                  boolean domains) {
//        List<UnipeptPeptInfo> unipeptPeptInfoList = new ArrayList<>(peptideList.size());
//
//        // url
//        String url = "http://api.unipept.ugent.be/api/v1/peptinfo.json";
//        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
//        peptideList.stream()
//                .forEach(pep -> params.add("input[]", pep));
//        if (equateIL) {
//            params.add("equate_il", "true");
//        }
//        if (extra) {
//            params.add("extra", "true");
//            params.add("names", "true");
//        }
//        if (domains) {
//            params.add("domains", "true");
//        }
//        HttpHeaders headers = new HttpHeaders();
//        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));
//        RequestEntity requestEntity = new RequestEntity(params, headers, HttpMethod.POST, URI.create(url));
//
//        // fetch nodeData
//        logger.debug("fetch nodeData from Unipept ...");
//        ResponseEntity<JsonNode> responseEntity = restTemplate.exchange(requestEntity, JsonNode.class);
//        logger.debug("parse response body ...");
//        JsonNode responseBody = responseEntity.getBody();
//
//        // parse response
//        for (int i = 0; i < responseBody.size(); i++) {
//            final JsonNode pep = responseBody.get(i);
//            UnipeptPeptInfo peptInfo = new UnipeptPeptInfo();
//            peptInfo.setSequence(pep.get("peptide").textValue());
//            Taxon lca = new Taxon();
//            lca.setId(pep.get("taxon_id").intValue());
//            lca.setName(pep.get("taxon_name").textValue());
//            lca.setRank(pep.get("taxon_rank").textValue());
//            Map<String, Integer> lineageIdMap = new HashMap<>();
//            Map<String, String> lineageNameMap = new HashMap<>();
//            for (Taxon.Rank rank : Taxon.Rank.values()) {
//                if (pep.hasNonNull(rank.rankToString() + "_id")) {
//                    lineageIdMap.put(rank.rankToString(), pep.get(rank.rankToString() + "_id").intValue());
//                    lineageNameMap.put(rank.rankToString(), pep.get(rank.rankToString() + "_name").textValue());
//                }
//            }
//            peptInfo.setLca(lca);
//            unipeptPeptInfoList.add(peptInfo);
//        }
//        return unipeptPeptInfoList;
//    }

    /**
     * Submit peptide list in bulk to "Unipept Metaproteomics Analysis" automatically and in parallel
     *
     * @param peptideSet
     * @param equateIL
     * @param missedCleavage
     * @param resultFile
     * @throws IOException
     */
    public static void parallelMPA(Set<String> peptideSet,
                                   boolean equateIL,
                                   boolean missedCleavage,
                                   String resultFile) throws IOException {
        List<String> peptideList = new ArrayList<>(peptideSet);
//        if (equateIL) {
//            peptideList = peptideSet.parallelStream()
//                    .map(pep -> pep.replace("I", "L"))
//                    .distinct()
//                    .collect(Collectors.toList());
//        } else {
//            peptideList = new ArrayList<>(peptideSet);
//        }

        final int batchSize = 300;
        final int times = peptideList.size() % batchSize == 0
                ? peptideList.size() / batchSize
                : peptideList.size() / batchSize + 1;
        logger.debug("total peptides: <{}> -- batch size: <{}> -- times: <{}>", peptideList.size(), batchSize, times);

        final Path[] tempResultDirArr = new Path[times];
        final List<String> queryPart = new ArrayList<>(times);
        for (int i = 0; i < times; i++) {
            // create a temporary directory as download file location
            tempResultDirArr[i] = Files.createTempDirectory("unipept");
            queryPart.add(
                    peptideList.subList(
                            batchSize * i,
                            batchSize * (i + 1) < peptideList.size()
                                    ? batchSize * (i + 1)
                                    : peptideList.size()
                    )
                            .stream()
                            .collect(Collectors.joining("\n"))
            );
        }

        AtomicInteger atomicInteger = new AtomicInteger(0);

        queryPart.parallelStream()
                .map(query -> {
                    try {

                        Path tempDirPath = tempResultDirArr[atomicInteger.getAndIncrement()];

                        // set chrome driver
                        HashMap<String, Object> chromePrefs = new HashMap<>();
                        chromePrefs.put("profile.default_content_settings.popups", 0);
                        ChromeOptions options = new ChromeOptions();
                        options.setExperimentalOption("prefs", chromePrefs);
                        options.addArguments("--test-type");
//                        options.addArguments("--headless"); // open browser windows silently in background
                        options.addArguments("--disable-gpu");

                        ChromeDriverService driverService = ChromeDriverService.createDefaultService();
                        ChromeDriver driver = new ChromeDriver(driverService, options);

                        Map<String, String> params = new HashMap<>();
                        params.put("behavior", "allow");
                        params.put("downloadPath", tempDirPath.toFile().getPath());
                        Map<String, Object> commandParams = new HashMap<>();
                        commandParams.put("params", params);
                        commandParams.put("cmd", "Page.setDownloadBehavior");
                        ObjectMapper mapper = new ObjectMapper();
                        String command = mapper.writeValueAsString(commandParams);
                        String uri = driverService.getUrl().toString() + "/session/"
                                + driver.getSessionId().toString()
                                + "/chromium/send_command";
                        HttpPost request = new HttpPost(uri);
                        request.setEntity(new StringEntity(command));
                        HttpClient httpClient = HttpClientBuilder.create().build();
                        httpClient.execute(request);


                        // fill in form
                        driver.get("https://unipept.ugent.be/datasets");
                        WebElement form = driver.findElement(By.tagName("form"));
                        driver.executeScript("arguments[0].value=arguments[1]",
                                form.findElement(By.id("qs")),
                                query);
                        if (form.findElement(By.id("il")).isSelected() != equateIL) {
                            form.findElement(By.id("il")).click();
                        }
                        if (form.findElement(By.id("missed")).isSelected() != missedCleavage) {
                            form.findElement(By.id("missed")).click();
                        }
                        form.submit();

                        // download result file
                        WebDriverWait wait = new WebDriverWait(driver, Duration.ofDays(1).getSeconds());
                        WebElement progressBar = driver.findElement(By.id("progress-fa-analysis"));
                        wait.until(ExpectedConditions.invisibilityOf(progressBar));
                        driver.findElement(By.id("mpa-download-results")).click();
                        Path downloadFilePath = Paths.get(tempDirPath.toFile().getAbsolutePath(),
                                "mpa_result.csv");
                        logger.debug("-<{}>-download metaproteomics analysis result into <{}>", atomicInteger.get(), downloadFilePath);
                        wait.until(new ExpectedCondition<Boolean>() {
                            @Nullable
                            @Override
                            public Boolean apply(@Nullable WebDriver webDriver) {
                                return Files.exists(downloadFilePath);
                            }
                        });
                        driver.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return true;
                })
                .count();

        // write into result file
        Path resultFilePath = Paths.get(resultFile);
        Files.deleteIfExists(resultFilePath);
        Files.createFile(resultFilePath);

        for (int i = 0; i < tempResultDirArr.length; i++) {
            Path path = Paths.get(tempResultDirArr[i].toFile().getAbsolutePath(), "mpa_result.csv"); // transform result dir to result file path
            if (i == 0) {
                Files.write(resultFilePath,
                        Files.readAllLines(path)
                                .stream()
                                .collect(Collectors.toList()),
                        StandardOpenOption.APPEND);
            } else {
                Files.write(resultFilePath,
                        Files.readAllLines(path)
                                .stream()
                                .skip(1)
                                .collect(Collectors.toList()),
                        StandardOpenOption.APPEND);
            }

            Files.delete(path);
            Files.delete(path.getParent());
            logger.debug("delete temporary result file<{}>", path.getParent());
        }
    }

    /**
     * Submit peptide list in bulk to "Unipept Metaproteomics Analysis" automatically
     *
     * @param peptideSet
     * @param equateIL
     * @param missedCleavage
     * @param resultFile
     * @throws IOException
     */
    public static void mpa(Set<String> peptideSet,
                           boolean equateIL,
                           boolean missedCleavage,
                           String resultFile) throws IOException {

        // result file
        Path resultFilePath = Paths.get(resultFile);
        Files.deleteIfExists(resultFilePath);
        Files.createFile(resultFilePath);

        List<String> peptideList = new ArrayList<>(peptideSet);
//        if (equateIL) {
//            peptideList = peptideSet.parallelStream()
//                    .map(pep -> pep.replace("I", "L"))
//                    .distinct()
//                    .collect(Collectors.toList());
//        } else {
//            peptideList = new ArrayList<>(peptideSet);
//        }

        final int batchSize = 500;
        final int times = peptideList.size() % batchSize == 0
                ? peptideList.size() / batchSize
                : peptideList.size() / batchSize + 1;
        logger.debug("total peptides: <{}> -- batch size: <{}> -- times: <{}>", peptideList.size(), batchSize, times);

        for (int i = 0; i < times; i++) {

            // create a temporary directory as download file location
            Path tempDirPath = Files.createTempDirectory("unipept");
            String query = peptideList.subList(
                    batchSize * i,
                    batchSize * (i + 1) < peptideList.size()
                            ? batchSize * (i + 1)
                            : peptideList.size()
            )
                    .stream()
                    .collect(Collectors.joining("\n"));

            // set chrome driver
            HashMap<String, Object> chromePrefs = new HashMap<>();
            chromePrefs.put("profile.default_content_settings.popups", 0);
            ChromeOptions options = new ChromeOptions();
            options.setExperimentalOption("prefs", chromePrefs);
            options.addArguments("--test-type");
//                        options.addArguments("--headless"); // open browser windows silently in background
            options.addArguments("--disable-gpu");

            ChromeDriverService driverService = ChromeDriverService.createDefaultService();
            ChromeDriver driver = new ChromeDriver(driverService, options);

            Map<String, String> params = new HashMap<>();
            params.put("behavior", "allow");
            params.put("downloadPath", tempDirPath.toFile().getPath());
            Map<String, Object> commandParams = new HashMap<>();
            commandParams.put("params", params);
            commandParams.put("cmd", "Page.setDownloadBehavior");
            ObjectMapper mapper = new ObjectMapper();
            String command = mapper.writeValueAsString(commandParams);
            String uri = driverService.getUrl().toString() + "/session/"
                    + driver.getSessionId().toString()
                    + "/chromium/send_command";
            HttpPost request = new HttpPost(uri);
            request.setEntity(new StringEntity(command));
            HttpClient httpClient = HttpClientBuilder.create().build();
            httpClient.execute(request);


            // fill in form
            driver.get("https://unipept.ugent.be/datasets");
            WebElement form = driver.findElement(By.tagName("form"));
            driver.executeScript("arguments[0].value=arguments[1]",
                    form.findElement(By.id("qs")),
                    query);
            if (form.findElement(By.id("il")).isSelected() != equateIL) {
                form.findElement(By.id("il")).click();
            }
            if (form.findElement(By.id("missed")).isSelected() != missedCleavage) {
                form.findElement(By.id("missed")).click();
            }
            form.submit();

            // download result file
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofDays(1).getSeconds());
            WebElement progressBar = driver.findElement(By.id("progress-fa-analysis"));
            wait.until(ExpectedConditions.invisibilityOf(progressBar));
            driver.findElement(By.id("mpa-download-results")).click();
            Path downloadFilePath = Paths.get(tempDirPath.toFile().getAbsolutePath(),
                    "mpa_result.csv");
            logger.debug("download metaproteomics analysis result into <{}>", downloadFilePath);
            wait.until(new ExpectedCondition<Boolean>() {
                @Nullable
                @Override
                public Boolean apply(@Nullable WebDriver webDriver) {
                    return Files.exists(downloadFilePath);
                }
            });
            driver.close();

            // write result into file
            Path path = Paths.get(tempDirPath.toFile().getAbsolutePath(), "mpa_result.csv"); // transform temporary result dir to result file path
            if (i == 0) {
                Files.write(resultFilePath,
                        Files.readAllLines(path)
                                .stream()
                                .collect(Collectors.toList()),
                        StandardOpenOption.APPEND);
            } else {
                Files.write(resultFilePath,
                        Files.readAllLines(path)
                                .stream()
                                .skip(1)
                                .collect(Collectors.toList()),
                        StandardOpenOption.APPEND);
            }

            Files.delete(path);
            Files.delete(path.getParent());
            logger.debug("delete temporary result file<{}>", path.getParent());
        }
    }

//    public static void parseResult(String mpaFile) throws IOException {
//        final String[] ranks = Arrays.stream(Taxon.Rank.values())
//                .map(Taxon.Rank::rankToString)
//                .toArray(String[]::new);
//
//        FileReaderUtil .init(mpaFile, ",", true);
//        List<String> headers = FileReaderUtil.extractHeaders();
//        Function<String[], UnipeptPeptInfo> rowProcessor = (String[] tmp) -> {
//
//            // organize as UnipeptPeptInfo object
//            UnipeptPeptInfo peptInfo = new UnipeptPeptInfo();
//            peptInfo.setSequence(tmp[headers.indexOf("peptide")]);
//
//            Taxon lca = new Taxon();
//            lca.setName(tmp[headers.indexOf("lca")]);
//            Map<String, String> lineageNameMap = new HashMap<>();
//            for(String rank: ranks){
//                int index = headers.indexOf(rank);
//                if(tmp.length > index && !tmp[index].equals("")){
//                    lineageNameMap.put(rank, tmp[index]);
//                    if(tmp[index].equals(lca.getName())){
//                        lca.setRank(rank);
//                    }
//                }
//            }
//            return peptInfo;
//        };
//
//        // read file
//        List<UnipeptPeptInfo> peptInfoList = FileReaderUtil.read(rowProcessor);
//    }

    public static List<UnipeptPeptInfo> parseMPAResult(List<MQPeptide> mqPeptideList,
                                                       boolean equateIL,
                                                       String mpaResultFile) throws IOException {
//        Map<String, MQPeptide> pepMQPepMap = mqPeptideList.parallelStream()
//                .collect(Collectors.toMap(
//                        pep -> pep.getSequence(),
//                        pep -> pep)
//                );
        Map<String, List<MQPeptide>> pepMQPepListMap = new HashMap<>();
        for (MQPeptide mqPep : mqPeptideList) {
            if (equateIL) {
                String sequence = mqPep.getSequence().replace("I", "L");
                if (!pepMQPepListMap.containsKey(sequence)) {
                    pepMQPepListMap.put(sequence, new ArrayList<>());
                }
                pepMQPepListMap.get(sequence).add(mqPep);
            } else {
                String sequence = mqPep.getSequence();
                pepMQPepListMap.put(sequence, new ArrayList());
                pepMQPepListMap.get(sequence).add(mqPep);
            }
        }

        /** read Unipept mpa result **/
        final List<String> ranks = Arrays.stream(Taxon.Rank.values())
                .map(Taxon.Rank::rankToString)
                .collect(Collectors.toList());

        FileReaderUtil fr = new FileReaderUtil(mpaResultFile, ",", true);
        List<String> headers = fr.extractHeaders();
        Function<String[], UnipeptPeptInfo> rowProcessor = (String[] tmp) -> {

            // organize as UnipeptPeptInfo object
            UnipeptPeptInfo peptInfo = new UnipeptPeptInfo();
            String sequence = tmp[headers.indexOf("peptide")].trim();
            peptInfo.setUnipeptSequence(sequence);
            if (equateIL) {
                if (!pepMQPepListMap.containsKey(sequence.replace("I", "L"))) {
                    throw new RuntimeException(sequence + " did match any entry in MaxQuant peptides");
                }
                peptInfo.setMqPeptideList(pepMQPepListMap.get(sequence.replace("I", "L")));
            } else {
                if (!pepMQPepListMap.containsKey(sequence)) {
                    throw new RuntimeException(sequence + " did match any entry in MaxQuant peptides");
                }
                peptInfo.setMqPeptideList(pepMQPepListMap.get(sequence));
            }

            Taxon lca = new Taxon();
            lca.setName(tmp[headers.indexOf("lca")].trim());
            if (lca.getName().equals("root")) {
                lca.setRank("no rank");
            } else {
                Map<String, String> lineageNameMap = new HashMap<>();
                for (String rank : ranks) {
                    int index = headers.indexOf(rank);
                    if (tmp.length > index && !tmp[index].trim().equals("")) {
                        lineageNameMap.put(rank, tmp[index]);
                        if (tmp[index].trim().equals(lca.getName())) {
                            lca.setRank(rank);
                        }
                    }
                }
                // if the rank of some peptides LCA is not located the rank in Taxon.Ranks.values(),
                // raise the LCA to the nearest rank
                if (lca.getRank() == null) {
                    int index = IntStream.range(0, tmp.length)
                            .filter(i -> tmp[i].trim().equals(lca.getName()))
                            .skip(1)
                            .findFirst()
                            .getAsInt();
                    for (int i = index; i > headers.indexOf("lca"); i--) {
                        if (ranks.contains(headers.get(i))) {
                            lca.setRank(headers.get(i));
                            lca.setName(tmp[i].trim());
                            break;
                        }
                    }
                }
                lca.setLineageNameMap(lineageNameMap);
            }
            peptInfo.setLca(lca);
            return peptInfo;
        };

        // read file
        return fr.read(rowProcessor);
    }

    /**
     * Remove entries whose LCA's rank are above the specific rank and group entries by specific rank.
     * <p>
     * Note:
     * 1. If the LCA's rank of a entry is satisfied with the requirement but there is
     * no taxon on specific rank, the entry will be removed.
     *
     * @param peptInfoList
     * @param specificRank
     * @return
     */
    public static Map<String, List<UnipeptPeptInfo>> filterByLca(List<UnipeptPeptInfo> peptInfoList,
                                                                 Taxon.Rank specificRank) {
        return peptInfoList.parallelStream()
                // remove no rank taxon
                .filter(peptInfo -> !peptInfo.getLca().getRank().equals("no rank"))
                // only remain Bacteria
                .filter(peptInfo -> peptInfo.getLca().getLineageNameMap().get(Taxon.Rank.SUPERKINGDOM.rankToString()).equals("Bacteria"))
                // remove entries whose LCA's rank are above the specific rank
                .filter(peptInfo -> Taxon.Rank.stringToRank(peptInfo.getLca().getRank())
                        .index() >= specificRank.index())
                // remove no taxon on specific rank
                .filter(peptInfo -> peptInfo.getLca().getLineageNameMap().containsKey(specificRank.rankToString()))
                .collect(Collectors.groupingBy(peptInfo -> peptInfo.getLca()
                        .getLineageNameMap()
                        .get(specificRank.rankToString()))
                );
    }

    /**
     * @param taxonNamePeptInfoMap
     * @param groups
     * @return taxon name => (group name => distribution)
     */
    public static Map<String, Map<String, Double>> calTaxonDistribution(Map<String, List<UnipeptPeptInfo>> taxonNamePeptInfoMap,
                                                                        Map<String, int[]> groups) {
        Map<Integer, String> reverseIndex = new HashMap<>(groups.values().size());
        for (String groupName : groups.keySet()) {
            for (int index : groups.get(groupName)) {
                reverseIndex.put(index, groupName);
            }
        }

        return taxonNamePeptInfoMap.entrySet().parallelStream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> {
                            double[][] intensityArr = entry.getValue().parallelStream()
                                    .map((UnipeptPeptInfo peptInfo) -> peptInfo.getMqPeptideList().stream()
                                            .map((MQPeptide pep) -> pep.getIntensityArr())
                                    )
                                    .flatMap((Stream<double[]> arr) -> arr)
                                    .toArray(double[][]::new);
                            return sumSpecificCols(intensityArr, reverseIndex);
                        }
                ));
    }

    private static Map<String, Double> sumSpecificCols(double[][] arr,
                                                       Map<Integer, String> reverseIndex) {
        Map<String, Double> sampleIntensitySumMap = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (sampleIntensitySumMap.containsKey(reverseIndex.get(j))) {
                    sampleIntensitySumMap.replace(reverseIndex.get(j),
                            sampleIntensitySumMap.get(reverseIndex.get(j)) + arr[i][j]);
                } else {
                    sampleIntensitySumMap.put(reverseIndex.get(j), arr[i][j]);
                }
            }
        }
        return sampleIntensitySumMap;
    }
}

@Getter
@Setter
@NoArgsConstructor
@ToString
class UnipeptPeptInfo {
    private String unipeptSequence;
    private List<MQPeptide> mqPeptideList;
    private Taxon lca;
}
