package cn.phoenixcenter.metaproteomics.analysis.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class MQPeptide {

    private String sequence;

    private String[] proteins;

    private boolean reverse;

    private boolean potentialContaminant;

    private double[] intensityArr;
}
