package cn.phoenixcenter.metaproteomics.analysis.model;

import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class MQProteinGroupEx {

    private MQProteinGroup mqProteinGroup;

    private int[] majorityProteinTaxonIds;

    private Taxon majorityProteinTaxonLCA;
}
