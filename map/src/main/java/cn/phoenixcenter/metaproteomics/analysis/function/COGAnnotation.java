package cn.phoenixcenter.metaproteomics.analysis.function;

import cn.phoenixcenter.metaproteomics.analysis.model.MQProteinGroup;
import cn.phoenixcenter.metaproteomics.utils.FileReaderUtil;
import cn.phoenixcenter.metaproteomics.utils.MathUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class COGAnnotation {

    private static Logger logger = LogManager.getLogger(COGAnnotation.class);

    /**
     * Extract sequence by protein id
     *
     * @param proIdSet   protein id set need to be extracted
     * @param proSeqFile the location of fasta format sequence library file
     * @param outputFile
     * @throws IOException
     */
    public static void extractSequence(Set<String> proIdSet,
                                       String proSeqFile,
                                       String outputFile) throws IOException {
        List<FastaSeq> fastaSeqList = new ArrayList<>();

        /** read fasta file**/
        BufferedReader br = new BufferedReader(new FileReader(proSeqFile));
        String line;
        FastaSeq fastaSeq = null;
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">")) {
                fastaSeq = new FastaSeq();
                fastaSeqList.add(fastaSeq);
                fastaSeq.setId(line.substring(1, line.indexOf(" ")));
                fastaSeq.setHeader(line);
            } else {
                fastaSeq.addSequence(line);
            }
        }
        br.close();

        /** filter **/
        List<FastaSeq> results = fastaSeqList.parallelStream()
                .filter(fs -> proIdSet.contains(fs.getId()))
                .collect(Collectors.toList());

        /** recorded protein ids that did not map any sequence **/
        if (results.size() != proIdSet.size()) {
            Set<String> processedProIdSet = results.parallelStream()
                    .map(FastaSeq::getId)
                    .collect(Collectors.toSet());
            logger.warn("{{}} did match sequence",
                    proIdSet.parallelStream()
                            .filter(id -> !processedProIdSet.contains(id))
                            .collect(Collectors.joining(","))
            );
        }

        /** write **/
        Files.write(Paths.get(outputFile),
                results.parallelStream()
                        .map(FastaSeq::fPrint)
                        .collect(Collectors.toList()),
                Charset.forName("UTF-8"),
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);

    }

    public static void align(String querySeqFile, String outputFile) throws IOException, InterruptedException {
        String diamondLocation = COGAnnotation.class
                .getResource("/diamond/diamond")
                .getPath();
        String cogDBLocation = COGAnnotation.class
                .getResource("/COG/COG.dmnd")
                .getPath();
        String command = String.join(" ", diamondLocation, "blastp -d",
                cogDBLocation, "-q", querySeqFile,
                "-b 0.2 -k 1 -o", outputFile);
        logger.debug("diamond command: {}", command);

        /** execute diamond **/
        BufferedReader br = null;
        Process process = Runtime.getRuntime().exec(command);
        if (process.waitFor() != 0) {
            br = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String line;
            while ((line = br.readLine()) != null) {
                logger.error(line);
            }
            br.close();
            System.exit(process.exitValue());
        } else {
            br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = br.readLine()) != null) {
                logger.debug(line);
            }
            br.close();
        }
    }

    /**
     * Read cogname2003-2014.tab file and parse as COG object
     * <PRE>
     * tab-delimited
     * [COG-id], [functional-class], [COG-annotation]
     * </PRE>
     *
     * @param cogFile
     * @return
     * @throws IOException
     */
    public static Map<String, COG> parseCOGNode(String cogFile) throws IOException {
        // cog id => COG object
        Map<String, COG> cogMap = new HashMap<>();

        FileReaderUtil fr = new FileReaderUtil(cogFile, "\t", true);
        Consumer<String> rowConsumer = (String line) -> {
            String[] tmp = line.split("\t");
            COG cog = new COG();
            cog.setId(tmp[0].trim());
            cog.setFunctionClass(tmp[1].trim());
            cog.setAnnotation(tmp[2].trim());
            cogMap.put(cog.getId(), cog);
        };
        fr.read(rowConsumer);
        return cogMap;
    }

    /**
     * Read cog2003-2014.csv and extract mapping from protein id to COG-id
     * <PRE>
     * comma-delimited
     * [domain-id], [genome-name], [protein-id], [protein-length],
     * [domain-start], [domain-end], [COG-id], [membership-class]
     * </PRE>
     *
     * @param cogFile
     * @return
     * @throws IOException
     */
    public static Map<String, String> extractProCOGIdMap(String cogFile) throws IOException {
        // protein id => COG id
        Map<String, String> proCOGIdMap = new HashMap<>();
        // read
        FileReaderUtil fr = new FileReaderUtil(cogFile, ",", false);
        Consumer<String> rowConsumer = (String line) -> {
            String[] tmp = line.split("\t");
            proCOGIdMap.put(tmp[2].trim(), tmp[6].trim());
        };
        fr.read(rowConsumer);
        return proCOGIdMap;
    }

    /**
     * <PRE>
     * tab-delimited
     * [qseqid], [sseqid], [pident], [length], [mismatch],
     * [gapopen], [qstart], [qend], [sstart], [send], [evalue], [bitscore]
     * </PRE>
     *
     * @param alignmentFile
     */
    public static List<MQProteinGroupCOG> parseAlignmentResult(String alignmentFile,
                                                               List<MQProteinGroup> pgList) throws IOException {
        // extract mapping from query sequence ids to subject sequence ids
        Map<String, String> qsIdMap = new HashMap<>();
        FileReaderUtil fr = new FileReaderUtil(alignmentFile, "\t", false);
        //gi|29349220|ref|NP_812723.1|
        final Pattern proIdPat = Pattern.compile("gi\\|(\\d+)\\|");
        Consumer<String> rowConsumer = (String line) -> {
            String[] tmp = line.split("\t");
            Matcher m = proIdPat.matcher(tmp[1].trim());
            m.find();
            qsIdMap.put(tmp[0].trim(), m.group(1));
        };
        fr.read(rowConsumer);
        // annotate query sequences by subject sequences id
        return pgList.parallelStream()
                .map((MQProteinGroup pg) -> {
                    MQProteinGroupCOG pgCOG = new MQProteinGroupCOG();
                    pgCOG.setMqProteinGroup(pg);
                    COG[] majProCOGs = Arrays.stream(pg.getMajorityProteinIds())
                            .map(qid -> annotateCOG(qsIdMap.get(qid)))
                            .toArray(COG[]::new);
                    if (Arrays.stream(majProCOGs).filter(cog -> cog != null).count() != 0) {
                        pgCOG.setMajorityProCOGs(majProCOGs);
                        pgCOG.generateLeadingCOG();
                    }
                    return pgCOG;
                })
                .collect(Collectors.toList());
    }

    static Map<String, String> proCOGIdMap;
    static Map<String, COG> cogMap;

    static {
        try {
            proCOGIdMap = extractProCOGIdMap(COGAnnotation.class
                    .getResource("/COG/cog2003-2014.csv")
                    .getPath());
            cogMap = parseCOGNode(COGAnnotation.class
                    .getResource("/COG/cognames2003-2014.tab")
                    .getPath());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * subject sequence => COG id => COG
     *
     * @param proId
     * @return
     */
    public static COG annotateCOG(String proId) {
        if (proCOGIdMap.containsKey(proId)) {
            return cogMap.get(proCOGIdMap.get(proId));
        } else {
            return null;
        }
    }


    public static Map<String, double[]> cogDistribution(List<MQProteinGroupCOG> pgCOGList) {
        Map<String, List<MQProteinGroupCOG>> groups = pgCOGList.parallelStream()
                .filter(pgCOG -> pgCOG.getLeadingCOG() != null)
                .collect(Collectors.groupingBy(pgCOG -> pgCOG.getLeadingCOG().getFunctionClass()));
        return groups.entrySet().parallelStream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> entry.getValue().stream()
                                .map(pgCOG -> pgCOG.getMqProteinGroup().getLfqIntensityArr())
                                .reduce((lfqArr1, lfqArr2) -> MathUtil.vectorPlus(lfqArr1, lfqArr2))
                                .get()
                ));
    }

    public static void writeCOGDistribution(Map<String, double[]> cogDistribution,
                                            String outputFile) throws IOException {
        List<String> lines = cogDistribution.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))
                .map(entry -> String.join("\t", entry.getKey(),
                        Arrays.stream(entry.getValue())
                                .mapToObj(String::valueOf)
                                .collect(Collectors.joining("\t"))
                ))
                .collect(Collectors.toList());
        Files.write(Paths.get(outputFile),
                lines,
                Charset.forName("UTF-8"),
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);
    }
}

@Getter
@Setter
@NoArgsConstructor
@ToString
class FastaSeq {
    private String id;

    private String header;

    private String sequence;

    public void addSequence(String seq) {
        if (sequence == null) {
            sequence = seq;
        } else {
            sequence += seq;
        }
    }

    /**
     * formatted as <b>FASTA</b> and print
     */
    public String fPrint() {
        return header + System.lineSeparator() + sequence;
    }
}