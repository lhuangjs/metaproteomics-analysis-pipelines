package cn.phoenixcenter.metaproteomics.analysis.taxonomy;

import cn.phoenixcenter.metaproteomics.analysis.model.MQPeptide;
import cn.phoenixcenter.metaproteomics.analysis.model.MQPeptideEx;
import cn.phoenixcenter.metaproteomics.taxonomy.LCAAnalysis;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import cn.phoenixcenter.metaproteomics.taxonomy.TaxonSearcher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class MQPeptideHandler {

    private static Logger logger = LogManager.getLogger(MQPeptide.class);

    private static TaxonSearcher taxonSearcher;

    /**
     * Read accession and taxon id mapping
     *
     * @param accTaxonIdMappingFile
     * @return
     * @throws IOException
     */
    public static Map<String, Integer> readAccTaxonIdMapping(String accTaxonIdMappingFile) throws IOException {
        return Files.readAllLines(Paths.get(accTaxonIdMappingFile), Charset.forName("UTF-8"))
                .parallelStream()
                .map((String line) -> line.split("\t"))
                .filter((String[] tmp) -> tmp.length == 2)
                .collect(Collectors.toMap((String[] tmp) -> tmp[0].trim(),
                        (String[] tmp) -> Integer.parseInt(tmp[1])));
    }

    /**
     * Add taxon ids and LCA info of proteins for each peptide.
     * <p>If taxon ids of proteins is null(possibly due to lack of protein id to taxon id info),
     * the taxon ids and LCA info of proteins will be set null.</p>
     *
     * @param mqPepList
     * @param pid2TidFile
     * @param ignoreNoTaxonIdMappingAcc
     * @return
     * @throws IOException
     */
    public static List<MQPeptideEx> addTaxonInfo(List<MQPeptide> mqPepList,
                                                 String pid2TidFile,
                                                 boolean ignoreNoTaxonIdMappingAcc) throws IOException {
        // accession number => taxon id
        Map<String, Integer> pid2TidMap = readAccTaxonIdMapping(pid2TidFile);
        // taxon id => taxon
        Map<Integer, Taxon> taxonMap = taxonSearcher.getTaxaByIds(
                pid2TidMap.values()
                        .parallelStream()
                        .collect(Collectors.toSet())
        );

        // add fields proteinsTaxonIds and calculate LCA of proteins, then convert to MQPeptideEx
        List<MQPeptideEx> pepExList = mqPepList.parallelStream()
                .map((MQPeptide mqPep) -> {
                    // get taxon id of proteins
                    int[] proteinsTaxonIds = Arrays.stream(mqPep.getProteins())
                            .filter(acc -> {
                                boolean flag = pid2TidMap.containsKey(acc);
                                if (!flag) {
                                    if (ignoreNoTaxonIdMappingAcc) {
                                        logger.warn("protein <" + acc
                                                + "> don't have taxon id mapping in peptide <"
                                                + mqPep.getSequence()
                                                + ">");
                                    } else {
                                        throw new RuntimeException("protein <" + acc
                                                + "> don't have taxon id mapping in peptide <"
                                                + mqPep.getSequence()
                                                + ">");
                                    }
                                }
                                return flag;
                            })
                            .mapToInt(pid2TidMap::get)
                            .toArray();
                    // calculate LCA
                    int lcaTaxonId = LCAAnalysis.calculateUnipeptLCA(taxonMap,
                            Arrays.stream(proteinsTaxonIds)
                                    .boxed()
                                    .collect(Collectors.toSet()));

                    // organize into MQPeptideEx object
                    MQPeptideEx mqPepEx = new MQPeptideEx();
                    mqPepEx.setMqPeptide(mqPep);
                    mqPepEx.setProteinsTaxonIds(proteinsTaxonIds.length > 0 ? proteinsTaxonIds : null);
                    if (lcaTaxonId != -1) {
                        Taxon lca = new Taxon();
                        lca.setId(lcaTaxonId);
                        mqPepEx.setProteinsLCA(lca);
                    }
                    return mqPepEx;
                })
                .collect(Collectors.toList());
        // set lca Taxon info
        Set<Integer> lcaIdSet = pepExList.parallelStream()
                .filter(p -> p.getProteinsLCA() != null)
                .map(p -> p.getProteinsLCA().getId())
                .collect(Collectors.toSet());
        Map<Integer, Taxon> lcaTaxonMap = taxonSearcher.getTaxaByIds(lcaIdSet);
        return pepExList.parallelStream()
                .filter(p -> p.getProteinsLCA() != null)
                .map(p -> {
                    p.setProteinsLCA(lcaTaxonMap.get(p.getProteinsLCA().getId()));
                    return p;
                })
                .collect(Collectors.toList());

    }

    /**
     * Remove entries whose LCA are higher than the specific rank,
     * then group entries by taxon id on specific rank
     * and remove taxa whose unique peptides are less than the threshold(minUniquePeptides).
     * <p>
     * NOTE: If a entry whose LCA's rank is equal to or below than the specific rank
     * but there is no taxon on specific rank, it also will be removed.
     * For Example,
     * <PRE>
     * specific rank: genus
     * LCA: {id=22, rank=species, lineageIdMap={"species": 22, "family": 44}}
     * the entry will be removed
     * </PRE>
     *
     * @param pepExList
     * @param specificRank
     * @param minUniquePeptides
     * @return
     */
    public static Map<Taxon, List<MQPeptideEx>> filterByLCACount(List<MQPeptideEx> pepExList,
                                                                 Taxon.Rank specificRank,
                                                                 int minUniquePeptides) throws IOException {

        /**
         * remove entries:
         * 1) LCA is null
         * or 2) taxon is null on specific rank
         * or 3) superkingdom is not 2
         * or 4) LCA rank is above the specific rank
         */
        // taxon id on specific rank => peptide list
        Map<Integer, List<MQPeptideEx>> groupMap = pepExList.parallelStream()
                .filter(pepEx -> pepEx.getProteinsLCA() != null)
                .filter(pepEx -> pepEx.getProteinsLCA().getLineageIdMap().containsKey(specificRank.rankToString())
                        && pepEx.getProteinsLCA().getLineageIdMap().get(specificRank.rankToString()) != null)
                .filter(pepEX -> pepEX.getProteinsLCA().getLineageIdMap().get("superkingdom") == 2)
                .filter(pepEx -> Taxon.Rank.stringToRank(pepEx.getProteinsLCA().getRank()).index()
                        >= specificRank.index())
                .collect(Collectors.groupingBy(
                        pepEx -> pepEx.getProteinsLCA()
                                .getLineageIdMap()
                                .get(specificRank.rankToString())
                        )
                );

        /** remove taxa whose unique peptides are less than the threshold(minUniquePeptides) **/
        groupMap = groupMap.entrySet()
                .parallelStream()
                .filter(e -> e.getValue().size() >= minUniquePeptides)
                .collect(Collectors.toMap(
                        e -> e.getKey(),
                        e -> e.getValue()
                ));
        // get taxon by taxon id
        Map<Integer, Taxon> taxonMap = taxonSearcher.getTaxaByIds(groupMap.keySet());
        return groupMap.entrySet().parallelStream()
                .collect(Collectors.toMap(
                        e -> taxonMap.get(e.getKey()),
                        e -> e.getValue()
                        )
                );
    }

    /**
     * Calculate taxon distribute by sum the top (topNIntensity) peptide intensity values
     *
     * @param taxonPeptideMap
     * @param topNPepIntensity
     * @return
     */
    public static Map<Taxon, Double> calTaxonDistribution(Map<Taxon, List<MQPeptideEx>> taxonPeptideMap,
                                                          int topNPepIntensity) {
        return taxonPeptideMap.entrySet().parallelStream()
                .filter((Map.Entry<Taxon, List<MQPeptideEx>> entry) -> entry.getValue().size() >= topNPepIntensity)
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> entry.getValue().stream()
                                .map(pepEx ->
                                        Arrays.stream(pepEx.getMqPeptide().getIntensityArr())
                                                .boxed()
                                                .sorted(Comparator.reverseOrder())
                                                .limit(topNPepIntensity)
                                                .mapToDouble(Double::doubleValue)
                                                .sum()
                                )
                                .findFirst()
                                .get()
                ));

    }
}
