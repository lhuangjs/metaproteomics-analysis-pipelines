package cn.phoenixcenter.metaproteomics.analysis.taxonomy;

import cn.phoenixcenter.metaproteomics.analysis.model.MQProteinGroupEx;
import cn.phoenixcenter.metaproteomics.analysis.model.MQProteinGroup;
import cn.phoenixcenter.metaproteomics.taxonomy.LCAAnalysis;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import cn.phoenixcenter.metaproteomics.taxonomy.TaxonSearcher;
import cn.phoenixcenter.metaproteomics.utils.MathUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;

public class MQProteinGroupHandler {

    private static Logger logger = LogManager.getLogger(MQProteinGroupHandler.class);

    private static TaxonSearcher taxonSearcher;

    /**
     * Read accession and taxon id mapping
     *
     * @param accTaxonIdMappingFile
     * @return
     * @throws IOException
     */
    public static Map<String, Integer> readAccTaxonIdMapping(String accTaxonIdMappingFile) throws IOException {
//        FileReaderUtil.init(accTaxonIdMappingFile, "\t", true);

        return Files.readAllLines(Paths.get(accTaxonIdMappingFile), Charset.forName("UTF-8"))
                .parallelStream()
                .map((String line) -> line.split("\t"))
                .filter((String[] tmp) -> tmp.length == 2)
                .collect(Collectors.toMap((String[] tmp) -> tmp[0].trim(),
                        (String[] tmp) -> Integer.parseInt(tmp[1])));
    }

//    /**
//     * @param pid2TidMap
//     * @return
//     */
//    public static Map<String, Taxon> pid2Taxon(Map<String, Integer> pid2TidMap) throws IOException {
//        Map<Integer, Taxon> tid2TaxonMap = TaxonSearcher.getTaxaByIds(pid2TidMap.values().parallelStream()
//                .collect(Collectors.toSet()));
//
//        return pid2TidMap.entrySet().parallelStream()
//                .collect(Collectors.toMap(
//                        e -> e.getKey(),
//                        e -> tid2TaxonMap.get(e.getValue())
//                        )
//                );
//    }

    /**
     * Set taxon information for each MaxQuant protein group according to "Majority protein IDs".
     * <p>protein group -> Majority protein IDs -> taxon ids -> LCA</p>
     *
     * @param pgList
     * @param pid2TidFile
     * @param ignoreNoTaxonIdMappingAcc if true, a protein accession will be ignore when the protein accession
     *                                  do not have a taxon id mapping. if false, RuntimeException will be throw
     *                                  when the protein accession do not have a taxon id mapping.
     * @throws IOException
     */
    public static List<MQProteinGroupEx> addTaxonInfo(List<MQProteinGroup> pgList,
                                                      String pid2TidFile,
                                                      boolean ignoreNoTaxonIdMappingAcc) throws IOException {
        // protein id => taxon id
        Map<String, Integer> pid2TidMap = readAccTaxonIdMapping(pid2TidFile);
        // taxon id => taxon
        Map<Integer, Taxon> taxonMap = taxonSearcher.getTaxaByIds(
                pid2TidMap.values().parallelStream()
                        .collect(Collectors.toSet())
        );

        // add LCA taxon info
        List<MQProteinGroupEx> pgExList = pgList.parallelStream()
                .map((MQProteinGroup pg) -> {
                    // get taxon ids of majority proteins
                    int[] majorityProTaxonIds = Arrays.stream(pg.getMajorityProteinIds())
                            .filter((String acc) -> {
                                boolean flag = pid2TidMap.containsKey(acc);
                                if (!flag) {
                                    if (ignoreNoTaxonIdMappingAcc) {
                                        logger.warn("protein <" + acc
                                                + "> don't have taxon id mapping in Majority protein IDs <"
                                                + Arrays.stream(pg.getMajorityProteinIds())
                                                .collect(Collectors.joining(";")) + ">");
                                    } else {
                                        throw new RuntimeException("protein <" + acc
                                                + "> don't have taxon id mapping in Majority protein IDs <"
                                                + Arrays.stream(pg.getMajorityProteinIds())
                                                .collect(Collectors.joining(";")) + ">");
                                    }
                                }
                                return flag;
                            })
                            .mapToInt((String acc) -> pid2TidMap.get(acc))
                            .toArray();

                    // calculate LCA
                    int lcaTaxonId = majorityProTaxonIds.length != 0
                            ? LCAAnalysis.calculateUnipeptLCA(taxonMap,
                            Arrays.stream(majorityProTaxonIds)
                                    .boxed()
                                    .collect(Collectors.toSet()))
                            : null;

                    // organize MQProteinGroupEx object
                    MQProteinGroupEx pgEx = new MQProteinGroupEx();
                    pgEx.setMqProteinGroup(pg);
                    pgEx.setMajorityProteinTaxonIds(majorityProTaxonIds.length > 0 ? majorityProTaxonIds : null);
                    if (lcaTaxonId != -1) {
                        Taxon lcaTaxon = new Taxon();
                        lcaTaxon.setId(lcaTaxonId);
                        pgEx.setMajorityProteinTaxonLCA(lcaTaxon);
                    }
                    return pgEx;
                })
                .collect(Collectors.toList());
        // set LCA taxon info
        Set<Integer> lcaTaxonIdSet = pgExList.parallelStream()
                .filter(pg -> pg.getMajorityProteinTaxonLCA() != null)
                .map(pg -> pg.getMajorityProteinTaxonLCA().getId())
                .collect(Collectors.toSet());
        Map<Integer, Taxon> lcaTaxonMap = taxonSearcher.getTaxaByIds(lcaTaxonIdSet);
        return pgExList.parallelStream()
                .filter(pg -> pg.getMajorityProteinTaxonLCA() != null)
                .map(pg -> {
                    pg.setMajorityProteinTaxonLCA(lcaTaxonMap.get(pg.getMajorityProteinTaxonLCA().getId()));
                    return pg;
                })
                .collect(Collectors.toList());
    }

//    public static List<MQProteinGroupEx> addTaxonInfo(List<MQProteinGroup> pgList,
//                                                      Map<String, Integer> accTaxonIdMap,
//                                                      boolean ignoreNoTaxonIdMappingAcc) throws IOException {
//        // taxon id => taxon
//        Map<Integer, Taxon> taxonMap = TaxonSearcher.getTaxaByIds(
//                accTaxonIdMap.values().parallelStream()
//                        .collect(Collectors.toSet())
//        );
//
//        return pgList.parallelStream()
//                .map((MQProteinGroup pg) -> {
//
//                    // get taxon ids of majority proteins
//                    int[] majorityProTaxonIds = Arrays.stream(pg.getMajorityProteinIds())
//                            .filter((String acc) -> {
//                                boolean flag = accTaxonIdMap.containsKey(acc);
//                                if (!flag) {
//                                    if (ignoreNoTaxonIdMappingAcc) {
//                                        logger.warn("protein <" + acc
//                                                + "> don't have taxon id mapping in Majority protein IDs <"
//                                                + Arrays.stream(pg.getMajorityProteinIds())
//                                                .collect(Collectors.joining(";")) + ">");
//                                    } else {
//                                        throw new RuntimeException("protein <" + acc
//                                                + "> don't have taxon id mapping in Majority protein IDs <"
//                                                + Arrays.stream(pg.getMajorityProteinIds())
//                                                .collect(Collectors.joining(";")) + ">");
//                                    }
//                                }
//                                return flag;
//                            })
//                            .mapToInt((String acc) -> accTaxonIdMap.get(acc))
//                            .toArray();
//
//                    // calculate LCA
//                    Taxon lca = majorityProTaxonIds.length != 0
//                            ? LCAAnalysis.calculateLCA(taxonMap, majorityProTaxonIds)
//                            : null;
//
//                    // organize MQProteinGroupEx object
//                    MQProteinGroupEx pgEx = new MQProteinGroupEx();
//                    pgEx.setMqProteinGroup(pg);
//                    if (lca != null) {
//                        pgEx.setMajorityProteinTaxonIds(majorityProTaxonIds);
//                        pgEx.setMajorityProteinTaxonLCA(lca);
//                    }
//                    return pgEx;
//                })
//                .collect(Collectors.toList());
//    }

    /**
     * <p>1. Remove entries if LCA is null</p>
     * <p>2. Remove entries if its "Razor + unique peptides" count < minRazorPlusUniquePeptidesCount</p>
     * <p>3. Remove entries if its superkingdom is not Bacteria(taxon id = 2)</p>
     * <p>4. Remove entries if its LCA is above specific rank</p>
     *
     * @param pgExList
     * @return
     */
    public static List<MQProteinGroupEx> filterBySuperKingdomAndLCA(List<MQProteinGroupEx> pgExList,
                                                                    int minRazorPlusUniquePepNum,
                                                                    Taxon.Rank specificRank) {
        return pgExList.parallelStream()
                .filter(pgEx -> pgEx.getMajorityProteinTaxonLCA() != null)
                .filter(pgEx -> pgEx.getMajorityProteinTaxonLCA().getId() != 1)
                .filter(pgEx -> pgEx.getMqProteinGroup().getRazorPlusUniquePeptides() >= minRazorPlusUniquePepNum)
                .filter(pgEx -> pgEx.getMajorityProteinTaxonLCA().getLineageIdMap().get("superkingdom") == 2)
                .filter(pgEx -> Taxon.Rank.stringToRank(pgEx.getMajorityProteinTaxonLCA().getRank()).index()
                        >= specificRank.index())
                .collect(Collectors.toList());
    }

    /**
     * Calculate taxon distribution
     *
     * @param pgExList
     * @param specificRank
     * @return
     */
    public static Map<Taxon, double[]> calTaxonDistribution(List<MQProteinGroupEx> pgExList,
                                                            Taxon.Rank specificRank) throws IOException {
        // taxon on specific rank => samples LFQ intensity arr
        Map<Taxon, double[]> taxonIntensityMap = new HashMap();

        // bulk obtain taxon info of entries whose ranks are below the specific rank
        Set<Integer> taxonIdSet = pgExList.parallelStream()
                .filter(pg -> !pg.getMajorityProteinTaxonLCA().getRank().equals(specificRank.rankToString()))
                // remove entries that do not have taxon on specific rank
                .filter(pg -> pg.getMajorityProteinTaxonLCA().getLineageIdMap().containsKey(specificRank.rankToString())
                        && pg.getMajorityProteinTaxonLCA().getLineageIdMap().get(specificRank.rankToString()) != null)
                .map(pg -> pg.getMajorityProteinTaxonLCA().getLineageIdMap().get(specificRank.rankToString()))
                .collect(Collectors.toSet());
        Map<Integer, Taxon> taxonMap = taxonSearcher.getTaxaByIds(taxonIdSet);

        for (int i = 0; i < pgExList.size(); i++) {
            double[] intensityGroup = pgExList.get(i).getMqProteinGroup().getLfqIntensityArr();
            // obtain taxon on the specific rank(some LCA rank is below specific rank)
            // FIXME rank: String->Rank, equals; filteredLCA[i].getLineageIdMap().get
            if (pgExList.get(i).getMajorityProteinTaxonLCA().getLineageIdMap().containsKey(specificRank.rankToString())
                    && pgExList.get(i).getMajorityProteinTaxonLCA().getLineageIdMap().get(specificRank.rankToString()) != null) {

                Taxon taxon = pgExList.get(i).getMajorityProteinTaxonLCA().getRank().equals(specificRank.rankToString())
                        ? pgExList.get(i).getMajorityProteinTaxonLCA()
                        : taxonMap.get(
                        pgExList.get(i)
                                .getMajorityProteinTaxonLCA()
                                .getLineageIdMap()
                                .get(specificRank.rankToString())
                );
                // sum up the LFQ intensity values belong to the same taxon
                if (taxonIntensityMap.containsKey(taxon)) {
                    taxonIntensityMap.replace(taxon,
                            MathUtil.vectorPlus(taxonIntensityMap.get(taxon), intensityGroup));
                } else {
                    taxonIntensityMap.put(taxon, intensityGroup);
                }
            }
        }
        // FIXME 如果全部过滤完了，那么这里会运行当时没有结果怎么办
        // calculate taxon distribution
        double[] sum = taxonIntensityMap.values().stream()
                .reduce(new double[pgExList.get(0).getMqProteinGroup().getLfqIntensityArr().length],
                        (double[] matrix1, double[] matrix2) -> MathUtil.vectorPlus(matrix1, matrix2)
                );
        for (Taxon taxon : taxonIntensityMap.keySet()) {
            // taxon intensity for each sample ./ total intensity for each sample * 100
            taxonIntensityMap.replace(taxon, MathUtil.numberMultiply(100.0,
                    MathUtil.dotDivision(taxonIntensityMap.get(taxon), sum)));
        }
        return taxonIntensityMap;
    }

    public static void writeTaxonDistribution(Map<Taxon, double[]> distributionMap,
                                              Map<String, String[]> groups,
                                              List<String> lfqTagList,
                                              String outputFile) throws IOException {

        // format groups
        String[] samples = new String[lfqTagList.size()];
        int[] indics = new int[lfqTagList.size()];
        int i = 0;
        for (String g : groups.keySet()) {
            for (String sample : groups.get(g)) {
                samples[i] = sample;
                indics[i++] = lfqTagList.indexOf("LFQ intensity " + sample);
            }
        }
        List<String> lines = new ArrayList<>(distributionMap.size() + 1);

        // header
        lines.add(String.join("\t", "Taxon Name", "Taxon Id",
                Arrays.stream(samples)
                        .collect(Collectors.joining("\t"))
                )
        );

        // content
        lines.addAll(
                distributionMap.keySet().stream()
                        .sorted((t1, t2) -> t1.getName().compareToIgnoreCase(t2.getName()))
                        .map(t -> String.join("\t", t.getName(), String.valueOf(t.getId()),
                                Arrays.stream(indics)
                                        .mapToObj(index -> String.valueOf(distributionMap.get(t)[index]))
                                        .collect(Collectors.joining("\t"))
                        ))
                        .collect(Collectors.toList())
        );

        // write
        Files.write(Paths.get(outputFile), lines, Charset.forName("UTF-8"),
                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
    }
}