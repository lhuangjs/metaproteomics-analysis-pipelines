package cn.phoenixcenter.metaproteomics.analysis.preprocessing;

import cn.phoenixcenter.metaproteomics.analysis.model.MQPeptide;
import cn.phoenixcenter.metaproteomics.utils.FileReaderUtil;
import lombok.Getter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Getter
public class MQPeptideReader {

    private List<String> headers;

    private List<String> intensityTagList;

    /**
     * Read peptides.txt from MaxQuant and delete entries by setting
     *
     * @param pepFile
     * @param deleteReverse
     * @param deletePotentialContaminant
     * @return
     * @throws IOException
     */
    public List<MQPeptide> read(String pepFile,
                                boolean deleteReverse,
                                boolean deletePotentialContaminant) throws IOException {
        FileReaderUtil fr = new FileReaderUtil(pepFile, "\t", true);
        // extract headers
        headers = fr.extractHeaders();
        // get  "Intensity sample_name" columns indices
        final int[] intensityIndexArr = IntStream.range(0, headers.size())
                // the tailing space is necessary
                .filter((int index) -> headers.get(index).startsWith("Intensity "))
                .toArray();
        // extract names of all  "Intensity sample_name" columns
        intensityTagList = headers.stream()
                // the tailing space is necessary
                .filter(h -> h.startsWith("Intensity "))
                .collect(Collectors.toList());

        // row processor
        Function<String[], MQPeptide> rowProcessor = (String[] tmp) -> {
            // organize into MQPeptide object
            MQPeptide mqPeptide = new MQPeptide();
            mqPeptide.setSequence(tmp[headers.indexOf("Sequence")].trim());
            mqPeptide.setProteins(tmp[headers.indexOf("Proteins")].trim().split(";"));
            mqPeptide.setReverse(tmp[headers.indexOf("Reverse")].trim().equals("+"));
            mqPeptide.setPotentialContaminant(tmp[headers.indexOf("Potential contaminant")].trim().equals("+"));
            mqPeptide.setIntensityArr(Arrays.stream(intensityIndexArr)
                    .mapToDouble(index -> Double.parseDouble(tmp[index].trim()))
                    .toArray());
            return mqPeptide;
        };

        // row filter
        Predicate<String[]> rowDeleter = (String[] tmp) -> {
            if (deleteReverse
                    && tmp[headers.indexOf("Reverse")].trim().equals("+")) {
                return true;
            }
            if (deletePotentialContaminant
                    && tmp[headers.indexOf("Potential contaminant")].trim().equals("+")) {
                return true;
            }
            return false;
        };

        return fr.read(rowDeleter, rowProcessor);
    }

    /**
     * Read all entry in peptides.txt file from MaxQuant
     *
     * @param pepFile
     * @return
     * @throws IOException
     */
    public List<MQPeptide> read(String pepFile) throws IOException {
        return read(pepFile, false, false);
    }
}
