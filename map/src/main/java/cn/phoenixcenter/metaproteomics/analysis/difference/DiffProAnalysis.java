package cn.phoenixcenter.metaproteomics.analysis.difference;

import cn.phoenixcenter.metaproteomics.analysis.model.MQProteinGroup;
import com.google.common.math.DoubleMath;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.math3.stat.inference.TestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DiffProAnalysis {

    /**
     * Perform two sample two-sides t-test
     *
     * @param pgList
     * @param g1Memb
     * @param g2Memb
     * @param g1MinValidCount
     * @param g2MinValidCount
     * @param lfqTagList
     * @return
     */
    public static List<MQProteinGroupStat> tTest(List<MQProteinGroup> pgList,
                                                 String[] g1Memb,
                                                 String[] g2Memb,
                                                 int g1MinValidCount,
                                                 int g2MinValidCount,
                                                 List<String> lfqTagList) {
        List<MQProteinGroupStat> pgListStat = new ArrayList<>(pgList.size());

        for (MQProteinGroup pg : pgList) {
            double[] g1Lfq = Arrays.stream(g1Memb)
                    .mapToInt(gMemb -> lfqTagList.indexOf(gMemb))
                    .mapToDouble(membIndex -> pg.getLfqIntensityArr()[membIndex])
                    .map(DoubleMath::log2)
                    .toArray();
            double[] g2Lfq = Arrays.stream(g2Memb)
                    .mapToInt(gMemb -> lfqTagList.indexOf(gMemb))
                    .mapToDouble(membIndex -> pg.getLfqIntensityArr()[membIndex])
                    .map(DoubleMath::log2)
                    .toArray();
            if (validValueCount(g1Lfq) >= g1MinValidCount
                    && validValueCount(g2Lfq) >= g2MinValidCount) {
                double pValue = TestUtils.tTest(extractValidValue(g1Lfq), extractValidValue(g2Lfq));
                MQProteinGroupStat pgStat = new MQProteinGroupStat();
                pgStat.setMqProteinGroup(pg);
                pgStat.setPValue(pValue);
                pgListStat.add(pgStat);
            }
        }
        return pgListStat;
    }

    /**
     * Benjamini-Hochberg FDR correction
     *
     * @param pgListStat
     * @return
     */
    public static List<MQProteinGroupStat> bhFDRCorrection(List<MQProteinGroupStat> pgListStat) {
        // sort by p-value in ascending order
        final List<MQProteinGroupStat> sortedPGListStat = pgListStat.parallelStream()
                .sorted(Comparator.comparingDouble(MQProteinGroupStat::getPValue))
                .collect(Collectors.toList());
        final int count = sortedPGListStat.size();
        // adjust p-value
        double[] adjustedPValue = new double[count];
        for (int i = count - 1; i >= 0; i--) {
            if (i == count - 1) {
                adjustedPValue[i] = sortedPGListStat.get(i).getPValue();
            } else {
                double right = adjustedPValue[i + 1];
                double left = sortedPGListStat.get(i).getPValue() * count / (i + 1);
                adjustedPValue[i] = Double.min(right, left);
            }
            sortedPGListStat.get(i).setQValue(adjustedPValue[i]);
        }
        return sortedPGListStat;
    }

    /**
     * The entries whose q-value <= alpha will be retained.
     *
     * @param pgStatList
     * @param alpha      threshold
     * @return
     */
    public static List<MQProteinGroupStat> filterByQValue(List<MQProteinGroupStat> pgStatList,
                                                          double alpha) {
        return pgStatList.parallelStream()
                .filter(pgStat -> pgStat.getQValue() <= alpha)
                .collect(Collectors.toList());
    }

    private static boolean existNonZero(double[] arr) {
        return Arrays.stream(arr)
                .anyMatch(d -> d != 0.0);
    }

    private static int validValueCount(double[] arr) {
        return (int) Arrays.stream(arr)
                .filter(d -> !Double.isInfinite(d))
                .count();
    }

    private static double[] extractValidValue(double[] arr) {
        return Arrays.stream(arr)
                .filter(d -> !Double.isInfinite(d))
                .toArray();
    }
}

@Getter
@Setter
@ToString
@NoArgsConstructor
class MQProteinGroupStat {
    private MQProteinGroup mqProteinGroup;

    private double pValue;

    private double qValue;
}