package cn.phoenixcenter.metaproteomics.analysis.preprocessing;

import cn.phoenixcenter.metaproteomics.analysis.model.MQProteinGroup;
import cn.phoenixcenter.metaproteomics.utils.FileReaderUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Getter
@ToString
@NoArgsConstructor
public class MQProteinGroupReader {

    private List<String> headers;

    private List<String> lfqTagList;

    /**
     * Read MaxQuant "proteinGroups.txt" file
     *
     * @param pgFile
     * @return
     * @throws IOException
     */
    public List<MQProteinGroup> read(String pgFile) throws IOException {
        return read(pgFile, false, false, false);
    }

    /**
     * Read MaxQuant "proteinGroups.txt" file.
     * Meanwhile, some entries can be deleted according to users' choice.
     *
     * @param pgFile
     * @return
     * @throws IOException
     */
    public List<MQProteinGroup> read(String pgFile,
                                              boolean deleteOnlyIdentifiedBySite,
                                              boolean deleteReverse,
                                              boolean deletePotentialContaminant) throws IOException {

        FileReaderUtil fr = new FileReaderUtil(pgFile, "\t", true);
        headers = fr.extractHeaders();

        // get the LFQ intensity indices
        int[] lfqIndexArr = IntStream.range(0, headers.size())
                .filter((int index) -> headers.get(index).startsWith("LFQ"))
                .toArray();
        lfqTagList = Arrays.stream(lfqIndexArr)
                .mapToObj(index -> headers.get(index))
                .collect(Collectors.toList());

        // row filter
        Predicate<String[]> rowDeleter = (String[] tmp) -> {
            if (deleteOnlyIdentifiedBySite
                    && tmp[headers.indexOf("Only identified by site")].trim().equals("+")) {
                return true;
            }
            if (deleteReverse
                    && tmp[headers.indexOf("Reverse")].trim().equals("+")) {
                return true;
            }
            if (deletePotentialContaminant
                    && tmp[headers.indexOf("Potential contaminant")].trim().equals("+")) {
                return true;
            }
            return false;
        };

        // row processor
        Function<String[], MQProteinGroup> rowProcessor = (String[] tmp) -> {
            // organize as MQProteinGroup object
            MQProteinGroup pg = new MQProteinGroup();
            String[] majorityPros = Arrays.stream(tmp[headers.indexOf("Majority protein IDs")].split(";"))
                    .map(String::trim)
                    .toArray(String[]::new);
            pg.setMajorityProteinIds(majorityPros);
            pg.setRazorPlusUniquePeptides(Integer.parseInt(tmp[headers.indexOf("Razor + unique peptides")]));
            pg.setPotentialContaminant(tmp[headers.indexOf("Potential contaminant")].trim().equals("+"));
            pg.setReverse(tmp[headers.indexOf("Reverse")].trim().equals("+"));
            double[] lfqIntensityArr = Arrays.stream(lfqIndexArr)
                    .mapToDouble((int index) -> Double.parseDouble(tmp[index]))
                    .toArray();
            pg.setLfqIntensityArr(lfqIntensityArr);
            return pg;
        };
        return fr.read(rowDeleter, rowProcessor);
    }
}
