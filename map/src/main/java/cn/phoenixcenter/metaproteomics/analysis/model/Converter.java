package cn.phoenixcenter.metaproteomics.analysis.model;

import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Converter {

    public static String getPGHeader(List<String> lfqTagList) {
        return String.join("\t",
                "Majority protein IDs",
                "Razor + unique peptides",
                "Only identified by site",
                "Reverse",
                "Potential contaminant",
                lfqTagList.stream()
                        .collect(Collectors.joining("\t"))

        );
    }

    public static final Function<MQProteinGroup, String> PG_TO_STR =
            (MQProteinGroup pg) ->
                    String.join("\t",
                            Arrays.stream(pg.getMajorityProteinIds())
                                    .collect(Collectors.joining(";")),
                            String.valueOf(pg.getRazorPlusUniquePeptides()),
                            pg.isOnlyIdentifiedBySite() ? "+" : "",
                            pg.isReverse() ? "+" : "",
                            pg.isPotentialContaminant() ? "+" : "",
                            Arrays.stream(pg.getLfqIntensityArr())
                                    .mapToObj(String::valueOf)
                                    .collect(Collectors.joining("\t"))
                    );

    public static final Function<String, MQProteinGroup> STR_TO_PG =
            (String pgStr) -> {
                MQProteinGroup pg = new MQProteinGroup();
                String[] tmp = pgStr.split("\t");
                pg.setMajorityProteinIds(tmp[0].split(";"));
                pg.setRazorPlusUniquePeptides(Integer.parseInt(tmp[1]));
                pg.setOnlyIdentifiedBySite(tmp[2].equals("+"));
                pg.setReverse(tmp[3].equals("+"));
                pg.setPotentialContaminant(tmp[3].equals("+"));
                return pg;
            };

    public static String getTaxonHeader() {
        String rankStr = Arrays.stream(Taxon.Rank.values())
                .map(r -> r.rankToString())
                .collect(Collectors.joining("\t"));
        return String.join("\t",
                "LCA taxon id",
                "LCA taxon name",
                "LCA rank",
                rankStr,
                rankStr
        );
    }

    public static final Function<Taxon, String> TAXON_TO_STR =
            (Taxon t) -> {
                Taxon.Rank[] ranks = Taxon.Rank.values();
                String tIds = "";
                String tNames = "";
                for (Taxon.Rank r : ranks) {
                    if (t.getLineageIdMap().containsKey(r.rankToString())) {
                        tIds += t.getLineageIdMap().get(r.rankToString());
                        tNames += t.getLineageNameMap().get(r.rankToString());
                    } else {
                        tIds += "-";
                        tNames += "-";
                    }
                }
                return String.join("\t",
                        String.valueOf(t.getId()),
                        t.getName(),
                        t.getRank(),
                        tNames,
                        tIds
                );
            };

    public static final Function<String, Taxon> STR_TO_TAXON =
            (String str) -> {
                Taxon t = new Taxon();
                String[] tmp = str.split("\t");
                t.setId(Integer.parseInt(tmp[0]));
                t.setName(tmp[1]);
                t.setRank(tmp[2]);
                Taxon.Rank[] ranks = Taxon.Rank.values();
                Map<String, Integer> lineageIdMap = new HashMap<>(ranks.length);
                Map<String, String> lineageNameMap = new HashMap<>(ranks.length);
                for (int i = 3, j = 0; j < ranks.length; i++, j++) {
                    if (!tmp[i].equals("-")) {
                        lineageNameMap.put(ranks[j].rankToString(), tmp[i]);
                        lineageIdMap.put(ranks[j].rankToString(), Integer.parseInt(tmp[i + ranks.length]));
                    }
                }
                return t;
            };

    public static String getPGExHeader(List<String> lfqTagList) {
        return String.join("\t",
                getPGHeader(lfqTagList),
                "Majority protein taxon IDs",
                getTaxonHeader()
        );
    }

    public static final Function<MQProteinGroupEx, String> PGEX_TO_STR =
            (MQProteinGroupEx pgEx) -> String.join("\t",
                    PG_TO_STR.apply(pgEx.getMqProteinGroup()),
                    Arrays.stream(pgEx.getMajorityProteinTaxonIds())
                            .mapToObj(String::valueOf)
                            .collect(Collectors.joining(";")),
                    TAXON_TO_STR.apply(pgEx.getMajorityProteinTaxonLCA())
            );

    public static final Function<String, MQProteinGroupEx> STR_TO_PGEX =
            (String str) -> {
                MQProteinGroupEx pgEx = new MQProteinGroupEx();
                String[] tmp = str.split("\t");
                pgEx.setMqProteinGroup(STR_TO_PG.apply(tmp[0]));
                pgEx.setMajorityProteinTaxonIds(Arrays.stream(tmp[1].split(";"))
                        .mapToInt(Integer::parseInt)
                        .toArray());
                pgEx.setMajorityProteinTaxonLCA(STR_TO_TAXON.apply(tmp[2]));
                return pgEx;
            };
}
