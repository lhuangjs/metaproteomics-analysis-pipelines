//package cn.phoenixcenter.metaproteomics.analysis.taxonomy;
//
//import cn.phoenixcenter.metaproteomics.taxonomy.LCAAnalysis;
//import cn.phoenixcenter.metaproteomics.taxonomy.Taxonomy;
//import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
//
//import java.utils.HashMap;
//import java.utils.Map;
//
///**
// *
// */
//public class ProteinCentricMethod {
//
//    /**
//     * Calculate LCA for each protein groups.
//     *
//     * @param taxonIdGroupArr 1-dim: each row represent a protein group; 2-dim: the taxon ids of members in a protein group
//     */
//    public static Taxon[] calculateLCA(int[][] taxonIdGroupArr) {
//        Taxon[] lcaArr = new Taxon[taxonIdGroupArr.length];
//        // obtain each protein group's LCA
//        for (int i = 0; i < taxonIdGroupArr.length; i++) {
//            lcaArr[i] = LCAAnalysis.calculateLCA(taxonIdGroupArr[i]);
//        }
//        return lcaArr;
//    }
//
//
//    /**
//     * Only remain entries whose LCA are not above specific rank
//     * and those entries whose LCA are above specific rank will be assigned null.
//     * <PRE>
//     * [genus1, phylum1, species1] --specific rank: genus--> [genus1, null, species1]
//     * </PRE>
//     *
//     * @param lcaArr
//     * @param specificRank
//     * @return
//     */
//    public static Taxon[] filterByLCA(Taxon[] lcaArr, Taxon.Rank specificRank) {
//        Taxon[] filteredLcaArr = new Taxon[lcaArr.length];
//        for (int i = 0; i < lcaArr.length; i++) {
//            if (lcaArr[i] != null) {
//                filteredLcaArr[i] = Taxon.Rank.stringToRank(lcaArr[i].getRank()).index() >= specificRank.index()
//                        ? lcaArr[i] : null;
//            } else {
//                filteredLcaArr[i] = null;
//            }
//        }
//        return filteredLcaArr;
////        return Arrays.stream(lcaArr)
////                .map((Taxon t) -> Taxon.Rank.stringToRank(t.getRank()).index() >= specificRank.index() ? t : null)
////                .toArray(Taxon[]::new);
//
//    }
//
//    public static Map<Taxon, double[]> LFQIntensityQuantification(double[][] intensityGroupArr,
//                                                                  Taxon[] filteredLCA,
//                                                                  Taxon.Rank specificRank) {
//        // taxon on specific rank => samples LFQ intensity arr
//        Map<Taxon, double[]> taxonIntensityMap = new HashMap();
//        for (int i = 0; i < filteredLCA.length; i++) {
//            if (filteredLCA[i] != null) {
//                double[] intensityGroup = intensityGroupArr[i];
//                // obtain taxon on the specific rank(some LCA rank is below specific rank)
//                // FIXME rank: String->Rank, equals; filteredLCA[i].getLineageIdMap().get
//                Taxon taxon = filteredLCA[i].getRank().equals(specificRank.rankToString())
//                        ? filteredLCA[i]
//                        : Taxonomy.getTaxonById(filteredLCA[i].getLineageIdMap().get(specificRank.rankToString()));
//                // sum up the LFQ intensity values belong to the same taxon
//                if (taxonIntensityMap.containsKey(taxon)) {
//                    taxonIntensityMap.replace(taxon,
//                            matrixPlus(taxonIntensityMap.get(taxon), intensityGroup));
//                } else {
//                    taxonIntensityMap.put(taxon, intensityGroup);
//                }
//            }
//        }
//
//        // calculate taxon distribution
//        double[] sum = taxonIntensityMap.values().stream()
//                .reduce((double[] matrix1, double[] matrix2) -> matrixPlus(matrix1, matrix2))
//                .get();
//        for (Taxon taxon : taxonIntensityMap.keySet()) {
//            // taxon intensity for each sample ./ total intensity for each sample * 100
//            taxonIntensityMap.replace(taxon, numberMultiply(100.0,
//                    dotDivision(taxonIntensityMap.get(taxon), sum)));
//        }
//        return taxonIntensityMap;
//    }
//
//    /**
//     * matrix1 + matrix2
//     *
//     * @param matrix1
//     * @param matrix2
//     * @return
//     */
//    public static double[] matrixPlus(double[] matrix1, double[] matrix2) {
//        if (matrix1.length != matrix2.length) {
//            throw new RuntimeException("matrix1's length must equate matrix2's, but "
//                    + matrix1.length + " != " + matrix2.length);
//        }
//        double[] result = new double[matrix1.length];
//        for (int i = 0; i < matrix1.length; i++) {
//            result[i] = matrix1[i] + matrix2[i];
//        }
//        return result;
//    }
//
//    /**
//     * matrix1 - matrix2
//     *
//     * @param matrix1
//     * @param matrix2
//     * @return
//     */
//    public static double[] matrixSubtraction(double[] matrix1, double[] matrix2) {
//        if (matrix1.length != matrix2.length) {
//            throw new RuntimeException("matrix1's length must equate matrix2's, but "
//                    + matrix1.length + " != " + matrix2.length);
//        }
//        double[] result = new double[matrix1.length];
//        for (int i = 0; i < matrix1.length; i++) {
//            result[i] = matrix1[i] - matrix2[i];
//        }
//        return result;
//    }
//
//    /**
//     * matrix1 ./ matrix2
//     *
//     * @param matrix1
//     * @param matrix2
//     * @return
//     */
//    public static double[] dotDivision(double[] matrix1, double[] matrix2) {
//        if (matrix1.length != matrix2.length) {
//            throw new RuntimeException("matrix1's length must equate matrix2's, but "
//                    + matrix1.length + " != " + matrix2.length);
//        }
//        double[] result = new double[matrix1.length];
//        for (int i = 0; i < matrix1.length; i++) {
//            result[i] = matrix1[i] / matrix2[i];
//        }
//        return result;
//    }
//
//    /**
//     * num * matrix
//     *
//     * @param num
//     * @param matrix
//     * @return
//     */
//    public static double[] numberMultiply(double num, double[] matrix) {
//        double[] result = new double[matrix.length];
//        for (int i = 0; i < matrix.length; i++) {
//            result[i] = num * matrix[i];
//        }
//        return result;
//    }
//}