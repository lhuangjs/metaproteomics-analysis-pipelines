package cn.phoenixcenter.metaproteomics.analysis.model;


import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class MQPeptideEx {

    private MQPeptide mqPeptide;

    private int[] proteinsTaxonIds;

    private Taxon proteinsLCA;
}
