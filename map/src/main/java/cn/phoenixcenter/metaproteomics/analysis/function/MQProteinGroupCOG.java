package cn.phoenixcenter.metaproteomics.analysis.function;

import cn.phoenixcenter.metaproteomics.analysis.model.MQProteinGroup;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class MQProteinGroupCOG {
    private MQProteinGroup mqProteinGroup;

    private COG[] majorityProCOGs;

    private COG leadingCOG;

    public void generateLeadingCOG() {
        Optional<COG> optional = Arrays.stream(majorityProCOGs)
                .filter(cog -> cog != null)
                .findFirst();

        leadingCOG = optional.isPresent()
                ? optional.get()
                : null;
    }

    public String fPrint() {
        if (majorityProCOGs != null) {
            long uniCount = Arrays.stream(majorityProCOGs)
                    .filter(cog -> cog != null)
                    .distinct()
                    .count();
            String majorityProCOGsStr = null;
            switch ((int) uniCount) {
                case 0:
                    majorityProCOGsStr = "";
                    break;
                case 1:
                    majorityProCOGsStr = Arrays.stream(majorityProCOGs)
                            .filter(cog -> cog != null)
                            .distinct()
                            .map(cog -> String.join(" | ",
                                    cog.getId(),
                                    cog.getFunctionClass(),
                                    cog.getAnnotation()))
                            .findFirst()
                            .get();
                    break;
                default:
                    majorityProCOGsStr = Arrays.stream(majorityProCOGs)
                            .map(cog -> cog == null
                                    ? ""
                                    : String.join(" | ",
                                    cog.getId(),
                                    cog.getFunctionClass(),
                                    cog.getAnnotation())
                            )
                            .collect(Collectors.joining(";"));

            }
            return String.join("\t",
                    mqProteinGroup.fPrint(),
                    leadingCOG.fPrint(),
                    majorityProCOGsStr,
                    String.valueOf(uniCount)
            );
        } else {
            return String.join("\t",
                    mqProteinGroup.fPrint(),
                    "",
                    "",
                    "",
                    "",
                    String.valueOf(0)
            );
        }

    }
}