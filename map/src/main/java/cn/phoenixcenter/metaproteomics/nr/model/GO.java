package cn.phoenixcenter.metaproteomics.nr.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.*;

import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GO {

    protected String id;

    protected String term;

    protected Category category;

    public enum Category {
        P("Biological Process"),
        F("Molecular Function"),
        C("Cellular Component");

        String category;

        Category(String category) {
            this.category = category;
        }

        @Override
        public String toString() {
            return category;
        }

        @JsonCreator
        public static Category toCategory(String name) {
            for (Category c : Category.values()) {
                if (c.name().equals(name)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(name);
        }

        @JsonValue
        public String toText() {
            return category;
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GO go = (GO) o;
        return Objects.equals(id, go.id) &&
                Objects.equals(term, go.term) &&
                Objects.equals(category, go.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, term, category);
    }
}
