package cn.phoenixcenter.metaproteomics.nr;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.nr.model.GO;
import cn.phoenixcenter.metaproteomics.nr.model.GOWithPerc;
import cn.phoenixcenter.metaproteomics.nr.model.NRProtein;
import cn.phoenixcenter.metaproteomics.nr.model.TrypticPeptide;
import cn.phoenixcenter.metaproteomics.taxonomy.LCAAnalysis;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import cn.phoenixcenter.metaproteomics.taxonomy.TaxonSearcher;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Log4j2
@Setter
@Getter
public class NRSearcher {

    private TransportClient esClient;

    private String nrIndexName;

    private int scrollSize;

    private TimeValue scrollKeepAliveTime;

    // the size per phrase query
    private int multiPhraseQuerySize;

    private int multiTermQuerySize;

    // the number of per terms query.
    // The value need to less than index.max_terms_count(es default value is 65536).
    private int termsQuerySize;

    private TaxonSearcher taxonSearcher;

    private int maxParallelTask;

    private ExecutorService peptSearchThreadPool;

    private Semaphore peptSearchSemaphore;

    private Semaphore jsonWriteSemaphore;

    private JsonFactory jsonFactory;

    public NRSearcher() {
        maxParallelTask = GlobalConfig.getValueAsInt("nr-searcher.max.parallel.task");
        BlockingQueue<Runnable> taskQueue = new LinkedBlockingQueue<>(maxParallelTask);
        peptSearchThreadPool = new ThreadPoolExecutor(
                maxParallelTask,
                maxParallelTask,
                0,
                TimeUnit.MILLISECONDS,
                taskQueue
        );
        peptSearchSemaphore = new Semaphore(maxParallelTask);
        jsonWriteSemaphore = new Semaphore(1);
        jsonFactory = new ObjectMapper().getFactory();
    }

    public NRSearcher(TransportClient esClient, String nrIndexName,
                      int scrollSize, TimeValue scrollKeepAliveTime,
                      int multiPhraseQuerySize, int multiTermQuerySize,
                      int termsQuerySize,
                      TaxonSearcher taxonSearcher) {
        this();
        this.esClient = esClient;
        this.nrIndexName = nrIndexName;
        this.scrollSize = scrollSize;
        this.scrollKeepAliveTime = scrollKeepAliveTime;
        this.multiPhraseQuerySize = multiPhraseQuerySize;
        this.multiTermQuerySize = multiTermQuerySize;
        this.termsQuerySize = termsQuerySize;
        this.taxonSearcher = taxonSearcher;
    }

    /**
     * Verify whether the peptide sequence can be search.
     *
     * @param peptide
     * @return
     */
    public static boolean verify(String peptide) {
        return peptide.length() >= 5 && peptide.length() <= 70;
    }

    /**
     * Obtain proteins info matching the input peptides and the result has same order with the input
     * <p>
     * If peptide cannot match any entries , only the field sequence for TrypticPeptide will be set.
     *
     * @param peptideSet
     * @param equateIL
     * @param consumer
     * @return
     * @throws InterruptedException
     * @throws IOException
     */
    public void pepinfo(Set<String> peptideSet,
                        boolean equateIL,
                        Consumer<TrypticPeptide> consumer) throws InterruptedException, IOException {
        Set<TrypticPeptide> trypticPeptideSet = peptideSet.stream()
                .map(seq -> new TrypticPeptide(seq))
                .collect(Collectors.toSet());
        addLCAInfo(trypticPeptideSet, equateIL, consumer);
    }

    /**
     * Search proteins containing the specific peptides and calculate LCA.
     * If a peptide doesn't match any protein in nr, the null will be set for fields "lca" and "nrProteinSet"
     *
     * @param trypticPeptSet
     * @param equateIL
     */
    public void addLCAInfo(Set<TrypticPeptide> trypticPeptSet,
                           boolean equateIL,
                           Consumer<TrypticPeptide> consumer) throws InterruptedException, IOException {
        if (!trypticPeptSet.parallelStream().map(TrypticPeptide::getSequence).allMatch(NRSearcher::verify)) {
            throw new IllegalArgumentException("The input peptide length must belong [5, 70]. " +
                    "You can filter input peptide by NRSearcher.verify(peptide) before use the method");
        }
        log.info("input peptide list size: {}", trypticPeptSet.size());

        /** retrieve **/
        // group: true - does not exists missed cleavage; false - exist missed cleavage
        Map<Boolean, List<TrypticPeptide>> peptideGroupMap =
                trypticPeptSet.parallelStream()
                        .collect(Collectors.groupingBy(
                                pept -> Boolean.valueOf(pept.getSequence().split("(?<=[KR])(?!P)").length == 1)
                        ));
        // retrieve and save into temporary file
        Path tmpSearchResultPath = Files.createTempFile("nr-search", ".json");
        log.debug("Create temporary file <{}> to save nr-search result", tmpSearchResultPath);
        JsonGenerator jsonGen = jsonFactory.createGenerator(tmpSearchResultPath.toFile(), JsonEncoding.UTF8);
        jsonGen.writeStartArray();
        if (peptideGroupMap.containsKey(false)) {
            log.info("total {} phrase query", peptideGroupMap.get(false).size());
            long startTime = System.currentTimeMillis();
            pept2ProtWithMC(peptideGroupMap.get(false), equateIL, jsonGen);
            // wait for finishing all task
            peptSearchSemaphore.acquire(maxParallelTask);
            peptSearchSemaphore.release(maxParallelTask);
            log.info("phrase query size: {}; time consuming: {}s", peptideGroupMap.get(false).size(),
                    ((System.currentTimeMillis() - startTime) / 1000.0));
        }

        if (peptideGroupMap.containsKey(true)) {
            log.info("total {} term queries", peptideGroupMap.get(true).size());
            long startTime = System.currentTimeMillis();
            pept2ProtWithoutMC(peptideGroupMap.get(true), equateIL, jsonGen);
            // wait for finishing all task
            peptSearchSemaphore.acquire(maxParallelTask);
            peptSearchSemaphore.release(maxParallelTask);
            log.info("term query size: {}; time consuming: {} s",
                    peptideGroupMap.get(true).size(),
                    ((System.currentTimeMillis() - startTime) / 1000.0));
        }
        jsonWriteSemaphore.acquire();
        jsonGen.writeEndArray();
        jsonGen.close();
        jsonWriteSemaphore.release();
        // parse temporary file
        JsonParser jsonParser = jsonFactory.createParser(tmpSearchResultPath.toFile());
        jsonParser.nextToken(); // [
        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
            TrypticPeptide trypticPeptide = new TrypticPeptide();
            jsonParser.nextToken(); // sequence field
            jsonParser.nextToken();// sequence value
            trypticPeptide.setSequence(jsonParser.getValueAsString());
            jsonParser.nextToken(); // nrProteinSet field
            if (jsonParser.nextToken() != JsonToken.VALUE_NULL) { // [ or null
                while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                    NRProtein prot = jsonParser.readValueAs(NRProtein.class);
                    if (trypticPeptide.getNrProteinSet() == null) {
                        trypticPeptide.setNrProteinSet(new HashSet<>());
                    }
                    trypticPeptide.getNrProteinSet().add(prot);
                }
            }
            jsonParser.nextToken(); // quantVals field
            jsonParser.nextToken(); // quantVals value
            double[] quantVals = jsonParser.readValueAs(double[].class);
            trypticPeptide.setQuantVals(quantVals);
            jsonParser.nextToken(); // }

            /** calculate LCA and GO info **/
            if (trypticPeptide.getNrProteinSet() != null) {
                // obtain taxonomy infos of proteins matching the peptide, and then calculate LCA
                // obtain proteins' taxon ids
                Set<Integer> tidSet = trypticPeptide.getNrProteinSet().stream()
                        .map(NRProtein::getTid)
                        .collect(Collectors.toSet());
                Map<Integer, Taxon> taxonMap = taxonSearcher.getTaxaByIds(tidSet);
                // calculate LCA using unipept algorithm
                trypticPeptide.setLca(taxonSearcher.getTaxonById(LCAAnalysis.calculateUnipeptLCA(taxonMap, tidSet)));

                // add GO function count(TrypticPeptide.go2ProtCount)
                int totalProtCount = 0;
                Map<GO, Integer> go2ProtCount = null;
                for (NRProtein prot : trypticPeptide.getNrProteinSet()) {
                    if (prot.getGoArr() != null) {
                        totalProtCount++;
                        if (go2ProtCount == null) {
                            go2ProtCount = new HashMap<>();
                        }
                        for (GO go : prot.getGoArr()) {
                            go2ProtCount.computeIfPresent(go, (key, val) -> val + 1);
                            go2ProtCount.putIfAbsent(go, 1);
                        }
                    }
                }
                if (go2ProtCount != null) {
                    List<GOWithPerc> goWithPercList = new ArrayList<>(go2ProtCount.size());
                    for (Map.Entry<GO, Integer> e : go2ProtCount.entrySet()) {
                        GOWithPerc goWithPerc = new GOWithPerc(e.getKey());
                        goWithPerc.setProtPerc(e.getValue() / (double) totalProtCount);
                        goWithPercList.add(goWithPerc);
                    }
                    trypticPeptide.setGoWithPercList(goWithPercList);
                }
            }
            consumer.accept(trypticPeptide);
        }
        jsonParser.close();
        Files.delete(tmpSearchResultPath);
        log.debug("Delete temporary file <{}> to save nr-search result", tmpSearchResultPath);
    }

    /**
     * Execute "match_phrase" query when there is missed cleavage in peptide list.
     * equalIL is true => retrieve against seq.eqILSeq
     * equalIL is false => retrieve against seq
     *
     * @param trypticPeptList
     * @param equateIL
     * @return
     */
    private void pept2ProtWithMC(List<TrypticPeptide> trypticPeptList,
                                 boolean equateIL,
                                 JsonGenerator jsonGen) throws InterruptedException {
        // split search request
        int querySize = trypticPeptList.size();
        int start = 0;
        int end = start + multiPhraseQuerySize > querySize
                ? querySize
                : start + multiPhraseQuerySize;
        int times = querySize % multiPhraseQuerySize == 0
                ? querySize / multiPhraseQuerySize
                : querySize / multiPhraseQuerySize + 1;
        for (int i = 0; i < times; i++) {
            // create multi search (phrase query)
            MultiSearchRequestBuilder multiRequest = esClient.prepareMultiSearch();
            final List<TrypticPeptide> subPeptideList = trypticPeptList.subList(start, end);
            for (TrypticPeptide pept : subPeptideList) {
                multiRequest.add(
                        esClient.prepareSearch(nrIndexName)
                                .addSort(FieldSortBuilder.DOC_FIELD_NAME, SortOrder.ASC)
                                .setScroll(scrollKeepAliveTime)
                                .setQuery(QueryBuilders.matchPhraseQuery(equateIL
                                                ? "seq.eqILSeq" : "seq",
                                        pept.getSequence()))
                                .setFetchSource(null, new String[]{"seq"})
                                .setSize(scrollSize)
                );
            }
            final MultiSearchResponse.Item[] items = multiRequest.get().getResponses();
            peptSearchSemaphore.acquire();
            Runnable task = new ResponseParser(i, times, items, subPeptideList, jsonGen);
            peptSearchThreadPool.execute(task);
            start = end;
            end = start + multiPhraseQuerySize > querySize
                    ? querySize
                    : start + multiPhraseQuerySize;
        }
    }

    /**
     * Executor "term" query when there is no missed cleavage.
     * equalIL is true => retrieve against seq.eqILSeq, and must replace "I" with "L" in all peptides before retrieving
     * equalIL is false => retrieve against seq
     *
     * @param trypticPeptList
     * @param equateIL
     * @return
     */
    private void pept2ProtWithoutMC(List<TrypticPeptide> trypticPeptList,
                                    boolean equateIL,
                                    JsonGenerator jsonGen) throws InterruptedException {
        // split search request
        int querySize = trypticPeptList.size();
        int start = 0;
        int end = start + multiTermQuerySize > querySize ? querySize : start + multiTermQuerySize;
        int times = querySize % multiTermQuerySize == 0
                ? querySize / multiTermQuerySize
                : querySize / multiTermQuerySize + 1;
        for (int i = 0; i < times; i++) {
            MultiSearchRequestBuilder multiRequest = esClient.prepareMultiSearch();
            List<TrypticPeptide> subPeptideList = trypticPeptList.subList(start, end);
            for (TrypticPeptide pept : subPeptideList) {
                multiRequest.add(
                        esClient.prepareSearch(nrIndexName)
                                .addSort(FieldSortBuilder.DOC_FIELD_NAME, SortOrder.ASC)
                                .setScroll(scrollKeepAliveTime)
                                .setQuery(QueryBuilders.termQuery(equateIL ? "seq.eqILSeq" : "seq",
                                        equateIL ? pept.getSequence().replace('I', 'L')
                                                : pept.getSequence())
                                )
                                .setFetchSource(null, new String[]{"seq"})
                                .setSize(scrollSize)
                );
            }
            // parse response
            final MultiSearchResponse.Item[] items = multiRequest.get().getResponses();
            final int k = i + 1;
            peptSearchSemaphore.acquire();
            Runnable task = new ResponseParser(i, times, items, subPeptideList, jsonGen);
            peptSearchThreadPool.execute(task);
            start = end;
            end = start + multiTermQuerySize > querySize ? querySize : start + multiTermQuerySize;
        }
    }

    @AllArgsConstructor
    private class ResponseParser implements Runnable {

        private int batchIdx;

        private int totalBatch;

        private MultiSearchResponse.Item[] items;

        private List<TrypticPeptide> subPeptideList;

        private JsonGenerator jsonGen;

        @Override
        public void run() {
            try {
                log.info("phrase query {} / {}", (batchIdx + 1), totalBatch);
                List<Map.Entry<TrypticPeptide, String[]>> pept2ProtsList = new ArrayList<>(subPeptideList.size());
                for (int j = 0; j < items.length; j++) {
                    if (items[j].getFailure() != null) {
                        throw new RuntimeException(items[j].getFailure());
                    }
                    SearchResponse response = items[j].getResponse();
                    SearchHits searchHits = response.getHits();
                    if (searchHits.getTotalHits() > 0L) {
                        String[] proteins = new String[(int) searchHits.getTotalHits()];
                        pept2ProtsList.add(new AbstractMap.SimpleEntry<>(subPeptideList.get(j), proteins));
                        int k = 0;
                        do {
                            for (SearchHit hit : searchHits.getHits()) {
                                proteins[k++] = hit.getSourceAsString();
                            }
                            response = esClient.prepareSearchScroll(response.getScrollId())
                                    .setScroll(scrollKeepAliveTime)
                                    .get();
                            searchHits = response.getHits();
                        } while (searchHits.getHits().length != 0);
                    } else {
                        pept2ProtsList.add(new AbstractMap.SimpleEntry<>(subPeptideList.get(j), null));
                    }
                }
                peptSearchSemaphore.release();
                // save into json file
                jsonWriteSemaphore.acquire();
                for (Map.Entry<TrypticPeptide, String[]> e : pept2ProtsList) {
                    jsonGen.writeStartObject();
                    TrypticPeptide pept = e.getKey();
                    jsonGen.writeStringField("sequence", pept.getSequence());
                    if (e.getValue() == null) {
                        jsonGen.writeObjectField("nrProteinSet", null);
                    } else {
                        jsonGen.writeArrayFieldStart("nrProteinSet");
                        for (String prot : e.getValue()) {
                            jsonGen.writeRawValue(prot);
                        }
                        jsonGen.writeEndArray();
                    }
                    jsonGen.writeObjectField("quantVals", pept.getQuantVals());
                    jsonGen.writeEndObject();
                    jsonGen.writeRaw(System.lineSeparator());
                }
                jsonWriteSemaphore.release();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

    }

    private NRProtein map2NRProtein(Map<String, Object> map) {
        GO[] goArr = map.containsKey("goArr")
                ? ((ArrayList<Map<String, Object>>) map.get("goArr")).stream()
                .map(goMap -> GO.builder()
                        .id((String) goMap.get("id"))
                        .category(GO.Category.toCategory((String) goMap.get("category")))
                        .term((String) goMap.get("term"))
                        .build()
                )
                .toArray(GO[]::new)
                : null;
        return NRProtein.builder()
                .pid((String) map.get("pid"))
                .tid((Integer) map.get("tid"))
                .gi((Integer) map.get("gi"))
                .uid((String) map.get("uid"))
                .goArr(goArr)
                .seq((String) map.get("seq"))
                .build();
    }

    /**
     * add taxon info for each protein and calculate LCA
     *
     * @param trypticPeptList
     * @return
     */
    private void addLCAInfo(List<TrypticPeptide> trypticPeptList) {
        trypticPeptList.parallelStream()
                .filter(pept -> pept.getNrProteinSet() != null)
                .forEach(pept -> {
                    /** obtain taxonomy infos of proteins matching the peptide, and then calculate LCA **/
                    // obtain proteins' taxon ids
                    Set<Integer> tidSet = pept.getNrProteinSet().stream()
                            .map(NRProtein::getTid)
                            .collect(Collectors.toSet());
                    Map<Integer, Taxon> taxonMap = taxonSearcher.getTaxaByIds(tidSet);
                    // calculate LCA using unipept algorithm
                    pept.setLca(taxonSearcher.getTaxonById(LCAAnalysis.calculateUnipeptLCA(taxonMap, tidSet)));
                });
    }

    /**
     * Get protein info and write into file
     *
     * @param pidSet
     * @param outputPath
     * @throws IOException
     */
    public void getProteinsByIds(Set<String> pidSet, Path outputPath) throws IOException {
        BufferedWriter bw = Files.newBufferedWriter(outputPath,
                StandardCharsets.UTF_8,
                StandardOpenOption.TRUNCATE_EXISTING,
                StandardOpenOption.CREATE);
        int start = 0;
        int end = start + termsQuerySize > pidSet.size() ? pidSet.size() : start + termsQuerySize;
        int times = pidSet.size() % termsQuerySize == 0 ? pidSet.size() / termsQuerySize : pidSet.size() / termsQuerySize + 1;
        List<String> pidList = new ArrayList(pidSet);
        for (int i = 0; i < times; i++) {
            getProteinsByIds(pidList, start, end, bw);
            start = end;
            end = start + termsQuerySize > pidSet.size() ? pidSet.size() : start + termsQuerySize;
            log.info("finish {} / {}", i + 1, times);
        }
        bw.close();
    }

    private void getProteinsByIds(List<String> pidList, int start, int end, BufferedWriter bw) throws IOException {

        /** build search request **/
        SearchRequestBuilder searchRequestBuilder = esClient.prepareSearch(nrIndexName)
                .addSort(FieldSortBuilder.DOC_FIELD_NAME, SortOrder.ASC)
                .setScroll(scrollKeepAliveTime)
                .setSize(scrollSize)
                .setQuery(QueryBuilders.termsQuery("pid", pidList.subList(start, end)));

        /** parse search response and write into file **/
        SearchResponse searchResponse = searchRequestBuilder.get();
        while (searchResponse.getHits().getHits().length != 0) {
            SearchHit[] hits = searchResponse.getHits().getHits();
            List<Map<String, Object>> nrProteinList = new ArrayList<>(end - start);
            // parse search response
            for (SearchHit hit : hits) {
                nrProteinList.add(hit.getSourceAsMap());
            }
            // write
            for (Map<String, Object> prot : nrProteinList) {
                writeFasta(prot, bw);
            }
            searchResponse = esClient.prepareSearchScroll(searchResponse.getScrollId())
                    .setScroll(scrollKeepAliveTime).get();
        }
    }

    /**
     * Obtain fasta-formatted proteins for the specified taxon id.
     * <p>
     * Note: don't contain sequences belonging to the specified taxon.
     *
     * @param taxon
     * @param outputPath
     * @throws IOException
     */
    public void getProteinsByTaxon(Taxon taxon, Path outputPath) throws IOException {
        BufferedWriter bw = Files.newBufferedWriter(outputPath, StandardCharsets.UTF_8,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        long counter = 0;
        SearchRequestBuilder searchRequest = esClient.prepareSearch(nrIndexName)
                .addSort(FieldSortBuilder.DOC_FIELD_NAME, SortOrder.ASC)
                .setScroll(scrollKeepAliveTime)
                .setSize(scrollSize)
                .setQuery(QueryBuilders.termQuery("tid", taxon.getId()));
        SearchResponse searchResponse = searchRequest.get();
        while (searchResponse.getHits().getHits().length != 0) {
            SearchHit[] hits = searchResponse.getHits().getHits();
            for (int j = 0; j < hits.length; j++) {
                Map<String, Object> prot = hits[j].getSourceAsMap();
                // write
                writeFasta(prot, bw);
                counter++;
            }
            searchResponse = esClient.prepareSearchScroll(searchResponse.getScrollId())
                    .setScroll(scrollKeepAliveTime)
                    .get();
        }
        log.info("find total {} protein sequences for {}", counter, taxon.getId());
        bw.close();
    }

    /**
     * Obtain fasta-formatted proteins for the specified taxon set.
     *
     * @param taxonSet
     * @param outputPath
     * @throws IOException
     */
    public void getProteinsByTaxa(Set<Taxon> taxonSet, Path outputPath, BiConsumer<Taxon, Long> countConsumer) throws IOException {
        BufferedWriter bw = Files.newBufferedWriter(outputPath, StandardCharsets.UTF_8,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        for (Taxon taxon : taxonSet) {
            SearchRequestBuilder searchRequest = esClient.prepareSearch(nrIndexName)
                    .addSort(FieldSortBuilder.DOC_FIELD_NAME, SortOrder.ASC)
                    .setScroll(scrollKeepAliveTime)
                    .setSize(scrollSize)
                    .setQuery(QueryBuilders.termQuery("tid", taxon.getId()));
            SearchResponse searchResponse = searchRequest.get();
            log.info("find total {} protein sequences for {}[{}]", searchResponse.getHits().getTotalHits(), taxon.getName(), taxon.getId());
            while (searchResponse.getHits().getHits().length != 0) {
                SearchHit[] hits = searchResponse.getHits().getHits();
                for (int j = 0; j < hits.length; j++) {
                    Map<String, Object> prot = hits[j].getSourceAsMap();
                    // write
                    writeFasta(prot, bw);
                }
                searchResponse = esClient.prepareSearchScroll(searchResponse.getScrollId())
                        .setScroll(scrollKeepAliveTime)
                        .get();
            }
            if (countConsumer != null) {
                countConsumer.accept(taxon, searchResponse.getHits().getTotalHits());
            }
        }
        bw.close();
    }

    private void writeFasta(Map<String, Object> prot, BufferedWriter bw) throws IOException {
        Taxon taxon = taxonSearcher.getTaxonById((int) (prot.get("tid")));
        StringBuffer annotation = new StringBuffer(">");
        annotation.append(prot.get("pid") + " ");
        XContentBuilder builder = XContentFactory.jsonBuilder()
                .startObject()
                .field("id", prot.get("pid"))
                .field("gi", prot.get("gi"))
                .startObject("taxon")
                .field("id", taxon.getId())
                .field("name", taxon.getName())
                .field("rank", taxon.getRank())
                .endObject();
        if (prot.containsKey("goArr")) {
            builder.startArray("goArr");
            List<Map<String, Object>> goArr = (List<Map<String, Object>>) (prot.get("goArr"));
            for (Map<String, Object> go : goArr) {
                builder.startObject();
                builder.field("id", go.get("id"));
                builder.field("category", GO.Category.toCategory((String) go.get("category")).toText());
                builder.endObject();
            }
            builder.endArray();
        }
        builder.endObject();
        annotation.append(Strings.toString(builder));
        bw.write(annotation.toString() + System.lineSeparator());
        bw.write(Arrays.stream(((String) prot.get("seq")).split("(?<=\\G.{80})"))
                .collect(Collectors.joining(System.lineSeparator()))
                + System.lineSeparator());
    }

    /**
     * Add GO function count(TrypticPeptide.go2ProtCount)
     *
     * @param trypticPeptList
     */
    public void addFunctionCount(List<TrypticPeptide> trypticPeptList) {
        // GO => the number of proteins contain the GO term for a peptide
        trypticPeptList.parallelStream()
                .forEach(pept -> {
                            if (pept.getNrProteinSet() != null) {
                                int totalProtCount = 0;
                                Map<GO, Integer> go2ProtCount = null;
                                for (NRProtein prot : pept.getNrProteinSet()) {
                                    if (prot.getGoArr() != null) {
                                        totalProtCount++;
                                        if (go2ProtCount == null) {
                                            go2ProtCount = new HashMap<>();
                                        }
                                        for (GO go : prot.getGoArr()) {
                                            go2ProtCount.computeIfPresent(go, (key, val) -> val + 1);
                                            go2ProtCount.putIfAbsent(go, 1);
                                        }
                                    }
                                }
                                if (go2ProtCount != null) {
                                    List<GOWithPerc> goWithPercList = new ArrayList<>(go2ProtCount.size());
                                    for (Map.Entry<GO, Integer> e : go2ProtCount.entrySet()) {
                                        GOWithPerc goWithPerc = new GOWithPerc(e.getKey());
                                        goWithPerc.setProtPerc(e.getValue() / (double) totalProtCount);
                                        goWithPercList.add(goWithPerc);
                                    }
                                    pept.setGoWithPercList(goWithPercList);
                                }
                            }
                        }
                );
    }
}
