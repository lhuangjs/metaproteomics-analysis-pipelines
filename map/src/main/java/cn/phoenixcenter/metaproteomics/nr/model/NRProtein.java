package cn.phoenixcenter.metaproteomics.nr.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NRProtein {

    // protein accession number
    private String pid;

    // taxon id
    private Integer tid;

    // gene number
    private Integer gi;

    // uniprot id
    private String uid;

    // delete
    private GO[] goArr;

    private String seq;

    /**
     * Add sequence fragment
     *
     * @param seq
     */
    public void addSeq(String seq) {
        if (this.seq == null) {
            this.seq = seq;
        } else {
            this.seq += seq;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NRProtein nrProtein = (NRProtein) o;
        return Objects.equals(pid, nrProtein.pid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pid);
    }
}
