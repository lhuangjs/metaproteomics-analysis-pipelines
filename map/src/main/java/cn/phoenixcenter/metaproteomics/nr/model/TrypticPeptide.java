package cn.phoenixcenter.metaproteomics.nr.model;

import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrypticPeptide {

    private String sequence;

    // protein entries that match the peptide
    private Set<NRProtein> nrProteinSet;

    private Taxon lca;

    // quantitative values
    private double[] quantVals;

    private List<GOWithPerc> goWithPercList;

    public TrypticPeptide(String sequence) {
        this.sequence = sequence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TrypticPeptide that = (TrypticPeptide) o;
        return Objects.equals(sequence, that.sequence);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sequence);
    }
}
