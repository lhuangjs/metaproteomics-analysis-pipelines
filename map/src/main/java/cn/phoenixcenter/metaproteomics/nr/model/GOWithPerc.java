package cn.phoenixcenter.metaproteomics.nr.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class GOWithPerc extends GO {

    // the percentage of proteins belong to the GO
    private double protPerc;

    public GOWithPerc(GO go) {
        this.id = go.id;
        this.term = go.term;
        this.category = go.category;
    }
}
