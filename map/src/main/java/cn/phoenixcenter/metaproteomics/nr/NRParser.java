package cn.phoenixcenter.metaproteomics.nr;

import cn.phoenixcenter.metaproteomics.nr.model.GO;
import cn.phoenixcenter.metaproteomics.utils.ESUtil;
import cn.phoenixcenter.metaproteomics.utils.FileReaderUtil;
import cn.phoenixcenter.metaproteomics.utils.model.ESIndex;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.*;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryAction;
import org.elasticsearch.index.reindex.DeleteByQueryRequestBuilder;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

@Log4j2
@Setter
@Getter
@NoArgsConstructor
@RequiredArgsConstructor
public class NRParser {

    @NonNull
    private TransportClient esClient;

    private ObjectMapper objectMapper = new ObjectMapper();

    private final InputStream nrIndexConfStream = NRParser.class.getResourceAsStream("/es_mappings/nr.json");

    /**
     * Parse nr and  write into gz files and name file as "nr-shard_id.json.gz".
     * Meanwhile, entries will be ignored whose protein ids are not in pid2tid file
     * and those ignored entries will be wrote into mismatchedProFastaFile.
     * In addition, some entries will be wrote into protWithoutTaxonJsonFile whose taxon ids are not in names.dmp.
     * <p>
     * The result file is non-standard json file
     * and each protein info is saved as json string on one line, looks like,
     * <PRE>
     * {"pid": "XXX", "seq": "XXX", "tid": 123, "gi": 124, "giArr": [{"id": "XXX", "term": "XXX", "category": "XXX"}]}
     * {"pid": "XXX", "seq": "XXX", "tid": 123, "gi": 124}
     * </PRE>
     *
     * @param nrShardNum
     * @param nrGZFile
     * @param pid2TidGZFiles
     * @param gene2GOGZFile
     * @param namesDmpFile
     * @param tempDir              store temporary files and dirs
     * @param missedProtFile
     * @param protNoTidFile
     * @param tidNotInNamesDmpFile
     * @param nrShardsDir
     * @throws IOException
     */
    public void parseNR(int nrShardNum,
                        String nrGZFile,
                        String[] pid2TidGZFiles,
                        String gene2GOGZFile,
                        String namesDmpFile,
                        String tempDir,
                        String missedProtFile,
                        String protNoTidFile,
                        String tidNotInNamesDmpFile,
                        String nrShardsDir
    ) throws IOException {


        /** split nr and pid2tid gz file **/
        log.info("Start to split pid2tid gz file");
        Path pid2TidShardsPath = splitPid2TidGZFile(tempDir, nrShardNum, pid2TidGZFiles);
        log.info("Start to split nr gz file");
        Path nrShardsPath = splitNR(tempDir, nrShardNum, nrGZFile);

        /** parse and write info into json **/
        parseNRInfo(pid2TidShardsPath, nrShardsPath, 0, nrShardNum, gene2GOGZFile, namesDmpFile, protNoTidFile,
                missedProtFile, tidNotInNamesDmpFile, nrShardsDir);
    }

    /**
     * Parse pid2Tid and nr shards, then write info into json file.
     *
     * @param pid2TidShardsPath
     * @param nrShardsPath
     * @param startShard           the start shard number, 0-based system.
     * @param nrShardNum           the total shard numbers
     * @param gene2GOGZFile
     * @param namesDmpFile
     * @param protNoTidFile        store protein whose taxon ids are in nr(pid2Tid) not in names.dmp.
     *                             the file need to update after getting taxon info from ncbi
     * @param missedProtFile       the protein in nr but not in pid2Tid file
     * @param tidNotInNamesDmpFile all taxon ids are in nr(pid2Tid) not in names.dmp.
     *                             The file is used to search in ncbi:
     *                             https://www.ncbi.nlm.nih.gov/Taxonomy/TaxIdentifier/tax_identifier.cgi
     * @param nrShardsDir
     * @throws IOException
     */
    public void parseNRInfo(Path pid2TidShardsPath,
                            Path nrShardsPath,
                            int startShard,
                            int nrShardNum,
                            String gene2GOGZFile,
                            String namesDmpFile,
                            String protNoTidFile,
                            String missedProtFile,
                            String tidNotInNamesDmpFile,
                            String nrShardsDir) throws IOException {
        Path nrShardsDirPath = Paths.get(nrShardsDir);
        if (Files.notExists(nrShardsDirPath)) {
            Files.createDirectories(nrShardsDirPath);
        }
        JsonFactory jsonFactory = new JsonFactory();
        BufferedWriter misWriter = Files.newBufferedWriter(Paths.get(missedProtFile), StandardCharsets.UTF_8,
                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
        // get gi2GOArr
        Map<Integer, GO[]> gi2GOArrMap = getGi2GOArrMap(gene2GOGZFile);

        /** process nr and pid2tid shards one by one **/
        // crete temporary dir to store nr json shard files
        Set<Integer> existedTaxonIdSet = new HashSet<>();
        FileReaderUtil fileReaderUtil = new FileReaderUtil(namesDmpFile, null, false);
        fileReaderUtil.read(line -> {
            String tid = line.substring(0, line.indexOf("\t"));
            existedTaxonIdSet.add(Integer.valueOf(tid));
        });
        JsonGenerator protWithoutTaxonJsonGen = jsonFactory.createGenerator(new File(protNoTidFile), JsonEncoding.UTF8);
        for (int i = startShard; i < nrShardNum; i++) {
            log.info("start write nr sequence and annotation info [{} / {}]", (i + 1), nrShardNum);
            OutputStream os = new FileOutputStream(Paths.get(nrShardsDir, "nr-" + i + ".json").toFile());
            JsonGenerator jsonGenerator = jsonFactory.createGenerator(os, JsonEncoding.UTF8);
            // read a pid2tid shard: protein id => accession, accession.version, taxid, gi
            Path pid2TidShardPath = Paths.get(pid2TidShardsPath.toString(), i + ".txt");
            Map<String, String[]> pid2TidMap = Files.readAllLines(pid2TidShardPath, StandardCharsets.UTF_8)
                    .parallelStream()
                    .map(line -> line.split("\t"))
                    .collect(Collectors.toMap(
                            tmp -> tmp[1],
                            tmp -> tmp
                    ));
            // read nr gz shard
            Path nrShardPath = Paths.get(nrShardsPath.toString(), i + ".txt");
            BufferedReader nrReader = Files.newBufferedReader(nrShardPath, StandardCharsets.UTF_8);
            String line;
            while ((line = nrReader.readLine()) != null) {
                String[] tmp = line.split(" "); // must same as the separator used in split
                if (pid2TidMap.containsKey(tmp[0])) {
                    String[] annotation = pid2TidMap.get(tmp[0]);
                    Integer tid = Integer.valueOf(annotation[2]);
                    if (existedTaxonIdSet.contains(tid)) {
                        // the taxon id is in names.dmp
                        jsonGenerator.writeStartObject();
                        jsonGenerator.writeStringField("pid", tmp[0]);
                        jsonGenerator.writeStringField("seq", tmp[1]);
                        // TODO do equate IL ?
//                        jsonGenerator.writeStringField("equateILSeq", tmp[1].replace("I", "L"));
                        jsonGenerator.writeNumberField("tid", tid);
                        int gi = Integer.parseInt(annotation[3]);
                        jsonGenerator.writeNumberField("gi", gi);
                        // if there is go annotation for the protein
                        if (gi2GOArrMap.containsKey(gi)) {
                            jsonGenerator.writeArrayFieldStart("goArr");
                            for (GO go : gi2GOArrMap.get(gi)) {
                                jsonGenerator.writeStartObject();
                                jsonGenerator.writeStringField("id", go.getId());
                                jsonGenerator.writeStringField("term", go.getTerm());
                                jsonGenerator.writeStringField("category", go.getCategory().toText());
                                jsonGenerator.writeEndObject();
                            }
                            jsonGenerator.writeEndArray();
                        }
                        jsonGenerator.writeEndObject();
                        jsonGenerator.writeRaw(System.lineSeparator());
                    } else {
                        // the taxon id is not in names.dmp
                        protWithoutTaxonJsonGen.writeStartObject();
                        protWithoutTaxonJsonGen.writeStringField("pid", tmp[0]);
                        protWithoutTaxonJsonGen.writeStringField("seq", tmp[1]);
                        // TODO do equate IL ?
//                        protWithoutTaxonJsonGen.writeStringField("equateILSeq", tmp[1].replace("I", "L"));
                        protWithoutTaxonJsonGen.writeNumberField("tid", tid);
                        int gi = Integer.parseInt(annotation[3]);
                        protWithoutTaxonJsonGen.writeNumberField("gi", gi);
                        // if there is go annotation for the protein
                        if (gi2GOArrMap.containsKey(gi)) {
                            protWithoutTaxonJsonGen.writeArrayFieldStart("goArr");
                            for (GO go : gi2GOArrMap.get(gi)) {
                                protWithoutTaxonJsonGen.writeStartObject();
                                protWithoutTaxonJsonGen.writeStringField("id", go.getId());
                                protWithoutTaxonJsonGen.writeStringField("term", go.getTerm());
                                protWithoutTaxonJsonGen.writeStringField("category", go.getCategory().toText());
                                protWithoutTaxonJsonGen.writeEndObject();
                            }
                            protWithoutTaxonJsonGen.writeEndArray();
                        }
                        protWithoutTaxonJsonGen.writeEndObject();
                        protWithoutTaxonJsonGen.writeRaw(System.lineSeparator());
                    }
                } else {
                    misWriter.write(">" + tmp[0] + System.lineSeparator() + tmp[1] + System.lineSeparator());
                }
            }
            //close
            nrReader.close();
            jsonGenerator.close();
            Files.delete(pid2TidShardPath);
            Files.delete(nrShardPath);
            log.info("delete shard file [{}, {}]", pid2TidShardPath, nrShardPath);
        }
        protWithoutTaxonJsonGen.close();
        misWriter.close();
        Files.delete(pid2TidShardsPath);
        Files.delete(nrShardsPath);
        log.info("delete shard dir [{}, {}]", pid2TidShardsPath, nrShardsPath);
        extractTidNotInNamesDmp(protNoTidFile, tidNotInNamesDmpFile);
    }

    /**
     * Split nr gz file as small gz file and name file as "shard_id.gz"
     *
     * @param tempDir
     * @param nrShardNum
     * @param nrGZFile
     * @return
     * @throws IOException
     */
    public Path splitNR(String tempDir, int nrShardNum, String nrGZFile) throws IOException {

        /** create temporary dir and shard writer **/
        Path tempDirPath = Files.createDirectory(Paths.get(tempDir, "nr-shards" + System.nanoTime()));
        log.info("create temporary dir <{}>", tempDirPath);
        Map<Integer, BufferedWriter> shardWriterMap = new HashMap<>(nrShardNum);
        for (int i = 0; i < nrShardNum; i++) {
            shardWriterMap.put(i,
//                    new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(
//                            new FileOutputStream(Paths.get(tempDirPath.toString(), i + ".gz").toFile())
//                    )))
                    Files.newBufferedWriter(Paths.get(tempDirPath.toString(), i + ".txt"), StandardCharsets.UTF_8)
            );
        }

        /** read nr gz file **/
        BufferedReader nrReader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(nrGZFile))));
        String line;
        String[] prot = new String[]{null, ""}; // 0 - protein id, 1 - sequence
        while ((line = nrReader.readLine()) != null) {
            if (line.startsWith(">")) {
                if (prot[0] != null) {
                    // calculate route
                    shardWriterMap.get(Math.abs(Objects.hash(prot[0]) % nrShardNum))
                            .write(prot[0] + " " + prot[1] + System.lineSeparator());
                    prot = new String[]{null, ""};
                }
                prot[0] = line.substring(1, line.indexOf(" "));
            } else {
                prot[1] += line;
            }
        }

        /** close **/
        for (BufferedWriter bw : shardWriterMap.values()) {
            bw.close();
        }
        nrReader.close();
        return tempDirPath;
    }

    /**
     * Split pid2tid gz file and name file as "shard_id"
     *
     * @param tempDir
     * @param nrShardNum
     * @param pid2TidGZFiles
     * @return
     * @throws IOException
     */
    public Path splitPid2TidGZFile(String tempDir, int nrShardNum, String... pid2TidGZFiles) throws IOException {

        /** create temporary dir and shard writer **/
        Path tempDirPath = Files.createDirectory(Paths.get(tempDir, "pid2tid-shards" + System.nanoTime()));
        log.info("create temporary dir <{}>", tempDirPath);
        Map<Integer, BufferedWriter> shardWriterMap = new HashMap<>(nrShardNum);
        for (int i = 0; i < nrShardNum; i++) {
            shardWriterMap.put(i,
                    Files.newBufferedWriter(Paths.get(tempDirPath.toString(), i + ".txt"), StandardCharsets.UTF_8)
            );
        }

        /** read pid2tid gz file **/
        for (String file : pid2TidGZFiles) {
            log.info("start to process file <{}>", file);
            BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file))));
            br.readLine();
            String line;
            while ((line = br.readLine()) != null) {
                // accession, accession.version, taxid, gi (tab)
                shardWriterMap.get(Math.abs(Objects.hash(line.split("\t")[1].trim()) % nrShardNum))
                        .write(line + System.lineSeparator());
            }
            br.close();
        }

        /** close **/
        for (BufferedWriter bw : shardWriterMap.values()) {
            bw.close();
        }
        return tempDirPath;
    }

    /**
     * Extract taxon ids that are in nr library but not in names.dmp, then write into file.
     *
     * @param protWithoutTaxonJsonFile
     * @param output
     * @throws IOException
     */
    public void extractTidNotInNamesDmp(String protWithoutTaxonJsonFile, String output) throws IOException {
        Set<String> tidSet = Files.readAllLines(Paths.get(protWithoutTaxonJsonFile), StandardCharsets.UTF_8)
                .stream()
                .map(line -> {
                    try {
                        return objectMapper.readTree(line).get("tid").asText();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    throw new RuntimeException("error line: " + line);
                })
                .collect(Collectors.toSet());
        Files.write(Paths.get(output), tidSet, StandardCharsets.UTF_8,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    /**
     * Update taxon id info in file protWithoutTaxonJsonFile.
     * <p>
     * Note: you can get file ncbiTidMappingFile from \
     * "https://www.ncbi.nlm.nih.gov/Taxonomy/TaxIdentifier/tax_identifier.cgi"
     *
     * @param ncbiTidMappingFile
     * @param protNoTidFile
     * @param outputFile
     * @throws IOException
     */
    public void updateTaxonNotInNamesDmp(String ncbiTidMappingFile, String protNoTidFile, String outputFile) throws IOException {
        // read ncbi mapping file: [code	|	taxid	|	primary taxid	|	taxname]
        Map<Integer, Integer> tid2PrimaryTid = Files.readAllLines(Paths.get(ncbiTidMappingFile), StandardCharsets.UTF_8)
                .stream()
                .map(line -> {
                    if (line.startsWith("2")) {
                        String[] tmp = line.split("\t\\|\t");
                        return new String[]{tmp[1], tmp[2]};
                    } else {
                        return null;
                    }
                })
                .filter(pair -> pair != null)
                .collect(Collectors.toMap(
                        pair -> Integer.parseInt(pair[0]),
                        pair -> Integer.parseInt(pair[1])
                ));
        // update taxon info for proteins and ignore entries whose ids do not have mappings in ncbiTidMappingFile
        // the json file is not standard
        List<String> updatedProtList = Files.readAllLines(Paths.get(protNoTidFile), StandardCharsets.UTF_8)
                .stream()
                .map(line -> {
                    try {
                        JsonNode node = objectMapper.readTree(line);
                        int tid = node.get("tid").asInt();
                        if (tid2PrimaryTid.containsKey(tid)) {
                            ((ObjectNode) node).put("tid", tid2PrimaryTid.get(tid));
                            return node.toString();
                        } else {
                            return null;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    throw new RuntimeException("error line: " + line);
                })
                .filter(line -> line != null)
                .collect(Collectors.toList());
        // write
        BufferedWriter bw = Files.newBufferedWriter(Paths.get(outputFile), StandardCharsets.UTF_8);
        for (String line : updatedProtList) {
            bw.write(line + System.lineSeparator());
        }
        bw.close();
    }

    /**
     * Create a nr index according to nrIndexName and config file
     * and the RuntimeException will be thrown if the index exists
     *
     * @param nrIndexName
     * @return
     * @throws IOException
     */
    public ESIndex createNRIndex(String nrIndexName) throws IOException {
        return ESUtil.createIndex(esClient,
                nrIndexName,
                nrIndexConfStream,
                false);
    }

    /**
     * @param bulkActions
     * @param bulkSize
     * @param concurrentReq
     * @param nrIndex
     * @param reindex
     * @param nrJsonFiles
     * @throws IOException
     * @throws InterruptedException
     */
    public void storeNR(int bulkActions,
                        ByteSizeValue bulkSize,
                        int concurrentReq,
                        ESIndex nrIndex,
                        boolean reindex,
                        String... nrJsonFiles) throws IOException, InterruptedException {
        BulkProcessor bulkProcessor = BulkProcessor.builder(esClient, ESUtil.getDefaultBulkProcessorListener())
                .setBulkActions(bulkActions)
                .setBulkSize(bulkSize)
                .setFlushInterval(new TimeValue(30, TimeUnit.SECONDS))
                .setConcurrentRequests(concurrentReq)
                .build();

        /** read protein info and store into es **/
        if (reindex) {
            int termsSize = 10_000;
            Map<String, String> pid2ProMap = new HashMap<>(termsSize);
            for (String file : nrJsonFiles) {
                log.info("start to insert entries from <{}>", file);
                BufferedReader nrReader = Files.newBufferedReader(Paths.get(file), StandardCharsets.UTF_8);
                String line;
                while ((line = nrReader.readLine()) != null) {
                    pid2ProMap.put(objectMapper.readTree(line).get("pid").asText(), line);
                    if (pid2ProMap.size() == termsSize) {
                        // delete doc existed
                        deleteByPid(nrIndex.getIndexName(), pid2ProMap.keySet());
                        // add entries into bulk processor
                        for (String pro : pid2ProMap.values()) {
                            bulkProcessor.add(
                                    new IndexRequest(nrIndex.getIndexName(), nrIndex.getTypeName())
                                            .source(pro, XContentType.JSON)
                            );
                        }
                        // initialize
                        pid2ProMap = new HashMap<>(termsSize);
                    }
                }
                // close or flush
                nrReader.close();
                // delete doc existed
                deleteByPid(nrIndex.getIndexName(), pid2ProMap.keySet());
                // add entries into bulk processor
                for (String pro : pid2ProMap.values()) {
                    bulkProcessor.add(
                            new IndexRequest(nrIndex.getIndexName(), nrIndex.getTypeName())
                                    .source(pro, XContentType.JSON)
                    );
                }
                bulkProcessor.flush();
                // initialize
                pid2ProMap = new HashMap<>(termsSize);
            }
        } else {
            for (String file : nrJsonFiles) {
                log.info("start to insert entries from <{}>", file);
                BufferedReader nrReader = Files.newBufferedReader(Paths.get(file), StandardCharsets.UTF_8);
                String line;
                while ((line = nrReader.readLine()) != null) {
                    bulkProcessor.add(
                            new IndexRequest(nrIndex.getIndexName(), nrIndex.getTypeName())
                                    .source(line, XContentType.JSON)
                    );
                }
                // close or flush
                nrReader.close();
                bulkProcessor.flush();
            }
        }

        /** close **/
        bulkProcessor.flush();
        bulkProcessor.awaitClose(10, TimeUnit.MINUTES);
        esClient.admin().indices().prepareRefresh(nrIndex.getIndexName()).get();
    }

    /**
     * Read gene id to GO file
     *
     * @param gene2GOGZFile
     * @return
     * @throws IOException
     */
    private Map<Integer, GO[]> getGi2GOArrMap(String gene2GOGZFile) throws IOException {
        // #tax_id, GeneID, GO_ID, Evidence, Qualifier, GO_term, PubMed, Category (separator: tab)
        FileReaderUtil gi2GOReader = new FileReaderUtil(gene2GOGZFile, "\t", true, FileReaderUtil.Format.GZIP);
        // gi => GO set
        Map<Integer, Set<GO>> gi2GOSetMap = new HashMap<>();
        gi2GOReader.read(
                (String line) -> {
                    String[] tmp = line.split("\t");
                    GO go = new GO();
                    Integer gi = Integer.valueOf(tmp[1].trim());
                    go.setId(tmp[2].trim());
                    go.setTerm(tmp[5].trim());
                    go.setCategory(GO.Category.toCategory(tmp[7].trim()));
                    if (!gi2GOSetMap.containsKey(gi)) {
                        gi2GOSetMap.put(gi, new HashSet<>());
                    }
                    gi2GOSetMap.get(gi).add(go);
                }
        );
        return gi2GOSetMap.entrySet().parallelStream()
                .collect(Collectors.toMap(
                        e -> e.getKey(),
                        e -> e.getValue().stream().toArray(GO[]::new)
                ));
    }

    /**
     * Delete protein entries by protein ids
     *
     * @param nrIndexName
     * @param pidSet
     */
    private void deleteByPid(String nrIndexName, Set<String> pidSet) {
        new DeleteByQueryRequestBuilder(esClient, DeleteByQueryAction.INSTANCE)
                .filter(QueryBuilders.termsQuery("pid", pidSet))
                .source(nrIndexName)
                .get();
    }
}
