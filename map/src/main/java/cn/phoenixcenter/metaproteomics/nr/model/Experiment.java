package cn.phoenixcenter.metaproteomics.nr.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class Experiment {

    private Set<TrypticPeptide> peptideSet;

    private List<String> sampleNameList;

    private boolean equateIL;
}
