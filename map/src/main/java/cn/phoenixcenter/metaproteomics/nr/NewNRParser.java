package cn.phoenixcenter.metaproteomics.nr;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.utils.ESUtil;
import cn.phoenixcenter.metaproteomics.utils.model.ESIndex;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

@Getter
@Log4j2
public class NewNRParser {

    private int shardNum;

    private final TransportClient esClient = GlobalConfig.getObj(TransportClient.class);

    private final InputStream nrIndexConfStream = NRParser.class.getResourceAsStream("/es_mappings/nr.json");

    private final JsonFactory factory = new JsonFactory();

    public NewNRParser(int shardNum) {
        this.shardNum = shardNum;
    }


    // 219273759
    public void splitNR(String nrGZFile, Path nrShardDirPath) throws IOException {
        splitNR(new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(nrGZFile)))),
                nrShardDirPath);
    }

    public void splitNR(Path nrPath, Path nrShardDirPath) throws IOException {
        splitNR(Files.newBufferedReader(nrPath, StandardCharsets.UTF_8), nrShardDirPath);
    }

    /**
     * Split NR database as shards. Each line contains [pid, seq] in files
     *
     * @param br
     * @param nrShardDirPath
     * @throws IOException
     */
    private void splitNR(BufferedReader br, Path nrShardDirPath) throws IOException {
        long count = 0;
        Map<Integer, BufferedWriter> shard2BW = createBWs(nrShardDirPath);
        String line;
        String pid = null;
        StringBuilder seq = null;
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">")) {
                if (pid != null) {
                    // pid, seq
                    shard2BW.get(Math.abs(pid.hashCode()) % shardNum)
                            .write(pid + "\t" + seq.toString() + System.lineSeparator());
                }
                pid = line.substring(1, line.indexOf(" "));
                seq = new StringBuilder();
                if (++count % 10_000_000 == 0) {
                    log.info("{} has been parsed", count);
                }
            } else {
                seq.append(line);
            }
        }
        if (pid != null) {
            // pid, seq
            shard2BW.get(Math.abs(pid.hashCode()) % shardNum)
                    .write(pid + "\t" + seq.toString() + System.lineSeparator());
        }
        br.close();
        closeBWs(shard2BW.values());
        log.info("total {} lines has benn parsed", count);
    }


    /**
     * Split acc2tid as shards. Each line contains [accession.version, taxid, gi].
     *
     * @param acc2TidShardDir
     * @param acc2TidGZFiles
     * @throws IOException
     */
    public void splitAcc2Tid(Path acc2TidShardDir, String... acc2TidGZFiles) throws IOException {
        Map<Integer, BufferedWriter> shard2BW = createBWs(acc2TidShardDir);
        for (String file : acc2TidGZFiles) {
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    new GZIPInputStream(new FileInputStream(file)))
            );
            br.readLine();
            String line;
            while ((line = br.readLine()) != null) {
                int idx = line.indexOf("\t") + 1;
                String pid = line.substring(idx, line.indexOf("\t", idx));
                // accession.version
                shard2BW.get(Math.abs(pid.hashCode()) % shardNum)
                        // accession.version, taxid, gi
                        .write(line.substring(idx) + System.lineSeparator());
            }
            br.close();
        }
        closeBWs(shard2BW.values());
    }

    /**
     * Get intersection between nr files and acc2tid files. The result will be write into a file
     *
     * @param namesDmpPath
     * @param nrShardDirDir
     * @param acc2TidShardDirPath
     * @param nrWithTidPath
     * @param nrWithIllegalTidPath those proteins' taxon ids are not in names.dmp and need retrieve in NCBI
     * @param nrWithIllegalPidPath those proteins' ids are not in acc2tid and will be ignore when storing into db
     * @throws IOException
     */
    public void addTidInfo(Path namesDmpPath,
                           Path nrShardDirDir,
                           Path acc2TidShardDirPath,
                           Path nrWithTidPath,
                           Path nrWithIllegalTidPath,
                           Path nrWithIllegalPidPath) throws IOException {
        // read ncbi taxonomy names.dmp file
        Set<String> nameSet = new HashSet<>();
        BufferedReader namesReader = Files.newBufferedReader(namesDmpPath, StandardCharsets.UTF_8);
        String row;
        while ((row = namesReader.readLine()) != null) {
            nameSet.add(row.substring(0, row.indexOf("\t")));
        }
        namesReader.close();
        // read nr and acc2tid
        BufferedWriter tidWriter = Files.newBufferedWriter(nrWithTidPath, StandardCharsets.UTF_8);
        BufferedWriter illegalTidWriter = Files.newBufferedWriter(nrWithIllegalTidPath, StandardCharsets.UTF_8);
        BufferedWriter illegalPidWriter = Files.newBufferedWriter(nrWithIllegalPidPath, StandardCharsets.UTF_8);
        for (int i = 0; i < shardNum; i++) {
            // read acc2tid
            Map<String, String> acc2Tid = Files.readAllLines(acc2TidShardDirPath.resolve(String.valueOf(i)), StandardCharsets.UTF_8)
                    .parallelStream()
                    .map(line -> {
                        int idx = line.indexOf("\t");
                        return new String[]{line.substring(0, idx), line.substring(idx + 1)};
                    })
                    .collect(Collectors.toMap(
                            // pid
                            arr -> arr[0],
                            // tid, gi
                            arr -> arr[1]
                    ));
            BufferedReader br = Files.newBufferedReader(nrShardDirDir.resolve(String.valueOf(i)));
            String line;
            while ((line = br.readLine()) != null) {
                int idx = line.indexOf("\t");
                String pid = line.substring(0, idx);
                if (acc2Tid.containsKey(pid)) {
                    String tidGi = acc2Tid.get(pid);
                    String tid = tidGi.substring(0, tidGi.indexOf("\t"));
                    if (nameSet.contains(tid)) {
                        // taxid, gi, pid, sequence
                        tidWriter.write(tidGi + "\t" + line + System.lineSeparator());
                    } else {
                        // taxid, gi, pid, sequence
                        illegalTidWriter.write(tidGi + "\t" + line + System.lineSeparator());
                    }
                } else {
                    illegalPidWriter.write(line + System.lineSeparator());
                }
            }
            br.close();
        }
        illegalTidWriter.close();
        illegalPidWriter.close();
        tidWriter.close();
    }

    /**
     * Extract illegal taxon id for retrieving in NCBI
     * [https://www.ncbi.nlm.nih.gov/Taxonomy/TaxIdentifier/tax_identifier.cgi]
     *
     * @param nrWithIllegalTidPath
     * @param illegalTidPath
     * @throws IOException
     */
    public void extractIllegalTid(Path nrWithIllegalTidPath, Path illegalTidPath) throws IOException {
        Files.write(illegalTidPath,
                Files.readAllLines(nrWithIllegalTidPath, StandardCharsets.UTF_8)
                        .stream()
                        .map(line -> line.substring(0, line.indexOf("\t")))
                        .collect(Collectors.toSet()),
                StandardCharsets.UTF_8);
    }

    /**
     * Update taxon id after retrieving in NCBI
     *
     * @param updatedTidPath
     * @param nrWithIllegalTidPath
     * @param nrWithUpdatedTidPath
     * @throws IOException
     */
    public void updateTid(Path updatedTidPath, Path nrWithIllegalTidPath, Path nrWithUpdatedTidPath) throws IOException {
        // read ncbi mapping file: [code	|	taxid	|	primary taxid	|	taxname]
        Map<String, String> tid2PrimaryTid = Files.readAllLines(updatedTidPath, StandardCharsets.UTF_8)
                .stream()
                .map(line -> {
                    if (line.startsWith("2")) {
                        String[] tmp = line.split("\t\\|\t");
                        return new String[]{tmp[1], tmp[2]};
                    } else {
                        return null;
                    }
                })
                .filter(pair -> pair != null)
                .collect(Collectors.toMap(
                        pair -> pair[0],
                        pair -> pair[1]
                ));
        // update
        Files.write(nrWithUpdatedTidPath,
                Files.readAllLines(nrWithIllegalTidPath, StandardCharsets.UTF_8)
                        .stream()
                        .map(line -> {
                            int idx = line.indexOf("\t");
                            String tid = line.substring(0, idx);
                            if (tid2PrimaryTid.containsKey(tid)) {
                                return tid2PrimaryTid.get(tid) + line.substring(idx);
                            } else {
                                return null;
                            }
                        })
                        .filter(line -> line != null)
                        .collect(Collectors.toList()),
                StandardCharsets.UTF_8
        );
    }

    /**
     * Split nr as shards by GI
     *
     * @param nrWithTidPaths
     * @param nrGiShardDirPath
     * @throws IOException
     */
    public void splitNRByGi(Path[] nrWithTidPaths, Path nrGiShardDirPath) throws IOException {
        Map<Integer, BufferedWriter> shard2BW = createBWs(nrGiShardDirPath);
        for (Path nrWithTidPath : nrWithTidPaths) {
            BufferedReader br = Files.newBufferedReader(nrWithTidPath, StandardCharsets.UTF_8);
            String line;
            while ((line = br.readLine()) != null) {
                int idx = line.indexOf("\t");
                String gi = line.substring(idx + 1, line.indexOf("\t", idx + 1));
                shard2BW.get(Math.abs(gi.hashCode()) % shardNum)
                        .write(line + System.lineSeparator());
            }
            br.close();
        }
        closeBWs(shard2BW.values());
    }

    /**
     * Split id mapping file from uniprot as shards by uniprot id.
     * Each line contains [gi, uniprot id]
     *
     * @param idMappingGZPath
     * @param idMappingUidShardDirPath
     * @throws IOException
     */
    public void splitIDMappingByUid(Path idMappingGZPath, Path idMappingUidShardDirPath) throws IOException {
        Map<Integer, BufferedWriter> shard2BWs = createBWs(idMappingUidShardDirPath);
        BufferedReader br = new BufferedReader(new InputStreamReader(
                new GZIPInputStream(new FileInputStream(idMappingGZPath.toFile()))
        ));
        String line;
        while ((line = br.readLine()) != null) {
            String[] tmp = line.split("\t");
            if (tmp[1].equals("GI")) {
                shard2BWs.get(Math.abs(tmp[0].hashCode()) % shardNum)
                        // gi, uniprot id
                        .write(tmp[2] + "\t" + tmp[0] + System.lineSeparator());
            }
        }
        closeBWs(shard2BWs.values());
        br.close();
    }

    /**
     * Split "goa_uniprot_all.gaf.gz" as shards by uniprot id(uid).
     * Each line contains [uid, GO category|GO id]
     *
     * @param goaGZPath
     * @param goaUidShardDirPath
     * @throws IOException
     */
    public void splitGOAByUid(Path goaGZPath, Path goaUidShardDirPath) throws IOException {
        Map<Integer, BufferedWriter> uidWriters = createBWs(goaUidShardDirPath);
        BufferedReader br = new BufferedReader(
                new InputStreamReader(new GZIPInputStream(new FileInputStream(goaGZPath.toFile())))
        );
        String line;
        while ((line = br.readLine()) != null) {
            if (!line.startsWith("!")) {
                String[] tmp = line.split("\t");
                uidWriters.get(Math.abs(tmp[1].hashCode()) % shardNum)
                        // uid, GO category|GO id
                        .write(tmp[1] + "\t" + tmp[8] + "|" + tmp[4] + System.lineSeparator());
            }
        }
        br.close();
        closeBWs(uidWriters.values());
    }

    public void addGOInfoForUid(Path idMappingUidShardDirPath, Path goaUidShardDirPath, Path idMappingGiShardDirPath) throws IOException {
        Map<Integer, BufferedWriter> giWriters = createBWs(idMappingGiShardDirPath);
        for (int i = 0; i < shardNum; i++) {
            // uid, GO category|GO id
            Map<String, String> uid2GOs = Files.readAllLines(goaUidShardDirPath.resolve(String.valueOf(i)),
                    StandardCharsets.UTF_8)
                    .parallelStream()
                    .map(line -> line.split("\t"))
                    .collect(Collectors.toMap(
                            arr -> arr[0],
                            arr -> arr[1],
                            (oldVal, newVal) -> oldVal + ";" + newVal
                    ));

            BufferedReader br = Files.newBufferedReader(idMappingUidShardDirPath.resolve(String.valueOf(i)),
                    StandardCharsets.UTF_8);
            String line;
            while ((line = br.readLine()) != null) {
                // gi, uid
                String[] tmp = line.split("\t");
                // ignore those uniprot id without GO annotation
                if (uid2GOs.containsKey(tmp[1])) {
                    giWriters.get(Math.abs(tmp[0].hashCode()) % shardNum)
                            // gi, uid, GO category|GO id;GO category|GO id
                            .write(line + "\t" + uid2GOs.get(tmp[1]) + System.lineSeparator());
                }
            }
            br.close();
        }
        closeBWs(giWriters.values());
    }

    /**
     * Add uniprot id mapping for NCBI protein  id.
     * Each line contains [uid, tid, gi, pid, seq] or [tid, gi, pid, seq].
     * And a entry will be write multiple line if gi of the entry matches multiple uniprot id,
     * we will collapse those entries after splitting shards.
     *
     * @param nrGiShardDirPath
     * @param idMappingGiShardDirPath
     * @param nrShardDirPath
     * @throws IOException
     */
    public void addUidInfo(Path nrGiShardDirPath, Path idMappingGiShardDirPath,
                           Path nrShardDirPath) throws IOException {
        for (int i = 0; i < shardNum; i++) {
            JsonGenerator gen = factory.createGenerator(nrShardDirPath.resolve(String.valueOf(i)).toFile(), JsonEncoding.UTF8);
            // gi => {uids => GO1;GO2}
            Map<String, Map.Entry<Set<String>, Set<String>>> gi2GOs = new HashMap<>();
            BufferedReader gi2uidsReader = Files.newBufferedReader(idMappingGiShardDirPath.resolve(String.valueOf(i)),
                    StandardCharsets.UTF_8);
            String line;
            while ((line = gi2uidsReader.readLine()) != null) {
                // gi, uid, GO category|GO id;GO category|GO id
                String[] tmp = line.split("\t");
                if (!gi2GOs.containsKey(tmp[0])) {
                    gi2GOs.put(tmp[0], new AbstractMap.SimpleEntry<>(new HashSet<>(), new HashSet<>()));
                }
                Map.Entry<Set<String>, Set<String>> uid2GOs = gi2GOs.get(tmp[0]);
                uid2GOs.getKey().add(tmp[1]);
                Arrays.stream(tmp[2].split(";"))
                        .forEach(go -> uid2GOs.getValue().add(go));
            }
            gi2uidsReader.close();
            BufferedReader br = Files.newBufferedReader(nrGiShardDirPath.resolve(String.valueOf(i)), StandardCharsets.UTF_8);
            while ((line = br.readLine()) != null) {
                // taxid, gi, pid, sequence
                String[] tmp = line.split("\t");
                gen.writeStartObject();
                gen.writeStringField("pid", tmp[2]);
                gen.writeNumberField("tid", Integer.parseInt(tmp[0]));
                gen.writeNumberField("gi", Integer.parseInt(tmp[1]));
                gen.writeStringField("seq", tmp[3]);
                if (gi2GOs.containsKey(tmp[1])) {
                    Map.Entry<Set<String>, Set<String>> uid2GOs = gi2GOs.get(tmp[1]);
                    gen.writeStringField("uid", uid2GOs.getKey().stream().collect(Collectors.joining(";")));
                    gen.writeArrayFieldStart("goArr");
                    for (String go : uid2GOs.getValue()) {
                        String[] goTmp = go.split("\\|");
                        gen.writeStartObject();
                        gen.writeStringField("id", goTmp[1]);
                        gen.writeStringField("category", goTmp[0]);
                        gen.writeEndObject();
                    }
                    gen.writeEndArray();

                }
                gen.writeEndObject();
                gen.writeRaw(System.lineSeparator());
            }
            br.close();
            gen.close();
        }
    }

//    public void addGOInfo(Path nrUidShardDirPath, Path goaUidShardDirPath, Path nrPidShardDirPath) throws IOException {
//        Map<Integer, BufferedWriter> pidWriters = createBWs(nrPidShardDirPath);
//        for (int i = 0; i < shardNum; i++) {
//            Map<String, String> uid2GOs = Files.readAllLines(goaUidShardDirPath.resolve(String.valueOf(i)),
//                    StandardCharsets.UTF_8)
//                    .parallelStream()
//                    .map(line -> line.split("\t", 2))
//                    .collect(Collectors.toMap(
//                            // uid, GO category|GO id
//                            arr -> arr[0],
//                            arr -> arr[1],
//                            (oldVal, newVal) -> oldVal + ";" + newVal
//                    ));
//            BufferedReader br = Files.newBufferedReader(nrUidShardDirPath.resolve(String.valueOf(i)),
//                    StandardCharsets.UTF_8);
//            String line;
//            while ((line = br.readLine()) != null) {
//                // uid, tid, gi, pid, seq
//                String uid = line.substring(0, line.indexOf("\t"));
//                int endIdx = line.lastIndexOf("\t");
//                String pid = line.substring(line.lastIndexOf("\t", endIdx - 1) + 1, endIdx);
//                if (uid2GOs.containsKey(uid)) {
//                    pidWriters.get(Math.abs(pid.hashCode()) % shardNum)
//                            .write(uid2GOs.get(uid) + "\t" + line + System.lineSeparator());
//                } else {
//                    // some uniprot ids have no GO
//                    pidWriters.get(Math.abs(pid.hashCode()) % shardNum)
//                            .write(line + System.lineSeparator());
//                }
//            }
//            br.close();
//        }
//        closeBWs(pidWriters.values());
//    }
//
//    /**
//     * Some entries' gi match multiple uniprot ids, now we need collapse those entries.
//     */
//    private void collapse(Path nrPidShardDirPath) {
//        for (int i = 0; i < shardNum; i++) {
//
//        }
//    }
//
//    public void combine() {
//
//    }

    private Map<Integer, BufferedWriter> createBWs(Path resultDirPath) throws IOException {
        Map<Integer, BufferedWriter> shard2BW = new HashMap<>(shardNum);
        for (int i = 0; i < shardNum; i++) {
            shard2BW.put(i, Files.newBufferedWriter(resultDirPath.resolve(String.valueOf(i)), StandardCharsets.UTF_8));
        }
        return shard2BW;
    }

    private void closeBWs(Collection<BufferedWriter> bws) throws IOException {
        for (BufferedWriter bw : bws) {
            bw.close();
        }
    }

    public void storeInES(int bulkActions,
                          ByteSizeValue bulkSize,
                          int concurrentReq,
                          ESIndex nrIndex,
                          Path nrShardDirPath) throws IOException, InterruptedException {
        BulkProcessor bulkProcessor = BulkProcessor.builder(esClient, ESUtil.getDefaultBulkProcessorListener())
                .setBulkActions(bulkActions)
                .setBulkSize(bulkSize)
                .setFlushInterval(new TimeValue(1, TimeUnit.MINUTES))
                .setConcurrentRequests(concurrentReq)
                .build();
        for (int i = 0; i < shardNum; i++) {
            Path path = nrShardDirPath.resolve(String.valueOf(i));
            BufferedReader br = Files.newBufferedReader(path, StandardCharsets.UTF_8);
            String line;
            log.info("start write nr sequence and annotation info [{} / {}]", (i + 1), shardNum);
            while ((line = br.readLine()) != null) {
//                // [uid], tid, gi, pid, seq
//                String[] tmp = line.split("\t");
//                XContentBuilder builder = XContentFactory.jsonBuilder()
//                        .startObject();
//                if (tmp.length == 5) {
//                    // exist uniprot id
//                    builder.field("uid", tmp[0]);
//                    builder.field("tid", tmp[1]);
//                    builder.field("gi", tmp[2]);
//                    builder.field("pid", tmp[3]);
//                    builder.field("seq", tmp[4]);
//                } else {
//                    builder.field("tid", tmp[0]);
//                    builder.field("gi", tmp[1]);
//                    builder.field("pid", tmp[2]);
//                    builder.field("seq", tmp[3]);
//                }
//                builder.endObject();
//                bulkProcessor.add(new IndexRequest(nrIndex.getIndexName(), nrIndex.getTypeName())
//                        .source(builder)
//                );
                bulkProcessor.add(new IndexRequest(nrIndex.getIndexName(), nrIndex.getTypeName())
                        .source(line, XContentType.JSON)
                );
            }
            br.close();
            bulkProcessor.flush();
        }
        bulkProcessor.flush();
        bulkProcessor.awaitClose(1, TimeUnit.DAYS);
        esClient.admin().indices().prepareRefresh(nrIndex.getIndexName()).get();
    }
}
