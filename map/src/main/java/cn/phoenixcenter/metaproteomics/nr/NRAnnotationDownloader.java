package cn.phoenixcenter.metaproteomics.nr;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.io.CopyStreamAdapter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.atomic.AtomicInteger;

public class NRAnnotationDownloader {

//    private FTPClient ncbiFTPClient;

    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    private int maxRetryNum = 1;

    private final Logger logger = LogManager.getLogger(NRAnnotationDownloader.class);

    private final String ncbiFTPHost = "ftp.ncbi.nih.gov";

    private final String user = "anonymous";

    private final int port = 21;

    private final String nrFilePath = "";

    private final String pid2TidFilePath = "/pub/taxonomy/accession2taxid/prot.accession2taxid.gz";

    private final String pid2TidMd5FilePath = "/pub/taxonomy/accession2taxid/prot.accession2taxid.gz.md5";

    // taxdump
    private final String taxdumpDir = "/pub/taxonomy";

    private final String taxdumpFile = "taxdump.tar.gz";

    private final String taxdumpMd5File = "taxdump.tar.gz.md5";

    public NRAnnotationDownloader() throws IOException {
//        ncbiFTPClient = new FTPClient();
//        ncbiFTPClient.connect(ncbiFTPHost, port);
//        ncbiFTPClient.login(user, "");
//        ncbiFTPClient.enterLocalPassiveMode();
//        ncbiFTPClient.setFileType(FTP.BINARY_FILE_TYPE);
//        System.err.println(ncbiFTPClient.listFiles().length);
    }

    public FTPClient createNcbiFTPClient() throws IOException {
        FTPClient ncbiFTPClient = new FTPClient();
        ncbiFTPClient.connect(ncbiFTPHost, port);
        ncbiFTPClient.login(user, "");
        ncbiFTPClient.enterLocalPassiveMode();
        ncbiFTPClient.setFileType(FTP.BINARY_FILE_TYPE);
        System.err.println(ncbiFTPClient.listFiles().length);
        return ncbiFTPClient;
    }

    public CopyStreamAdapter createProcessListener() {
        return new CopyStreamAdapter() {
            @Override
            public void bytesTransferred(long totalBytesTransferred, int bytesTransferred, long streamSize) {
                System.out.println("===start===");
                System.out.println(totalBytesTransferred);
                System.out.println(bytesTransferred);
                System.out.println(streamSize);
                System.out.println("===end===");
            }
        };
    }

//    public void downloadPid2TidFile(String outputDir) throws IOException {
//        downloadPid2TidFile(outputDir, "prot.accession2taxid.gz");
//    }
//
//    public void downloadPid2TidFile(String outputDir, String outputFile) throws IOException {
//        String output = Paths.get(outputDir, outputFile).toString();
//        OutputStream os = new FileOutputStream(output);
//        ncbiFTPClient.retrieveFile(pid2TidFilePath, os);
//        os.close();
//    }

    public void downloadTaxdumpFile(String outputDir) throws IOException, NoSuchAlgorithmException, ParseException {
        downloadTaxdumpFile(outputDir, "taxdump.tar.gz");
    }

    public void downloadTaxdumpFile(String outputDir, String outputFile) throws IOException, NoSuchAlgorithmException, ParseException {
        FTPClient ncbiFTPClient = createNcbiFTPClient();
        downloadFile(ncbiFTPClient, taxdumpDir, taxdumpFile, taxdumpMd5File, outputDir, outputFile);
    }

    public void downloadFile(
            FTPClient ncbiFTPClient,
            String sourceDir,
            String sourceFile,
            String sourceMd5File,
            String outputDir,
            String outputFile) throws IOException, NoSuchAlgorithmException, ParseException {

        NCBIFTPFile ncbiFTPFile = new NCBIFTPFile();

        // change ftp working directory
        ncbiFTPClient.changeWorkingDirectory(sourceDir);

        /** download md5 file **/
        Path md5FilePath = Paths.get(outputDir, outputFile + ".md5");
        OutputStream md5OS = Files.newOutputStream(md5FilePath,
                StandardOpenOption.TRUNCATE_EXISTING,
                StandardOpenOption.CREATE);
        boolean md5Success = ncbiFTPClient.retrieveFile(sourceMd5File, md5OS);
        md5OS.close();
        if (!md5Success) {
            throw new RuntimeException("<" + sourceMd5File + "> has been downloaded failed");
        }
        // obtain md5 code
        String expectedMd5 = Files.readAllLines(md5FilePath)
                .stream()
                .limit(1)
                .map(line -> line.split("\\s")[0])
                .findFirst()
                .get();

        /** download annotation file **/
        Path outputFilePath = Paths.get(outputDir, outputFile);
        OutputStream downloadFileOS = Files.newOutputStream(outputFilePath,
                StandardOpenOption.TRUNCATE_EXISTING,
                StandardOpenOption.CREATE);
        // file info
        FTPFile ftpFile = ncbiFTPClient.mlistFile(sourceFile);
        // process listener
        final AtomicInteger times = new AtomicInteger(1);
        CopyStreamAdapter listener = new CopyStreamAdapter() {
            @Override
            public void bytesTransferred(long totalBytesTransferred, int bytesTransferred, long streamSize) {
                if(100.0 * totalBytesTransferred / ftpFile.getSize() > 10.0 * times.get()) {
                    logger.debug("{}--{}%", outputFilePath.toString(),
                            100.0 * totalBytesTransferred / ftpFile.getSize());
                    times.incrementAndGet();
                }
            }
        };
        ncbiFTPClient.setCopyStreamListener(listener);
        // download
        boolean fileSuccess = ncbiFTPClient.retrieveFile(sourceFile, downloadFileOS);
        // close link
        downloadFileOS.close();
        ncbiFTPClient.logout();
        ncbiFTPClient.disconnect();
        if (!fileSuccess) {
            throw new RuntimeException("<" + sourceFile + "> has been downloaded failed");
        }

        // calculate md5 of download file
        byte[] b = Files.readAllBytes(outputFilePath);
        String actualMd5 = DatatypeConverter.printHexBinary(MessageDigest.getInstance("MD5").digest(b));
        boolean match = actualMd5.equalsIgnoreCase(expectedMd5);
        if (match) {
            Files.delete(md5FilePath);
            ncbiFTPFile.setStoragePath(outputFilePath.toString());
            ncbiFTPFile.setMd5(actualMd5);
            ncbiFTPFile.setUpdateDate(dateFormat.format(ftpFile.getTimestamp().getTime()));
        } else {
            String info = "The md5 code of file is not matched [Excepted: " +
                    expectedMd5 + "; Actual: " + actualMd5 + "]. The download file locates <" + outputFilePath +
                    "> and the md5 file locates <" + md5FilePath + ">.";
            throw new RuntimeException(info);
        }
    }
}

@Getter
@Setter
@NoArgsConstructor
@ToString
class NCBIFTPFile {

    private String storagePath;

    private String md5;

    private String updateDate;
}
