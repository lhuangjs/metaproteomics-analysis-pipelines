package cn.phoenixcenter.metaproteomics.nr.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@ToString
/**
 {
 "sampleNameList": [
 "sample1",
 "sample2"
 ],
 "rank2TaxonQuant": {
 "family": [
 {
 "taxon": {
 "id": 543,
 "name": "Enterobacteriaceae",
 "parentId": 91347,
 "rank": "family",
 "lineageIdMap": {
 "superkingdom": 2,
 "phylum": 1224,
 "family": 543,
 "class": 1236,
 "order": 91347
 },
 "lineageNameMap": {
 "superkingdom": "Bacteria",
 "phylum": "Proteobacteria",
 "family": "Enterobacteriaceae",
 "class": "Gammaproteobacteria",
 "order": "Enterobacterales"
 }
 },
 "peptCountForTaxon": 0,
 "peptCountForSuntaxa": 1,
 "quantVals": [
 14.0,
 15.0
 ]
 }
 */
public class QuantResult {

    private List<String> sampleNameList;

    private Map<String, List<TaxonQuant>> rank2TaxonQuant;
}
