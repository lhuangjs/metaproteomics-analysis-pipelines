package cn.phoenixcenter.metaproteomics.nr;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.nr.model.*;
import cn.phoenixcenter.metaproteomics.taxonomy.LCAAnalysis;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import cn.phoenixcenter.metaproteomics.taxonomy.TaxonNode;
import cn.phoenixcenter.metaproteomics.utils.MathUtil;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Taxonomy analysis
 */
@Log4j2
public class TaxAnalysis {

    private static final NRSearcher nrSearcher = GlobalConfig.getObj(NRSearcher.class);

    private static final String delimiter = "\t";

    private static final DecimalFormat decimalFormat = new DecimalFormat("#.00%");

    private static final Taxon.Rank[] ranks = Taxon.Rank.values();

    private static final ObjectMapper objectMapper = new ObjectMapper()
            .enable(SerializationFeature.INDENT_OUTPUT);

    private static final JsonFactory jsonFactory = objectMapper.getFactory();

    /**
     * @param equateIL
     * @param experimentPath
     * @param lcaPath
     * @param goAnnoPath
     * @param treePath
     * @param quantPath
     * @throws IOException
     * @throws InterruptedException
     */
    public static void run(boolean equateIL,
                           Path experimentPath,
                           Path lcaPath,
                           Path goAnnoPath,
                           Path treePath,
                           Path quantPath) throws IOException, InterruptedException {
        log.debug("Start to parse {}", experimentPath);
        Experiment experiment = parse(equateIL, experimentPath, lcaPath);
        log.debug("Write GO annotation json file {}", goAnnoPath);
        writeGOAnno(experiment.getPeptideSet(), goAnnoPath);
        log.debug("Write lineage tree json file {}", treePath);
        TaxonNode<TaxonNodeData> root = groupByLCA(experiment);
        Function<TaxonNodeData, Map<String, Integer>> nodeDataConverter = (TaxonNodeData data) -> {
            Map<String, Integer> lineageTreeNodeData = new HashMap<>(2);
            if (data.getPeptForTaxon() != null) {
                lineageTreeNodeData.put("peptCountForTaxon", data.getPeptForTaxon().size());
            } else {
                lineageTreeNodeData.put("peptCountForTaxon", 0);
            }
            if (data.getPeptForSubtaxa() != null) {
                lineageTreeNodeData.put("peptCountForSubtaxa", data.getPeptForSubtaxa().size());
            } else {
                lineageTreeNodeData.put("peptCountForSubtaxa", 0);
            }
            return lineageTreeNodeData;
        };
        LCAAnalysis.writeTreeWithPlaceholder(root, nodeDataConverter, treePath);
        log.debug("Write quantitative result json file {}", quantPath);
        quantify(root, experiment, quantPath);
    }

    private static Experiment parse(boolean equateIL, Path experimentPath, Path lcaPath)
            throws IOException, InterruptedException {
        /** read headers **/
        Experiment experiment = new Experiment();
        experiment.setEquateIL(equateIL);
        BufferedReader br = Files.newBufferedReader(experimentPath, StandardCharsets.UTF_8);
        List<String> sampleList = Arrays.stream(br.readLine().split(delimiter))
                .skip(1)
                .map(String::trim)
                .collect(Collectors.toList());
        boolean containsQuantData = sampleList.size() != 0 ? true : false;
        if (containsQuantData) {
            experiment.setSampleNameList(sampleList);
        }

        /** read content **/
        String line;
        Integer duplicatePeptCount;
        Map<String, TrypticPeptide> seq2Pept = new HashMap<>();
        Map<String, Integer> duplicatePept2Count = new HashMap<>();
        Set<String> illegalPeptSet = new HashSet<>(); // the peptide length is too short or too long
        int totalPeptCount = 0;
        while ((line = br.readLine()) != null) {
            String[] tmp = line.split(delimiter);
            String seq = equateIL ? tmp[0].trim().replace("I", "L") : tmp[0].trim();
            totalPeptCount++;
            if (NRSearcher.verify(seq)) {
                // quantitative values
                double[] quantVals = null;
                if (containsQuantData) {
                    quantVals = new double[sampleList.size()]; // the default values is 0.0
                    for (int i = 1; i < tmp.length; i++) {
                        quantVals[i - 1] = tmp[i].trim().length() == 0 ? 0.0 : Double.parseDouble(tmp[i]);
                    }
                }
                if (seq2Pept.containsKey(seq)) {
                    if (containsQuantData) {
                        seq2Pept.get(seq).setQuantVals(MathUtil.vectorPlus(seq2Pept.get(seq).getQuantVals(),
                                quantVals));
                    }
                    if ((duplicatePeptCount = duplicatePept2Count.get(seq)) != null) {
                        duplicatePept2Count.put(seq, duplicatePeptCount + 1);
                    } else {
                        duplicatePept2Count.put(seq, 2);
                    }
                } else {
                    TrypticPeptide pept = new TrypticPeptide();
                    pept.setSequence(seq);
                    pept.setQuantVals(quantVals);
                    seq2Pept.put(seq, pept);
                }
            } else {
                illegalPeptSet.add(seq);
                log.debug("peptide <{}> is too short or long and it is ignore!");
            }
        }
        StringJoiner remark = new StringJoiner(System.lineSeparator());
        remark.add("## Equate I and L: " + experiment.isEquateIL());
        if (duplicatePept2Count.size() > 0) {
            remark.add("## " + duplicatePept2Count.size() +
                    " peptides are repeated and their quantitative values are merged: " +
                    duplicatePept2Count.entrySet().stream()
                            .sorted(Comparator.comparing(Map.Entry::getKey))
                            .map(e -> e.getKey() + "[" + e.getValue() + "]")
                            .collect(Collectors.joining(";"))
            );
        }
        if (illegalPeptSet.size() > 0) {
            remark.add("## " + illegalPeptSet.size() +
                    " peptides are ignored because their length are too long(>70) or too short(<5): " +
                    illegalPeptSet.stream()
                            .collect(Collectors.joining(";"))
            );
        }
        duplicatePept2Count = null;
        illegalPeptSet = null;
        Set<TrypticPeptide> peptideSet = new HashSet<>(seq2Pept.values());
        seq2Pept = null;
        br.close();

        /** add LCA info **/
        StringJoiner missedPepts = new StringJoiner(";");
        AtomicInteger missedPeptCount = new AtomicInteger(0);
        Path tmpLCAPath = Files.createTempFile("lca", ".tsv");
        log.debug("Create temporary LCA file <{}>", tmpLCAPath);
        BufferedWriter tmpLCAWriter = Files.newBufferedWriter(tmpLCAPath, StandardCharsets.UTF_8);
        // write LCA and add info into TrypticPeptide except proteinSet(sometime it is to big for memory)
        Set<TrypticPeptide> experimentPeptideSet = new HashSet<>(peptideSet.size());
        Consumer<TrypticPeptide> consumer = writeLCA(missedPepts, missedPeptCount,
                containsQuantData, tmpLCAWriter, experimentPeptideSet);
        nrSearcher.addLCAInfo(peptideSet, equateIL, consumer);
        tmpLCAWriter.close();
        experiment.setPeptideSet(experimentPeptideSet);
        if (missedPeptCount.get() != 0) {
            remark.add("## " + missedPeptCount.get() + " peptides couldn't be found: "
                    + missedPepts.toString()
            );
        }
        remark.add("## " + experiment.getPeptideSet().size() + " of your " + totalPeptCount + " peptides were analyzed");
        // write remark and header
        OutputStream lcaOut = Files.newOutputStream(lcaPath);
        InputStream lcaIn = Files.newInputStream(tmpLCAPath);
        String ranksStr = Arrays.stream(Taxon.Rank.values())
                .map(Taxon.Rank::rankToString)
                .collect(Collectors.joining(delimiter));
        GO.Category[] goCategories = GO.Category.values();
        String header = String.join(delimiter, "Peptide",
                "LCA id",
                "LCA name",
                "LCA rank",
                ranksStr,
                ranksStr,
                "Proteins",
                "Protein count",
                "Protein count with GO",
                Stream.of(goCategories)
                        .map(cat -> "GO (" + cat.toText() + ")")
                        .collect(Collectors.joining(delimiter))
        );
        if (containsQuantData) {
            lcaOut.write((header + delimiter
                    + experiment.getSampleNameList().stream().collect(Collectors.joining(delimiter))
                    + System.lineSeparator()).getBytes(StandardCharsets.UTF_8)
            );
        } else {
            lcaOut.write((header + delimiter + System.lineSeparator()).getBytes(StandardCharsets.UTF_8));
        }
        lcaOut.write((remark.toString() + System.lineSeparator()).getBytes(StandardCharsets.UTF_8));
        byte[] buff = new byte[1024];
        int len;
        while ((len = lcaIn.read(buff)) > 0) {
            lcaOut.write(buff, 0, len);
        }
        lcaOut.close();
        lcaIn.close();
        Files.delete(tmpLCAPath);
        log.debug("Delete temporary LCA file <{}>", tmpLCAPath);
        return experiment;
    }

    /**
     * Group TrypticPeptide list in experiment and build LCA tree.
     *
     * @param experiment
     * @return
     */
    public static TaxonNode<TaxonNodeData> groupByLCA(Experiment experiment) {
        Map<Taxon, List<TrypticPeptide>> taxon2Pepts = experiment.getPeptideSet()
                .stream()
                .collect(Collectors.groupingBy(TrypticPeptide::getLca));
        Set<TaxonNode<TaxonNodeData>> taxonNodeSet = taxon2Pepts.entrySet().stream()
                .map(e -> {
                    TaxonNode<TaxonNodeData> node = new TaxonNode();
                    node.setTaxon(e.getKey());
                    TaxonNodeData data = new TaxonNodeData();
                    data.setPeptForTaxon(e.getValue()); // peptide specific to the taxon
                    node.setNodeData(data);
                    return node;
                })
                .collect(Collectors.toSet());
        TaxonNode<TaxonNodeData> root = LCAAnalysis.toTree(taxonNodeSet);
        updateNodeData(root);
        return root;
    }

    /**
     * Add info: peptides specific to subtaxa of the taxon
     *
     * @param taxonNode
     */
    private static void updateNodeData(TaxonNode<TaxonNodeData> taxonNode) {
        if (taxonNode.getChildren() != null) {
            List<TrypticPeptide> peptForSubtaxa = new ArrayList<>();
            for (TaxonNode<TaxonNodeData> childNode : taxonNode.getChildren()) {
                updateNodeData(childNode);
                if (childNode.getNodeData().getPeptForTaxon() != null) {
                    peptForSubtaxa.addAll(childNode.getNodeData().getPeptForTaxon());
                }
                if (childNode.getNodeData().getPeptForSubtaxa() != null) {
                    peptForSubtaxa.addAll(childNode.getNodeData().getPeptForSubtaxa());
                }
            }
            if (taxonNode.getNodeData() == null) {
                taxonNode.setNodeData(new TaxonNodeData());
            }
            taxonNode.getNodeData().setPeptForSubtaxa(peptForSubtaxa);
        }
    }

    /**
     * write simplified peptide quant info
     * <p>
     * # peptide quant info
     * <p>
     * {
     * "superkingdom" : [ {
     * "taxon" : {
     * "id" : 2,
     * "name" : "Bacteria",
     * "parentId" : 131567,
     * "rank" : "superkingdom",
     * "lineageIdMap" : {
     * "superkingdom" : 2
     * },
     * "lineageNameMap" : {
     * "superkingdom" : "Bacteria"
     * }
     * },
     * "peptCountForTaxon" : 7,
     * "peptCountForSubtaxa" : 32,
     * "quantVals" : [ 824791.0190590002, 815245.636056, ... ]
     * }, ...]
     * ...
     * }
     *
     * @param taxonNode
     * @param experiment
     * @param quantPath
     * @throws IOException
     */
    public static void quantify(TaxonNode<TaxonNodeData> taxonNode,
                                Experiment experiment,
                                Path quantPath) throws IOException {

        QuantResult quantResult = quantify(taxonNode, experiment);
        // write
        JsonGenerator quantWriter = jsonFactory.createGenerator(quantPath.toFile(), JsonEncoding.UTF8);
        quantWriter.writeStartObject();
        quantWriter.writeObjectField("sampleNameList", quantResult.getSampleNameList());
        quantWriter.writeObjectFieldStart("rank2TaxonQuant");
        Map<String, List<TaxonQuant>> rank2TaxonQuant = quantResult.getRank2TaxonQuant();
        List<String> rankNames = new ArrayList<>(ranks.length + 1);
        rankNames.add("root");
        rankNames.addAll(Stream.of(ranks).map(Taxon.Rank::rankToString).collect(Collectors.toList()));
        for (String rankName : rankNames) {
            if (rank2TaxonQuant.containsKey(rankName)) {
                quantWriter.writeArrayFieldStart(rankName);
                for (TaxonQuant taxonQuant : rank2TaxonQuant.get(rankName)) {
                    // quant info
                    quantWriter.writeStartObject();
                    quantWriter.writeObjectField("taxon", taxonQuant.getTaxon());
                    quantWriter.writeNumberField("peptCountForTaxon", taxonQuant.getPeptCountForTaxon());
                    quantWriter.writeNumberField("peptCountForSubtaxa", taxonQuant.getPeptCountForSubtaxa());
                    quantWriter.writeObjectField("quantVals", taxonQuant.getQuantVals());
                    quantWriter.writeEndObject();
                }
                quantWriter.writeEndArray();
            }
        }
        quantWriter.writeEndObject();
        quantWriter.close();
    }

    private static QuantResult quantify(TaxonNode<TaxonNodeData> taxonNode, Experiment experiment) {
        QuantResult quantResult = new QuantResult();
        quantResult.setSampleNameList(experiment.getSampleNameList());
        Map<String, List<TaxonQuant>> rank2TaxonQuant = new HashMap<>(ranks.length);
        quantify(taxonNode,
                experiment.getSampleNameList() != null
                        ? experiment.getSampleNameList().size()
                        : 0,
                rank2TaxonQuant);
        quantResult.setRank2TaxonQuant(rank2TaxonQuant);
        return quantResult;
    }

    /**
     * @param taxonNode
     * @param sampleCount
     * @param rank2TaxonQuant output, rank => taxon quant info on this rank
     */
    private static void quantify(TaxonNode<TaxonNodeData> taxonNode,
                                 int sampleCount,
                                 Map<String, List<TaxonQuant>> rank2TaxonQuant) {
        TaxonQuant taxonQuant = new TaxonQuant();
        String rank = taxonNode.getTaxon().getRank();
        if (!rank2TaxonQuant.containsKey(rank)) {
            rank2TaxonQuant.put(rank, new ArrayList<>());
        }
        rank2TaxonQuant.get(rank).add(taxonQuant);
        taxonQuant.setTaxon(taxonNode.getTaxon());
        double[] quantVals = sampleCount > 0 ? new double[sampleCount] : null;
        if (taxonNode.getNodeData().getPeptForTaxon() != null) {
            taxonQuant.setPeptForTaxon(taxonNode.getNodeData().getPeptForTaxon().parallelStream()
                    .map(TrypticPeptide::getSequence)
                    .toArray(String[]::new)
            );
            if (sampleCount > 0) {
                for (TrypticPeptide pept : taxonNode.getNodeData().getPeptForTaxon()) {
                    quantVals = MathUtil.vectorPlus(quantVals, pept.getQuantVals());
                }
            }
            taxonQuant.setPeptCountForTaxon(taxonQuant.getPeptForTaxon().length);
        } else {
            taxonQuant.setPeptCountForTaxon(0);
        }
        if (taxonNode.getNodeData().getPeptForSubtaxa() != null) {
            taxonQuant.setPeptForSubtaxa(taxonNode.getNodeData().getPeptForSubtaxa().parallelStream()
                    .map(TrypticPeptide::getSequence)
                    .toArray(String[]::new)
            );
            if (sampleCount > 0) {
                for (TrypticPeptide pept : taxonNode.getNodeData().getPeptForSubtaxa()) {
                    quantVals = MathUtil.vectorPlus(quantVals, pept.getQuantVals());
                }
            }
            taxonQuant.setPeptCountForSubtaxa(taxonQuant.getPeptForSubtaxa().length);
        } else {
            taxonQuant.setPeptCountForSubtaxa(0);
        }
        taxonQuant.setQuantVals(quantVals);
        if (taxonNode.getChildren() != null) {
            for (TaxonNode<TaxonNodeData> node : taxonNode.getChildren()) {
                quantify(node, sampleCount, rank2TaxonQuant);
            }
        }
    }

    /**
     * Parse prot file and get protein ids. Then, download sequences by nr-searcher
     *
     * @param protPath
     * @param rank
     * @param minUniqPeptCount
     * @param fastaPath
     * @throws IOException
     */
    public static void getProteins(Path protPath, Taxon.Rank rank, int minUniqPeptCount, Path fastaPath) throws IOException {
        JsonParser jsonParser = jsonFactory.createParser(protPath.toFile());
        Set<String> pidSet = new HashSet<>();
        jsonParser.nextToken();
        while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
            if (jsonParser.getCurrentName().equals(rank.rankToString())) {
                jsonParser.nextToken(); // superkingdom": [
                while (jsonParser.nextToken() != JsonToken.END_ARRAY) { // {
                    jsonParser.nextToken(); // field name: totalPeptCountForTaxon
                    jsonParser.nextToken(); // field value: totalPeptCountForTaxon
                    if (jsonParser.getValueAsInt() >= minUniqPeptCount) {
                        jsonParser.nextToken(); // filed name: proteins
                        jsonParser.nextToken(); // proteins array start: [
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                            pidSet.add(jsonParser.getValueAsString());
                        }
                        jsonParser.nextToken(); // taxon end: }
                        System.out.println();
                    } else {
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY) ; // proteins array end: ]
                        jsonParser.nextToken(); // a taxon end: }
                    }
                }
                jsonParser.close();
                break;
            } else {
                jsonParser.nextToken(); // "superkingdom": [
                while (jsonParser.nextToken() != JsonToken.END_ARRAY) { // START_OBJECT or rank taxon array end: ]
                    while (jsonParser.nextToken() != JsonToken.END_ARRAY) ; // proteins array end: ]
                    jsonParser.nextToken();
                }
            }
        }
        log.info("total {} proteins has been find when rank = {} and unique peptide count >= {}",
                pidSet.size(), rank.rankToString(), minUniqPeptCount);
        nrSearcher.getProteinsByIds(pidSet, fastaPath);
    }

    public static void writeGOAnno(Set<TrypticPeptide> trypticPeptideSet, Path goAnnoPath) throws IOException {
        Map<GO.Category, List<GOWithPerc>> cat2GOWithPerc = trypticPeptideSet.parallelStream()
                .filter(pept -> pept.getGoWithPercList() != null)
                .flatMap(pept -> pept.getGoWithPercList().stream())
                .collect(Collectors.groupingBy(GOWithPerc::getCategory));
        Map<GO.Category, List<GOQuant>> cat2GOQuant = new HashMap<>(3);
        for (GO.Category cat : cat2GOWithPerc.keySet()) {
            List<GOWithPerc> goWithPercList = cat2GOWithPerc.get(cat);
            Map<String, GOQuant> id2GOQuant = new HashMap<>(goWithPercList.size());
            for (GOWithPerc goWithPerc : goWithPercList) {
                if (!id2GOQuant.containsKey(goWithPerc.getId())) {
                    id2GOQuant.put(goWithPerc.getId(),
                            new GOQuant(goWithPerc.getId(), new ArrayList<>()));
                }
                id2GOQuant.get(goWithPerc.getId()).getProtPercList().add(goWithPerc.getProtPerc());
            }
            cat2GOQuant.put(cat, new ArrayList<>(id2GOQuant.values()));
        }
        objectMapper.writeValue(goAnnoPath.toFile(), cat2GOQuant);
    }

    private static Consumer<TrypticPeptide> writeLCA(StringJoiner missedPepts,
                                                     AtomicInteger missedPeptCount,
                                                     boolean containsQuantData,
                                                     BufferedWriter bw,
                                                     Set<TrypticPeptide> experimentPeptideSet) {
        return (TrypticPeptide pept) -> {
            if (pept.getNrProteinSet() == null) {
                missedPepts.add(pept.getSequence());
                missedPeptCount.incrementAndGet();
            } else {
                GO.Category[] goCategories = GO.Category.values();
                // GO annotation
                StringJoiner goAnno = new StringJoiner(delimiter);
                if (pept.getGoWithPercList() != null) {
                    Map<GO.Category, String> cat2GOList = pept.getGoWithPercList().stream()
                            .collect(Collectors.groupingBy(GOWithPerc::getCategory,
                                    Collectors.reducing("",
                                            goWithPerc -> goWithPerc.getId()
                                                    + "[" + decimalFormat.format(goWithPerc.getProtPerc()) + "]",
                                            (v1, v2) -> v1.equals("") ? v1 + v2 : v1 + ";" + v2)
                            ));
                    for (GO.Category c : goCategories) {
                        if (cat2GOList.containsKey(c)) {
                            goAnno.add(cat2GOList.get(c));
                        } else {
                            goAnno.add("");
                        }
                    }
                } else {
                    for (GO.Category c : goCategories) {
                        goAnno.add("");
                    }
                }
                // protein count with GO
                long protCountWithGO = pept.getNrProteinSet().stream()
                        .filter(prot -> prot.getGoArr() != null)
                        .count();
                String proteins = pept.getNrProteinSet().stream()
                        .map(NRProtein::getPid)
                        .collect(Collectors.joining(";"));
                String row = String.join("\t", pept.getSequence(),
                        String.valueOf(pept.getLca().getId()),
                        pept.getLca().getName(),
                        pept.getLca().getRank(),
                        pept.getLca().tname2String(delimiter),
                        pept.getLca().tid2String(delimiter),
                        proteins,
                        String.valueOf(pept.getNrProteinSet().size()),
                        String.valueOf(protCountWithGO),
                        goAnno.toString()
                );
                if (containsQuantData) {
                    row += "\t" + Arrays.stream(pept.getQuantVals())
                            .mapToObj(String::valueOf)
                            .collect(Collectors.joining(delimiter));
                }
                try {
                    bw.write(row + System.lineSeparator());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                pept.setNrProteinSet(null);
                experimentPeptideSet.add(pept);
            }
        };
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    private static class GOQuant {
        String id;
        List<Double> protPercList;
    }
}

@Data
@NoArgsConstructor
class TaxonNodeData {

    // peptide specific to the taxon
    private List<TrypticPeptide> peptForTaxon;

    // peptide specific to subtaxa of the taxon
    private List<TrypticPeptide> peptForSubtaxa;
}

