package cn.phoenixcenter.metaproteomics.nr.model;

import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class TaxonQuant {

    private Taxon taxon;

    private int peptCountForTaxon;

    private int peptCountForSubtaxa;

    // peptide specific to the taxon
    private String[] peptForTaxon;

    // peptide specific to subtaxa of the taxon
    private String[] peptForSubtaxa;

    private double[] quantVals;
}
