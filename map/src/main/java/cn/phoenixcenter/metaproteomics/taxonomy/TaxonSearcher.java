package cn.phoenixcenter.metaproteomics.taxonomy;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Log4j2
public class TaxonSearcher {

    private TransportClient esClient;

    private String taxonomyTreeIndexName;

    private int scrollSize;

    private TimeValue scrollKeepAliveTime;

    /**
     * Get multiple taxon info by id set. The taxon info is set null if a taxon id is not in db.
     * <PRE>
     * If there are no taxa on some ranks for some taxa, the key will be not existed in lineage map.
     * </PRE>
     *
     * @param tidSet
     * @return taxon id => taxon or null
     * @throws IOException
     */
    public Map<Integer, Taxon> getTaxaByIds(Set<Integer> tidSet) {
        /** build search **/
        SearchResponse response = esClient.prepareSearch(taxonomyTreeIndexName)
                .addSort(FieldSortBuilder.DOC_FIELD_NAME, SortOrder.ASC)
                .setScroll(scrollKeepAliveTime)
                .setQuery(QueryBuilders.termsQuery("id", tidSet))
                .setSize(scrollSize)
                .get();

        /** convert search result as object **/
        Map<Integer, Taxon> tid2Taxon = new HashMap<>(response.getHits().getHits().length);
        do {
            for (SearchHit hit : response.getHits().getHits()) {
                Taxon t = source2Taxon(hit.getSourceAsMap());
                tid2Taxon.put(t.getId(), t);
            }
            response = esClient.prepareSearchScroll(response.getScrollId())
                    .setScroll(scrollKeepAliveTime)
                    .get();
        } while (response.getHits().getHits().length != 0);
        for (Integer tid : tidSet) {
            if (!tid2Taxon.containsKey(tid)) {
                tid2Taxon.put(tid, null);
            }
        }
        return tid2Taxon;
    }

    /**
     * Get single taxon info by id
     *
     * @param taxonId
     * @return
     * @throws IOException
     */
    public Taxon getTaxonById(int taxonId) {

        /** build search **/
        SearchResponse response = esClient.prepareSearch(taxonomyTreeIndexName)
                .setQuery(QueryBuilders.termQuery("id", taxonId))
                .setSize(1)
                .get();

        /** convert search result as object **/
        if (response.getHits().getTotalHits() == 0) {
            log.warn("taxon id {} cannot match any taxon", taxonId);
            return null;
        } else {
            return source2Taxon(response.getHits().getAt(0).getSourceAsMap());
        }
    }

    /**
     * Get single taxon info by name
     *
     * @param tname
     * @return
     * @throws IOException
     */
    public Taxon getTaxonByName(String tname) {

        /** build search **/
        SearchResponse response = esClient.prepareSearch(taxonomyTreeIndexName)
                .setQuery(QueryBuilders.termQuery("name", tname))
                .setSize(scrollSize)
                .get();

        /** convert search result as object **/
        if (response.getHits().getTotalHits() == 0) {
            return null;
        } else if (response.getHits().getTotalHits() > 1) {
            throw new RuntimeException(tname + " have multiple match: " +
                    Arrays.stream(response.getHits().getHits())
                            .map(hit -> hit.getSourceAsMap().get("id").toString())
                            .collect(Collectors.joining(";"))
            );
        } else {
            return source2Taxon(response.getHits().getAt(0).getSourceAsMap());
        }
    }

    private Taxon source2Taxon(Map<String, Object> source) {
        Taxon t = new Taxon();
        t.setId((int) source.get("id"));
        t.setName((String) source.get("name"));
        t.setParentId((int) source.get("parentId"));
        t.setRank((String) source.get("rank"));
        t.setLineageNameMap((Map<String, String>) source.get("lineageNameMap"));
        t.setLineageIdMap((Map<String, Integer>) source.get("lineageIdMap"));
        return t;
    }

    /**
     * Get multiple taxon name by id set
     *
     * @param taxonIdSet
     * @return
     * @throws IOException
     */
    public Map<Integer, String> getTaxonNamesByIds(Set<Integer> taxonIdSet) {

        /** build search **/
        SearchResponse response = esClient.prepareSearch(taxonomyTreeIndexName)
                .addSort(FieldSortBuilder.DOC_FIELD_NAME, SortOrder.ASC)
                .setScroll(scrollKeepAliveTime)
                .setQuery(QueryBuilders.termsQuery("id", taxonIdSet))
                .setFetchSource(new String[]{"id", "name"}, null)
                .setSize(scrollSize)
                .get();

        /** convert search result as map **/
        Map<Integer, String> tid2NameMap = new HashMap<>(response.getHits().getHits().length);
        do {
            for (SearchHit hit : response.getHits().getHits()) {
                Map<String, Object> map = hit.getSourceAsMap();
                tid2NameMap.put((int) map.get("id"), (String) map.get("name"));
            }
            response = esClient.prepareSearchScroll(response.getScrollId())
                    .setScroll(scrollKeepAliveTime)
                    .get();
        } while (response.getHits().getHits().length != 0);
        return tid2NameMap;
    }

    /**
     * Get related taxa of the specified taxon. null will be returned if there are no related taxa.
     * The related taxa contains:
     * 1. sibling nodes
     * 2. child nodes
     * <p>
     * Note: don't contain itself
     *
     * @param superTaxon
     * @return
     */
    public Set<Taxon> getRelatedTaxa(Taxon superTaxon) {
        SearchRequestBuilder searchRequest = esClient.prepareSearch(taxonomyTreeIndexName)
                .addSort(FieldSortBuilder.DOC_FIELD_NAME, SortOrder.ASC)
                .setScroll(scrollKeepAliveTime)
                .setSize(scrollSize)
                .setQuery(QueryBuilders.termQuery("lineageIdMap." + superTaxon.getRank(), superTaxon.getId()));
        SearchResponse searchResponse = searchRequest.get();
        if (searchResponse.getHits().getTotalHits() == 0) {
            log.info("find 0 subtaxa for {}", superTaxon.getId());
            return null;
        } else {
            Set<Taxon> relatedTaxonSet = new HashSet<>((int) searchResponse.getHits().getTotalHits());
            while (searchResponse.getHits().getHits().length != 0) {
                SearchHit[] hits = searchResponse.getHits().getHits();
                relatedTaxonSet.addAll(Arrays.stream(hits)
                        .map(hit -> source2Taxon(hit.getSourceAsMap()))
                        .collect(Collectors.toSet())
                );
                searchResponse = esClient.prepareSearchScroll(searchResponse.getScrollId())
                        .setScroll(scrollKeepAliveTime)
                        .get();
            }
            log.info("find {} subtaxa for {}", relatedTaxonSet.size(), superTaxon.getId());
            return relatedTaxonSet.stream()
                    .filter(taxon -> taxon.getId() != superTaxon.getId())
                    .collect(Collectors.toSet());
        }
    }
}
