package cn.phoenixcenter.metaproteomics.taxonomy;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;

public class LCAAnalysis {

    private static final TaxonSearcher taxonSearcher = GlobalConfig.getObj(TaxonSearcher.class);

    private final static Taxon.Rank[] ranks = Taxon.Rank.values();

    private final static JsonFactory jsonFactory = new ObjectMapper().getFactory();
//    /**
//     * Calculate LCA of a group of taxon ids.
//     * <p>Note:
//     * <p>-1 will be returned if there is no LCA.
//     * <p> If LCA locate no rank,
//     * the taxon on the closest rank (direction: species -> superkingdom) will be returned. For example,
//     * <PRE>
//     * input noRankTaxon is taxon1: no rank, lineageIdMap={superkingdom=12, phylum=13, class=14}
//     * return value is taxon2 whose taxon id is 14
//     * </PRE>
//     *
//     * @param taxonMap   taxon id => taxon, the map must contain all taxon ids used to calculate LCA
//     * @param taxonIdArr
//     * @return
//     */
//    public static int calculateLCA(Map<Integer, Taxon> taxonMap, int... taxonIdArr) {
//
//        int lcaTaxonId = -1;
//
//        /** parameter judgement **/
//        if (taxonIdArr.length < 1) {
//            throw new IllegalArgumentException("the input length must >= 1");
//        } else if (taxonIdArr.length == 1) {
//            // find the taxon on the closest rank
//            if (taxonMap.get(taxonIdArr[0]).getRank().equals("no rank")) {
//                lcaTaxonId = taxonMap.get(taxonIdArr[0]).getLineageIdMap().entrySet().stream()
//                        .filter(e -> e.getValue() != null)
//                        .sorted((e1, e2) -> Integer.compare(Taxon.Rank.str2Rank(e2.getKey()).index(),
//                                Taxon.Rank.str2Rank(e1.getKey()).index()))
//                        .limit(1)
//                        .map(e -> e.getValue())
//                        .findFirst()
//                        .get();
//            } else {
//                lcaTaxonId = taxonIdArr[0];
//            }
//        } else {
//            /** taxonIdArr.length > 1 **/
//            // get lineage by taxon id
//            int[][] lineages = new int[taxonIdArr.length][];
//            for (int i = 0; i < taxonIdArr.length; i++) {
//                lineages[i] = taxonMap.get(taxonIdArr[i]).lineageIdMapToArr();
//            }
//
//            // calculate LCA: i is rank, j is sample
//            for (int i = Taxon.Rank.values().length - 1; i >= 0; i--) {
//                int j = 0;
//                for (; j < lineages.length - 1; j++) {
//                    // there is no taxon on current rank
//                    if (lineages[j][i] == -1) {
//                        break;
//                    }
//                    if (lineages[j][i] != lineages[j + 1][i]) {
//                        break;
//                    }
//                }
//                // all taxon are same on current rank, so the common taxon is LCA
//                if (j == lineages.length - 1) {
//                    lcaTaxonId = lineages[0][i];
//                    break;
//                }
//                // if there is no LCA on superkongdom, the LCA is root
//                if (i == 0) {
//                    lcaTaxonId = 1;
//                }
//            }
//        }
//        return lcaTaxonId;
//    }

    /**
     * Calculate LCA of a group of taxon ids using Unipept method:
     * <strong>a gap is treated as separate value except species and genus rank</strong>
     * <p>
     * Note:
     * <p>If LCA locate no rank,
     * the taxon on the closest rank (direction: species -> superkingdom) will be returned, for example,
     * <PRE>
     * input noRankTaxon is taxon1: no rank, lineageIdMap={superkingdom=12, phylum=13, class=14}
     * return value is taxon2 whose taxon id is 14
     * </PRE>
     *
     * @param tid2Taxon  taxon id => taxon, the map must contain all taxon ids used to calculate LCA
     * @param taxonIdSet
     * @return
     */
    public static int calculateUnipeptLCA(Map<Integer, Taxon> tid2Taxon, Set<Integer> taxonIdSet) {
        int lcaTaxonId = 1;
        if (taxonIdSet.size() < 1) {
            throw new IllegalArgumentException("the input length must >= 1");
        } else {
            // key: taxon rank(superkingdom -> species)
            // value: taxon id set on the rank from lineage of input taxon id set
            Map<Taxon.Rank, Set<Integer>> rank2TidsMap = new TreeMap<>(Comparator.comparingInt(Taxon.Rank::index));
            for (Taxon.Rank rank : ranks) {
                rank2TidsMap.put(rank, new HashSet<>(taxonIdSet.size()));
                for (Integer tid : taxonIdSet) {
                    if (tid2Taxon.get(tid).existsTaxonOnRank(rank)
                            && tid2Taxon.get(tid).getLineageIdMap().get(rank.rankToString()) != null) {
                        rank2TidsMap.get(rank).add(tid2Taxon.get(tid).getLineageIdMap().get(rank.rankToString()));
                    } else {
                        // -1 will be set if a taxon's lineage on the rank have no taxon
                        rank2TidsMap.get(rank).add(-1);
                    }
                }
            }
            // scan from superkingdom to species
            Taxon.Rank lcaRank = null;
            Integer[] commonTaxonIds = new Integer[ranks.length];
            for (Map.Entry<Taxon.Rank, Set<Integer>> e : rank2TidsMap.entrySet()) {
                lcaRank = e.getKey();
                if (e.getKey().index() < Taxon.Rank.GENUS.index()) {
                    // if taxon rank is on from superkingdom to family
                    if (e.getValue().size() > 1) {
                        // existing more than 1 taxon suggests the LCA is on previous rank
                        if (e.getKey() == Taxon.Rank.SUPERKINGDOM) {
                            lcaRank = null;
                            lcaTaxonId = 1;
                            break;
                        } else {
                            lcaRank = ranks[e.getKey().index() - 1];
                            for (int i = lcaRank.index(); i >= 0; i--) {
                                if (commonTaxonIds[i] != -1) {
                                    // consider gap from superkingdom to family as a separate value
                                    // so find a rank than have taxon(not -1) from current rank to superkingdom
                                    lcaTaxonId = commonTaxonIds[i];
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    commonTaxonIds[e.getKey().index()] = e.getValue().iterator().next();
                } else {
                    // if taxon rank is on from genus to species: ignore gaps
                    if (e.getValue().size() > 1) {
                        if (e.getValue().size() > 2) {
                            // existing more than 2 taxon suggests the LCA is on previous rank
                            lcaRank = ranks[e.getKey().index() - 1];
                            for (int i = lcaRank.index(); i >= 0; i--) {
                                if (commonTaxonIds[i] != -1) {
                                    lcaTaxonId = commonTaxonIds[i];
                                    break;
                                }
                            }
                            break;
                        } else {
                            // only contain 2 elements
                            Optional<Integer> optional = e.getValue().stream()
                                    .filter(tid -> tid == -1)
                                    .findAny();
                            if (optional.isPresent()) {
                                // if exists a gap in two elements, find LCA on next rank
                                commonTaxonIds[e.getKey().index()] = e.getValue().stream()
                                        .filter(tid -> tid != -1)
                                        .findAny()
                                        .get();
                                continue;
                            } else {
                                // if there is no a gap in two elements, the LCA is on previous rank
                                lcaRank = ranks[e.getKey().index() - 1];
                                for (int i = lcaRank.index(); i >= 0; i--) {
                                    if (commonTaxonIds[i] != -1) {
                                        lcaTaxonId = commonTaxonIds[i];
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    commonTaxonIds[e.getKey().index()] = e.getValue().iterator().next();
                }
            }
            if (lcaRank == Taxon.Rank.SPECIES) {
                // the LCA locate species: maybe [-1, species1], [species1]
                for (int i = lcaRank.index(); i >= 0; i--) {
                    if (commonTaxonIds[i] != -1) {
                        lcaTaxonId = commonTaxonIds[i];
                        break;
                    }
                }
            }
        }
        return lcaTaxonId;
    }


    /**
     * Build tree based on lineage.
     * <p>
     * Note: the method will set taxon(id=1)' s rank as 'root' not 'no rank for visualization
     *
     * @param taxonNodeSet
     * @param <T>
     * @return
     */
    public static <T> TaxonNode<T> toTree(Set<TaxonNode<T>> taxonNodeSet) {
        if (taxonNodeSet == null || taxonNodeSet.size() == 0) {
            throw new IllegalArgumentException("input is empty");
        }
        /** build tree **/
        Map<Integer, TaxonNode<T>> visitedTid2Node = new HashMap<>();
        // root node
        final Taxon rootTaxon = new Taxon(1);
        rootTaxon.setName("root");
        TaxonNode<T> root = taxonNodeSet.stream()
                .filter(node -> node.getTaxon().equals(rootTaxon))
                .findFirst()
                .orElseGet(() -> {
                    TaxonNode<T> taxonNode = new TaxonNode<>();
                    taxonNode.setTaxon(rootTaxon);
                    return taxonNode;
                });
        root.getTaxon().setRank("root");

        for (TaxonNode<T> taxonNode : taxonNodeSet) {
            Taxon taxon = taxonNode.getTaxon();
            Map<Integer, Taxon> tid2Taxon = taxonSearcher.getTaxaByIds(
                    new HashSet<>(taxon.getLineageIdMap().values())
            );
            Taxon.Rank taxonRank = Taxon.Rank.stringToRank(taxon.getRank()); // some taxa with rank not in Taxon.Rank.values()
            int limit = taxonRank != null ? taxonRank.index() : ranks.length - 1;
            for (int i = 0; i <= limit; i++) {
                Taxon.Rank rank = ranks[i];
                if (taxon.existsTaxonOnRank(rank)) {
                    Taxon currTaxon = tid2Taxon.get(taxon.getLineageIdMap().get(rank.rankToString()));
                    if (visitedTid2Node.containsKey(currTaxon.getId())) {
                        // some taxa have been added into tree but the node data have not been added
                      if(rank == taxonRank){
                          visitedTid2Node.get(currTaxon.getId()).setNodeData(taxonNode.getNodeData());
                      }
                    }else{
                        TaxonNode<T> currTaxonNode = null;
                        if (rank == taxonRank) {
                            currTaxonNode = taxonNode; // get original node
                        } else {
                            currTaxonNode = new TaxonNode<>();
                            currTaxonNode.setTaxon(currTaxon);
                        }
                        String parentRank = getParentRank(currTaxon);
                        if (parentRank == null) {
                            if (root.getChildren() == null) {
                                root.setChildren(new ArrayList<>());
                            }
                            root.getChildren().add(currTaxonNode);
                        } else {
                            TaxonNode<T> parentNode = visitedTid2Node
                                    .get(currTaxon.getLineageIdMap().get(parentRank));
                            if (parentNode.getChildren() == null) {
                                parentNode.setChildren(new ArrayList<>());
                            }
                            parentNode.getChildren().add(currTaxonNode);
                        }
                        visitedTid2Node.put(currTaxon.getId(), currTaxonNode);
                    }
                }
            }
        }
        return root;
    }

    private static String getParentRank(Taxon taxon) {
        String parentRank = null;
        for (int i = Taxon.Rank.stringToRank(taxon.getRank()).index() - 1; i >= 0; i--) {
            if (taxon.getLineageIdMap().containsKey(ranks[i].rankToString())) {
                parentRank = ranks[i].rankToString();
                break;
            }
        }
        return parentRank;
    }

    /**
     * Write tree with placeholder into json file. Insert placeholder for visualization
     * if there is no taxon on rank superkingdom->species.
     *
     * @param root
     * @param nodeDataConverter
     * @param treePath
     * @param <T>               the type of node nodeData saved into tree node
     * @param <D>               the type of node nodeData need to be write into file
     * @throws IOException
     */
    public static <T, D> void writeTreeWithPlaceholder(TaxonNode<T> root,
                                                       Function<T, D> nodeDataConverter,
                                                       Path treePath) throws IOException {
        JsonGenerator jsonGen = jsonFactory.createGenerator(treePath.toFile(), JsonEncoding.UTF8);
        jsonGen.writeStartObject();
        jsonGen.writeObjectField("taxon", root.getTaxon());
        jsonGen.writeObjectField("nodeData", nodeDataConverter.apply(root.getNodeData()));
        if (root.getChildren() != null) {
            jsonGen.writeArrayFieldStart("children");
            for (TaxonNode<T> taxonNode : root.getChildren()) {
                writeTreeWithPlaceholder(taxonNode, Taxon.Rank.SUPERKINGDOM, nodeDataConverter, jsonGen);
            }
            jsonGen.writeEndArray();
        }
        jsonGen.writeEndObject();
        jsonGen.close();
    }

    private static <T, D> void writeTreeWithPlaceholder(TaxonNode<T> taxonNode,
                                                        Taxon.Rank rank,
                                                        Function<T, D> nodeDataConverter,
                                                        JsonGenerator jsonGen) throws IOException {
        Taxon.Rank taxonRank = Taxon.Rank.stringToRank(taxonNode.getTaxon().getRank());
        if (taxonRank == rank) {
            // don't need to add null and placeholder node, so recursive its children
            jsonGen.writeStartObject();
            jsonGen.writeObjectField("taxon", taxonNode.getTaxon());
            jsonGen.writeObjectField("nodeData", nodeDataConverter.apply(taxonNode.getNodeData()));
            if (taxonNode.getChildren() != null) {
                jsonGen.writeArrayFieldStart("children");
                for (TaxonNode<T> childNode : taxonNode.getChildren()) {
                    writeTreeWithPlaceholder(childNode, ranks[rank.index() + 1], nodeDataConverter, jsonGen);
                }
                jsonGen.writeEndArray();
            }
            jsonGen.writeEndObject();
        } else {
            // current rank is absent, so need to add a placeholder node
            jsonGen.writeStartObject(); // { placeholder
            jsonGen.writeBooleanField("placeholder", true);
            // only for filter
            jsonGen.writeObjectField("nodeData", nodeDataConverter.apply(taxonNode.getNodeData()));
            for (int i = rank.index() + 1; i < taxonRank.index(); i++) {
                jsonGen.writeArrayFieldStart("children");
                jsonGen.writeStartObject();
                jsonGen.writeBooleanField("placeholder", true);
                jsonGen.writeObjectField("nodeData", nodeDataConverter.apply(taxonNode.getNodeData()));
            }
            jsonGen.writeArrayFieldStart("children"); // [ placeholder
            jsonGen.writeStartObject(); // taxonNode {
            jsonGen.writeObjectField("taxon", taxonNode.getTaxon());
            jsonGen.writeObjectField("nodeData", nodeDataConverter.apply(taxonNode.getNodeData()));
            if (taxonNode.getChildren() != null) {
                for (TaxonNode<T> childNode : taxonNode.getChildren()) {
                    jsonGen.writeArrayFieldStart("children");
                    writeTreeWithPlaceholder(childNode, taxonRank, nodeDataConverter, jsonGen);
                    jsonGen.writeEndArray();
                }
            }
            jsonGen.writeEndObject(); // } taxonNode
            for (int i = rank.index(); i < taxonRank.index(); i++) {
                jsonGen.writeEndArray(); // ] placeholder
                jsonGen.writeEndObject();
            }
        }
    }

    public static <T, V> void writeSankeyGraph(TaxonNode<T> root, Function<TaxonNode<T>, V> linkValFun, String jsonFile) throws IOException {
        JsonGenerator jsonGen = jsonFactory.createGenerator(new File(jsonFile), JsonEncoding.UTF8);
        List<TaxonNode<T>> sankeyNodeList = new ArrayList<>();
        sankeyNodeList.add(new TaxonNode<>(root.getTaxon(), root.getNodeData()));
        jsonGen.writeStartObject(); // {

        /** links **/
        jsonGen.writeArrayFieldStart("links"); //  "links" : [
        for (TaxonNode<T> taxonNode : root.getChildren()) {
            writeSankeyGraph(root, taxonNode, sankeyNodeList, linkValFun, jsonGen);
        }
        // write placeholder

        jsonGen.writeEndArray();

        /** nodes **/
        jsonGen.writeArrayFieldStart("nodes"); // "nodes" : [
        for (TaxonNode<T> sankeyNode : sankeyNodeList) {
            jsonGen.writeStartObject();
            jsonGen.writeObjectField("taxon", sankeyNode.getTaxon());
            jsonGen.writeObjectField("nodeData", sankeyNode.getNodeData());
            jsonGen.writeEndObject();
        }
        jsonGen.writeEndArray();
        jsonGen.writeEndObject();
        jsonGen.close();
    }

    private static <T, V> void writeSankeyGraph(TaxonNode<T> sourceNode, TaxonNode<T> targetNode,
                                                List<TaxonNode<T>> sankeyNodeList,
                                                Function<TaxonNode<T>, V> linkValFun,
                                                JsonGenerator jsonGen) throws IOException {
        jsonGen.writeStartObject();
        jsonGen.writeNumberField("source", sourceNode.getTaxon().getId());
        jsonGen.writeNumberField("target", targetNode.getTaxon().getId());
        jsonGen.writeObjectField("value", linkValFun.apply(targetNode));
        jsonGen.writeEndObject();
        sankeyNodeList.add(targetNode);
        if (targetNode.getChildren() != null) {
            for (TaxonNode<T> taxonNode : targetNode.getChildren()) {
                writeSankeyGraph(targetNode, taxonNode, sankeyNodeList, linkValFun, jsonGen);
            }
        }
    }
}