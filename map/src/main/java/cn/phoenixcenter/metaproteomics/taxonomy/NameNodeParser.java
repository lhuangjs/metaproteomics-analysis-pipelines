package cn.phoenixcenter.metaproteomics.taxonomy;

import cn.phoenixcenter.metaproteomics.utils.ESUtil;
import cn.phoenixcenter.metaproteomics.utils.model.ESIndex;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * The class is used to parse NCBI taxonomy tree files(names.dmp and nodes.dmp).
 */

@Log4j2
@NoArgsConstructor
public class NameNodeParser {

    private final Pattern pattern = Pattern.compile("\\|");

    @Getter
    private final InputStream taxTreeConfStream = NameNodeParser.class
            .getResourceAsStream("/es_mappings/taxonomy-tree.json");

    private List<String> rankList = Arrays.stream(Taxon.Rank.values())
            .map(Taxon.Rank::rankToString)
            .collect(Collectors.toList());

    /**
     * Parse NCBI taxonomy tree files.
     *
     * @param namesFile names.dmp file location
     * @param nodesFile nodes.dmp file location
     * @return
     * @throws IOException
     */
    public List<Taxon> parse(String namesFile, String nodesFile) throws IOException {
        return parse(new FileInputStream(namesFile), new FileInputStream(nodesFile));
    }

    public List<Taxon> parse(InputStream namesStream, InputStream nodesStream) throws IOException {

        // taxon id => Taxon
        Map<Integer, Taxon> taxonMap = new HashMap<>();

        BufferedReader namesReader = new BufferedReader(new InputStreamReader(namesStream));
        BufferedReader nodesReader = new BufferedReader(new InputStreamReader(nodesStream));
        String line;

        while ((line = nodesReader.readLine()) != null) {
            String[] tmp = pattern.split(line);
            int taxonId1 = Integer.parseInt(tmp[0].trim());
            int parentTaxonId = Integer.parseInt(tmp[1].trim());
            String rank = tmp[2].trim();
            int taxonId2 = 0;
            String clazz = null;
            String taxonName = null;
            while (!"scientific name".equals(clazz) && (line = namesReader.readLine()) != null) {
                tmp = pattern.split(line);
                clazz = tmp[3].trim();
                taxonName = tmp[1].trim();
                taxonId2 = Integer.parseInt(tmp[0].trim());
            }
            if ("scientific name".equals(clazz) && taxonId2 == taxonId1) {
                Taxon taxon = new Taxon();
                taxon.setId(taxonId1);
                taxon.setName(taxonName);
                taxon.setRank(rank);
                taxon.setParentId(parentTaxonId);
                taxonMap.put(taxonId1, taxon);
            } else {
                throw new RuntimeException("Taxon " + taxonId1 + " did not have scientific name");
            }
        }
        namesReader.close();
        nodesReader.close();
        log.debug("finish taxonomy tree read ...");
        return setLineage(taxonMap);
    }

    private List<Taxon> setLineage(Map<Integer, Taxon> taxonMap) {
        List<Taxon> taxonList = new ArrayList<>(taxonMap.values());
        for (Taxon taxon : taxonList) {
            int[] lineage = new int[rankList.size()];
            createLineage(taxonMap, taxon, lineage);
            // set taxon
            Map<String, Integer> lineageIdMap = new HashMap<>(rankList.size());
            Map<String, String> lineageNameMap = new HashMap<>(rankList.size());
            for (int i = 0; i < rankList.size(); i++) {
                // the default value is 0 in int array, so 0 suggest there no taxon on current rank
                if (lineage[i] != 0) {
                    String rankStr = rankList.get(i);
                    lineageIdMap.put(rankStr, lineage[i]);
                    lineageNameMap.put(rankStr, taxonMap.get(lineage[i]).getName());
                }
            }
            taxon.setLineageIdMap(lineageIdMap);
            taxon.setLineageNameMap(lineageNameMap);
        }
        log.debug("finish taxonomy lineage set ...");
        return taxonList;
    }

    private void createLineage(Map<Integer, Taxon> taxonMap, Taxon taxon, int[] lineage) {
        if (taxon.getId() == taxon.getParentId()) {
            if (rankList.contains(taxon.getRank())) {
                lineage[Taxon.Rank.stringToRank(taxon.getRank()).index()] = taxon.getId();
            }
        } else {
            if (Taxon.Rank.SUPERKINGDOM.rankToString().equals(taxon.getRank())) {
                lineage[Taxon.Rank.stringToRank(taxon.getRank()).index()] = taxon.getId();
            } else {
                // only obtain the specific rank
                if (rankList.contains(taxon.getRank())) {
                    lineage[Taxon.Rank.stringToRank(taxon.getRank()).index()] = taxon.getId();
                }
                createLineage(taxonMap, taxonMap.get(taxon.getParentId()), lineage);
            }
        }
    }

    /**
     * Store taxonomy tree into ES
     *
     * @param esClient
     * @param bulkSize
     * @param bulkActions
     * @param taxonomyTreeIndexName
     * @param taxonList
     * @throws IOException
     * @throws InterruptedException
     */
    public void storeTaxonomyTree(
            TransportClient esClient,
            ByteSizeValue bulkSize,
            int bulkActions,
            ESIndex taxIndex,
            List<Taxon> taxonList) throws InterruptedException {
        log.debug("start store taxon list<total:{}> into ES", taxonList.size());

        /** transform and store **/
        BulkProcessor bulkProcessor = BulkProcessor.builder(esClient, ESUtil.getDefaultBulkProcessorListener())
                .setBulkSize(bulkSize)
                .setBulkActions(bulkActions)
                .setConcurrentRequests(taxIndex.getPrimaryShardsNum())
                .build();
        taxonList.parallelStream()
                .map(t -> {
                    try {
                        XContentBuilder content = XContentFactory.jsonBuilder()
                                .startObject()
                                .field("id", t.getId())
                                .field("name", t.getName())
                                .field("parentId", t.getParentId())
                                .field("rank", t.getRank())
                                // lineage id map
                                .field("lineageIdMap")
                                .startObject();
                        for (String rank : rankList) {
                            if (t.getLineageIdMap().containsKey(rank)) {
                                content.field(rank, t.getLineageIdMap().get(rank));
                            }
                        }
                        content.endObject()
                                //lineage name map
                                .field("lineageNameMap")
                                .startObject();
                        for (String rank : rankList) {
                            if (t.getLineageNameMap().containsKey(rank)) {
                                content.field(rank, t.getLineageNameMap().get(rank));
                            }
                        }
                        content.endObject()
                                .endObject();
                        return content;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .forEach(content -> bulkProcessor.add(new IndexRequest(taxIndex.getIndexName(), taxIndex.getTypeName())
                        .source(content)));
        bulkProcessor.flush();
        bulkProcessor.awaitClose(10, TimeUnit.MINUTES);
        esClient.admin().indices().prepareRefresh().get();
    }
}
