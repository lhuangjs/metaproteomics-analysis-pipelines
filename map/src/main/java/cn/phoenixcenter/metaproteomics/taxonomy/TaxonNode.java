package cn.phoenixcenter.metaproteomics.taxonomy;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class TaxonNode<T> {

    private Taxon taxon;

    private T nodeData;

    private List<TaxonNode<T>> children;

    public TaxonNode(Taxon taxon, T nodeData) {
        this.taxon = taxon;
        this.nodeData = nodeData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaxonNode<?> taxonNode = (TaxonNode<?>) o;
        return Objects.equals(taxon, taxonNode.taxon);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taxon);
    }
}
