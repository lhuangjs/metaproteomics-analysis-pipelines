package cn.phoenixcenter.metaproteomics.taxonomy;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@ToString
public class Taxon {

    @NonNull
    private int id;

    private String name;

    private int parentId;

    private String rank; // String type allows other ranks to be set but Rank.values().

    private Map<String, Integer> lineageIdMap;

    private Map<String, String> lineageNameMap;

    public Taxon(Taxon taxon) {
        this.id = taxon.id;
        this.name = taxon.name;
        this.parentId = taxon.parentId;
        this.rank = taxon.rank;
        if (taxon.lineageIdMap != null) {
            this.lineageIdMap = taxon.lineageIdMap.entrySet()
                    .stream()
                    .collect(Collectors.toMap(
                            e -> e.getKey(),
                            e -> e.getValue()
                    ));
        }
        if (taxon.lineageNameMap != null) {
            this.lineageNameMap = taxon.lineageNameMap.entrySet()
                    .stream()
                    .collect(Collectors.toMap(
                            e -> e.getKey(),
                            e -> e.getValue()
                    ));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Taxon taxon = (Taxon) o;
        return id == taxon.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public enum Rank {
        SUPERKINGDOM,
        PHYLUM,
        CLASS,
        ORDER,
        FAMILY,
        GENUS,
        SPECIES;

        private static Rank[] ranks = Rank.values();
        private static Map<Rank, Integer> indices = new HashMap<>();

        static {
            for (int i = 0; i < ranks.length; i++) {
                indices.put(ranks[i], i);
            }
        }

        public int index() {
            return indices.get(this);
        }

        public static Rank stringToRank(String rankStr) {
            try {
                return Rank.valueOf(rankStr.toUpperCase());
            } catch (IllegalArgumentException e) {
                return null;
            }
        }

        public String rankToString() {
            return this.name().toLowerCase();
        }

        @JsonValue
        @Override
        public String toString() {
            return this.rankToString();
        }
    }

    public boolean existsTaxonOnRank(String rank) {
        return lineageIdMap.containsKey(rank);
    }

    public boolean existsTaxonOnRank(Rank rank) {
        return lineageIdMap.containsKey(rank.rankToString());
    }

    public Integer[] tid2Table() {
        Rank[] ranks = Rank.values();
        Integer[] tidArr = new Integer[ranks.length];
        for (int i = 0; i < ranks.length; i++) {
            tidArr[i] = this.lineageIdMap.get(ranks[i].rankToString());
        }
        return tidArr;
    }

    public String[] tname2Table() {
        Rank[] ranks = Rank.values();
        String[] tnameArr = new String[ranks.length];
        for (int i = 0; i < ranks.length; i++) {
            tnameArr[i] = this.lineageNameMap.get(ranks[i].rankToString());
        }
        return tnameArr;
    }

    public String tid2String(String delimiter) {
        Rank[] ranks = Rank.values();
        StringJoiner joiner = new StringJoiner(delimiter);
        for (int i = 0; i < ranks.length; i++) {
            if (this.lineageIdMap.get(ranks[i].rankToString()) != null) {
                joiner.add(String.valueOf(this.lineageIdMap.get(ranks[i].rankToString())));
            } else {
                joiner.add("");
            }
        }
        return joiner.toString();
    }

    public String tname2String(String delimiter) {
        Rank[] ranks = Rank.values();
        StringJoiner joiner = new StringJoiner(delimiter);
        for (int i = 0; i < ranks.length; i++) {
            if (this.lineageNameMap.get(ranks[i].rankToString()) != null) {
                joiner.add(String.valueOf(this.lineageNameMap.get(ranks[i].rankToString())));
            } else {
                joiner.add("");
            }
        }
        return joiner.toString();
    }

    public static Map<String, Integer> string2LineageIdMap(String[] idArr) {
        Rank[] ranks = Rank.values();
        Map<String, Integer> lineageIdMap = new HashMap<>(ranks.length);
        for (int i = 0; i < idArr.length; i++) {
            if (idArr[i] != null && idArr[i].length() > 0) {
                lineageIdMap.put(ranks[i].rankToString(),
                        Integer.parseInt(idArr[i]));
            }
        }
        return lineageIdMap;
    }

    public static Map<String, String> string2LineageNameMap(String[] nameArr) {
        Rank[] ranks = Rank.values();
        Map<String, String> lineageNameMap = new HashMap<>(ranks.length);
        for (int i = 0; i < nameArr.length; i++) {
            if (nameArr[i] != null && nameArr[i].length() > 0) {
                lineageNameMap.put(ranks[i].rankToString(), nameArr[i]);
            }
        }
        return lineageNameMap;
    }
}
