package cn.phoenixcenter.metaproteomics.inference.algorithm.fido;


import cn.phoenixcenter.metaproteomics.pipeline.converter.peptide.PercolatorConverter;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.Fido;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model.BipartiteGraphWithWeight;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model.PSMNode;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model.ProteinNode;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by huangjs
 */
public class FidoTest {

    @Test
    public void findCuttingEdges() {
        BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> graph = new BipartiteGraphWithWeight<>();
        ProteinNode protNode1 = new ProteinNode("prot1");
        ProteinNode protNode2 = new ProteinNode("prot2");
        ProteinNode protNode3 = new ProteinNode("prot3");
        PSMNode psmNode1 = new PSMNode("p1");
        PSMNode psmNode2 = new PSMNode("p2");
        graph.addEdge(protNode1, psmNode1, 10.0);
        graph.addEdge(protNode1, psmNode2, 20.0);
        graph.addEdge(protNode2, psmNode1, 20.0);
        graph.addEdge(protNode2, psmNode2, 10.0);
        graph.addEdge(protNode3, psmNode2, 10.0);

        System.out.println(graph);

        Fido fido = new Fido();
        Map<PSMNode, ProteinNode> allCutPointList = fido.findCuttingEdges(graph);
        // prot3 - p2
        System.out.println(allCutPointList);
    }

    @Test
    public void findCuttingEdges2() {
        BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> graph = new BipartiteGraphWithWeight<>();
        ProteinNode protNode1 = new ProteinNode("prot1");
        ProteinNode protNode2 = new ProteinNode("prot2");
        PSMNode psmNode1 = new PSMNode("p1");
        PSMNode psmNode2 = new PSMNode("p2");
        PSMNode psmNode3 = new PSMNode("p3");
        graph.addEdge(protNode1, psmNode1, 10.0);
        graph.addEdge(protNode1, psmNode2, 20.0);
        graph.addEdge(protNode2, psmNode2, 20.0);
        graph.addEdge(protNode2, psmNode3, 10.0);

        System.out.println(graph);

        Fido fido = new Fido();
        Map<PSMNode, ProteinNode> allCutPointList = fido.findCuttingEdges(graph);
        // prot2 - p2
        System.out.println(allCutPointList);
    }

    @Test
    public void findCuttingEdges3() {
        BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> graph = new BipartiteGraphWithWeight<>();
        ProteinNode protNode1 = new ProteinNode("prot1");
        ProteinNode protNode2 = new ProteinNode("prot2");
        ProteinNode protNode3 = new ProteinNode("prot3");
        ProteinNode protNode4 = new ProteinNode("prot4");
        PSMNode psmNode1 = new PSMNode("p1");
        PSMNode psmNode2 = new PSMNode("p2");
        PSMNode psmNode3 = new PSMNode("p3");
        graph.addEdge(protNode1, psmNode1, 10.0);
        graph.addEdge(protNode1, psmNode2, 20.0);
        graph.addEdge(protNode2, psmNode1, 20.0);
        graph.addEdge(protNode2, psmNode2, 10.0);
        graph.addEdge(protNode3, psmNode2, 10.0);
        graph.addEdge(protNode4, psmNode2, 10.0);
        graph.addEdge(protNode4, psmNode3, 10.0);

        System.out.println(graph);

        Fido fido = new Fido();
        Map<PSMNode, ProteinNode> allCutPointList = fido.findCuttingEdges(graph);
        // p2 - pro4
        System.out.println(allCutPointList);
    }

    @Test
    public void analyzeMascotResult() throws IOException {
        String mpTargetFile_ = "/home/huangjs/Documents/MascotPercolator/demo2/F001652.peptides";
        String mpDecoyFile_ = "/home/huangjs/Documents/MascotPercolator/demo2/F001652_decoy.peptides";
        String decoyProtPrefix = "REV__";

        Fido fido = new Fido(";", decoyProtPrefix, 0.1);
        PercolatorConverter converter = new PercolatorConverter();
        BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> graph = converter.convertMPToFido(mpTargetFile_, mpDecoyFile_, decoyProtPrefix);
        // generate subgraph
        List<BipartiteGraphWithWeight<ProteinNode, PSMNode, Double>> subgraphList = fido.initialize(graph);
        fido.pruningByThreshold(subgraphList);
//        // calculate protein probabilities
//        FidoModel fidoModel = new FidoModel(0.008, 0.001, 0.5, 0.1);
//        int i = 0;
//        for(BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> subgraph : subgraphList) {
//            System.out.println(++i);
//            fido.probRGivenD(subgraph, fidoModel);
//            System.out.println(
//                    subgraph.getVertexCollectionA().keySet().stream()
//                            .map(protNode -> protNode.getId() + "[" + protNode.getPosteriorErrorProb() + "]")
//                            .collect(Collectors.joining("\t"))
//            );
//        }
    }

    @Test
    public void convertMPFeatureFile() throws IOException {
        String featureFile = "/home/huangjs/Documents/MascotPercolator/demo2/percolator_features4683187764887955747.tmp.txt";
        AtomicInteger target = new AtomicInteger(0);
        AtomicInteger decoy = new AtomicInteger(0);
        List<String> lines = Files.readAllLines(Paths.get(featureFile))
                .stream()
                .map(line -> {
                    String[] tmp = line.split("\t");
                    if (tmp[0].startsWith("query")) {
                        if (tmp[1].equals("1")) {
                            target.incrementAndGet();
                            return line;
                        } else {
                            decoy.incrementAndGet();
                            for (int i = 61; i < tmp.length; i++) {
                                tmp[i] = "REV__" + tmp[i];
                            }
                            return String.join("\t", tmp);
                        }
                    } else {
                        return line;
                    }
                })
                .collect(Collectors.toList());
        Files.write(Paths.get("/home/huangjs/Documents/MascotPercolator/demo2/percolator_features.tsv"), lines,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        System.out.println("target: " + target.get());
        System.out.println("decoy: " + decoy.get());
    }
}