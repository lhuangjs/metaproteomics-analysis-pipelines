package cn.phoenixcenter.metaproteomics.inference.algorithm.picked_protein;

import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.EnzymeDigester;
import org.junit.Assert;
import org.junit.Test;

import java.util.AbstractMap;
import java.util.List;

/**
 * Created by huangjs
 */
public class EnzymeDigesterTest {

    @Test
    public void digest() {
        EnzymeDigester digester = new EnzymeDigester(2, 6, 50);
        List<AbstractMap.SimpleEntry<String, Boolean>> peptideList = digester.digest("MAAAAAAAAAAAAARNNRTTRNNRTTRTK");
        System.out.println(peptideList);
        Assert.assertEquals(15, peptideList.size());
    }

    @Test
    public void degest2() {
        String pept = "MCCTPQCCPSCCHTRCCTTRWCTTKPCCPQCCSSVCCAPQ" +
                "CCSTSSCTTVTTRPCCTNQCSTRCYVPACSSSQGCSSGCSTQGCCSSGCCTPQCCPTSCCSP" +
                "QCCSTVCTPQCCTTRRCCPQCCNSGCSQNLCGPLCVTTPYYCTRPCCTTECCARCCSPQCCSSVCTPQFCTT";
        EnzymeDigester digester = new EnzymeDigester(2, 6, 50);
        List<AbstractMap.SimpleEntry<String, Boolean>> peptideList = digester.digest(pept);
        peptideList.forEach(e -> System.out.println(e.getKey() + " " + e.getKey().length()));
        System.out.println(peptideList.size());
    }
}