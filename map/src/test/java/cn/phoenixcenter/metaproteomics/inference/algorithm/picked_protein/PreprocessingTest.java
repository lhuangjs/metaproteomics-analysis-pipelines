package cn.phoenixcenter.metaproteomics.inference.algorithm.picked_protein;

import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.EnzymeDigester;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.Preprocessing;
import cn.phoenixcenter.metaproteomics.utils.FileReaderUtil;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.AbstractMap;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by huangjs
 */
public class PreprocessingTest {

    @Test
    public void parseMaxQuantPeptidesFile() throws IOException {
        /** parse peptides.txt **/
        String peptidesFile = "/home/huangjs/metaproteomics/row_data/PXD006118/maxquant/Run1_U1_PSM-1_Prot-1/peptides.txt";
        String fastaFile = "/home/huangjs/metaproteomics/row_data/PXD006118/maxquant/Mock_Comm_RefDB_V3.fasta";
        Preprocessing preprocessing = new Preprocessing();
        preprocessing.parseMaxQuantPeptidesFile(peptidesFile, fastaFile, true,
                "REV__",
                EnzymeDigester.Enzyme.TRYPSIN,
                "./results/inference/mq-pept2prots.json");
//        Files.write(Paths.get("./results/inference/mq-pept2prots.json"),
//                peptideList.parallelStream()
//                        .map(pept -> pept.getSequence() + "\t" + pept.getAsscociatedProtIdSet()
//                                .stream()
//                                .sorted()
//                                .collect(Collectors.joining(";"))
//                        )
//                        .sorted()
//                        .collect(Collectors.toList()),
//                StandardCharsets.UTF_8,
//                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    @Test
    public void mqPeptides() throws IOException {
        String peptidesFile = "/home/huangjs/metaproteomics/row_data/PXD006118/maxquant/Run1_U1_PSM-1_Prot-1/peptides.txt";
        final FileReaderUtil fr = new FileReaderUtil(peptidesFile, "\t", true);
        Predicate<String[]> rowDeleter = (String[] fields) -> {
            // remove peptide that "Potential contaminant" is "+"
            if (fields[fr.getColNum("Potential contaminant")].equals("+")) {
                return true;
            }
            // remove peptide whose score is NaN
            if (fields[fr.getColNum("Score")].equals("NaN")) {
                return true;
            }
            return false;
        };
        // obtain peptide info
        Function<String[], AbstractMap.SimpleEntry<String, Boolean>> rowProcessor = (String[] fields) -> {
            Boolean flag = fields[fr.getColNum("Reverse")].equals("+") ? false : true; // true - target
            String score = "-" + fields[fr.getColNum("Score")];
            return new AbstractMap.SimpleEntry<>(score, flag);
        };
        // read peptides
        List<AbstractMap.SimpleEntry<String, Boolean>> peptideList = fr.read(rowDeleter, rowProcessor);
        Path targetPath = Files.createTempFile("target", ".txt");
        Path decoyPath = Files.createTempFile("decoy", ".txt");
        System.out.println(targetPath);
        System.out.println(decoyPath);
        BufferedWriter targetBW = Files.newBufferedWriter(targetPath);
        BufferedWriter decoyBW = Files.newBufferedWriter(decoyPath);
        for (AbstractMap.SimpleEntry<String, Boolean> entry : peptideList) {
            if (entry.getValue()) {
                targetBW.write(entry.getKey() + System.lineSeparator());
            } else {
                decoyBW.write(entry.getKey() + System.lineSeparator());
            }
        }
        targetBW.close();
        decoyBW.close();
    }
}