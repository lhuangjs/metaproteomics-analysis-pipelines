//package cn.phoenixcenter.metaproteomics.inference.algorithm;
//
//import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.ParsimonyAlgorithm;
//import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.bipartite_graph.BipartiteGraph;
//import cn.phoenixcenter.metaproteomics.pipeline.inference.model.Protein;
//import cn.phoenixcenter.metaproteomics.pipeline.inference.model.ProteinGroup;
//import cn.phoenixcenter.metaproteomics.pipeline.inference.model.ProteinWithRelationship;
//import cn.phoenixcenter.metaproteomics.pipeline.inference.preprocessing.PeptideListParser;
//import org.junit.Before;
//import org.junit.Test;
//
//import java.io.IOException;
//import java.utils.List;
//
//public class BipartiteGraphTest {
//
//    BipartiteGraph bipartite;
//    ParsimonyAlgorithm parsimony;
//    PeptideListParser parser;
//
//
//    @Before
//    public void startUp() throws IOException {
//        parsimony = new ParsimonyAlgorithm();
//        parser = new PeptideListParser();
//        // parse peptide list
////        String pepListFile = this.getClass().getResource("/inference/pep2pros.tsv")
////                .getFile();
////        processor.parsePep2Pros(pepListFile);
//        String pepListFile = this.getClass().getResource("/inference/pep2pro.tsv")
//                .getFile();
//        parser.parsePep2Pro(pepListFile);
//
//        // perform parsimony algorithm
//        parsimony.addParsimonyTypeInfo(parser.getPeptideSet(), parser.getProteinSet());
//    }
//
//    @Test
//    public void process() {
//        bipartite = new BipartiteGraph();
//        List<ProteinGroup> pgList = bipartite.process(parsimony.getProteinWithRelationshipSet());
//        bipartite.updateParsimonyType(pgList);
//        for (ProteinGroup pg : pgList) {
//            System.out.println(pg.fPrint());
//        }
//    }
//
//    @Test
//    public void test() {
//        Protein protein = new Protein("12");
//        ProteinWithRelationship proteinWithRelationship = new ProteinWithRelationship("12");
//        Protein protein2 = new ProteinWithRelationship("12");
//
//        System.out.println(protein.getClass());
//        System.out.println(proteinWithRelationship.getClass());
//        System.out.println(protein2.getClass());
//
//        System.out.println();
//        System.out.println(protein instanceof Protein);
//        System.out.println(protein2 instanceof Protein);
//        System.out.println(protein2 instanceof ProteinWithRelationship);
//        System.out.println(protein instanceof ProteinWithRelationship);
//
//        System.out.println();
//        System.out.println(protein.equals(proteinWithRelationship));
//        System.out.println(proteinWithRelationship.equals(protein));
//        System.out.println(protein.equals(protein2));
//        System.out.println(protein2.equals(protein));
//    }
//}