package cn.phoenixcenter.metaproteomics.inference.preprocessing;

import cn.phoenixcenter.metaproteomics.pipeline.inference.model.Protein;
import cn.phoenixcenter.metaproteomics.pipeline.inference.preprocessing.PeptideListParser;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
public class PeptideListParserTest {

    PeptideListParser parser = new PeptideListParser();

    @Test
    public void parsePep2Library() throws IOException {
        List<String> pepList = Files.readAllLines(
                Paths.get(this.getClass().getResource("/inference/PXD006118-peptides.txt").getFile())
        );
        String libraryFile = "/home/huangjs/documents/java/metaproteomics/row_data/PXD006118/Mock_Comm_RefDB_V3.fasta.txt";
        parser.parsePep2Library(true, new HashSet<>(pepList), libraryFile);
        List<String> lines =
                parser.getPeptideSet()
                        .parallelStream()
                        .sorted(Comparator.comparing(peptide -> peptide.getSequence()))
                        .map(
                                peptide ->
                                        String.join("\t",
                                                peptide.getSequence(),
                                                peptide.getAssociatedProteinSet().stream()
                                                        .map(Protein::getId)
                                                        .collect(Collectors.joining(";"))
                                        )

                        )
                        .collect(Collectors.toList());
        Files.write(Paths.get("./results/inference/peptides.tab"), lines);
    }

    /**
     * Show difference between my pep2pro and maxquant pep2pro
     *
     * @throws IOException
     */
    @Test
    public void differece() throws IOException {
        Map<String, String> file1 = Files.readAllLines(Paths.get("./results/inference/peptides.tab"))
                .parallelStream()
                .map(line -> line.split("\t"))
                .collect(Collectors.toMap(
                        tmp -> tmp[0].replace("I", "L"),
                        tmp -> {
                            if (tmp.length > 1) {
                                return tmp[0] + "\t" + Arrays.stream(tmp[1].split(";")).sorted().collect(Collectors.joining(";"));
                            } else {
                                return tmp[0];
                            }
                        }
                ));
        Map<String, String> file2 = Files.readAllLines(Paths.get("./results/inference/peptides-maxquant.tab"))
                .parallelStream()
                .map(line -> line.split("\t"))
                .collect(Collectors.toMap(
                        tmp -> tmp[0].replace("I", "L"),
                        tmp -> {
                            if (tmp.length > 1) {
                                return tmp[0] + "\t" + Arrays.stream(tmp[1].split(";")).sorted().collect(Collectors.joining(";"));
                            } else {
                                return tmp[0];
                            }
                        }
                ));
        List<String> lines = file1.entrySet().stream()
                .filter(e -> {
                    if (file1.get(e.getKey()).split("\t").length == 2
                            && file2.get(e.getKey()).split("\t").length == 2) {
                        return !file1.get(e.getKey()).split("\t")[1]
                                .equals(file2.get(e.getKey()).split("\t")[1]);
                    } else if (file1.get(e.getKey()).split("\t").length == 1
                            && file2.get(e.getKey()).split("\t").length == 1) {
                        return false;
                    } else {
                        return true;
                    }
                })
                .map(e -> String.join("\t",
                        file1.get(e.getKey()).split("\t").length == 2 ? file1.get(e.getKey()) : file1.get(e.getKey()) + "\t ",
                        file2.get(e.getKey()).split("\t").length == 2 ? file2.get(e.getKey()) : file2.get(e.getKey()) + "\t "
                        )
                )
                .collect(Collectors.toList());
        lines.add(0, String.join("\t", "my_peptide", "my_pros", "mq_peptide", "mq_pros"));
        Files.write(Paths.get("./results/inference/peptides.diff.tab"), lines);
    }
}