package cn.phoenixcenter.metaproteomics.inference.algorithm;

import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.ParsimonyAlgorithm;
import cn.phoenixcenter.metaproteomics.pipeline.inference.model.Protein;
import cn.phoenixcenter.metaproteomics.pipeline.inference.preprocessing.PeptideListParser;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class ParsimonyAlgorithmTest {

    ParsimonyAlgorithm parsimony;
    PeptideListParser parser;

    @Before
    public void startUp() {
        parsimony = new ParsimonyAlgorithm();
        parser = new PeptideListParser();
    }

    @Test
    public void addParsimonyTypeInfo() throws IOException {
        // parse peptide list
        String pep2ProFile = this.getClass().getResource("/inference/pep2pro.tsv")
                .getFile();
        parser.parsePep2Pro(pep2ProFile);
//        String pep2ProsFile = this.getClass().getResource("/inference/pep2pros.tsv")
//                .getFile();
//        processor.parsePep2Pros(pep2ProsFile);


        // perform parsimony algorithm
        parsimony.addParsimonyTypeInfo(parser.getPeptideSet(), parser.getProteinSet());
        for (Protein pro : parser.getProteinSet()) {
            System.out.println(pro);
        }
    }
}