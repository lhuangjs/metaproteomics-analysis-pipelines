package cn.phoenixcenter.metaproteomics.inference.algorithm.picked_protein;

import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.EnzymeDigester;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.PickedProteinAlgorithm;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.Preprocessing;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.model.Peptide;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.picked_protein.model.Protein;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by huangjs
 */
public class PickedProteinAlgorithmTest {

    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void processPercolator() throws IOException, InterruptedException {
        EnzymeDigester digester = new EnzymeDigester(2, 6, 50);
        PickedProteinAlgorithm pickedProtein = new PickedProteinAlgorithm("REV1_", digester);
//        String fastaFile = this.getClass().getResource("/inference/picked_protein/picked-protein.fasta").getPath();
//        String fastaFile = this.getClass().getResource("/inference/picked-protein.fasta").getPath();
//        String percolatorPSMFile = this.getClass().getResource("/inference/picked-protein-psmCount.tab").getPath();

        String fastaFile = "/home/huangjs/Documents/percolator/picked-protein-test.fasta";
        String targetPeptFile = this.getClass().getResource("/inference/picked_protein/pept_target.tab").getPath();
        String decoyPeptFile = this.getClass().getResource("/inference/picked_protein/pept_decoy.tab").getPath();

        /** group fasta library **/
        // TODO modify steps
//        pickedProtein.mapPept2Prot(fastaFile, false);

        // output protein group with at least 1 protein members
        System.out.println("============representative protein => protein group============");
        Set<List<String>> pgSet = new HashSet<>(pickedProtein.getPid2ProteinGroup().values());
        for (List<String> e : pgSet) {
            if (e.size() >= 1) {
                System.out.println(e.get(0) + " => " + e);
            }
        }

        /** obtain unique peptides and corresponding proteins **/
        Preprocessing preprocessing = new Preprocessing();
        List<Peptide> peptideList = preprocessing.parsePercolatorTabFile(targetPeptFile, decoyPeptFile);
        pickedProtein.filterProtein(peptideList);

        /** erase proteins **/
        pickedProtein.pickedProteinStrategy();

        /** calculate probability **/
        pickedProtein.calProbabilities();

        for (Protein prot : pickedProtein.getRepProteinList()) {
            System.out.println(prot);
        }
    }

    @Test
    public void processMaxQuant() throws IOException, InterruptedException {
        /** parse peptides.txt **/
        String peptidesJsonFile = "./results/inference/mq-pept2prots.json";
        String fastaFile = "/home/huangjs/metaproteomics/row_data/PXD006118/maxquant/Mock_Comm_RefDB_V3.fasta";
        Preprocessing preprocessing = new Preprocessing();
        List<Peptide> peptideList = preprocessing.parseGenericJsonFile(peptidesJsonFile);

        /** perform picked protein algorithm **/
        PickedProteinAlgorithm pickedProtein = new PickedProteinAlgorithm("REV__", preprocessing.getEnzymeDigester());

        // group library
        Path pid2PepsTempJsonFile = pickedProtein.mapPept2Prot(fastaFile, true);
        // Path pid2PepsTempJsonFile = Paths.get("/tmp/pid2pepts2352257073960949056.json");
        pickedProtein.organizeProteinGroup(pid2PepsTempJsonFile);
        // filter protein by unique peptide
        pickedProtein.filterProtein(peptideList);
        // erase proteins
        pickedProtein.pickedProteinStrategy();
        // calculate probability
        pickedProtein.calProbabilities();
        // write result
        List<String> lines = pickedProtein.getRepProteinList().stream()
                .map(prot -> String.join("\t", prot.getId(), String.valueOf(prot.getScore()),
                        String.valueOf(prot.getPosteriorErrorProb()),
                        String.valueOf(prot.getQValue()))
                )
                .collect(Collectors.toList());
        Files.write(Paths.get("./results/inference/picked-protein.tab"), lines);
        File jsonFile = new File("./results/inference/picked-protein.json");
        objectMapper.writeValue(jsonFile, pickedProtein.getRepProteinList());
    }

    @Test
    // mascot -> MascotPercolator -> picked protein
    public void mpPipeline() throws IOException, InterruptedException {
        String javaHome = "/home/huangjs/software/jdk1.8.0_191/";
        String datFile = "/home/huangjs/Documents/MascotPercolator/demo2/F001652.dat";
        String mpLocation = "/home/huangjs/Documents/MascotPercolator/MascotPercolator.2.16/";
        String mpTargetFile = "/home/huangjs/Documents/MascotPercolator/demo2/F001652";
        String mpTargetFile_ = "/home/huangjs/Documents/MascotPercolator/demo2/F001652.peptides";
        String mpDecoyFile_ = "/home/huangjs/Documents/MascotPercolator/demo2/F001652_decoy.peptides";
        String decoyProtPrefix = "REV__";
        String ppInputFile = "/home/huangjs/Documents/MascotPercolator/demo2/picked-protein-inout.json";
        String fastaLibrary = "/home/huangjs/Documents/MascotPercolator/demo/Mock_Comm_RefDB_V3.fasta";
        String output = "/home/huangjs/Documents/MascotPercolator/demo2/my_picked_protein.tsv";

//        MascotPercolator mp = new MascotPercolator(javaHome, mpLocation);
//        mp.run(datFile, datFile, mpTargetFile);
//
//        PercolatorConverter plConverter = new PercolatorConverter();
//        plConverter.convertMPToPickedProtein(mpTargetFile_, mpDecoyFile_, decoyProtPrefix,
//                EnzymeDigester.Enzyme.TRYPSIN, ppInputFile);

        PickedProteinAlgorithm pp = new PickedProteinAlgorithm();
        pp.setDecoyProtPrefix(decoyProtPrefix);
        pp.run(ppInputFile, fastaLibrary, true);
        pp.exportReport(output);
    }
}