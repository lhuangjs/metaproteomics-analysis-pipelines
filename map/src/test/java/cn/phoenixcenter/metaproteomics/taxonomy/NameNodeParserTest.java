package cn.phoenixcenter.metaproteomics.taxonomy;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.utils.ESUtil;
import cn.phoenixcenter.metaproteomics.utils.model.ESIndex;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class NameNodeParserTest {

    @Test
    public void storeTaxonomyTree() throws IOException, InterruptedException {
        NameNodeParser nameNodeParser = new NameNodeParser();
        String namesFile = "/home/huangjs/data250/metadata/nr/data/names.dmp";
        String nodesFile = "/home/huangjs/data250/metadata/nr/data/nodes.dmp";
        List<Taxon> taxonList = nameNodeParser.parse(namesFile, nodesFile);
        /** create taxonomy tree Index if it dose not exist**/
        ESIndex taxIndex = ESUtil.createIndex(
                GlobalConfig.getObj(TransportClient.class),
                "taxonomy-tree-v1",
                nameNodeParser.getTaxTreeConfStream(),
                true);
        nameNodeParser.storeTaxonomyTree(
                GlobalConfig.getObj(TransportClient.class),
                new ByteSizeValue(20, ByteSizeUnit.MB),
                5000,
                taxIndex,
                taxonList
        );
    }
}
