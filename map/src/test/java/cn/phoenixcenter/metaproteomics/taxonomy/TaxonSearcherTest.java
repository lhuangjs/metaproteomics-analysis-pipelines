package cn.phoenixcenter.metaproteomics.taxonomy;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.utils.FileReaderUtil;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaxonSearcherTest {

    TaxonSearcher taxonSearcher;

    @Before
    public void setUp() {
        taxonSearcher = GlobalConfig.getObj(TaxonSearcher.class);

    }

    @Test
    public void getTaxaByIds() {
        Set<Integer> idSet = new HashSet<>(4);
        idSet.add(175421);
        idSet.add(860116);
        idSet.add(-1);
        System.out.println(taxonSearcher.getTaxaByIds(idSet));
    }

    @Test
    public void getTaxaByIds2() {
        String[] ranks = Arrays.stream(Taxon.Rank.values()).map(Taxon.Rank::rankToString).toArray(String[]::new);
        Set<Integer> idSet = Stream.of(176299, 1004788, 224308, 266265, 3055, 536, 266264, 882, 83333, 228410,
                44577, 926571, 323848, 266, 101570, 1283336, 329852, 1977402, 10754, 1294143, 294, 1149133, 216596,
                1041145, 1907202, 99287, 46170, 46170, 1407502, 262724)
                .collect(Collectors.toSet());
        Map<Integer, Taxon> tid2Taxon = taxonSearcher.getTaxaByIds(idSet);
        // print result
        String delimiter = "\t";
        System.out.println(String.join(delimiter, "taxon id", "taxon name")
                + delimiter
                + Arrays.stream(ranks).collect(Collectors.joining(delimiter)));
        for (Map.Entry<Integer, Taxon> e : tid2Taxon.entrySet()) {
            System.out.print(e.getKey() + delimiter);
            System.out.print(e.getValue().getName() + delimiter);
            for (int i = 0; i < ranks.length; i++) {
                if (e.getValue().getLineageIdMap().containsKey(ranks[i])) {
                    System.out.print(e.getValue().getLineageIdMap().get(ranks[i]));
                }
                System.out.print(i == ranks.length - 1 ? "" : delimiter);
            }
            System.out.println();
        }
    }

    @Test
    public void getTaxaByIds3() throws IOException {
        String[] ranks = Arrays.stream(Taxon.Rank.values()).map(Taxon.Rank::rankToString).toArray(String[]::new);
        List<Integer> tidList = Stream.of(358, 28108, 1423, 36873, 3055, 536, 119219, 881, 562, 915, 44577, 1034015,
                1231, 266, 101570, 1985310, 329852, 1977402, 10754, 1294143, 294,
                1149133, 384, 384, 1907202, 28901, 1280, 1280, 40324, 274
        )
                .collect(Collectors.toList());
        Map<Integer, Taxon> tid2Taxon = taxonSearcher.getTaxaByIds(new HashSet<>(tidList));
        // print result
        Path path = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/biomass_lineage.tsv");
        List<String> lines = new ArrayList<>(tidList.size());
        String delimiter = "\t";
        lines.add(String.join(delimiter, "taxon id", "taxon name")
                + delimiter
                + Arrays.stream(ranks).collect(Collectors.joining(delimiter)));
        for (Integer tid : tidList) {
            Taxon taxon = tid2Taxon.get(tid);
            StringJoiner joiner = new StringJoiner(delimiter);
            joiner.add(String.valueOf(taxon.getId()));
            joiner.add(taxon.getName());
            joiner.add(taxon.tname2String(delimiter));
            lines.add(joiner.toString());
        }
        Files.write(path, lines);
    }

    @Test
    public void getTaxaByIds4() throws IOException {
        String[] ranks = Arrays.stream(Taxon.Rank.values()).map(Taxon.Rank::rankToString).toArray(String[]::new);
        List<Integer> tidList = Stream.of(28125, 813)
                .collect(Collectors.toList());
        Map<Integer, Taxon> tid2Taxon = taxonSearcher.getTaxaByIds(new HashSet<>(tidList));
        // print result
        Path path = Paths.get("/home/huangjs/Documents/biomass/lca_lineage.tsv");
        List<String> lines = new ArrayList<>(tidList.size());
        String delimiter = "\t";
        lines.add(String.join(delimiter, "taxon id", "taxon name")
                + delimiter
                + Arrays.stream(ranks).collect(Collectors.joining(delimiter)));
        for (Integer tid : tidList) {
            Taxon taxon = tid2Taxon.get(tid);
            StringJoiner joiner = new StringJoiner(delimiter);
            joiner.add(String.valueOf(taxon.getId()));
            joiner.add(taxon.getName());
            joiner.add(taxon.tname2String(delimiter));
            lines.add(joiner.toString());
        }
        Files.write(path, lines);
    }

    @Test
    public void getSubTaxonById() throws IOException {
        Taxon taxon = new Taxon();
        taxon.setId(46147);
        taxon.setRank("species");
        Set<Taxon> taxonSet = taxonSearcher.getRelatedTaxa(taxon);
        Files.write(Paths.get("/home/huangjs/Documents/metaprot/homologous/taxon-46147.tsv"), taxonSet.stream()
                .map(t -> Arrays.stream(t.tname2Table()).collect(Collectors.joining("\t")))
                .collect(Collectors.toList())
        );
    }

    @Test
    public void tname2Taxon() throws IOException {
        FileReaderUtil reader = new FileReaderUtil("/home/huangjs/metadata/taxonomy/user2/mpa_result.csv",
                ",", true);
        BufferedWriter bw = Files.newBufferedWriter(Paths.get("/home/huangjs/metadata/taxonomy/user2/mpa_result_rank.csv"));
        Consumer<String> consumer = (String line) -> {
            try {
                int start = line.indexOf(",") + 1;
                int end = line.indexOf(",", start);
                String tname = line.substring(start, end);
                Taxon taxon = taxonSearcher.getTaxonByName(tname);
                if (taxon != null) {
                    bw.write(line.substring(0, end + 1) + taxon.getRank() + line.substring(end) + System.lineSeparator());
                } else {
                    bw.write(line.substring(0, end + 1) + "" + line.substring(end) + System.lineSeparator());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
        reader.read(consumer);
        bw.close();
    }
}