package cn.phoenixcenter.metaproteomics.taxonomy;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class LCAAnalysisTest {

    TaxonSearcher taxonSearcher;


    @Before
    public void setUp() throws Exception {
        taxonSearcher = GlobalConfig.getObj(TaxonSearcher.class);
    }

    /**
     * superkingdom=2
     * superkingdom=2
     * phylum=1239
     * phylum=1239
     * class=186801
     * class=186801
     * order=186802
     * order=186802
     * family=186803
     * family=186803
     * genus=572511 *
     * genus=572511
     * species=1262756
     * species=33039
     *
     * @throws IOException
     */
    @Test
    public void calculateUnipeptLCA() {
        Set<Integer> idSet = new HashSet<>();
        idSet.add(33039);
        idSet.add(1262756);
        Map<Integer, Taxon> taxonMap = taxonSearcher.getTaxaByIds(idSet);
        taxonMap.values().stream()
                .flatMap(t -> t.getLineageIdMap().entrySet().stream())
                .sorted(Comparator.comparingInt(e -> Taxon.Rank.stringToRank(e.getKey()).index()))
                .forEach(System.out::println);
        int lca = LCAAnalysis.calculateUnipeptLCA(taxonMap, idSet);
        System.out.println(taxonSearcher.getTaxonById(lca));
    }

    /**
     * "family" rank is absent for all taxa
     * superkingdom=2
     * superkingdom=2
     * phylum=1239
     * phylum=1239
     * class=91061
     * class=91061
     * order=1385
     * order=1385
     * genus=1378 *
     * genus=1378
     * species=84136
     * species=1379
     */
    @Test
    public void calculateUnipeptLCA2() {
        Set<Integer> idSet = new HashSet<>();
        idSet.add(1321820);
        idSet.add(1379);
        Map<Integer, Taxon> taxonMap = taxonSearcher.getTaxaByIds(idSet);
        taxonMap.values().stream()
                .flatMap(t -> t.getLineageIdMap().entrySet().stream())
                .sorted(Comparator.comparingInt(e -> Taxon.Rank.stringToRank(e.getKey()).index()))
                .forEach(System.out::println);
        int lca = LCAAnalysis.calculateUnipeptLCA(taxonMap, idSet);
        System.out.println(taxonSearcher.getTaxonById(lca));
    }

    /**
     * There is a gap on family rank
     * superkingdom=2
     * superkingdom=2
     * phylum=1239
     * phylum=1239
     * class=91061
     * class=91061
     * order=1385 *
     * order=1385
     * family=186823
     * genus=1502725
     * genus=1378
     * species=1117041
     * species=1379
     *
     * @throws IOException
     */
    @Test
    public void calculateUnipeptLCA3() {
        Set<Integer> idSet = new HashSet<>();
        idSet.add(1117041);
        idSet.add(1379);
        Map<Integer, Taxon> taxonMap = taxonSearcher.getTaxaByIds(idSet);
        taxonMap.values().stream()
                .flatMap(t -> t.getLineageIdMap().entrySet().stream())
                .sorted(Comparator.comparingInt(e -> Taxon.Rank.stringToRank(e.getKey()).index()))
                .forEach(System.out::println);
        int lca = LCAAnalysis.calculateUnipeptLCA(taxonMap, idSet);
        System.out.println(taxonSearcher.getTaxonById(lca));
    }

    /**
     * Need search for a taxon on the closest rank with genus
     * superkingdom=2
     * superkingdom=2
     * phylum=1239
     * phylum=1239
     * class=186801
     * class=186801
     * order=186802
     * order=186802
     * family=186803 *
     * family=186803
     * species=742723
     * species=397287
     *
     * @throws IOException
     */
    @Test
    public void calculateUnipeptLCA4() {
        Set<Integer> idSet = new HashSet<>();
        idSet.add(742723);
        idSet.add(397287);
        Map<Integer, Taxon> taxonMap = taxonSearcher.getTaxaByIds(idSet);
        taxonMap.values().stream()
                .flatMap(t -> t.getLineageIdMap().entrySet().stream())
                .sorted(Comparator.comparingInt(e -> Taxon.Rank.stringToRank(e.getKey()).index()))
                .forEach(System.out::println);
        int lca = LCAAnalysis.calculateUnipeptLCA(taxonMap, idSet);
        System.out.println(taxonSearcher.getTaxonById(lca));
    }

    /**
     * There is only one gap on genus rank
     * superkingdom=2
     * superkingdom=2
     * phylum=1239
     * phylum=1239
     * class=186801
     * class=186801
     * order=186802
     * order=186802
     * family=186803
     * family=186803
     * genus=1427378 *
     * species=397287
     * species=879566
     *
     * @throws IOException
     */
    @Test
    public void calculateUnipeptLCA5() {
        Set<Integer> idSet = new HashSet<>();
        idSet.add(879566);
        idSet.add(397287);
        Map<Integer, Taxon> taxonMap = taxonSearcher.getTaxaByIds(idSet);
        taxonMap.values().stream()
                .flatMap(t -> t.getLineageIdMap().entrySet().stream())
                .sorted(Comparator.comparingInt(e -> Taxon.Rank.stringToRank(e.getKey()).index()))
                .forEach(System.out::println);
        int lca = LCAAnalysis.calculateUnipeptLCA(taxonMap, idSet);
        System.out.println(taxonSearcher.getTaxonById(lca));
    }

    @Test
    public void calculateUnipeptLCA6() {
        Set<Integer> idSet = new HashSet<>();
        idSet.add(397287);
        Map<Integer, Taxon> taxonMap = taxonSearcher.getTaxaByIds(idSet);
        taxonMap.values().stream()
                .flatMap(t -> t.getLineageIdMap().entrySet().stream())
                .sorted(Comparator.comparingInt(e -> Taxon.Rank.stringToRank(e.getKey()).index()))
                .forEach(System.out::println);
        int lca = LCAAnalysis.calculateUnipeptLCA(taxonMap, idSet);
        System.out.println(taxonSearcher.getTaxonById(lca));
    }

    @Test
    public void calculateUnipeptLCA7() {
        Set<Integer> idSet = new HashSet<>();
        idSet.add(536);
        idSet.add(3055);
        Map<Integer, Taxon> taxonMap = taxonSearcher.getTaxaByIds(idSet);
        taxonMap.values().stream()
                .flatMap(t -> t.getLineageIdMap().entrySet().stream())
                .sorted(Comparator.comparingInt(e -> Taxon.Rank.stringToRank(e.getKey()).index()))
                .forEach(System.out::println);
        int lca = LCAAnalysis.calculateUnipeptLCA(taxonMap, idSet);
        System.out.println(taxonSearcher.getTaxonById(lca));
    }

    @Test
    public void toTree() throws IOException {
        Set<String> rankSet = Arrays.stream(Taxon.Rank.values()).map(Taxon.Rank::rankToString).collect(Collectors.toSet());
        Random random = new Random(1);
        Set<Taxon> taxonSet = new HashSet<>();
        while (taxonSet.size() < 30) {
            Taxon taxon = taxonSearcher.getTaxonById(random.nextInt(99999));
            if (taxon != null && rankSet.contains(taxon.getRank()) && !taxonSet.contains(taxon)) {
                taxonSet.add(taxon);
            }
        }
        // print taxon
        Files.write(Paths.get("/home/huangjs/Documents/metaprot/nr-searcher/lineage.tsv"),
                taxonSet.stream().map(taxon -> Arrays.stream(taxon.tid2Table())
                        .map(String::valueOf)
                        .collect(Collectors.joining("\t"))
                ).collect(Collectors.toList())
        );
//        taxonSet.add(taxonSearcher.getTaxonById(313));
//        taxonSet.add(taxonSearcher.getTaxonById(1205));
//        taxonSet.add(taxonSearcher.getTaxonById(1415));
//        taxonSet.add(taxonSearcher.getTaxonById(1428));
//        taxonSet.add(taxonSearcher.getTaxonById(1452));

        Set<TaxonNode<Integer>> taxonNodeSet = new HashSet<>();
        for (Taxon taxon : taxonSet) {
            TaxonNode<Integer> taxonNode = new TaxonNode();
            taxonNode.setTaxon(taxon);
            taxonNode.setNodeData(random.nextInt(30));
            taxonNodeSet.add(taxonNode);
        }
        Path treePath = Paths.get("/home/huangjs/coding/vue/d3-practice/d3-demo/tree/tree1.json");
        TaxonNode<Integer> root = LCAAnalysis.toTree(taxonNodeSet);
        Function<Integer, Integer> noteDataConverter = (Integer i) -> i;
        LCAAnalysis.writeTreeWithPlaceholder(root, noteDataConverter, treePath);
    }

    @Test
    public void writeTreeWithPlaceholder() throws IOException {
        Set<Taxon> taxonSet = new HashSet<>();
        taxonSet.add(taxonSearcher.getTaxonById(683));
        taxonSet.add(taxonSearcher.getTaxonById(87));
        Set<TaxonNode<Integer>> taxonNodeSet = new HashSet<>();
        for (Taxon taxon : taxonSet) {
            TaxonNode<Integer> taxonNode = new TaxonNode();
            taxonNode.setTaxon(taxon);
            taxonNode.setNodeData(10);
            taxonNodeSet.add(taxonNode);
        }
        Path treePath = Paths.get("/home/huangjs/coding/vue/d3-practice/d3-demo/tree/tree1.json");
        TaxonNode<Integer> root = LCAAnalysis.toTree(taxonNodeSet);
        Function<Integer, Integer> noteDataConverter = (Integer i) -> i;
        LCAAnalysis.writeTreeWithPlaceholder(root, noteDataConverter, treePath);
    }

    @Test
    public void writeSankeyGraph() throws IOException {
        Set<Taxon> taxonSet = new HashSet<>();
        taxonSet.add(taxonSearcher.getTaxonById(683));
//        taxonSet.add(taxonSearcher.getTaxonById(87));
        Set<TaxonNode<Integer>> taxonNodeSet = new HashSet<>();
        for (Taxon taxon : taxonSet) {
            TaxonNode<Integer> taxonNode = new TaxonNode();
            taxonNode.setTaxon(taxon);
            taxonNode.setNodeData(10);
            taxonNodeSet.add(taxonNode);
        }
//        String treeJsonFile = "/home/huangjs/Documents/metaprot/nr-searcher/tree-with-placeholder.json";
        String treeJsonFile = "/home/huangjs/coding/vue/d3-practice/d3-demo/sankey/sankey.json";
        TaxonNode<Integer> root = LCAAnalysis.toTree(taxonNodeSet);
        root.setNodeData(10);
        LCAAnalysis.writeSankeyGraph(root,
                (TaxonNode<Integer> node) -> node.getNodeData() == null ? 0 : node.getNodeData().intValue(),
                treeJsonFile);
    }
}