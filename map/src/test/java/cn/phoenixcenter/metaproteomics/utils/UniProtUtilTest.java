package cn.phoenixcenter.metaproteomics.utils;

import org.junit.Assert;
import org.junit.Test;
import uk.ac.ebi.uniprot.dataservice.client.exception.ServiceException;

import java.io.IOException;
import java.util.*;

public class UniProtUtilTest {

    @Test
    public void accessionToTaxonId() throws IOException {
        Assert.assertEquals(UniProtUtil.accToTaxonByHistory("X6R7M0"), 9606);
        Assert.assertEquals(UniProtUtil.accToTaxonByHistory("NO"), -1);
        UniProtUtil.accToTaxonByHistory("Q13428-7");
    }

    @Test
    public void accToTaxonIdByJAPI() throws ServiceException {
        Set<String> accSet = new HashSet<>();
        accSet.add("O60238-2");
        Map<String, Integer> accTaxonIdMap = UniProtUtil.accToTaxonIdByJAPI(accSet);
        for (String acc : accSet) {
            System.out.println(acc + "=>" + (accTaxonIdMap.containsKey(acc) ?
                    accTaxonIdMap.get(acc)
                    : ""));
        }
    }

    @Test
    public void accToTaxonIdByRest1() {
        Map.Entry<String, Integer> entry1 = UniProtUtil.accToTaxonIdByRest("ENSEMBL:ENSP00000377550");
        Assert.assertNull(entry1);
    }

    @Test
    public void accToTaxonIdByRest2() {
        List<String> accList = new ArrayList<>();
        accList.add("O60238-2");
        accList.add("NO");
        Map<String, Integer> accTaxonIdMap = UniProtUtil.accToTaxonIdByRest(accList);
        for (String acc : accTaxonIdMap.keySet()) {
            System.out.println(acc + "=>" + accTaxonIdMap.get(acc));
        }
    }
}