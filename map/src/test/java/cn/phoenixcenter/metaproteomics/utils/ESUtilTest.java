package cn.phoenixcenter.metaproteomics.utils;

import cn.phoenixcenter.metaproteomics.utils.model.ESIndex;
import org.junit.Test;

import java.io.IOException;

public class ESUtilTest {

    @Test
    public void parseJsonIndex() throws IOException {
        ESIndex index = ESUtil.parseJsonIndex("nr",
                ESUtil.class.getResourceAsStream("/es_mappings/nr.json"));
        System.out.println(index);
    }
}