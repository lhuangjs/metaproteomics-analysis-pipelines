package cn.phoenixcenter.metaproteomics.dental.utils;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class CollectionCalculationTest {

    @Test
    public void calDifferenceSet() throws IOException {
        String file1 = CollectionCalculationTest.class.getResource("/dental/genera-from-paper.txt").getPath();
        String file2 = CollectionCalculationTest.class.getResource("/dental/genera-from-species.txt").getPath();
        System.out.println("===remove redundancy: set 1===");
        List<String> rs1 = CollectionCalculation.removeRedundancy(file1, true);
        System.out.println("set 1 has " + rs1.size() + " elements");

        System.out.println("===remove redundancy: set 2===");
        List<String> rs2 = CollectionCalculation.removeRedundancy(file2, true);
        rs2.forEach(System.out::println);
        System.out.println("set 2 has " + rs2.size() + " elements");

        System.out.println("===(set1 - set2)===");
        List<String> rs3 = CollectionCalculation.calDifferenceSet(file1, file2, true);
        System.out.println("(set1 - set2) has " + rs3.size() + " elements");
        rs3.forEach(System.out::println);

        System.out.println("===(set2 - set1)===");
        List<String> rs4 = CollectionCalculation.calDifferenceSet(file2, file1, true);
        System.out.println("(set2 - set1) has " + rs4.size() + " elements");
        rs4.forEach(System.out::println);
    }

    @Test
    public void conpareMyVsPaper() throws IOException {
        String file1 = CollectionCalculationTest.class.getResource("/dental/genera-from-paper.txt").getPath();
        String file2 = CollectionCalculationTest.class.getResource("/dental/genera-from-my-filter.txt").getPath();

        // paper - my
        List<String> rs1 = CollectionCalculation.calDifferenceSet(file1, file2, true);
        System.out.println("(===(paper - my): " + rs1.size() +"===");
        rs1.stream()
                .sorted()
                .forEach(System.out::println);

        // my - paper
        List<String> rs2 = CollectionCalculation.calDifferenceSet(file2, file1, true);
        System.out.println("(===(my - paper): " + rs2.size() + "===");
        rs2.stream()
                .sorted()
                .forEach(System.out::println);


    }
}