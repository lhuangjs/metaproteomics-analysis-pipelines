package cn.phoenixcenter.metaproteomics.dental.utils;

import org.junit.Test;

import java.io.IOException;


public class TSVToXLSXTest {

    @Test
    public void parse() throws IOException {
        String inputFile = "/home/huangjs/coding/java/java-web/pipeline-reconstruction/src/main/resources/dental/proteinGroups_workfile.txt";
        String outputFile = "/home/huangjs/coding/java/java-web/pipeline-reconstruction/src/main/resources/dental/proteinGroups_workfile.xlsx";
        TSVToXLSX.parse(inputFile, outputFile);
    }
}