package cn.phoenixcenter.metaproteomics.dental;

import cn.phoenixcenter.metaproteomics.dental.utils.FileReaderUtil;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class AnalysisTest {

    private Analysis analysis = new Analysis();
    private String proteinGroupsFile = "/home/huangjs/coding/java/java-web/pipeline-reconstruction/src/main/resources/dental/proteinGroups_workfile.txt";

    @Test
    public void bacterialDistribution() throws IOException {
        Map<String, double[]> taxonIntensityMap = analysis.bacterialDistribution(proteinGroupsFile);

        Path path = Paths.get("/home/huangjs/coding/java/java-web/pipeline-reconstruction/src/main/resources/dental/taxon-distribution-my.tsv");
        Files.deleteIfExists(path);
        for (String taxon : taxonIntensityMap.keySet().stream().sorted().collect(Collectors.toList())) {
            List<Double> intensityList = Arrays.stream(taxonIntensityMap.get(taxon))
                    .mapToObj(Double::valueOf)
                    .collect(Collectors.toList());
            long calcFre = intensityList.subList(0, 7)
                    .stream()
                    .filter(d -> Double.compare(d, 0.0) != 0)
                    .count();
            long plaqueFre = intensityList.subList(7, 14)
                    .stream()
                    .filter(d -> Double.compare(d, 0.0) != 0)
                    .count();
            long ctjaerbyFre = intensityList.subList(14, intensityList.size())
                    .stream()
                    .filter(d -> Double.compare(d, 0.0) != 0)
                    .count();

            String line = String.join("\t", String.valueOf(taxon), String.valueOf(calcFre),
                    String.valueOf(plaqueFre), String.valueOf(ctjaerbyFre)) + "\t"
                    + Arrays.stream(taxonIntensityMap.get(taxon))
                    .mapToObj(String::valueOf)
                    .collect(Collectors.joining("\t"))
                    + System.lineSeparator();

            Files.write(path,
                    line.getBytes(),
                    StandardOpenOption.CREATE,
                    StandardOpenOption.APPEND);
        }

    }

    @Test
    public void loadProteinGroupData() throws IOException {
        List<String[]> entryList = analysis.loadProteinGroupData(proteinGroupsFile);
        Files.write(Paths.get("/home/huangjs/coding/java/java-web/pipeline-reconstruction/src/main/resources/dental/genus-LFQ.csv"),
                entryList.parallelStream()
                        .map(arr -> Arrays.stream(arr).collect(Collectors.joining(",")))
                        .collect(Collectors.toList())
        );

        Set<String> genusSet = entryList.parallelStream()
                .map(arr -> arr[0])
                .collect(Collectors.toSet());
        System.out.println("===candidate genera(" + genusSet.size() + ")===");
        Files.write(Paths.get("/home/huangjs/coding/java/java-web/pipeline-reconstruction/src/test/resources/dental/genera-from-my-filter.txt"),
                genusSet);

    }

    @Test
    public void matrixPlus() {
        double[] m1 = new double[]{0.1, 0.2, 0.3};
        double[] m2 = new double[]{0.2, 0.2, 0.5};
        double[] rs = Analysis.matrixPlus(m1, m2);
        System.out.println(Arrays.stream(rs).mapToObj(Double::valueOf).collect(Collectors.toList()));
    }

    @Test
    public void dotDivision() {
        double[] m1 = new double[]{0.1, 0.2, 0.3};
        double[] m2 = new double[]{0.2, 0.2, 0.5};
        double[] rs = Analysis.dotDivision(m1, m2);
        System.out.println(Arrays.stream(rs).mapToObj(Double::valueOf).collect(Collectors.toList()));
    }

    @Test
    public void calBrayCurtisDissimilarity() {
        double[] g1 = new double[]{23, 64, 14, 0, 0, 3, 1};
        double[] g2 = new double[]{0, 5, 5, 0, 40, 40, 0};
        double result = Analysis.calBrayCurtisDissimilarity(g1, g2);
        System.out.println("bc: " + result);
    }

    @Test
    public void calAbundanceDifference() throws IOException {
        List<AbundanceData> abundanceDataList = analysis.calAbundanceDifference("/home/huangjs/coding/java/java-web/pipeline-reconstruction/src/main/resources/dental/all-genera-DE-paper.tsv");

        // write into file
        Path path = Paths.get("/home/huangjs/coding/java/java-web/pipeline-reconstruction/src/main/resources/dental/DE-result-using-paper-date.tsv");
        String header = String.join("\t", "taxon name", "p-value", "-log10(p-value)", "FC", "log2(FC)", "group1", "group2")
                + System.lineSeparator();
        Files.write(path, header.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        abundanceDataList.stream()
                .sorted(Comparator.comparingDouble(AbundanceData::getPValue))
                .map((AbundanceData data) -> String.join("\t", data.getTaxonName(),
                        String.valueOf(data.getPValue()),
                        String.valueOf(data.getNegLog10PValue()),
                        String.valueOf(data.getFc()),
                        String.valueOf(data.getLog2FC())
                        ,
                        Arrays.stream(data.getAbundanceArr1()).mapToObj(String::valueOf).collect(Collectors.joining(",")),
                        Arrays.stream(data.getAbundanceArr2()).mapToObj(String::valueOf).collect(Collectors.joining(","))
                        )
                                + System.lineSeparator()
                )
                .forEach((String line) -> {
                    try {
                        Files.write(path, line.getBytes(), StandardOpenOption.APPEND);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    /**
     * TODO 后期可以删除
     * 检索all species file所有species的genus
     *
     * @throws IOException
     */
    @Test
    public void searchGenus() throws IOException {

        FileReaderUtil fileReaderUtil = new FileReaderUtil();
        String[] speciesArr = new String[]{
                "Actinomyces dentalis",
                "Actinomyces sp. oral taxon 414",
                "Filifactor alocis",
                "Fretibacterium fastidiosum",
                "Pseudomonas aeruginosa",
                "Lautropia mirabilis",
                "Pseudopropionibacterium propionicum",
                "Streptococcus sanguinis",
                "Actinomyces gerencseriae",
                "Actinomyces israelii",
                "Actinomyces johnsonii",
                "Actinomyces massiliensis",
                "Actinomyces naeslundii",
                "Campylobacter rectus",
                "Cardiobacterium valvarum",
                "Chloroflexi bacterium oral taxon 439",
                "Corynebacterium matruchotii",
                "Desulfomicrobium orale",
                "Fusobacterium nucleatum",
                "Johnsonella ignava",
                "Lachnoanaerobaculum saburreum",
                "Leptotrichia buccalis",
                "Leptotrichia hofstadii",
                "Leptotrichia wadei",
                "Olsenella sp. oral taxon 807",
                "Porphyromonas gingivalis",
                "Tannerella forsythia",
                "Treponema denticola",
                "Treponema socranskii",
                "[Eubacterium] yurii",
                "Actinomyces cardiffensis",
                "Actinomyces georgiae",
                "Anaerococcus tetradius",
                "Campylobacter gracilis",
                "Desulfobulbus sp. oral taxon 041",
                "Leptotrichia sp. oral taxon 212",
                "Ottowia sp. oral taxon 894",
                "Tannerella sp. oral taxon HOT-286",
                "Treponema medium",
                "uncultured bacterium",
                "Actinomyces odontolyticus",
                "Actinomyces sp. oral taxon 170",
                "Lachnospiraceae bacterium oral taxon 082",
                "Selenomonas sputigena",
                "[Eubacterium] nodatum",
                "Bacteroidetes oral taxon 274",
                "Centipeda periodontii",
                "Selenomonas infelix",
                "Streptococcus mitis",
                "Actinomyces sp. oral taxon 172",
                "Rothia aeria",
                "Treponema maltophilum",
                "Abiotrophia defectiva",
                "Actinomyces sp. oral taxon 175",
                "Pyramidobacter piscolens",
                "[Eubacterium] saphenum",
                "Campylobacter showae",
                "Cardiobacterium hominis",
                "Eikenella corrodens",
                "Actinomyces timonensis",
                "Candidatus Saccharibacteria oral taxon TM7x",
                "Dialister invisus",
                "Eubacterium limosum",
                "Fusobacterium periodonticum",
                "Selenomonas noxia",
                "Streptococcus cristatus",
                "Actinomyces sp. oral taxon 171",
                "Lactobacillus delbrueckii",
                "Streptococcus sinensis",
                "Jonquetella anthropi",
                "Mitsuokella sp. oral taxon 131",
                "Neisseria elongata",
                "Actinomyces oris",
                "Lachnospiraceae bacterium oral taxon 500",
                "Leptotrichia sp. oral taxon 215",
                "Olsenella sp. oral taxon 809",
                "Prevotella loescheii",
                "Prevotella melaninogenica",
                "Prevotella nigrescens",
                "Prevotella sp. oral taxon 472",
                "Streptococcus intermedius",
                "Streptococcus sp. F0442",
                "Aggregatibacter segnis",
                "Capnocytophaga sp. oral taxon 329",
                "Lactobacillus johnsonii",
                "Lactobacillus vaginalis",
                "Porphyromonas endodontalis",
                "Prevotella denticola",
                "Prevotella oris",
                "Variovorax paradoxus",
                "Neisseria mucosa",
                "Neisseria sicca",
                "Neisseria sp. oral taxon 014",
                "Pseudoramibacter alactolyticus",
                "Rothia dentocariosa",
                "Treponema putidum",
                "[Eubacterium] brachy",
                "Actinomyces meyeri",
                "Capnocytophaga sp. oral taxon 863",
                "Desulfovibrio fairfieldensis",
                "Leptotrichia sp. oral taxon 225",
                "Prevotella buccae",
                "Acidovorax sp. JS42",
                "Actinomyces graevenitzii",
                "Actinomyces sp. oral taxon 181",
                "Actinomyces viscosus",
                "Aggregatibacter sp. oral taxon 458",
                "Catonella morbi",
                "Haemophilus parainfluenzae",
                "Prevotella intermedia",
                "Selenomonas sp. oral taxon 138",
                "Actinobaculum sp. oral taxon 183",
                "Actinomyces sp. oral taxon 849",
                "Aggregatibacter aphrophilus",
                "Bacillus subtilis",
                "Capnocytophaga granulosa",
                "Capnocytophaga sputigena",
                "Chelativorans sp. BNC1",
                "Kingella denitrificans",
                "Neisseria bacilliformis",
                "Actinomyces sp. oral taxon 178",
                "Aggregatibacter actinomycetemcomitans",
                "Campylobacter ureolyticus",
                "Capnocytophaga gingivalis",
                "Leptotrichia sp. oral taxon 847",
                "Neisseria gonorrhoeae",
                "Peptoclostridium acidaminophilum",
                "Selenomonas sp. F0473",
                "Selenomonas sp. oral taxon 892",
                "Actinomyces sp. oral taxon 448",
                "Actinomyces sp. oral taxon 848",
                "Capnocytophaga ochracea",
                "Capnocytophaga sp. oral taxon 380",
                "Corynebacterium durum",
                "Leptotrichia sp. oral taxon 879",
                "Neisseria lactamica",
                "Neisseria sp. oral taxon 020",
                "Neisseria subflava",
                "Peptostreptococcus stomatis",
                "Prevotella micans",
                "Streptococcus anginosus",
                "Veillonella parvula",
                "Candidatus Saccharibacteria bacterium RAAC3_TM7_1",
                "Capnocytophaga sp. oral taxon 338",
                "Desulfotalea psychrophila",
                "Lachnoclostridium phytofermentans",
                "Leptotrichia goodfellowii",
                "Leptotrichia shahii",
                "Neisseria flavescens",
                "Neisseria meningitidis",
                "Olsenella profusa",
                "Olsenella uli",
                "Oribacterium sinus",
                "Porphyromonas catoniae",
                "Prevotella sp. oral taxon 317",
                "Prevotella veroralis",
                "Propionibacterium acidifaciens",
                "Streptococcus vestibularis",
                "Actinomyces radicidentis",
                "Alloprevotella tannerae",
                "Candidatus Saccharimonas aalborgensis",
                "Corynebacterium glutamicum",
                "Haemophilus sputorum",
                "Listeria monocytogenes",
                "Oribacterium sp. oral taxon 078",
                "Propionibacterium sp. oral taxon 192",
                "Streptococcus parasanguinis",
                "Borreliella burgdorferi",
                "Capnocytophaga sp. oral taxon 323",
                "Capnocytophaga sp. oral taxon 326",
                "Corynebacterium aurimucosum",
                "Corynebacterium jeikeium",
                "Coxiella burnetii",
                "Geobacter metallireducens",
                "Granulicatella adiacens",
                "Helicobacter pylori",
                "Kingella oralis",
                "Paenibacillus sp. oral taxon 786",
                "Porphyromonas sp. oral taxon 279",
                "Prevotella oulorum",
                "Prevotella pleuritidis",
                "Pseudomonas stutzeri",
                "Selenomonas artemidis",
                "Selenomonas sp. oral taxon 137",
                "Solobacterium moorei",
                "Staphylococcus warneri",
                "Stomatobaculum longum",
                "Streptococcus pneumoniae",
                "Treponema vincentii",
                "Acinetobacter baumannii",
                "Anaeroglobus geminatus",
                "Bartonella schoenbuchensis",
                "Bilophila wadsworthia",
                "Borreliella afzelii",
                "Burkholderia cepacia",
                "Capnocytophaga haemolytica",
                "Capnocytophaga sp. oral taxon 332",
                "Enterococcus faecalis",
                "Escherichia coli",
                "Gemella morbillorum",
                "Granulicatella elegans",
                "Haemophilus haemolyticus",
                "Haemophilus influenzae",
                "Kingella kingae",
                "Mitsuokella multacida",
                "Neisseria weaveri",
                "Prevotella buccalis",
                "Prevotella histicola",
                "Prevotella maculosa",
                "Prevotella marshii",
                "Prevotella pallens",
                "Prevotella saccharolytica",
                "Sanguibacter keddieii",
                "Selenomonas sp. oral taxon 478",
                "Simonsiella muelleri",
                "Staphylococcus aureus",
                "Streptococcus australis",
                "Streptococcus gordonii",
                "Treponema lecithinolyticum",
                "candidate division TM7 single-cell isolate TM7a",
                "Aromatoleum aromaticum",
                "Arsenicicoccus sp. oral taxon 190",
                "Candidatus Azobacteroides pseudotrichonymphae",
                "Capnocytophaga sp. oral taxon 335",
                "Cryptobacterium curtum",
                "Cutibacterium acnes",
                "Cutibacterium avidum",
                "Fusobacterium sp. oral taxon 370",
                "Gloeobacter violaceus",
                "Haemophilus pittmaniae",
                "Peptoclostridium litorale",
                "Polaromonas naphthalenivorans",
                "Porphyromonas sp. oral taxon 278",
                "Prevotella fusca",
                "Prevotella oralis",
                "Prevotella salivae",
                "Prevotella sp. oral taxon 299",
                "Ralstonia pickettii",
                "Selenomonas sp. oral taxon 149",
                "Streptococcus infantis",
                "Streptococcus oralis",
                "Streptococcus sp. oral taxon 058",
                "Thiobacillus denitrificans",
                "Veillonella dispar",
                "candidate division SR1 bacterium MGEHA"
        };
        List<String> speciesList = Arrays.stream(speciesArr).collect(Collectors.toList());

        // read lineage
        fileReaderUtil.setInputFile(Analysis.class.getResource("/taxonomy/lineages.csv").getPath());
        fileReaderUtil.setSeparator(",");
        Map<String, Integer> lineagesHeaderMap = fileReaderUtil.getHeaderMap();
        Predicate<String[]> lineageRowFilter = (String[] tmp) -> speciesList.contains(tmp[1]);
        Function<String[], String[]> lineageRowProcessor = (String[] tmp) ->
                new String[]{tmp[lineagesHeaderMap.get("genus_name")], tmp[lineagesHeaderMap.get("species_name")]};
        List<String[]> lineageList = fileReaderUtil.read(lineageRowFilter, lineageRowProcessor);
        Map<String, String> speciesGenusMap = lineageList.stream()
                .collect(Collectors.toMap((String[] tmp) -> tmp[1], (String[] tmp) -> tmp[0]));
        speciesList.stream().forEach((String s) -> System.out.println(s + "\t" + speciesGenusMap.get(s)));

    }
}
