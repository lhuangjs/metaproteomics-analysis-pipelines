//package cn.phoenixcenter.metaproteomics.analysis.taxonomy;
//
//import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
//import org.junit.Test;
//
//import java.utils.Arrays;
//import java.utils.Map;
//import java.utils.stream.Collectors;
//
//public class ProteinCentricMethodTest {
//
//    private double[][] intensityGroupArr = new double[][]{
//            {1, 2, 3},
//            {1, 2, 3},
//            {1, 2, 3},
//            {1, 2, 3},
//            {1, 2, 3}
//    };
//
//    private int[][] taxonIdGroupArr = new int[][]{
//            {9606}, // 9606
//            {72, 71}, // 71
//            {72, 71}, // 71
//            {392334, 59892}, // 157
//            {9606, 59892}, // null
//    };
//
//    private Taxon.Rank specificRank = Taxon.Rank.GENUS;
//
//    @Test
//    public void calculateLCA() {
//        // calculate LCA
//        System.out.println("===calculate LCA===");
//        Taxon[] lcaArr = ProteinCentricMethod.calculateLCA(taxonIdGroupArr);
//        Arrays.asList(lcaArr)
//                .forEach(System.out::println);
//
//
//        // remove homo sapience
//        lcaArr = Arrays.stream(lcaArr)
//                .filter((Taxon t) -> t != null && t.getId() != 9606)
//                .toArray(Taxon[]::new);
//
//        // filter
//        System.out.println("===filter by LCA===");
//        Taxon[] filteredLcaArr = ProteinCentricMethod.filterByLCA(lcaArr, specificRank);
//        Arrays.asList(filteredLcaArr)
//                .forEach(System.out::println);
//
//        // quantification
//        System.out.println("===quantification===");
//        Map<Taxon, double[]> result = ProteinCentricMethod.LFQIntensityQuantification(intensityGroupArr, filteredLcaArr, specificRank);
//        for (Taxon taxon : result.keySet()) {
//            String line = String.join("\t", taxon.getName(), taxon.getRank(),
//                    Arrays.stream(result.get(taxon)).mapToObj(String::valueOf).collect(Collectors.joining(",")));
//            System.out.println(line);
//        }
//
//
//    }
//}