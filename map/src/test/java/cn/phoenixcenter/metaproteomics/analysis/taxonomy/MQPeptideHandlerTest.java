package cn.phoenixcenter.metaproteomics.analysis.taxonomy;


import cn.phoenixcenter.metaproteomics.analysis.model.MQPeptide;
import cn.phoenixcenter.metaproteomics.analysis.model.MQPeptideEx;
import cn.phoenixcenter.metaproteomics.analysis.preprocessing.MQPeptideReader;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import cn.phoenixcenter.metaproteomics.utils.UniProtUtil;
import org.junit.Test;
import uk.ac.ebi.uniprot.dataservice.client.exception.ServiceException;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class MQPeptideHandlerTest {

    private String mqPepFile = MQPeptideHandlerTest.class.getResource("/analysis/taxonomy/peptides.txt")
            .getPath();

    private String accTaxonIdMappingFile = MQPeptideHandlerTest.class.getResource("/analysis/taxonomy/peptides-accTaxonIdMapping.tsv")
            .getPath();

    /**
     * Generate accession - taxon id mapping file for peptides.txt's column "Proteins"
     *
     * @throws IOException
     * @throws ServiceException
     */
    @Test
    public void generateTaxonIdMappingFile() throws IOException, ServiceException {
        MQPeptideReader reader = new MQPeptideReader();
        List<MQPeptide> pepList = reader.read(mqPepFile);

        // obtain proteins list
        Set<String> accSet = pepList.parallelStream()
                .map(pg -> pg.getProteins())
                .filter(proArr -> proArr != null)
                .flatMap((String[] ids) -> Arrays.stream(ids))
                .collect(Collectors.toSet());

        // map accession to taxon id
        Map<String, Integer> accTaxonIdMap = UniProtUtil.batchAccessionToTaxonId(accSet);

        // write
        List<String> lines = accTaxonIdMap.keySet().stream()
                .sorted()
                .map(key -> key + "\t" + String.valueOf(accTaxonIdMap.get(key)))
                .collect(Collectors.toList());
        lines.addAll(
                accSet.parallelStream()
                        .filter(acc -> !accTaxonIdMap.containsKey(acc))
                        .sorted()
                        .collect(Collectors.toList())
        );

        Files.write(Paths.get("./results/saliva/peptides-accTaxonIdMapping.tsv"), lines,
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);

    }

    @Test
    public void testAll() throws IOException {

        // read peptides
        MQPeptideReader reader = new MQPeptideReader();
        List<MQPeptide> pepList = reader.read(mqPepFile, true, true);

        // add taxon info
        List<MQPeptideEx> pepListEx = MQPeptideHandler.addTaxonInfo(pepList, accTaxonIdMappingFile, true);
        List<String> pepListStr = pepListEx.stream()
                .map(p -> String.join("\t",
                        p.getMqPeptide().getSequence(),
                        Arrays.stream(p.getMqPeptide().getProteins()).collect(Collectors.joining(";")),
                        Arrays.stream(p.getProteinsTaxonIds()).mapToObj(String::valueOf).collect(Collectors.joining(";")),
                        p.getProteinsLCA() == null ? "" : p.getProteinsLCA().getRank(),
                        p.getProteinsLCA() == null ? "" : p.getProteinsLCA().toString()
                        )
                )
                .collect(Collectors.toList());
        Files.write(Paths.get("./results/saliva/peptides-with-LCA.tsv"),
                pepListStr, Charset.forName("UTF-8"),
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);


        // filter by LCA count
        Map<Taxon, List<MQPeptideEx>> taxonPepMap = MQPeptideHandler.filterByLCACount(pepListEx, Taxon.Rank.GENUS, 1);

        // write
        List<String> lines = taxonPepMap.entrySet().stream()
                .sorted((e1, e2) -> e1.getKey().getName().compareToIgnoreCase(e2.getKey().getName()))
                .map(e -> {
                            List<String> pepSeqList = e.getValue().stream()
                                    .map(pepEx -> pepEx.getMqPeptide().getSequence())
                                    .collect(Collectors.toList());
                            return String.join("\t",
                                    e.getKey().getName(),
                                    String.valueOf(pepSeqList.size()),
                                    pepSeqList.stream()
                                            .collect(Collectors.joining(","))
                            );
                        }
                )
                .collect(Collectors.toList());
        Files.write(Paths.get("./results/saliva/taxon-peptide.tsv"),
                lines, Charset.forName("UTF-8"),
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);

        // calculate taxon distribution
        Map<Taxon, Double> distribution = MQPeptideHandler.calTaxonDistribution(taxonPepMap, 10);
        lines = distribution.entrySet().stream()
                .sorted((e1, e2) -> Double.compare(e2.getValue().doubleValue(), e1.getValue().doubleValue()))
                .map(e -> String.join("\t",
                        e.getKey().getName(),
                        String.valueOf(e.getValue()))
                )
                .collect(Collectors.toList());

        Files.write(Paths.get("./results/saliva/peptide-distribution.tsv"),
                lines, Charset.forName("UTF-8"),
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);

    }
}