package cn.phoenixcenter.metaproteomics.analysis.taxonomy;

import cn.phoenixcenter.metaproteomics.analysis.model.MQProteinGroup;
import cn.phoenixcenter.metaproteomics.analysis.model.MQProteinGroupEx;
import cn.phoenixcenter.metaproteomics.analysis.preprocessing.MQProteinGroupReader;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import cn.phoenixcenter.metaproteomics.utils.UniProtUtil;
import org.junit.Test;
import uk.ac.ebi.uniprot.dataservice.client.exception.ServiceException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MQProteinGroupHandlerTest {

//    private String pgFile = MQProteinGroupHandlerTest.class
//            .getResource("/analysis/taxonomy/proteinGroups-test.txt")
//            .getPath();
//
//    private String accTaxonIdMappingFile = MQProteinGroupHandlerTest.class
//            .getResource("/analysis/taxonomy/accTaxonIdMapping-test.txt")
//            .getPath();

    private String pgFile = MQProteinGroupHandlerTest.class
            .getResource("/analysis/taxonomy/proteinGroups.txt")
            .getPath();

    private String accTaxonIdMappingFile = MQProteinGroupHandlerTest.class
            .getResource("/analysis/taxonomy/accTaxonIdMapping.txt")
            .getPath();

    private int minRazorPlusUniquePeptidesCount = 2;

    private Taxon.Rank specificRank = Taxon.Rank.GENUS;

    @Test
    public void generateTaxonIdMappingFile() throws IOException, ServiceException {
        // read and filter proteinGroups.txt
        MQProteinGroupReader reader = new MQProteinGroupReader();
        List<MQProteinGroup> pgList = reader.read(pgFile);

        // obtain majority protein list
        Set<String> accSet = pgList.parallelStream()
                .map(pg -> pg.getMajorityProteinIds())
                .flatMap((String[] ids) -> Arrays.stream(ids))
                .collect(Collectors.toSet());

        // map accession to taxon id
        Map<String, Integer> accTaxonIdMap = UniProtUtil.batchAccessionToTaxonId(accSet);
//        Map<String, Integer> accTaxonIdMap = UniProtUtil.batchAccessionToTaxonId(accSet,
//                "/tmp/acc-taxon-mapping2840425188869803565.tsv",
//                "\t");

        // write
        List<String> lines = accTaxonIdMap.keySet().stream()
                .sorted()
                .map(key -> key + "\t" + String.valueOf(accTaxonIdMap.get(key)))
                .collect(Collectors.toList());
        lines.addAll(
                accSet.parallelStream()
                        .filter(acc -> !accTaxonIdMap.containsKey(acc))
                        .sorted()
                        .collect(Collectors.toList())
        );

        Files.write(Paths.get("./results/saliva/accTaxonIdMapping.tsv"), lines,
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);

    }

    /**
     * CON__O76009 -> O76009
     * REV__A0A024ISM8 -> A0A024ISM8
     *
     * @throws IOException
     * @throws ServiceException
     */
    @Test
    public void generateTaxonIdMappingFileEx() throws IOException, ServiceException, URISyntaxException {

        // new acc => old acc
        Map<String, String> accMap = Files.readAllLines(Paths.get(MQProteinGroupHandlerTest.class
                        .getResource("/analysis/taxonomy/rev-con-acc.txt").toURI()),
                Charset.forName("UTF-8")).parallelStream()
                .collect(Collectors.toMap(
                        acc -> {
                            if (acc.startsWith("CON__") || acc.startsWith("REV__")) {
                                return acc.substring(5);
                            } else {
                                return acc;
                            }
                        },
                        acc -> acc
                ));

        // map accession to taxon id
        final Map<String, Integer> newAccTaxonIdMap = UniProtUtil.batchAccessionToTaxonId(accMap.keySet());

        // new acc -> old acc
        final Map<String, Integer> oldAccTaxonIdMap = newAccTaxonIdMap.keySet().stream()
                .collect(Collectors.toMap(key -> accMap.get(key),
                        key -> newAccTaxonIdMap.get(key)));

        // write
        List<String> lines = oldAccTaxonIdMap.keySet().stream()
                .sorted()
                .map(key -> key + "\t" + String.valueOf(oldAccTaxonIdMap.get(key)))
                .collect(Collectors.toList());
        lines.addAll(
                accMap.values().parallelStream()
                        .filter(acc -> !oldAccTaxonIdMap.containsKey(acc))
                        .sorted()
                        .collect(Collectors.toList())
        );

        Files.write(Paths.get("./results/saliva/rev-con-accTaxonIdMapping.tsv"), lines,
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);

    }

    @Test
    public void testAll() throws IOException {

        // read and filter protein groups
        MQProteinGroupReader reader = new MQProteinGroupReader();
        List<MQProteinGroup> pgList = reader.read(pgFile, true, true, true);
        List<String> lfqTagList = reader.getLfqTagList();
        System.out.println(".....pgList.size = " + pgList.size());

        // set taxon info
        List<MQProteinGroupEx> pgExList = MQProteinGroupHandler.addTaxonInfo(pgList, accTaxonIdMappingFile, true);
        writeMQProteinGroupExList(pgExList,
                "./results/saliva/proteinGroupsLCA.tsv");

        // filter
        List<MQProteinGroupEx> filteredPgExList = MQProteinGroupHandler.filterBySuperKingdomAndLCA(
                pgExList,
                minRazorPlusUniquePeptidesCount,
                specificRank);
        writeMQProteinGroupExList(filteredPgExList,
                "./results/saliva/filtered-proteinGroupsLCA.tsv");

        // calculate taxon distribution
        Map<Taxon, double[]> distributionMap = MQProteinGroupHandler.calTaxonDistribution(filteredPgExList, specificRank);

        // write distribution result
        Map<String, String[]> groups = new HashMap<>();
        groups.put("G1", new String[]{"LFQ intensity 1", "LFQ intensity 2", "LFQ intensity 3", "LFQ intensity 4", "LFQ intensity 5", "LFQ intensity 6", "LFQ intensity 7", "LFQ intensity 8"});
        groups.put("G2", new String[]{"LFQ intensity 1m", "LFQ intensity 2m", "LFQ intensity 3m", "LFQ intensity 4m", "LFQ intensity 5m", "LFQ intensity 6m", "LFQ intensity 7m", "LFQ intensity 8m"});
        groups.put("G3", new String[]{"LFQ intensity frac1", "LFQ intensity frac2", "LFQ intensity frac3", "LFQ intensity frac4", "LFQ intensity frac5", "LFQ intensity frac6", "LFQ intensity frac7", "LFQ intensity frac8"});
        MQProteinGroupHandler.writeTaxonDistribution(distributionMap,
                groups,
                lfqTagList,
                "./results/saliva/distributions.tsv");
    }

    public void writeMQProteinGroupExList(List<MQProteinGroupEx> pgExList, String outputFile) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
        for (MQProteinGroupEx pgEx : pgExList) {
            String proIds = Arrays.stream(pgEx.getMqProteinGroup().getMajorityProteinIds())
                    .collect(Collectors.joining(";"));
            String line;
            if (pgEx.getMajorityProteinTaxonIds() == null) {
                line = proIds;
            } else {
                String taxonIds = Arrays.stream(pgEx.getMajorityProteinTaxonIds())
                        .mapToObj(String::valueOf)
                        .collect(Collectors.joining(";"));
                line = String.join("\t", proIds, taxonIds, pgEx.getMajorityProteinTaxonLCA().toString());
            }
            bw.write(line + System.lineSeparator());
        }
        bw.close();
    }

    @Test
    public void test() {
        int[] arr = IntStream.range(0, 100)
                .limit(0)
                .toArray();
        System.out.println(arr);
        System.out.println(arr.length);
        int[] arr2 = new int[0];
    }

    @Test
    public void test2() {
        System.out.println(
                Arrays.stream(new int[]{})
                        .reduce((a, b) -> a + b)
                        .getAsInt()
        );
    }
}