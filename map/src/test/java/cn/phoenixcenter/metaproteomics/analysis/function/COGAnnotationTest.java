package cn.phoenixcenter.metaproteomics.analysis.function;

import cn.phoenixcenter.metaproteomics.analysis.preprocessing.MQProteinGroupReader;
import cn.phoenixcenter.metaproteomics.analysis.model.MQProteinGroup;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class COGAnnotationTest {

    String fastaLibFile = "/home/huangjs/documents/java/metaproteomics/row_data/gut#3/184sample_2.6M.GeneSet.pep";

    @Test
    public void extractSequence() throws IOException {
        // read and filter proteinGroups.txt
        MQProteinGroupReader reader = new MQProteinGroupReader();
        List<MQProteinGroup> proteinGroupList = reader.read(this.getClass()
                        .getResource("/gut/proteinGroups.txt")
                        .getPath(),
                true, true, true);

        // extract leading proteins
        Set<String> proIdSet = proteinGroupList.parallelStream()
                .flatMap((MQProteinGroup g) -> Arrays.stream(g.getMajorityProteinIds()))
                .collect(Collectors.toSet());
        System.out.println("protein set size: " + proIdSet.size());

        // extract sequence
        COGAnnotation.extractSequence(proIdSet, fastaLibFile, "./results/gut/mouse-protein.fasta");
    }

    @Test
    public void align() throws IOException, InterruptedException {
        COGAnnotation.align("./results/gut/mouse-protein.fasta",
                "./results/gut/mouse-pro-alignment.tsv");
    }

    @Test
    public void parseAlignmentResult() throws IOException {
        // read and filter proteinGroups.txt
        MQProteinGroupReader reader = new MQProteinGroupReader();
        List<MQProteinGroup> proteinGroupList = reader.read(this.getClass()
                        .getResource("/gut/proteinGroups.txt")
                        .getPath(),
                true, true, true);

        // parse alignment result
        List<MQProteinGroupCOG> pgCOGList = COGAnnotation.parseAlignmentResult("./results/gut/mouse-pro-alignment.tsv",
                proteinGroupList);

        // write into file
        Files.write(Paths.get("./results/gut/mouse-COG.tsv"),
                pgCOGList.parallelStream().map(MQProteinGroupCOG::fPrint).collect(Collectors.toList()),
                StandardOpenOption.TRUNCATE_EXISTING,
                StandardOpenOption.CREATE);
        Map<String, double[]> cogDistribution = COGAnnotation.cogDistribution(pgCOGList);
        COGAnnotation.writeCOGDistribution(cogDistribution,
                "./results/gut/mouse-COG-distribution.tsv");

    }
}