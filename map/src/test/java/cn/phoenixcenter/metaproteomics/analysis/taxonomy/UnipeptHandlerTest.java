package cn.phoenixcenter.metaproteomics.analysis.taxonomy;

import cn.phoenixcenter.metaproteomics.analysis.model.MQPeptide;
import cn.phoenixcenter.metaproteomics.analysis.preprocessing.MQPeptideReader;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class UnipeptHandlerTest {

//    @Test
//    public void pepInfo() throws URISyntaxException, IOException {
//        Set<String> peptideSet = Files.readAllLines(Paths.get(this.getClass().getResource("/gut/peptide-list.txt").toURI()))
//                .stream()
//                .skip(1)
//                .limit(20000)
//                .collect(Collectors.toSet());
//
//        List<String> lines = UnipeptHandler.pepInfo(peptideSet, true, true, true).stream()
//                .sorted(Comparator.comparing(UnipeptPeptInfo::getSequence))
////                .peek(peptInfo -> System.out.println(peptInfo))
//                .map(peptInfo -> String.join("\t", peptInfo.getSequence(), peptInfo.getLca().getRank(),
//                        peptInfo.getLca().getName(), peptInfo.getLca().getId() + ""))
//                .collect(Collectors.toList());
//
//        Files.write(Paths.get("./results/gut/unipept-lca.tsv"),
//                lines,
//                StandardOpenOption.TRUNCATE_EXISTING,
//                StandardOpenOption.CREATE);
//
//    }

    @Test
    public void parallelMPA() throws IOException, URISyntaxException {

        // read MaxQuant peptides.txt
        MQPeptideReader reader = new MQPeptideReader();
        List<MQPeptide> mqPeptideList = reader.read(this.getClass()
                        .getResource("/gut/peptides.txt")
                        .getPath(),
                true,
                true);
        Set<String> peptideSet = mqPeptideList.parallelStream()
                .map(MQPeptide::getSequence)
                .collect(Collectors.toSet());

        UnipeptHandler.parallelMPA(peptideSet, true, true,
                "./results/gut/filtered_mpa_result.csv");
    }

    @Test
    public void mpa() throws IOException, URISyntaxException {
        Set<String> peptideSet = Files.readAllLines(
                Paths.get(this.getClass()
                        .getResource("/gut/gut-peptides.txt")
                        .toURI()))
                .stream()
                .skip(1)
                .collect(Collectors.toSet());

        UnipeptHandler.mpa(peptideSet, true, true,
                "./results/gut/gut_mpa_result.csv");
    }

    @Test
    public void mpa2() throws IOException, URISyntaxException {
        Set<String> peptideSet = Files.readAllLines(
                Paths.get(this.getClass()
                        .getResource("/gut/homd-peptide.txt")
                        .toURI()))
                .stream()
                .collect(Collectors.toSet());

        UnipeptHandler.mpa(peptideSet, true, true,
                "/home/huangjs/Downloads/homd_mpa_result.csv");
    }

    @Test
    public void mpa3() throws IOException {
        Set<String> peptideSet = Files.readAllLines(
                Paths.get("/home/huangjs/Downloads/unipept/PD_bacteria_pep.txt"))
                .stream()
                .skip(1)
                .map(line -> line.split("\t", 2)[0])
                .collect(Collectors.toSet());

        UnipeptHandler.mpa(peptideSet, true, true,
                "/home/huangjs/Downloads/unipept/PD_bacteria_pep_6-26_result.csv");
    }

    @Test
    public void mpa4() throws IOException, URISyntaxException {
        Set<String> peptideSet = Files.readAllLines(
                Paths.get("/home/huangjs/Downloads/unipept/proteinpolit_bacteria_pep.txt"))
                .stream()
                .skip(1)
                .map(line -> line.split("\t", 2)[0])
                .collect(Collectors.toSet());

        UnipeptHandler.mpa(peptideSet, true, true,
                "/home/huangjs/Downloads/unipept/proteinpolit_bacteria_pep_6-26_result.csv");
    }

    @Test
    public void mpa5() throws IOException, URISyntaxException {
        Set<String> peptideSet = Files.readAllLines(
                Paths.get("/home/huangjs/Documents/map/shao/gut.tsv"))
                .stream()
                .skip(1)
                .map(line -> line.split("\t", 2)[0])
                .collect(Collectors.toSet());

        UnipeptHandler.mpa(peptideSet, true, true,
                "/home/huangjs/Documents/map/shao/gut-lca-unipept.tsv");
    }

    /**
     * Biomass Run1 U result
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void runBiomassU() throws IOException, URISyntaxException {
        Set<String> peptideSet = Files.readAllLines(
                Paths.get("/home/huangjs/Documents/map/result/peptide/biomass/peptide_Run1.tsv"))
                .stream()
                .skip(1)
                .map(line -> line.split("\t", 2)[0])
                .collect(Collectors.toSet());

        UnipeptHandler.mpa(peptideSet, true, true,
                "/home/huangjs/Documents/map/result/peptide/biomass/peptide_Run1_unipept.tsv");
    }

    @Test
    public void parseMPAResult() throws IOException {

        MQPeptideReader reader = new MQPeptideReader();
        List<MQPeptide> mqPeptideList = reader.read(this.getClass()
                        .getResource("/gut/peptides.txt")
                        .getPath(),
                true,
                true);

        // parse
        List<UnipeptPeptInfo> unipeptPeptInfoList = UnipeptHandler.parseMPAResult(mqPeptideList,
                true,
                "./results/gut/filtered_mpa_result.csv");

        // filtered by LCA
        Map<String, List<UnipeptPeptInfo>> taxonNamePeptInfoMap = UnipeptHandler.filterByLca(unipeptPeptInfoList,
                Taxon.Rank.PHYLUM);

        // calculate distribution
        Map<String, int[]> groups = new HashMap<>();
        groups.put("HFD", IntStream.range(0, 16).toArray());
        groups.put("LFD", IntStream.range(16, 32).toArray());
        Map<String, Map<String, Double>> distributionMap = UnipeptHandler.calTaxonDistribution(taxonNamePeptInfoMap,
                groups);

        // write into file
        String header = "Taxon\t" + distributionMap.entrySet().stream()
                .limit(1)
                .map(entry -> entry.getValue().keySet().stream()
                        .sorted()
                        .collect(Collectors.joining("\t")))
                .findFirst()
                .get();
        List<String> lines = distributionMap.entrySet().parallelStream()
                .map(entry -> entry.getKey() + "\t"
                        + entry.getValue().entrySet().stream()
                        .sorted(Comparator.comparing(e -> e.getKey()))
                        .map(e -> String.valueOf(e.getValue()))
                        .collect(Collectors.joining("\t"))
                )
                .collect(Collectors.toList());
        lines.add(0, header);
        Files.write(Paths.get("./results/gut/filtered-distribution.tsv"), lines,
                StandardOpenOption.TRUNCATE_EXISTING,
                StandardOpenOption.CREATE);


    }
}