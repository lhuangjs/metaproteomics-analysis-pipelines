package cn.phoenixcenter.metaproteomics.analysis.difference;


import cn.phoenixcenter.metaproteomics.analysis.function.COGAnnotation;
import cn.phoenixcenter.metaproteomics.analysis.function.MQProteinGroupCOG;
import cn.phoenixcenter.metaproteomics.analysis.preprocessing.MQProteinGroupReader;
import cn.phoenixcenter.metaproteomics.analysis.model.MQProteinGroup;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;

public class DiffProAnalysisTest {
    String fastaLibFile = "/home/huangjs/documents/java/metaproteomics/row_data/gut#3/184sample_2.6M.GeneSet.pep";

    @Test
    public void testAll() throws IOException, InterruptedException {
        // read proteinGroups.txt
        MQProteinGroupReader pgReader = new MQProteinGroupReader();
        List<MQProteinGroup> pgList = pgReader.read(this.getClass()
                        .getResource("/gut/proteinGroups.txt")
                        .getPath(),
                true,
                true,
                true);

        /** difference proteins analysis **/
        // two sample t-test
        String[] lfd0Memb = new String[]{"LFQ intensity NCD1_D0", "LFQ intensity NCD2_D0", "LFQ intensity NCD3_D0", "LFQ intensity NCD4_D0"};
        String[] hfd0Memb = new String[]{"LFQ intensity HFD1_D0", "LFQ intensity HFD2_D0", "LFQ intensity HFD3_D0", "LFQ intensity HFD4_D0"};
        String[] g1Memb = new String[]{"LFQ intensity NCD1_D14", "LFQ intensity NCD1_D29", "LFQ intensity NCD1_D43", "LFQ intensity NCD2_D14", "LFQ intensity NCD2_D29", "LFQ intensity NCD2_D43", "LFQ intensity NCD3_D14", "LFQ intensity NCD3_D29", "LFQ intensity NCD3_D43", "LFQ intensity NCD4_D14", "LFQ intensity NCD4_D29", "LFQ intensity NCD4_D43"};
        String[] g2Memb = new String[]{"LFQ intensity HFD1_D14", "LFQ intensity HFD1_D29", "LFQ intensity HFD1_D43", "LFQ intensity HFD2_D14", "LFQ intensity HFD2_D29", "LFQ intensity HFD2_D43", "LFQ intensity HFD3_D14", "LFQ intensity HFD3_D29", "LFQ intensity HFD3_D43", "LFQ intensity HFD4_D14", "LFQ intensity HFD4_D29", "LFQ intensity HFD4_D43"};

        List<MQProteinGroupStat> pgStatList = DiffProAnalysis.tTest(pgList,
//                new String[]{"LFQ intensity HFD1_D0", "LFQ intensity HFD1_D14", "LFQ intensity HFD1_D29", "LFQ intensity HFD1_D43", "LFQ intensity HFD2_D0", "LFQ intensity HFD2_D14", "LFQ intensity HFD2_D29", "LFQ intensity HFD2_D43", "LFQ intensity HFD3_D0", "LFQ intensity HFD3_D14", "LFQ intensity HFD3_D29", "LFQ intensity HFD3_D43", "LFQ intensity HFD4_D0", "LFQ intensity HFD4_D14", "LFQ intensity HFD4_D29", "LFQ intensity HFD4_D43",},
//                new String[]{"LFQ intensity NCD1_D0", "LFQ intensity NCD1_D14", "LFQ intensity NCD1_D29", "LFQ intensity NCD1_D43", "LFQ intensity NCD2_D0", "LFQ intensity NCD2_D14", "LFQ intensity NCD2_D29", "LFQ intensity NCD2_D43", "LFQ intensity NCD3_D0", "LFQ intensity NCD3_D14", "LFQ intensity NCD3_D29", "LFQ intensity NCD3_D43", "LFQ intensity NCD4_D0", "LFQ intensity NCD4_D14", "LFQ intensity NCD4_D29", "LFQ intensity NCD4_D43",},
//                new String[]{"LFQ intensity HFD1_D14", "LFQ intensity HFD1_D29", "LFQ intensity HFD1_D43", "LFQ intensity HFD2_D14", "LFQ intensity HFD2_D29", "LFQ intensity HFD2_D43", "LFQ intensity HFD3_D14", "LFQ intensity HFD3_D29", "LFQ intensity HFD3_D43", "LFQ intensity HFD4_D14", "LFQ intensity HFD4_D29", "LFQ intensity HFD4_D43",},
//                new String[]{"LFQ intensity HFD1_D0", "LFQ intensity HFD2_D0", "LFQ intensity HFD3_D0", "LFQ intensity HFD4_D0"},
                g1Memb, g2Memb,
                (int) Math.ceil(g1Memb.length / 2.0),
                (int) Math.ceil(g2Memb.length / 2.0),
                pgReader.getLfqTagList());
        // p-value correction
        pgStatList = DiffProAnalysis.bhFDRCorrection(pgStatList);
        // obtain different protein
        pgStatList = DiffProAnalysis.filterByQValue(pgStatList, 0.05);

        /** COG annotation **/
        // extract query sequence
        Set<String> qqseqIdSet = pgStatList.parallelStream()
                .flatMap(pgStat -> Arrays.stream(pgStat.getMqProteinGroup().getMajorityProteinIds()))
                .collect(Collectors.toSet());
        String qseqFile = "./results/gut/mouse-diff-pro.fasta";
        COGAnnotation.extractSequence(qqseqIdSet, fastaLibFile, qseqFile);
        // align
        String alignResFile = "./results/gut/mouse-diff-pro-alignment.tsv";
        COGAnnotation.align(qseqFile, alignResFile);
        // parse and annotate
        List<MQProteinGroup> pgList2 = pgStatList.parallelStream()
                .map(MQProteinGroupStat::getMqProteinGroup)
                .collect(Collectors.toList());
        List<MQProteinGroupCOG> pgCOGList = COGAnnotation.parseAlignmentResult(alignResFile,
                pgList2);
        // write result
        List<String> lines = new ArrayList<>(pgCOGList.size());
        lines.add(String.join("\t",
                "Majority protein IDs",
                "Average intensity of LFD",
                "Average intensity of HFD",
                "q value(LFD vs HFD)",
                "Fold change(LFD vs HFD)",
                "COF id",
                "COG category",
                "COG annotation",
                Arrays.stream(lfd0Memb).collect(Collectors.joining("\t")),
                Arrays.stream(hfd0Memb).collect(Collectors.joining("\t")),
                Arrays.stream(g1Memb).collect(Collectors.joining("\t")),
                Arrays.stream(g2Memb).collect(Collectors.joining("\t"))
        ));
        for (MQProteinGroup pg : pgList2) {
            double g1MeanLfq = Arrays.stream(g1Memb)
                    .mapToInt(gMemb -> pgReader.getLfqTagList().indexOf(gMemb))
                    .mapToDouble(membIndex -> pg.getLfqIntensityArr()[membIndex])
                    .sum() / g1Memb.length;
            double g2MeanLfq = Arrays.stream(g2Memb)
                    .mapToInt(gMemb -> pgReader.getLfqTagList().indexOf(gMemb))
                    .mapToDouble(membIndex -> pg.getLfqIntensityArr()[membIndex])
                    .sum() / g2Memb.length;
            double foldChange = g1MeanLfq > g2MeanLfq
                    ? -g1MeanLfq / g2MeanLfq
                    : g2MeanLfq / g1MeanLfq;
            MQProteinGroupCOG pgCOG1 = pgCOGList.parallelStream()
                    .filter(pgCOG -> pgCOG.getMqProteinGroup() == pg)
                    .findFirst()
                    .get();
            MQProteinGroupStat pgStat1 = pgStatList.parallelStream()
                    .filter(pgStat -> pgStat.getMqProteinGroup() == pg)
                    .findFirst()
                    .get();
            lines.add(String.join("\t",
                    Arrays.stream(pg.getMajorityProteinIds()).collect(Collectors.joining(";")),
                    String.valueOf(g1MeanLfq),
                    String.valueOf(g2MeanLfq),
                    String.valueOf(pgStat1.getQValue()),
                    String.valueOf(foldChange),
                    pgCOG1.getLeadingCOG() == null
                            ? String.join("\t", "", "", "")
                            : pgCOG1.getLeadingCOG().fPrint(),
                    Arrays.stream(lfd0Memb)
                            .mapToInt(gMemb -> pgReader.getLfqTagList().indexOf(gMemb))
                            .mapToDouble(membIndex -> pg.getLfqIntensityArr()[membIndex])
                            .mapToObj(String::valueOf)
                            .collect(Collectors.joining("\t")),
                    Arrays.stream(hfd0Memb)
                            .mapToInt(gMemb -> pgReader.getLfqTagList().indexOf(gMemb))
                            .mapToDouble(membIndex -> pg.getLfqIntensityArr()[membIndex])
                            .mapToObj(String::valueOf)
                            .collect(Collectors.joining("\t")),
                    Arrays.stream(g1Memb)
                            .mapToInt(gMemb -> pgReader.getLfqTagList().indexOf(gMemb))
                            .mapToDouble(membIndex -> pg.getLfqIntensityArr()[membIndex])
                            .mapToObj(String::valueOf)
                            .collect(Collectors.joining("\t")),
                    Arrays.stream(g2Memb)
                            .mapToInt(gMemb -> pgReader.getLfqTagList().indexOf(gMemb))
                            .mapToDouble(membIndex -> pg.getLfqIntensityArr()[membIndex])
                            .mapToObj(String::valueOf)
                            .collect(Collectors.joining("\t"))

            ));
            Files.write(Paths.get("./results/gut/mouse-diff-pro-COG.tsv"),
                    lines,
                    Charset.forName("UTF-8"),
                    StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING);
        }
        // calculate distribution of COG
        Map<String, double[]> cogDistribution = COGAnnotation.cogDistribution(pgCOGList);
        String[] baselineMemb = new String[]{"LFQ intensity NCD1_D0", "LFQ intensity NCD2_D0", "LFQ intensity NCD3_D0", "LFQ intensity NCD4_D0",
                "LFQ intensity HFD1_D0", "LFQ intensity HFD2_D0", "LFQ intensity HFD3_D0", "LFQ intensity HFD4_D0"};
        writeCOGDistribution(cogDistribution,
                "./results/gut/mouse-diff-pro-COG-distribution.tsv",
                pgReader.getLfqTagList(),
                baselineMemb,
                g1Memb,
                g2Memb);
    }

    public static void writeCOGDistribution(Map<String, double[]> cogDistribution,
                                            String outputFile,
                                            List<String> lfqTagList,
                                            String[] baselineMemb,
                                            String[] g1Memb,
                                            String[] g2Memb) throws IOException {

        List<String> lines = cogDistribution.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))
                .map(entry -> {
                            double[] baselineDist = Arrays.stream(baselineMemb)
                                    .mapToInt(m -> lfqTagList.indexOf(m))
                                    .mapToDouble(i -> entry.getValue()[i])
                                    .toArray();
                            double[] g1Dist = Arrays.stream(g1Memb)
                                    .mapToInt(m -> lfqTagList.indexOf(m))
                                    .mapToDouble(i -> entry.getValue()[i])
                                    .toArray();
                            double[] g2Dist = Arrays.stream(g2Memb)
                                    .mapToInt(m -> lfqTagList.indexOf(m))
                                    .mapToDouble(i -> entry.getValue()[i])
                                    .toArray();
                            return String.join("\t",
                                    entry.getKey(),
                                    String.valueOf(Arrays.stream(baselineDist).sum()),
                                    String.valueOf(Arrays.stream(g1Dist).sum()),
                                    String.valueOf(Arrays.stream(g2Dist).sum()),
                                    Arrays.stream(baselineDist)
                                            .mapToObj(String::valueOf)
                                            .collect(Collectors.joining("\t")),
                                    Arrays.stream(g1Dist)
                                            .mapToObj(String::valueOf)
                                            .collect(Collectors.joining("\t")),
                                    Arrays.stream(g2Dist)
                                            .mapToObj(String::valueOf)
                                            .collect(Collectors.joining("\t"))
                            );
                        }
                )
                .collect(Collectors.toList());
        lines.add(0, String.join("\t",
                "COG category",
                "Baseline",
                "LFD",
                "HFD",
                String.join("\t", baselineMemb),
                String.join("\t", g1Memb),
                String.join("\t", g2Memb)
        ));
        Files.write(Paths.get(outputFile),
                lines,
                Charset.forName("UTF-8"),
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);
    }

    @Test
    public void test() {
        double[] pValues = new double[]{0.01, 0.11, 0.21, 0.31, 0.41, 0.51, 0.61, 0.71, 0.81, 0.91};
        int count = 10;
        double[] adjustedPValue = new double[count];
        for (int i = count - 1; i >= 0; i--) {
            if (i == count - 1) {
                adjustedPValue[i] = pValues[i];
            } else {
                double right = adjustedPValue[i + 1];
                double left = pValues[i] * count / (i + 1);
                adjustedPValue[i] = Double.min(right, left);
            }
        }
        System.out.println(Arrays.stream(adjustedPValue)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(", ")));
    }
}