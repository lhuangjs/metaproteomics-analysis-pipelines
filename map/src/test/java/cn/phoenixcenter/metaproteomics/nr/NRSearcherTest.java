package cn.phoenixcenter.metaproteomics.nr;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import cn.phoenixcenter.metaproteomics.taxonomy.TaxonSearcher;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NRSearcherTest {

    NRSearcher nrSearcher = GlobalConfig.getObj(NRSearcher.class);

    TaxonSearcher taxonSearcher = GlobalConfig.getObj(TaxonSearcher.class);

    @Test
    public void pepinfo() throws InterruptedException, IOException {
        Set<String> peptideSet = new HashSet<>();
        peptideSet.add("ABCDEFGHIR");
        peptideSet.add("XXXXXXXXXR");
        peptideSet.add("VHGFRKR");
        nrSearcher.pepinfo(peptideSet, true, pept -> System.out.println(pept));
    }

    @Test
    public void functionAnalysis() throws InterruptedException, IOException {
        Set<String> peptideSet = new HashSet<>();
        peptideSet.add("LLDALVDYMPAPTDVEDLK");
        nrSearcher.pepinfo(peptideSet, true, pept -> System.out.println(pept));
    }

    @Test
    public void getProteinsByIds() throws IOException {
        Set<String> pidSet = new HashSet();
        pidSet.add("WP_135183328.1");
        pidSet.add("VDO52558.1");
        pidSet.add("CAA31818.1");
        Path outputPath = Paths.get("/home/huangjs/Documents/metaprot/nr-searcher/proteins.fasta");
        nrSearcher.getProteinsByIds(pidSet, outputPath);
    }

    @Test
    public void getProteinsByIds2() throws IOException {
        Set<String> pidSet = new HashSet();
        pidSet.add("WP_004337092.1");
        Path outputPath = Paths.get("/home/huangjs/Documents/biomass/CRH76259.fasta");
        nrSearcher.getProteinsByIds(pidSet, outputPath);
    }

    @Test
    public void getProteinsByTaxon() throws IOException {
        Path outputPath = Paths.get("/home/huangjs/Documents/metaprot/nr-searcher/subtaxa-proteins.fasta");
        nrSearcher.getProteinsByTaxon(new Taxon(46147), outputPath);
    }

    @Test
    public void getProteinsByTaxa() throws IOException {
        Path outputPath = Paths.get("/home/huangjs/Documents/biomass/proteins.fasta");
        TaxonSearcher taxonSearcher = GlobalConfig.getObj(TaxonSearcher.class);
        Taxon taxon = taxonSearcher.getTaxonById(813);
        nrSearcher.getProteinsByTaxa(Stream.of(taxon).collect(Collectors.toSet()), outputPath, null);
    }


    @Test
    public void nrTid() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String baseDir = "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/nr-shards/";
        String[] nrJsonFiles = new String[161];
        for (int i = 0; i < 160; i++) {
            nrJsonFiles[i] = baseDir + "nr-" + i + ".json";
        }
        nrJsonFiles[160] = "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/nr-shards/prot-without-taxon.json";
        BufferedWriter bw = Files.newBufferedWriter(Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/tid.txt"));
        int batchSize = 5000;
        for (String file : nrJsonFiles) {
            System.err.println("start to process entries from " + file);
            BufferedReader nrReader = Files.newBufferedReader(Paths.get(file), StandardCharsets.UTF_8);
            String line;
            // don't care all pid related with the tid
            Map<Integer, String> tid2Pid = new HashMap<>(batchSize);
            while ((line = nrReader.readLine()) != null) {
                JsonNode jsonNode = objectMapper.readTree(line);
                tid2Pid.put(jsonNode.get("tid").intValue(), jsonNode.get("pid").asText());
                if (tid2Pid.size() == batchSize) {
                    Map<Integer, Taxon> tid2Taxon = taxonSearcher.getTaxaByIds(tid2Pid.keySet());
                    for (Taxon t : tid2Taxon.values()) {
                        bw.write(tid2Pid.get(t.getId()) + "\t" + t.getId() + "\t" + t.getRank() + System.lineSeparator());
                    }
                    tid2Pid.clear();
                }
            }
        }
        bw.close();
    }
}