package cn.phoenixcenter.metaproteomics.nr;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.utils.ESUtil;
import cn.phoenixcenter.metaproteomics.utils.model.ESIndex;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.stream.IntStream;

public class NRParserTest {
    TransportClient esClient;
    NRParser nrParser;

    @Before
    public void setUp() throws Exception {
        esClient = GlobalConfig.getObj(TransportClient.class);
        nrParser = new NRParser(esClient);
    }

    @Test
    public void parseNR1() throws IOException {
        nrParser.parseNR(2,
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/testdata/nr.gz",// source is too big
                new String[]{
                        "/home/huangjs/archive/java/metaproteomics/raw_data/nr/testdata/prot.accession2taxid.gz", // source is too big
                        "/home/huangjs/archive/java/metaproteomics/raw_data/nr/nodeData/pdb.accession2taxid.gz"
                },
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/nodeData/gene2go.gz",
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/nodeData/names.dmp",
                "/tmp",
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/mismatched-prots.fasta",
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/prot-without-taxon.json",
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/taxon_id-not-in-names_dmp.txt",
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/nr-shards"
        );
    }

    @Test
    public void updateTaxonNotInNamesDmp2() throws IOException {
        nrParser.updateTaxonNotInNamesDmp(
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/tax_report.txt",
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/prot-without-taxon.json",
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/nr-shards"
        );
    }

    @Test
    public void parseNR() throws IOException {
        nrParser.parseNR(160,
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/nodeData/nr.gz",
                new String[]{
                        "/home/huangjs/archive/java/metaproteomics/raw_data/nr/nodeData/prot.accession2taxid.gz",
                        "/home/huangjs/archive/java/metaproteomics/raw_data/nr/nodeData/pdb.accession2taxid.gz"
                },
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/nodeData/gene2go.gz",
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/nodeData/names.dmp",
                "/tmp",
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/mismatched-prots.fasta",
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/prot-without-taxon.json",
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/taxon_id-not-in-names_dmp.txt",
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/nr-shards"
        );
    }

    @Test
    public void updateTaxonNotInNamesDmp() throws IOException {
        nrParser.updateTaxonNotInNamesDmp(
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/tax_report.txt",
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/prot-without-taxon.json",
                "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/nr-shards/prot-without-taxon.json"
        );
    }

    @Test
    public void storeNRWithoutReindex() throws IOException, InterruptedException {
        String baseDir = "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/nr-shards/";
        String[] nrFiles = new String[161];
        for (int i = 0; i < 160; i++) {
            nrFiles[i] = baseDir + "nr-" + i + ".json";
        }
        nrFiles[160] = "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/nr-shards/prot-without-taxon.json";
        // get nr index
        ESIndex nrIndex = ESUtil.parseJsonIndex("nr-v1", nrParser.getNrIndexConfStream());
        ESUtil.createIndex(esClient, nrIndex, true);
        // store
        nrParser.storeNR(
                50_00,
                new ByteSizeValue(20, ByteSizeUnit.MB),
                3,
                nrIndex,
                false,
                nrFiles
        );
    }

    /**
     * Store some shards for test
     *
     * @throws IOException
     * @throws InterruptedException
     */
    @Test
    public void storePartNRWithoutReindex() throws IOException, InterruptedException {
        String baseDir = "/home/huangjs/archive/java/metaproteomics/raw_data/nr/output/nr-shards/";
        String[] nrFiles = new String[53];
        for (int i = 0; i < 53; i++) {
            nrFiles[i] = baseDir + "nr-" + i + ".json";
        }
        // get nr index
        ESIndex nrIndex = ESUtil.parseJsonIndex("nr-v1", nrParser.getNrIndexConfStream());
        ESUtil.createIndex(esClient, nrIndex, false);
        // store
        nrParser.storeNR(
                50_00,
                new ByteSizeValue(20, ByteSizeUnit.MB),
                3,
                nrIndex,
                false,
                nrFiles
        );
    }

    @Test
    public void storeNRWithReindex() throws IOException, InterruptedException {
        String dir = "/home/huangjs/archive/java/metaproteomics/raw_data/nr/nr-shards/";
        String[] nrFiles = IntStream.rangeClosed(78, 78)
                .mapToObj(i -> dir + "nr-" + i + ".json.gz")
                .toArray(String[]::new);
        // get nr index
        ESIndex nrIndex = ESUtil.parseJsonIndex("nr-v1", nrParser.getNrIndexConfStream());
        // store
        nrParser.storeNR(
                50_000,
                new ByteSizeValue(120, ByteSizeUnit.MB),
                3,
                nrIndex,
                true,
                nrFiles
        );
    }

    @Test
    public void route() {
        String[] pidArr = new String[]{
                "WP_065892259.1",
                "OGK19126.1",
                "KKR69581.1"
        };
        for (String pid : pidArr){
            System.out.println(pid.hashCode());
            System.out.println(pid.hashCode() % 127);
        }
    }
}