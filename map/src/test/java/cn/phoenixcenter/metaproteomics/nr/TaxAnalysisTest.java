package cn.phoenixcenter.metaproteomics.nr;

import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TaxAnalysisTest {

    @Test
    public void runTest() throws IOException, InterruptedException {
        Path experimentPath = Paths.get("/home/huangjs/metadata/taxonomy/pept1/experiment.tsv");
        Path lcaPath = Paths.get("/home/huangjs/metadata/taxonomy/pept1/lca.tsv");
        Path goAnnoPath = Paths.get("/home/huangjs/metadata/taxonomy/pept1/go.json");
        Path treePath = Paths.get("/home/huangjs/metadata/taxonomy/pept1/tree.json");
        Path quantPath = Paths.get("/home/huangjs/metadata/taxonomy/pept1/quant.json");
        TaxAnalysis.run(true, experimentPath, lcaPath, goAnnoPath, treePath, quantPath);
    }

    @Test
    public void runUnipeptWithoutQuant() throws IOException, InterruptedException {
        Path experimentPath = Paths.get("/home/huangjs/metadata/taxonomy/26475d4b-57b4-4c5a-b95f-6ed165d1b35f/experiment.tsv");
        Path lcaPath = Paths.get("/home/huangjs/metadata/taxonomy/26475d4b-57b4-4c5a-b95f-6ed165d1b35f/lca.tsv");
        Path goAnnoPath = Paths.get("/home/huangjs/metadata/taxonomy/26475d4b-57b4-4c5a-b95f-6ed165d1b35f/go.json");
        Path treePath = Paths.get("/home/huangjs/metadata/taxonomy/26475d4b-57b4-4c5a-b95f-6ed165d1b35f/tree.json");
        Path quantPath = Paths.get("/home/huangjs/metadata/taxonomy/26475d4b-57b4-4c5a-b95f-6ed165d1b35f/quant.json");
        TaxAnalysis.run(true, experimentPath, lcaPath, goAnnoPath, treePath, quantPath);
    }

    @Test
    public void runGut() throws IOException, InterruptedException {
        // format
        Path experimentPath = Paths.get("/home/huangjs/Documents/map/shao/gut.tsv");
        Files.write(experimentPath,
                Files.lines(Paths.get("/home/huangjs/Documents/map/shao/gut-reduce.csv"))
                        .map(line -> {
                            if (line.startsWith("Peptide")) {
                                return line.replace(",", "\t");
                            } else {
                                return line.replaceAll("\\(.+?\\)", "").replace(",", "\t");
                            }
                        })
                        .collect(Collectors.toList()),
                StandardCharsets.UTF_8
        );
        Path resultPath = Paths.get("/home/huangjs/Documents/map/shao/nr");
        if (Files.notExists(resultPath)) {
            Files.createDirectories(resultPath);
        }
        Path lcaPath = resultPath.resolve("lca.tsv");
        Path goAnnoPath = resultPath.resolve("go.json");
        Path treePath = resultPath.resolve("tree.json");
        Path quantPath = resultPath.resolve("quant.json");
        Path protPath;
        TaxAnalysis.run(true, experimentPath, lcaPath, goAnnoPath, treePath, quantPath);
    }

    @Test
    public void gutNResult() throws IOException {
        Map<String, double[]> pept2Quant = Files.lines(Paths.get("/home/huangjs/Documents/map/shao/gut.tsv"))
                .skip(1)
                .map(line -> line.split("\t"))
                .collect(Collectors.toMap(
                        arr -> arr[0].replace("I", "L"),
                        arr -> new double[]{Double.parseDouble(arr[1]), Double.parseDouble(arr[2])},
                        (v1, v2) -> new double[]{v1[0] + v2[0], v1[1] + v1[1]}
                ));
        BufferedWriter nrWriter = Files.newBufferedWriter(Paths.get("/home/huangjs/Documents/map/shao/nr-taxon-distribution.tsv"));
        List<String[]> lines = Files.lines(Paths.get("/home/huangjs/Documents/map/shao/nr/lca.tsv"))
                .filter(line -> !(line.startsWith("#") || line.startsWith("Peptide")))
                .map(line -> line.split("\t", 12))
                .collect(Collectors.toList());
        Map<String[], double[]> taxon2Quant = lines.stream()
                .collect(Collectors.toMap(
                        arr -> Arrays.copyOfRange(arr, 4, 11), // lineage
                        arr -> pept2Quant.get(arr[0].replace("I", "L")) // quant value
                ));
        Taxon.Rank[] ranks = Taxon.Rank.values();
        for (int i = 0; i < ranks.length; i++) {
            final int j = i;
            Map<String, double[]> rank2Quant = taxon2Quant.entrySet().stream()
                    .collect(Collectors.groupingBy(e -> e.getKey()[j]))
                    .entrySet()
                    .stream()
                    .collect(Collectors.toMap(
                            e -> e.getKey(),
                            e -> {
                                double[] quant = e.getValue().stream().map(Map.Entry::getValue)
                                        .reduce(new double[]{0.0, 0.0}, (quant1, quant2) -> new double[]{quant1[0] + quant2[0], quant1[1] + quant2[1]});
                                return new double[]{quant[0], quant[1], e.getValue().size()};
                            }
                    ));
            nrWriter.write(String.format("Taxon on %s\tQuantitative(Area Sample 1)\tQuantitative percentage(Area Sample 1)\t" +
                            "Quantitative(Area Sample 2)\tQuantitative percentage(Area Sample 2)\t" +
                            "Unique peptide count\tUnique peptide percentage(Peptide count / %d)\n",
                    ranks[j], taxon2Quant.size()));
            double[] totalQuants = rank2Quant.values().stream()
                    .reduce(new double[]{0.0, 0.0}, (quant1, quant2) -> new double[]{quant1[0] + quant2[0], quant1[1] + quant2[1]});
            rank2Quant.entrySet().stream()
                    .sorted(Comparator.comparing((Map.Entry<String, double[]> e) -> e.getValue()[0]).reversed())
                    .forEach(e -> {
                        try {
                            double[] stat = e.getValue();
                            nrWriter.write(String.format("%s\t%.2f\t%.2f%%\t%.2f\t%.2f%%\t%d\t%.2f%%\n",
                                    e.getKey().length() == 0 ? "Specific to " + ranks[j] + " rank above" : e.getKey(),
                                    stat[0],
                                    stat[0] / totalQuants[0] * 100,
                                    stat[1],
                                    stat[1] / totalQuants[1] * 100,
                                    (int) stat[2],
                                    stat[2] / taxon2Quant.size() * 100
                            ));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    });
            nrWriter.newLine();
        }
        nrWriter.close();
    }

    @Test
    public void gutUnipeptResult() throws IOException {
        Map<String, double[]> pept2Quant = Files.lines(Paths.get("/home/huangjs/Documents/map/shao/gut.tsv"))
                .skip(1)
                .map(line -> line.split("\t"))
                .collect(Collectors.toMap(
                        arr -> arr[0].replace("I", "L"),
                        arr -> new double[]{Double.parseDouble(arr[1]), Double.parseDouble(arr[2])},
                        (v1, v2) -> new double[]{v1[0] + v2[0], v1[1] + v1[1]}
                ));
        BufferedWriter nrWriter = Files.newBufferedWriter(Paths.get("/home/huangjs/Documents/map/shao/unipept-taxon-distribution.tsv"));
        List<String[]> lines = Files.lines(Paths.get("/home/huangjs/Documents/map/shao/gut-lca-unipept.csv"))
                .filter(line -> !line.startsWith("peptide"))
                .map(line -> {
                    String[] tmp = line.split(",", 28);
                    return new String[]{tmp[0].replace("I", "L"), tmp[2], tmp[6], tmp[9], tmp[13], tmp[18], tmp[22], tmp[26]};
                })
                .collect(Collectors.toList());
        Map<String[], double[]> taxon2Quant = lines.stream()
                .collect(Collectors.toMap(
                        arr -> Arrays.copyOfRange(arr, 1, arr.length), // lineage
                        arr -> pept2Quant.get(arr[0]) // quant value
                ));
        Taxon.Rank[] ranks = Taxon.Rank.values();
        for (int i = 0; i < ranks.length; i++) {
            final int j = i;
            Map<String, double[]> rank2Quant = taxon2Quant.entrySet().stream()
                    .collect(Collectors.groupingBy(e -> e.getKey()[j]))
                    .entrySet()
                    .stream()
                    .collect(Collectors.toMap(
                            e -> e.getKey(),
                            e -> {
                                double[] quant = e.getValue().stream().map(Map.Entry::getValue)
                                        .reduce(new double[]{0.0, 0.0}, (quant1, quant2) -> new double[]{quant1[0] + quant2[0], quant1[1] + quant2[1]});
                                return new double[]{quant[0], quant[1], e.getValue().size()};
                            }
                    ));
            nrWriter.write(String.format("Taxon on %s\tQuantitative(Area Sample 1)\tQuantitative percentage(Area Sample 1)\t" +
                            "Quantitative(Area Sample 2)\tQuantitative percentage(Area Sample 2)\t" +
                            "Unique peptide count\tUnique peptide percentage(Peptide count / %d)\n",
                    ranks[j], taxon2Quant.size()));
            double[] totalQuants = rank2Quant.values().stream()
                    .reduce(new double[]{0.0, 0.0}, (quant1, quant2) -> new double[]{quant1[0] + quant2[0], quant1[1] + quant2[1]});
            rank2Quant.entrySet().stream()
                    .sorted(Comparator.comparing((Map.Entry<String, double[]> e) -> e.getValue()[0]).reversed())
                    .forEach(e -> {
                        try {
                            double[] stat = e.getValue();
                            nrWriter.write(String.format("%s\t%.2f\t%.2f%%\t%.2f\t%.2f%%\t%d\t%.2f%%\n",
                                    e.getKey().length() == 0 ? "Specific to " + ranks[j] + " rank above" : e.getKey(),
                                    stat[0],
                                    stat[0] / totalQuants[0] * 100,
                                    stat[1],
                                    stat[1] / totalQuants[1] * 100,
                                    (int) stat[2],
                                    stat[2] / taxon2Quant.size() * 100
                            ));
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    });
            nrWriter.newLine();
        }
        nrWriter.close();
    }


    /**
     * Unipept
     * @throws IOException
     */
    @Test
    public void runUnipeptWithQuant() throws IOException, InterruptedException {
        // format
        Path experimentPath = Paths.get("/home/huangjs/Documents/map/peptide-pipeline/unipept/pept.tsv");
        Path resultPath = Paths.get("/home/huangjs/Documents/map/peptide-pipeline/unipept");
        if (Files.notExists(resultPath)) {
            Files.createDirectories(resultPath);
        }
        Path lcaPath = resultPath.resolve("lca.tsv");
        Path goAnnoPath = resultPath.resolve("go.json");
        Path treePath = resultPath.resolve("tree.json");
        Path quantPath = resultPath.resolve("quant.json");
        TaxAnalysis.run(true, experimentPath, lcaPath, goAnnoPath, treePath, quantPath);
    }

    /**
     * Biomass Run1 U result
     * @throws IOException
     */
    @Test
    public void runBiomassU() throws IOException, InterruptedException {
        // format
        Path experimentPath = Paths.get("/home/huangjs/Documents/map/result/peptide/biomass/peptide_Run1.tsv");
        Path resultPath = Paths.get("/home/huangjs/Documents/map/result/peptide/biomass/nr");
        if (Files.notExists(resultPath)) {
            Files.createDirectories(resultPath);
        }
        Path lcaPath = resultPath.resolve("lca.tsv");
        Path goAnnoPath = resultPath.resolve("go.json");
        Path treePath = resultPath.resolve("tree.json");
        Path quantPath = resultPath.resolve("quant.json");
        TaxAnalysis.run(true, experimentPath, lcaPath, goAnnoPath, treePath, quantPath);
    }
}