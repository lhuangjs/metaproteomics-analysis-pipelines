package cn.phoenixcenter.metaproteomics.nr;

import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
import cn.phoenixcenter.metaproteomics.utils.ESUtil;
import cn.phoenixcenter.metaproteomics.utils.model.ESIndex;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class NewNRParserTest {

    NewNRParser parser = new NewNRParser(160);

    @Test
    public void splitNR() throws IOException {
        Path nrShardDirPath = Paths.get("/home/huangjs/data250/metadata/nr/output/nr-shards");
        parser.splitNR("/home/huangjs/data250/metadata/nr/data/nr.gz",
                nrShardDirPath);
    }

    @Test
    public void splitNR2() throws IOException {
        Path nrShardDirPath = Paths.get("/home/huangjs/Documents/nr-data/nr-shard");
        parser.splitNR(Paths.get("/media/huangjs/mydata/nr-data/nr"),
                nrShardDirPath);
    }

    @Test
    public void splitAcc2Tid() throws IOException {
        Path acc2TidShardPath = Paths.get("/home/huangjs/data250/metadata/nr/output/acc2tid-shards");
        parser.splitAcc2Tid(acc2TidShardPath,
                "/home/huangjs/data250/metadata/nr/data/pdb.accession2taxid.gz",
                "/home/huangjs/data250/metadata/nr/data/prot.accession2taxid.gz");
    }

    @Test
    public void addTidInfo() throws IOException {
        Path namesDmpPath = Paths.get("/home/huangjs/data250/metadata/nr/data/names.dmp");
        Path nrShardDirPath = Paths.get("/home/huangjs/Documents/nr-data/nr-shard");
        Path acc2TidShardPath = Paths.get("/home/huangjs/data250/metadata/nr/output/acc2tid-shards");
        Path nrWithTidPath = Paths.get("/home/huangjs/Documents/nr-data/nr-tid");
        Path nrWithIllegalTidPath = Paths.get("/home/huangjs/Documents/nr-data/nr-illegal-tid");
        Path nrWithIllegalPidPath = Paths.get("/home/huangjs/Documents/nr-data/nr-illegal-pid");
        parser.addTidInfo(namesDmpPath, nrShardDirPath, acc2TidShardPath, nrWithTidPath, nrWithIllegalTidPath, nrWithIllegalPidPath);
    }

    @Test
    public void extractIllegalTid() throws IOException {
        Path nrWithIllegalTidPath = Paths.get("/home/huangjs/Documents/nr-data/nr-illegal-tid");
        Path illegalTidPath = Paths.get("/home/huangjs/Documents/nr-data/illegal-tid");
        parser.extractIllegalTid(nrWithIllegalTidPath, illegalTidPath);
    }

    @Test
    public void updateTid() throws IOException {
        Path updatedTidPath = Paths.get("/home/huangjs/Documents/nr-data/tax_report.txt");
        Path nrWithIllegalTidPath = Paths.get("/home/huangjs/Documents/nr-data/nr-illegal-tid");
        Path nrWithUpdatedTidPath = Paths.get("/home/huangjs/Documents/nr-data/updated-nr-illegal-tid");
        parser.updateTid(updatedTidPath, nrWithIllegalTidPath, nrWithUpdatedTidPath);
    }

    @Test
    public void splitNRByGi() throws IOException {
        Path[] nrWithTidPaths = new Path[]{
                Paths.get("/home/huangjs/Documents/nr-data/nr-tid"),
                Paths.get("/home/huangjs/Documents/nr-data/updated-nr-illegal-tid")
        };
        Path nrGiShardDirPath = Paths.get("/home/huangjs/Documents/nr-data/nr-shard2");
        parser.splitNRByGi(nrWithTidPaths, nrGiShardDirPath);
    }

    @Test
    public void splitIDMappingByUid() throws IOException {
        Path idMappingGZPath = Paths.get("/home/huangjs/data250/metadata/nr/data/idmapping.dat.gz");
        Path idMappingUidShardDirPath = Paths.get("/home/huangjs/data250/metadata/nr/output/id-mapping-shards");
        parser.splitIDMappingByUid(idMappingGZPath, idMappingUidShardDirPath);
    }

    // 13m 30s
    @Test
    public void splitGOAByUid() throws IOException {
        Path goaGZPath = Paths.get("/home/huangjs/data250/metadata/nr/data/goa_uniprot_all.gaf.gz");
        Path goaUidShardDirPath = Paths.get("/home/huangjs/data250/metadata/nr/output/goa-shards");
        parser.splitGOAByUid(goaGZPath, goaUidShardDirPath);
    }

    // 11m 40s
    @Test
    public void addGOInfoForUid() throws IOException {
        Path idMappingUidShardDirPath = Paths.get("/home/huangjs/data250/metadata/nr/output/id-mapping-shards");
        Path goaUidShardDirPath = Paths.get("/home/huangjs/data250/metadata/nr/output/goa-shards");
        Path idMappingGiShardDirPath = Paths.get("/home/huangjs/data250/metadata/nr/output/id-mapping-goa-shards");
        parser.addGOInfoForUid(idMappingUidShardDirPath, goaUidShardDirPath, idMappingGiShardDirPath);
    }

    @Test
    public void addUidInfo() throws IOException {
        Path nrGiShardDirPath = Paths.get("/home/huangjs/Documents/nr-data/nr-shard2");
        Path idMappingGiShardDirPath = Paths.get("/home/huangjs/data250/metadata/nr/output/id-mapping-goa-shards");
        Path nrShardDirPath = Paths.get("/home/huangjs/data250/metadata/nr/output/nr-shards3");
        parser.addUidInfo(nrGiShardDirPath, idMappingGiShardDirPath, nrShardDirPath);
    }

    @Test
    public void storeInES() throws IOException, InterruptedException {
        ESIndex nrIndex = ESUtil.parseJsonIndex("nr-v1", parser.getNrIndexConfStream());
        ESUtil.createIndex(GlobalConfig.getObj(TransportClient.class), nrIndex, true);
        Path nrShardDirPath = Paths.get("/home/huangjs/data250/metadata/nr/output/nr-shards3");

        parser.storeInES(
                50_00,
                new ByteSizeValue(20, ByteSizeUnit.MB),
                3,
                nrIndex,
                nrShardDirPath
        );
    }
}