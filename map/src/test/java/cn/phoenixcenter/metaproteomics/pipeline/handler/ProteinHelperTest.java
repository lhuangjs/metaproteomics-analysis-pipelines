package cn.phoenixcenter.metaproteomics.pipeline.handler;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ProteinHelperTest {

    @Test
    public void parseLibrary() throws IOException {
        Path fastaPath = Paths.get("/home/huangjs/Documents/map/test/biomass_with_Pseudomonas.fasta");
        Path resultPath = Paths.get("/home/huangjs/Documents/map/test/handler/biomass_homo.json");
        Files.createDirectories(resultPath.getParent());
        ProteinHelper.writeJsonLibrary(fastaPath,
                ">(\\S+)",
                "\"taxon\":\\{\"id\":(\\d+)",
                resultPath);
    }

    @Test
    public void json2MPProtein() throws IOException {
        ProteinHelper.parseJsonLibrary("/home/huangjs/Documents/map/test/handler/biomass_homo.json",
                (MPProtein prot) -> System.out.println(prot));
    }

    @Test
    public void calQvalue() throws IOException {
        Map<String, Double> pid2Prob = new HashMap<>();
        pid2Prob.put("pro1", 1.0);
        pid2Prob.put("pro2", 1.0);
        pid2Prob.put("pro3", 0.7);
        pid2Prob.put("pro4", 0.8);
        pid2Prob.put("decoy_pro3", 0.2);
        pid2Prob.put("decoy_pro4", 0.3);
        ProteinHelper.calQvalue(pid2Prob, "decoy").entrySet().stream()
                .forEach(e -> System.out.println(e.getKey() + ": " + Arrays.stream(e.getValue())
                        .mapToObj(String::valueOf)
                        .collect(Collectors.joining(",")))
                );
    }

    @Test
    public void writeTSVProtein() throws IOException {
        Set<MPProtein> mpProteinSet = ProteinHelper.parseJsonMPProteins(
                "/home/huangjs/Documents/map/test/fido/fido.json",
                0.01);
        ProteinHelper.writeTSVProtein(
                mpProteinSet,
                Paths.get("/home/huangjs/Documents/map/test/handler/protein-Run1_P1-0.01.tsv")
        );
    }
}