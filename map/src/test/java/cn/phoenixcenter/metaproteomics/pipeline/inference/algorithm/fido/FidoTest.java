package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido;

import cn.phoenixcenter.metaproteomics.pipeline.converter.peptide.PeptideProphetConverter;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model.BipartiteGraphWithWeight;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model.FidoModel;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model.PSMNode;
import cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido.model.ProteinNode;
import org.junit.Test;
import umich.ms.fileio.exceptions.FileParsingException;

import java.util.List;

/**
 * Created by huangjs
 */
public class FidoTest {

    Fido fido = new Fido(";", "REV__", 0.1);

    @Test
    public void run() throws FileParsingException {
        String pepXML = "/home/huangjs/tppdata/F001652_my/F001652.xinteract.pep.xml";
        BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> graph = PeptideProphetConverter.convert2Fido(pepXML);
        List<BipartiteGraphWithWeight<ProteinNode, PSMNode, Double>> subgraphList = fido.initialize(graph);
        FidoModel model = new FidoModel(0.1, 0.05, 0.5, 0.1);
        for (BipartiteGraphWithWeight<ProteinNode, PSMNode, Double> subgraph : subgraphList) {
            fido.probRGivenD(subgraph, model);
            subgraph.getAllVerticesInA().stream()
                    .map(protNode -> protNode.getId() + ": " + (1 - protNode.getPosteriorErrorProb()))
                    .forEach(line -> System.out.println(line));
        }
    }
}