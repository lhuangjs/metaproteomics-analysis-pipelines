//package cn.phoenixcenter.metaproteomics.pipeline.peptide_validation;
//
//import cn.phoenixcenter.metaproteomics.pipeline.MascotPipeline;
//import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
//import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
//import cn.phoenixcenter.metaproteomics.pipeline.base.MPProteinGroup;
//import cn.phoenixcenter.metaproteomics.pipeline.processor.PeptidePostprocess;
//import cn.phoenixcenter.metaproteomics.pipeline.processor.ProteinGroupPostprocess;
//import cn.phoenixcenter.metaproteomics.pipeline.processor.ProteinGroupProcess;
//import cn.phoenixcenter.metaproteomics.pipeline.processor.ProteinPostprocess;
//import cn.phoenixcenter.metaproteomics.pipeline.utils.SequenceHandler;
//import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
//import org.dom4j.DocumentException;
//import org.junit.Test;
//
//import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.util.*;
//import java.util.regex.Pattern;
//import java.util.stream.Collectors;
//
//public class MascotPercolatorTest {
//
//    @Test
//    public void run() throws IOException {
//        Path datPath = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002589.dat");
//        Path featuresPath = Paths.get("/home/huangjs/Documents/metaprot/mascot-pipeline/F002589_feature.tsv");
//        Path xmlResultPath = Paths.get("/home/huangjs/Documents/metaprot/mascot-pipeline/F002589_percolator.xml");
//        MascotPercolator.run(datPath, datPath, featuresPath, xmlResultPath);
//    }
//
//    @Test
//    public void convert() throws DocumentException, IOException {
//        Set<MPPeptide> mpPeptideSet = new HashSet<>();
//        Set<MPProtein> mpProteinSet = new HashSet<>();
//        Path xmlResultPath = Paths.get("/home/huangjs/Documents/metaprot/mc-mp-pipeline/P_Run4_5/original/F002589_percolator.xml");
//        MascotPercolator.convert2PeptProt(xmlResultPath, 0.01, true, mpPeptideSet, mpProteinSet);
//        Path fidoResultPath = Paths.get("/home/huangjs/Documents/metaprot/mc-mp-pipeline/P_Run4_5/original/fido.result");
//        Set<MPProtein> mpProtSetForFido = new HashSet<>(mpProteinSet.size());
//        Set<MPPeptide> mpPeptSetForFido = new HashSet<>(mpPeptideSet.size());
//        MascotPipeline.runFidoModel(mpPeptideSet, mpProteinSet, 2, fidoResultPath);
//    }
//
//    @Test
//    public void updateFeaturesFile() throws IOException {
//        Pattern pidPat = Pattern.compile(">(\\S+)");
//        Path libraryPath = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/fasta/biomass.fasta");
//        Path oldFeaturesPath = Paths.get("/home/huangjs/Documents/metaprot/mc-mp-pipeline/P_Run4_5/original/F002589_features.tsv");
//        Path newFeaturesPath = Paths.get("/home/huangjs/Documents/metaprot/mc-mp-pipeline/P_Run4_5/original/new_F002589_features.tsv");
//        MascotPercolator.updateFeaturesFile(true, pidPat, libraryPath, "REV___", oldFeaturesPath, newFeaturesPath);
//    }
//
//    /**
//     * percolator xml -> fido -> group
//     */
//    @Test
//    public void runPipeline() throws DocumentException, IOException {
//        Pattern pidPat = Pattern.compile(">(\\S+)");
//        Pattern tidPat = Pattern.compile("\"id\":(\\d+)");
//        Path resultDirPath = Paths.get("/home/huangjs/Documents/metaprot/biomass");
//        Path distDirPath = resultDirPath.resolve("distribution");
//        if (Files.notExists(distDirPath)) {
//            Files.createDirectories(distDirPath);
//        }
//        Path fastaPath = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/fasta/biomass.fasta");
//        Path xmlResultPath = Paths.get("/home/huangjs/archive/java/metaproteomics/practise/crux/demo/percolator-output/percolator.pout.xml");
//        double psmFDR = 0.01;
//        double proteinFDR = 0.01;
//        boolean ignoreSharedPept = true;
//        String project = "Run1_U1";
//
//        Path protJsonPath = resultDirPath.resolve("biomass.json");
//        SequenceHandler.fasta2Json(fastaPath, pidPat, tidPat, protJsonPath);
//
//        /** build MPPeptide and MPProtein **/
//        Set<MPPeptide> mpPeptideSet = new HashSet<>();
//        Set<MPProtein> mpProteinSet = new HashSet<>();
//        MascotPercolator.convert2PeptProt(xmlResultPath, psmFDR, ignoreSharedPept, mpPeptideSet, mpProteinSet);
//        ProteinPostprocess.addTaxonInfo(mpProteinSet, protJsonPath);
//        // write peptide
//        PeptidePostprocess.writePeptide(mpPeptideSet, resultDirPath.resolve(psmFDR + "_" + project + "_peptide.txt"));
//
//        /** run fido **/
//        Path fidoResultPath = resultDirPath.resolve(psmFDR + "_" + project + "_fido.txt");
//        Set<MPProtein> mpProtSetForFido = new HashSet<>(mpProteinSet.size());
//        Set<MPPeptide> mpPeptSetForFido = new HashSet<>(mpPeptideSet.size());
//        MascotPipeline.runFidoModel(mpPeptideSet, mpProteinSet, 2, fidoResultPath);
//        MascotPipeline.parseFidoResult(mpPeptideSet, mpProteinSet, proteinFDR,
//                ignoreSharedPept, fidoResultPath,
//                mpPeptSetForFido, mpProtSetForFido);
//
//        // remove decoy proteins
//        mpProtSetForFido = new HashSet<>(ProteinPostprocess.delDecoyProts(mpProtSetForFido));
//        ProteinPostprocess.updatePeptProt(mpPeptSetForFido, mpProtSetForFido, ignoreSharedPept);
//
//        // filter by unique peptide count
//        mpProtSetForFido = new HashSet<>(ProteinPostprocess.filterByUniqPeptide(mpProtSetForFido, 3));
//        ProteinPostprocess.updatePeptProt(mpPeptSetForFido, mpProtSetForFido, ignoreSharedPept);
//
//        // write protein list
//        ProteinPostprocess.writeProtein(mpProtSetForFido, resultDirPath.resolve(project + "_protein-list.tsv"));
//        // write result based on protein
//        Map<Taxon, List<MPProtein>> taxon2Prots = ProteinPostprocess.groupByTaxon(mpProtSetForFido);
//        Path taxonDistOnProtPath = distDirPath.resolve(project);
//        ProteinPostprocess.writeTaxonDistribution(taxon2Prots, taxonDistOnProtPath);
//
//        /** group **/
//        List<MPProteinGroup> mpPGList = ProteinGroupProcess.groupProteins(mpProtSetForFido);
//        Map<Integer, Taxon> tid2Taxon = mpProtSetForFido.parallelStream()
//                .filter(prot -> prot.getTaxon() != null)
//                .collect(Collectors.toMap(
//                        prot -> prot.getTaxon().getId(),
//                        prot -> prot.getTaxon(),
//                        (oldValue, value) -> value
//                ));
//        ProteinGroupPostprocess.calLCA(mpPGList, tid2Taxon);
//        ProteinGroupPostprocess.calPSM(mpPGList);
//
//        // write protein groups
//        Path pgResultPath = resultDirPath.resolve(project + "_pg-result.tsv");
//        ProteinGroupPostprocess.writePGResult(mpPGList, pgResultPath);
//        // filter by LCA
//        List<MPProteinGroup> filteredMPPGList = ProteinGroupPostprocess
//                .filterByLCA(Taxon.Rank.GENUS, mpPGList, mpPeptSetForFido, true);
//
//        ProteinGroupPostprocess.calPSM(filteredMPPGList);
//        Map<Taxon, List<MPProteinGroup>> taxon2MPPGList = ProteinGroupPostprocess.groupByTaxon(filteredMPPGList);
//        ProteinGroupPostprocess.writeTaxonDistribution(taxon2MPPGList, distDirPath.resolve("pg_" + project));
//    }
//
//    /**
//     * percolator xml -> fido -> group
//     */
//    @Test
//    public void runPipeline2() throws DocumentException, IOException {
//        Pattern pidPat = Pattern.compile(">(\\S+)");
//        Pattern tidPat = Pattern.compile("\"id\":(\\d+)");
//        Path resultDirPath = Paths.get("/home/huangjs/Documents/metaprot/biomass");
//        Path distDirPath = resultDirPath.resolve("distribution");
//        if (Files.notExists(distDirPath)) {
//            Files.createDirectories(distDirPath);
//        }
//        Path fastaPath = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/fasta/biomass.fasta");
//        double peptFDR = 0.01;
//        double proteinFDR = 0.01;
//        boolean ignoreSharedPept = false;
//        int minUniqPeptCount = 2;
//        String[] projects = new String[]{"Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4"};
//        Path[] xmlResultPaths = Arrays.stream(projects)
//                .map(proj -> Paths.get("/home/huangjs/archive/java/metaproteomics/practise/crux/demo/percolator-" + proj + "/percolator.pout.xml"))
//                .toArray(Path[]::new);
//
//        for (int i = 0; i < projects.length; i++) {
//            String project = projects[i];
//            Path xmlResultPath = xmlResultPaths[i];
//            Path protJsonPath = resultDirPath.resolve("biomass.json");
//            SequenceHandler.fasta2Json(fastaPath, pidPat, tidPat, protJsonPath);
//
//            /** build MPPeptide and MPProtein **/
//            Set<MPPeptide> mpPeptideSet = new HashSet<>();
//            Set<MPProtein> mpProteinSet = new HashSet<>();
//            MascotPercolator.convert2PeptProt(xmlResultPath, peptFDR, ignoreSharedPept, mpPeptideSet, mpProteinSet);
//            ProteinPostprocess.addTaxonInfo(mpProteinSet, protJsonPath);
//            // write peptide
//            PeptidePostprocess.writePeptide(mpPeptideSet, resultDirPath.resolve(peptFDR + "_" + project + "_peptide.txt"));
//
//            /** run fido **/
//            Path fidoResultPath = resultDirPath.resolve(peptFDR + "_" + project + "_fido.txt");
//            Set<MPProtein> mpProtSetForFido = new HashSet<>(mpProteinSet.size());
//            Set<MPPeptide> mpPeptSetForFido = new HashSet<>(mpPeptideSet.size());
//            MascotPipeline.runFidoModel(mpPeptideSet, mpProteinSet, 2, fidoResultPath);
//            MascotPipeline.parseFidoResult(mpPeptideSet, mpProteinSet, proteinFDR,
//                    ignoreSharedPept, fidoResultPath,
//                    mpPeptSetForFido, mpProtSetForFido);
//
//            // remove decoy proteins
//            mpProtSetForFido = new HashSet<>(ProteinPostprocess.delDecoyProts(mpProtSetForFido));
//            ProteinPostprocess.updatePeptProt(mpPeptSetForFido, mpProtSetForFido, ignoreSharedPept);
//
//            // filter by unique peptide count
//            mpProtSetForFido = new HashSet<>(ProteinPostprocess.filterByUniqPeptide(mpProtSetForFido, minUniqPeptCount));
//            ProteinPostprocess.updatePeptProt(mpPeptSetForFido, mpProtSetForFido, ignoreSharedPept);
//
//            // write protein list
//            ProteinPostprocess.writeProtein(mpProtSetForFido, resultDirPath.resolve(project + "_protein-list.tsv"));
//            // write result based on protein
//            Map<Taxon, List<MPProtein>> taxon2Prots = ProteinPostprocess.groupByTaxon(mpProtSetForFido);
//            Path taxonDistOnProtPath = distDirPath.resolve(project);
//            ProteinPostprocess.writeTaxonDistribution(taxon2Prots, taxonDistOnProtPath);
//        }
//    }
//}