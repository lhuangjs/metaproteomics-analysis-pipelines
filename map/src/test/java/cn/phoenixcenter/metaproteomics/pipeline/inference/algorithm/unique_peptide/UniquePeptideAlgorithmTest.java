//package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.unique_peptide;
//
//import cn.phoenixcenter.metaproteomics.pipeline.base.*;
//import cn.phoenixcenter.metaproteomics.pipeline.converter.peptide.MaxQuantConverter;
//import cn.phoenixcenter.metaproteomics.pipeline.processor.ProteinGroupProcess;
//import cn.phoenixcenter.metaproteomics.pipeline.utils.DecoySeqFactory;
//import cn.phoenixcenter.metaproteomics.pipeline.utils.PeptideMapper;
//import org.junit.Test;
//
//import java.io.IOException;
//import java.utils.HashSet;
//import java.utils.Map;
//import java.utils.Set;
//import java.utils.concurrent.ExecutionException;
//import java.utils.regex.Pattern;
//import java.utils.stream.Collectors;
//
//public class UniquePeptideAlgorithmTest {
//
//    @Test
//    public void run() throws IOException, ExecutionException, InterruptedException {
//        /** obtain MPPeptide and MPProtein **/
//        String peptidesFile = "/home/huangjs/Documents/metaprot/best-peptide/peptides.txt";
//        MaxQuantConverter mqConverter = new MaxQuantConverter();
//        Set<MPPeptide> mpPeptideSet = mqConverter.convert2UniquePeptide(peptidesFile, true);
//        String decoyPrefix = "REV__";
//        DecoySeqFactory.Type decoyType = DecoySeqFactory.Type.MAXQUANT;
//        Set<String> peptideSet = mpPeptideSet.stream().map(MPPeptide::getSequence).collect(Collectors.toSet());
//        String fastaFile = "/home/huangjs/Documents/metaprot/best-peptide/Mock_Comm_RefDB_V3.fasta";
//        String pept2PidsFile = "/home/huangjs/Documents/metaprot/best-peptide/pept2pids.json";
//        Pattern pidPattern = Pattern.compile(">(\\S+)");
//        PeptideMapper pm = new PeptideMapper(decoyPrefix, decoyType, peptideSet);
////        pm.mapPept2Prots(fastaFile, pidPattern, pept2PidsFile);
//        Map<String, Set<String>> pept2Pids = pm.mapPept2Prots(pept2PidsFile);
//        Set<MPProtein> mpProteinSet = new HashSet<>();
////        pm.convert2PeptProt(pept2Pids, decoyPrefix, mpPeptideSet, mpProteinSet);
//
//        /** group protein **/
//        Set<MPPeptideWithType> mpPeptTypeSet = new HashSet<>(mpPeptideSet.size());
//        Set<MPProteinWithType> mpProtTypeSet = new HashSet<>(mpProteinSet.size());
//        ProteinGroupProcess pgHandler = new ProteinGroupProcess();
//        pgHandler.addParsimonyTypeInfo(mpPeptideSet, mpProteinSet,
//                mpPeptTypeSet, mpProtTypeSet);
//
//        Map<ParsimonyType, Long> peptType2Count = mpPeptTypeSet.stream()
//                .collect(Collectors.groupingBy(MPPeptideWithType::getParsimonyType,
//                        Collectors.counting()
//                ));
//        Map<ParsimonyType, Long> protType2Count = mpProtTypeSet.stream()
//                .collect(Collectors.groupingBy(MPProteinWithType::getParsimonyType,
//                        Collectors.counting()
//                ));
//        System.out.println(peptType2Count);
//        System.out.println(protType2Count);
//        System.out.println(mpProtTypeSet.parallelStream().filter(mpProteinWithRelationship -> !mpProteinWithRelationship.isTarget()).count());
//
//
//        /** unique peptide **/
//        UniquePeptideAlgorithm up = new UniquePeptideAlgorithm();
//        mpProteinSet = up.run(mpPeptideSet, mpProteinSet, 1);
//        System.err.println(mpProteinSet.size());
//
//        /** group protein **/
//        mpPeptTypeSet = new HashSet<>(mpPeptideSet.size());
//        mpProtTypeSet = new HashSet<>(mpProteinSet.size());
//        pgHandler = new ProteinGroupProcess();
//        pgHandler.addParsimonyTypeInfo(mpPeptideSet, mpProteinSet,
//                mpPeptTypeSet, mpProtTypeSet);
//
//        peptType2Count = mpPeptTypeSet.stream()
//                .collect(Collectors.groupingBy(MPPeptideWithType::getParsimonyType,
//                        Collectors.counting()
//                ));
//        protType2Count = mpProtTypeSet.stream()
//                .collect(Collectors.groupingBy(MPProteinWithType::getParsimonyType,
//                        Collectors.counting()
//                ));
//        System.out.println(peptType2Count);
//        System.out.println(protType2Count);
//        System.out.println(mpProtTypeSet.parallelStream().filter(mpProteinWithRelationship -> !mpProteinWithRelationship.isTarget()).count());
//    }
//}