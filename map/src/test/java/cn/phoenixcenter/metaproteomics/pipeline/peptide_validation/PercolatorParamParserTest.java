package cn.phoenixcenter.metaproteomics.pipeline.peptide_validation;

import cn.phoenixcenter.metaproteomics.pipeline.peptide_validation.model.PercolatorParam;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class PercolatorParamParserTest {

    @Test
    public void toParamFile() throws IOException, IllegalAccessException {
        PercolatorParamParser.toParamFile(new PercolatorParam(),
                Paths.get("/home/huangjs/Documents/metaprot/protein-pipeline/demo/percolator.param"));
    }
}