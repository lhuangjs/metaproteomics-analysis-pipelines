package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.fido;

import cn.phoenixcenter.metaproteomics.pipeline.MacroCommand;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProteinGroup;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.TideIndexCmd;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.TideParamParser;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.TideSearchCmd;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.model.TideParam;
import cn.phoenixcenter.metaproteomics.pipeline.handler.ProteinGroupHandler;
import cn.phoenixcenter.metaproteomics.pipeline.handler.ProteinHelper;
import cn.phoenixcenter.metaproteomics.pipeline.inference.FidoCmd;
import cn.phoenixcenter.metaproteomics.pipeline.peptide_validation.PercolatorCmd;
import cn.phoenixcenter.metaproteomics.pipeline.peptide_validation.model.PercolatorParam;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FidoCmdTest {

    @Test
    public void execute() throws Exception {
        TideParam tideParam = new TideParam();
        String fileroot = "species1";
        tideParam.setFileroot(fileroot);
        Path tideParamPath = Paths.get("/home/huangjs/Documents/metaprot/protein-pipeline/demo/tide.param");
        TideParamParser.toParamFile(tideParam, tideParamPath);
        String cruxLocation = "/home/huangjs/coding/java/java-web/metaproteomics/mpa/software/crux";
        String fastaFile = "/home/huangjs/Documents/metaprot/protein-pipeline/demo/species1.fasta";
        String libraryDir = "/home/huangjs/Documents/metaprot/protein-pipeline/demo/species1";
        String tiedIndexOut = "/home/huangjs/Documents/metaprot/protein-pipeline/demo/tide-index";
        TideIndexCmd tideIndexCmd = new TideIndexCmd(tideParamPath.toString(),
                fastaFile, libraryDir, tiedIndexOut);
        // tide-search
        String[] mgfFiles = new String[]{
                "/home/huangjs/Documents/metaprot/protein-pipeline/demo/example.mgf"
        };
        String tideSearchOut = "/home/huangjs/Documents/metaprot/protein-pipeline/demo/tide-search";
        TideSearchCmd tideSearchCmd = new TideSearchCmd(tideParamPath.toString(),
                mgfFiles, libraryDir, tideSearchOut);

        // percolator
        String featureFile = tideSearchOut + "/" + fileroot + ".tide-search.pin";
        String percolatorOut = "/home/huangjs/Documents/metaprot/protein-pipeline/demo/percolator";
        PercolatorParam percolatorParam = new PercolatorParam(percolatorOut, featureFile);
        PercolatorCmd percolatorCmd = new PercolatorCmd(percolatorParam);
        MacroCommand macroCommand = new MacroCommand();

        macroCommand.addCommand(tideIndexCmd, tideSearchCmd, percolatorCmd);
        macroCommand.execute();

        // fido
        String percolatorXml = "/home/huangjs/Documents/metaprot/protein-pipeline/demo/percolator/percolator.pout.xml";
        String fidoOutputFile = "/home/huangjs/Documents/metaprot/protein-pipeline/demo/percolator/fido.out";
        String libraryJsonFile = "/home/huangjs/Documents/map/test/handler/biomass_homo.json";
        FidoCmd fidoCmd = new FidoCmd().toBuilder()
                .percolatorXml(percolatorXml)
                .mpProteinJsonFile(fidoOutputFile)
                .build();
        fidoCmd.execute();
    }

    @Test
    public void execute1() throws Exception {
        String percolatorXml = "/home/huangjs/Documents/map/test/biomass-P/percolator/Run1_P1.percolator.pout.xml";
        String libraryJsonFile = "/home/huangjs/Documents/map/test/handler/biomass_homo.json";
        String mpProteinJsonFile = "/home/huangjs/Documents/map/test/fido/fido.json";
        FidoCmd fidoCmd = new FidoCmd().toBuilder()
                .percolatorXml(percolatorXml)
                .mpProteinJsonFile(mpProteinJsonFile)
                .accuracyLevel(3)
                .build();
        fidoCmd.execute();
    }

    @Test
    public void executeRun1_U() throws Exception {
        int uniqPeptThreshold = 1;
        // Run1_U_homo
        String[] projects = new String[]{
                "Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4"
        };
        String percolatorXMLDir = "/home/huangjs/Documents/map/test/biomass-homo/percolator";
        String resultDir = "/home/huangjs/Documents/map/test/biomass-homo/pg-psm-uniq" +uniqPeptThreshold + "-species";
        // Run1_P
//        String[] projects = new String[]{
//                "Run1_P1", "Run1_P2", "Run1_P3", "Run1_P4"
//        };
//        String percolatorXMLDir = "/home/huangjs/Documents/map/test/biomass-P/percolator";
//        String resultDir = "/home/huangjs/Documents/map/test/biomass-P/pg-psm-0.25";

//         Run1_U
//        String[] projects = new String[]{
//                "Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4"
//        };
//        String percolatorXMLDir = "/home/huangjs/Documents/map/test/biomass/percolator";
//        String resultDir = "/home/huangjs/Documents/map/test/biomass/pg-psm-uniq" + uniqPeptThreshold + "-species";


        double peptFDR = 0.01;
        double protFDR = 0.01;
        String libraryJsonFile = "/home/huangjs/Documents/map/test/handler/biomass_homo.json";
        if (Files.notExists(Paths.get(resultDir))) {
            Files.createDirectories(Paths.get(resultDir));
        }
        for (String project : projects) {
            // percolator - fido
            String mpProteinJsonFile = Paths.get(resultDir, project + ".fido.json").toString();
            FidoCmd fidoCmd = FidoCmd.builder()
                    .decoyPrefix("decoy_")
                    .percolatorXml(Paths.get(percolatorXMLDir, project + ".percolator.pout.xml").toString())
                    .peptFDRThreshold(peptFDR)
                    .accuracyLevel(2)
                    .mpProteinJsonFile(mpProteinJsonFile)
                    .build();
            fidoCmd.execute();
            Path pgOutputPath = Paths.get(resultDir, project + ".pg.tsv");
            List<MPProteinGroup> mpProtGroupList = ProteinGroupHandler.groupProteins(mpProteinJsonFile,
                    protFDR, libraryJsonFile, uniqPeptThreshold);
            ProteinGroupHandler.writeTSVProteinGroup(mpProtGroupList, pgOutputPath);

            // taxonomy distribution on protein level
            Map<Taxon, Double> taxon2DistOnProt = ProteinGroupHandler
                    .calTaxonDistribution(mpProtGroupList, Taxon.Rank.SPECIES);
            List<String> lines = taxon2DistOnProt.entrySet().stream()
                    .map(e -> e.getKey().getName() + "\t" + e.getValue())
                    .sorted()
                    .collect(Collectors.toList());
            lines.add(0, "Taxon\tDistribution");
            Files.write(Paths.get(resultDir, project + "_protein-group.taxon.tsv"),
                    lines);
        }
    }

    @Test
    public void writeMPProteins() throws IOException {
        String protJsonFile = "/home/huangjs/Documents/map/test/biomass-homo/pg-psm-uniq1-species/Run1_U1.fido.json";
        String libraryJsonFile = "/home/huangjs/Documents/map/test/handler/biomass_homo.json";
        Path mpProteinTSVPath = Paths.get("/home/huangjs/Documents/map/test/biomass-homo/pg-psm-uniq1-species/Run1_U1.fido.tsv");
        ProteinHelper.writeMPProteins(protJsonFile, libraryJsonFile, mpProteinTSVPath);
    }
}