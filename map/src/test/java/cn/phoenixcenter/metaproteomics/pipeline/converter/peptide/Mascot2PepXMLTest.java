package cn.phoenixcenter.metaproteomics.pipeline.converter.peptide;

import cn.phoenixcenter.metaproteomics.pipeline.converter.psm.Mascot2PepXML;
import cn.phoenixcenter.metaproteomics.utils.CommandExecutor;
import org.junit.Before;
import org.junit.Test;

import javax.xml.stream.XMLStreamException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by huangjs
 */
public class Mascot2PepXMLTest {

    String datFile = "/home/huangjs/tppdata/F001652_my/F001652.dat";
    String pepXMLOutput = "/home/huangjs/tppdata/F001652_my/F001652.pep.xml";
    String libFile = "/home/huangjs/tppdata/F001652_my/Mock_Comm_RefDB_V3.fasta";
    String comLibFile = "/home/huangjs/tppdata/F001652_my/Com_Mock_Comm_RefDB_V3.fasta";
    Mascot2PepXML converter;

    @Before
    public void setUp() {
        converter = new Mascot2PepXML();
    }

    @Test
    public void convert() throws XMLStreamException, IOException {
        converter.convert("REV__", libFile, datFile, comLibFile, pepXMLOutput);
    }

    @Test
    public void convert2() throws XMLStreamException, IOException, InterruptedException {
        converter.convert("REV__", libFile, datFile, comLibFile, pepXMLOutput);
        String xinteractPepXML = "/home/huangjs/tppdata/F001652_my/F001652.xinteract.pep.xml";
        String peptCom = "/home/huangjs/coding/java/java-web/metaproteomics/mpa/src/main/resources/software/xinteract " +
                "-N" + xinteractPepXML + " -p0.0 -l7 -PPM -O " + pepXMLOutput;
        CommandExecutor.exec(peptCom);
        String protXML = "/home/huangjs/tppdata/F001652_my/F001652.xinteract.prot.xml";
        String protCom = "/home/huangjs/coding/java/java-web/metaproteomics/mpa/src/main/resources/software/ProteinProphet " +
                xinteractPepXML + " " + protXML;
        CommandExecutor.exec(protCom);
        System.out.println(peptCom);
        System.out.println(protCom);
    }

    @Test
    public void gen() throws IOException {
        String decoyPrefix = "REV__";
        String libraryFile = "/home/huangjs/tppdata/F001652_my/Mock_Comm_RefDB_V3.fasta";
        String decoyLibFile = "/home/huangjs/tppdata/F001652_my/Decoy_Mock_Comm_RefDB_V3.fasta";
        BufferedReader br = Files.newBufferedReader(Paths.get(libraryFile), StandardCharsets.UTF_8);
        BufferedWriter bw = Files.newBufferedWriter(Paths.get(decoyLibFile), StandardCharsets.UTF_8,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        String line;
        String header = null;
        StringBuilder sequence = new StringBuilder();
        while ((line = br.readLine()) != null) {
            if (line.startsWith(">")) {
                if (header != null) {
                    // decoy
                    bw.write(">" + decoyPrefix + header.substring(1) + System.lineSeparator());
                    bw.write(sequence.reverse().toString() + System.lineSeparator());
                    // reset
                    sequence.delete(0, sequence.length());
                }
                header = line;
            } else {
                sequence.append(line);
            }
        }
        // the last
        // target
        bw.write(header + System.lineSeparator());
        bw.write(sequence.toString() + System.lineSeparator());
        // decoy
        bw.write(">" + decoyPrefix + header.substring(1));
        bw.write(sequence.reverse().toString() + System.lineSeparator());
        br.close();
        bw.close();
    }

    @Test
    public void extractTargetDecoyIdForFido() throws IOException {
        Pattern pidPat = Pattern.compile(">([A-Za-z0-9_.]+)");
        String decoyPrefix = "REV__";
        String libraryFile = "/home/huangjs/tppdata/F001652_my/Mock_Comm_RefDB_V3.fasta";
        String resultFile = "/home/huangjs/archives/java/metaproteomics/practise/Fido/fido/bin/fido_ids.txt";
        List<String> targetList = Files.readAllLines(Paths.get(libraryFile))
                .stream()
                .filter(line -> line.startsWith(">"))
                .map(anno -> {
                    Matcher m = pidPat.matcher(anno);
                    if (m.find()) {
                        return m.group(1);
                    }
                    return null;
                })
                .collect(Collectors.toList());
        List<String> decoyList = targetList.parallelStream()
                .map(pid -> decoyPrefix + pid)
                .collect(Collectors.toList());
        List<String> rs = new ArrayList<>(2);
        rs.add("{ " + targetList.stream().collect(Collectors.joining(" , ")) + " }");
        rs.add("{ " + decoyList.stream().collect(Collectors.joining(" , ")) + " }");
        Files.write(Paths.get(resultFile), rs);
    }
}