package cn.phoenixcenter.metaproteomics.pipeline.utils;

import lombok.*;
import org.junit.Test;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CloneUtilTest {

    @Test
    public void deepCopy() {
        Female lili = new Female();
        Male tom = new Male();
        lili.setName("lili");
        lili.setAge(12);
        tom.setName("tom");
        tom.setAge(14);
        lili.setRelatedMales(Stream.of(tom).collect(Collectors.toSet()));
//        tom.setRelatedFemale(Stream.of(lili).collect(Collectors.toSet()));

        Female clonedLili = CloneUtil.deepCopy(lili);
        System.out.println(lili.hashCode());
        System.out.println(lili);
        System.out.println(clonedLili.hashCode());
        System.out.println(clonedLili);
    }
}


@Getter
@Setter
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
class Female {
    public String name;
    public int age;
    public Set<Male> relatedMales;

}

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PUBLIC)
class Male {
    public String name;
    public int age;
    @ToString.Exclude
    public Set<Female> relatedFemale;

}