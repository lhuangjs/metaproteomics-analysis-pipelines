package cn.phoenixcenter.metaproteomics.pipeline.handler;

import org.dom4j.DocumentException;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class PeptideHandlerTest {

    @Test
    public void extractPeptide() throws IOException, DocumentException {
        // Run1_U
        String[] projects = new String[]{
                "Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4"
        };
        String[] percolatorXMLFiles = Arrays.stream(projects)
                .map(id -> "/home/huangjs/Documents/map/test/biomass/percolator/" + id + ".percolator.pout.xml")
                .toArray(String[]::new);
        PeptideHandler.extractPeptide(0.01, percolatorXMLFiles,
                "/home/huangjs/Documents/map/test/nr-search/Run1_U-0.01.tsv");

    }

    @Test
    public void distribution() throws IOException {
        Map<String, Long> psmCount2Count = Files.lines(Paths.get("/home/huangjs/Documents/map/test/nr-search/Run1_U-0.01.tsv"))
                .map(line -> line.split("\t"))
                .skip(1)
                .collect(Collectors.groupingBy((String[] arr) -> arr[2],
                        Collectors.counting()));
        System.out.println(psmCount2Count);
    }
}