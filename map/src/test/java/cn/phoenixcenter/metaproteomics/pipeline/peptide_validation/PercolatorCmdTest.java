package cn.phoenixcenter.metaproteomics.pipeline.peptide_validation;

import cn.phoenixcenter.metaproteomics.pipeline.MacroCommand;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.TideIndexCmd;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.TideParamParser;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.TideSearchCmd;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.model.TideParam;
import cn.phoenixcenter.metaproteomics.pipeline.peptide_validation.model.PercolatorParam;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PercolatorCmdTest {

    @Test
    public void toJsonFile() throws IOException, IllegalAccessException {
        PercolatorParamParser.toJsonFile(new PercolatorParam(),
                Paths.get("/home/huangjs/coding/vue/metaproteomics/pipeline-reconstruction-front-end/src/plugins/PercolatorParam.json"));
    }

    @Test
    public void toOptions() throws IllegalAccessException {
        PercolatorParam percolatorParam = new PercolatorParam("out", "feature");
        System.out.println(PercolatorParamParser.toOptions(percolatorParam));
    }

    @Test
    public void execute() throws Exception {
        TideParam tideParam = new TideParam();
//        String fileroot = "species1";
//        tideParam.setFileroot(fileroot);
        Path tideParamPath = Paths.get("/home/huangjs/Documents/metaprot/protein-pipeline/demo/tide.param");
        TideParamParser.toParamFile(tideParam, tideParamPath);
        String cruxLocation = "/home/huangjs/coding/java/java-web/metaproteomics/mpa/software/crux";
        String fastaFile = "/home/huangjs/Documents/metaprot/protein-pipeline/demo/species1.fasta";
        String libraryDir = "/home/huangjs/Documents/metaprot/protein-pipeline/demo/species1";
        String tiedIndexOut = "/home/huangjs/Documents/metaprot/protein-pipeline/demo/tide-index";
        TideIndexCmd tideIndexCmd = new TideIndexCmd(tideParamPath.toString(),
                fastaFile, libraryDir, tiedIndexOut);
        // tide-search
        String[] mgfFiles = new String[]{
                "/home/huangjs/Documents/metaprot/protein-pipeline/demo/example.mgf"
        };
        String tideSearchOut = "/home/huangjs/Documents/metaprot/protein-pipeline/demo/tide-search";
        TideSearchCmd tideSearchCmd = new TideSearchCmd(tideParamPath.toString(),
                mgfFiles, libraryDir, tideSearchOut);

        // percolator
//        String featureFile = tideSearchOut + "/" + fileroot + ".tide-search.pin";
        String featureFile = tideSearchOut + "/" + "tide-search.pin";
        String percolatorOut = "/home/huangjs/Documents/metaprot/protein-pipeline/demo/percolator";
        PercolatorParam percolatorParam = new PercolatorParam(percolatorOut, featureFile);
        PercolatorCmd percolatorCmd = new PercolatorCmd(percolatorParam);
        MacroCommand macroCommand = new MacroCommand();

        macroCommand.addCommand(tideIndexCmd, tideSearchCmd, percolatorCmd);
        macroCommand.execute();
    }

    @Test
    public void cruxParam2Tab() throws IOException {
        Path paramPath = Paths.get("/home/huangjs/Downloads/percolator.txt");
        Path outPath = Paths.get("/home/huangjs/Downloads/percolator.tsv");
        BufferedReader br = Files.newBufferedReader(paramPath);
        BufferedWriter bw = Files.newBufferedWriter(outPath);
        String line;
        while ((line = br.readLine()) != null) {
            line = line.trim();
            if (line.startsWith("USAGE:")) {
                bw.write(line);
                while (!(line = br.readLine()).startsWith("REQUIRED ARGUMENTS:")) {
                    line = line.trim();
                    if (line.length() > 0) {
                        bw.write(line);
                    }
                }
            } else if (line.startsWith("OPTIONAL ARGUMENTS:")) {
                while ((line = br.readLine()) != null) {
                    line = line.trim();
                    if (line.startsWith("[")) {
                        bw.newLine();
                        bw.write(line + "\t");
                    } else if (line.length() > 0) {
                        bw.write(line + " ");
                    }
                }
            }
        }
        bw.close();
        br.close();
    }
}