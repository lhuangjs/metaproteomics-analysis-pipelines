//package cn.phoenixcenter.metaproteomics.pipeline.processor;
//
//import cn.phoenixcenter.metaproteomics.config.GlobalConfig;
//import cn.phoenixcenter.metaproteomics.pipeline.base.MPProteinGroup;
//import cn.phoenixcenter.metaproteomics.taxonomy.TaxonSearcher;
//import org.junit.Test;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
//public class ProteinGroupPostprocessTest {
//
//    @Test
//    public void toTree() throws IOException {
//        TaxonSearcher taxonSearcher = GlobalConfig.getObj(TaxonSearcher.class);
//        List<MPProteinGroup> mpPGList = new ArrayList<>();
//        MPProteinGroup g1 = new MPProteinGroup();
//        g1.setLca(taxonSearcher.getTaxonById(40520));
//        MPProteinGroup g2 = new MPProteinGroup();
//        g2.setLca(taxonSearcher.getTaxonById(418240));
//        mpPGList.add(g1);
//        mpPGList.add(g2);
//        String treeJsonFile = "/home/huangjs/Documents/metaprot/mc-pp-pipeline/tree.json";
//        ProteinGroupPostprocess.toTree(mpPGList, treeJsonFile);
//    }
//}