package cn.phoenixcenter.metaproteomics.pipeline.database_search.tide;

import cn.phoenixcenter.metaproteomics.base.ICommand;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;

public class TideIndexCmdTest {
    @Test
    public void writeExternal() throws IOException, ClassNotFoundException {
        ICommand command = new TideIndexCmd("param", "fasta", "library", "output");
        // serialize
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutput out = new ObjectOutputStream(baos);
        out.writeObject(command);
        out.close();
        baos.close();
        // deserialize
        ObjectInput in = new ObjectInputStream(new ByteArrayInputStream(baos.toByteArray()));
        ICommand commandCopy = ICommand.toObj(in);
        in.close();
        System.out.println(commandCopy);
    }
}