package cn.phoenixcenter.metaproteomics.pipeline.converter.peptide;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import org.junit.Test;

import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

public class PepxmlParserTest {

    @Test
    public void parse() throws FileNotFoundException, XMLStreamException {
        Map<String, Integer> label2Count = new HashMap<>();
        BiConsumer<Integer, MPPeptide> consumer = (Integer rank, MPPeptide mpPeptide) -> {
            if (rank.intValue() == 1) {
                Set<String> labelSet = mpPeptide.getAssociatedProteinSet().stream()
                        .map(prot -> prot.getId().substring(0, prot.getId().indexOf("_")))
                        .collect(Collectors.toSet());
                for (String label : labelSet) {
                    if (label2Count.containsKey(label)) {
                        label2Count.put(label, label2Count.get(label) + 1);
                    } else {
                        label2Count.put(label, 1);
                    }
                }
            }
        };
        PepxmlParser.parse("/home/huangjs/Documents/Run1_U1_2000ng.pep.xml", consumer);
        label2Count.put("137;259", label2Count.get("137") + label2Count.get("259"));
        label2Count.put("841;VF", label2Count.get("841") + label2Count.get("VF"));
        label2Count.remove("137");
        label2Count.remove("259");
        label2Count.remove("841");
        label2Count.remove("VF");
        label2Count.remove("CRAP");
        int totalCount = label2Count.values().stream()
                .mapToInt(count -> count)
                .sum();
        label2Count.entrySet().stream()
                .sorted(Comparator.comparing(e -> e.getKey()))
                .forEach(e -> System.out.println(e.getKey() + ": " + (100.0 * e.getValue() / totalCount)));
    }

    @Test
    public void search() throws IOException {
        PepxmlParser.search("/home/huangjs/Documents/Run1_U1_2000ng.pep.xml", "BXL");
    }
}