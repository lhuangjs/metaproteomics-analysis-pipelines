package cn.phoenixcenter.metaproteomics.pipeline.utils;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SequenceHandlerTest {

    @Test
    public void fasta2Json() {
        Path fastaPath = Paths.get("/home/huangjs/Documents/metaprot/biomass/biomass.fasta");
        Pattern pidPat = Pattern.compile(">(\\S+)");
        Pattern tidPat = Pattern.compile("\"tid\":(\\d+)");
        Path jsonPath = Paths.get("/home/huangjs/Documents/metaprot/fasta-handler/demo.json");
        SequenceHandler.fasta2Json(fastaPath, pidPat, tidPat, jsonPath);
    }

    @Test
    public void json2MPProtein() {
        Path jsonPath = Paths.get("/home/huangjs/Documents/metaprot/fasta-handler/demo.json");
        SequenceHandler.json2MPProteins(jsonPath, (MPProtein mpProt) -> System.out.println(mpProt));
    }

    @Test
    public void setAltProteinSet() {
        MPPeptide mpPept1 = new MPPeptide("pept1");

        MPProtein mpProt1 =  new MPProtein("prot1");
        MPProtein mpProt2 =  new MPProtein("prot2");
        mpProt1.setContainedPeptideSet(Stream.of(mpPept1).collect(Collectors.toSet()));
        mpProt2.setContainedPeptideSet(Stream.of(mpPept1).collect(Collectors.toSet()));
        mpPept1.setAssociatedProteinSet(Stream.of(mpProt1, mpProt2).collect(Collectors.toSet()));
        Set<MPProtein> mpProtSet = Stream.of(mpProt1, mpProt2).collect(Collectors.toSet());
        SequenceHandler.setAltProteinSet(mpProtSet);
        System.out.println(mpProtSet);
    }
}