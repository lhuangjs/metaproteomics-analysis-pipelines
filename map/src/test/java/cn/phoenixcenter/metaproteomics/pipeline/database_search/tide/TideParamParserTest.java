package cn.phoenixcenter.metaproteomics.pipeline.database_search.tide;

import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.model.TideParam;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;

public class TideParamParserTest {

    @Test
    public void toParamFile() throws IOException, IllegalAccessException {
        TideParam tideParam = new TideParam();
        TideParamParser.toParamFile(tideParam, Paths.get("/home/huangjs/Documents/map/test/tide/param.properties"));
    }

    @Test
    public void printMods() {
        TideParamParser.printMods();
    }

    @Test
    public void toJsonFile() throws IOException, IllegalAccessException {
        TideParam tideParam = new TideParam();
        TideParamParser.toJsonFile(tideParam, Paths.get("/home/huangjs/Documents/metaprot/tide/param.json"));
    }
}