package cn.phoenixcenter.metaproteomics.pipeline.converter.psm;

import cn.phoenixcenter.metaproteomics.pipeline.utils.DecoySeqFactory;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Mascot2XMLEnhancerTest {

    @Test
    public void convert() throws IOException {
        Path datPath = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/Run1_U/class1/F001238.dat");
        Path fastaPath = Paths.get("/home/huangjs/Documents/metaprot/dat2pepxml/Mock_Comm_RefDB_V3.fasta");
        Path pepxmlPath = Paths.get("/home/huangjs/Documents/metaprot/dat2pepxml/F001238.pep.xml");
        /** generate decoy library **/
        String decoyPrefix = "REV__";
        Path libraryWithDecoyPath = Paths.get("/home/huangjs/Documents/metaprot/dat2pepxml/biomass-with-revered-decoy.fasta");
        DecoySeqFactory.genLibraryWithDecoy(fastaPath, decoyPrefix,
                DecoySeqFactory.Type.REVERSE, libraryWithDecoyPath);

        Map<String, String> params = new HashMap<>();
        params.put("-E", "trypsin");
        Mascot2XMLEnhancer.convertAutoDecoyMode(datPath, params, decoyPrefix,
                libraryWithDecoyPath, pepxmlPath);
    }
}