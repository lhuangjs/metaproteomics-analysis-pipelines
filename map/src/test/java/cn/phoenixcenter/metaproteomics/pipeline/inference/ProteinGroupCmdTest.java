package cn.phoenixcenter.metaproteomics.pipeline.inference;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProteinGroup;
import cn.phoenixcenter.metaproteomics.pipeline.handler.ProteinGroupHandler;
import cn.phoenixcenter.metaproteomics.pipeline.handler.ProteinHelper;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ProteinGroupCmdTest {

    @Test
    public void execute() throws Exception {
        ProteinGroupCmd proteinGroupCmd = ProteinGroupCmd.builder()
                .proteinJsonFile("/home/huangjs/Documents/map/test/fido/fido.json")
                .proteinGroupTSVFile("/home/huangjs/Documents/map/test/fido/protein-group.txt")
                .build();
        proteinGroupCmd.execute();
    }

    @Test
    public void biomass() throws Exception {
        String[] projects = new String[]{
                "Run1_U2"
        };
//        String[] projects = new String[]{
//                "Run1_U1", "Run1_U2", "Run1_U3", "Run1_U4"
//        };


        String percolatorXMLDir = "/home/huangjs/Documents/map/test/biomass/percolator";
        String resultDir = "/home/huangjs/Documents/map/test/biomass/fido-single";
//        String[] projects = new String[]{
//                "Run1_P1", "Run1_P2","Run1_P3", "Run1_P4"
//        };
//        String percolatorXMLDir = "/home/huangjs/Documents/map/test/biomass_Run4_U/percolator";
//        String resultDir = "/home/huangjs/Documents/map/test/biomass_Run4_U/fido_al";
        double peptFDR = 0.01;
        double protFDR = 0.01;
        if (Files.notExists(Paths.get(resultDir))) {
            Files.createDirectories(Paths.get(resultDir));
        }
        for (String project : projects) {
            // percolator - fido
            String protJsonFile = Paths.get(resultDir, project + ".fido.json").toString();
            FidoCmd fidoCmd = FidoCmd.builder()
                    .decoyPrefix("decoy_")
                    .percolatorXml(Paths.get(percolatorXMLDir, project + ".percolator.pout.xml").toString())
                    .peptFDRThreshold(0.01)
                    .accuracyLevel(2)
                    .mpProteinJsonFile(protJsonFile)
                    .build();
            fidoCmd.execute();
            Path filteredProteinTSVPath = Paths.get(resultDir, project + "_" + protFDR + "_protein_.tsv");
            Set<MPProtein> mpProteinSet = ProteinHelper.parseJsonMPProteins(protJsonFile, protFDR);
            ProteinHelper.writeTSVProtein(mpProteinSet, filteredProteinTSVPath);
            // taxonomy distribution on protein level
            Map<Taxon, Double> taxon2DistOnProt = ProteinHelper.calTaxonDistribution(mpProteinSet, Taxon.Rank.SPECIES);
            List<String> lines = taxon2DistOnProt.entrySet().stream()
                    .map(e -> e.getKey().getName() + "\t" + e.getValue())
                    .sorted()
                    .collect(Collectors.toList());
            lines.add(0, "taxon");
            Files.write(Paths.get(resultDir, project + "_" + protFDR + "_prot.taxon.tsv"),
                    lines);

            // fido - protein group
            ProteinGroupCmd proteinGroupCmd = new ProteinGroupCmd().toBuilder()
                    .proteinJsonFile(protJsonFile)
                    .protFDRThreshold(protFDR)
                    .proteinGroupTSVFile(Paths.get(resultDir, project + "_protein-group.tsv").toString())
                    .build();
            proteinGroupCmd.execute();
            List<MPProteinGroup> mpProteinGroupList = proteinGroupCmd.getMpProteinGroupList();

            // taxonomy distribution on protein group level
            Map<Taxon, Double> taxon2DistOnPG = ProteinGroupHandler
                    .calTaxonDistribution(mpProteinGroupList, Taxon.Rank.SPECIES);
           lines = taxon2DistOnPG.entrySet().stream()
                    .map(e -> e.getKey().getName() + "\t" + e.getValue())
                    .sorted()
                    .collect(Collectors.toList());
            lines.add(0, "taxon");
            Files.write(Paths.get(resultDir, project + "_" + protFDR + "_protein-group.taxon.tsv"),
                    lines);
        }
    }
}