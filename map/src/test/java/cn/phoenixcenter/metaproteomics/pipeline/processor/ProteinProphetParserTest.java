package cn.phoenixcenter.metaproteomics.pipeline.processor;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPProteinGroup;
import org.junit.Before;
import org.junit.Test;
import umich.ms.fileio.exceptions.FileParsingException;

import java.io.IOException;
import java.util.List;

/**
 * Created by huangjs
 */
public class ProteinProphetParserTest {
    ProteinProphetParser parser = new ProteinProphetParser();

    @Before
    public void setUp() {
        String qvalityLoc = "/home/huangjs/coding/java/java-web/metaproteomics/mpa/src/main/resources/software/qvality";
        parser.setQvalityLocation(qvalityLoc);
    }

    @Test
    public void parser() throws FileParsingException, IOException, InterruptedException {
        String protXML = "/home/huangjs/tppdata/F001652_my/F001652.xinteract.prot.xml";
        List<MPProteinGroup> mpPGList = parser.parse(protXML, "REV__");
        parser.writeTSV(mpPGList, "/home/huangjs/tppdata/F001652_my/before_qvalue.tsv");
        parser.calQvalue(mpPGList);
        int candidateCount = 0;
        for (MPProteinGroup pg : mpPGList) {
            if (pg.getQvalue() <= 0.01) {
                candidateCount++;
            }
        }
        System.out.println("<=0.01 : " + candidateCount);
        System.out.println("<=0.01 : " + ((double) candidateCount / mpPGList.size()));
        parser.writeTSV(mpPGList, "/home/huangjs/tppdata/F001652_my/F001652_PG.tsv");
        parser.calQvalue2(mpPGList);
        parser.writeTSV(mpPGList, "/home/huangjs/tppdata/F001652_my/F001652_PG2.tsv");
    }

    @Test
    public void statistic() throws FileParsingException {
        String protXML = "/home/huangjs/tppdata/F001652_my/F001652.xinteract.prot.xml";
        parser.statistic(protXML);
    }
}