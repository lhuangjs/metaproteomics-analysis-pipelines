//package cn.phoenixcenter.metaproteomics.pipeline;
//
//import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
//import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
//import cn.phoenixcenter.metaproteomics.pipeline.base.MPProteinGroup;
//import cn.phoenixcenter.metaproteomics.pipeline.converter.peptide.PeptideProphetConverter;
//import cn.phoenixcenter.metaproteomics.pipeline.converter.psm.Mascot2XMLEnhancer;
//import cn.phoenixcenter.metaproteomics.pipeline.peptide_validation.MascotPercolator;
//import cn.phoenixcenter.metaproteomics.pipeline.peptide_validation.PeptideProphetEnhancer;
//import cn.phoenixcenter.metaproteomics.pipeline.processor.PeptidePostprocess;
//import cn.phoenixcenter.metaproteomics.pipeline.processor.ProteinGroupPostprocess;
//import cn.phoenixcenter.metaproteomics.pipeline.processor.ProteinGroupProcess;
//import cn.phoenixcenter.metaproteomics.pipeline.processor.ProteinPostprocess;
//import cn.phoenixcenter.metaproteomics.pipeline.utils.DecoySeqFactory;
//import cn.phoenixcenter.metaproteomics.pipeline.utils.SequenceHandler;
//import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
//import lombok.extern.log4j.Log4j2;
//import org.dom4j.DocumentException;
//import org.junit.Test;
//import umich.ms.fileio.exceptions.FileParsingException;
//
//import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.util.*;
//import java.util.regex.Pattern;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//@Log4j2
//public class MascotPipelineTest {
//
//    @Test
//    public void extractPeptdes() throws IOException {
//        Path[] peptPaths = Stream.of("F002614", "F002615", "F002616", "F002617")
//                .map(id -> Paths.get("/home/huangjs/Documents/metaprot/mc-pp-pipeline/P_Run4_5/homo1/", id + "_" + "peptides.txt"))
//                .toArray(Path[]::new);
//        Map<String, Double> pept2Prob = new HashMap<>();
//        for (Path path : peptPaths) {
//            Files.readAllLines(path)
//                    .stream()
//                    .skip(1)
//                    .forEach(line -> {
//                        String[] tmp = line.split("\t");
//                        if (pept2Prob.containsKey(tmp[0])) {
//                            double prob = Double.parseDouble(tmp[1]);
//                            if (prob > pept2Prob.get(tmp[0])) {
//                                pept2Prob.put(tmp[0], Double.parseDouble(tmp[1]));
//                            }
//                        } else {
//                            pept2Prob.put(tmp[0], Double.parseDouble(tmp[1]));
//                        }
//                    });
//        }
//        List<String> lines = new ArrayList();
//        lines.add("peptide\tprob");
//        lines.addAll(
//                pept2Prob.entrySet().stream()
//                        .map(e -> e.getKey() + "\t" + e.getValue())
//                        .collect(Collectors.toList())
//        );
//        Files.write(Paths.get("/home/huangjs/metadata/taxonomy/homo1", "experiment.tsv"), lines);
//    }
//
//    /**
//     * Run4&5 P, original library
//     * ProteinProphet -> Fido -> taxon distribution on protein level
//     *
//     * @throws IOException
//     * @throws FileParsingException
//     */
//    @Test
//    public void proteinPipeline() throws IOException, FileParsingException {
////        String[] datFiles = new String[]{
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002589.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002591.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002593.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002595.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002597.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002599.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002601.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002603.dat"
////        };
//
//        String[] datFiles = new String[]{
//                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002589.dat",
//                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002591.dat",
//                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002593.dat",
//                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002595.dat"
//        };
////        String[] datFiles = new String[]{
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002589.dat"
////        };
//
////        String[] datFiles = new String[]{
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/homo1/F002614.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/homo1/F002615.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/homo1/F002616.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/homo1/F002617.dat"
////        };
//
////        String[] datFiles = new String[]{
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/2uniq-pept-genus/F002640.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/2uniq-pept-genus/F002641.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/2uniq-pept-genus/F002642.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/2uniq-pept-genus/F002643.dat"
////        };
//
//        // result dir
//        String resultDir = "/home/huangjs/Documents/metaprot/mc-pp-pipeline/P_Run4_5/original/";
////        String resultDir = "/home/huangjs/Documents/metaprot/mc-pp-pipeline/P_Run4_5/homo1/";
////        String resultDir = "/home/huangjs/Documents/metaprot/mc-pp-pipeline/P_Run4_5/uniq2";
//
//        // fasta -> json
//        Path fastaPath = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/fasta/biomass.fasta");
////        Path fastaPath = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/fasta/biomass_with_Pseudomonas.fasta");
////        Path fastaPath = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/fasta/proteins-2uniq-peptides-genus.fasta");
//
//        Pattern pidPat = Pattern.compile(">(\\S+)");
//        Pattern tidPat = Pattern.compile("\"id\":(\\d+)");
//        Path jsonPath = Paths.get(resultDir, "biomass.json");
////        SequenceHandler.fasta2Json(fastaPath, pidPat, tidPat, jsonPath);
//        // generate target-decoy library
//        String decoyPrefix = "REV__";
//        Path libraryWithDecoyPath = Paths.get(resultDir, "biomass-with-reversed-decoy.fasta");
////        DecoySeqFactory.genLibraryWithDecoy(fastaPath, decoyPrefix, DecoySeqFactory.Type.REVERSE, libraryWithDecoyPath);
//        // FDR
//        double psmFDR = 0.01;
//        double proteinFDR = 0.01;
//        // ignore shared peptides when PSM counting
//        boolean ignoreSharedPept = true;
//
//        for (String dat : datFiles) {
//            proteinPipeline(dat, libraryWithDecoyPath, decoyPrefix, jsonPath,
//                    psmFDR, proteinFDR, ignoreSharedPept, resultDir);
//        }
//    }
//
//    private void proteinPipeline(String datFile,
//                                 Path libraryWithDecoyPath,
//                                 String decoyPrefix,
//                                 Path protJsonPath,
//                                 double psmFDR,
//                                 double proteinFDR,
//                                 boolean ignoreSharedPept,
//                                 String resultDir) throws IOException, FileParsingException {
//
//        Path datPath = Paths.get(datFile);
//        String datName = datPath.getFileName().toString();
//        datName = datName.substring(0, datName.indexOf("."));
//        Path taxonDistPath = Paths.get(resultDir, "distribution2");
//        if (Files.notExists(taxonDistPath)) {
//            Files.createDirectory(taxonDistPath);
//        }
//
//        /** dat -> pepxml**/
//        Map<String, String> mascot2XMLParams = new HashMap<>();
//        mascot2XMLParams.put("-E", "trypsin");
//        Path pepxmlPath = Paths.get(resultDir, datName + ".pep.xml");
////        Mascot2XMLEnhancer.convertAutoDecoyMode(datPath, mascot2XMLParams, decoyPrefix, libraryWithDecoyPath, pepxmlPath);
//
//        /** pepxml -> PeptideProphet **/
//        Map<String, String> ppParams = new HashMap<>();
//        ppParams.put("-PPM", null);
//        Path ppPepxmlPath = Paths.get(resultDir, datName + ".xinteract.pep.xml");
////        PeptideProphetEnhancer.run(pepxmlPath, ppParams, ppPepxmlPath);
//
//        /** filter by peptide FDR **/
//        Path fppPepxmlPath = Paths.get(resultDir, psmFDR + "_" + datName + ".xinteract.pep.xml");
////        PeptideProphetEnhancer.filterByFDR(ppPepxmlPath, decoyPrefix, psmFDR, fppPepxmlPath);
//
//        /** build MPPeptide and MPProtein **/
//        Set<MPPeptide> mpPeptideSet = new HashSet<>();
//        Set<MPProtein> mpProteinSet = new HashSet<>();
//        PeptideProphetConverter.convert2PeptProt(ppPepxmlPath, decoyPrefix, ignoreSharedPept,
//                mpPeptideSet, mpProteinSet);
//        ProteinPostprocess.addTaxonInfo(mpProteinSet, protJsonPath);
//        // write peptide
//        PeptidePostprocess.writePeptide(mpPeptideSet, Paths.get(resultDir, datName + "_" + "peptides.txt"));
//
//        /** run fido **/
//        Path fidoResultPath = Paths.get(resultDir, psmFDR + "_" + datName + "_fido.txt");
//        Set<MPProtein> mpProtSetForFido = new HashSet<>(mpProteinSet.size());
//        Set<MPPeptide> mpPeptSetForFido = new HashSet<>(mpPeptideSet.size());
////        MascotPipeline.runFidoModel(mpPeptideSet, mpProteinSet, 2, fidoResultPath);
////        MascotPipeline.parseFidoResult(mpPeptideSet, mpProteinSet, proteinFDR,
////                ignoreSharedPept, fidoResultPath,
////                mpPeptSetForFido, mpProtSetForFido);
//        // add proteins with 3 unique peptides
//        MascotPipeline.parseFidoResult(mpPeptideSet, mpProteinSet, proteinFDR,
//                ignoreSharedPept, fidoResultPath,
//                mpPeptSetForFido, mpProtSetForFido);
////        Set<MPProtein> mpProtSetForFido = mpProteinSet;
////        Set<MPPeptide> mpPeptSetForFido = mpPeptideSet;
//
//        // remove decoy proteins
//        mpProtSetForFido = new HashSet<>(ProteinPostprocess.delDecoyProts(mpProtSetForFido));
//        ProteinPostprocess.updatePeptProt(mpPeptSetForFido, mpProtSetForFido, ignoreSharedPept);
//
//        // filter by peptide count
//        mpProtSetForFido = new HashSet<>(ProteinPostprocess.filterByUniqPeptide(mpProtSetForFido, 2));
//        ProteinPostprocess.updatePeptProt(mpPeptSetForFido, mpProtSetForFido, ignoreSharedPept);
//
//        // write protein list
//        ProteinPostprocess.writeProtein(mpProtSetForFido, Paths.get(resultDir, "protein-list.tsv"));
//        // write result based on protein
//        Map<Taxon, List<MPProtein>> taxon2Prots = ProteinPostprocess.groupByTaxon(mpProtSetForFido);
//        Path taxonDistOnProtPath = taxonDistPath.resolve(datName);
//        ProteinPostprocess.writeTaxonDistribution(taxon2Prots, taxonDistOnProtPath);
//
//        /** group **/
//        List<MPProteinGroup> mpPGList = ProteinGroupProcess.groupProteins(mpProtSetForFido);
//        Map<Integer, Taxon> tid2Taxon = mpProtSetForFido.parallelStream()
//                .filter(prot -> prot.getTaxon() != null)
//                .collect(Collectors.toMap(
//                        prot -> prot.getTaxon().getId(),
//                        prot -> prot.getTaxon(),
//                        (oldValue, value) -> value
//                ));
//        ProteinGroupPostprocess.calLCA(mpPGList, tid2Taxon);
//        ProteinGroupPostprocess.calPSM(mpPGList);
//
//        // write protein groups
//        Path pgResultPath = Paths.get(resultDir, "pg-result.tsv");
//        ProteinGroupPostprocess.writePGResult(mpPGList, pgResultPath);
//        // filter by LCA
//        List<MPProteinGroup> filteredMPPGList = ProteinGroupPostprocess
//                .filterByLCA(Taxon.Rank.GENUS, mpPGList, mpPeptSetForFido, true);
//
//        ProteinGroupPostprocess.calPSM(filteredMPPGList);
//        Map<Taxon, List<MPProteinGroup>> taxon2MPPGList = ProteinGroupPostprocess.groupByTaxon(filteredMPPGList);
//        ProteinGroupPostprocess.writeTaxonDistribution(taxon2MPPGList, taxonDistPath.resolve("pg_" + datName));
//    }
//
//    /**
//     * Run4&5 P, original library
//     * percolator -> Fido -> taxon distribution on protein level
//     *
//     * @throws IOException
//     * @throws FileParsingException
//     */
//    @Test
//    public void percolatorPipeline() throws IOException, DocumentException, FileParsingException {
//        // Run4_P
////        String[] datFiles = new String[]{
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002589.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002591.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002593.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/P_Run4_5/F002595.dat"
////        };
//
//        // Run1_U
//        String[] datFiles = new String[]{
//                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/Run1_U/F002651.dat",
//                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/Run1_U/F002652.dat",
//                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/Run1_U/F002653.dat",
//                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/Run1_U/F002654.dat"
//        };
////        String[] datFiles = new String[]{
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/Run1_U/F002651.dat"
////        };
//
//         // result dir
//        final Path resultDirPath = Paths.get("/home/huangjs/Documents/map/test/U_Run1/");
//        final Path distDirPath = resultDirPath.resolve("distribution");
//        if (Files.notExists(resultDirPath)) {
//            Files.createDirectories(resultDirPath);
//        }
//        if (Files.notExists(distDirPath)) {
//            Files.createDirectories(distDirPath);
//        }
//        // fasta -> json
//        Path fastaPath = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/fasta/biomass.fasta");
//        // FDR
//        final double psmFDR = 0.01;
//        final double proteinFDR = 0.01;
//        // ignore shared peptides when PSM counting
//        final boolean ignoreSharedPept = false;
//
//        /** constant **/
//        final Pattern pidPat = Pattern.compile(">(\\S+)");
//        final Pattern tidPat = Pattern.compile("\"id\":(\\d+)");
//        final Path jsonPath = resultDirPath.resolve("biomass.json");
//        SequenceHandler.fasta2Json(fastaPath, pidPat, tidPat, jsonPath);
//        // generate target-decoy library
//        final String decoyPrefix = "REV__";
//        final Path libraryWithDecoyPath = resultDirPath.resolve("biomass-with-reversed-decoy.fasta");
//        DecoySeqFactory.genLibraryWithDecoy(fastaPath, decoyPrefix, DecoySeqFactory.Type.REVERSE, libraryWithDecoyPath);
//
//        for (String dat : datFiles) {
//            percolatorPipeline(dat, pidPat, libraryWithDecoyPath, decoyPrefix, jsonPath,
//                    psmFDR, proteinFDR, ignoreSharedPept, resultDirPath, distDirPath);
//        }
//    }
//
//    private void percolatorPipeline(String datFile,
//                                    Pattern pidPat,
//                                    Path libraryWithDecoyPath,
//                                    String decoyPrefix,
//                                    Path protJsonPath,
//                                    double psmFDR,
//                                    double proteinFDR,
//                                    boolean ignoreSharedPept,
//                                    Path resultDirPath,
//                                    Path distDirPath) throws IOException, DocumentException, FileParsingException {
//        Path datPath = Paths.get(datFile);
//        String datName = datPath.getFileName().toString();
//        datName = datName.substring(0, datName.indexOf("."));
//
////        /** dat -> pepxml**/
////        Map<String, String> mascot2XMLParams = new HashMap<>();
////        mascot2XMLParams.put("-E", "trypsin");
////        Path pepxmlPath = resultDirPath.resolve(datName + ".pep.xml");
////        Mascot2XMLEnhancer.convertAutoDecoyMode(datPath, mascot2XMLParams, decoyPrefix, libraryWithDecoyPath, pepxmlPath);
////
////        /** pepxml -> PeptideProphet **/
////        Map<String, String> ppParams = new HashMap<>();
////        ppParams.put("-PPM", null);
////        Path ppPepxmlPath = resultDirPath.resolve(datName + ".xinteract.pep.xml");
////        PeptideProphetEnhancer.run(pepxmlPath, ppParams, ppPepxmlPath);
////
////        /** filter by peptide FDR **/
////        Path fppPepxmlPath = resultDirPath.resolve(psmFDR + "_" + datName + ".xinteract.pep.xml");
////        PeptideProphetEnhancer.filterByFDR(ppPepxmlPath, decoyPrefix, psmFDR, fppPepxmlPath);
////
////        /** build MPPeptide and MPProtein **/
////        Set<MPPeptide> ppPeptideSet = new HashSet<>();
////        Set<MPProtein> ppProteinSet = new HashSet<>();
////        PeptideProphetConverter.convert2PeptProt(ppPepxmlPath, decoyPrefix, ignoreSharedPept,
////                ppPeptideSet, ppProteinSet);
////        ProteinPostprocess.addTaxonInfo(ppProteinSet, protJsonPath);
//
//        /********************************************************************************/
//
//        /** dat -> feature **/
//        Path featuresPath = resultDirPath.resolve(datName + "_features.tsv");
//        Path newFeaturesPath = resultDirPath.resolve(datName + "_new_features.tsv");
//        MascotPercolator.generateFeaturesFile(datPath, featuresPath);
//        MascotPercolator.updateFeaturesFile(true, pidPat,
//                libraryWithDecoyPath, decoyPrefix, featuresPath, newFeaturesPath);
//
//        /** feature -> percolator xml **/
//        Path xmlResultPath = resultDirPath.resolve(datName + "_percolator.xml");
//        MascotPercolator.runPercolator(featuresPath, xmlResultPath);
//
//        /** build MPPeptide and MPProtein **/
//        Set<MPPeptide> percPeptideSet = new HashSet<>();
//        Set<MPProtein> percProteinSet = new HashSet<>();
//        MascotPercolator.convert2PeptProt(xmlResultPath, psmFDR, ignoreSharedPept, percPeptideSet, percProteinSet);
//        ProteinPostprocess.addTaxonInfo(percProteinSet, protJsonPath);
//
//        /** merge peptides **/
///*        // pp -> percolator
//        Set<MPPeptide> mpPeptideSet = new HashSet<>(percPeptideSet);
//        Set<MPProtein> mpProteinSet = new HashSet<>(percProteinSet);
//        ppPeptideSet.stream()
//                .filter(pept -> !percPeptideSet.contains(pept) && pept.isUniq())
//                .forEach(pept -> {
//               count.incrementAndGet();
//                    mpPeptideSet.add(pept);
//                    mpProteinSet.addAll(pept.getAssociatedProteinSet());
//                });*/
////        // percolator -> pp
////        Set<MPPeptide> mpPeptideSet = new HashSet<>(ppPeptideSet);
////        Set<MPProtein> mpProteinSet = new HashSet<>(ppProteinSet);
////        AtomicInteger count = new AtomicInteger();
////        percPeptideSet.stream()
////                .filter(pept -> !ppPeptideSet.contains(pept) && pept.isUniq())
////                .forEach(pept -> {
////                    count.incrementAndGet();
////                    mpPeptideSet.add(pept);
////                    mpProteinSet.addAll(pept.getAssociatedProteinSet());
////                });
//        // merge percolator ^ pp
////        Set<MPProtein> mpProteinSet = new HashSet<>();
////        AtomicInteger count = new AtomicInteger();
////        Set<MPPeptide> mpPeptideSet = ppPeptideSet.stream()
////                .filter(pept -> percPeptideSet.contains(pept))
////                .peek(i -> count.incrementAndGet())
////                .collect(Collectors.toSet());
////        ProteinPostprocess.updatePeptProtByPept(mpPeptideSet, mpProteinSet, ignoreSharedPept);
//
////        log.info("add {} peptide from percolator", count.get());
//        // write peptide
//        PeptidePostprocess.writePeptide(percPeptideSet, resultDirPath.resolve(psmFDR + "_" + datName + "_peptide.txt"));
//
//        /** run fido **/
//        Path fidoResultPath = resultDirPath.resolve(psmFDR + "_" + datName + "_fido.txt");
//        Set<MPProtein> mpProtSetForFido = new HashSet<>(percProteinSet.size());
//        Set<MPPeptide> mpPeptSetForFido = new HashSet<>(percPeptideSet.size());
//        MascotPipeline.runFidoModel(percPeptideSet, percProteinSet, 2, fidoResultPath);
//        MascotPipeline.parseFidoResult(percPeptideSet, percProteinSet, proteinFDR,
//                ignoreSharedPept, fidoResultPath,
//                mpPeptSetForFido, mpProtSetForFido);
//        // add proteins with 3 unique peptides
////        MascotPipeline.parseFidoResult(mpPeptideSet, mpProteinSet, proteinFDR, 3,
////                ignoreSharedPept, fidoResultPath,
////                mpPeptSetForFido, mpProtSetForFido);
//
//        // remove decoy proteins
//        mpProtSetForFido = new HashSet<>(ProteinPostprocess.delDecoyProts(mpProtSetForFido));
//        ProteinPostprocess.updatePeptProt(mpPeptSetForFido, mpProtSetForFido, ignoreSharedPept);
//
//        // filter by unique peptide count
//        mpProtSetForFido = new HashSet<>(ProteinPostprocess.filterByUniqPeptide(mpProtSetForFido, 2));
//        ProteinPostprocess.updatePeptProt(mpPeptSetForFido, mpProtSetForFido, ignoreSharedPept);
//
//        // write protein list
//        ProteinPostprocess.writeProtein(mpProtSetForFido, resultDirPath.resolve(datName + "_protein-list.tsv"));
//        // write result based on protein
//        Map<Taxon, List<MPProtein>> taxon2Prots = ProteinPostprocess.groupByTaxon(mpProtSetForFido);
//        Path taxonDistOnProtPath = distDirPath.resolve(datName);
//        ProteinPostprocess.writeTaxonDistribution(taxon2Prots, taxonDistOnProtPath);
//
//        /** group **/
//        List<MPProteinGroup> mpPGList = ProteinGroupProcess.groupProteins(mpProtSetForFido);
//        Map<Integer, Taxon> tid2Taxon = mpProtSetForFido.parallelStream()
//                .filter(prot -> prot.getTaxon() != null)
//                .collect(Collectors.toMap(
//                        prot -> prot.getTaxon().getId(),
//                        prot -> prot.getTaxon(),
//                        (oldValue, value) -> value
//                ));
//        ProteinGroupPostprocess.calLCA(mpPGList, tid2Taxon);
//        ProteinGroupPostprocess.calPSM(mpPGList);
//
//        // write protein groups
//        Path pgResultPath = resultDirPath.resolve(datName + "_pg-result.tsv");
//        ProteinGroupPostprocess.writePGResult(mpPGList, pgResultPath);
//        // filter by LCA
//        List<MPProteinGroup> filteredMPPGList = ProteinGroupPostprocess
//                .filterByLCA(Taxon.Rank.GENUS, mpPGList, mpPeptSetForFido, true);
//
//        ProteinGroupPostprocess.calPSM(filteredMPPGList);
//        Map<Taxon, List<MPProteinGroup>> taxon2MPPGList = ProteinGroupPostprocess.groupByTaxon(filteredMPPGList);
//        ProteinGroupPostprocess.writeTaxonDistribution(taxon2MPPGList, distDirPath.resolve("pg_" + datName));
//    }
//
//    @Test
//    public void tppPipeline() throws IOException, DocumentException, FileParsingException {
//        // Run1_U
////        String[] datFiles = new String[]{
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/Run1_U/F002651.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/Run1_U/F002652.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/Run1_U/F002653.dat",
////                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/Run1_U/F002654.dat"
////        };
//        String[] datFiles = new String[]{
//                "/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/mascot/Run1_U/F002651.dat"
//        };
//        // result dir
//        final Path resultDirPath = Paths.get("/home/huangjs/Documents/map/test/U_Run1/");
//        final Path distDirPath = resultDirPath.resolve("distribution");
//        if (Files.notExists(resultDirPath)) {
//            Files.createDirectories(resultDirPath);
//        }
//        if (Files.notExists(distDirPath)) {
//            Files.createDirectories(distDirPath);
//        }
//        // fasta -> json
//        Path fastaPath = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/PXD006118/fasta/biomass.fasta");
//        // FDR
//        final double psmFDR = 0.01;
//        final double proteinFDR = 0.01;
//        // ignore shared peptides when PSM counting
//        final boolean ignoreSharedPept = false;
//
//        /** constant **/
//        final Pattern pidPat = Pattern.compile(">(\\S+)");
//        final Pattern tidPat = Pattern.compile("\"id\":(\\d+)");
//        final Path jsonPath = resultDirPath.resolve("biomass.json");
//        SequenceHandler.fasta2Json(fastaPath, pidPat, tidPat, jsonPath);
//        // generate target-decoy library
//        final String decoyPrefix = "REV__";
//        final Path libraryWithDecoyPath = resultDirPath.resolve("biomass-with-reversed-decoy.fasta");
//        DecoySeqFactory.genLibraryWithDecoy(fastaPath, decoyPrefix, DecoySeqFactory.Type.REVERSE, libraryWithDecoyPath);
//
//        for (String dat : datFiles) {
//            tpp(dat, libraryWithDecoyPath, decoyPrefix, resultDirPath);
//        }
//    }
//
//    private void tpp(String datFile,
//                                    Path libraryWithDecoyPath,
//                                    String decoyPrefix,
//                                    Path resultDirPath) throws IOException, DocumentException, FileParsingException {
//        Path datPath = Paths.get(datFile);
//        String datName = datPath.getFileName().toString();
//        datName = datName.substring(0, datName.indexOf("."));
//
//        /** dat -> pepxml**/
//        Map<String, String> mascot2XMLParams = new HashMap<>();
//        mascot2XMLParams.put("-E", "trypsin");
//        Path pepxmlPath = resultDirPath.resolve(datName + ".pep.xml");
//        Mascot2XMLEnhancer.convertAutoDecoyMode(datPath, mascot2XMLParams, decoyPrefix, libraryWithDecoyPath, pepxmlPath);
//
//        /** pepxml -> PeptideProphet **/
//        Map<String, String> ppParams = new HashMap<>();
//        ppParams.put("-PPM", null);
//        Path ppPepxmlPath = resultDirPath.resolve(datName + ".xinteract.pep.xml");
//        PeptideProphetEnhancer.run(pepxmlPath, ppParams, ppPepxmlPath);
//    }
//
//    /**
//     * Oral person 1, 3, 5, 7
//     * PeptideProphet -> Fido -> taxon distribution on protein level
//     *
//     * @throws IOException
//     * @throws FileParsingException
//     */
//    @Test
//    public void oralPPAnalysis() throws IOException, DocumentException, FileParsingException {
//        String[] datFiles = new String[]{
//                "/home/huangjs/archive/java/metaproteomics/raw_data/oral#2/data/p1357/F002742.dat",
//                "/home/huangjs/archive/java/metaproteomics/raw_data/oral#2/data/p1357/F002743.dat",
//                "/home/huangjs/archive/java/metaproteomics/raw_data/oral#2/data/p1357/F002744.dat",
//                "/home/huangjs/archive/java/metaproteomics/raw_data/oral#2/data/p1357/F002745.dat"
//        };
//
//        // result dir
//        final Path resultDirPath = Paths.get("/home/huangjs/Documents/metaprot/oral");
//        final Path distDirPath = resultDirPath.resolve("distribution");
//        if (Files.notExists(resultDirPath)) {
//            Files.createDirectories(resultDirPath);
//        }
//        if (Files.notExists(distDirPath)) {
//            Files.createDirectories(distDirPath);
//        }
//        // fasta -> json
//        Path fastaPath = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/oral#2/data/library/formatted_HOMD_HUMAN.faa");
//        // FDR
//        final double psmFDR = 0.01;
//        final double proteinFDR = 0.01;
//        // ignore shared peptides when PSM counting
//        final boolean ignoreSharedPept = true;
//
//        /** constant **/
//        final Pattern pidPat = Pattern.compile(">(\\S+)");
//        final Pattern tidPat = Pattern.compile("\"tid\":(\\d+)");
//        final Path jsonPath = resultDirPath.resolve("oral.json");
//        SequenceHandler.fasta2Json(fastaPath, pidPat, tidPat, jsonPath);
//        // generate target-decoy library
//        final String decoyPrefix = "REV__";
//        final Path libraryWithDecoyPath = resultDirPath.resolve("library-with-reversed-decoy.fasta");
//        DecoySeqFactory.genLibraryWithDecoy(fastaPath, decoyPrefix, DecoySeqFactory.Type.REVERSE, libraryWithDecoyPath);
//
//        for (String dat : datFiles) {
//            oralPPAnalysis(dat, pidPat, libraryWithDecoyPath, decoyPrefix, jsonPath,
//                    psmFDR, proteinFDR, ignoreSharedPept, resultDirPath, distDirPath);
//        }
//    }
//
//    private void oralPPAnalysis(String datFile,
//                                Pattern pidPat,
//                                Path libraryWithDecoyPath,
//                                String decoyPrefix,
//                                Path protJsonPath,
//                                double psmFDR,
//                                double proteinFDR,
//                                boolean ignoreSharedPept,
//                                Path resultDirPath,
//                                Path distDirPath) throws IOException, DocumentException, FileParsingException {
//        Path datPath = Paths.get(datFile);
//        String datName = datPath.getFileName().toString();
//        datName = datName.substring(0, datName.indexOf("."));
//
//        /** dat -> feature **/
//        Path featuresPath = resultDirPath.resolve(datName + "_features.tsv");
//        Path newFeaturesPath = resultDirPath.resolve(datName + "_new_features.tsv");
//        MascotPercolator.generateFeaturesFile(datPath, featuresPath);
//        MascotPercolator.updateFeaturesFile(true, pidPat,
//                libraryWithDecoyPath, decoyPrefix, featuresPath, newFeaturesPath);
//
//        /** feature -> percolator xml **/
//        Path xmlResultPath = resultDirPath.resolve(datName + "_percolator.xml");
//        MascotPercolator.runPercolator(featuresPath, xmlResultPath);
//
//        /** build MPPeptide and MPProtein **/
//        Set<MPPeptide> mpPeptideSet = new HashSet<>();
//        Set<MPProtein> mpProteinSet = new HashSet<>();
//        MascotPercolator.convert2PeptProt(xmlResultPath, psmFDR, ignoreSharedPept, mpPeptideSet, mpProteinSet);
//        ProteinPostprocess.addTaxonInfo(mpProteinSet, protJsonPath);
//
//        // write peptide
//        PeptidePostprocess.writePeptide(mpPeptideSet, resultDirPath.resolve(psmFDR + "_" + datName + "_peptide.txt"));
//
//        /** run fido **/
//        Path fidoResultPath = resultDirPath.resolve(psmFDR + "_" + datName + "_fido.txt");
//        Set<MPProtein> mpProtSetForFido = new HashSet<>(mpProteinSet.size());
//        Set<MPPeptide> mpPeptSetForFido = new HashSet<>(mpPeptideSet.size());
//        MascotPipeline.runFidoModel(mpPeptideSet, mpProteinSet, 2, fidoResultPath);
//        MascotPipeline.parseFidoResult(mpPeptideSet, mpProteinSet, proteinFDR,
//                ignoreSharedPept, fidoResultPath,
//                mpPeptSetForFido, mpProtSetForFido);
//        // add proteins with 3 unique peptides
////        MascotPipeline.parseFidoResult(mpPeptideSet, mpProteinSet, proteinFDR, 3,
////                ignoreSharedPept, fidoResultPath,
////                mpPeptSetForFido, mpProtSetForFido);
//
//        // remove decoy proteins
//        mpProtSetForFido = new HashSet<>(ProteinPostprocess.delDecoyProts(mpProtSetForFido));
//        ProteinPostprocess.updatePeptProt(mpPeptSetForFido, mpProtSetForFido, ignoreSharedPept);
//
//        // filter by unique peptide count
//        mpProtSetForFido = new HashSet<>(ProteinPostprocess.filterByUniqPeptide(mpProtSetForFido, 2));
//        ProteinPostprocess.updatePeptProt(mpPeptSetForFido, mpProtSetForFido, ignoreSharedPept);
//
//        // write protein list
//        ProteinPostprocess.writeProtein(mpProtSetForFido, resultDirPath.resolve(datName + "_protein-list.tsv"));
//        // write result based on protein
//        Map<Taxon, List<MPProtein>> taxon2Prots = ProteinPostprocess.groupByTaxon(mpProtSetForFido);
//        Path taxonDistOnProtPath = distDirPath.resolve(datName);
//        ProteinPostprocess.writeTaxonDistribution(taxon2Prots, taxonDistOnProtPath);
//
//        /** group **/
//        List<MPProteinGroup> mpPGList = ProteinGroupProcess.groupProteins(mpProtSetForFido);
//        Map<Integer, Taxon> tid2Taxon = mpProtSetForFido.parallelStream()
//                .filter(prot -> prot.getTaxon() != null)
//                .collect(Collectors.toMap(
//                        prot -> prot.getTaxon().getId(),
//                        prot -> prot.getTaxon(),
//                        (oldValue, value) -> value
//                ));
//        ProteinGroupPostprocess.calLCA(mpPGList, tid2Taxon);
//        ProteinGroupPostprocess.calPSM(mpPGList);
//
//        // write protein groups
//        Path pgResultPath = resultDirPath.resolve(datName + "_pg-result.tsv");
//        ProteinGroupPostprocess.writePGResult(mpPGList, pgResultPath);
//        // filter by LCA
//        List<MPProteinGroup> filteredMPPGList = ProteinGroupPostprocess
//                .filterByLCA(Taxon.Rank.GENUS, mpPGList, mpPeptSetForFido, true);
//
//        ProteinGroupPostprocess.calPSM(filteredMPPGList);
//        Map<Taxon, List<MPProteinGroup>> taxon2MPPGList = ProteinGroupPostprocess.groupByTaxon(filteredMPPGList);
//        ProteinGroupPostprocess.writeTaxonDistribution(taxon2MPPGList, distDirPath.resolve("pg_" + datName));
//    }
//}