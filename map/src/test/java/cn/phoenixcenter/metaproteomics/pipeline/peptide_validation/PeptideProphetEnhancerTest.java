package cn.phoenixcenter.metaproteomics.pipeline.peptide_validation;

import cn.phoenixcenter.metaproteomics.pipeline.converter.psm.Mascot2XMLEnhancer;
import cn.phoenixcenter.metaproteomics.pipeline.utils.DecoySeqFactory;
import org.junit.Test;
import umich.ms.fileio.exceptions.FileParsingException;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class PeptideProphetEnhancerTest {

    @Test
    public void run() throws IOException {
        Path pepxmlPath = Paths.get("/home/huangjs/Documents/metaprot/mc-pp-pipeline/F001238.pep.xml");
        Map<String, String> ppParams = new HashMap<>();
        ppParams.put("-PPM", null);
        Path ppPepxmlPath = Paths.get("/home/huangjs/Documents/metaprot/mc-pp-pipeline/F001238.xinteract.pep.xml");
        PeptideProphetEnhancer.run(pepxmlPath, ppParams, ppPepxmlPath);
    }

    @Test
    public void filterByFDR() throws IOException, FileParsingException {
        Path  resultDirPath = Paths.get("/home/huangjs/Documents/metaprot/oral/");
        Path datPath=  Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/oral#2/data/p1357/F002742.dat");
        String datName = "F002742";
        String decoyPrefix = "REV__";

        Path fastaPath = Paths.get("/home/huangjs/archive/java/metaproteomics/raw_data/oral#2/data/library/formatted_HOMD_HUMAN.faa");
        Path libraryWithDecoyPath = resultDirPath.resolve("library-with-reversed-decoy.fasta");
        DecoySeqFactory.genLibraryWithDecoy(fastaPath, decoyPrefix, DecoySeqFactory.Type.REVERSE, libraryWithDecoyPath);

        /** dat -> pepxml**/
        Map<String, String> mascot2XMLParams = new HashMap<>();
        mascot2XMLParams.put("-E", "trypsin");
        Path pepxmlPath = resultDirPath.resolve(datName + ".pep.xml");
        Mascot2XMLEnhancer.convertAutoDecoyMode(datPath, mascot2XMLParams, decoyPrefix, libraryWithDecoyPath, pepxmlPath);

        /** pepxml -> PeptideProphet **/
        Map<String, String> ppParams = new HashMap<>();
        ppParams.put("-PPM", null);
        Path ppPepxmlPath = resultDirPath.resolve(datName + ".xinteract.pep.xml");
        PeptideProphetEnhancer.run(pepxmlPath, ppParams, ppPepxmlPath);

        /** filter by peptide FDR **/
        Path fppPepxmlPath = resultDirPath.resolve("0.01_" + datName + ".xinteract.pep.xml");
        PeptideProphetEnhancer.filterByFDR(ppPepxmlPath, decoyPrefix, 0.01, fppPepxmlPath);
    }
}