package cn.phoenixcenter.metaproteomics.pipeline.converter.peptide;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import org.junit.Test;
import umich.ms.fileio.exceptions.FileParsingException;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class PeptideProphetConverterTest {

    @Test
    public void convert2PeptProt() throws FileParsingException {
        Path pepxmlPath = Paths.get("/home/huangjs/Documents/metaprot/mc-pp-pipeline/P_Run4_5/original/0.01_F002589.xinteract.pep.xml");
        String decoyPrefix = "REV__";
        boolean ignoreSharedPept = true;
        Set<MPPeptide> mpPeptideSet = new HashSet<>();
        Set<MPProtein> mpProteinSet = new HashSet<>();
        PeptideProphetConverter.convert2PeptProt(pepxmlPath, decoyPrefix, ignoreSharedPept, mpPeptideSet, mpProteinSet);

        System.out.println("peptide count: " + mpPeptideSet.size());
        System.out.println("protein count: " + mpProteinSet.size());
    }

    @Test
    public void peptProtCount() throws IOException {
        Path path = Paths.get("/home/huangjs/Documents/metaprot/mc-pp-pipeline/P_Run4_5/original/0.01_F002589.xinteract.pep.xml");
        BufferedReader br = Files.newBufferedReader(path);
        String line;
        Pattern peptPat = Pattern.compile("peptide=\"(\\w+)\"");
        Pattern pidPat = Pattern.compile("protein=\"([\\w._-]+)\"");
        Set<String> peptSet = new HashSet<>();
        Set<String> protSet = new HashSet<>();
        while ((line = br.readLine()) != null) {
            if (line.startsWith("<search_hit")) {
                Matcher m = peptPat.matcher(line);
                if (m.find()) {
                    peptSet.add(m.group(1));
                } else {
                    throw new RuntimeException(line);
                }
                m = pidPat.matcher(line);
                if (m.find()) {
                    protSet.add(m.group(1));
                } else {
                    throw new RuntimeException(line);
                }
            } else if (line.startsWith("<alternative_protein")) {
                Matcher m = pidPat.matcher(line);
                if (m.find()) {
                    protSet.add(m.group(1));
                } else {
                    throw new RuntimeException(line);
                }
            }
        }
        System.out.println("peptide count: " + peptSet.size());
        System.out.println("protein count: " + protSet.size());
    }

    @Test
    public void peptProtCount2() throws IOException {
        Path path = Paths.get("/home/huangjs/Documents/metaprot/mascot-pipeline/F002589.xml");
        BufferedReader br = Files.newBufferedReader(path);
        String line;
        Pattern peptPat = Pattern.compile("<peptide p:peptide_id=\"(\\w+)\" p:decoy=\"(\\w+)\">");
        Pattern protPat = Pattern.compile("<protein_id>([\\w._\\-]+)</protein_id>");
        Pattern qvalPat = Pattern.compile("<q_value>([0-9e\\-+.]+)</q_value>");
        boolean isTarget = false;
        boolean start = false;
        double qval = 1.0;
        String peptide = null;
        Set<String> peptSet = new HashSet<>();
        Set<String> protSet = new HashSet<>();
        while ((line = br.readLine()) != null) {
            if (line.startsWith("    <peptide ")) {
                start = true;
                Matcher m = peptPat.matcher(line);
                if (m.find()) {
                    peptide = m.group(1);
                    isTarget = Boolean.valueOf(m.group(2));
                } else {
                    throw new RuntimeException(line);
                }
            } else if (line.startsWith("      <q_value>")) {
                Matcher m = qvalPat.matcher(line);
                if (m.find()) {
                    qval = Double.parseDouble(m.group(1));
                } else {
                    throw new RuntimeException(line);
                }
            } else if (start && qval <= 0.01 && line.startsWith("      <protein_id>")) {
                Matcher m = protPat.matcher(line);
                if (m.find()) {
                    peptSet.add(peptide);
                    protSet.add((isTarget ? "" : "REV__") + m.group(1));
                } else {
                    throw new RuntimeException(line);
                }
            }
        }
        System.out.println("peptide count: " + peptSet.size());
        System.out.println("protein count: " + protSet.size());
        Files.readAllLines(Paths.get("/home/huangjs/Documents/metaprot/mascot-pipeline/F002589.percolator.peptide"))
                .parallelStream()
                .filter(entry -> {
                    String[] tmp = entry.split("\t");
                    if(protSet.contains(tmp[0])){
                        return false;
                    }else{
                        return true;
                    }
                })
                .forEach(entry-> System.out.println(entry));

    }
}