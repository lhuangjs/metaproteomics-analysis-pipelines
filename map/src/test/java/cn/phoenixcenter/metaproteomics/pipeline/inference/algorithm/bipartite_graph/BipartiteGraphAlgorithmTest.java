package cn.phoenixcenter.metaproteomics.pipeline.inference.algorithm.bipartite_graph;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.pipeline.processor.ProteinGroupProcess;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BipartiteGraphAlgorithmTest {

    @Test
    public void run() {
        MPPeptide pept1 = new MPPeptide("pept1");
        MPPeptide pept2 = new MPPeptide("pept2");
        MPPeptide pept3 = new MPPeptide("pept3");
        Set<MPPeptide> mpPeptideSet = Stream.of(pept1, pept2, pept3).collect(Collectors.toSet());

        MPProtein prot1 = new MPProtein("prot1");
        prot1.setContainedPeptideSet(Stream.of(pept1, pept2).collect(Collectors.toSet()));
        MPProtein prot2 = new MPProtein("prot2");
        prot2.setContainedPeptideSet(Stream.of(pept1, pept3).collect(Collectors.toSet()));
        MPProtein prot3 = new MPProtein("prot3");
        prot3.setContainedPeptideSet(Stream.of(pept2, pept3).collect(Collectors.toSet()));
        MPProtein prot4 = new MPProtein("prot4");
        prot4.setContainedPeptideSet(Stream.of(pept3).collect(Collectors.toSet()));
        Set<MPProtein> mpProteinSet = Stream.of(prot1, prot2, prot3, prot4).collect(Collectors.toSet());
        // peptide associated proteins
        pept1.setAssociatedProteinSet(Stream.of(prot1, prot2).collect(Collectors.toSet()));
        pept2.setAssociatedProteinSet(Stream.of(prot1, prot3).collect(Collectors.toSet()));
        pept3.setAssociatedProteinSet(Stream.of(prot2, prot3, prot4).collect(Collectors.toSet()));

        Set<MPPeptide> mpPeptideWithTypeSet = new HashSet<>(3);
        Set<MPProtein> mpProteinWithTypeSet = new HashSet<>(4);
        ProteinGroupProcess.addParsimonyTypeInfo(mpPeptideSet, mpProteinSet);

        BipartiteGraphAlgorithm bg = new BipartiteGraphAlgorithm();
        Set<MPProtein> candidateSet = bg.run(mpProteinWithTypeSet);
        System.out.println(candidateSet.stream().map(MPProtein::getId).collect(Collectors.joining("; ")));
        System.out.println(candidateSet);
    }
}