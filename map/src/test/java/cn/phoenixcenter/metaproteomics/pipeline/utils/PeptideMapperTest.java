package cn.phoenixcenter.metaproteomics.pipeline.utils;

import cn.phoenixcenter.metaproteomics.pipeline.base.MPPeptide;
import cn.phoenixcenter.metaproteomics.pipeline.base.MPProtein;
import cn.phoenixcenter.metaproteomics.pipeline.converter.peptide.PeptideProphetConverter;
import org.junit.Test;
import umich.ms.fileio.exceptions.FileParsingException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PeptideMapperTest {
    @Test
    public void mapPept2Prots() throws InterruptedException, ExecutionException, IOException {
        String fastaFile = "/home/huangjs/Documents/metaprot/biomass/Formatted_Mock_Comm_RefDB_V3.fasta";
        Pattern pidPattern = Pattern.compile(">(\\S+)");
        DecoySeqFactory.Type decoyType = DecoySeqFactory.Type.REVERSE;
        String decoyPrefix = "REV__";
        String output = "/home/huangjs/Documents/metaprot/biomass/pept2pid.json";
        Set<String> peptideSet = Files.readAllLines(Paths.get("/home/huangjs/Documents/metaprot/homologous/pp-peptides.txt"))
                .stream()
                .collect(Collectors.toSet());
        Map<String, Set<String>> pept2Pids = PeptideMapper.mapPept2Prots(peptideSet, fastaFile, pidPattern, decoyType, decoyPrefix);
        Files.write(Paths.get("/home/huangjs/Documents/metaprot/homologous/my-pept2prot.txt"),
                pept2Pids.entrySet().stream()
                        .map(e -> e.getKey() + "\t" + e.getValue().stream()
                                .sorted()
                                .collect(Collectors.joining(";")))
                        .sorted()
                        .collect(Collectors.toList())
        );
    }

    @Test
    public void genPeptides() throws FileParsingException, IOException, ExecutionException, InterruptedException {
        Path ppPepxmlPath = Paths.get("/home/huangjs/Documents/metaprot/mc-pp-pipeline/F002513.pp.pep.xml");
        String decoyPrefx = "REV__";
        Set<MPPeptide> mpPeptideSet = new HashSet<>();
        Set<MPProtein> mpProteinSet = new HashSet<>();
        // convert pepxml to MPPeptide and MPProtein
        PeptideProphetConverter.convert2PeptProt(ppPepxmlPath, decoyPrefx, false, mpPeptideSet, mpProteinSet);
        Files.write(Paths.get("/home/huangjs/Documents/metaprot/homologous/pp-peptides.txt"),
                mpPeptideSet.stream()
                        .map(MPPeptide::getSequence)
                        .sorted()
                        .collect(Collectors.toList())
        );

        Map<String, Set<String>> ppPept2Pids = mpPeptideSet.stream()
                .collect(Collectors.toMap(
                        pept -> {
                            if (pept.getSequence().equals("SASLMTQGHDALAR")) {
                                System.out.println(pept.getAssociatedProteinSet());
                            }
                            return pept.getSequence();
                        },
                        pept -> pept.getAssociatedProteinSet().stream()
                                .flatMap(prot -> {
                                    if (prot.getAltProteinSet() == null) {
                                        return Stream.of(prot.getId());
                                    } else {
                                        List<String> pidList = new ArrayList<>(prot.getAltProteinSet());
                                        pidList.add(prot.getId());
                                        Collections.sort(pidList);
                                        return pidList.stream();
                                    }
                                })
                                .collect(Collectors.toSet())
                        )
                );
        /** my  mapping **/
        String fastaFile = "/home/huangjs/Documents/metaprot/biomass/Formatted_Mock_Comm_RefDB_V3.fasta";
        Pattern pidPattern = Pattern.compile(">(\\S+)");
        DecoySeqFactory.Type decoyType = DecoySeqFactory.Type.REVERSE;
        String decoyPrefix = "REV__";
        String output = "/home/huangjs/Documents/metaprot/homologous/diff.txt";
        Set<String> peptideSet = Files.readAllLines(Paths.get("/home/huangjs/Documents/metaprot/homologous/pp-peptides.txt"))
                .stream()
                .collect(Collectors.toSet());
        Map<String, Set<String>> myPept2Pids = PeptideMapper.mapPept2Prots(peptideSet, fastaFile, pidPattern, decoyType, decoyPrefix);
        Files.write(Paths.get(output),
                peptideSet.stream()
                        .map(pept -> ppPept2Pids.get(pept).size() != myPept2Pids.get(pept).size()
                                ? String.join("\t",
                                pept, ppPept2Pids.get(pept).stream().collect(Collectors.joining(";")),
                                myPept2Pids.get(pept).stream().collect(Collectors.joining(";")))
                                : null
                        )
                        .filter(line -> line != null)
                        .collect(Collectors.toList())
        );
    }
}

