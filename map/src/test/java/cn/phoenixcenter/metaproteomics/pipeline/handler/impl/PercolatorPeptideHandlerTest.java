package cn.phoenixcenter.metaproteomics.pipeline.handler.impl;

import org.junit.Test;

import static org.junit.Assert.*;

public class PercolatorPeptideHandlerTest {

    @Test
    public void getPeptideChargeStateDistribution() {
        PercolatorPeptideHandler percolatorPeptideHandler = new PercolatorPeptideHandler(
                "/home/huangjs/Documents/metaprot/protein-pipeline/demo/biomass-percolator/peptide.txt");
        System.out.println(percolatorPeptideHandler.getPeptideChargeStateDistribution(0.01));;
        System.out.println(percolatorPeptideHandler.getPeptideLengthDistribution(0.01));;
        System.out.println(percolatorPeptideHandler.getPeptideTypeDistribution(0.01));;
    }

    @Test
    public void distribution() {
        PercolatorPeptideHandler percolatorPeptideHandler = new PercolatorPeptideHandler(
                "/home/huangjs/Documents/metaprot/protein-pipeline/demo/biomass-percolator/percolator.target.peptides.txt");
        System.out.println(percolatorPeptideHandler.getPeptideChargeStateDistribution(0.01));;
        System.out.println(percolatorPeptideHandler.getPeptideLengthDistribution(0.01));;
        System.out.println(percolatorPeptideHandler.getPeptideTypeDistribution(0.01));;
    }
}