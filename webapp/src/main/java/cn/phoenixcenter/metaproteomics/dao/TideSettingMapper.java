package cn.phoenixcenter.metaproteomics.dao;

import cn.phoenixcenter.metaproteomics.domain.TideSetting;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TideSettingMapper {

    int insert(TideSetting t);

    int deleteByPK(Integer primaryKey);

    int updateByPK(TideSetting t);

    TideSetting selectByPK(Integer primaryKey);

    TideSetting selectByUserIdAndName(@Param("userId") Integer userId,
                                      @Param("paramSettingName") String paramSettingName);

    List<TideSetting> selectByUserId(Integer userId);
}
