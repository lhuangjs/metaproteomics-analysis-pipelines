package cn.phoenixcenter.metaproteomics.dao;

import cn.phoenixcenter.metaproteomics.domain.Task;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskMapper {

    Task getTaskByPK(String taskId);

    /**
     * Add task into db
     *
     * @param task
     * @return
     */
    void insertTask(Task task);

    /**
     * Update task status or completedTime
     *
     * @param task
     */
    void updateTaskStatus(Task task);

    /**
     * Obtain all tasks does not send email
     *
     * @return
     */
    List<Task> getNonNotifiedTasks();

    /**
     * Obtain limit tasks wait to run
     *
     * @param limit
     * @return
     */
    List<Task> getWaitingTasks(@Param("limit") int limit);

    List<Task> getTaskByUserId(@Param("userId") int userId);

    List<Task> getTaskByStatus(@Param("status") Task.Status status);

    List<Task> getNTaskByStatus(@Param("status") Task.Status status,
                                @Param("limit") int limit);
}
