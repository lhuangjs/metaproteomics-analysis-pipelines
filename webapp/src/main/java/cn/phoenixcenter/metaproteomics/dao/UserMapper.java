package cn.phoenixcenter.metaproteomics.dao;

import cn.phoenixcenter.metaproteomics.domain.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {

    int insert(User t);

    int deleteByPK(Integer primaryKey);

    int updateByPK(User t);

    User selectByPK(Integer primaryKey);

    int checkEmail(@Param("email") String email);

    int checkUser(@Param("email") String email, @Param("password") String password);

    User getUserByEmailPassword(@Param("email") String email, @Param("password") String password);

    User getUserByEmail(@Param("email") String email);

    int updatePasswordByPK(User user);
}
