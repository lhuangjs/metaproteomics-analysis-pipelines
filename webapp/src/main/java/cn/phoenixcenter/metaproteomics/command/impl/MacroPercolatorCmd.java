package cn.phoenixcenter.metaproteomics.command.impl;

import cn.phoenixcenter.metaproteomics.base.ICommand;
import cn.phoenixcenter.metaproteomics.pipeline.handler.IPeptideHandler;
import cn.phoenixcenter.metaproteomics.pipeline.handler.impl.PercolatorPeptideHandler;
import cn.phoenixcenter.metaproteomics.pipeline.peptide_validation.PercolatorCmd;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.nio.file.Paths;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class MacroPercolatorCmd implements ICommand {

    @NonNull
    private PercolatorCmd percolatorCmd;

    @NonNull
    private String percolatorPeptideOutFile;

    @NonNull
    private String peptideReportOutFile;

    @Override
    public String getDesc() {
        return "percolator";
    }

    @Override
    public void execute() throws Exception {
        percolatorCmd.execute();
        IPeptideHandler peptideHandler = new PercolatorPeptideHandler(percolatorPeptideOutFile);
        peptideHandler.writePeptide(Paths.get(peptideReportOutFile));
    }
}
