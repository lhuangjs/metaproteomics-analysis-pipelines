package cn.phoenixcenter.metaproteomics.command.impl;

import cn.phoenixcenter.metaproteomics.base.ICommand;
import cn.phoenixcenter.metaproteomics.dao.TideSettingMapper;
import cn.phoenixcenter.metaproteomics.domain.TideSetting;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.TideIndexCmd;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class MacroTideIndexCmd implements ICommand {

    private static final long serialVersionUID = 1L;
    
    @NonNull
    private TideIndexCmd tideIndexCmd;

    @NonNull
    private TideSetting tideSetting;

    private TideSettingMapper tideSettingMapper;

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.writeObject(tideIndexCmd);
        oos.writeObject(tideSetting);
    }

    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
        this.tideIndexCmd = (TideIndexCmd) ois.readObject();
        this.tideSetting = (TideSetting) ois.readObject();
    }

    @Override
    public String getDesc() {
        return tideIndexCmd.getDesc();
    }

    @Override
    public void execute() throws Exception {
        // execute tide-index
        tideIndexCmd.execute();
        // enable library
        tideSettingMapper.insert(tideSetting);
    }
}
