package cn.phoenixcenter.metaproteomics.command.impl;

import cn.phoenixcenter.metaproteomics.base.ICommand;
import cn.phoenixcenter.metaproteomics.nr.TaxAnalysis;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.nio.file.Paths;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PeptideAnalysisCmd implements ICommand {

    private boolean equateIL;

    private String experimentFile;

    private String lcaFile;

    private String goAnnoFile;

    private String treeFile;

    private String quantFile;


    @Override
    public String getDesc() {
        return "peptide-centric pipeline";
    }

    @Override
    public void execute() throws Exception {
        TaxAnalysis.run(equateIL, Paths.get(experimentFile), Paths.get(lcaFile), Paths.get(goAnnoFile),
                Paths.get(treeFile), Paths.get(quantFile));
    }
}
