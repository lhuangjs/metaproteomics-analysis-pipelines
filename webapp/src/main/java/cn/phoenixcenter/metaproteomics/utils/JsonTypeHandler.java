package cn.phoenixcenter.metaproteomics.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JsonTypeHandler<T> implements TypeHandler<T> {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private Class<T> clazz;

    public JsonTypeHandler(@NonNull Class<T> clazz) {
        this.clazz = clazz;
    }

    /**
     * Java object => Jdbc type
     *
     * @param preparedStatement
     * @param i
     * @param o
     * @param jdbcType
     * @throws SQLException
     */
    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, T o, JdbcType jdbcType) throws SQLException {
        try {
            preparedStatement.setString(i, objectMapper.writeValueAsString(o));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public T getResult(ResultSet resultSet, String s) throws SQLException {
        try {
            return objectMapper.readValue(resultSet.getString(s), clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public T getResult(ResultSet resultSet, int i) throws SQLException {
        try {
            return objectMapper.readValue(resultSet.getString(i), clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public T getResult(CallableStatement callableStatement, int i) throws SQLException {
        try {
            return objectMapper.readValue(callableStatement.getString(i), clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
