package cn.phoenixcenter.metaproteomics.utils;

import cn.phoenixcenter.metaproteomics.common.exception.CheckException;

public class CheckUtil {
    public static void check(boolean Condition, String msg) {
        if (!Condition) {
            fail(msg);
        }
    }

    public static void notNull(Object obj, String msg) {
        if (obj == null) {
            fail(msg);
        }
    }

    public static <T> void equals(T t1, T t2, String msg) {
        if (t1 == null || !t1.equals(t2)) {
            fail(msg);
        }
    }

    private static void fail(String msg) {
        throw new CheckException(msg);
    }
}
