package cn.phoenixcenter.metaproteomics.utils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileDownloadUtil {
    public static void download(Path filePath, String fileName, HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/x-msdownload");
        response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
        byte[] buff = new byte[2048];
        InputStream is = Files.newInputStream(filePath);
        OutputStream os = response.getOutputStream();
        int len;
        while ((len = is.read(buff)) != -1) {
            os.write(buff, 0, len);
        }
        is.close();
        os.close();
    }
}
