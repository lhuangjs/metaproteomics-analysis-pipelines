package cn.phoenixcenter.metaproteomics.controller.user;

import cn.phoenixcenter.metaproteomics.common.Cons;
import cn.phoenixcenter.metaproteomics.common.ResultBean;
import cn.phoenixcenter.metaproteomics.domain.User;
import cn.phoenixcenter.metaproteomics.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @PostMapping("/email")
    public ResultBean<String> existsEmail(@RequestParam("email") String email) {
        return userService.checkEmail(email)
                ? ResultBean.error("Email already exists")
                : ResultBean.success();
    }

    @PostMapping("/sign_up")
    public ResultBean<String> signUp(@RequestParam("email") String email,
                                     @RequestParam("password") String password,
                                     HttpServletRequest request) {
        String userUrl = request.getRequestURL().toString();
        userUrl = userUrl.substring(0, userUrl.lastIndexOf("/sign_up"));
        userService.signUp(userUrl, email, password);
        return ResultBean.success();
    }

    @GetMapping("/activate")
    public ResultBean<String> activateUser(@RequestParam("email") String email,
                                           @RequestParam("code") String code) {
        return userService.activateUser(email, code)
                ? ResultBean.success()
                : new ResultBean<>(ResultBean.CHECK_EXCEPTION,
                "Invalid activation link, please register again");
    }

    @PostMapping(value ="/sign_in")
    public ResultBean<String> signIn(User user) {
        String token = userService.signIn(user);
        Map<String, String> tokenResult = new HashMap<>(1);
        tokenResult.put(Cons.HEADER_TOKEN_KEY, token);
        return ResultBean.success(tokenResult);
    }

    @PostMapping("/sign_out")
    public void signOut(HttpServletRequest request) {
        String token = request.getHeader(Cons.HEADER_TOKEN_KEY);
        userService.signOut(token);
    }

    @PostMapping("/password/apply")
    public ResultBean applyForResetPassword(@RequestParam("email") String email,
                                            HttpServletRequest request) {
        if (userService.checkEmail(email)) {
            String userUrl = request.getRequestURL().toString();
            userService.applyForResetPassword(userUrl, email);
            return ResultBean.success("We have sent you a password reset email, please check and follow the instructions");
        } else {
            return ResultBean.error("Email does not exist");
        }
    }

    @PostMapping("/password/reset")
    public ResultBean resetPassword(User user) {
        userService.confirmResetPassword(user);
        return ResultBean.success();
    }
}
