package cn.phoenixcenter.metaproteomics.controller.protein;

import cn.phoenixcenter.metaproteomics.common.ResultBean;
import cn.phoenixcenter.metaproteomics.common.annotation.Authorization;
import cn.phoenixcenter.metaproteomics.common.annotation.UserInfo;
import cn.phoenixcenter.metaproteomics.common.exception.ReadFileException;
import cn.phoenixcenter.metaproteomics.domain.TideSetting;
import cn.phoenixcenter.metaproteomics.domain.User;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.model.TideParam;
import cn.phoenixcenter.metaproteomics.service.IProteinCentricPipelineService;
import cn.phoenixcenter.metaproteomics.vo.PeptideInProtVO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/prot")
@Authorization
public class ProteinCentricPipelineController {

    @Autowired
    private IProteinCentricPipelineService proteinCentricPipelineService;

    @Autowired
    private ObjectMapper objectMapper;

    @PostMapping("/param_setting_name")
    public ResultBean checkParamSettingName(@RequestParam("paramSettingName") String paramSettingName,
                                            @UserInfo User user) {
        return proteinCentricPipelineService.checkParamSettingName(paramSettingName, user.getId())
                ? ResultBean.error("Parameter setting name exists")
                : ResultBean.success();
    }

    @PostMapping("/param_editor")
    public ResultBean paramEditor(@RequestParam("paramSettingName") String paramSettingName,
                                  @RequestParam("fastaFile") String fastaFile,
                                  @RequestParam("libraryName") String libraryName,
                                  @RequestParam("tideParam") String tideParamStr,
                                  @UserInfo User user,
                                  HttpServletRequest request) {
        try {
            TideParam tideParam = objectMapper.readValue(tideParamStr, TideParam.class);
            StringBuffer url = request.getRequestURL();
            proteinCentricPipelineService.prepareTide(url.substring(0, url.indexOf("/tide/param_editor")),
                    user, paramSettingName, fastaFile, libraryName, tideParam);
            return ResultBean.success();
        } catch (IOException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/params")
    public ResultBean getParams(@UserInfo User user) {
        List<TideSetting> paramList = proteinCentricPipelineService.getParams(user.getId());
        return ResultBean.success(paramList);
    }

    @GetMapping("/param_names")
    public ResultBean getParamNames(@UserInfo User user) {
        List<String> paramNameList = proteinCentricPipelineService.getParamNames(user.getId());
        return ResultBean.success(paramNameList);
    }

    @Authorization
    @PostMapping("/task_editor")
    public ResultBean taskEditor(@RequestBody String requestBody, @UserInfo User user) {
        try {
            JsonNode node = objectMapper.readTree(requestBody);
            proteinCentricPipelineService.taskEditor(node, user);
            return ResultBean.success();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }

    @Authorization
    @GetMapping("/peptide_report/{taskId}")
    public ResultBean peptideReport(@PathVariable("taskId") String taskId, @UserInfo User user) {
        try {
            List<PeptideInProtVO> peptideList = proteinCentricPipelineService.peptideReport(taskId, user);
            return ResultBean.success(peptideList);
        } catch (IOException e) {
            throw new ReadFileException(e);
        }
    }
}
