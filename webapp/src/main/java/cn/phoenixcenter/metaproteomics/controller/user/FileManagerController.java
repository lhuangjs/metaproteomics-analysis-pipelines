package cn.phoenixcenter.metaproteomics.controller.user;

import cn.phoenixcenter.metaproteomics.common.ResultBean;
import cn.phoenixcenter.metaproteomics.common.annotation.Authorization;
import cn.phoenixcenter.metaproteomics.common.annotation.UserInfo;
import cn.phoenixcenter.metaproteomics.common.exception.CheckException;
import cn.phoenixcenter.metaproteomics.domain.User;
import cn.phoenixcenter.metaproteomics.service.IFileManagerService;
import cn.phoenixcenter.metaproteomics.vo.DirectoryNodeVO;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

//@RequestMapping("/user/files")
//@RestController
//@Log4j2
//public class FileManagerController {
//
//    @RequestMapping(value = "/upload", method = RequestMethod.POST)
//    @ResponseBody
//    public ResultBean uploadFiles(@RequestParam("userId") String userId,
//                                  @RequestParam("files") MultipartFile[] files) {
//        System.out.println(userId);
//        for (MultipartFile file : files) {
//            if (!file.isEmpty()) {
//                try {
//                    log.debug("{} - {} MB", file.getOriginalFilename(), file.getSize() / 1024.0 / 1024);
//                    file.transferTo(new File("/home/huangjs/Documents/mpa/" + file.getOriginalFilename()));
//                } catch (IOException e) {
//                    return ResultBean.error(e.toString());
//                }
//            }
//        }
//        return ResultBean.success();
//    }
//}

@RequestMapping("/file")
@RestController
@Log4j2
public class FileManagerController {

    @Autowired
    private IFileManagerService fileManagerService;

    @Autowired
    private ObjectMapper objectMapper;

    @Authorization
    @PostMapping("/directory_tree")
    public ResultBean getDirStructure(@UserInfo User user) {
        return ResultBean.success(fileManagerService.getDirStructure(user.getWorkspacePath(), null));
    }

    @Authorization
    @PostMapping("/new_folder")
    public ResultBean newFolder(@RequestParam("activeNode") String activeNodeJson,
                                @RequestParam("newFolderName") String newFolderName,
                                @UserInfo User user) {
        try {
            DirectoryNodeVO activeNode = objectMapper.readValue(activeNodeJson, DirectoryNodeVO.class);
            return ResultBean.success(fileManagerService.createFolder(activeNode, newFolderName, user));
        } catch (IOException e) {
            throw new CheckException("active node format error");
        }
    }


    @Authorization
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResultBean uploadFile(@RequestParam("file") MultipartFile file,
                                 @RequestParam("activeNode") String activeNodeJson,
                                 @UserInfo User user) {
        try {
            DirectoryNodeVO activeNode = objectMapper.readValue(activeNodeJson, DirectoryNodeVO.class);
            return ResultBean.success(fileManagerService.upload(file, activeNode, user));
        } catch (IOException e) {
            throw new CheckException("active node format error");
        }
    }
}
