package cn.phoenixcenter.metaproteomics.controller.peptide;

import cn.phoenixcenter.metaproteomics.common.ResultBean;
import cn.phoenixcenter.metaproteomics.common.exception.CreateFolderException;
import cn.phoenixcenter.metaproteomics.common.exception.ReadFileException;
import cn.phoenixcenter.metaproteomics.dto.TaxResultDTO;
import cn.phoenixcenter.metaproteomics.service.IPeptideCentricPipelineService;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import cn.phoenixcenter.metaproteomics.utils.FileDownloadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Path;

@RestController
@RequestMapping("/pept")
public class PeptideCentricPipelineController {

    @Value("${pept.data.dir}")
    private String peptDataDir;

    @Value("${pept.experiment.filename}")
    private String experimentFilename;

    @Autowired
    private IPeptideCentricPipelineService peptideCentricPipeline;

    @PostMapping("/input")
    public ResultBean submitInput(@RequestParam("email") String email,
                                  @RequestParam("equateIL") boolean equateIL,
                                  @RequestParam("file") MultipartFile file,
                                  @RequestParam("resultUrl") String resultUrl) {
        try {
            String url = peptideCentricPipeline.submitTask(file, equateIL, email,
                    resultUrl);
            return ResultBean.success(url);
        } catch (IOException e) {
            throw new CreateFolderException(e);
        }
    }

    @GetMapping("/result/{taskId}")
    public ResultBean getResultData(@PathVariable("taskId") String taskId) {
        TaxResultDTO taxResultDTO = peptideCentricPipeline.getResultData(taskId);
        return ResultBean.success(taxResultDTO);
    }

    @PostMapping("/pid2prot")
    public void pid2Prot(@RequestParam("taskId") String taskId,
                         @RequestParam("rank") String rankStr,
                         @RequestParam("minUniqPeptCount") int minUniqPpetCount,
                         HttpServletResponse response) {
        try {
            Taxon.Rank rank = Taxon.Rank.stringToRank(rankStr);
            Path fastaPath = peptideCentricPipeline.pid2Prot(taskId, rank, minUniqPpetCount);
            FileDownloadUtil.download(fastaPath, "proteins.fasta", response);
        } catch (IOException e) {
            throw new ReadFileException(e);
        }
    }

    @GetMapping("/result/{taskId}/lca")
    public void getLCAResult(@PathVariable("taskId") String taskId, HttpServletResponse response) {
        Path lcaPath = peptideCentricPipeline.getLCAResult(taskId);
        try {
            FileDownloadUtil.download(lcaPath, "LCA.tsv", response);
        } catch (IOException e) {
            throw new ReadFileException(e);
        }
    }
}
