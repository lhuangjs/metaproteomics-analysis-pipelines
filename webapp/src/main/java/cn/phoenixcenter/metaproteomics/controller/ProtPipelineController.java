package cn.phoenixcenter.metaproteomics.controller;

import cn.phoenixcenter.metaproteomics.common.ResultBean;
import cn.phoenixcenter.metaproteomics.common.annotation.Authorization;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/protein")
public class ProtPipelineController {

    @PostMapping("/input")
    @Authorization
    public ResultBean runPipeline() {
        return ResultBean.success();
    }
}
