package cn.phoenixcenter.metaproteomics.controller;

import cn.phoenixcenter.metaproteomics.common.ResultBean;
import cn.phoenixcenter.metaproteomics.common.annotation.Authorization;
import cn.phoenixcenter.metaproteomics.common.annotation.UserInfo;
import cn.phoenixcenter.metaproteomics.domain.User;
import cn.phoenixcenter.metaproteomics.service.ITaskManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Authorization
@RestController
@RequestMapping("/task")
public class TaskManagerController {

    @Autowired
    private ITaskManagerService taskManagerService;

    @GetMapping("/all")
    public ResultBean getAllTasks(@UserInfo User user){
        return ResultBean.success(taskManagerService.getTaskListByUserId(user.getId()));
    }
}
