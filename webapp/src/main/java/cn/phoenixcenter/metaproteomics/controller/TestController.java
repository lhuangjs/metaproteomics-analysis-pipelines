package cn.phoenixcenter.metaproteomics.controller;

import cn.phoenixcenter.metaproteomics.domain.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class TestController {


    @PostMapping("/test")
    public void testPost(@RequestBody User user) {
        System.out.println(user);
    }

    @GetMapping("/test")
    public void testGet(HttpServletRequest request) {
        System.out.println(request);
    }
}
