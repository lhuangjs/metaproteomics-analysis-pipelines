package cn.phoenixcenter.metaproteomics.config;

import cn.phoenixcenter.metaproteomics.common.ResultBean;
import cn.phoenixcenter.metaproteomics.common.exception.AuthorizationException;
import cn.phoenixcenter.metaproteomics.common.exception.CheckException;
import cn.phoenixcenter.metaproteomics.common.exception.MailSendException;
import cn.phoenixcenter.metaproteomics.common.exception.UnLoginException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class ControllerExceptionAspect {

    @Pointcut("execution(public cn.phoenixcenter.metaproteomics.common.ResultBean *(..))")
    public void controllerExceptionPointcut() {
    }

    @Around("controllerExceptionPointcut()")
    public Object handleControllerMethod(ProceedingJoinPoint pjp) {
        ResultBean<?> result;
        try {
            result = (ResultBean<?>) pjp.proceed();
        } catch (Throwable e) {
            result = handleException(pjp, e);
        }
        return result;
    }

    private ResultBean<?> handleException(ProceedingJoinPoint pjp, Throwable e) {
        ResultBean<?> result = new ResultBean<>();
        if (e instanceof CheckException) {
            result.setCode(ResultBean.CHECK_EXCEPTION);
            result.setMessage(e.getLocalizedMessage());
        } else if (e instanceof MailSendException) {
            result.setMessage(e.getLocalizedMessage());
            result.setCode(ResultBean.EMAIL_SEND_EXCEPTION);
        } else if (e instanceof UnLoginException) {
            result.setMessage(e.getLocalizedMessage());
            result.setCode(ResultBean.UN_LOGIN);
        } else if (e instanceof AuthorizationException) {
            result.setMessage(e.getLocalizedMessage());
            result.setCode(ResultBean.AUTHORIZATION_ERROR);
        } else {
            result.setCode(ResultBean.UNKNOWN_EXCEPTION);
            result.setMessage(e.toString());
        }
        return result;
    }
}