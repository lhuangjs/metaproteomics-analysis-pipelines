package cn.phoenixcenter.metaproteomics.config.handler;

import cn.phoenixcenter.metaproteomics.common.Cons;
import cn.phoenixcenter.metaproteomics.common.ResultBean;
import cn.phoenixcenter.metaproteomics.common.annotation.Authorization;
import cn.phoenixcenter.metaproteomics.domain.User;
import cn.phoenixcenter.metaproteomics.service.ITokenManageService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

public class AuthorizationInterceptor implements HandlerInterceptor {

    @Autowired
    private ITokenManageService tokenManageService;

    private ObjectMapper objectMapper;

    public AuthorizationInterceptor() {
        this.objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    /**
     * Intercept requests to judge whether the request has permission
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        // if there is a @Authorization
        if (method.getDeclaringClass().isAnnotationPresent(Authorization.class)
                || method.isAnnotationPresent(Authorization.class)) {
            String token = request.getHeader(Cons.HEADER_TOKEN_KEY);
            User user = tokenManageService.checkToken(token);
            if (user != null) {
                request.setAttribute(Cons.CURRENT_USER_KEY, user);
                return true;
            }
            response.getWriter().write(
                    objectMapper.writeValueAsString(new ResultBean(ResultBean.UN_LOGIN,
                            "You must sign in first"))
            );
            return false;
        }
        return true;
    }
}
