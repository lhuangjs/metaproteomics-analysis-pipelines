package cn.phoenixcenter.metaproteomics.config.handler;

import cn.phoenixcenter.metaproteomics.common.Cons;
import cn.phoenixcenter.metaproteomics.common.annotation.UserInfo;
import cn.phoenixcenter.metaproteomics.domain.User;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class UserInfoMethodArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        // param type is User and with @UserInfo annotation
        if (parameter.getParameterType().isAssignableFrom(User.class)
                && parameter.hasParameterAnnotation(UserInfo.class)) {
            return true;
        }
        return false;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {
        // get user info from reques
        User user = (User) webRequest.getAttribute(Cons.CURRENT_USER_KEY, RequestAttributes.SCOPE_REQUEST);
        return user;
    }
}
