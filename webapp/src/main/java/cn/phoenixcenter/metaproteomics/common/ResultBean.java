package cn.phoenixcenter.metaproteomics.common;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ResultBean<D> {

    public static final int SUCCESS = 0;

    public static final int FAIL = 1;

    // user not logged in
    public static final int UN_LOGIN = 2;

    // check exception
    public static final int CHECK_EXCEPTION = 3;

    public static final int EMAIL_SEND_EXCEPTION = 4;

    // the token is invalid or expired
    public static final int AUTHORIZATION_ERROR = 5;

    public static final int UNKNOWN_EXCEPTION = -1;

    private int code;

    private String message;

    private D data;

    public ResultBean(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ResultBean(int code, String message, D data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static ResultBean success() {
        return new ResultBean(SUCCESS, "success");
    }

    public static <D> ResultBean success(D data) {
        return new ResultBean(SUCCESS, "success", data);
    }

    public static ResultBean error(String message) {
        return new ResultBean<String>(FAIL, message);
    }
}
