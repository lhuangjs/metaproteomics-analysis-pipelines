package cn.phoenixcenter.metaproteomics.common.exception;

public class WriteFileException extends RuntimeException {
    public WriteFileException() {
    }

    public WriteFileException(String message) {
        super(message);
    }

    public WriteFileException(Throwable cause) {
        super(cause);
    }

    public WriteFileException(String message, Throwable cause) {
        super(message, cause);
    }

}
