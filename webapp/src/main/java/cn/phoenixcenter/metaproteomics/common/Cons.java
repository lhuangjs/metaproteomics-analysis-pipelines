package cn.phoenixcenter.metaproteomics.common;

public class Cons {
    // the key to save token in request header
    public static final String HEADER_TOKEN_KEY = "token";

    public static final String CURRENT_USER_KEY = "user";

    public static final String PERCOLATOR_PEPTIDE_OUT_FILENAME = "percolator.target.peptides.txt";

    public static final String PEPTIDE_REPORT = "peptide.report.txt";
}
