package cn.phoenixcenter.metaproteomics.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The annotation will be used on controller methods to check whether
 * a user signed in. If a user did not sign in, it will return "UnLoginException".
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface UserInfo {
}
