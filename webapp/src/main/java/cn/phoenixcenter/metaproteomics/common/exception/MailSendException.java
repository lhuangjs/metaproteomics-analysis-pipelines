package cn.phoenixcenter.metaproteomics.common.exception;

public class MailSendException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public MailSendException(Throwable cause) {
        super(cause);
    }
}
