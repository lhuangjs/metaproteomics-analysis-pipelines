package cn.phoenixcenter.metaproteomics.common.exception;

public class UnLoginException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public UnLoginException() {
        super("You must sign in first");
    }
}
