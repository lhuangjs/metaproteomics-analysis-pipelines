package cn.phoenixcenter.metaproteomics.common.exception;

public class CreateUserException extends RuntimeException {
    public CreateUserException() {
    }

    public CreateUserException(String message) {
        super(message);
    }
}
