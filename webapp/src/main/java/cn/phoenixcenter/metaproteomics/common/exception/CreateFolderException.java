package cn.phoenixcenter.metaproteomics.common.exception;

public class CreateFolderException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public CreateFolderException(Throwable cause) {
        super(cause);
    }

    public CreateFolderException(String message) {
        super(message);
    }
}
