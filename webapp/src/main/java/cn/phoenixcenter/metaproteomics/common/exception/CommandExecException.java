package cn.phoenixcenter.metaproteomics.common.exception;

public class CommandExecException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public CommandExecException(Throwable cause) {
        super(cause);
    }
}
