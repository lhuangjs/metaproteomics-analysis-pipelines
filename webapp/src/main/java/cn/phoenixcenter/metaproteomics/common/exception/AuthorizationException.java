package cn.phoenixcenter.metaproteomics.common.exception;

public class AuthorizationException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public AuthorizationException() {
    }

    public AuthorizationException(String message) {
        super(message);
    }
}
