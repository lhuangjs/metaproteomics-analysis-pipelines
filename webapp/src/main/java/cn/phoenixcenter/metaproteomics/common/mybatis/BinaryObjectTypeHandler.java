package cn.phoenixcenter.metaproteomics.common.mybatis;

import cn.phoenixcenter.metaproteomics.base.ICommand;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.io.*;
import java.sql.*;

public class BinaryObjectTypeHandler implements TypeHandler<ICommand> {

    /**
     * convert java object to jdbc type at column i
     *
     * @param preparedStatement
     * @param i
     * @param command
     * @param jdbcType
     * @throws SQLException
     */
    @Override
    public void setParameter(PreparedStatement preparedStatement,
                             int i,
                             ICommand command,
                             JdbcType jdbcType) throws SQLException {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(baos);
            out.writeObject(command);
            out.close();
            baos.close();
            InputStream is = new ByteArrayInputStream(baos.toByteArray());
            preparedStatement.setBlob(i, is, baos.size());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ICommand getResult(ResultSet resultSet, String s) throws SQLException {
        try {
            return transferType(resultSet.getBlob(s));
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ICommand getResult(ResultSet resultSet, int i) throws SQLException {
        try {
            return transferType(resultSet.getBlob(i));
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ICommand getResult(CallableStatement callableStatement, int i) throws SQLException {
        try {
            return transferType(callableStatement.getBlob(i));
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private ICommand transferType(Blob blob) throws SQLException, IOException, ClassNotFoundException {
        ObjectInput in = new ObjectInputStream(blob.getBinaryStream());
        return ICommand.toObj(in);
    }
}
