package cn.phoenixcenter.metaproteomics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MAPApplication {
    public static void main(String[] args) {
        SpringApplication.run(MAPApplication.class);
    }
}
