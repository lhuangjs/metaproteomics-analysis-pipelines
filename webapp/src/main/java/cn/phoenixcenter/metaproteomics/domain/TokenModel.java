package cn.phoenixcenter.metaproteomics.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TokenModel {

    private Integer userId;

    private String email;

    private String token;
}
