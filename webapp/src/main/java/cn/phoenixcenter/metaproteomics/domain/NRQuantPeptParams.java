package cn.phoenixcenter.metaproteomics.domain;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class NRQuantPeptParams {
    private boolean equateIL;
}
