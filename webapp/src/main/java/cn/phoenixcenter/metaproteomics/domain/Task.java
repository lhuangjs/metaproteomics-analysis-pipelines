package cn.phoenixcenter.metaproteomics.domain;

import cn.phoenixcenter.metaproteomics.base.ICommand;
import lombok.*;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Task {

    @NonNull
    private String id;

    @NonNull
    private ICommand command;

    @NonNull
    private String email;

    private String url;

    private Integer userId;

    @Builder.Default
    private Status status = Status.WAITING;

    private Date createTime;

    private Date completedTime;

    public enum Status {
        // the task is waiting for running
        WAITING,
        RUNNING,
        // the task is finished but does not notify user
        NON_NOTIFIED,
        COMPLETED,
        ERROR
    }

    public Task(String id) {
        this.id = id;
    }
}
