package cn.phoenixcenter.metaproteomics.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.nio.file.Path;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class User {

    private Integer id;

    private String email;

    private String password;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    // sign up, reset password
    private String code;

    private Path rootDirPath;

    private Path workspacePath;

    private Path paramDirPath;

    private Path libraryDirPath;

    private Path reportDirPath;
}
