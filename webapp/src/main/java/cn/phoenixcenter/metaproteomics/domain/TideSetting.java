package cn.phoenixcenter.metaproteomics.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * The class stores a user's parameter setting.
 * We can obtain library name, library location(constant path + library name)
 * and parameter location(constant path + name)
 */
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class TideSetting implements Serializable {

    private static final long serialVersionUID = 7947832921241235669L;

    private Integer id;

    @NonNull
    private String name;

    @NonNull
    private String libraryName;

    @NonNull
    private Integer userId;

    private LocalDateTime createTime;
}
