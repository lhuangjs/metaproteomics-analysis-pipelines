package cn.phoenixcenter.metaproteomics.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Peptide vo in protein-centric pipeline
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PeptideInProtVO {

    private String sequence;

    private double qvalue;

    private double prob;

    private int psmCount;

    private String charge;

}
