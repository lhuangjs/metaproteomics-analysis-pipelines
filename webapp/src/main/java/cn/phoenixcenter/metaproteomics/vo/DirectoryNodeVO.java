package cn.phoenixcenter.metaproteomics.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DirectoryNodeVO {

    private String name;

    // the file or dir path:
    // user root path + / +
    // parent path + / +
    // name
    private String parent;

    private Boolean isDir;

    // modified time
    private String modified;

    // If File object points to a file, the field is no null
    // If File object points to a dir, the field is null
    // unit: MB
    private Double size;

    // If File object points to a file, the field is the suffix of file
    // If File object points to a dir, the field is null
    private String fileType;

    // If File object points to a file, the field is null
    // If File object points to a dir, the field is no null(size >= 0)
    private DirectoryNodeVO[] children;
}
