package cn.phoenixcenter.metaproteomics.vo;


import cn.phoenixcenter.metaproteomics.domain.Task;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;

@Data
@NoArgsConstructor
public class TaskVO {

    private String taskId;

    private String command;

    private Task.Status status = Task.Status.WAITING;

    private String createTime;

    private String completedTime;

    public TaskVO(Task task) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.taskId = task.getId();
        this.command = task.getCommand().getDesc();
        this.status = task.getStatus();
        this.createTime = format.format(task.getCreateTime());
        this.completedTime = task.getCompletedTime() == null
                ? null
                : format.format(task.getCompletedTime());
    }
}
