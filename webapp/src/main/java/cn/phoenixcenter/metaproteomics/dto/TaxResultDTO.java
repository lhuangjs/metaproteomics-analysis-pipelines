package cn.phoenixcenter.metaproteomics.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaxResultDTO {

    private List<String> remarkList;

    private String tree;

    private String quant;

    private String goAnno;
}
