package cn.phoenixcenter.metaproteomics.service.impl;

import cn.phoenixcenter.metaproteomics.base.ICommand;
import cn.phoenixcenter.metaproteomics.command.impl.MacroPercolatorCmd;
import cn.phoenixcenter.metaproteomics.command.impl.MacroTideIndexCmd;
import cn.phoenixcenter.metaproteomics.common.Cons;
import cn.phoenixcenter.metaproteomics.dao.TideSettingMapper;
import cn.phoenixcenter.metaproteomics.domain.Task;
import cn.phoenixcenter.metaproteomics.domain.TideSetting;
import cn.phoenixcenter.metaproteomics.domain.User;
import cn.phoenixcenter.metaproteomics.pipeline.MacroCommand;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.TideIndexCmd;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.TideParamParser;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.TideSearchCmd;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.model.TideParam;
import cn.phoenixcenter.metaproteomics.pipeline.inference.FidoCmd;
import cn.phoenixcenter.metaproteomics.pipeline.peptide_validation.PercolatorCmd;
import cn.phoenixcenter.metaproteomics.pipeline.peptide_validation.model.PercolatorParam;
import cn.phoenixcenter.metaproteomics.service.ITaskManagerService;
import cn.phoenixcenter.metaproteomics.service.IProteinCentricPipelineService;
import cn.phoenixcenter.metaproteomics.utils.CheckUtil;
import cn.phoenixcenter.metaproteomics.utils.FileReaderUtil;
import cn.phoenixcenter.metaproteomics.vo.PeptideInProtVO;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ProteinCentricPipelineServiceImpl implements IProteinCentricPipelineService {

    @Value("${crux}")
    private String cruxLocation;

    @Value("${prot.param.dir}")
    private String paramDir;

    @Value("${prot.library.dir}")
    private String libraryDir;

    @Autowired
    private ITaskManagerService taskManagerService;

    @Autowired
    private TideSettingMapper tideSettingMapper;

    @Override
    public boolean checkParamSettingName(String paramSettingName, Integer userId) {
        return tideSettingMapper.selectByUserIdAndName(userId, paramSettingName) != null;
    }

    @Override
    public void prepareTide(String baseUrl,
                            User user,
                            String paramSettingName,
                            String fastaFile,
                            String libraryName,
                            TideParam tideParam) throws IOException, IllegalAccessException {
        // check
        CheckUtil.check(
                tideSettingMapper.selectByUserIdAndName(user.getId(), paramSettingName) == null,
                "Parameter setting name exists");
        // write parameter file
        Path paramPath = user.getParamDirPath().resolve(paramSettingName);
        TideParamParser.toParamFile(tideParam, paramPath);
        // build tide-index command
        Path libraryPath = user.getLibraryDirPath().resolve(libraryName);
        Path tideIndexOutPath = Paths.get(user.getRootDirPath().toString(), libraryDir, libraryName + "_out");
        String absoluteFastaFile = user.getRootDirPath().toString() + File.separator + fastaFile;
        TideIndexCmd tideIndexCmd = new TideIndexCmd(paramPath.toString(),
                absoluteFastaFile, libraryPath.toString(), tideIndexOutPath.toString());
        TideSetting tideSetting = new TideSetting(paramSettingName, libraryName, user.getId());
        ICommand microTideIndexCmd = new MacroTideIndexCmd(tideIndexCmd, tideSetting);
        // add tide-index task
        String resultUrl = baseUrl;
        Task task = Task.builder()
                .id(UUID.randomUUID().toString())
                .command(microTideIndexCmd)
                .email(user.getEmail())
                .url(resultUrl)
                .userId(user.getId())
                .build();
        taskManagerService.addTask(task);
    }

    @Override
    public List<TideSetting> getParams(Integer userId) {
        return tideSettingMapper.selectByUserId(userId);
    }

    @Override
    public List<String> getParamNames(Integer userId) {
        return tideSettingMapper.selectByUserId(userId)
                .stream()
                .map(TideSetting::getName)
                .collect(Collectors.toList());
    }

    @Override
    public void taskEditor(JsonNode node, User user) {
        String taskId = UUID.randomUUID().toString();
        MacroCommand macroCommand = new MacroCommand();
        JsonNode paramsNode = node.get("params");
        if (node.get("enableTideSearch").asBoolean()) {
            JsonNode tideSearchParams = paramsNode.get("tide-search");
            TideSetting tideSetting = tideSettingMapper.selectByUserIdAndName(user.getId(),
                    tideSearchParams.get("paramSettingName").asText());
            String paramFile = user.getParamDirPath().resolve(tideSetting.getName()).toString();
            JsonNode rowFilesNode = tideSearchParams.get("rawFiles");
            String[] mgfFiles = new String[rowFilesNode.size()];
            Iterator<JsonNode> itr = rowFilesNode.elements();
            for (int i = 0; itr.hasNext(); i++) {
                mgfFiles[i] = user.getRootDirPath() + File.separator + itr.next().asText();
            }
            String libraryDir = user.getLibraryDirPath().resolve(tideSetting.getLibraryName()).toString();
            String outputDir = user.getRootDirPath() + File.separator + tideSearchParams.get("outputDir").asText();
            TideSearchCmd tideSearchCmd = new TideSearchCmd(paramFile, mgfFiles, libraryDir, outputDir);
            macroCommand.addCommand(tideSearchCmd);
        }
        if (node.get("enablePercolator").asBoolean()) {
            JsonNode percolatorParams = paramsNode.get("percolator");
            String decoyPrefix = percolatorParams.get("decoyPrefix").asText();
            String featureFile = user.getRootDirPath() + File.separator + percolatorParams.get("featureFile").asText();
            String outputDir = user.getRootDirPath() + File.separator + percolatorParams.get("outputDir").asText();
            PercolatorParam percolatorParam = new PercolatorParam(decoyPrefix, outputDir, featureFile);
            PercolatorCmd percolatorCmd = new PercolatorCmd(percolatorParam);
            Path reportPath = user.getReportDirPath().resolve(taskId);
            if (Files.notExists(reportPath)) {
                try {
                    Files.createDirectory(reportPath);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            MacroPercolatorCmd macroPercolatorCmd = new MacroPercolatorCmd(percolatorCmd,
                    outputDir + File.separator + Cons.PERCOLATOR_PEPTIDE_OUT_FILENAME,
                    reportPath.resolve(Cons.PEPTIDE_REPORT).toString()
            );
            macroCommand.addCommand(macroPercolatorCmd);
        }
        if (node.get("enableFido").asBoolean()) {
            JsonNode fidoParams = paramsNode.get("fido");
            String percolatorXml = user.getRootDirPath() + File.separator + fidoParams.get("percolatorXml").asText();
            double fdrThreshold = fidoParams.get("peptideLevelFDR").asDouble();
            int accuracyLevel = fidoParams.get("accuracyLevel").asInt();
            String outputFile = String.join(File.separator, user.getRootDirPath().toString(),
                    fidoParams.get("outputDir").asText(),
                    "fido.out.text");
//            FidoCmd fidoCmd = new FidoCmd(percolatorXml, fdrThreshold, accuracyLevel, outputFile);
            // TODO fix
            FidoCmd fidoCmd = new FidoCmd();
            macroCommand.addCommand(fidoCmd);
        }
        // add task
        String resultUrl = "task";
        Task task = Task.builder()
                .id(taskId)
                .command(macroCommand)
                .email(user.getEmail())
                .url(resultUrl)
                .userId(user.getId())
                .build();
        taskManagerService.addTask(task);
    }

    @Override
    public List<PeptideInProtVO> peptideReport(String taskId, User user) throws IOException {
        String peptideTSVFile = Paths.get(user.getReportDirPath().toString(), taskId, Cons.PEPTIDE_REPORT).toString();
        FileReaderUtil reader = new FileReaderUtil(peptideTSVFile, "\t", true);
        Function<String[], PeptideInProtVO> rowProcessor = (String[] tmp) -> {
            return new PeptideInProtVO(
                    tmp[reader.getColNum("Sequence")],
                    Double.parseDouble(tmp[reader.getColNum("Q-value")]),
                    Double.parseDouble(tmp[reader.getColNum("Probability")]),
                    Integer.parseInt(tmp[reader.getColNum("PSM count")]),
                    tmp[reader.getColNum("Charge")]
            );
        };
        return reader.read(rowProcessor);
    }
}
