package cn.phoenixcenter.metaproteomics.service.impl;

import cn.phoenixcenter.metaproteomics.common.exception.CreateUserException;
import cn.phoenixcenter.metaproteomics.dao.UserMapper;
import cn.phoenixcenter.metaproteomics.domain.User;
import cn.phoenixcenter.metaproteomics.service.IMailService;
import cn.phoenixcenter.metaproteomics.service.ITokenManageService;
import cn.phoenixcenter.metaproteomics.service.IUserService;
import cn.phoenixcenter.metaproteomics.utils.CheckUtil;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalNotification;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
@Log4j2
public class UserServiceImpl implements IUserService, InitializingBean {

    @Value("${sign.up.expiration.minute}")
    private long signUpExp;

    @Value("${prot.data.dir}")
    private String protDataDir;

    @Value("${prot.param.dir}")
    private String protParamDir;

    @Value("${prot.library.dir}")
    private String protLibraryDir;

    @Value("${prot.report.dir}")
    private String protReportDir;

    @Value("${prot.user.workspace}")
    private String workspaceDir;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private IMailService mailService;

    @Autowired
    private ITokenManageService tokenManageService;

    // email => user info, the cache is used when user register a new account or reset password.
    private Cache<String, User> email2UserCache;

    @Override
    public void afterPropertiesSet() throws Exception {
        email2UserCache = CacheBuilder.newBuilder()
                .expireAfterWrite(signUpExp, TimeUnit.MINUTES)
                .removalListener((RemovalNotification<String, User> e) -> {
                    log.debug("Account <{}> will be deleted because the user did not " +
                            "activate account within the specified time", e.getKey());
                })
                .build();
    }

    @Override
    public boolean checkEmail(String email) {
        return userMapper.checkEmail(email) == 1;
    }

    /**
     * Verify user by email and password.
     * If the user exists, erase password and set fields id, createTime and updateTime
     * against original user instance and then return true.
     * If not return false.
     *
     * @param user
     * @return
     */
    public boolean verifyUser(User user) {
        User userInfo = userMapper.getUserByEmailPassword(user.getEmail(), user.getPassword());
        if (user != null) {
            user.setPassword(null);
            user.setId(userInfo.getId());
            user.setCreateTime(userInfo.getCreateTime());
            user.setUpdateTime(userInfo.getUpdateTime());
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void signUp(String userUrl, String email, String password) {
        // check email
        CheckUtil.check(checkEmail(email), "Email has been registered.");
        // insert user
        String code = UUID.randomUUID().toString();
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        user.setCode(code);
        email2UserCache.put(email, user);
        // send user activation email
        new Thread(() -> {
            Map<String, Object> params = new HashMap<>();
            params.put("url", userUrl + "/activate?email=" + email + "&code=" + code);
            params.put("time", signUpExp);
            mailService.sendTemplateEmail(email, "User Activation", params, "UserActivation.ftl");
        }).start();
    }

    @Override
    public boolean activateUser(String email, String code) {
        User user = email2UserCache.getIfPresent(email);
        if (user != null && user.getEmail().equals(email) && user.getCode().equals(code)) {
            try {
                createUserDirs(user.getEmail());
            } catch (IOException e) {
                throw new CreateUserException("Failed to create user directories");
            }
            // insert user info into db
            userMapper.insert(user);
            return true;
        } else {
            return false;
        }
    }

    private void createUserDirs(String email) throws IOException {
        Path userRootDir = Paths.get(protDataDir, email);
        Path[] paths = new Path[]{
                userRootDir.resolve(protParamDir),
                userRootDir.resolve(protLibraryDir),
                userRootDir.resolve(workspaceDir)
        };
        for (Path path : paths) {
            Files.createDirectories(path);
        }
    }

    @Override
    public String signIn(User user) {
        // check user
        CheckUtil.notNull(user.getEmail(), "Input params is null");
        CheckUtil.check(verifyUser(user), "Incorrect username or password");
        // create token
        String token = tokenManageService.createToken(user);
        // set user root dir
        Path userRootDirPath = Paths.get(this.protDataDir, user.getEmail()); // protein-centric pipeline dir
        user.setRootDirPath(userRootDirPath);
        user.setWorkspacePath(userRootDirPath.resolve(workspaceDir));
        user.setParamDirPath(userRootDirPath.resolve(protParamDir));
        user.setLibraryDirPath(userRootDirPath.resolve(protLibraryDir));
        user.setReportDirPath(userRootDirPath.resolve(protReportDir));
        return token;
    }

    @Override
    public void signOut(String token) {
        tokenManageService.deleteToken(token);
    }

    @Override
    public void applyForResetPassword(String userUrl, String email) {
        // check the email
        User user = userMapper.getUserByEmail(email);
        CheckUtil.notNull(user, "The email does not exist. ");
        // put user info into cache
        String code = UUID.randomUUID().toString();
        user.setCode(code);
        email2UserCache.put(email, user);
        // send password reset email
        Map<String, Object> params = new HashMap<>();
        params.put("url", String.join("", userUrl + "/password/reset",
                "?email=" + email, "&code=" + code)
        );
        mailService.sendTemplateEmail(user.getEmail(), "Password reset", params, "PasswordReset.ftl");
    }

    @Override
    public void confirmResetPassword(User user) {
        CheckUtil.notNull(user.getEmail(), "The url is invalid.");
        User oldUser = email2UserCache.getIfPresent(user.getEmail());
        CheckUtil.notNull(oldUser, "The url is invalid.");
        CheckUtil.equals(oldUser.getCode(), user.getCode(), "The url is invalid.");
        user.setId(oldUser.getId());
        userMapper.updatePasswordByPK(user);
    }
}
