package cn.phoenixcenter.metaproteomics.service;

import cn.phoenixcenter.metaproteomics.domain.User;
import cn.phoenixcenter.metaproteomics.vo.DirectoryNodeVO;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

public interface IFileManagerService {

    /**
     * Return dir structure.
     *
     * @param dirPath
     * @param parentDir
     * @return
     */
    DirectoryNodeVO getDirStructure(Path dirPath, String parentDir);

    /**
     * Create new folder
     *
     * @param activeNode
     * @param newFolderName
     * @param user
     * @return
     */
    DirectoryNodeVO createFolder(DirectoryNodeVO activeNode, String newFolderName, User user);

    DirectoryNodeVO upload(MultipartFile file, DirectoryNodeVO activeNode, User user);
}
