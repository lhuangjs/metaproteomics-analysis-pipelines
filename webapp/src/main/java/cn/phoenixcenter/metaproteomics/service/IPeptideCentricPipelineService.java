package cn.phoenixcenter.metaproteomics.service;

import cn.phoenixcenter.metaproteomics.dto.TaxResultDTO;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;

public interface IPeptideCentricPipelineService {
    String submitTask(MultipartFile file, boolean equateIL,
                    String email, String resultUrl) throws IOException;

    TaxResultDTO getResultData(String taskId);

    Path pid2Prot(String taskId, Taxon.Rank rank, int minUniqPeptCount);

    Path getLCAResult(String taskId);
}
