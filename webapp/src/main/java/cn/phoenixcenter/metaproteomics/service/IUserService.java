package cn.phoenixcenter.metaproteomics.service;

import cn.phoenixcenter.metaproteomics.domain.User;

public interface IUserService {

    /**
     * Check if the email is registered when the new user registers.
     *
     * @param email
     * @return true - if the email exists
     */
    boolean checkEmail(String email);

    /**
     * Create a new account.
     *
     * @param userUrl  the base url for user manager(http://xxxx/user)
     * @param email
     * @param password
     */
    void signUp(String userUrl, String email, String password);

    /**
     * Verify email and code. Only when email and code are matched, insert user info into db(activate account).
     *
     * @param email
     * @param code
     * @return
     */
    boolean activateUser(String email, String code);

    String signIn(User user);

    void signOut(String token) ;

    void applyForResetPassword(String userUrl, String email);

    void confirmResetPassword(User user);
}
