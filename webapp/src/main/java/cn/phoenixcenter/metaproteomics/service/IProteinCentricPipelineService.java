package cn.phoenixcenter.metaproteomics.service;

import cn.phoenixcenter.metaproteomics.domain.TideSetting;
import cn.phoenixcenter.metaproteomics.domain.User;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.model.TideParam;
import cn.phoenixcenter.metaproteomics.vo.PeptideInProtVO;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.util.List;

public interface IProteinCentricPipelineService {

    /**
     * Check whether the prama setting name exists.
     * Return true if it exists, if not return false.
     *
     * @param paramSettingName
     * @param userId
     * @return
     */
    boolean checkParamSettingName(String paramSettingName, Integer userId);

    void prepareTide(String baseUrl,
                     User user,
                     String paramSettingName,
                     String fastaFile,
                     String libraryName,
                     TideParam tideParam) throws IOException, IllegalAccessException;

    List<TideSetting> getParams(Integer userId);

    List<String> getParamNames(Integer userId);

    void taskEditor(JsonNode node, User user);

    List<PeptideInProtVO> peptideReport(String taskId, User user) throws IOException;
}
