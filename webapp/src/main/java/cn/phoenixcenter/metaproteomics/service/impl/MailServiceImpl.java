package cn.phoenixcenter.metaproteomics.service.impl;

import cn.phoenixcenter.metaproteomics.common.exception.MailSendException;
import cn.phoenixcenter.metaproteomics.service.IMailService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Map;

@Service
public class MailServiceImpl implements IMailService {

    @Value("${spring.mail.username}")
    private String fromUser;

    @Autowired
    private JavaMailSender javaMailSender;

    private Configuration cfg;

    public MailServiceImpl() {
        cfg = new Configuration(Configuration.getVersion());
        cfg.setClassForTemplateLoading(MailServiceImpl.class, "/email_template");
        cfg.setDefaultEncoding("UTF-8");
    }

    public void sendTemplateEmail(String toUser, String subject, Map<String, Object> params, String templateName) {
        try {
            MimeMessage mMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper mMessageHelper = new MimeMessageHelper(mMessage, true, "UTF-8");
            mMessageHelper.setFrom(fromUser);
            mMessageHelper.setTo(toUser);
            mMessageHelper.setSubject(subject);
            Template template = cfg.getTemplate(templateName);
            String text = FreeMarkerTemplateUtils.processTemplateIntoString(template, params);
            mMessageHelper.setText(text, true);
            javaMailSender.send(mMessage);
        } catch (IOException | TemplateException | MessagingException e) {
            throw new MailSendException(e);
        }
    }

    public void sendMail(String toUser, String subject, String message) throws MessagingException {
        MimeMessage mMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mMessageHelper = new MimeMessageHelper(mMessage, false, "UTF-8");
        mMessageHelper.setFrom(fromUser);
        mMessageHelper.setTo(toUser);
        mMessageHelper.setSubject(subject);
        // true: html
        mMessageHelper.setText(message, true);
        javaMailSender.send(mMessage);
    }

    public void sendMail(String toUser, String message) throws MessagingException {
        sendMail(toUser, "Metaproteomics analysis", message);
    }

    public void sendTaxAnalysisResultMail(String toUser, String url) throws MessagingException {
        StringBuilder sb = new StringBuilder();
        sb.append("Hello, your task has finished, please check the result by <a href=" + url + ">" + url + "</a>");
        sendMail(toUser, sb.toString());
    }

    public void sendTaskMail(String toUser, String url) throws MessagingException {
        StringBuilder sb = new StringBuilder();
        sb.append("Hello, your task has finished, please check the result by <a href=" + url + ">" + url + "</a>");
        sendMail(toUser, sb.toString());
    }
}
