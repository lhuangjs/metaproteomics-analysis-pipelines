package cn.phoenixcenter.metaproteomics.service;

import cn.phoenixcenter.metaproteomics.domain.Task;
import cn.phoenixcenter.metaproteomics.vo.TaskVO;

import java.util.List;

public interface ITaskManagerService {

    void addTask(Task task);

    /**
     * Search and return task by user id.
     * Return null if there is no task for the user
     *
     * @param userId
     * @return
     */
    List<TaskVO> getTaskListByUserId(Integer userId);
}
