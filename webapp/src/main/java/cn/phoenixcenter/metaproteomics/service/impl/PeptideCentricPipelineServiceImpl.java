package cn.phoenixcenter.metaproteomics.service.impl;

import cn.phoenixcenter.metaproteomics.command.impl.PeptideAnalysisCmd;
import cn.phoenixcenter.metaproteomics.common.exception.ReadFileException;
import cn.phoenixcenter.metaproteomics.common.exception.WriteFileException;
import cn.phoenixcenter.metaproteomics.domain.Task;
import cn.phoenixcenter.metaproteomics.dto.TaxResultDTO;
import cn.phoenixcenter.metaproteomics.nr.TaxAnalysis;
import cn.phoenixcenter.metaproteomics.service.IPeptideCentricPipelineService;
import cn.phoenixcenter.metaproteomics.service.ITaskManagerService;
import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PeptideCentricPipelineServiceImpl implements IPeptideCentricPipelineService {

    @Value("${pept.data.dir}")
    private String peptDataDir;

    @Value("${pept.experiment.filename}")
    private String experimentFilename;

    @Value("${pept.lca.filename}")
    private String lcaFilename;

    @Value("${pept.go.anno.filename}")
    private String goAnnoFilename;

    @Value("${pept.tree.filename}")
    private String treeFilename;

    @Value("${pept.quant.filename}")
    private String quantFilename;

    @Value("${pept.prot.filename}")
    private String protFilename;

    @Value("${pept.fasta.filename}")
    private String fastaFilename;

    @Autowired
    private ITaskManagerService taskManagerService;

    @Override
    public String submitTask(MultipartFile file, boolean equateIL,
                             String email, String resultUrl) throws IOException {
        String taskId = UUID.randomUUID().toString();
        Path taskDirPath = Paths.get(peptDataDir, taskId);
        Files.createDirectory(taskDirPath);
        file.transferTo(taskDirPath.resolve(experimentFilename));
        PeptideAnalysisCmd peptideAnalysisCmd = new PeptideAnalysisCmd(
                equateIL,
                taskDirPath.resolve(experimentFilename).toString(),
                taskDirPath.resolve(lcaFilename).toString(),
                taskDirPath.resolve(goAnnoFilename).toString(),
                taskDirPath.resolve(treeFilename).toString(),
                taskDirPath.resolve(quantFilename).toString()
        );
        // the result url
        String url = resultUrl.charAt(resultUrl.length() - 1) == '/'
                ? resultUrl + taskId
                : resultUrl + "/" + taskId;
        Task task = Task.builder()
                .id(taskId)
                .command(peptideAnalysisCmd)
                .email(email)
                .url(url)
                .build();
        taskManagerService.addTask(task);
        return url;
    }

    public TaxResultDTO getResultData(String taskId) {
        TaxResultDTO taxResultDTO = new TaxResultDTO();
        try {
            BufferedReader br = Files.newBufferedReader(Paths.get(peptDataDir, taskId, lcaFilename), StandardCharsets.UTF_8);
            String line;
            List<String> remarkList = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                if (line.startsWith("## ")) {
                    if (line.startsWith("## Equate I and L: ")) {
                        remarkList.add(line.substring(3));
                    } else {
                        int idx = line.indexOf(":");
                        remarkList.add(line.substring(3, idx == -1 ? line.length() : idx));
                    }

                } else {
                    break;
                }
            }
            br.close();
            taxResultDTO.setRemarkList(remarkList);
            taxResultDTO.setTree(
                    Files.readAllLines(Paths.get(peptDataDir, taskId, treeFilename), StandardCharsets.UTF_8)
                            .stream()
                            .collect(Collectors.joining())
            );
            taxResultDTO.setQuant(
                    Files.readAllLines(Paths.get(peptDataDir, taskId, quantFilename), StandardCharsets.UTF_8)
                            .stream()
                            .collect(Collectors.joining())
            );
            taxResultDTO.setGoAnno(
                    Files.readAllLines(Paths.get(peptDataDir, taskId, goAnnoFilename), StandardCharsets.UTF_8)
                            .stream()
                            .collect(Collectors.joining())
            );
        } catch (IOException e) {
            throw new ReadFileException("Cannot read result data for task [" + taskId + "]", e);
        }
        return taxResultDTO;
    }

    public Path pid2Prot(String taskId, Taxon.Rank rank, int minUniqPeptCount) {
        try {
            Path nrSearcherDataDirPath = Paths.get(peptDataDir, taskId);
            Path fastaPath = nrSearcherDataDirPath.resolve(fastaFilename);
            TaxAnalysis.getProteins(nrSearcherDataDirPath.resolve(protFilename),
                    rank, minUniqPeptCount, fastaPath);
            return fastaPath;
        } catch (IOException e) {
            throw new WriteFileException(e);
        }
    }

    public Path getLCAResult(String taskId) {
        Path lcaPath = Paths.get(peptDataDir, taskId, lcaFilename);
        if (Files.exists(lcaPath)) {
            return lcaPath;
        } else {
            return null;
        }
    }
}
