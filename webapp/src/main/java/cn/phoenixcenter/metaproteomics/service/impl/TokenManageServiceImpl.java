package cn.phoenixcenter.metaproteomics.service.impl;

import cn.phoenixcenter.metaproteomics.common.exception.AuthorizationException;
import cn.phoenixcenter.metaproteomics.domain.User;
import cn.phoenixcenter.metaproteomics.service.ITokenManageService;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
@Log4j2
public class TokenManageServiceImpl implements ITokenManageService, InitializingBean {

    @Value("${token.expiration.minute}")
    private long tokenExp;

    // cache
    private Cache<String, User> token2User;

    @Override
    public void afterPropertiesSet() {
        token2User = CacheBuilder.newBuilder()
                .expireAfterAccess(tokenExp, TimeUnit.MINUTES)
                .removalListener(e -> log.debug("{} has been removed", e.getKey()))
                .build();
    }

    @Override
    public String createToken(User user) {
        String token = UUID.randomUUID().toString();
        token2User.put(token, user);
        return token;
    }

    @Override
    public User checkToken(String token) {

        if (token == null) {
            throw new AuthorizationException("The token is invalid!");
        }
        User user = token2User.getIfPresent(token);
        if (user != null) {
            return user;
        } else {
            throw new AuthorizationException("The token is invalid!");
        }
    }

    @Override
    public void deleteToken(String token) {
        token2User.invalidate(token);
    }
}
