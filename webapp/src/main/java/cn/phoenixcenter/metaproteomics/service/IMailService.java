package cn.phoenixcenter.metaproteomics.service;

import javax.mail.MessagingException;
import java.util.Map;

public interface IMailService {

    void sendMail(String toUser, String subject, String message) throws MessagingException;

    void sendMail(String toUser, String message) throws MessagingException;

    void sendTaxAnalysisResultMail(String toUser, String url) throws MessagingException;

    void sendTemplateEmail(String toUser, String subject, Map<String, Object> params, String templateName);
}
