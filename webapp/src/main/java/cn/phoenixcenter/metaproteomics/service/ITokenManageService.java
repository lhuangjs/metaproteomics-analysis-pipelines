package cn.phoenixcenter.metaproteomics.service;

import cn.phoenixcenter.metaproteomics.domain.User;

public interface ITokenManageService {

    /**
     * Create token for a user.
     *
     * @param user
     * @return
     */
    String createToken(User user);

    /**
     * Check if this token exists.
     *
     * @param token
     * @return
     */
    User checkToken(String token);

    /**
     * Delete the token from cache.
     *
     * @param email
     */
    void deleteToken(String email);
}
