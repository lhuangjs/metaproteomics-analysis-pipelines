package cn.phoenixcenter.metaproteomics.service.impl;

import cn.phoenixcenter.metaproteomics.common.exception.CreateFolderException;
import cn.phoenixcenter.metaproteomics.common.exception.ReadFileException;
import cn.phoenixcenter.metaproteomics.common.exception.WriteFileException;
import cn.phoenixcenter.metaproteomics.domain.User;
import cn.phoenixcenter.metaproteomics.service.IFileManagerService;
import cn.phoenixcenter.metaproteomics.utils.CheckUtil;
import cn.phoenixcenter.metaproteomics.vo.DirectoryNodeVO;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class FileManagerServiceImpl implements IFileManagerService {

    @Override
    public DirectoryNodeVO getDirStructure(Path dirPath, String parentDir) {
        try {
            DirectoryNodeVO rootNode = new DirectoryNodeVO();
            rootNode.setName(dirPath.getFileName().toString());
            rootNode.setParent(parentDir);
            rootNode.setModified(Files.getLastModifiedTime(dirPath).toString());
            if (Files.isDirectory(dirPath)) {
                rootNode.setIsDir(true);
                DirectoryNodeVO[] children = Files.list(dirPath)
                        .map(path -> getDirStructure(path,
                                rootNode.getParent() == null
                                        ? rootNode.getName()
                                        : rootNode.getParent() + File.separator + rootNode.getName())
                        )
                        .toArray(DirectoryNodeVO[]::new);
                // if the folder is empty: children array exists but the length is 0
                rootNode.setChildren(children);
            } else {
                rootNode.setIsDir(false);
                rootNode.setSize(Files.size(dirPath) / 1024.0 / 1024.0);
                int idx = rootNode.getName().lastIndexOf(".");
                rootNode.setFileType(idx == -1 ? "" : rootNode.getName().substring(idx + 1));
            }
            return rootNode;
        } catch (IOException e) {
            throw new ReadFileException(e);
        }
    }

    @Override
    public DirectoryNodeVO createFolder(DirectoryNodeVO activeNode, String newFolderName, User user) {
        String parent = activeNode.getParent() == null
                ? activeNode.getName()
                : activeNode.getParent() + File.separator + activeNode.getName();
        Path newFolderAbsolutePath = Paths.get(user.getRootDirPath().toString(), parent, newFolderName);
        CheckUtil.check(Files.notExists(newFolderAbsolutePath), "Folder already exists");
        try {
            Files.createDirectory(newFolderAbsolutePath);
            return getDirStructure(newFolderAbsolutePath, parent);
        } catch (IOException e) {
            throw new CreateFolderException(e);
        }
    }

    @Override
    public DirectoryNodeVO upload(MultipartFile file, DirectoryNodeVO activeNode, User user) {
        String parent = activeNode.getParent() == null
                ? activeNode.getName()
                : activeNode.getParent() + File.separator + activeNode.getName();
        Path dirAbsolutePath = Paths.get(user.getRootDirPath().toString(),
                parent);
        Path fileAbsolutePath = dirAbsolutePath.resolve(file.getOriginalFilename());
        CheckUtil.check(Files.notExists(fileAbsolutePath), "File already exist");
        try {
            file.transferTo(fileAbsolutePath.toFile());
            return getDirStructure(fileAbsolutePath, parent);
        } catch (IOException e) {
            throw new WriteFileException("Failed to upload file: " + file.getOriginalFilename());
        }
    }
}
