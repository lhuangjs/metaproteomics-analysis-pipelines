package cn.phoenixcenter.metaproteomics.service.impl;

import cn.phoenixcenter.metaproteomics.base.ICommand;
import cn.phoenixcenter.metaproteomics.command.impl.MacroTideIndexCmd;
import cn.phoenixcenter.metaproteomics.command.impl.PeptideAnalysisCmd;
import cn.phoenixcenter.metaproteomics.common.exception.CommandExecException;
import cn.phoenixcenter.metaproteomics.dao.TaskMapper;
import cn.phoenixcenter.metaproteomics.dao.TideSettingMapper;
import cn.phoenixcenter.metaproteomics.domain.Task;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.TideSearchCmd;
import cn.phoenixcenter.metaproteomics.service.IMailService;
import cn.phoenixcenter.metaproteomics.service.ITaskManagerService;
import cn.phoenixcenter.metaproteomics.vo.TaskVO;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@Service
@Log4j2
public class TaskManagerServiceImpl implements ITaskManagerService, InitializingBean {

    @Value("${max.task.size}")
    private int maxTaskSize;

    @Value("${max.tide.size}")
    private int maxTideTaskSize;

    @Value("${max.nr-searcher.size}")
    private int maxNRSearcherTaskSize;

    @Autowired
    private TaskMapper taskMapper;

    @Autowired
    private TideSettingMapper tideSettingMapper;

    @Autowired
    private IMailService mailService;

    private LinkedBlockingQueue<Runnable> taskQueue;

    private ExecutorService executorService;

    // the flow control of tide task
    private Semaphore tideTaskSema;

    // the flow control of nr-searcher task
    private Semaphore nrSearcherTaskSema;

    @Override
    public void afterPropertiesSet() throws Exception {
        taskQueue = new LinkedBlockingQueue<>(maxTaskSize);
        executorService = new ThreadPoolExecutor(maxTaskSize,
                maxTaskSize,
                0L,
                TimeUnit.MILLISECONDS,
                taskQueue,
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.DiscardPolicy());
        tideTaskSema = new Semaphore(maxTideTaskSize, true);
        nrSearcherTaskSema = new Semaphore(maxNRSearcherTaskSize, true);
        synchronized (taskQueue) {
            log.info("Deal with interrupted tasks");
            execInterruptedTasks();
        }
        log.info("Start to execute tasks");
    }

    /**
     * Add task into queue. we need do somethings before add the newest task:
     * 1. Add tasks from db if the queue has not be filled
     * 2. Add the newest task into queue if the queue has not be filled
     *
     * @param task
     */
    @Override
    public void addTask(Task task) {
        // the task queue has not be filled
        if (taskQueue.remainingCapacity() > 0) {
            List<Task> waitingTaskList = taskMapper.getNTaskByStatus(Task.Status.WAITING,
                    taskQueue.remainingCapacity());
            // there are waiting tasks in db
            if (waitingTaskList.size() > 0) {
                for (Task waitingTask : waitingTaskList) {
                    waitingTask.setStatus(Task.Status.RUNNING);
                    taskMapper.updateTaskStatus(waitingTask);
                    executorService.execute(wrapTask(waitingTask));
                }
            }
            // the task queue still has not be filled
            if (taskQueue.remainingCapacity() > 0) {
                task.setStatus(Task.Status.RUNNING);
                executorService.execute(wrapTask(task));
            }
        }
        // write db
        taskMapper.insertTask(task);
    }

    /**
     * Customized task executor: control the flow of some tasks
     *
     * @param task
     */
    private Runnable wrapTask(Task task) {
        return () -> {
            try {
                // execute command
                ICommand command = task.getCommand();
                Class clazz = command.getClass();
                if (clazz.isAssignableFrom(MacroTideIndexCmd.class)) {
                    tideTaskSema.acquire();
                    MacroTideIndexCmd macroTideIndexCmd = (MacroTideIndexCmd) command;
                    macroTideIndexCmd.setTideSettingMapper(tideSettingMapper);
                    command.execute();
                    tideTaskSema.release();
                } else if (clazz.isAssignableFrom(TideSearchCmd.class)) {
                    tideTaskSema.acquire();
                    command.execute();
                    tideTaskSema.release();
                } else if (clazz.isAssignableFrom(PeptideAnalysisCmd.class)) {
                    nrSearcherTaskSema.acquire();
                    command.execute();
                    nrSearcherTaskSema.release();
                } else {
                    command.execute();
                }
                // update task status
                task.setStatus(Task.Status.NON_NOTIFIED);
                taskMapper.updateTaskStatus(task);
                // send email to notify user
                // TODO create task result email template
                mailService.sendTaxAnalysisResultMail(task.getEmail(), task.getUrl());
                task.setStatus(Task.Status.COMPLETED);
                taskMapper.updateTaskStatus(task);
            } catch (Exception e) {
                throw new CommandExecException(e);
            }
        };
    }

    /**
     * Execute interrupted task:
     * 1. NON_NOTIFIED: A task is finished but not notify user
     * 2. RUNNING: A task is interrupted when running - rerun the task
     */
    public void execInterruptedTasks() throws MessagingException {
        // NON_NOTIFIED task
        List<Task> nonNotifiedTaskList = taskMapper.getNonNotifiedTasks();
        log.debug("There are {} tasks have not been notified!", nonNotifiedTaskList.size());
        for (Task task : nonNotifiedTaskList) {
            mailService.sendTaxAnalysisResultMail(task.getEmail(), task.getUrl());
        }
        // rerun interrupted task(status: RUNNING)
        List<Task> runningTaskList = taskMapper.getTaskByStatus(Task.Status.RUNNING);
        log.debug("There are {} tasks need to run again", runningTaskList.size());
        for (int i = 0; i < runningTaskList.size(); i++) {
            if (i < maxTaskSize) {
                executorService.execute(wrapTask(runningTaskList.get(i)));
            } else {
                Task task = runningTaskList.get(i);
                task.setStatus(Task.Status.WAITING);
                taskMapper.updateTaskStatus(task);
            }
        }
       if(taskQueue.remainingCapacity() > 0){
           // run new tasks(status: WAITING)
           List<Task> waitingTaskList = taskMapper.getNTaskByStatus(Task.Status.WAITING,
                   taskQueue.remainingCapacity());
           // there are waiting tasks in db
           if (waitingTaskList.size() > 0) {
               for (Task waitingTask : waitingTaskList) {
                   waitingTask.setStatus(Task.Status.RUNNING);
                   taskMapper.updateTaskStatus(waitingTask);
                   executorService.execute(wrapTask(waitingTask));
               }
           }
       }
    }

    public List<TaskVO> getTaskListByUserId(Integer userId) {
        List<Task> taskList = taskMapper.getTaskByUserId(userId);
        return taskList.size() == 0
                ? null
                : taskList.stream()
                .map(task -> new TaskVO(task))
                .collect(Collectors.toList());
    }
}
