<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User Activation</title>
</head>
<body>
<p>
    Hi, thank you for joining the MPAPs.
    Your account has been generated, please click the link below
    to activate your account within ${time} minutes.
    <a href="${url}">${url}</a>.
</p>
</body>
</html>