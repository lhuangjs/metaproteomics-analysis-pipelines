<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User Activation</title>
</head>
<body>
<p>Hello,</p>

<p>You're receiving this e-mail because you or someone else has requested a password reset for your user account at
    MPP.
</p>
<p>Click the link below to reset your password.</p>

<p>Please click the url <a href="${url}">${url}</a> to reset your password.</p>

<p>Ignore the message if you did not request this password reset.</p>

<p>Thanks for using our site!</p>
</p>
</body>
</html>