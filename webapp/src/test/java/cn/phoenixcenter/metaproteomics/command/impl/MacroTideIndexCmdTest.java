package cn.phoenixcenter.metaproteomics.command.impl;

import cn.phoenixcenter.metaproteomics.base.ICommand;
import cn.phoenixcenter.metaproteomics.domain.TideSetting;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.TideIndexCmd;
import org.junit.Test;

import java.io.*;

public class MacroTideIndexCmdTest {

    @Test
    public void writeExternal() throws IOException, ClassNotFoundException {
        TideIndexCmd tideIndexCmd = new TideIndexCmd("param", "fasta", "library", "output");
        TideSetting tideSetting = new TideSetting("name", "libraryName", 1);
        ICommand command = new MacroTideIndexCmd(tideIndexCmd, tideSetting);
        // serialize
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutput out = new ObjectOutputStream(baos);
        out.writeObject(command);
        out.close();
        baos.close();
        // deserialize
        ObjectInput in = new ObjectInputStream(new ByteArrayInputStream(baos.toByteArray()));
        ICommand commandCopy = ICommand.toObj(in);
        in.close();
        System.out.println(commandCopy);
    }
}