package cn.phoenixcenter.metaproteomics.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.mail.MessagingException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/mpa-servlet.xml"})
public class IMailServiceTest {

    @Autowired
    IMailService mailService;

    @Test
    public void sendMail() throws MessagingException {
        mailService.sendMail("1191578955@qq.com", "hello second");
    }
}