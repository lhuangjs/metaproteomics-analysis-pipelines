package cn.phoenixcenter.metaproteomics.service;

import cn.phoenixcenter.metaproteomics.base.ICommand;
import cn.phoenixcenter.metaproteomics.domain.Task;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.TideIndexCmd;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.TideSearchCmd;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class ITaskManagerServiceTest {

    @Autowired
    private ITaskManagerService taskManagerService;

    @Test
    public void addTask() throws InterruptedException {
        int taskCount = 2;
        List<Task> taskList = new ArrayList<>(taskCount);
        for (int i = 0; i < taskCount / 2; i++) {
            // tide-index
            ICommand tideIndexCmd = new TideIndexCmd(
                    "/home/huangjs/Documents/metaprot/protein-pipeline/1191578955@qq.com/.param/default.properties",
                    "/home/huangjs/Documents/metaprot/protein-pipeline/1191578955@qq.com/.sequence/species1.fasta",
                    "/home/huangjs/Documents/metaprot/protein-pipeline/1191578955@qq.com/.library/species1",
                    "/home/huangjs/Documents/metaprot/protein-pipeline/1191578955@qq.com/.result/tide-index-out"
            );
            Task tideIndexTask =Task.builder()
                    .id("tide-index-" + i)
                    .command(tideIndexCmd)
                    .email("1191578955@qq.com")
                    .url("url")
                    .userId(1)
                    .build();
            taskList.add(tideIndexTask);

            // tide-search
            ICommand tideSearchCmd = new TideSearchCmd(
                    "/home/huangjs/Documents/metaprot/protein-pipeline/1191578955@qq.com/.param/default.properties",
                    new String[]{"/home/huangjs/Documents/metaprot/protein-pipeline/1191578955@qq.com/.raw/example1.mgf",
                            "/home/huangjs/Documents/metaprot/protein-pipeline/1191578955@qq.com/.raw/example2.mgf"},
                    "/home/huangjs/Documents/metaprot/protein-pipeline/1191578955@qq.com/.library/species1",
                    "/home/huangjs/Documents/metaprot/protein-pipeline/1191578955@qq.com/.result/tide-search-out"

            );
            Task tideSearchTask =Task.builder()
                    .id("tide-search-" + i)
                    .command(tideIndexCmd)
                    .email("1191578955@qq.com")
                    .url("url")
                    .userId(1)
                    .build();
            taskList.add(tideSearchTask);

        }
        for (int i = 0; i < 4; i++) {
            taskManagerService.addTask(taskList.get(i));
        }
        Thread.sleep(60000);
        for (int i = 4; i < 8; i++) {
            taskManagerService.addTask(taskList.get(i));
        }
        while (true) ;
    }

    @Test
    public void getTaskListByUserId() {
        System.out.println(taskManagerService.getTaskListByUserId(1));
        System.out.println(taskManagerService.getTaskListByUserId(2));
    }
}