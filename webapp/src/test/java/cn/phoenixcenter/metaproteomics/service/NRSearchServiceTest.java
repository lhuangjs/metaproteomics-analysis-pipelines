//package cn.phoenixcenter.metaproteomics.service;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import java.io.IOException;
//import java.utils.ArrayList;
//import java.utils.List;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:mpa-servlet.xml"})
//public class NRSearchServiceTest {
//
//    @Autowired
//    private NRSearchService nrSearchService;
//
//    @Test
//    public void getPeptideInfo() throws IOException {
//        List<String> pepList = new ArrayList<>();
//        pepList.add("SGIVLPGQAQEKPQQAEVVAVGPGGVVDGK");
//        System.err.println(nrSearchService.getPeptideInfo(pepList, true, true));
//    }
//}