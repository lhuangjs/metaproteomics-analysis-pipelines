package cn.phoenixcenter.metaproteomics.service.impl;

import cn.phoenixcenter.metaproteomics.domain.User;
import cn.phoenixcenter.metaproteomics.service.IFileManagerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.nio.file.Paths;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/mpa-servlet.xml"})
public class FileManagerServiceImplTest {

    @Autowired
    private IFileManagerService fileManagerService;

    @Test
    public void getDirStructure() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        User user = new User();
        user.setWorkspacePath(Paths.get("/home/huangjs/Documents/MPAPs/protein-centrix_pipeline/11@qq.com/workspace"));
        System.err.println(objectMapper.writeValueAsString(
                fileManagerService.getDirStructure(user.getWorkspacePath(), null)));
    }
}