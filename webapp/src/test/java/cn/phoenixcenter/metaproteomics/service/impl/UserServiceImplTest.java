package cn.phoenixcenter.metaproteomics.service.impl;

import cn.phoenixcenter.metaproteomics.domain.User;
import cn.phoenixcenter.metaproteomics.service.ITokenManageService;
import cn.phoenixcenter.metaproteomics.service.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/mpa-servlet.xml"})
public class UserServiceImplTest {

    @Autowired
    private IUserService userService;

    @Autowired
    private ITokenManageService tokenManageService;

    @Test
    public void name() {
        User user = new User();
        user.setEmail("1191578955@qq.com");
        user.setPassword("123");
    }

    @Test
    public void signUp() {
        userService.signUp(" http://localhost:8181/user", "1191578955@qq.com", "123");
        while(true);
    }

    @Test
    public void signIn() {
        User user = new User();
        user.setEmail("1191578955@qq.com");
        user.setPassword("123");
    }

    @Test
    public void applyForResetPassword() {
        userService.applyForResetPassword("url", "1191578955@qq.com");
    }

    @Test
    public void confirmResetPassword() {
        User user = new User();
        user.setEmail("1191578955@qq.com");
        user.setPassword("12344");
        user.setCode("9e841409-03bd-4973-8189-acd8cb1f5605");
        userService.confirmResetPassword(user);
    }
}