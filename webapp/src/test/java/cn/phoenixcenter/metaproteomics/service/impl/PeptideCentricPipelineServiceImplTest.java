package cn.phoenixcenter.metaproteomics.service.impl;

import cn.phoenixcenter.metaproteomics.BaseTest;
import cn.phoenixcenter.metaproteomics.command.impl.PeptideAnalysisCmd;
import cn.phoenixcenter.metaproteomics.dao.TaskMapper;
import cn.phoenixcenter.metaproteomics.domain.Task;
import cn.phoenixcenter.metaproteomics.service.ITaskManagerService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class PeptideCentricPipelineServiceImplTest extends BaseTest {

    @Autowired
    private ITaskManagerService taskManagerService;

    @Autowired
    private TaskMapper taskMapper;

    @Test
    public void submitTask() {
        String taskId = UUID.randomUUID().toString();
        PeptideAnalysisCmd peptideAnalysisCmd = new PeptideAnalysisCmd(
                true,
                "experimentFilename",
                "lcaFilename",
                "goAnnoFilename",
                "treeFilename",
                "quantFilename"
        );
        // the result url
        String url = String.join("/", "pept/result", taskId);
        Task task = Task.builder()
                .id(taskId)
                .command(peptideAnalysisCmd)
                .email("email")
                .url(url)
                .build();
        taskManagerService.addTask(task);
        Assertions.assertEquals(taskId, taskMapper.getTaskByPK(taskId).getId());
    }
}