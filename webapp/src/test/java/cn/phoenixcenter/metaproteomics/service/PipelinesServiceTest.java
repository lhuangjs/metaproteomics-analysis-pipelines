//package cn.phoenixcenter.metaproteomics.service;
//
//import cn.phoenixcenter.metaproteomics.domain.PipelineFilter;
//import cn.phoenixcenter.metaproteomics.domain.PipelinesParams;
//import cn.phoenixcenter.metaproteomics.taxonomy.Taxon;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import java.io.IOException;
//import java.utils.ArrayList;
//import java.utils.HashMap;
//import java.utils.List;
//import java.utils.Map;
//
//@ContextConfiguration("classpath:/mpa-servlet.xml")
//@RunWith(SpringJUnit4ClassRunner.class)
//public class PipelinesServiceTest {
//
//    @Autowired
//    private PipelinesService service;
//
//    @Value("${data_dir}")
//    private String dataDir;
//
//    private PipelinesParams pipelinesParams;
//
//    @Before
//    public void setUp() throws Exception {
//        pipelinesParams = new PipelinesParams();
//        pipelinesParams.setProjectId("project1");
//        Map<String, String[]> groupMap = new HashMap<>();
//        groupMap.put("g1", new String[]{"1", "2", "3", "4", "5", "6", "7", "8"});
//        groupMap.put("g2", new String[]{"1m", "2m", "3m", "4m", "5m", "6m", "7m", "8m"});
//        groupMap.put("g3", new String[]{"frac1", "frac2", "frac3", "frac4", "frac5", "frac6", "frac7", "frac8"});
//        pipelinesParams.setGroupMap(groupMap);
//        pipelinesParams.setPid2TidFile("accTaxonIdMapping.txt");
//        // protein-groups-based pipeline
//        pipelinesParams.setPgPipeline(true);
//        pipelinesParams.setPgFile("proteinGroups.txt");
//        List<PipelineFilter> pgFilterList = new ArrayList<>();
//        pgFilterList.add(PipelineFilter.RSITE);
//        pgFilterList.add(PipelineFilter.RREV);
//        pgFilterList.add(PipelineFilter.RCONT);
//        pipelinesParams.setPgFilterList(pgFilterList);
//        pipelinesParams.setPgLCAThreshold(Taxon.Rank.GENUS);
//        pipelinesParams.setMinRazorPlusUniquePepNum(2);
//        // peptide-based pipeline
//        pipelinesParams.setPepFile("peptides.txt");
//
//    }
//
//    @Test
//    public void doPGPipeline() throws IOException {
//        service.doPGPipeline(pipelinesParams);
//    }
//}