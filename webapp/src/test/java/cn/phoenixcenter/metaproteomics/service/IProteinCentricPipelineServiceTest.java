package cn.phoenixcenter.metaproteomics.service;

import cn.phoenixcenter.metaproteomics.domain.User;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.model.TideParam;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.nio.file.Paths;

@SpringBootTest
public class IProteinCentricPipelineServiceTest {

    @Autowired
    private IProteinCentricPipelineService proteinCentricPipelineService;

    @Test
    public void prepareTide() throws IOException, IllegalAccessException {
        User user = new User();
        user.setId(1);
        user.setEmail("1191578955@qq.com");
        user.setRootDirPath(Paths.get("/home/huangjs/Documents/metaprot/protein-pipeline/1191578955@qq.com"));
        TideParam tideParam = new TideParam();
        proteinCentricPipelineService.prepareTide(
                "baseUrl",
                user,
                "paramSettingName",
                "/home/huangjs/Documents/metaprot/protein-pipeline/1191578955@qq.com/.sequence/species1.fasta",
                "libraryName",
                tideParam);
    }
}