package cn.phoenixcenter.metaproteomics.dao;

import cn.phoenixcenter.metaproteomics.BaseTest;
import cn.phoenixcenter.metaproteomics.base.ICommand;
import cn.phoenixcenter.metaproteomics.domain.Task;
import cn.phoenixcenter.metaproteomics.pipeline.database_search.tide.TideIndexCmd;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;


public class TaskMapperTest extends BaseTest {

    @Autowired
    private TaskMapper taskMapper;

    @Test
    public void insertTask() {
        ICommand tideIndexCmd = new TideIndexCmd(
                "/home/huangjs/Documents/metaprot/protein-pipeline/1191578955@qq.com/.param/default.properties",
                "/home/huangjs/Documents/metaprot/protein-pipeline/1191578955@qq.com/.sequence/species1.fasta",
                "/home/huangjs/Documents/metaprot/protein-pipeline/1191578955@qq.com/.library/species1",
                "/home/huangjs/Documents/metaprot/protein-pipeline/1191578955@qq.com/.result/tide-index-out"
        );
        String taskId = UUID.randomUUID().toString();
        Task tideIndexTask = Task.builder()
                .id(taskId)
                .command(tideIndexCmd)
                .email("1191578955@qq.com")
                .url("url")
                .userId(1)
                .build();
        taskMapper.insertTask(tideIndexTask);
        Assertions.assertNotNull(taskMapper.getTaskByPK(taskId));
    }

    @Test
    public void getTaskByUserId() throws Exception {
        List<Task> taskList = taskMapper.getTaskByUserId(1);
        for (Task task : taskList) {
            task.getCommand().execute();
        }
    }

    @Test
    public void getTaskByStatus() {
        List<Task> taskList = taskMapper.getTaskByStatus(Task.Status.WAITING);
        for (Task task : taskList) {
            System.out.println(task);
        }
    }
}